﻿
namespace KSSOCA.Core.Data
{
    using System.Collections.Generic;
    using KSSOCA.Data;
    using System.Linq;

    public class AcessControlService
    {
        ACLDataRepository repository;

        public AcessControlService()
        {
            repository = new ACLDataRepository();
        }

        public List<ACLModel> Get(int roleId)
        {
            return repository.GetACL(roleId);
        }

        public List<ACLDataModel> GetMenuItems(int roleId)
        {
            return repository.GetACL(roleId).Select(m => new ACLDataModel()
            {
                Id = m.Id,
                Name = m.Name,
                Url = m.Url,
                Ordinal = m.Ordinal,
                //View = m.View,
                //Add = m.Add,
                //Edit = m.Edit,
                //Delete = m.Delete,
                ParentId = m.ParentId,
                ShowMenuItem = m.ShowMenuItem
            }).ToList();
        }
    }

    public class ACLDataModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        //public bool Add { get; set; }
        //public bool Edit { get; set; }
        //public bool Delete { get; set; }
        //public bool View { get; set; }
        public int? ParentId { get; set; }
        public bool ShowMenuItem { get; set; }
        public int Ordinal { get; set; }
    }
}
