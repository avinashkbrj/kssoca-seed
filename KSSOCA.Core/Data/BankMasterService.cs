﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class BankMasterService
    {
        BankMasterRepository repository;

        public BankMasterService()
        {
            repository = new BankMasterRepository();
        }

        public List<BankMaster> GetAll()
        {
            return repository.GetAll();
        }
     
        public BankMaster GetById(int id)
        {
            return repository.Get(id);
        }
    }
}
