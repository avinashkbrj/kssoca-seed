﻿namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;
    public class BulkEntryService
    {
        BulkEntryRepository repository;

        public BulkEntryService()
        {
            repository = new BulkEntryRepository();
        }
        
        public BulkEntry GetById(int id)
        {             
             return repository.Get(id);
        }
        public BulkEntry GetbyForm1ID(int Form1ID)
        {
            return repository.GetbyForm1ID(Form1ID);
        }
        public int Create(BulkEntry entity)
        { 
            return repository.Create(entity);
        }
        public int Update(BulkEntry entity)
        { 
            return repository.Update(entity);
        }
        
        public DataTable GetBulkDetails(int userid, int roleid, int form1id, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            return repository.GetBulkDetails(userid,roleid,form1id, FromDate, ToDate).ToDataTable();
        }
    }
}
