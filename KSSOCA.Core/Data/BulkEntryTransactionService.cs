﻿namespace KSSOCA.Core.Data
{
    using System;
    using System.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Data;
    using KSSOCA.Model;

    public class BulkEntryTransactionService
    {
        BulkEntryTransactionRepository repository;

        public BulkEntryTransactionService()
        {
            repository = new BulkEntryTransactionRepository();
        }

        public BulkEntryTransaction GetMaxTransactionDetails(int Form1ID)
        {
            return repository.GetMaxTransactionDetails(Form1ID);
        }
        public int Create(BulkEntryTransaction entity)
        {
            // HashPassword(crop);
            return repository.Create(entity);
        } 
        public DataTable GetAllTransactionsBy(int Form1ID)
        {
            return repository.GetAllTransactions(Form1ID).ToDataTable();
        }
    }
}
