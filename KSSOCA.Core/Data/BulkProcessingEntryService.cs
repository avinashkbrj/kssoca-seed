﻿namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;
    public class BulkProcessingEntryService
    {
        BulkProcessingEntryRepository repository;

        public BulkProcessingEntryService()
        {
            repository = new BulkProcessingEntryRepository();
        }
        public BulkProcessingEntry GetById(int id)
        {
            return repository.Get(id);
        }

        public List<BulkProcessingEntry> GetbyDate(DateTime FromDate, DateTime ToDate)
        {
            return repository.GetbyDate(FromDate, ToDate);
        }
        public BulkProcessingEntry GetbyForm1ID(int Form1ID)
        {
            return repository.GetbyForm1ID(Form1ID);
        }
        public int Create(BulkProcessingEntry entity)
        {
            return repository.Create(entity);
        }
        public int Update(BulkProcessingEntry entity)
        {
            return repository.Update(entity);
        }
        public int GenerateID()
        {
            int lastID = repository.GetLastID();
            return lastID + 1;
        }
        public DataTable GetBulkProcessingDetails(int userid, int roleid, int form1id, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {

            return repository.GetBulkProcessingDetails(userid, roleid, form1id, FromDate, ToDate).ToDataTable();
        }
    }
}
