﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class ClassSeedMasterService
    {
        ClassSeedMasterRepository repository;

        public ClassSeedMasterService()
        {
            repository = new ClassSeedMasterRepository();
        }

        public List<ClassSeedMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<ClassSeedMaster> GetAllClassforCertification()
        {
            return repository.GetAllClassforCertification();
        }
        public List<ClassSeedMaster> GetAllClassProduced(string CertificationClassid )
        {
            return repository.GetAllClassProduced(CertificationClassid);
        }

        public ClassSeedMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(ClassSeedMaster classSeed)
        {
            //HashPassword(cropvariety);
            return repository.Create(classSeed);
        }
        
        public DataTable GetAllClassProducedIDs(int ClassID)
        {
            return repository.GetAllClassProducedIDs(ClassID).ToDataTable();
        }
        public DataTable GetAllClassSeed()
        {
            return repository.GetAllClassSeed().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(ClassSeedMaster classSeed)
        {
            return repository.Update(classSeed);
        }
    }
}
