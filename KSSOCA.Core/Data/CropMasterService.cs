﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{


    public class CropMasterService
    {
        CropMasterRepository repository;

        public CropMasterService()
        {
            repository = new CropMasterRepository();
        }

        public List<CropMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<CropMaster> GetByType(string CropType)
        {
            return repository.GetByType(CropType);
        }

        public CropMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(CropMaster crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public DataTable GetAllCrop()
        {
            return repository.GetAllCrop().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(CropMaster crop)
        {
            return repository.Update(crop);
        }

    }
}
