﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{
    public class CropVarietyMasterService
    {
        CropVarietyMasterRepository repository;

        public CropVarietyMasterService()
        {
            repository = new CropVarietyMasterRepository();
        }

        public List<CropVarietyMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<CropVarietyMaster> GetAllByCropID(int cropID)
        {
            return repository.GetAllByCropID(cropID);
        }

        public CropVarietyMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(CropVarietyMaster cropvariety)
        {
            //HashPassword(cropvariety);
            return repository.Create(cropvariety);
        }

        public DataTable GetvarietySV(int CropID)
        {
            return repository.GetvarietySV(CropID).ToDataTable();
        }
        public DataTable GetAllVariety(int CropID)
        {
            return repository.GetAllVariety(CropID).ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(CropVarietyMaster cropvariety)
        {
            return repository.Update(cropvariety);
        }

    }
}
