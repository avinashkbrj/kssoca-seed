﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class DecodingService
    {
        DecodingRepository repository;
        public DecodingService()
        { repository = new DecodingRepository(); }
        public Decoding GetById(int id)
        {
            return repository.GetBy(id);
        }
        public Decoding GetBySSCID(int SSCID)
        {
            return repository.GetBySSCID(SSCID);
        }
        public int Create(Decoding entity)
        { 
            return repository.Create(entity);
        }
        public DataTable ReadByUser(int RoleID,int UserID,string TestType)
        {
            return repository.ReadByUser(RoleID, UserID, TestType).ToDataTable();
        }
        public DataTable ReadByUserWithoutPayment(int RoleID,int UserID,string TestType)
        {
            return repository.ReadByUserWithoutPayment(RoleID, UserID, TestType).ToDataTable();
        }
        public bool IsSSCIdExists(int SSC_Id)
        {
            return repository.IsSSCIdExists(SSC_Id);
        }
    }
}
