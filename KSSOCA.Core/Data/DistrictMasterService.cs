﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;

namespace KSSOCA.Core.Data
{
    public class DistrictMasterService
    {
        DistrictMasterRepository repository;

        public DistrictMasterService()
        {
            repository = new DistrictMasterRepository();
        }

        public List<DistrictMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<DistrictMaster> GetDistrictNotinUserMaster(int userID)
        {
            return  repository.GetDistrictNotinUserMaster(userID);
        }

        public DistrictMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(DistrictMaster district)
        {
            //HashPassword(cropvariety);
            return repository.Create(district);
        }

        public DataTable GetAllVariety()
        {
            return repository.GetAllDistrict().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(DistrictMaster district)
        {
            return repository.Update(district);
        }
    }
}
