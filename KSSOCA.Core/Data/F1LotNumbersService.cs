﻿namespace KSSOCA.Core.Data
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;

    public class F1LotNumbersService
    {
        F1LotNumbersRepository repository;
        public List<F1LotNumbers> GetByF1Id(int F1ID)
        {
            return repository.GetByF1Id(F1ID);
        }
        public string LotNumbers(int F1ID)
        {
            string lotNumbers = "";
            var Lots = repository.GetByF1Id(F1ID);
            for (int i = 0; i < Lots.Count; i++)
            {
                lotNumbers += Lots[i].LotNumber+" , ";
            }
           return lotNumbers = lotNumbers.Remove(lotNumbers.Length - 2);
        }
        public F1LotNumbersService()
        {
            repository = new F1LotNumbersRepository();
        }
        public int DeleteByFormIdLotNo(int F1ID, string LotNumber)
        {
            return repository.DeleteByFormIdLotNo(F1ID, LotNumber);
        }
        public List<int> Create(List<F1LotNumbers> entities)
        {
            return repository.Create(entities);
        }

        public string ConvertToJson(List<F1LotNumbers> entities)
        {
            return Serializer.JsonSerialize(entities);
        }
        public List<int> Update(List<F1LotNumbers> SVLotNumbers, int Form1ID)
        {
            return repository.Update(SVLotNumbers, Form1ID);
        }
    }
}
