﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class FarmerDetailsService
    {
        FarmerDetailsRepository repository;

        public FarmerDetailsService()
        {
            repository = new FarmerDetailsRepository();
        }

        public List<FarmerDetails> GetAll()
        {
            return repository.GetAll();
        }
        public FarmerDetails GetById(int id)
        {
            return repository.Get(id);
        }
        public List<FarmerDetails> GetByProducerID(int ProducerID)
        {
            return repository.GetByProducerID(ProducerID);
        }
        public int Update(FarmerDetails entity)
        {
            return repository.Update(entity);
        }
        public int Create(FarmerDetails entity)
        {
            return repository.Create(entity);
        }

        public int Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}
