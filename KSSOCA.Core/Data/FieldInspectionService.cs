﻿namespace KSSOCA.Core.Data
{
    using System;
    using System.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using Exceptions;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    public class FieldInspectionService
    {
        FieldInspectionRepository repository;
        
        public FieldInspectionService()
        {
            //userMasterService = new UserMasterService();
            //cropMasterService = new CropMasterService();
            //bankMasterService = new BankMasterService();
            //formOneService = new FormOneDetailsService();
            //talukMasterService = new TalukMasterService();
            //seasonMasterService = new SeasonMasterService();
            //districtMasterService = new DistrictMasterService();
            //classSeedMasterService = new ClassSeedMasterService();
            //cropVarietyMasterService = new CropVarietyMasterService();
            //villageMasterService = new VillageMasterService();
            //HobliMasterService = new TalukHobliMasterService();
            //sourceverficationservice = new SourceVerificationService();
            //registrationFormService = new RegistrationFormService();
            //fieldInspectionService = new FieldInspectionService();
            //fieldInspectionSuggestionMasterService = new FieldInspectionSuggestionMasterService();
            //farmerDetailsService = new FarmerDetailsService();
            //f1LotNumbersService = new F1LotNumbersService();

            repository = new FieldInspectionRepository();
        }

        public DataTable ReadAll()
        {
            return repository.GetAll().ToDataTable();
        }

        public FieldInspection GetLastInspection(int Form1ID)
        {
            return repository.GetLastInspection(Form1ID);
        }
        public FieldInspection GetFinalInspection(int Form1ID)
        {
            return repository.GetFinalInspection(Form1ID);
        }
        public FieldInspection ReadById(int id)
        {
            return repository.Read(id);
        }

        public DataTable ReadByForm1ID(int Form1ID)
        {
            return repository.ReadByForm1Id(Form1ID).ToDataTable();
        }
        public int Update(FieldInspection entity)
        {
            return repository.Update(entity);
        }
        public int Create(FieldInspection FieldInspection)
        {
            return repository.Create(FieldInspection);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }


       
    }
}
