﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

   public class FieldInspectionSuggestionMasterService
    {
        FieldInspectionSuggestionMasterRepository repository;

        public FieldInspectionSuggestionMasterService()
        {
            repository = new FieldInspectionSuggestionMasterRepository();
        }

        public List<FieldInspectionSuggestionMaster> GetAll()
        {
            return repository.GetAll();
        }
        public FieldInspectionSuggestionMaster GetById(int id)
        {
            return repository.Get(id);
        }
        public List<FieldInspectionSuggestionMaster> GetSuggestions(string suggestionIDs)
        {
            return repository.GetSuggestions(suggestionIDs);
        }
    }
}
