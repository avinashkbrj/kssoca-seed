﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Core.Data
{    

    public class Form1Data
    {
        public string Form1ID { get; set; }
        public string InspectionSerialNo { get; set; }
        public string LastInspectionDetails { get; set; }
        public string ProducerName { get; set; }
        public string ProducerRegNo { get; set; }
        public string FarmerName { get; set; }
        public string FatherName { get; set; }
        public string FarmerAddress { get; set; }
        public string LocationOfSeedPlot { get; set; }
        public string SurveyNo { get; set; }
        public string Season { get; set; }
        public string ClassOfSeed { get; set; }
        public string Crop { get; set; }
        public string Variety { get; set; }
        public string LotNo { get; set; }
        public string DateOfSowingAsForm1 { get; set; }
        public string AreaRegistered { get; set; }
    }
}
