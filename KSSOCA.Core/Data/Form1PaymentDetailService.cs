﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class Form1PaymentDetailService
    {
        Form1PaymentDetailRepository repository;

        public Form1PaymentDetailService()
        {
            repository = new Form1PaymentDetailRepository();
        }

        public List<Form1PaymentDetails> GetAll()
        {
            return repository.GetAll();
        }

        public Form1PaymentDetails GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(Form1PaymentDetails crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public Form1PaymentDetails GetBy(int Form1ID)
        {
            return repository.GetBy(Form1ID);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(Form1PaymentDetails crop)
        {
            return repository.Update(crop);
        }
    }
}
