﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class Form2DetailsService
    {
        Form2DetailsRepository repository;

        public Form2DetailsService()
        {
            repository = new Form2DetailsRepository();
        }

        public List<Form2Details> GetAll()
        {
            return repository.GetAll();
        }

        public Form2Details GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(Form2Details form2)
        {
            // HashPassword(crop);
            return repository.Create(form2);
        }

        public Form2Details GetBy(int Form1Id)
        {
            return repository.GetBy(Form1Id);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(Form2Details crop)
        {
            return repository.Update(crop);
        }
        public DataTable getf2details(int userid,int roleid)
        {
           return repository.GetF2Details(userid, roleid).ToDataTable();
        }
    }
}
