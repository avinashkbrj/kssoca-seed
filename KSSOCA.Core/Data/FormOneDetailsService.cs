﻿
namespace KSSOCA.Core.Data
{
    using System;
    using System.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Data;
    using KSSOCA.Model;

    public class FormOneDetailsService
    {
        Form1DetailsRepository repository;

        public FormOneDetailsService()
        {
            repository = new Form1DetailsRepository();
        }

        public DataTable ReadAll()
        {
            return repository.GetAll().ToDataTable();
        }

        public Form1Details ReadById(int id)
        {
            return repository.Read(id);
        }

        public Form1Details ReadByStatus(int id)
        {
            return repository.ReadByStatus(id);
        }

        public int Update(Form1Details entity)
        {
            return repository.Update(entity);
        }

        public DataTable ReadAllByUserAuthority(int userId)
        {
            return repository.ReadAllByUserAuthority(userId).ToDataTable();
        }
        public DataTable ReadBySourceVerificationId(int SourceVerID)
        {
            return repository.ReadBySourceVerificationId(SourceVerID).ToDataTable();
        }

        public int Create(Form1Details form1details)
        {
            return repository.Create(form1details);
        }

        public int Delete(int id)
        {
            return repository.Delete(id);
        }
    }
}