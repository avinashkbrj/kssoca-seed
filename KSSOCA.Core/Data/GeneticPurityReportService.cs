﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class GeneticPurityReportService
    {
        GeneticPurityReportRepository repository;

        public GeneticPurityReportService()
        {
            repository = new GeneticPurityReportRepository();
        }

        public List<GeneticPurityReport> GetAll()
        {
            return repository.GetAll();
        }

        public GeneticPurityReport GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(GeneticPurityReport crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public GeneticPurityReport GetByLabTestNo(int LabTestNo)
        {
            return repository.GetByLabTestNo(LabTestNo);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(GeneticPurityReport crop)
        {
            return repository.Update(crop);
        }
    }
}
