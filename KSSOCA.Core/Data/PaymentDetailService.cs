﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class PaymentDetailService
    {
        PaymentDetailRepository repository;

        public PaymentDetailService()
        {
            repository = new PaymentDetailRepository();
        }

        public List<PaymentDetails> GetAll()
        {
            return repository.GetAll();
        }

        public PaymentDetails GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(PaymentDetails crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public PaymentDetails GetBy(int UserID)
        {
            return repository.GetBy(UserID);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(PaymentDetails crop)
        {
            return repository.Update(crop);
        }
    }
}
