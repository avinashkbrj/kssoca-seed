﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{
   public  class ProcessingPaymentDetailsService
    {
        ProcessingPaymentDetailsRepository repository;

        public ProcessingPaymentDetailsService()
        {
            repository = new ProcessingPaymentDetailsRepository();
        }

        public List<ProcessingPaymentDetails> GetAll()
        {
            return repository.GetAll();
        }

        public ProcessingPaymentDetails GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(ProcessingPaymentDetails entity)
        {
            // HashPassword(crop);
            return repository.Create(entity);
        }

        public ProcessingPaymentDetails GetBy(int Form1ID)
        {
            return repository.GetBy(Form1ID);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(ProcessingPaymentDetails entity)
        {
            return repository.Update(entity);
        }
    }
}
