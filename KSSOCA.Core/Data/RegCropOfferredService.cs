﻿
namespace KSSOCA.Core.Data
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;

    public class RegCropOfferredService
    {
        RegCropOfferredRepository repository;

        public RegCropOfferredService()
        {
            repository = new RegCropOfferredRepository();
        }

        public List<RegCropOffered> GetByRegistrationId(int id)
        {
            return repository.GetByRegistrationId(id);
        }

        public List<int> Create(List<RegCropOffered> entities)
        {
            return repository.Create(entities);
        }

        public List<int> Update(List<RegCropOffered> regCropOfferred, int registrationFormId)
        {
            return repository.Update(regCropOfferred, registrationFormId);
        }

        public string ConvertToJson(List<RegCropOffered> entities)
        {
            return Serializer.JsonSerialize(entities);
        }
    }
}
