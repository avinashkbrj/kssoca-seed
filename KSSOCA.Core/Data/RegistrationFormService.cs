﻿
namespace KSSOCA.Core.Data
{
    using System;
    using System.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Data;
    using KSSOCA.Model;

    public class RegistrationFormService
    {
        RegistrationFormRepository repository;


        public RegistrationFormService()
        {
            repository = new RegistrationFormRepository();
        }

        public RegistrationForm GetById(int id)
        {
            return repository.GetById(id);
        }

        public RegistrationForm GetByUserId(int id)
        {
            return repository.GetByUserId(id);
        }

        //public DataTable ReadAll()
        //{
        //    return repository.ReadAll().ToDataTable();
        //}
        public DataTable ReadAllByUserAuthority(int userid)
        {
            return repository.ReadAllByUserAuthority(userid).ToDataTable();
        }

        public int Create(RegistrationForm entity)
        {
            return repository.Create(entity);
        }

        public int Update(RegistrationForm entity)
        {
            return repository.Update(entity);   
        }
        public string GenerateRegistrationNumber()
        {
            // Ex: 008/2017-2018
            string financialYear = DateTime.Now.ToFinancialYear();
            string lastRegNumber = repository.GetLastRegisterNumber(financialYear);

            int startIndex = lastRegNumber.IndexOf('/');
            lastRegNumber = lastRegNumber.Remove(startIndex);
            int lastNumber = int.Parse(lastRegNumber);

            return string.Format("{0:D4}/{1}", lastNumber + 1, financialYear);
        }
        public DataTable GetProducerListBy(string Type, string RegistrationNo, Nullable<int> DivisionId)
        {
            return repository.GetProducerBy(Type, RegistrationNo, DivisionId).ToDataTable();
        }
        public int CheckForRegistration(UserMaster um)
        {
            return repository.CheckForRegistration(um);
        }
    }
}
