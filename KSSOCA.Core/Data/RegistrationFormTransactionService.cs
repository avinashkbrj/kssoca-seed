﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class RegistrationFormTransactionService
    {
        RegistrationFormTransactionRepository repository;

        public RegistrationFormTransactionService()
        {
            repository = new RegistrationFormTransactionRepository();
        }

        public List<RegistrationFormTransaction> GetAll()
        {
            return repository.GetAll();
        }

        public RegistrationFormTransaction GetById(int id)
        {
            return repository.Get(id);
        }
        public RegistrationFormTransaction GetMaxTransactionDetails(int RegFormID)
        {
            return repository.GetMaxTransactionDetails(RegFormID);
        }
        public List<RegistrationFormTransaction> GetByRegistrationFormId(int id)
        {
            return repository.GetByRegistrationFormID(id);
        }

        public int Create(RegistrationFormTransaction crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }


        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(RegistrationFormTransaction crop)
        {
            return repository.Update(crop);
        }
        public DataTable GetAllTransactionsBy(int FormRegistrationID)
        {
            return repository.GetAllTransactions(FormRegistrationID).ToDataTable();
        }
    }
}
