﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class RoleMasterService
    {
        RoleMasterRepository repository;

        public RoleMasterService()
        {
            repository = new RoleMasterRepository();
        }

        public List<RoleMaster> GetAll()
        {
            return repository.GetAll();
        }

        public RoleMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(RoleMaster roleMaster)
        {
            //HashPassword(cropvariety);
            return repository.Create(roleMaster);
        }

        public DataTable GetAllRole()
        {
            return repository.GetAllRole().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(RoleMaster roleMaster)
        {
            return repository.Update(roleMaster);
        }
    }
}
