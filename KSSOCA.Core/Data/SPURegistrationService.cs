﻿

namespace KSSOCA.Core.Data
{
    using System;
    using System.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System.Collections.Generic;

    public class SPURegistrationService
    {
        SPURegistrationRepository repository;
        public SPURegistrationService()
        {
            repository = new SPURegistrationRepository();
        }
        public List<SPURegistration> GetAll( )
        {
            return repository.GetAll();
        }
        public SPURegistration GetById(int id)
        {
            return repository.GetById(id);
        }
        public SPURegistration GetByUserId(int SPUId)
        {
            return repository.GetByUserId(SPUId);
        }
        public int Create(SPURegistration entity)
        {
            return repository.Create(entity);
        }
        public int Update(SPURegistration entity)
        {
            return repository.Update(entity);
        }
        public DataTable ReadAllByUserAuthority(int userid)
        {
            return repository.ReadAllByUserAuthority(userid).ToDataTable();
        }
        public string GenerateRegistrationNumber()
        {
            // Ex: 008/2017-2018
            string financialYear = DateTime.Now.ToFinancialYear();
            string lastRegNumber = repository.GetLastRegisterNumber(financialYear);

            int startIndex = lastRegNumber.IndexOf('/');
            lastRegNumber = lastRegNumber.Remove(startIndex);
            int lastNumber = int.Parse(lastRegNumber);

            return string.Format("{0:D4}/{1}", lastNumber + 1, financialYear);
        }
        public DataTable GetSPUListBy(string Type, string RegistrationNo, Nullable<int> DivisionId)
        {
            return repository.GetSPUListBy(Type, RegistrationNo, DivisionId).ToDataTable();
        }
    }
}
