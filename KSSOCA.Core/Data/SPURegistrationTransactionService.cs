﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{
  public  class SPURegistrationTransactionService
    {
        SPURegistrationTransactionRepository repository;

        public SPURegistrationTransactionService()
        {
            repository = new SPURegistrationTransactionRepository();
        }

        public List<SPURegistrationTransaction> GetAll()
        {
            return repository.GetAll();
        }

        public SPURegistrationTransaction GetById(int id)
        {
            return repository.Get(id);
        }
        public SPURegistrationTransaction GetMaxTransactionDetails(int RegFormID)
        {
            return repository.GetMaxTransactionDetails(RegFormID);
        }
        public List<SPURegistrationTransaction> GetByRegistrationFormId(int id)
        {
            return repository.GetBySPURegistrationID(id);
        }

        public int Create(SPURegistrationTransaction crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public DataTable GetAllTransactionsBy(int SPURegistrationID)
        {
            return repository.GetAllTransactions(SPURegistrationID).ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SPURegistrationTransaction crop)
        {
            return repository.Update(crop);
        }
    }
}

