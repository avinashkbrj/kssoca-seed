﻿namespace KSSOCA.Core.Data
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;

    public class SVLotNumbersService
    {
        SVLotNumbersRepository repository;
        public decimal QuantityLeft(int SVID, string LotNo)
        {
            return repository.QuantityLeft(SVID, LotNo);
        }
        public List<SVLotNumbers> GetBySVId(int SVId)
        {
            return repository.GetBySVId(SVId);
        }
        public SVLotNumbers GetById(int Id)
        {
            return repository.GetById(Id);
        }
        public SVLotNumbersService()
        {
            repository = new SVLotNumbersRepository();
        }
        public List<int> Create(List<SVLotNumbers> entities)
        {
            return repository.Create(entities);
        }

        public int SingleObjCreate(SVLotNumbers sVLotNumbers)
        {
            return repository.SingleObjCreate(sVLotNumbers);
        }
        public string ConvertToJson(List<SVLotNumbers> entities)
        {
            return Serializer.JsonSerialize(entities);
        }
        public List<int> Update(List<SVLotNumbers> SVLotNumbers, int SVID)
        {
            return repository.Update(SVLotNumbers, SVID);
        }
        public List<SVLotNumbers> GetByLotNo(string LotNumber)
        {
            return repository.GetByLotNo(LotNumber);
        }
        public List<SVLotNumbers> GetByLotByID(int LotNumber)
        {
            return repository.GetByLotNoById(LotNumber);
        }
    }
}
