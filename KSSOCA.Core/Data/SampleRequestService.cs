﻿namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;
    class SampleRequestService
    {
        SampleRequestRepository repository;

        public SampleRequestService()
        {
            repository = new SampleRequestRepository();
        }

        public SampleRequest GetById(int id)
        {
            return repository.Get(id);
        }
        public List<SampleRequest> GetbyForm1ID(int Form1ID)
        {
            return repository.GetbyForm1ID(Form1ID);
        }
        public int Create(SampleRequest entity)
        {
            return repository.Create(entity);
        }
        public int Update(SampleRequest entity)
        {
            return repository.Update(entity);
        }
        public int GenerateID()
        {
            int lastID = repository.GetLastID();
            return lastID + 1;
        }
    }
}
