﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

   public class SampleRequestTransactionService
    {
        SampleRequestTransactionRepository repository;

        public SampleRequestTransactionService()
        {
            repository = new SampleRequestTransactionRepository();
        }

        public SampleRequestTransaction GetMaxTransactionDetails(int Form1ID)
        {
            return repository.GetMaxTransactionDetails(Form1ID);
        }

        public int Create(SampleRequestTransaction entity)
        { 
            return repository.Create(entity);
        }

        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SampleRequestTransaction entity)
        {
            return repository.Update(entity);
        }
        public DataTable GetAllTransactionsBy(int Form1ID)
        {
            return repository.GetAllTransactions(Form1ID).ToDataTable();
        }
    }
}
