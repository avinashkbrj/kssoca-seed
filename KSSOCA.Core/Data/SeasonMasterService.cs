﻿
namespace KSSOCA.Core.Data
{
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Helper;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using KSSOCA.Core.Extensions;
    using System.Data;

    public class SeasonMasterService
    {
        SeasonMasterRepository repository;

        public SeasonMasterService()
        {
            repository = new SeasonMasterRepository();
        }

        public List<SeasonMaster> GetAll()
        {
            return repository.GetAll();
        }

        public SeasonMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(SeasonMaster season)
        {
            //HashPassword(cropvariety);
            return repository.Create(season);
        }

        public DataTable GetAllSeason()
        {
            return repository.GetAllSeason().ToDataTable();
        }
        public string GetSeasonsBy(string SeasonIds)
        {
            string TestsRequired = "";
            List<SeasonMaster> tests = repository.GetSeasonsBy(SeasonIds);
            for (int i = 0; i < tests.Count; i++)
            {
                TestsRequired += tests[i].Name + ",";
            }
            return TestsRequired.Remove(TestsRequired.Length - 1);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SeasonMaster season)
        {
            return repository.Update(season);
        }
    }
}
