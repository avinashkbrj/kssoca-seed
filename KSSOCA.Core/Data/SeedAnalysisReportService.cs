﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class SeedAnalysisReportService
    {
        SeedAnalysisReportRepository repository;

        public SeedAnalysisReportService()
        {
            repository = new SeedAnalysisReportRepository();
        }

        public List<SeedAnalysisReport> GetAll()
        {
            return repository.GetAll();
        }
       

        public SeedAnalysisReport GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(SeedAnalysisReport crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        //public SeedAnalysisReport GetBy(int Form1Id)
        //{
        //    return repository.GetBy(Form1Id);
        //}
        public SeedAnalysisReport GetByLabTestNo(int LabNo)
        {
            return repository.GetByLabTestNo(LabNo);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SeedAnalysisReport crop)
        {
            return repository.Update(crop);
        }
        public DataTable GetAllTransactionsBy(int Form1ID)
        {
            return repository.GetAllTransactions(Form1ID).ToDataTable();
        }
    }
}
