﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class SeedSampleCouponService
    {
        SeedSampleCouponRepository repository;

        public SeedSampleCouponService()
        {
            repository = new SeedSampleCouponRepository();
        }

        public List<SeedSampleCoupon> GetAll()
        {
            return repository.GetAll();
        }

        public SeedSampleCoupon GetById(int id)
        {
            return repository.Get(id);
        }

        public int Create(SeedSampleCoupon crop)
        {
            // HashPassword(crop);
            return repository.Create(crop);
        }

        public List<SeedSampleCoupon> GetBy(int Form1Id)
        {
            return repository.GetBy(Form1Id);
        }
        
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SeedSampleCoupon crop)
        {
            return repository.Update(crop);
        }
        public DataTable ReadByForm1ID(int Form1ID)
        {
            return repository.ReadByForm1Id(Form1ID).ToDataTable();
        }
        public bool IsValidCoupan(int SSC_Id,string TestType)
        {
            return repository.IsSSCIdExists(SSC_Id, TestType);
        }
    }
}
