﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;

namespace KSSOCA.Core.Data
{
    public class SeedTransferTransactionService
    {
        SeedTransferTransactionRepository repository;
        public int Create(SeedTransferTransaction entity)
        {
            // HashPassword(crop);
            return repository.Create(entity);
        }

        public DataTable GetAllTransactionsBy(int SourceVerificationID)
        {
            return repository.GetAllTransactions(SourceVerificationID).ToDataTable();
        }
       

    }
}
