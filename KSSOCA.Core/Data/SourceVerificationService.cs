﻿
namespace KSSOCA.Core.Data
{
    using System.Data;
    using KSSOCA.Data;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using System;

    public class SourceVerificationService
    {
        SourceVerificationRepository repository;

        public SourceVerificationService()
        {
            repository = new SourceVerificationRepository();
        }

        public SourceVerification ReadById(int id)
        {
            return repository.Read(id);
        }
        public SourceVerification GetById(int id)
        {
            return repository.GetId(id);
        }

        public DataTable GetSourceVerifications(int roleId, int userId,string Type)
        {
            return repository.GetSourceVerifications(roleId,userId, Type).ToDataTable();
        }

        public int Create(SourceVerification sourceVerification)
        {
            return repository.Create(sourceVerification);
        }

        public int Update(SourceVerification sourceVerification)
        {
            return repository.Update(sourceVerification);
        }

        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        
    }
}
