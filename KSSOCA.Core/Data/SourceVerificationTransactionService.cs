﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{

    public class SourceVerificationTransactionService
    {
        SourceVerificationTransactionRepository repository;

        public SourceVerificationTransactionService()
        {
            repository = new SourceVerificationTransactionRepository();
        }

        public List<SourceVerificationTransaction> GetAll()
        {
            return repository.GetAll();
        }

        public SourceVerificationTransaction GetById(int id)
        {
            return repository.Get(id);
        }
        public SourceVerificationTransaction GetMaxTransactionDetails(int SVID)
        {
            return repository.GetMaxTransactionDetails(SVID);
        }
        public List<SourceVerificationTransaction> GetBySourceVerificationId(int id)
        {
            return repository.GetByRegistrationFormID(id);
        }

        public int Create(SourceVerificationTransaction entity)
        {
            // HashPassword(crop);
            return repository.Create(entity);
        }

        public DataTable GetAllCrop()
        {
            return repository.GetAllCrop().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(SourceVerificationTransaction entity)
        {
            return repository.Update(entity);
        }
        public DataTable GetAllTransactionsBy(int SourceVerificationID)
        {
            return repository.GetAllTransactions(SourceVerificationID).ToDataTable();
        }
    }
}
