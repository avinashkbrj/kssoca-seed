﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;

namespace KSSOCA.Core.Data
{
    public class TalukHobliMasterService
    {
        TalukHobliMasterRepositary repository;

        public TalukHobliMasterService()
        {
            repository = new TalukHobliMasterRepositary();
        }

        public List<TalukHobliMaster> GetAllBy(int districtcode, int talukcode)
        {
            return repository.GetAllBy(districtcode, talukcode);
        }

        public TalukHobliMaster GetById(int id)
        {
            return repository.Get(id);
        }
        public TalukHobliMaster GetBy(int DistrictCode, int talukCode, int HobliCode)
        {
            return (TalukHobliMaster)repository.GetBy(DistrictCode, talukCode, HobliCode);
        }

        public int Register(TalukHobliMaster hobli)
        {
            //HashPassword(cropvariety);
            return repository.Create(hobli);
        }

        public DataTable GetAllHobali(int districtcode, int talukcode)
        {
            return repository.GetAllHobali(districtcode, talukcode).ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(TalukHobliMaster hobli)
        {
            return repository.Update(hobli);
        }
    }
}
