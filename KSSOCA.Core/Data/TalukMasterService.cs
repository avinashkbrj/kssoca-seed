﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;

namespace KSSOCA.Core.Data
{
    public class TalukMasterService
    {
        TalukMasterRepository repository;

        public TalukMasterService()
        {
            repository = new TalukMasterRepository();
        }

        public List<TalukMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<TalukMaster> GetAllbyDistID(int districtcode)
        {
            return repository.GetAllbyDistID(districtcode);
        }

        public TalukMaster GetById(int id)
        {
            return repository.Get(id);
        }
        public TalukMaster GetBy(int DistrictCode, int talukCode)
        {
            return (TalukMaster)repository.GetBy(DistrictCode, talukCode);
        }
        public int Register(TalukMaster taluk)
        {
            //HashPassword(cropvariety);
            return repository.Create(taluk);
        }

        public DataTable GetAllTaluk()
        {
            return repository.GetAllTaluk().ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(TalukMaster taluk)
        {
            return repository.Update(taluk);
        }
        public List<TalukMaster> GetTalukNotinUserMaster(int userID,int DistrictID)
        {
            return repository.GetTalukNotinUserMaster(userID, DistrictID);
        }
    }
}
