﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{
    public class TestMasterService
    {
        TestMasterRepository repository;

        public TestMasterService()
        {
            repository = new TestMasterRepository();
        }
        public List<TestMaster> GetAll()
        {
            return repository.GetAll();
        }
        public List<TestMaster> GetTestsByType(string TestType)
        {
            return repository.GetTestsByType(TestType);
        }
        public int Create(TestMaster entity)
        {
            return repository.Create(entity);
        }
        public string TestsRequired(string TestIds)
        {
            string TestsRequired = "";
            List<TestMaster> tests = repository.GetTestsBy(TestIds);
            for (int i = 0; i < tests.Count; i++)
            {
                TestsRequired += tests[i].Name + ",";
            }
            return TestsRequired.Remove(TestsRequired.Length - 1);
        }
        public List<TestMaster> GetTestsBy(string TestIds)
        {
            return repository.GetTestsBy(TestIds);
        }
    }
}
