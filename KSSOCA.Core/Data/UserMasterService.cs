﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;


namespace KSSOCA.Core.Data
{
    public class UserMasterService
    {
        UserMasterRepository repository;
        Common common = new Common();

        public UserMasterService()
        {
            repository = new UserMasterRepository();
        }

        public UserMaster GetById(int id)
        {
            return repository.Get(id);
        }
        public UserMaster GetbyUserName(string UserName)
        {
            return repository.GetbyUserName(UserName);
        }

        public int Register(UserMaster user)
        {
            HashPassword(user);
            return repository.Create(user);
        }

        //public bool ValidateLogin(string email, string password, bool isPersistent)
        //{
        //    bool validUser = false;
        //    var user = repository.Get(email);

        //    if (user == null)
        //    {
        //       return validUser;
        //    }
        //    if (!(user.IsActive.HasValue && user.IsActive.Value))
        //        return validUser;

        //    validUser = Encryption.Verify(password, user.Password);

        //    if (validUser)
        //    {
        //        CreateFormsAuthentication(user, isPersistent);
        //        Update(new UserMaster() { Id = user.Id, LoginAttempt = 0 });
        //    }
        //    else
        //    {
        //        user.LoginAttempt =+ 
        //            (user.LoginAttempt.HasValue ? user.LoginAttempt.Value + 1 : 1);

        //        if (user.LoginAttempt >= 5)
        //        {
        //            user.IsActive = false;
        //            user.LoginAttempt = 0;
        //        }

        //        Update(new UserMaster() { Id = user.Id, IsActive = user.IsActive, LoginAttempt = user.LoginAttempt = 0 });
        //    }

        //    return validUser;
        //}

        public bool ValidateLogin(string UserName, string password, bool isPersistent)
        {
            bool validUser = false;
            var user = repository.GetbyUserName(UserName);

            if (user == null)
            {
                
               // common.ExecuteNonQuery("insert into LoginDetails values('" + UserName + "','" + System.DateTime.Now + "','" + common.GetIPAddress() + "'," + 0 + " )");
                return validUser;
            }
            if (!(user.IsActive.HasValue && user.IsActive.Value))
            {
               // common.ExecuteNonQuery("insert into LoginDetails values('" + UserName + "','" + System.DateTime.Now + "','" + common.GetIPAddress() + "'," + 0 + " )");

                return validUser=true;
            }

            validUser = Encryption.Verify(password, user.Password);

            if (validUser)
            {
                CreateFormsAuthentication(user, isPersistent);
                Update(new UserMaster() { Id = user.Id, LoginAttempt = 0 });
               // common.ExecuteNonQuery("insert into LoginDetails values('" + UserName + "','" + System.DateTime.Now + "','" + common.GetIPAddress() + "'," + 1 + " )");

            }
            else
            {
                user.LoginAttempt = +
                    (user.LoginAttempt.HasValue ? user.LoginAttempt.Value + 1 : 1);

                if (user.LoginAttempt >= 5)
                {
                    user.IsActive = false;
                    user.LoginAttempt = 0;
                }

                Update(new UserMaster() { Id = user.Id, IsActive = user.IsActive, LoginAttempt = user.LoginAttempt = 0 });
            }
            
                       return validUser;
        }

        public UserMaster GetFromAuthCookie()
        {
            UserMaster userMaster = null;
           
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    string userData = authTicket.UserData;
                    userMaster = Serializer.JsonDeSerialize<UserMaster>(userData);
                }
            }
            return userMaster;
        }

        public bool IsEmailIdExist(string email)
        {
            return repository.IsEmailIdExist(email);
        }
        public bool IsUserNameExist(string UserName)
        {
            return repository.IsUserNameExist(UserName);
        }
        public bool IsMobileNumberExist(string MobileNumber)
        {
            return repository.IsMobileNumberExist(MobileNumber);
        }

        public string GenerateRegistrationNumber()
        {
            // Ex: 008/2017-2018
            string financialYear = DateTime.Now.ToFinancialYear();
            string lastRegNumber = repository.GetLastRegisterNumber();

            int startIndex = lastRegNumber.IndexOf('/');
            lastRegNumber = lastRegNumber.Remove(startIndex);
            int lastNumber = int.Parse(lastRegNumber);

            return string.Format("{0:D4}/{1}", lastNumber + 1, financialYear);
        }

        public DataTable GetAllUser()
        {
            return repository.GetAll().ToDataTable();
        }
        public DataTable GetSPUByReportingManager(int SCO_ID)
        {
            return repository.GetSPUByReportingManager(SCO_ID).ToDataTable();
        }
        

        public List<UserMaster> GetUserList(int Role_Id)
        {
            return repository.GetUserList(Role_Id);
        }

        public List<UserMaster> GetByReportingOfficer(int Reporting_Id, int Role_Id)
        {
            return repository.GetByReportingOfficer(Reporting_Id, Role_Id);
        }

        public List<UserMaster> GetReportingManager()
        {
            return repository.GetReportingManager()
                .Select(s => new UserMaster()
                {
                    Id = s.Id,
                    Name = s.Name
                }).ToList();
        }

        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(UserMaster user)
        {
            if (!string.IsNullOrEmpty(user.Password))
                HashPassword(user);

            return repository.Update(user);
        }

        private void CreateFormsAuthentication(UserMaster user, bool isPersistent)
        {
            user.Password = string.Empty;
            string serializeObject = Serializer.JsonSerialize(user);

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                1, user.EmailId, DateTime.Now, DateTime.Now.AddMonths(1), isPersistent
                , serializeObject, FormsAuthentication.FormsCookiePath);

            string hashed_ticket = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashed_ticket);

            if (ticket.IsPersistent)
                cookie.Expires = ticket.Expiration;

            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        private void HashPassword(UserMaster user)
        {
            user.Password = Encryption.Encrypt(user.Password);
        }
        public List<UserMaster> GetUserList()
        {
            return repository.GetUserList(5);
        }
    }
}