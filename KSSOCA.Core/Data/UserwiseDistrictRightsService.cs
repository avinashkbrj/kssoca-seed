﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;
namespace KSSOCA.Core.Data
{
    public class UserwiseDistrictRightsService
    {
        UserwiseDistrictRightsRepository repository;

        public UserwiseDistrictRightsService()
        {
            repository = new UserwiseDistrictRightsRepository();
        }

        public List<UserwiseDistrictRights> GetAll()
        {
            return repository.GetAll();
        }

        public UserwiseDistrictRights GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(UserwiseDistrictRights userwiseDistrictRights)
        {
            //HashPassword(cropvariety);
            return repository.Create(userwiseDistrictRights);
        }

        public DataTable GetUsers(int userID, int DistrictID)
        {
            return repository.GetUsers(userID, DistrictID).ToDataTable();
        }
        public UserwiseDistrictRights GetAllBy(int District_Id, int Taluk_Id, int UserRole_Id)
        {
            return repository.GetAllBy(District_Id, Taluk_Id, UserRole_Id);
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(UserwiseDistrictRights userwiseDistrictRights)
        {
            return repository.Update(userwiseDistrictRights);
        }

    }
}
