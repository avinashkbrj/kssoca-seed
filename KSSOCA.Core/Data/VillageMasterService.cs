﻿using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
using KSSOCA.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using KSSOCA.Core.Extensions;
using System.Data;

namespace KSSOCA.Core.Data
{
    public class VillageMasterService
    {
        VillageMasterRepositary repository;

        public VillageMasterService()
        {
            repository = new VillageMasterRepositary();
        }

        public List<VillageMaster> GetAllBy(int districtcode, int talukcode, int hobliCode)
        {
            return repository.GetAllBy(districtcode, talukcode, hobliCode);
        }
        public VillageMaster GetBy(int districtcode, int talukcode, int hobliCode, int villegeCode)
        {
            return (VillageMaster)repository.GetBy(districtcode, talukcode, hobliCode, villegeCode);
        }

        public VillageMaster GetById(int id)
        {
            return repository.Get(id);
        }

        public int Register(VillageMaster viilage)
        {
            //HashPassword(cropvariety);
            return repository.Create(viilage);
        }

        public DataTable GetAllVillage(int districtcode, int talukcode, int hobliCode)
        {
            return repository.GetAllVillage(districtcode, talukcode, hobliCode).ToDataTable();
        }
        public int Delete(int id)
        {
            return repository.Delete(id);
        }

        public int Update(VillageMaster viilage)
        {
            return repository.Update(viilage);
        }
    }
}

