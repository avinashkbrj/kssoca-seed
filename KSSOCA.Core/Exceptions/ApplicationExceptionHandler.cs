﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using context = System.Web.HttpContext;

namespace KSSOCA.Core.Exceptions
{
    public class ApplicationExceptionHandler
    {
        private static String exepurl;
        static SqlConnection con;
        private static void connecttion()
        {
            string constr = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
            con = new SqlConnection(constr);
            con.Open();
        }
        public static void Handle(Exception exdb)
        {
            connecttion();
            exepurl = context.Current.Request.Url.ToString();
            SqlCommand com = new SqlCommand("ExceptionLoggingToDataBase", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@ExceptionMsg", exdb.Message.ToString());
            com.Parameters.AddWithValue("@ExceptionType", exdb.GetType().Name.ToString());
            com.Parameters.AddWithValue("@ExceptionURL", exepurl);
            com.Parameters.AddWithValue("@ExceptionSource", exdb.StackTrace.ToString());
            com.Parameters.AddWithValue("@Status", 0);
            com.ExecuteNonQuery();
           // KSSOCA.Core.Extensions.Messaging.SendSMS("New Exception arrived.", "8904101864", "1");
        }
    }
}
