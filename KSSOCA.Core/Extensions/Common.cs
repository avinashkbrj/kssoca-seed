﻿namespace KSSOCA.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Data.SqlClient;
    using System.Data;
    using System.Configuration;
    using System.Web.Script.Serialization;
    using Helper;
    using System.Web.UI.WebControls;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using System.Net;

    public class Common
    {
        public Common()
        {

        }
        public decimal getLabPaymentByForm1(int Form1ID)
        {
            string query = "SELECT isnull( sum([AmountPaid]),0)  FROM  [LabPaymentDetails] where Form1ID=" + Form1ID + "";
            return Convert.ToDecimal(ExecuteScalar(query));
        }
        public DataTable GetFinancialYears()
        {
            string query = "select distinct  FinancialYear  from  SourceVerification ";
            DataTable dt = GetData(query);
            return dt;
        }
        public string getCheckedItems(CheckBoxList list)
        {
            string CheckedItems = "";
            for (int counter = 0; counter < list.Items.Count; counter++)
            {
                if (list.Items[counter].Selected)
                {
                    CheckedItems += list.Items[counter].Value + ",";
                }
            }
            if (CheckedItems.Length > 1)
            {
                return CheckedItems.Remove(CheckedItems.Length - 1);
            }
            else return "";
        }
        public string GetIPAddress()
        {
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST  
            Console.WriteLine(hostName);
            // Get the IP  
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
            //System.Web.HttpContext context = System.Web.HttpContext.Current;
            //string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //if (!string.IsNullOrEmpty(ipAddress))
            //{
            //    string[] addresses = ipAddress.Split(',');
            //    if (addresses.Length != 0)
            //    {
            //        return addresses[0];
            //    }
            //}

            //return context.Request.ServerVariables["REMOTE_ADDR"];
        }
        public string DataTableToJSON(DataTable table)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
            Dictionary<string, object> childRow;
            foreach (DataRow row in table.Rows)
            {
                childRow = new Dictionary<string, object>();
                foreach (DataColumn col in table.Columns)
                {
                    childRow.Add(col.ColumnName, row[col]);
                }
                parentRow.Add(childRow);
            }
            return Serializer.JsonSerialize(parentRow);
        }

        public DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }
        public DataTable GetData(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;
            try
            {
                con.Open();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                con.Close();
                sda.Dispose();
                con.Dispose();
            }
        }
        public DataTable ExecuteStoredProcedure(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                 
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                 
                return dt;
            }
        }
        public int ExecuteNonQuery(string query)
        {
            String strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand(query, cnn);

            SqlTransaction trans = null;


            if (query.StartsWith("SELECT") | query.StartsWith("select") | query.StartsWith("INSERT") | query.StartsWith("insert") | query.StartsWith("UPDATE") | query.StartsWith("update") | query.StartsWith("DELETE") | query.StartsWith("delete"))
            {
                cmd.CommandType = CommandType.Text;
            }
            else
            {
                cmd.CommandType = CommandType.StoredProcedure;
            }
            int retval;
            try
            {
                cnn.Open();
                trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                retval = cmd.ExecuteNonQuery();
                trans.Commit();
            }
            catch (Exception exp)
            {
                trans.Rollback();

                throw exp;
            }
            finally
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
            return retval;
        }
        public object ExecuteScalar(string query)
        {
            object retval = null;
            String strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand(query, cnn);
            if (query.StartsWith("SELECT") | query.StartsWith("select") | query.StartsWith("INSERT") | query.StartsWith("insert") | query.StartsWith("UPDATE") | query.StartsWith("update") | query.StartsWith("DELETE") | query.StartsWith("delete"))
            {
                cmd.CommandType = CommandType.Text;
            }
            else
            {
                cmd.CommandType = CommandType.StoredProcedure;
            }
            try
            {
                cnn.Open();
                retval = cmd.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
            return retval;
        }
        public int CreateHistory(string TableName, int RecordID)
        {
            string query = "INSERT into " + TableName + "_History select [Id],[StatusCode],[ActionRole_Id],[ApprovedBy],[ApprovedDate],getdate() as HistoryDate from " + TableName + " where ID=" + RecordID + "";
            String strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            SqlConnection cnn = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand(query, cnn);
            SqlTransaction trans = null;
            if (query.StartsWith("SELECT") | query.StartsWith("select") | query.StartsWith("INSERT") | query.StartsWith("insert") | query.StartsWith("UPDATE") | query.StartsWith("update") | query.StartsWith("DELETE") | query.StartsWith("delete"))
            {
                cmd.CommandType = CommandType.Text;
            }
            else
            {
                cmd.CommandType = CommandType.StoredProcedure;
            }
            int retval;
            try
            {
                cnn.Open();
                trans = cnn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;
                retval = cmd.ExecuteNonQuery();
                trans.Commit();
            }
            catch (Exception exp)
            {
                trans.Rollback();

                throw exp;
            }
            finally
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
            return retval;
        }
        public string getHeading(string ShortHeading)
        {
            string heading = "";
            string query = "select * from HeadingMaster where ShortHeading='" + ShortHeading + "'";
            DataTable dt = GetData(query);
            if (dt.Rows.Count == 1)
            {
                heading = dt.Rows[0]["FullHeading"].ToString().Trim();
            }
            return heading;
        }
        public string getNote(string ShortHeading)
        {
            string Note = "";
            string query = "select * from HeadingMaster where ShortHeading='" + ShortHeading + "'";
            DataTable dt = GetData(query);
            if (dt.Rows.Count == 1)
            {
                Note = dt.Rows[0]["Note"].ToString().Trim();
            }
            return Note;
        }

        public decimal getFees(string type)
        {
            decimal fees = 0;
            string query = "select * from FeesMaster where FeesType='" + type + "'";
            DataTable dt = GetData(query);
            if (dt.Rows.Count == 1)
            {
                fees = Convert.ToDecimal(dt.Rows[0]["AmountToPay"]);
            }
            return fees;
        }

        public int setIsCurrentAction(string TableName, string SpecificationColumn, int SpecificationID)
        {
            string qy = "update " + TableName + " set IsCurrentAction=0 where " + SpecificationColumn + " = " + SpecificationID; // Update as Renewal
            return ExecuteNonQuery(qy);
        }
        public string getTransactionStatus(int ID)
        {
            string Status = "";
            string query = "select * from TransactionStatusMaster where ID='" + ID + "'";
            DataTable dt = GetData(query);
            if (dt.Rows.Count == 1)
            {
                Status = Convert.ToString(dt.Rows[0]["Status"]);
            }
            return Status;
        }
        public void ExpireFirm(int userId, int RegistrationFormID)
        {
            //string query = "update PaymentDetails set PaymentStatus='N' where UserID= " + userId
            //               + "insert into RegNos (ID,RegNo) select " + userId + ",RegistrationNo from RegistrationForm where ProducerID=" + userId
            //               + "update RegistrationForm set StatusCode='0', RegistrationNo='' where ProducerID= " + userId
            //               + "insert into KSSOCA_BACKUP.[dbo].RegistrationFormTransaction"
            //               + " select * from RegistrationFormTransaction where RegistrationFormID =" + RegistrationFormID
            //               + "delete from RegistrationFormTransaction where RegistrationFormID = " + RegistrationFormID;

            string query = "update PaymentDetails set PaymentStatus='N' where UserID= " + userId
                           + " insert into RegNos (ID,RegNo) select Id,RegistrationNo from RegistrationForm where ProducerID=" + userId
                           
                           + " update RegistrationForm set StatusCode='0', RegistrationNo='' where ProducerID= " + userId

                           + " select * from RegistrationFormTransaction where RegistrationFormID =" + RegistrationFormID
                           + " delete from RegistrationFormTransaction where RegistrationFormID = " + RegistrationFormID;
                            
            this.ExecuteNonQuery(query);

        }
        public void ExpireUnit(int SPURegistrationID)
        {

            string query = "update PaymentDetails set PaymentStatus='N' where PaymentTypeRefID= " + SPURegistrationID
                           + " insert into RegNos (ID,RegNo) select Id,RegistrationNo from SPURegistration where Id=" + SPURegistrationID
                           + " update SPURegistration set StatusCode='0', RegistrationNo='' where Id= " + SPURegistrationID

                           + " select * from SPURegistrationTransaction where SPURegistrationID =" + SPURegistrationID
                           + " delete from SPURegistrationTransaction where SPURegistrationID = " + SPURegistrationID;
                           
            this.ExecuteNonQuery(query);

        }
        public int GenerateID(string TableName)
        {
            int LastID = 0;

            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString))
            {
                var selectQuery = "select isnull( max([id]),0) from  " + TableName;
                LastID = Convert.ToInt32(ExecuteScalar(selectQuery));
            }
            return LastID + 1;
        }
    }
}
