﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace KSSOCA.Core.Extensions
{
   public class CommonMethods
    {
  
        public string  getConnectionString()
        {
            
            return ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        }
    }
}
