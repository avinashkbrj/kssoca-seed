﻿using KSSOCA.Core.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KSSOCA.Core.Extensions
{
    public static class ExtensionMethods
    {
        public static int? ToNullableInt(this string value)
        {
            int i;

            if (int.TryParse(value, out i))
                return i;

            return null;
        }


        public static int ToInt(this string value)
        {
            return ToNullableInt(value).Value;
        }

        public static decimal? ToNullableDecimal(this string value)
        {
            decimal i;

            if (decimal.TryParse(value, out i))
                return i;

            return null;
        }

        public static decimal ToDecimal(this string value)
        {
            return ToNullableDecimal(value).Value;
        }

        public static DateTime? ToNullableDateTime(this string value)
        {
            DateTime dateTime;

            if (DateTime.TryParse(value, out dateTime))
                return dateTime;
            else
                return null;
        }

        public static DateTime ToDateTime(this string value)
        {
            return ToNullableDateTime(value).Value;
        }

        public static void BindListItem<T>(this DropDownList list, List<T> entity)
        {
            ListItem i = new ListItem();
            i.Text = "-------Select-------";
            i.Value = "0";
            list.Items.Add(i);
            list.Items.AddRange(entity.Select(s =>
            {
                return new ListItem()
                {
                    Text = s.GetType().GetProperty("Name").GetValue(s, null).ToString(),
                    Value = s.GetType().GetProperty("Id").GetValue(s, null).ToString()
                };
            }).OrderBy(c => c.Text).ToArray());

        }
        public static void BindDropDown(this DropDownList dropdown, DataTable dt, string ValueField, string TextField)
        {
            dropdown.Items.Clear(); 
            dropdown.DataSource = dt;
            dropdown.DataValueField = ValueField;
            dropdown.DataTextField = TextField;
            dropdown.DataBind();
            dropdown.Items.Insert(0, new ListItem("-------Select-------", "0"));
        }
        public static void BindLotItem<T>(this DropDownList list, List<T> entity)
        {
            ListItem i = new ListItem();
            i.Text = "-------Select-------";
            i.Value = "0";
            list.Items.Add(i);
            list.Items.AddRange(entity.Select(s =>
            {
                return new ListItem()
                {
                    Text = s.GetType().GetProperty("LotNumber").GetValue(s, null).ToString(),
                    Value = s.GetType().GetProperty("LotNumber").GetValue(s, null).ToString()
                };
            }).OrderBy(c => c.Text).ToArray());

        }
        public static void BindCheckBoxItem<T>(this CheckBoxList list, List<T> entity)
        {
            list.Items.AddRange(entity.Select(s =>
            {
                return new ListItem()
                {
                    Text = s.GetType().GetProperty("Name").GetValue(s, null).ToString(),
                    Value = s.GetType().GetProperty("Id").GetValue(s, null).ToString()
                };
            }).OrderBy(c => c.Text).ToArray());
        }

        public static void Bind_T_H_V_ListItem<T>(this DropDownList list, List<T> entity) // generic method only to bind taluk, Hobli, Village data to the dropdown list.
        {
            ListItem i = new ListItem();
            i.Text = "-------Select-------";
            i.Value = "0";
            list.Items.Add(i);
            list.Items.AddRange(entity.Select(s =>
            {
                return new ListItem()
                {
                    Text = s.GetType().GetProperty("Name").GetValue(s, null).ToString(),
                    Value = s.GetType().GetProperty("Code").GetValue(s, null).ToString()
                };
            }).OrderBy(c => c.Text).ToArray());

        }


        public static string ToFinancialYear(this DateTime dateTime)
        {
            return (dateTime.Month >= 4)
                ? string.Format("{0}-{1}", dateTime.Year, dateTime.Year + 1)
                : string.Format("{0}-{1}", dateTime.Year - 1, dateTime.Year);
        }

        public static DataTable ToDataTable<T>(this List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static DataTable ToDataTable(this IEnumerable<dynamic> items)
        {
            var data = items.ToArray();
            if (data.Count() == 0) return null;

            var dt = new DataTable();
            foreach (var key in ((IDictionary<string, object>)data[0]).Keys)
            {
                dt.Columns.Add(key);
            }
            foreach (var d in data)
            {
                dt.Rows.Add(((IDictionary<string, object>)d).Values.ToArray());
            }
            return dt;
        }

        public static void Redirect(this Page page, string url, bool endResponse = false)
        {
            url = string.Format("~/{0}", url);
            page.Response.Redirect(url, endResponse);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static void Redirect(this UserControl userControl, string url, bool endResponse = false)
        {
            url = string.Format("~/{0}", url);
            userControl.Response.Redirect(url, endResponse);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

    }
}
