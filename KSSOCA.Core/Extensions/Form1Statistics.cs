﻿namespace KSSOCA.Core.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Data.SqlClient;
    using System.Data;
    using System.Configuration;

    public class Form1Statistics
    {
        public DataTable GetData(int RoleID,  long UserID, int FormID)
        {
            string query = @"
            select f1.Id as Form1ID,sv.Id as SourceVerificationID,um.Name as ProducerName,fd.Name as FarmerName,
			cm.Name as CropName,cv.Name as VarietyName,dm.Name as DistrictName,tm.Name as TalukName,
			f1.RegistrationDate,
			fi.id as FieldInspectionID,
			ssc.id as SeedSampleCouponID,
			sar.id as SeedAnalysisReportID,
			gpr.id as GeneticPurityReportID,
			f2.id as Form2DetailsID
			from Form1Details f1
			inner join SourceVerification sv on sv.id=f1.SourceVarification_Id
			inner join UserMaster um on um.id=f1.Producer_Id
			inner join FarmerDetails fd on fd.id=f1.FarmerID
			inner join CropMaster cm on cm.Id=sv.Crop_Id
			inner join CropVarietyMaster cv on cv.Id=sv.Variety_Id
			inner join DistrictMaster dm on dm.id=fd.District_Id
			inner join TalukMaster tm on tm.Code=fd.Taluk_Id and dm.id=tm.District_Id
			left join FieldInspection fi on fi.Form1Id=f1.Id and fi.IsFinal=1
			left join SeedSampleCoupon ssc on ssc.Form1Id=f1.Id
			left join SeedAnalysisReport sar on sar.Form1Id=ssc.Id and sar.Result=1
			left join GeneticPurityReport gpr on gpr.Form1Id=f1.Id
			left join Form2Details f2 on f2.Form1Id=f1.Id";

            string particularForm = "";
            if (FormID > 0)
            { particularForm = " and f1.Id=" + FormID; }

            string queryProducer = " where  f1.Producer_Id=" + UserID + " " + particularForm + " order by Form1ID desc";

            string queryAuthority = @" inner join UserwiseDistrictRights udr on  udr.District_Id=fd.District_Id and udr.Taluk_Id=fd.Taluk_Id
                                     where udr.User_Id = " + UserID + " " + particularForm + " order by Form1ID desc";
            if (RoleID == 5)
            {
                query = query + queryProducer;
            }
            else
            {
                query = query + queryAuthority;
            }
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }
    }
}
