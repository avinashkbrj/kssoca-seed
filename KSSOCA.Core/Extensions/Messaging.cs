﻿

namespace KSSOCA.Core.Extensions
{
    using System;
    using System.Net.Mail;
    using System.Net;
    using System.Text;
    using System.IO;
    using System.Collections;
    using System.Web;
    using System.Threading;
    using KSSOCA.Core.Exceptions;

    /// <summary>
    /// Summary description for Messaging
    /// </summary>
    public class Messaging
    {
        public Messaging()
        {

        }
        /*
        private static Boolean sendemail(String strFrom, string strTo, string strSubject, string strBody, string strAttachmentPath, bool IsBodyHTML)
        {

            Array arrToArray;
            char[] splitter = { ';' };
            arrToArray = strTo.Split(splitter);
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress(strFrom);
            mm.Subject = strSubject;
            mm.Body = strBody;
            mm.IsBodyHtml = IsBodyHTML;
            mm.ReplyTo = new MailAddress("dayananda.kadal@gmail.com");
            foreach (string s in arrToArray)
            {
                mm.To.Add(new MailAddress(s));
            }
            if (!String.IsNullOrEmpty(strAttachmentPath))
            {
                try
                {
                    //Add Attachment
                    Attachment attachFile = new Attachment(strAttachmentPath);
                    mm.Attachments.Add(attachFile);
                }
                catch { }
            }
            SmtpClient smtp = new SmtpClient();
            try
            {
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true; //Depending on server SSL Settings true/false
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = "dayananda.kadal@gmail.com";
                NetworkCred.Password = "";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;//Specify your port No;
                smtp.Send(mm);
                return true;

            }
            catch (Exception ex)
            {
                // ExceptionLoggerDAL.LogException(ex.GetBaseException());
                mm.Dispose();
                smtp = null;
                return false;
            }
        }
        public static Boolean SendMail(string recepientEmail, string bodymsg)
        {
            Thread threadSendMails;
            Boolean status = false;
           // threadSendMails = new Thread(delegate()
            //{
                status = sendemail("DAYANANDA.KADAL@GMAIL.COM", recepientEmail, "DIPLOMA ADMISSION 2015-16 NON-INTERACTIVE APPLICATION & OPTION ENTRY REGISTRATION", bodymsg, null, false);
            //});
            //threadSendMails.IsBackground = true;
            //threadSendMails.Start();
            return status;
        }
         */

        public static bool SendSMS(string message, string mobileno, string uid)
        {

            try
            {
                string strResponse = PushMessage(message, mobileno, "KSSOCA", " BABEFAF5-EBA1-43EF-AD8C-6D5641A7CE33", uid);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static String PushMessage(String strMessage, String strMobileNo, String strSenderID, String strServiceKey, string uid)
        {

            String strSendSMSUrl = "http://smspush.openhouseplatform.com/smsmessaging/1/outbound/tel%3A%2BKSSOCA/requests";
            String strResponse = String.Empty;
            String strMainContent = String.Empty;
            try
            {
                strSendSMSUrl = strSendSMSUrl.Replace("{senderAddress}", HttpUtility.UrlEncode(strSenderID));
                strMainContent = "{\"outboundSMSMessageRequest\":{\"address\":[\"tel:!address!\"],\"senderAddress\":\"tel:!sendername!\",\"outboundSMSTextMessage\":{\"message\":\"!message!\"},\"clientCorrelator\":\"\",\"receiptRequest\": {\"notifyURL\":\"\",\"callbackData\":\"$(callbackData)\"} ,\"messageType\":\"\",\"senderName\":\"\"}}";
                strMainContent = strMainContent.Replace("!address!", strMobileNo);
                strMainContent = strMainContent.Replace("!sendername!", HttpUtility.UrlEncode(strSenderID));
                strMainContent = strMainContent.Replace("!key!", strServiceKey);
                strMainContent = strMainContent.Replace("!message!", strMessage);

                Hashtable htHeaders = new Hashtable();
                htHeaders.Add("key", strServiceKey);
                strResponse = DoRequest(strSendSMSUrl, strMainContent, "POST", "application/json", htHeaders, uid);
            }
            catch (Exception ex)
            { 
                ApplicationExceptionHandler.Handle(ex);
            }

            return strResponse;
        }

        public static String DoRequest(String strURL, String strData, String strMethod, String strContentType, Hashtable htHeaders, string uid)
        {
            long iStart = DateTime.Now.Ticks;
            WebRequest objReq;
            WebResponse objRes;
            StreamReader objSr;
            try
            {
                string strRetVal = string.Empty;

                System.Net.ServicePointManager.Expect100Continue = false;
                objReq = WebRequest.Create(strURL);
                objReq.ConnectionGroupName = Guid.NewGuid().ToString();

                if (htHeaders != null)
                {
                    foreach (DictionaryEntry entry in htHeaders)
                        objReq.Headers.Add(entry.Key.ToString(), entry.Value.ToString());
                }

                objReq.Method = strMethod;//GET/POST/DELETE/PUT
                objReq.Timeout = 20000;
                objReq.ContentType = strContentType;//"application/json","application/xml","application/x-www-form-urlencoded";
                if (strData.Trim().Length > 0)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(strData);
                    objReq.ContentLength = byteArray.Length;

                    Stream dataStream = objReq.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                }

                objRes = objReq.GetResponse();
                objSr = new StreamReader(objRes.GetResponseStream());
                strRetVal = objSr.ReadToEnd();
                objSr.Close();
                objRes.Close();
                //WriteLog("Url: " + strURL + ", Response: " + strRetVal + ", PostData: " + strData);
                return strRetVal;
            }
            catch(Exception ex)
            {
                //using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                //    WriteLog("Url: " + strURL + ", Error: " + reader.ReadToEnd());
                //Utilities.Write_Error(uid, "SMS", ex.Message, "PushMessage");
                ApplicationExceptionHandler.Handle(ex);
                return "Dear User, currently we are unable to process your request. Please try after sometime.";

            }
            finally
            {
                objReq = null;
                objRes = null;
                objSr = null;
            }
        }
    }
}