﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace KSSOCA.Core.Helper
{
    public class Encryption
    {
        static readonly string STR_SALT = "$2a$10$TDA.O/2QuBYy7B0uHWxUZu";

        public static string Encrypt(string password)
        {
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(password, STR_SALT);
            return passwordHash;
        }

        public static bool Verify(string password, string passwordHash)
        {
            return BCrypt.Net.BCrypt.Verify(password, passwordHash);

        }
    }
}
