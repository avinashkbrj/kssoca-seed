﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Core.Helper
{
    public class Serializer
    {
        public static string JsonSerialize(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static object JsonDeSerialize(string value)
        {
            return JsonConvert.DeserializeObject(value);
        }

        public static T JsonDeSerialize<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }

        public static string Base64String(byte[] imageByte)
        {
            return Convert.ToBase64String(imageByte);
        }
    }
}
