﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace KSSOCA.Core.Helper
{
    public class Utility
    {
        public static string GetConfigValue(string keyName)
        {
            return ConfigurationManager.AppSettings[keyName];
        }
        public static bool IsValidMobileNo(string MobileNumber)
        {
            if (MobileNumber.ToString() == "9999999999" ||
                 MobileNumber.ToString() == "8888888888" ||
                 MobileNumber.ToString() == "7777777777" ||
                 MobileNumber.ToString() == "6666666666" ||
                 MobileNumber.ToString() == "5555555555" ||
                 MobileNumber.ToString() == "4444444444" ||
                 MobileNumber.ToString() == "3333333333" ||
                 MobileNumber.ToString() == "2222222222" ||
                 MobileNumber.ToString() == "1111111111" ||
                 MobileNumber.ToString() == "0000000000" ||
                 MobileNumber.ToString().StartsWith("0") ||
                 MobileNumber.ToString().StartsWith("1") ||
                 MobileNumber.ToString().StartsWith("2") ||
                 MobileNumber.ToString().StartsWith("3") ||
                 MobileNumber.ToString().StartsWith("4") ||
                 MobileNumber.ToString().StartsWith("5") ||
                 MobileNumber.ToString().StartsWith("6")
                  )
            {
                return false;
            }
            else return true;
        }

        public static DateTime EndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day,  23, 59, 59, 999);
        }
        public static DateTime StartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }
    }
}
