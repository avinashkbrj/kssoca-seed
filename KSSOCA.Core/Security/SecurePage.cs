﻿using KSSOCA.Core.Data;
using KSSOCA.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;

namespace KSSOCA.Core.Security
{
    public class SecurePage : System.Web.UI.Page
    {
        AcessControlService aclService;
        UserMasterService userMasterService;

        public SecurePage()
        {
            aclService = new AcessControlService();
            userMasterService = new UserMasterService();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            SetNoCache();
        }

        protected override void OnError(EventArgs e)
        {
            base.OnError(e);
            var pageError = Server.GetLastError();
        }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
           
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                int roleId = userMasterService.GetFromAuthCookie().Role_Id.Value;

                if (roleId != 1)
                {
                    var acl = aclService.Get(roleId);
                    string requestedPage = Request.Url.AbsolutePath;
                    var page = acl.Where(p => ResolveUrl(p.Url ?? "") == requestedPage).SingleOrDefault();

                    if (page == null)
                    {
                        this.Redirect("Unauthorized.aspx");
                    }
                    //}
                    //else
                    //{
                    //    this.Redirect("Login.aspx");
                    //    Response.End();
                    //}
                }
            }
            else
            {
                this.Redirect("Login.aspx");
                Response.End();
            }
        }

        private void SetNoCache()
        {
            Response.Headers.Add("Cache-Control", "no-cache, no-store");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddYears(-1));
            Response.Cache.SetNoStore();
        }
    }
}