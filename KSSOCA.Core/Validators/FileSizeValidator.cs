﻿
namespace KSSOCA.Core.Validators
{
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Helper;

    public class FileSizeValidator : BaseValidator
    {
        public FileSizeValidator()
        {
        }

        protected override bool ControlPropertiesValid()
        {
            Control ctrl = FindControl(ControlToValidate) as FileUpload;
            return (ctrl != null);
        }

        protected override bool EvaluateIsValid()
        {
            return this.ValidateSizeLimit();
        }

        protected bool ValidateSizeLimit()
        {
            var isValid = false;
            var fileUploadControl = this.FindControl(this.ControlToValidate) as FileUpload;

            if ((fileUploadControl != null && fileUploadControl is FileUpload))
            {
                int? sizeLimit = Utility.GetConfigValue("UploadFileSizeInKB").ToNullableInt();

                if (sizeLimit != null)
                {
                    this.ErrorMessage = string.Format(this.ErrorMessage, sizeLimit);
                    int actualSize = (fileUploadControl.PostedFile.ContentLength / 1024);

                    if (actualSize <= sizeLimit)
                        isValid = true;
                }
            }

            return isValid;
        }
    }
}
