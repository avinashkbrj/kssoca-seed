﻿
namespace KSSOCA.Core.Validators
{
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Helper;

    public class FileTypeValidator : BaseValidator
    {
        public FileTypeValidator()
        {
        }

        protected override bool ControlPropertiesValid()
        {
            Control ctrl = FindControl(ControlToValidate) as FileUpload;
            return (ctrl != null);
        }

        protected override bool EvaluateIsValid()
        {
            return this.ValidateFileType();
        }

        private bool ValidateFileType()
        {
            var isValid = false;
            var fileUploadControl = this.FindControl(this.ControlToValidate) as FileUpload;

            if ((fileUploadControl != null && fileUploadControl is FileUpload))
            {
                string allowedFileTypes = Utility.GetConfigValue("AllowedFileExtension");

                if (allowedFileTypes != null)
                {
                    this.ErrorMessage = string.Format(this.ErrorMessage, allowedFileTypes);
                    string fileExtension = System.IO.Path.GetExtension(fileUploadControl.PostedFile.FileName);

                    if (allowedFileTypes.Contains(fileExtension))
                        isValid = true;
                }
            }
            return isValid;
        }
    }
}
