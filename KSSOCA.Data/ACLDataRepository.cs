﻿namespace KSSOCA.Data
{
    using System.Collections.Generic;


    public class ACLDataRepository
    {

        public List<ACLModel> GetACL(int roleId)
        {
            return Get(roleId);
        }

        public List<ACLModel> GetMenuItems(int roleId)
        {
            return Get(roleId);
        }

        private List<ACLModel> Get(int roleId)
        {
            var permissions = new List<ACLModel>();
            var dashboard = new ACLModel() { Id = 1, Ordinal = 1, Name = "Home", Url = "~/Dashboard.aspx", ShowMenuItem = true };

            if (roleId == 1) //Director, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "New Request for Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 3, Name = "Masters", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 4, Name = "Reports", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 5, Name = "Registered Institutions", ShowMenuItem = true });

                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, Name = "Crop Master", Url = "~/Masters/CropMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 2, Ordinal = 2, Name = "Variety Master", Url = "~/Masters/CropVarietyMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Allot SCO to SPU", Url = "~/Masters/AllotSPU.aspx" });

                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification Report", Url = "~/Reports/SourceVerificationReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Form-I Report", Url = "~/Reports/form1report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Field Inspection Report", Url = "~/Reports/FIR_Report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Seed Arrival Report", Url = "~/Reports/BulkReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 5, Ordinal = 5, ShowMenuItem = true, Name = "Seed Processing Report", Url = "~/Reports/ProcessingReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 6, Ordinal = 6, ShowMenuItem = true, Name = "Seed Sampling Report", Url = "~/Reports/Samplingreport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 5, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 5, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "New Request for Firm Registration ", Url = "~/Transactions/FormRegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "New Request for SPU Registration ", Url = "~/Transactions/SPURegistrationView.aspx" });

                permissions.Add(new ACLModel() { Url = "~/Transactions/FormRegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SPURegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/UserMasterCreate.aspx", ShowMenuItem = false });
            }
            else if (roleId == 3)//Deputy Director Seed Certification Officer, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 3, Name = "Source Verification", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 4, Name = "Form-I", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 5, Name = "Field Inspection", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 6, Ordinal = 6, Name = "Seed Processing", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 8, Ordinal = 8, Name = "Reports", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 7, Ordinal = 7, Name = "User Manual", Url = "~/pdf/Officer_UserManual.pdf", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "New Request for Firm Registration ", Url = "~/Transactions/FormRegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "New Request for SPU Registration ", Url = "~/Transactions/SPURegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });

                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification", Url = "~/Transactions/SourceVerificationView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Form1", Url = "~/Transactions/FormOneView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 5, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Field Inspection ", Url = "~/Transactions/FieldInspectionView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 6, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Bulk Entry", Url = "~/Transactions/BulkEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Processing Entry", Url = "~/Transactions/ProcessingEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Seed Sampling Coupon", Url = "~/Transactions/SeedSampleCouponView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 8, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification Report", Url = "~/Reports/SourceVerificationReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Form-I Report", Url = "~/Reports/form1report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Field Inspection Report", Url = "~/Reports/FIR_Report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Seed Arrival Report", Url = "~/Reports/BulkReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 5, Ordinal = 5, ShowMenuItem = true, Name = "Seed Processing Report", Url = "~/Reports/ProcessingReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 6, Ordinal = 6, ShowMenuItem = true, Name = "Seed Sampling Report", Url = "~/Reports/Samplingreport.aspx" });

                permissions.Add(new ACLModel() { Url = "~/Transactions/FormRegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SPURegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/UserMasterCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/BulkEntryApproval.aspx", ShowMenuItem = false });
            }
            else if (roleId == 4)//Assistant Director Seed Certification Officer, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 3, Name = "Source Verification", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 4, Name = "Form-I", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 5, Name = "Field Inspection", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 6, Ordinal = 6, Name = "Seed Processing", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 7, Ordinal = 7, Name = "Seed Testing", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 8, Ordinal = 8, Name = "Reports", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 10, Ordinal = 10, Name = "User Manual", Url = "~/pdf/Officer_UserManual.pdf", ShowMenuItem = true });

                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "New Request for Firm Registration ", Url = "~/Transactions/FormRegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "New Request for SPU Registration ", Url = "~/Transactions/SPURegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });

                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification", Url = "~/Transactions/SourceVerificationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "New Seed Transfer Request", Url = "~/Transactions/SeedTransferRequestView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Form1", Url = "~/Transactions/FormOneView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 5, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Field Inspection ", Url = "~/Transactions/FieldInspectionView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 6, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Bulk Entry", Url = "~/Transactions/BulkEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Processing Entry", Url = "~/Transactions/ProcessingEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Seed Sampling Coupon", Url = "~/Transactions/SeedSampleCouponView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 7, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "STL Test", Url = "~/Transactions/SeedAnalysisReportView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 7, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "GOT Test", Url = "~/Transactions/GeneticPurityReportView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 8, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification Report", Url = "~/Reports/SourceVerificationReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Form-I Report", Url = "~/Reports/form1report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Field Inspection Report", Url = "~/Reports/FIR_Report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Seed Arrival Report", Url = "~/Reports/BulkReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 5, Ordinal = 5, ShowMenuItem = true, Name = "Seed Processing Report", Url = "~/Reports/ProcessingReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 8, Id = 6, Ordinal = 6, ShowMenuItem = true, Name = "Seed Sampling Report", Url = "~/Reports/Samplingreport.aspx" });


                permissions.Add(new ACLModel() { Url = "~/Transactions/FormRegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SPURegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/UserMasterCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/BulkEntryApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedTransferRequestApproval.aspx", ShowMenuItem = false });
            }

            // Transactions
            else if (roleId == 5)//Producer, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "Registration", Url = "~/Transactions/FormRegistrationCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 2, Name = "Source Verification", Url = "~/Transactions/SourceVerificationView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 4, Name = "Form-I", Url = "~/Transactions/FormOneView.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 5, Name = "Field Inspection", Url = "~/Transactions/FieldInspectionView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 6, Ordinal = 6, Name = "Seed Processing", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 7, Ordinal = 7, Name = "Seed Testing", ShowMenuItem = true });
                //permissions.Add(new ACLModel() { Id = 8, Ordinal = 8, Name = "Bagging and Tagging", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 8, Ordinal = 108, Name = "Seed Transfer Request", Url = "~/Transactions/SeedTransferRequestView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 9, Ordinal = 9, Name = "Form-II", ShowMenuItem = true, Url = "~/Transactions/Form2DetailsView.aspx" });

                permissions.Add(new ACLModel() { Id = 10, Ordinal = 7, Name = "User Manual", Url = "~/pdf/Producer_UserManual.pdf", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 11, Ordinal = 3, Name = "Register Farmer/Grower", Url = "~/Masters/AddFarmer.aspx", ShowMenuItem = true });

                permissions.Add(new ACLModel() { ParentId = 6, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Bulk Entry", Url = "~/Transactions/BulkEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Processing Entry", Url = "~/Transactions/ProcessingEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Seed Sampling Request", Url = "~/Transactions/SampleRequestCreate.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Seed Sampling coupon", Url = "~/Transactions/SeedSampleCouponView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 7, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "STL Report", Url = "~/Transactions/SeedAnalysisReportView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 7, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "GOT Report", Url = "~/Transactions/GeneticPurityReportView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 7, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Testing Payment", Url = "~/Transactions/TestingPaymentView.aspx" });

                // permissions.Add(new ACLModel() { ParentId = 8, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Tag Request", Url = "~/PageNotFound.aspx" });

                permissions.Add(new ACLModel() { Url = "~/Transactions/FormRegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedTransferRequestApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedTransferRequestCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/UserMasterCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/BulkEntryApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/AddBranch.aspx", ShowMenuItem = false });
              //  permissions.Add(new ACLModel() { Url = "~/Transactions/FormOnePaymentResponse.aspx", ShowMenuItem = false });

            }
            else if (roleId == 6)//Seed Certification Officer, KSSOCA
            {
                //Parent Menus
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 3, Name = "Source Verification", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 4, Name = "Form-I", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 5, Name = "Field Inspection", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 6, Ordinal = 6, Name = "Seed Processing", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 7, Ordinal = 7, Name = "Seed Testing", ShowMenuItem = true });
                //permissions.Add(new ACLModel() { Id = 8, Ordinal = 8, Name = "Bagging and Tagging", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 9, Ordinal = 9, Name = "Form-II", ShowMenuItem = true, Url = "~/Transactions/Form2DetailsView.aspx" });
                permissions.Add(new ACLModel() { Id = 11, Ordinal = 11, Name = "User Manual", Url = "~/pdf/Officer_UserManual.pdf", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "New Request for Firm Registration ", Url = "~/Transactions/FormRegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "New Request for SPU Registration ", Url = "~/Transactions/SPURegistrationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });

                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "New Source Verification Request", Url = "~/Transactions/SourceVerificationView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Source Verification History", Url = "~/Transactions/SourceVerificationView.aspx?type=H" });
               // permissions.Add(new ACLModel() { ParentId = 3, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "New Seed Transfer Request", Url = "~/Transactions/SeedTransferRequestView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Form1", Url = "~/Transactions/FormOneView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 5, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Field Inspection ", Url = "~/Transactions/FieldInspectionView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 6, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Bulk Entry", Url = "~/Transactions/BulkEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Processing Entry", Url = "~/Transactions/ProcessingEntry.aspx" });
                permissions.Add(new ACLModel() { ParentId = 6, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Seed Sampling Coupon", Url = "~/Transactions/SeedSampleCouponView.aspx" });

                permissions.Add(new ACLModel() { ParentId = 7, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "STL Report", Url = "~/Transactions/SeedAnalysisReportView.aspx" });
                permissions.Add(new ACLModel() { ParentId = 7, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "GOT Report", Url = "~/Transactions/GeneticPurityReportView.aspx" });

                permissions.Add(new ACLModel() { Url = "~/Transactions/FormRegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SPURegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SourceVerificationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedTransferRequestApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FormOneApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/FieldInspectionApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedSampleCouponCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/SeedSampleCouponApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Masters/UserMasterCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Url = "~/Transactions/BulkEntryApproval.aspx", ShowMenuItem = false });
                //permissions.Add(new ACLModel() { Url = "~/Transactions/SeedTransferRequestView.aspx", ShowMenuItem = false });
            }
            else if (roleId == 7)//Seed Testing Officer, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "Samples for Test", Url = "~/Transactions/SeedAnalysisReportView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 14, Ordinal = 2, Name = "Link Lab number and Form 1 Number", Url = "~/Transactions/SeedAnalysisReportCreate.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Id = 12, Ordinal = 10, Name = "User Master", Url = "~/Masters/UserMasterCreate.aspx", ParentId = 2, ShowMenuItem = false });
            }
            else if (roleId == 8) //Admin, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 3, Name = "Masters", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 4, Name = "Reports", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 2, Name = "Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, Name = "Crop Master", Url = "~/Masters/CropMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, Name = "Variety Master", Url = "~/Masters/CropVarietyMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Allot SCO to SPU", Url = "~/Masters/AllotSPU.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification Report", Url = "~/Reports/SourceVerificationReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 2, ShowMenuItem = true, Name = "Form-I Report", Url = "~/Reports/form1report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Field Inspection Report", Url = "~/Reports/FIR_Report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Seed Arrival Report", Url = "~/Reports/BulkReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 5, Ordinal = 5, ShowMenuItem = true, Name = "Seed Processing Report", Url = "~/Reports/ProcessingReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 6, Ordinal = 6, ShowMenuItem = true, Name = "Seed Sampling Report", Url = "~/Reports/Samplingreport.aspx" });

                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });

            }
            else if (roleId == 9)//GOT, KSSOCA
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 13, Ordinal = 10, Name = "Genetic Purity Report", Url = "~/Transactions/GeneticPurityReportView.aspx", ShowMenuItem = true });
                // permissions.Add(new ACLModel() { Id = 13, Ordinal = 2, Name = "Form 1 Statistics ", Url = "~/Reports/FormReport.aspx",   ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 12, Ordinal = 10, Name = "User Master", Url = "~/Masters/UserMasterCreate.aspx", ParentId = 2, ShowMenuItem = false });
            }
            else if (roleId == 10)//SPU owner
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "SPU Registration ", Url = "~/Transactions/SPURegistrationCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 3, Name = "SPU Registration Approval", Url = "~/Transactions/SPURegistrationApproval.aspx", ShowMenuItem = false });
                permissions.Add(new ACLModel() { Id = 12, Ordinal = 10, Name = "User Master", Url = "~/Masters/UserMasterCreate.aspx", ParentId = 2, ShowMenuItem = false });
            }
            else if (roleId == 11)//Decoding officer
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "STL Coding", Url = "~/Transactions/SeedAnalysisReportView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 2, Name = "GOT Coding", Url = "~/Transactions/GeneticPurityReportView.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 12, Ordinal = 10, Name = "User Master", Url = "~/Masters/UserMasterCreate.aspx", ParentId = 2, ShowMenuItem = false });
            }
            else if (roleId == 12)//DB Admin
            {
                permissions.Add(dashboard);
                permissions.Add(new ACLModel() { Id = 5, Ordinal = 2, Name = "DB Administration", Url = "~/Admin/ExportServerData.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 12, Ordinal = 10, Name = "User Master", Url = "~/Masters/UserMasterCreate.aspx", ParentId = 2, ShowMenuItem = false });
                permissions.Add(new ACLModel() { Id = 2, Ordinal = 3, Name = "Masters", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 3, Ordinal = 4, Name = "Reports", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 4, Ordinal = 2, Name = "Registration", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 1, Ordinal = 1, Name = "Crop Master", Url = "~/Masters/CropMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 2, Ordinal = 2, Name = "Variety Master", Url = "~/Masters/CropVarietyMasterCreate.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { ParentId = 2, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Allot SCO to SPU", Url = "~/Masters/AllotSPU.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Source Verification Report", Url = "~/Reports/SourceVerificationReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 1, Ordinal = 2, ShowMenuItem = true, Name = "Form-I Report", Url = "~/Reports/form1report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 1, Ordinal = 1, ShowMenuItem = true, Name = "Registered Firm", Url = "~/Reports/ProducerList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 4, Id = 2, Ordinal = 2, ShowMenuItem = true, Name = "Registered SPU", Url = "~/Reports/SPUList.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 3, Ordinal = 3, ShowMenuItem = true, Name = "Field Inspection Report", Url = "~/Reports/FIR_Report.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 4, Ordinal = 4, ShowMenuItem = true, Name = "Seed Arrival Report", Url = "~/Reports/BulkReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 5, Ordinal = 5, ShowMenuItem = true, Name = "Seed Processing Report", Url = "~/Reports/ProcessingReport.aspx" });
                permissions.Add(new ACLModel() { ParentId = 3, Id = 6, Ordinal = 6, ShowMenuItem = true, Name = "Seed Sampling Report", Url = "~/Reports/Samplingreport.aspx" });
            }
            else
            {
                permissions.Add(new ACLModel() { Id = 29, Ordinal = 2, Name = "Home", Url = "~/Home.aspx", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 31, Ordinal = 2, Name = "User Manual for Producers", Url = "~/pdf/Producer_UserManual.pdf", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 32, Ordinal = 2, Name = "Contact Us", Url = "~/pdf/contacts.pdf", ShowMenuItem = true });
                permissions.Add(new ACLModel() { Id = 33, Ordinal = 3, Name = "Log In", Url = "~/Login.aspx", ShowMenuItem = true });
            }

            return permissions;
        }
    }
    public class ACLModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public int? ParentId { get; set; }
        public bool ShowMenuItem { get; set; }
        public int Ordinal { get; set; }
    }


}
