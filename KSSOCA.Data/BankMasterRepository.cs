﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class BankMasterRepository : CrudBase
    {
        public List<BankMaster> GetAll() { try { return base.Read<BankMaster>(); } catch (Exception) { throw; } }
        public BankMaster Get(int id)
        {
            try
            {
                return base.Read<BankMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
