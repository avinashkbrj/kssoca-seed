﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using Dapper;

    public class BulkEntryRepository : CrudBase
    {
        public int Create(BulkEntry entity)
        {
            try
            {
                return base.Create<BulkEntry>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(BulkEntry entity)
        {
            try
            {
                return base.Update<BulkEntry>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
         
        public BulkEntry Get(int id)
        {
            try
            {
                return base.Read<BulkEntry>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BulkEntry GetbyForm1ID(int Form1ID)
        {
            try
            {
                string whereCondition = "Form1ID=@Form1ID";
                return base.Read<BulkEntry>(whereCondition, new { Form1ID = Form1ID }).ToList().FirstOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<dynamic> GetBulkDetails(int userid, int roleid, int form1id,
            Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            string query = @"select blk.Id, f1.id as Form1ID,fd.Name as GrowerName,
                            cm.Name as Crop, 
                            cvm.Name as Variety,
                            cs.ShortName as Class,
                            fi.EstimatedYield,fi.AcceptedArea,blk.Quintal,blk.BulkStock,
                            spu.Name as SPU,um.Name as SentTo,blk.EntryDate,blk.AcceptedStock,
                            blk.ArrivalDate,blk.StatusCode,blkt.Remarks
                            from BulkEntry blk 
							inner join BulkEntryTransaction blkt on blkt.Form1ID=blk.Form1ID
                            inner join Form1Details f1 on blk.Form1ID=f1.Id
                            inner join FarmerDetails fd on fd.Id=f1.FarmerID
                            inner join  FieldInspection fi on fi.Form1Id=f1.Id and fi.IsFinal=1
                            inner join SourceVerification sv on sv.Id=f1.SourceVarification_Id
                            inner join CropMaster cm on cm.id=sv.Crop_Id
                            inner join CropVarietyMaster cvm on cvm.id=sv.Variety_Id
                            inner join ClassSeedMaster cs on cs.Id=f1.ClassSeedtoProduced
                            inner join SPURegistration spu on spu.id=f1.SPU_Id
                            inner join UserMaster um on um.id=blkt.ToID";
            string userType = "";
            string form1 = "";
            if (roleid == 5)
            {
                userType = "FromID ";
            }
            else  
            {
                userType = " ToID ";
            }
            if (form1id > 0)
            {
                form1 = " and blk.Form1ID=" + form1id;
            }
            else if (FromDate != null && ToDate != null)
            {
                if (roleid == 5)
                {
                    form1 = " and blk.entrydate between @FromDate and @ToDate";
                }
                else  
                {
                    form1 = " and blk.entrydate between @FromDate and @ToDate";
                }
            }
            query = query + " where blkt." + userType + "=@UserID " + form1 + " order by blk.entrydate desc";
            return base.Query(query, new { UserID = userid, FromDate = FromDate, ToDate = ToDate });
        }
    }
}
