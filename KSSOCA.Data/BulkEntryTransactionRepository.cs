﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class BulkEntryTransactionRepository : CrudBase
    {
        public int Create(BulkEntryTransaction entity)
        {
            try
            {
                return base.Create<BulkEntryTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int Form1ID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  BulkEntryTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.Form1ID = " + Form1ID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
      
        public BulkEntryTransaction GetMaxTransactionDetails(int Form1ID)
        {
            try
            {
                return (BulkEntryTransaction)base.
                         ReadByCondition<BulkEntryTransaction>
                       ("id = ( SELECT MAX(id) FROM BulkEntryTransaction where Form1ID= " + Form1ID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
