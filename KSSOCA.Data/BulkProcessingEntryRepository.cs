﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using Dapper;

    public class BulkProcessingEntryRepository : CrudBase
    {
        public int Create(BulkProcessingEntry entity)
        {
            try
            {
                return base.Create<BulkProcessingEntry>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(BulkProcessingEntry entity)
        {
            try
            {
                return base.Update<BulkProcessingEntry>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int GetLastID()
        {
            int LastID = 0;

            using (var connection = new SqlConnection(this.connectionString))
            {
                var selectQuery = "select isnull( max([id]),0) from BulkProcessingEntry ";
                LastID = connection.ExecuteScalar<int>(selectQuery);
            }
            return LastID;
        }
        public BulkProcessingEntry Get(int id)
        {
            try
            {
                return base.Read<BulkProcessingEntry>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public BulkProcessingEntry GetbyForm1ID(int Form1ID)
        {
            try
            {
                string whereCondition = "Form1ID=@Form1ID";
                return base.Read<BulkProcessingEntry>(whereCondition, new { Form1ID = Form1ID }).ToList().FirstOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BulkProcessingEntry> GetbyDate(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                string whereCondition = "EntryDate between @FromDate and @ToDate";
                return base.Read<BulkProcessingEntry>(whereCondition, new { FromDate = FromDate, ToDate = ToDate }).ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<dynamic> GetBulkProcessingDetails(int userid, int roleid, int form1id, 
            Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            string query = @"select bpe.Id, f1.id as Form1ID,fd.Name as GrowerName,
                            cm.Name as Crop, 
                            cvm.Name as Variety,
                            cs.ShortName as Class,
                             blk.BulkStock,
                             blk.AcceptedStock,
                             bpe.Moisture,bpe.DateOfCleaning,
							 bpe.ScreenRejection,bpe.BlownOff,
							 TotalRejection,bpe.CleanedSeed,bpe.Bags,bpe.LotNumber,bpe.entrydate,
                             PaymentStatus,AmountPaid
                            from BulkProcessingEntry bpe 
							inner join BulkEntry blk on blk.Form1ID=bpe.Form1ID
                            inner join Form1Details f1 on blk.Form1ID=f1.Id
                            inner join FarmerDetails fd on fd.Id=f1.FarmerID
                            inner join  FieldInspection fi on fi.Form1Id=f1.Id and fi.IsFinal=1
                            inner join SourceVerification sv on sv.Id=f1.SourceVarification_Id
                            inner join CropMaster cm on cm.id=sv.Crop_Id
                            inner join CropVarietyMaster cvm on cvm.id=sv.Variety_Id
                            inner join ClassSeedMaster cs on cs.Id=f1.ClassSeedtoProduced
                            left join ProcessingPaymentDetails ppd on ppd.form1id=bpe.Form1ID
                             ";
            string userType = "";
            string form1 = "";
            if (roleid == 5)
            {
                userType = "f1.Producer_Id ";
            }
            else if (roleid == 6)
            {
                userType = "bpe.ProcessedBy ";
            }
            if (form1id > 0)
            {
                form1 = " and bpe.Form1ID=" + form1id;
            }
            else if (FromDate != null && ToDate != null)
            {
                form1 = " and bpe.entrydate between @FromDate and @ToDate";
            }
            query = query + " where  " + userType + "=@UserID " + form1 + " order by bpe.entrydate desc";
            return base.Query(query, new { UserID = userid, FromDate = FromDate, ToDate = ToDate });
        }
    }
}
