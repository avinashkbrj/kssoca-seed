﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class ClassSeedMasterRepository : CrudBase
    {
        public int Create(ClassSeedMaster classSeed)
        {
            try
            {
                return base.Create<ClassSeedMaster>(classSeed);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ClassSeedMaster> GetAll()
        {
            try
            {
                return base.Read<ClassSeedMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ClassSeedMaster> GetAllClassProduced(string ProducedID)
        {
            try
            {
                return base.Read<ClassSeedMaster>(" id in ( " + ProducedID + ")").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<ClassSeedMaster> GetAllClassforCertification()
        {
            try
            {
                return base.Read<ClassSeedMaster>(" ClassProduced is not null or ltrim(rtrim(ClassProduced))!=''").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllClassSeed()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", u.Name as ClassSeed" +
                   " from ClassSeedMaster u ");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllClassProducedIDs(int classID)
        {
            try
            {
                string query = string.Format("SELECT ClassProduced from ClassSeedMaster where  id=" + classID + "");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(ClassSeedMaster entity)
        {
            try
            {
                return base.Update<ClassSeedMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<ClassSeedMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ClassSeedMaster Get(int id)
        {
            try
            {
                return base.Read<ClassSeedMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
