﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace KSSOCA.Data
{
    public class ConnectionManager
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
        }
    }
}
