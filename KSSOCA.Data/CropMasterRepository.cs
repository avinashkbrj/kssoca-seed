﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class CropMasterRepository : CrudBase
    {
        public int Create(CropMaster crop)
        {
            try
            {
                return base.Create<CropMaster>(crop);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CropMaster> GetAll()
        {
            try
            {
                return base.Read<CropMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CropMaster> GetByType(string CropType)
        {
            try
            {
                return base.Read<CropMaster>("CropType='" + CropType + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
       
        public IEnumerable<dynamic> GetAllCrop()
        {
            try
            {
                string query = string.Format("SELECT *  FROM  CropMaster order by Name");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(CropMaster entity)
        {
            try
            {
                return base.Update<CropMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<CropMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public CropMaster Get(int id)
        {
            try
            {
                return base.Read<CropMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
