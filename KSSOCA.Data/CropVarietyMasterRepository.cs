﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class CropVarietyMasterRepository : CrudBase
    {
        public int Create(CropVarietyMaster cropVariety)
        {
            try
            {
                return base.Create<CropVarietyMaster>(cropVariety);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CropVarietyMaster> GetAll()
        {
            try
            {
                return base.Read<CropVarietyMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CropVarietyMaster> GetAllByCropID(int Crop_Id)
        {
            try
            {
                return base.Read<CropVarietyMaster>("Crop_Id='" + Crop_Id + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetvarietySV(int Crop_Id)
        {
            try
            {
                string query = string.Format("SELECT id,NameSV as Name  FROM  CropVarietyMaster where Crop_Id='" + Crop_Id + "'");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllVariety(int CropID)
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", r.Name as 'CropName', r.CropType" +
                    ", u.Name as 'Variety',u.NameSV " +
                    " from CropVarietyMaster u" +
                    " inner join CropMaster r on u.Crop_Id = r.id where Crop_Id=" + CropID);

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(CropVarietyMaster entity)
        {
            try
            {
                return base.Update<CropVarietyMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<CropVarietyMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CropVarietyMaster Get(int id)
        {
            try
            {
                return base.Read<CropVarietyMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
