﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Dapper;
using System.Reflection;

namespace KSSOCA.Data
{
    public abstract class CrudBase
    {
        protected string connectionString = ConnectionManager.GetConnectionString();

        protected virtual int Create<T>(T entity, bool excludeId = true, bool scopeIdentity = false)
        {
            int retValue;

            using (var connection = new SqlConnection(connectionString))
            {
                string insertQuery = GetInsertQuery<T>(excludeId, scopeIdentity);

                if (scopeIdentity)
                    retValue = connection.Query<int>(insertQuery, entity).Single();
                else
                    retValue = connection.Execute(insertQuery, entity);
            }

            return retValue;
        }

        protected virtual List<int> Create<T>(List<T> entities, bool excludeId = true, bool scopeIdentity = false)
        {
            List<int> retValue = new List<int>();

            using (var connection = new SqlConnection(connectionString))
            {
                string insertQuery = GetInsertQuery<T>(excludeId, scopeIdentity);

                if (scopeIdentity)
                    retValue = connection.Query<int>(insertQuery, entities).ToList<int>();
                else
                    retValue.Add(connection.Execute(insertQuery, entities));
            }

            return retValue;
        }

        protected virtual IEnumerable<T> Read<T>(string whereCondition, object param = null)
        {
            IEnumerable<T> t = default(IEnumerable<T>);

            using (var connection = new SqlConnection(connectionString))
            {
                var selectQuery = GetSelectQuery<T>();
                selectQuery = string.Format("{0} WHERE {1} ", selectQuery, whereCondition);
                t = connection.Query<T>(selectQuery, param);
            }

            return t;
        }

        protected virtual T ReadByCondition<T>(string whereCondition)
        {
            T t = default(T);

            using (var connection = new SqlConnection(connectionString))
            {
                var selectQuery = GetSelectQuery<T>();
                selectQuery = string.Format("{0} WHERE {1} ", selectQuery, whereCondition);
                t = connection.QuerySingleOrDefault<T>(selectQuery);
            }

            return t;
        }

        protected virtual T Read<T>(int id)
        {
            T t = default(T);

            using (var connection = new SqlConnection(connectionString))
            {
                var selectQuery = GetSelectQuery<T>();
                selectQuery = string.Format("{0} WHERE Id=@Id", selectQuery);
                t = connection.QuerySingleOrDefault<T>(selectQuery, new { Id = id });
            }

            return t;
        }

        protected virtual List<T> Read<T>()
        {
            List<T> entity = null;

            using (var connection = new SqlConnection(connectionString))
            {
                var selectQuery = GetSelectQuery<T>();
                entity = connection.Query<T>(selectQuery).ToList<T>();
            }

            return entity;
        }

        protected virtual IEnumerable<dynamic> Query(string query, object param = null)
        {
            IEnumerable<dynamic> entity;

            using (var connection = new SqlConnection(connectionString))
            {
                entity = connection.Query(query, param);
            }

            return entity;
        }

        protected virtual int Delete<T>(int id)
        {
            return Delete<T>(new List<int>() { id });
        }

        protected virtual int Delete<T>(List<int> id)
        {
            int deleteCount;
            string deleteQuery = string.Format("DELETE FROM {0} WHERE Id=@Id", typeof(T).Name);

            using (var connection = new SqlConnection(connectionString))
            {
                deleteCount = connection.Execute(deleteQuery, new { Id = id });
            }

            return deleteCount;
        }

        protected virtual int Delete<T>(string whereCondition, object param = null)
        {
            int deleteCount;
            string deleteQuery = string.Format("DELETE FROM {0} WHERE {1}", typeof(T).Name, whereCondition);

            using (var connection = new SqlConnection(connectionString))
            {
                deleteCount = connection.Execute(deleteQuery, param);
            }

            return deleteCount;
        }

        protected virtual int Update<T>(List<T> entity, bool ignoreNull = true)
        {
            int updateCount;
            using (var connection = new SqlConnection(connectionString))
            {
                string updateQuery = GetUpdateQuery<T>(entity);
                updateCount = connection.Execute(updateQuery, entity);
            }

            return updateCount;
        }

        protected virtual int Update<T>(T entity, bool ignoreNull = true)
        {
            return Update<T>(new List<T>() { entity });
        }

        private string GetUpdateQuery<T>(List<T> entity)
        {
            StringBuilder query = new StringBuilder();
            var lstField = GetUpdateFieldName(entity[0]);
            var strFields = MakeCommaSeperated(lstField);

            query.AppendFormat("UPDATE {0} SET {1} WHERE Id=@Id", typeof(T).Name, strFields);

            return query.ToString();
        }

        private string GetSelectQuery<T>()
        {
            var query = new StringBuilder();
            var lstFields = GetFieldName<T>(false);
            var strFields = MakeCommaSeperated(lstFields, true);

            query.AppendFormat("SELECT {0} FROM {1}", strFields, typeof(T).Name);

            return query.ToString();
        }

        private string GetInsertQuery<T>(bool excludeId, bool scopeIdentity)
        {
            var query = new StringBuilder();
            var lstFields = GetFieldName<T>(excludeId);
            var strFields = MakeCommaSeperated(lstFields, true);
            var strParams = MakeCommaSeperated(lstFields.Select(s => "@" + s).ToList());

            query.AppendFormat("INSERT INTO {0}({1}) Values({2}); ",
                typeof(T).Name, strFields, strParams);

            if (scopeIdentity)
                query.Append("SELECT CAST(SCOPE_IDENTITY() as int);");

            return query.ToString();
        }


        private string MakeCommaSeperated(List<string> fields, bool isColumnNames = false)
        {
            if (isColumnNames)
                fields = fields.Select(s => "[" + s + "]").ToList();

            var strFields = string.Join(", ", fields);
            return strFields;
        }

        private List<string> GetUpdateFieldName(object entity)
        {
            object value;
            var fields = entity.GetType().GetProperties().Select(s =>
            {
                value = s.GetValue(entity, null);

                if (s.Name == "Id")
                    return null;
                else if (value != null && value.ToString() != "0" && value.ToString() != "0.00" && !string.IsNullOrEmpty(value.ToString())) // && value.ToString() != "0" && value.ToString() != "0.00"
                    return string.Format("[{0}] = @{0}", s.Name);
                else
                    return null;
            }).ToList();

            fields = fields.Where(f => f != null).ToList();
            return fields;
        }

        private List<string> GetFieldName<T>(bool excludeId)
        {
            var fields = typeof(T).GetProperties().Select(s => s.Name).ToList();

            if (excludeId)
            {
                fields.Remove("Id");
            }

            return fields;
        }

       
    }
}
