﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class DecodingRepository : CrudBase
    {
        public int Create(Decoding entity)
        {
            try
            {
                return base.Create<Decoding>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Decoding GetBy(int id)
        {
            try
            {
                return base.Read<Decoding>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Decoding GetBySSCID(int SSC_Id)
        {
            try
            {
                string whereCondition = "SSC_Id = " + SSC_Id;
                return base.ReadByCondition<Decoding>(whereCondition);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> ReadByUser(int RoleID, int UserID, string TestType)
        {
            try
            {
                string query = @"SELECT SSC.ID as SSC_ID,d.Id as LabTestNo ,CM.NAME AS CROPNAME,CVM.Name AS VARIETYNAME, 
                                 CSM.Name as Class,ssc.TestsRequired,d.DecodedDate as SampledRecievedDate,
									 R.result AS TESTRESULT  from Decoding d 
                                     INNER JOIN  SeedSampleCoupon ssc on SSC.id=d.SSC_Id and ssc.TestType='" + TestType + "'" +
                                    " INNER JOIN Form1Details F1 ON F1.ID=SSC.Form1Id" +
                                    " INNER JOIN ClassSeedMaster CSM ON CSM.ID=F1.ClassSeedtoProduced" +
                                    " INNER JOIN SourceVerification SV ON SV.ID=F1.SourceVarification_Id" +
                                    " INNER JOIN CropMaster CM ON CM.ID=SV.Crop_Id" +
                                    " INNER JOIN CropVarietyMaster CVM ON CVM.ID=SV.Variety_Id";
                                   
                if (TestType == "STL")
                {
                    query += " LEFT JOIN SeedAnalysisReport  R on  r.LabTestNo=d.id";
                }
                else if (TestType == "GOT")
                {
                    query += " LEFT JOIN GeneticPurityReport  R on  r.LabTestNo=d.id";
                }

                if (RoleID == 11)
                {
                    query += " where d.DecodedBy=@UserID order by d.DecodedDate desc";
                }
                else if (RoleID == 5)
                {
                    query += " where F1.Producer_Id=@UserID and R.Status=1 order by  R.TestedDate desc";
                }
                else if (RoleID == 6)
                {
                    query += " where ssc.SampledBy=@UserID  order by  R.TestedDate desc";
                }
                else if (RoleID == 4)
                {
                    query += " where  R.Status = 0 order by  R.TestedDate desc";
                }
                else
                {
                    query += " where d.Sent_To=@UserID  order by  R.TestedDate desc";
                }
                return base.Query(query, new { UserID = UserID });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> ReadByUserWithoutPayment(int RoleID, int UserID, string TestType)
        {
            try
            {
                string query = @"SELECT SSC.ID as SSC_ID,d.Id as LabTestNo ,CM.NAME AS CROPNAME,CVM.Name AS VARIETYNAME, 
                                 CSM.Name as Class,ssc.TestsRequired,d.DecodedDate as SampledRecievedDate,
									 R.result AS TESTRESULT  from Decoding d 
                                     INNER JOIN  SeedSampleCoupon ssc on SSC.id=d.SSC_Id and ssc.TestType='" + TestType + "'" +
                                    " INNER JOIN Form1Details F1 ON F1.ID=SSC.Form1Id" +
                                    " INNER JOIN ClassSeedMaster CSM ON CSM.ID=F1.ClassSeedtoProduced" +
                                    " INNER JOIN SourceVerification SV ON SV.ID=F1.SourceVarification_Id" +
                                    " INNER JOIN CropMaster CM ON CM.ID=SV.Crop_Id" +
                                    " INNER JOIN CropVarietyMaster CVM ON CVM.ID=SV.Variety_Id";
                                   
                if (TestType == "STL")
                {
                    query += " LEFT JOIN SeedAnalysisReport  R on  r.LabTestNo=d.id";
                }
                else if (TestType == "GOT")
                {
                    query += " LEFT JOIN GeneticPurityReport  R on  r.LabTestNo=d.id";
                }

                if (RoleID == 11)
                {
                    query += " where d.DecodedBy=@UserID order by d.DecodedDate desc";
                }
                else if (RoleID == 5)
                {
                    query += " where F1.Producer_Id=@UserID  order by  R.TestedDate desc";
                }
                else if (RoleID == 6)
                {
                    query += " where ssc.SampledBy=@UserID  order by  R.TestedDate desc";
                }
                else if (RoleID == 4)
                {
                    query += " where d.Sent_To=27 and R.Status = 0 order by  R.TestedDate desc";
                }
                else
                {
                    query += " where d.Sent_To=@UserID  order by  R.TestedDate desc";
                }
                return base.Query(query, new { UserID = UserID });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsSSCIdExists(int SSC_Id)
        {
            try
            {
                string whereCondition = "SSC_Id = @SSC_Id";
                return base.Read<Decoding>(whereCondition, new { SSC_Id = SSC_Id }).Any();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
