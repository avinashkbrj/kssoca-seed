﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace KSSOCA.Data
{
    public class DistrictMasterRepository : CrudBase
    {
        public int Create(DistrictMaster district)
        {
            try
            {
                return base.Create(district);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<DistrictMaster> GetAll()
        {
            try
            {
                return base.Read<DistrictMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<DistrictMaster> GetDistrictNotinUserMaster(int userID)
        {
            try
            {
                return (List<DistrictMaster>)base.Read<DistrictMaster>(" id not in(select District_Id from UserwiseDistrictRights where User_Id=@User_Id)", new { User_Id = userID });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllDistrict()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.id as Id" +
                    ", u.name as 'District'" +
                   " from DistrictMaster u order by Name");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

       

        public int Update(DistrictMaster entity)
        {
            try
            {
                return base.Update(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<DistrictMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DistrictMaster Get(int id)
        {
            try
            {
                return base.Read<DistrictMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
