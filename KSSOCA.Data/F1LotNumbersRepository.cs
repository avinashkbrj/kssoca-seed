﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class F1LotNumbersRepository : CrudBase
    {
        public List<F1LotNumbers> GetByF1Id(int F1ID)
        {
            return base.Read<F1LotNumbers>("F1ID = @F1ID", new { F1ID = F1ID }).ToList();
        }
        
        public int Create(F1LotNumbers entity)
        {
            try
            {
                return base.Create<F1LotNumbers>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(F1LotNumbers entity)
        {
            try
            {
                return base.Update<F1LotNumbers>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<int> Update(List<F1LotNumbers> entities, int Form1Id)
        {
            List<int> updateCount = new List<int>();
            try
            {
                if (Form1Id > 0)
                    DeleteByRegistrationFormId(Form1Id);

                if (entities.Count > 0)
                    updateCount = Create(entities);
            }
            catch (Exception)
            {
                throw;
            }

            return updateCount;
        }
        public int DeleteByRegistrationFormId(int F1ID)
        {
            try
            {
                string whereCondition = "F1ID = @Id";
                return base.Delete<F1LotNumbers>(whereCondition, new { Id = F1ID });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int DeleteByFormIdLotNo(int F1ID,string LotNumber)
        {
            try
            {
                string whereCondition = "F1ID = @Id and LotNumber=@LotNumber";
                return base.Delete<F1LotNumbers>(whereCondition, new { Id = F1ID, LotNumber= LotNumber });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<int> Create(List<F1LotNumbers> entities)
        {
            try
            {
                return base.Create<F1LotNumbers>(entities);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
