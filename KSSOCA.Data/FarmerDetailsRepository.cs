﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class FarmerDetailsRepository : CrudBase
    {
        public List<FarmerDetails> GetAll()
        {
            try
            {
                return base.Read<FarmerDetails>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public FarmerDetails Get(int id)
        {
            try
            {
                return base.Read<FarmerDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<FarmerDetails> GetByProducerID(int Producer_Id)
        {
            try
            {
                return  base.Read<FarmerDetails>(" Producer_Id=" + Producer_Id).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Create(FarmerDetails farmerDetails)
        {
            try
            {
                return base.Create(farmerDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(FarmerDetails entity)
        {
            try
            {
                return base.Update(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<FarmerDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
