﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Data
{
    public class FieldInspectionRepository : CrudBase
    {
        public List<FieldInspection> GetAll()
        {
            try
            {
                return base.Read<FieldInspection>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(FieldInspection entity)
        {
            return base.Update<FieldInspection>(entity);
        }

        public FieldInspection Read(int id)
        {
            try
            {
                return base.Read<FieldInspection>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FieldInspection GetLastInspection(int Form1ID)
        {
            try
            {
                return base.ReadByCondition<FieldInspection>(" id = ( SELECT MAX(id) FROM FieldInspection where Form1Id= " + Form1ID + " )");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public FieldInspection GetFinalInspection(int Form1ID)
        {
            try
            {
                return base.ReadByCondition<FieldInspection>(" IsFinal=1 and Form1Id= " + Form1ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> ReadByForm1Id(int id)
        {
            try
            {
                string query = @"select fi.Id,fi.InspectionSerialNo,
                                   fd.Name,
                                   fd.MobileNo as 'MobileNumber', 
                                   d.Name as 'District', 
                                   t.Name as 'Taluk',                                
                                   Fi.InspectedDate,
                                   fi.IsFinal ,
                                   fi.Form1Id
                                   from [FieldInspection] fi								 
                                   inner join Form1Details f1 on fi.Form1Id=f1.id
								   inner join FarmerDetails fd on fd.id=f1.FarmerID
                                   INNER JOIN DistrictMaster d on fd.District_Id = d.Id 
                                   INNER JOIN TalukMaster t on fd.Taluk_Id = t.Code and t.District_Id=fd.District_Id 
                                   where fi.Form1Id=@Form1Id and f1.StatusCode=1 order by Fi.InspectedDate desc";

                return base.Query(query, new { Form1Id = id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            return base.Delete<FieldInspection>(id);
        }

        public int Create(FieldInspection entity)
        {
            try
            {
                return base.Create<FieldInspection>(entity,false);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
