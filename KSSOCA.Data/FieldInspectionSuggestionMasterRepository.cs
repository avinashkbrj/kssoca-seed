﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class FieldInspectionSuggestionMasterRepository : CrudBase
    {
        public List<FieldInspectionSuggestionMaster> GetAll()
        {
            try
            {
                return base.Read<FieldInspectionSuggestionMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public FieldInspectionSuggestionMaster Get(int id)
        {
            try
            {
                return base.Read<FieldInspectionSuggestionMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<FieldInspectionSuggestionMaster> GetSuggestions(string suggestions)
        {
            return base.Read<FieldInspectionSuggestionMaster>(" id in(" + suggestions + ")  ").ToList();
        }
    }
}
