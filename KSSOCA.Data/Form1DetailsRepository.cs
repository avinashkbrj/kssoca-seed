﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Data
{
    public class Form1DetailsRepository : CrudBase
    {
        public List<Form1Details> GetAll()
        {
            try
            {
                return base.Read<Form1Details>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(Form1Details entity)
        {
            return base.Update<Form1Details>(entity);
        }

        public Form1Details Read(int id)
        {
            try
            {
                return base.Read<Form1Details>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Form1Details ReadByStatus(int id)
        {
            try
            {
                return base.ReadByCondition<Form1Details>("StatusCode=1 and Id=" + id);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public IEnumerable<dynamic> ReadAllByUserAuthority(int userid)
        {
            try
            {
                string query = @"SELECT spu.Id as 'Id',
                                spu.SourceVarification_Id, 
                                fd.Name as 'Name',  
                                fd.MobileNo as 'MobileNumber', 
                                d.Name as 'District', 
                                tm.Name as 'Taluk',                                
                                spu.RegistrationDate,
                                spu.StatusCode ,
								f1p.PaymentStatus,f1p.AmountPaid
                                FROM Form1DetailsTransaction sput
								inner join Form1Details spu on spu.Id=sput.Form1DetailsID
								inner join FarmerDetails fd on fd.id=spu.FarmerID
								inner join UserMaster um on um.id=spu.Producer_Id
                                INNER JOIN DistrictMaster d on d.id = um.District_Id								 
								inner join TalukMaster tm on tm.District_Id=um.District_Id and tm.Code=um.Taluk_Id  
                                 Inner Join Form1PaymentDetails f1p on f1p.Form1ID=spu.Id                            
                                WHERE sput.ToID=@ActionRole_Id AND  sput.IsCurrentAction=1 order by SPUT.TransactionDate desc";

                return base.Query(query, new { ActionRole_Id = userid }); // here ActionRole_Id is nothing but useriD of the authority
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> ReadBySourceVerificationId(int id)
        {
            try
            {
                string query = @"SELECT f.Id as 'Id',
                                f.SourceVarification_Id, 
                                fd.Name as 'Name',  
                                fd.MobileNo as 'MobileNumber', 
                                d.Name as 'District', 
                                t.Name as 'Taluk',                                
                                F.RegistrationDate,
                                f.StatusCode ,
								f1p.PaymentStatus,f1p.AmountPaid
                                from Form1Details f 
								inner join FarmerDetails fd on fd.id=f.FarmerID
                                INNER JOIN SourceVerification SV ON SV.Id=F.SourceVarification_Id
                                INNER JOIN DistrictMaster d on fd.District_Id = d.Id 
                                INNER JOIN TalukMaster t on fd.Taluk_Id = t.Code and t.District_Id=fd.District_Id
                                Inner Join Form1PaymentDetails f1p on f1p.Form1ID=f.Id 
                                WHERE SourceVarification_Id  = @SourceVarification_Id";

                return base.Query(query, new { SourceVarification_Id = id });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            return base.Delete<Form1Details>(id);
        }

        public int Create(Form1Details entity)
        {
            try
            {
                return base.Create<Form1Details>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
