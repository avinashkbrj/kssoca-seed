﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class Form1DetailsTransactionRepository : CrudBase
    {
        public int Create(Form1DetailsTransaction entity)
        {
            try
            {
                return base.Create<Form1DetailsTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        } 
        public IEnumerable<dynamic> GetAllTransactions(int Form1DetailsID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  Form1DetailsTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.Form1DetailsID = " + Form1DetailsID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(Form1DetailsTransaction entity)
        {
            try
            {
                return base.Update<Form1DetailsTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        } 
        public int Delete(int id)
        {
            try
            {
                return base.Delete<Form1DetailsTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        } 
        public Form1DetailsTransaction GetMaxTransactionDetails(int Form1DetailsID)
        {
            try
            {
                return (Form1DetailsTransaction)base.
                         ReadByCondition<Form1DetailsTransaction>
                       ("id = ( SELECT MAX(id) FROM Form1DetailsTransaction where Form1DetailsID= " + Form1DetailsID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
