﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class Form1PaymentDetailRepository:CrudBase
    {
        public int Create(Form1PaymentDetails paymentDetail)
        {
            try
            {
                return base.Create<Form1PaymentDetails>(paymentDetail,false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(Form1PaymentDetails entity)
        {
            try
            {
                return base.Update<Form1PaymentDetails>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<Form1PaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Form1PaymentDetails Get(int id)
        {
            try
            {
                return base.Read<Form1PaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Form1PaymentDetails GetBy( int Form1ID)
        {
            try
            {
                return base.ReadByCondition<Form1PaymentDetails>(" Form1ID= " + Form1ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Form1PaymentDetails> GetAll()
        {
            try
            {
                return base.Read<Form1PaymentDetails>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<PaymentDetail> GetByRegistrationFormID(int RegFormID)
        //{
        //    try
        //    {
        //        return base.Read<PaymentDetail>("RegistrationFormID=" + RegFormID).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
