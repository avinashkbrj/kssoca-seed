﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class Form2DetailsRepository : CrudBase
    {
        public int Create(Form2Details form2Details)
        {
            try
            {
                return base.Create<Form2Details>(form2Details);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(Form2Details entity)
        {
            try
            {
                return base.Update<Form2Details>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<Form2Details>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Form2Details Get(int id)
        {
            try
            {
                return base.Read<Form2Details>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Form2Details GetBy(int Form1Id)
        {
            try
            {
                return base.ReadByCondition<Form2Details>(" SSCID= " + Form1Id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Form2Details> GetAll()
        {
            try
            {
                return base.Read<Form2Details>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetF2Details(int userid, int roleid )
        {
            string query = @"select f1.id as Form1ID, f2.Id, f2.SSCID,
                            cm.Name as Crop,
                            cvm.Name as Variety,
                            cs.ShortName as Class,
                            ssc.LotNo, f2.ValidityPeriod, f2.IssuedDate
                            from Form2Details f2 
                            inner join SeedSampleCoupon ssc on ssc.Id= f2.SSCID
                            inner join Form1Details f1 on ssc.Form1ID= f1.Id
                            inner join SourceVerification sv on sv.Id= f1.SourceVarification_Id
                            inner join CropMaster cm on cm.id= sv.Crop_Id
                            inner join CropVarietyMaster cvm on cvm.id= sv.Variety_Id
                            inner join ClassSeedMaster cs on cs.Id= f1.ClassSeedtoProduced";

            if (roleid == 5)
            {
                query += " where f1.Producer_Id=@UserID";
            }
            else if (roleid == 6)
            {
                query += " where f2.IssuedBy=@UserID";
            }


            return base.Query(query, new { UserID = userid });
        }

        //public List<PaymentDetail> GetByRegistrationFormID(int RegFormID)
        //{
        //    try
        //    {
        //        return base.Read<PaymentDetail>("RegistrationFormID=" + RegFormID).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
