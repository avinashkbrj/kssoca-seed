﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class GeneticPurityReportRepository : CrudBase
    {
        public int Create(GeneticPurityReport paymentDetail)
        {
            try
            {
                return base.Create<GeneticPurityReport>(paymentDetail);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(GeneticPurityReport entity)
        {
            try
            {
                return base.Update<GeneticPurityReport>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<GeneticPurityReport>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public GeneticPurityReport Get(int id)
        {
            try
            {
                return base.Read<GeneticPurityReport>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public GeneticPurityReport GetByLabTestNo( int LabTestNo)
        {
            try
            {
                return base.ReadByCondition<GeneticPurityReport>("   LabTestNo = " + LabTestNo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<GeneticPurityReport> GetAll()
        {
            try
            {
                return base.Read<GeneticPurityReport>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<PaymentDetail> GetByRegistrationFormID(int RegFormID)
        //{
        //    try
        //    {
        //        return base.Read<PaymentDetail>("RegistrationFormID=" + RegFormID).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
