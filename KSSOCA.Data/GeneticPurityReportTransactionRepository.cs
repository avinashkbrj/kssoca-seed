﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

   public class GeneticPurityReportTransactionRepository : CrudBase
    {
        public int Create(GeneticPurityReportTransaction entity)
        {
            try
            {
                return base.Create<GeneticPurityReportTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int Form1DetailsID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  GeneticPurityReportTransaction gprt
                          inner join  UserMaster um1 on gprt.FromID = um1.Id
                          inner join UserMaster um2 on gprt.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = gprt.TransactionStatus
                          where gprt.ReportID = " + Form1DetailsID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(GeneticPurityReportTransaction entity)
        {
            try
            {
                return base.Update<GeneticPurityReportTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Delete(int id)
        {
            try
            {
                return base.Delete<GeneticPurityReportTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public GeneticPurityReportTransaction GetMaxTransactionDetails(int Form1DetailsID)
        {
            try
            {
                return (GeneticPurityReportTransaction)base.
                         ReadByCondition<GeneticPurityReportTransaction>
                       ("id = ( SELECT MAX(id) FROM GeneticPurityReportTransaction where ReportID= " + Form1DetailsID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
