﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class PaymentDetailRepository:CrudBase
    {
        public int Create(PaymentDetails paymentDetail)
        {
            try
            {
                return base.Create<PaymentDetails>(paymentDetail);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(PaymentDetails entity)
        {
            try
            {
                return base.Update<PaymentDetails>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<PaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PaymentDetails Get(int id)
        {
            try
            {
                return base.Read<PaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PaymentDetails GetBy( int UserID)
        {
            try
            {
                return base.ReadByCondition<PaymentDetails>(" UserID= " + UserID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PaymentDetails> GetAll()
        {
            try
            {
                return base.Read<PaymentDetails>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<PaymentDetail> GetByRegistrationFormID(int RegFormID)
        //{
        //    try
        //    {
        //        return base.Read<PaymentDetail>("RegistrationFormID=" + RegFormID).ToList();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
    }
}
