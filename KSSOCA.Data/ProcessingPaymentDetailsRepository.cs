﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class ProcessingPaymentDetailsRepository : CrudBase
    {
        public int Create(ProcessingPaymentDetails paymentDetail)
        {
            try
            {
                return base.Create<ProcessingPaymentDetails>(paymentDetail);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(ProcessingPaymentDetails entity)
        {
            try
            {
                return base.Update<ProcessingPaymentDetails>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<ProcessingPaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ProcessingPaymentDetails Get(int id)
        {
            try
            {
                return base.Read<ProcessingPaymentDetails>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ProcessingPaymentDetails GetBy(int Form1ID)
        {
            try
            {
                return base.ReadByCondition<ProcessingPaymentDetails>(" Form1ID= " + Form1ID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProcessingPaymentDetails> GetAll()
        {
            try
            {
                return base.Read<ProcessingPaymentDetails>();
            }
            catch (Exception)
            {
                throw;
            }
        }
    
    }
}
