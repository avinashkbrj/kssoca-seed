﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class RegCropOfferredRepository : CrudBase
    {
        public List<RegCropOffered> GetAll()
        {
            return base.Read<RegCropOffered>();
        }

        //public List<RegCropOffered> GetByRegistrationId(int id)
        //{
        //    return base.Read<RegCropOffered>("RegistrationForm_Id = @Id", new { Id = id }).ToList();
        //}
        public List<RegCropOffered> GetByRegistrationId(int id)
        {
            return base.Read<RegCropOffered>("RegistrationForm_Id = @Id", new { Id = id }).ToList();
        }

        public int Create(RegCropOffered entity)
        {
            try
            {
                return base.Create<RegCropOffered>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(RegCropOffered entity)
        {
            try
            {
                return base.Update<RegCropOffered>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<int> Update(List<RegCropOffered> entities, int registrationFormId)
        {
            List<int> updateCount = new List<int>();
            try
            {
                if(registrationFormId > 0)
                    DeleteByRegistrationFormId(registrationFormId);

                if (entities.Count > 0)
                    updateCount = Create(entities);
            }
            catch (Exception)
            {
                throw;
            }

            return updateCount;
        }

        public int DeleteByRegistrationFormId(int ids)
        {
            try
            {
                string whereCondition = "RegistrationForm_Id = @Id";
                return base.Delete<RegCropOffered>(whereCondition, new { Id = ids });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<int> Create(List<RegCropOffered> entities)
        {
            try
            {
                return base.Create<RegCropOffered>(entities);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
