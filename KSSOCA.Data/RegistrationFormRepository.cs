﻿
namespace KSSOCA.Data
{
    using Dapper;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;

    public class RegistrationFormRepository : CrudBase
    {
        public int Create(RegistrationForm entity)
        {
            try
            {
                return base.Create<RegistrationForm>(entity: entity, scopeIdentity: true);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public RegistrationForm GetById(int id)
        {
            try
            {
                return base.Read<RegistrationForm>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public RegistrationForm GetByUserId(int id)
        {
            try
            {
                return base.Read<RegistrationForm>("ProducerID=@ProducerID", new { ProducerID = id }).ToList().SingleOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public RegistrationForm Read(int id)
        {
            try
            {
                return base.Read<RegistrationForm>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(RegistrationForm entity)
        {
            return base.Update<RegistrationForm>(entity);
        }
        //public List<RegistrationForm> GetAll()
        //{
        //    try
        //    {
        //        return base.Read<RegistrationForm>();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //public IEnumerable<dynamic> ReadAll()
        //{
        //    try
        //    {
        //        string query = string.Format("select * from RegistrationForm");

        //        return base.Query(query);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public IEnumerable<dynamic> ReadAllByUserAuthority(int userid)
        {
            try
            {
                string query = @"SELECT spu.Id, um.Name,spu.OfficialName, d.Name as districtName,tm.Name as talukName,spu.StatusCode,SPUT.TransactionDate
                                FROM RegistrationFormTransaction sput
								inner join RegistrationForm spu on spu.Id=sput.RegistrationFormID
								inner join UserMaster um on um.id=spu.ProducerID
                                INNER JOIN DistrictMaster d on d.id = um.District_Id 
								inner join TalukMaster tm on tm.District_Id=um.District_Id and tm.Code=um.Taluk_Id 
                                inner join PaymentDetails pd on   pd.UserID=spu.ProducerID and pd.PaymentStatus='Y'                             
                                WHERE sput.ToID=@ActionRole_Id AND  sput.IsCurrentAction=1 order by SPUT.TransactionDate desc";

                return base.Query(query, new { ActionRole_Id = userid }); // here ActionRole_Id is nothing but useriD of the authority
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetProducerBy(string Type, string RegistrationNo, Nullable<int> DivisionId)
        {
            try
            {
                string query = @"select rf.Id, um1.Name as ProName,rf.RegistrationNo,rf.Validity,um1.MobileNo,rf.Address,um2.Name as DivisionalOffice 
                                from RegistrationForm rf 
                                inner join UserMaster um1 on um1.id=rf.ProducerID
                                inner join UserWiseDistrictRights udr on um1.District_Id=udr.District_Id and um1.Taluk_Id=udr.Taluk_Id and udr.UserRole_Id=4
                                inner join UserMaster um2 on um2.id=udr.User_Id 
                                where rf.statuscode=1 ";
                if (Type == "R")
                {
                    return base.Query(query + " and  rf.RegistrationNo=@RegistrationNo", new { RegistrationNo = RegistrationNo });
                }
                else if (Type == "D")
                {
                    return base.Query(query + " and um2.id= @DivisionId order by rf.RegistrationNo", new { DivisionId = DivisionId });
                }
                else
                { return base.Query(query + " order by rf.RegistrationNo"); }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetLastRegisterNumber(string financialYear)
        {
            // here added + "/" +
            string regNumber = string.Empty;
            using (var connection = new SqlConnection(this.connectionString))
            {
                var selectQuery = "select max(SUBSTRING(RegistrationNo,0,5 ))  from RegistrationForm where RegistrationNo like '%" + financialYear + "%'";
                regNumber = Convert.ToString(connection.Query<string>(selectQuery).Single());
                int maxno = (Convert.ToInt32(regNumber) + 1);
                if (maxno < 10)
                {
                    regNumber = "000" + maxno.ToString() + "/" + financialYear;
                }
                else if (maxno > 9 && maxno < 100)
                {
                    regNumber = "00" + maxno.ToString() + "/" + financialYear;
                }
                else if (maxno > 99 && maxno < 1000)
                {
                    regNumber = "0" + maxno.ToString() + "/" + financialYear;
                }
                else
                {
                    regNumber = maxno.ToString()+ "/" + financialYear;
                }
            }
            return regNumber;
        }
        public int CheckForRegistration(UserMaster um)
        {
            int firmregapprovalstatus = 0;
            RegistrationForm registrationForm;
            if (um.Reporting_Id > 0)
            { registrationForm = this.GetByUserId(um.Reporting_Id); }
            else
            { registrationForm = this.GetByUserId(um.Id); }
            if (registrationForm != null)
            { firmregapprovalstatus = registrationForm.StatusCode; }
            return firmregapprovalstatus;
        }
    }
}