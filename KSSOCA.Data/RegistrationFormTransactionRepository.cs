﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class RegistrationFormTransactionRepository: CrudBase
    {
        public int Create(RegistrationFormTransaction crop)
        {
            try
            {
                return base.Create<RegistrationFormTransaction>(crop);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RegistrationFormTransaction> GetAll()
        {
            try
            {
                return base.Read<RegistrationFormTransaction>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<RegistrationFormTransaction> GetByRegistrationFormID(int RegFormID)
        {
            try
            {
                return base.Read<RegistrationFormTransaction>("RegistrationFormID="+ RegFormID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllTransactions(int RegistrationID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                               FROM  RegistrationFormTransaction sput
                               inner join  UserMaster um1 on sput.FromID = um1.Id
                               inner join  UserMaster um2 on sput.ToID = um2.Id
                               inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                               where sput.RegistrationFormID = " + RegistrationID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(RegistrationFormTransaction entity)
        {
            try
            {
                return base.Update<RegistrationFormTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<RegistrationFormTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RegistrationFormTransaction Get(int id)
        {
            try
            {
                return base.Read<RegistrationFormTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public RegistrationFormTransaction GetMaxTransactionDetails(int RegFormID)
        {
            try
            {
                return (RegistrationFormTransaction)base.
                        ReadByCondition<RegistrationFormTransaction>
                       ("id = ( SELECT MAX(id) FROM RegistrationFormTransaction where RegistrationFormID= "+ RegFormID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
        
    }
}
