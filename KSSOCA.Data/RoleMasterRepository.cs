﻿
namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class RoleMasterRepository : CrudBase
    {
        public int Create(RoleMaster roleMaster)
        {
            try
            {
                return base.Create<RoleMaster>(roleMaster);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RoleMaster> GetAll()
        {
            try
            {
                return base.Read<RoleMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllRole()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", u.Name as 'Role'" +
                   " from RoleMaster u ");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(RoleMaster entity)
        {
            try
            {
                return base.Update<RoleMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<RoleMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RoleMaster Get(int id)
        {
            try
            {
                return base.Read<RoleMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
