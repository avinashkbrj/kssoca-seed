﻿namespace KSSOCA.Data
{
    using Dapper;
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;


    public class SPURegistrationRepository : CrudBase
    {
        public int Create(SPURegistration entity)
        {
            try
            {
                return base.Create<SPURegistration>(entity: entity, scopeIdentity: true);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SPURegistration GetById(int id)
        {
            try
            {
                return base.Read<SPURegistration>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SPURegistration GetByUserId(int SPUId)
        {
            try
            {
                return base.Read<SPURegistration>("SPUId=@SPUId", new { SPUId = SPUId }).ToList().SingleOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SPURegistration Read(int id)
        {
            try
            {
                return base.Read<SPURegistration>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(SPURegistration entity)
        {
            return base.Update<SPURegistration>(entity);
        }

        public List<SPURegistration> GetAll()
        {
            try
            {//" StatusCode=1"
                return base.Read<SPURegistration>().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> ReadAllByUserAuthority(int userid)
        {
            try
            {
                string query = @"SELECT spu.Id, um.Name,spu.OfficialName, d.Name as districtName,tm.Name as talukName,spu.StatusCode
                                FROM SPURegistrationTransaction sput
								inner join SPURegistration spu on spu.Id=sput.SPURegistrationID
								inner join UserMaster um on um.id=spu.SPUId
                                INNER JOIN DistrictMaster d on d.id = um.District_Id 
								inner join TalukMaster tm on tm.District_Id=um.District_Id and tm.Code=um.Taluk_Id                             
                                WHERE sput.ToID=@ActionRole_Id AND sput.IsCurrentAction=1 order by SPUT.TransactionDate";

                return base.Query(query, new { ActionRole_Id = userid }); // here ActionRole_Id is nothing but useriD of the authority
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetLastRegisterNumber(string financialYear)
        {
            string regNumber = string.Empty;

            using (var connection = new SqlConnection(this.connectionString))
            {
                var selectQuery = "select top 1 RegistrationNo from SPURegistration order by RegistrationNo desc";
                regNumber =Convert.ToString(connection.Query<string>(selectQuery).Single());

                if (regNumber == null || regNumber == string.Empty)
                {
                    regNumber = "0000/" + financialYear;
                }
                else
                {
                    string lastRegNumberfinancialYear = regNumber.Remove(0, 5);
                    if (financialYear == lastRegNumberfinancialYear)
                    {
                        return regNumber;
                    }
                    else
                    {
                        regNumber = "0000/" + financialYear;
                    }
                }
            }
            return regNumber;
        }

        public IEnumerable<dynamic> GetSPUListBy(string Type, string RegistrationNo, Nullable<int> DivisionId)
        {
            try
            {
                string query = @"select um1.Reporting_Id,rf.SPUId,rf.Id,rf.spuno,rf.Name, um1.Name as ProName,rf.RegistrationNo,rf.Validity,um1.MobileNo,rf.Address,um2.Name as DivisionalOffice 
                                from SPURegistration rf 
                                inner join UserMaster um1 on um1.id=rf.SPUId
                                inner join UserWiseDistrictRights udr on um1.District_Id=udr.District_Id and um1.Taluk_Id=udr.Taluk_Id and udr.UserRole_Id=4
                                inner join UserMaster um2 on um2.id=udr.User_Id 
                                where rf.statuscode=1 ";
                if (Type == "R")
                {
                    return base.Query(query + " and  rf.RegistrationNo=@RegistrationNo", new { RegistrationNo = RegistrationNo });
                }
                else if (Type == "D")
                {
                    return base.Query(query + " and um2.id= @DivisionId order by rf.RegistrationNo", new { DivisionId = DivisionId });
                }
                else
                { return base.Query(query + " order by rf.RegistrationNo"); }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
