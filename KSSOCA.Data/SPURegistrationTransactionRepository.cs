﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    public class SPURegistrationTransactionRepository : CrudBase
    {
        public int Create(SPURegistrationTransaction entity)
        {
            try
            {
                return base.Create<SPURegistrationTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SPURegistrationTransaction> GetAll()
        {
            try
            {
                return base.Read<SPURegistrationTransaction>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SPURegistrationTransaction> GetBySPURegistrationID(int RegFormID)
        {
            try
            {
                return base.Read<SPURegistrationTransaction>(" SPURegistrationID=" + RegFormID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllTransactions(int SPURegistrationID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SPURegistrationTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.SPURegistrationID = "+ SPURegistrationID+ " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(SPURegistrationTransaction entity)
        {
            try
            {
                return base.Update<SPURegistrationTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SPURegistrationTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SPURegistrationTransaction Get(int id)
        {
            try
            {
                return base.Read<SPURegistrationTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SPURegistrationTransaction GetMaxTransactionDetails(int RegFormID)
        {
            try
            {
                return (SPURegistrationTransaction)base.
                        ReadByCondition<SPURegistrationTransaction>
                       ("id = ( SELECT MAX(id) FROM SPURegistrationTransaction where SPURegistrationID= " + RegFormID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
