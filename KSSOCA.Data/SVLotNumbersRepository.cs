﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class SVLotNumbersRepository : CrudBase
    {
        public List<SVLotNumbers> GetBySVId(int SVID)
        {
            return base.Read<SVLotNumbers>("SVID = @SVID", new { SVID = SVID }).ToList();
        }

        public SVLotNumbers GetById(int ID)
        {
            try
            {
                return base.Read<SVLotNumbers>(ID);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public SVLotNumbers GetBySVIdandLotNo(int SVID, string LotNo)
        {
            return base.ReadByCondition<SVLotNumbers>("SVID =" + SVID + " and LotNumber='" + LotNo + "'");
        }
        public List<F1LotNumbers> GetUsedQuantity(int SVID, string LotNo)
        {
            return base.Read<F1LotNumbers>("SVID =" + SVID + " and LotNumber='" + LotNo + "'").ToList();
        }
        public decimal QuantityLeft(int SVID, string LotNo)
        {
            decimal TotalQuantity = GetBySVIdandLotNo(SVID, LotNo) != null ? GetBySVIdandLotNo(SVID, LotNo).Quantity : 0;
            if (TotalQuantity != 0)
            {
                var UsedQuantityList = GetUsedQuantity(SVID, LotNo);
                decimal UsedQuantity = UsedQuantityList.AsEnumerable().Sum(row => row.Quantity);
                return TotalQuantity - UsedQuantity;
            }
            else
                return 0;
        }
        public int Create(SVLotNumbers entity)
        {
            try
            {
                return base.Create<SVLotNumbers>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int SingleObjCreate(SVLotNumbers entity)
        {
            try
            {
                return base.Create<SVLotNumbers>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(SVLotNumbers entity)
        {
            try
            {
                return base.Update<SVLotNumbers>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<int> Update(List<SVLotNumbers> entities, int SVID)
        {
            List<int> updateCount = new List<int>();
            try
            {
                if (SVID > 0)
                    DeleteByRegistrationFormId(SVID);

                if (entities.Count > 0)
                    updateCount = Create(entities);
            }
            catch (Exception)
            {
                throw;
            }

            return updateCount;
        }

        public int DeleteByRegistrationFormId(int ids)
        {
            try
            {
                string whereCondition = "SVID = @Id";
                return base.Delete<SVLotNumbers>(whereCondition, new { Id = ids });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<int> Create(List<SVLotNumbers> entities)
        {
            try
            {
                return base.Create<SVLotNumbers>(entities);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SVLotNumbers> GetByLotNo(string LotNumber)
        {
            return base.Read<SVLotNumbers>("LotNumber = @LotNumber", new { LotNumber = LotNumber }).ToList();
        }
        public List<SVLotNumbers> GetByLotNoById(int Id)
        {
            return base.Read<SVLotNumbers>("Id = @Id", new { Id = Id }).ToList();
        }
    }
}
