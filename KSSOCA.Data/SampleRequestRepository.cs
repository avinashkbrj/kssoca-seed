﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using Dapper;
   public class SampleRequestRepository : CrudBase
    {
        public int Create(SampleRequest entity)
        {
            try
            {
                return base.Create<SampleRequest>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SampleRequest entity)
        {
            try
            {
                return base.Update<SampleRequest>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int GetLastID()
        {
            int LastID = 0;

            using (var connection = new SqlConnection(this.connectionString))
            {
                var selectQuery = "select isnull( max([id]),0) from SampleRequest ";
                LastID = connection.ExecuteScalar<int>(selectQuery);
            }
            return LastID;
        }
        public SampleRequest Get(int id)
        {
            try
            {
                return base.Read<SampleRequest>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<SampleRequest> GetbyForm1ID(int Form1ID)
        {
            try
            {
                string whereCondition = "Form1ID=@Form1ID";
                return base.Read<SampleRequest>(whereCondition, new { Form1ID = Form1ID }).ToList();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
