﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    public class SampleRequestTransactionRepository : CrudBase
    {
        public int Create(SampleRequestTransaction entity)
        {
            try
            {
                return base.Create<SampleRequestTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }        

        public IEnumerable<dynamic> GetAllTransactions(int SampleRequestID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SampleRequestTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.SampleRequestID = " + SampleRequestID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SampleRequestTransaction entity)
        {
            try
            {
                return base.Update<SampleRequestTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SampleRequestTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }       
        public SampleRequestTransaction GetMaxTransactionDetails(int SampleRequestID)
        {
            try
            {
                return (SampleRequestTransaction)base.
                         ReadByCondition<SampleRequestTransaction>
                       ("id = ( SELECT MAX(id) FROM Form1DetailsTransaction where Form1DetailsID= " + SampleRequestID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
