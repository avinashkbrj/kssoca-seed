﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Data
{
    public class SeasonMasterRepository : CrudBase
    {
        public int Create(SeasonMaster season)
        {
            try
            {
                return base.Create<SeasonMaster>(season);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SeasonMaster> GetAll()
        {
            try
            {
                return base.Read<SeasonMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllSeason()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", u.Name as 'Season'" +
                    ", u.FromMonth as 'FromMonth'" +
                    ", u.ToMonth as 'ToMonth'" +
                   " from SeasonMaster u ");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(SeasonMaster entity)
        {
            try
            {
                return base.Update<SeasonMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SeasonMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SeasonMaster Get(int id)
        {
            try
            {
                return base.Read<SeasonMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SeasonMaster> GetSeasonsBy(string SeasonIds)
        {
            try
            {
                return base.Read<SeasonMaster>(" id  in (" + SeasonIds + ")").ToList(); ;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
