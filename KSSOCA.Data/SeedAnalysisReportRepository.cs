﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class SeedAnalysisReportRepository : CrudBase
    {
        public int Create(SeedAnalysisReport paymentDetail)
        {
            try
            {
                return base.Create<SeedAnalysisReport>(paymentDetail);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SeedAnalysisReport entity)
        {
            try
            {
                return base.Update<SeedAnalysisReport>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SeedAnalysisReport>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SeedAnalysisReport Get(int id)
        {
            try
            {
                return base.Read<SeedAnalysisReport>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SeedAnalysisReport GetByLabTestNo(int LabTestNo)
        {
            try
            {
                return base.ReadByCondition<SeedAnalysisReport>(" LabTestNo= " + LabTestNo);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<SeedAnalysisReport> GetAll()
        {
            try
            {
                return base.Read<SeedAnalysisReport>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int Form1DetailsID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SeedAnalysisReportTransaction sart
                          inner join  UserMaster um1 on sart.FromID = um1.Id
                          inner join UserMaster um2 on sart.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sart.TransactionStatus
                          where sart.ReportID = " + Form1DetailsID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }


    }
}
