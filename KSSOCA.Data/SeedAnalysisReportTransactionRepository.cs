﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class SeedAnalysisReportTransactionRepository : CrudBase
    {
        public int Create(SeedAnalysisReportTransaction entity)
        {
            try
            {
                return base.Create<SeedAnalysisReportTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int Form1DetailsID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SeedAnalysisReportTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.ReportID = " + Form1DetailsID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SeedAnalysisReportTransaction entity)
        {
            try
            {
                return base.Update<SeedAnalysisReportTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Delete(int id)
        {
            try
            {
                return base.Delete<SeedAnalysisReportTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SeedAnalysisReportTransaction GetMaxTransactionDetails(int Form1DetailsID)
        {
            try
            {
                return (SeedAnalysisReportTransaction)base.
                         ReadByCondition<SeedAnalysisReportTransaction>
                       ("id = ( SELECT MAX(id) FROM SeedAnalysisReportTransaction where ReportID= " + Form1DetailsID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
