﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    using System.Data;

    public class SeedSampleCouponRepository : CrudBase
    {
        public int Create(SeedSampleCoupon paymentDetail)
        {
            try
            {
                return base.Create<SeedSampleCoupon>(paymentDetail);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SeedSampleCoupon entity)
        {
            try
            {
                return base.Update<SeedSampleCoupon>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SeedSampleCoupon>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SeedSampleCoupon Get(int id)
        {
            try
            {
                return base.Read<SeedSampleCoupon>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
         
        public List<SeedSampleCoupon> GetBy(int Form1Id)
        {
            try
            {
                return base.Read<SeedSampleCoupon>("Form1Id=" + Form1Id).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SeedSampleCoupon> GetAll()
        {
            try
            {
                return base.Read<SeedSampleCoupon>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> ReadByForm1Id(int Form1Id)
        {
            try
            {
                string query = @"SELECT SSC.ID,SSC.Form1Id,CM.NAME AS CROPNAME,CVM.Name AS VARIETYNAME,
                                    SSC.LotNo,SSC.Quantity,SSC.SampledDate,ssc.TestType,SAR.Result AS TESTRESULT ,GPR.Result AS GOTRESULT                              
                                    FROM  SeedSampleCoupon SSC
                                    INNER JOIN Form1Details F1 ON F1.ID=SSC.Form1Id
                                    INNER JOIN SourceVerification SV ON SV.ID=F1.SourceVarification_Id
                                    INNER JOIN CropMaster CM ON CM.ID=SV.Crop_Id
                                    INNER JOIN CropVarietyMaster CVM ON CVM.ID=SV.Variety_Id
									left join decoding d on d.SSC_Id=ssc.id
                                    LEFT JOIN SeedAnalysisReport SAR ON d.ID =sar.LabTestNo
                                    LEFT JOIN GeneticPurityReport GPR ON GPR.LabTestNo=d.ID                                      
                                    WHERE SSC.Form1Id=@Form1Id order by SSC.ID ASC";

                return base.Query(query, new { Form1Id = Form1Id });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsSSCIdExists(int SSC_Id,string TestType)
        {
            try
            {
                string whereCondition = " TestType='"+ TestType + "' and Id = @Id";
                return base.Read<SeedSampleCoupon>(whereCondition, new { Id = SSC_Id }).Any();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
