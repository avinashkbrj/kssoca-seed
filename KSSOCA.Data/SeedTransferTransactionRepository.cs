﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using KSSOCA.Model;

namespace KSSOCA.Data
{
    public class SeedTransferTransactionRepository : CrudBase
    {
        public int Create(SeedTransferTransaction entity)
        {
            try
            {
                return base.Create<SeedTransferTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int SourceVerificationID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SeedBranchTransferTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.SourceVerificationID = " + SourceVerificationID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
