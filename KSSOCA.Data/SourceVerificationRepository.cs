﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System.Data.SqlClient;
    using System;
    using System.Collections.Generic;
    using Dapper;
    public class SourceVerificationRepository : CrudBase
    {
        public SourceVerification Read(int id)
        {
            try
            {
                return base.Read<SourceVerification>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SourceVerification GetId(int id)
        {
            try
            {
                return base.Read<SourceVerification>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetSourceVerifications(int RoleId, int UserID, string Type)
        {
            try
            {
                string query = "";

                query = @"SELECT  SV.Id, um.Name, cm.Name as CropName,csm.Name as Class,cvm.Name as variety, sv.QuantityofSeed, 
                                dm.Name as districtName,tm.Name as talukName,sv.StatusCode,sv.RegistrationDate as TransactionDate
                                FROM SourceVerification SV
                                INNER JOIN UserMaster UM ON SV.Producer_Id = UM.Id                                
                                INNER JOIN DistrictMaster DM ON DM.Id = sv.Stk_District_Id
                                inner join TalukMaster tm on tm.District_Id=sv.Stk_District_Id and tm.Code=sv.Stk_Taluk_Id  
								INNER JOIN CropMaster CM ON CM.Id = SV.Crop_Id 
								inner join ClassSeedMaster csm on csm.Id=sv.ClassSeed_Id
								inner join CropVarietyMaster cvm on cvm.Id=sv.Variety_Id";
                if (RoleId == 5)
                {
                    query = query + " WHERE SV.Producer_Id =@UserID order by sv.RegistrationDate desc";
                }
                else
                {
                    if (Type == "H")
                    {
                        query = query + " inner join SourceVerificationTransaction sput on sput.SourceVerificationID=sv.Id where sput.ToID = @UserID  AND sput.IsCurrentAction != 1 order by SPUT.TransactionDate desc";
                    }
                    else if (Type == "C")
                    {
                        query = query + " inner join SourceVerificationTransaction sput on sput.SourceVerificationID=sv.Id where sput.ToID = @UserID AND sput.IsCurrentAction = 1 order by SPUT.TransactionDate desc"; }
                }
                return base.Query(query, new { UserID = UserID });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Delete(int id)
        {
            try
            {
                return base.Delete<SourceVerification>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(SourceVerification entity)
        {
            try
            {
                return base.Update<SourceVerification>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Create(SourceVerification entity)
        {
            try
            {
                return base.Create<SourceVerification>(entity, false);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
