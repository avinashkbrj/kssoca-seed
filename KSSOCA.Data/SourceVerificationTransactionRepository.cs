﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class SourceVerificationTransactionRepository : CrudBase
    {
        public int Create(SourceVerificationTransaction entity)
        {
            try
            {
                return base.Create<SourceVerificationTransaction>(entity, false);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SourceVerificationTransaction> GetAll()
        {
            try
            {
                return base.Read<SourceVerificationTransaction>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SourceVerificationTransaction> GetByRegistrationFormID(int RegFormID)
        {
            try
            {
                return base.Read<SourceVerificationTransaction>("RegistrationFormID=" + RegFormID).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetAllCrop()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", u.Name as 'Crop'" +
                    ", u.CropCategory as 'CropCategory'" +
                    ", u.CropType  as 'CropType'" +
                    ", r.Name as 'Season'" +
                    " from CropMaster u" +
                    " inner join SeasonMaster r on u.Season_Id = r.id");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTransactions(int SourceVerificationID)
        {
            try
            {
                string query = @"SELECT um1.Name FromID,um2.Name as ToId,tsm.Status as  TransactionStatus
                              ,[TransactionDate]
                              ,[Remarks]
                          FROM  SourceVerificationTransaction sput
                          inner join  UserMaster um1 on sput.FromID = um1.Id
                          inner join UserMaster um2 on sput.ToID = um2.Id
                          inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                          where sput.SourceVerificationID = " + SourceVerificationID + " order by TransactionDate desc";

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int Update(SourceVerificationTransaction entity)
        {
            try
            {
                return base.Update<SourceVerificationTransaction>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<SourceVerificationTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SourceVerificationTransaction Get(int id)
        {
            try
            {
                return base.Read<SourceVerificationTransaction>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public SourceVerificationTransaction GetMaxTransactionDetails(int SourceVerificationID)
        {
            try
            {
                return (SourceVerificationTransaction)base.
                        ReadByCondition<SourceVerificationTransaction>
                       ("id = ( SELECT MAX(id) FROM SourceVerificationTransaction where SourceVerificationID= " + SourceVerificationID + " ) ");
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
