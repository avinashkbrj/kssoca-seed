﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class TalukHobliMasterRepositary : CrudBase
    {
        public int Create(TalukHobliMaster hobali)
        {
            try
            {
                return base.Create(hobali);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public List<TalukHobliMaster> GetAll()
        //{
        //    try
        //    {
        //        return base.Read<TalukHobliMaster>();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<TalukHobliMaster> GetAllBy(int DistrictCode, int talukCode)
        {
            try
            {
                return base.Read<TalukHobliMaster>("District_Id='" + DistrictCode + "' and Taluk_Id='" + talukCode + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public  TalukHobliMaster  GetBy(int DistrictCode, int talukCode, int HobliCode)
        {
            try
            {
                return base.ReadByCondition<TalukHobliMaster>("District_Id='" + DistrictCode + "' and Taluk_Id='" + talukCode + "' and Code='" + HobliCode + "'");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllHobali(int DistrictCode, int talukCode)
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", r.Name as 'District'" +
                    ", u.Name as 'Taluk'" +
                    ", u.CenterTalukCode as 'TalukCode'" +
                    " from TalukHobliMaster u" +
                    " inner join DistrictMaster r on u.District_Id = r.id");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(TalukHobliMaster entity)
        {
            try
            {
                return base.Update<TalukHobliMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<TalukHobliMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TalukHobliMaster Get(int id)
        {
            try
            {
                return base.Read<TalukHobliMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
