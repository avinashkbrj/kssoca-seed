﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace KSSOCA.Data
{
    public class TalukMasterRepository : CrudBase
    {
        public int Create(TalukMaster taluk)
        {
            try
            {
                return base.Create<TalukMaster>(taluk);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TalukMaster> GetAll()
        {
            try
            {
                return base.Read<TalukMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public TalukMaster GetBy(int DistrictCode, int talukCode)
        {
            try
            {
                return base.ReadByCondition<TalukMaster>("District_Id='" + DistrictCode + "' and Code='" + talukCode + "'");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<TalukMaster> GetAllbyDistID(int DistrictCode)
        {
            try
            {
                return base.Read<TalukMaster>("District_Id='" + DistrictCode + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllTaluk()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", r.Name as 'District'" +
                    ", u.Name as 'Taluk'" +
                    ", u.Code as 'TalukCode'" +
                    " from TalukMaster u" +
                    " inner join DistrictMaster r on u.District_Id = r.id");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(TalukMaster entity)
        {
            try
            {
                return base.Update<TalukMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<TalukMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public TalukMaster Get(int id)
        {
            try
            {
                return base.Read<TalukMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<TalukMaster> GetTalukNotinUserMaster(int userID, int DistrictID)
        {
            try
            {
                return (List<TalukMaster>)base.Read<TalukMaster>(" District_Id=@DistrictID and code not in(select Taluk_Id from UserwiseDistrictRights where User_Id=@User_Id and District_Id=@DistrictID)", new { User_Id = userID , DistrictID = DistrictID });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
