﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;

    public class TestMasterRepository : CrudBase
    {
        public int Create(TestMaster test)
        {
            try
            {
                return base.Create<TestMaster>(test);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public List<TestMaster> GetAll()
        {
            try
            {
                return base.Read<TestMaster>();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<TestMaster> GetTestsByType(string testType)
        {
            try
            {
                return base.Read<TestMaster>(" TestType='"+ testType + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<TestMaster> GetTestsBy(string TestIds)
        {
            try
            {
                return base.Read<TestMaster>(" id  in (" + TestIds + ")").ToList(); ;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
