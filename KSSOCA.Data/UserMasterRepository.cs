﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace KSSOCA.Data
{
    using System.Linq;
    using System.Data.SqlClient;
    using Dapper;
    

    public class UserMasterRepository : CrudBase
    {
        public string errordisplay = "";
        public int Create(UserMaster user)
        {
            try
            {
                return base.Create<UserMaster>(user,false);
            }
            catch (Exception)
            {
                throw ;
            }
        }

        public UserMaster Get(string email)
        {
            try
            {
                string whereCondition = "EmailId=@EmailId";
                return base.Read<UserMaster>(whereCondition, new { EmailId = email }).ToList().FirstOrDefault();
            }

            catch (Exception ex)
            {

                throw ex;
                
            }
        }
        public UserMaster GetbyUserName(string UserName)
        {
            try
            {
                string whereCondition = "UserName=@UserName";
                return base.Read<UserMaster>(whereCondition, new { UserName = UserName }).ToList().FirstOrDefault();
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserMaster Get(int id)
        {
            try
            {
                return base.Read<UserMaster>(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetLastRegisterNumber()
        {
            string regNumber = string.Empty;

            using (var connection = new SqlConnection(this.connectionString))
            {
                var selectQuery = "select top 1 Reg_no from usermaster order by Reg_No desc";
                regNumber = connection.Query<string>(selectQuery).Single().ToString();

                //connection.Open();
                //SqlCommand SQLCommand = new SqlCommand();
                ////SQLCommand.CommandType = CommandType.Text;
                //SQLCommand.CommandText = selectQuery;
                //Int32 USRole = (Int32)SQLCommand.ExecuteScalar();


            }

            return regNumber;
        }

        public IEnumerable<dynamic> GetAll()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", u.EmailId as 'Email'" +
                    ", u.Name as 'Name'" +
                    ", r.Name as 'Role'" +
                    ", u.MobileNo as 'Mobile No'" +
                     ", u.PhoneNo as 'Phone No'" +
                    ", u.IsActive as 'Active'" +
                    " from UserMaster u" +
                    " inner join RoleMaster r on u.Role_Id = r.id");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UserMaster> GetUserList(int Role_Id)
        {
            try
            {
                return base.Read<UserMaster>("Role_Id ="+ Role_Id + "").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        public List<UserMaster> GetByReportingOfficer(int Reporting_Id,int Role_Id)
        {
            try
            {
                return base.Read<UserMaster>(" Reporting_Id ="+ Reporting_Id + " and Role_Id =" + Role_Id + "").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsEmailIdExist(string email)
        {
            try
            {
                string whereCondition = "EmailId = @email";
                return base.Read<UserMaster>(whereCondition, new { email = email }).Any();
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        
        public bool IsUserNameExist(string UserName)
        {
            try
            {
                string whereCondition = "UserName = @UserName";
                return base.Read<UserMaster>(whereCondition, new { UserName = UserName }).Any();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsMobileNumberExist(string MobileNumber)
        {
            try
            {
                string whereCondition = "MobileNo = @MobileNo";
                return base.Read<UserMaster>(whereCondition, new { MobileNo = MobileNumber }).Any();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public IEnumerable<dynamic> GetReportingManager()
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", CONCAT(u.Name, ' - ', r.ShortName) AS Name" +
                    " FROM UserMaster u" +
                    " INNER JOIN RoleMaster r ON u.Role_Id = r.Id" +
                    " WHERE r.Id not in( 5,10)");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetSPUByReportingManager(int SCO_ID)
        {
            try
            {
                string query = @"select um1.id,spu.Name as SPUNAme,um2.Name as SCONAME from UserMaster um1 
                                inner join UserMaster um2 on um1.Reporting_Id = um2.id
                                inner join SPURegistration spu on spu.SPUId = um1.Id
                                where um1.Reporting_Id = "+ SCO_ID;

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public int Update(UserMaster entity)
        {
            try
            {
                return base.Update<UserMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<UserMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
