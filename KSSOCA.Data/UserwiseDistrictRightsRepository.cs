﻿using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace KSSOCA.Data
{
    public class UserwiseDistrictRightsRepository : CrudBase
    {
        public int Create(UserwiseDistrictRights userwiseDistrictRights)
        {
            try
            {
                return base.Create<UserwiseDistrictRights>(userwiseDistrictRights);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UserwiseDistrictRights> GetAll()
        {
            try
            {
                return base.Read<UserwiseDistrictRights>();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IEnumerable<dynamic> GetUsers(int userID, int DistrictID)
        {
            try
            {
                string query = @"SELECT  u.Id as Id , d.Name as 'District' ,t.Name as Taluk, um.Name as 'User' 
                                from UserwiseDistrictRights u 
								inner join UserMaster um on u.User_Id=um.Id
                                inner join DistrictMaster d on u.District_Id = d.id 
                                inner join TalukMaster t on t.District_Id=u.District_Id and t.code=u.Taluk_Id
                                where User_Id=@User_Id and u.District_Id=@DistrictID";

                return base.Query(query, new { User_Id = userID, DistrictID = DistrictID });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public UserwiseDistrictRights GetAllBy(int District_Id, int Taluk_Id, int UserRole_Id)
        {
            try
            {
                return base.ReadByCondition<UserwiseDistrictRights>("District_Id='" + District_Id + "' and Taluk_Id='" + Taluk_Id + "' and UserRole_Id='" + UserRole_Id + "'");
               
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(UserwiseDistrictRights entity)
        {
            try
            {
                return base.Update<UserwiseDistrictRights>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<UserwiseDistrictRights>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public UserwiseDistrictRights Get(int id)
        {
            try
            {
                return base.Read<UserwiseDistrictRights>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
