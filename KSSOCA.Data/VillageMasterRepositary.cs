﻿namespace KSSOCA.Data
{
    using KSSOCA.Model;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Data.SqlClient;
    public class VillageMasterRepositary : CrudBase
    {
        public int Create(VillageMaster village)
        {
            try
            {
                return base.Create(village);
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public List<VillageMaster> GetAll()
        //{
        //    try
        //    {
        //        return base.Read<VillageMaster>();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<VillageMaster> GetAllBy(int DistrictCode, int talukCode, int hobliCode)
        {
            try
            {
                return base.Read<VillageMaster>("District_Id='" + DistrictCode + "' and Taluk_Id='" + talukCode + "' and Hobli_Id='" + hobliCode + "'").ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public VillageMaster GetBy(int DistrictCode, int talukCode, int hobliCode, int villageCode)
        {
            try
            {
                return base.ReadByCondition<VillageMaster>("District_Id='" + DistrictCode + "' and Taluk_Id='" + talukCode + "' and Hobli_Id='" + hobliCode + "' and Code='" + villageCode + "'");
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IEnumerable<dynamic> GetAllVillage(int DistrictCode, int talukCode, int hobliCode)
        {
            try
            {
                string query = string.Format("SELECT" +
                    " u.Id as Id" +
                    ", r.Name as 'District'" +
                    ", u.Name as 'Taluk'" +
                    ", u.CenterTalukCode as 'TalukCode'" +
                    " from VillageMaster u" +
                    " inner join DistrictMaster r on u.District_Id = r.id");

                return base.Query(query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Update(VillageMaster entity)
        {
            try
            {
                return base.Update<VillageMaster>(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Delete(int id)
        {
            try
            {
                return base.Delete<VillageMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public VillageMaster Get(int id)
        {
            try
            {
                return base.Read<VillageMaster>(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

