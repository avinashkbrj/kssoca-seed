﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BulkEntry")]
    public partial class BulkEntry
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }
        public int AcceptedBy { get; set; }
        //public int Quintal { get; set; }
        public int BulkStock { get; set; }
        public Nullable<int> AcceptedStock { get; set; }
        public int StatusCode { get; set; }
       // public string Remarks { get; set; }
       // public int SentTo { get; set; }
        public int SPUId { get; set; }
        public Nullable<DateTime> EntryDate { get; set; }
        public Nullable<DateTime> ArrivalDate { get; set; }
        public byte[] BulkArrivalLetter { get; set; }
        public byte[] UndertakingLetter { get; set; }
    }
}
