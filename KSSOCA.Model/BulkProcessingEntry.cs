﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BulkProcessingEntry")]
    public class BulkProcessingEntry
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }
        public int ProcessedBy { get; set; }
        public decimal Moisture { get; set; }
        public DateTime DateOfCleaning { get; set; }
        public int ScreenRejection { get; set; }
        public int BlownOff { get; set; }
        public int TotalRejection { get; set; }
        public int CleanedSeed { get; set; }
        public int Bags { get; set; }
        public string LotNumber { get; set; }
        public DateTime EntryDate { get; set; }
    }
}
