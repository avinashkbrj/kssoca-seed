namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CropMaster")]
    public partial class CropMaster
    {
        public int Id { get; set; }

        [Required]
        [StringLength(25)]
        public string Name { get; set; }
        public decimal Inspection { get; set; }     
        public decimal GOT { get; set; }
        public int LotSize{ get; set; }
        public string CropType { get; set; }
    }
}
