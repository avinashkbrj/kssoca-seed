namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CropVarietyMaster")]
    public partial class CropVarietyMaster
    {
        public int Id { get; set; } 
        public int? Crop_Id { get; set; } 
        [Required]
        [StringLength(20)]
        public string Name  { get; set; }
        public string NameSV { get; set; } 
    }
}
