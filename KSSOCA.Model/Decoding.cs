﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("Decoding")]
    public partial class Decoding
    {
        public int Id { get; set; }
        public int SSC_Id { get; set; }       
        public System.DateTime DecodedDate { get; set; }
        public int DecodedBy { get; set; }
        public int Sent_To { get; set; }
    }
}
