﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("F1LotNumbers")]
    public partial class F1LotNumbers
    {
        public int Id { get; set; }
        public int SVID { get; set; }
        public int F1ID { get; set; }
        public string LotNumber { get; set; }
        public decimal Quantity { get; set; }
        public string TagNo { get; set; }
    }
}
