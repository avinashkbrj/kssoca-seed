﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FarmerDetails
    {
        public int Id { get; set; }
        public int Producer_Id { get; set; }
        public string Name { get; set; }
        public string FatherName { get; set; }
        public Nullable<int> District_Id { get; set; }
        public Nullable<int> Taluk_Id { get; set; }
        public Nullable<int> Hobli_Id { get; set; }
        public Nullable<int> Village_Id { get; set; }
        public string GramPanchayath { get; set; }
        public string MobileNo { get; set; }
        public Nullable<int> Bank_Id { get; set; }
        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string AadharCardNo { get; set; }
        public byte[] Photo { get; set; }
        public byte[] BankPassBook { get; set; }
        public byte[] Phani { get; set; }
        public byte[] AdharCard { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public int StatusCode { get; set; }
       
    }
}
