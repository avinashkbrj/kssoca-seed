﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    [Table("FieldInspection")]
    public partial class FieldInspection
    {
        public int Id { get; set; }
        public int InspectionSerialNo { get; set; }
        public int IsFinal { get; set; }
        public int Form1Id { get; set; }
        //public int SourceVerId { get; set; }
        //public int ProducerId { get; set; }
        public System.DateTime ActualDateOfSowing { get; set; }
        public string DocumentsInvestigated { get; set; }
        public string CropStage { get; set; }
        public string LastCrop { get; set; }
        public string LastCropVariety { get; set; }
        public string FemaleMaleLinesRatio { get; set; }
        public Nullable<int> MaleLines { get; set; }
        public string DistanceBetweenLines { get; set; }
        public decimal InspectedArea { get; set; }
        public Nullable<decimal> RejectedArea { get; set; }
        public decimal AcceptedArea { get; set; }
        public int PlantCount { get; set; }
        public string StateOfCrop { get; set; }
        public string Quality { get; set; }
        public string EstimatedYield { get; set; }
        public string HarvestingMonth { get; set; }
        public string HarvestingFortNight { get; set; }
        public string Suggestions { get; set; }
        public string Remarks { get; set; }
        public byte[] FieldImage { get; set; } 
        public Nullable<int> InspectedBy { get; set; }
        public Nullable<System.DateTime> InspectedDate { get; set; }
        public string Parentage { get; set; }
        public string IPAddress { get; set; }
        public string AreaRejectedDueTo { get; set; }
        public string lattitude { get; set; }
        public string Longitude { get; set; }
        public int StatusCode { get; set; }
        public byte[] SelfieWithFarmerImage { get; set; }
        
    }
}
