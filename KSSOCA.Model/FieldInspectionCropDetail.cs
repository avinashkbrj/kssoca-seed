 
namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FieldInspectionCropDetails")]
    public partial class FieldInspectionCropDetail
    { 
        //public string Id { get; set; }
        //public int FieldInspectionId { get; set; }
        public   decimal  offGenderMale { get; set; }
        public  decimal  offGenderFemale { get; set; }
        public decimal  offPollenFemaleLines { get; set; }
        public  decimal  offPollenFemalePlants { get; set; }
        public decimal  offReceptive { get; set; }
        public decimal offVariety { get; set; }
        public decimal seedGenderFemale { get; set; }
        public decimal seedGenderMale { get; set; }        
        public decimal seedOtherCrop { get; set; }
        public decimal seedVariety { get; set; }
        public decimal seedWeedPlants { get; set; }   
        
    }
}
