namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FieldInspectionSuggestionMaster")]
    public partial class FieldInspectionSuggestionMaster
    {
        public int Id { get; set; } 
        public string Name { get; set; }
    }
}
