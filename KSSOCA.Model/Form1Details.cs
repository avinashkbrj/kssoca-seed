namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Form1Details
    {
        public int Id { get; set; }
        public int Producer_Id { get; set; }
        public int FarmerID { get; set; }
        public int SourceVarification_Id { get; set; }
        public Nullable<int> PlotTaluk_Id { get; set; }
        public Nullable<int> PlotHobli_Id { get; set; }
        public Nullable<int> PlotVillage_Id { get; set; }
        public string PlotGramPanchayath { get; set; }
        public int SurveyNo { get; set; }
        public Nullable<int> ClassSeedtoProduced { get; set; }
        public Nullable<decimal> Areaoffered { get; set; }
        public Nullable<decimal> SeedDistributed { get; set; }
        public Nullable<decimal> DistanceNorthSouth { get; set; }
        public Nullable<decimal> DistanceEastWest { get; set; }
        public Nullable<System.DateTime> ActualDateofSowing { get; set; }
        public string Organiser { get; set; }
        public  int SPU_Id { get; set; }   
        public Nullable<int> Season_Id { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public int StatusCode { get; set; }
        public int GOTRequiredOrNot { get; set; }
        public int PlotDistrict_Id { get; set; }

        public string HissaNo { get; set; }
        public byte[] PurchaseBill { get; set; }
        public int FarmerOtp { get; set; }
        public byte[] Vamshavruksha { get; set; }
    }
}
