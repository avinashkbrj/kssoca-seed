﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Form1PaymentDetails")]
    public class Form1PaymentDetails
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }
        public Nullable<decimal> RegFees { get; set; }
        public Nullable<decimal> InspectionCharge { get; set; }
        public Nullable<decimal> STLCharge { get; set; }
        public Nullable<decimal> GPTCharge { get; set; }
        public Nullable<decimal> OtherCharge { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public string BankTransactionID { get; set; }
        public byte[] PaySlip { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string PaymentMode { get; set; }
        public Nullable<System.DateTime> ChequeDate { get; set; }
        
       
    }
}
