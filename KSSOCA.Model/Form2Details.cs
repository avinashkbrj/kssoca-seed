namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Form2Details")]
    public partial class Form2Details
    {
        public int Id { get; set; }
        public int SSCID { get; set; }
        public System.DateTime ValidityPeriod { get; set; }
        public string TagsIssued { get; set; }
        public string TagsUsed { get; set; }
        public string TagsUnUsed { get; set; }
        public Nullable<int> IssuedBy { get; set; }
        public Nullable<System.DateTime> IssuedDate { get; set; }
        public string Form2Number { get; set; }
        public byte[] CertificateImage { get; set; }
         
    }
}
