namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GeneticPurityReport")]
    public partial class GeneticPurityReport
    {
        public int Id { get; set; }
        //public int SSC_Id { get; set; }
        public int LabTestNo { get; set; }
        public decimal GeneticPurity { get; set; }
        public string Result { get; set; }       
        public string PreparedBy { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> TestedBy { get; set; }
        public Nullable<System.DateTime> TestedDate { get; set; } 
        public int status  { get; set; }
        public int PaymentStatus { get; set; }
        public int RequisitionOfTags { get; set; }
        public int AllotmentsOfTags { get; set; }
        public int NoOfLabels { get; set; }
    }
}
