﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Model
{
    class LabPaymentDetails
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }
        public string PaymentType { get; set; }
        public string PaymentMode { get; set; }
        public decimal AmountPaid { get; set; }
        public string BankTransactionID { get; set; }
        public byte[] PaySlip { get; set; }
        public string PaymentStatus { get; set; }
        public System.DateTime PaymentDate { get; set; }
    }
}
