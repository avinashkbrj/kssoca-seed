namespace KSSOCA.Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<BankMaster> BankMasters { get; set; }
        public virtual DbSet<ClassSeedMaster> ClassSeedMasters { get; set; }
       
        public virtual DbSet<CropMaster> CropMasters { get; set; }
        public virtual DbSet<CropVarietyMaster> CropVarietyMasters { get; set; }
        public virtual DbSet<DistrictMaster> DistrictMasters { get; set; }      
        public virtual DbSet<Form1Details> Form1Details { get; set; }      
        
        public virtual DbSet<RegCropOffered> RegCropOffereds { get; set; }
        public virtual DbSet<RegistrationForm> RegistrationForms { get; set; }
        
        public virtual DbSet<RoleMaster> RoleMasters { get; set; }
        public virtual DbSet<SeasonMaster> SeasonMasters { get; set; }
         
        public virtual DbSet<SourceVerification> SourceVerifications { get; set; }
        public virtual DbSet<TalukMaster> TalukMasters { get; set; }
        public virtual DbSet<UserMaster> UserMasters { get; set; }
        public virtual DbSet<VillageMaster> VillageMasters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BankMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<ClassSeedMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            
            modelBuilder.Entity<CropMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

          
            modelBuilder.Entity<CropVarietyMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

           

            modelBuilder.Entity<DistrictMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);  
             

            modelBuilder.Entity<Form1Details>()
                .Property(e => e.DistanceNorthSouth)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Form1Details>()
                .Property(e => e.DistanceEastWest)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Form1Details>()
                .Property(e => e.Organiser)
                .IsUnicode(false);       

            
            
            modelBuilder.Entity<RegCropOffered>()
                .Property(e => e.Area)
                .IsUnicode(false);

            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.RegistrationNo)
                .IsUnicode(false);

           
            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.Address)
                .IsUnicode(false);            

            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.TinNo)
                .IsUnicode(false);          

            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.OfficialName)
                .IsUnicode(false);

            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.PrevRegNo)
                .IsUnicode(false);

            modelBuilder.Entity<RegistrationForm>()
                .Property(e => e.TotalArea)
                .IsUnicode(false);            

            modelBuilder.Entity<RoleMaster>()
                .Property(e => e.ShortName)
                .IsUnicode(false);

            modelBuilder.Entity<RoleMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<SeasonMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);
 
            

            modelBuilder.Entity<SourceVerification>()
                .Property(e => e.QuantityofSeed)
                .HasPrecision(10, 2);

            modelBuilder.Entity<SourceVerification>()
                .Property(e => e.AreainAcre)
                .HasPrecision(10, 2); 

            modelBuilder.Entity<SourceVerification>()
                .Property(e => e.LocationofSeedStock)
                .IsUnicode(false);        

            modelBuilder.Entity<TalukMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.EmailId)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);

            

            modelBuilder.Entity<UserMaster>()
                .Property(e => e.MobileNo)
                .IsUnicode(false);

           

            modelBuilder.Entity<VillageMaster>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}
