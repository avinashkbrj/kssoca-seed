﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KSSOCA.Model
{
   public class OnlinePayment
    {
        public int RegNumber { get; set; }
        public decimal RegFees { get; set; }
        public decimal InspectionCharge { get; set; }
        public decimal STLCharge { get; set; }
        public decimal GPTCharge { get; set; }
        public decimal OtherCharge { get; set; }
        public decimal TotalAmountPaid { get; set; }
        public decimal ProcessingCharge { get; set; }
        public decimal ClothBagCharge { get; set; }
        public decimal PolytheneCharge { get; set; }
        public decimal CourierCharge { get; set; }
        public int StlID { get; set; }
        public int GotId { get; set; }

    }
}
