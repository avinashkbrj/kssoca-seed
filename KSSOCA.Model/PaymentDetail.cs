namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PaymentDetails")]
    public partial class PaymentDetails
    {
        public int Id { get; set; }
        public int UserID { get; set; }
        public string PaymentType { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }
        public string BankTransactionID { get; set; }
        public byte[] PaySlip { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string PaymentMode { get; set; }
        public Nullable<int> PaymentTypeRefID { get; set; }
    }
}
