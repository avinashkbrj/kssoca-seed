﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProcessingPaymentDetails")]
    public class ProcessingPaymentDetails
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }       
        public Nullable<decimal> STLCharge { get; set; }
        public Nullable<decimal> GPTCharge { get; set; }
        public Nullable<decimal> ProcessingCharge { get; set; }        
        public Nullable<decimal> ClothBagCharge { get; set; }
        public Nullable<decimal> PolytheneCharge { get; set; }
        public Nullable<decimal> CourierCharge { get; set; }
        public Nullable<decimal> AmountPaid { get; set; }        
        public string BankTransactionID { get; set; }
        public byte[] PaySlip { get; set; }
        public string PaymentStatus { get; set; }
        public Nullable<System.DateTime> PaymentDate { get; set; }
        public string PaymentMode { get; set; }
    }
}
