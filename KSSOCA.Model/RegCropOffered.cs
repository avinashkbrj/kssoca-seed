namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RegCropOffered")]
    public partial class RegCropOffered
    {
        //public int Id { get; set; }

        [Required]
        public int RegistrationForm_Id { get; set; }

        public int Crop_Id { get; set; }

        public string Area { get; set; }

        public string Location { get; set; }
         
    }
}
