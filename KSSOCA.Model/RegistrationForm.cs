namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RegistrationForm")]
    public partial class RegistrationForm
    {
        public int Id { get; set; }
        public Nullable<int> ProducerID { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string AadhaarNumber { get; set; }
        public string Address { get; set; }
        public string TinNo { get; set; }
        public string OfficialName { get; set; }
        public string Season_Id { get; set; }
        public string PrevRegNo { get; set; }
        public string TotalArea { get; set; }
        public Nullable<int> SeedProcessUnit_Id { get; set; }
        public byte[] Photo { get; set; }
        public byte[] Sign { get; set; }
        public byte[] Seal { get; set; }
        public byte[] VATImage { get; set; }
        public byte[] LetterImage { get; set; }
        public int StatusCode { get; set; }
        public string RegistrationType { get; set; }
        public Nullable<DateTime> Validity { get; set; }
    }
}
