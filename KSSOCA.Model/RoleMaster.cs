namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RoleMaster")]
    public partial class RoleMaster
    {
        public int Id { get; set; }

        [StringLength(15)]
        public string ShortName { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}
