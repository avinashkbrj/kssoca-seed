﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SPURegistration")]
    public partial class SPURegistration
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> SPUId { get; set; }
        public string RegistrationNo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public string AadhaarNumber { get; set; }
        public string Address { get; set; }
        
        public string OfficialName { get; set; }
        public string SPUNO { get; set; }
        public string Last_Renewal { get; set; }
        public byte[] Photo { get; set; }
        public byte[] Sign { get; set; }
        public byte[] Seal { get; set; }      
        
        public byte[] RentLetter { get; set; }
        public byte[] BuildingMap { get; set; }
        public byte[] PhaniLetter { get; set; }
        public byte[] CurrentBill { get; set; }
        public byte[] CentralRegLetter { get; set; }
        public byte[] GramPanchayatLetter { get; set; }
        public byte[] AirPollutionDepLetter { get; set; }
        public byte[] PurchaseLetter { get; set; }
        public byte[] IDCard { get; set; }       
        public int StatusCode { get; set; }
        public string RegistrationType { get; set; }
        public Nullable<DateTime> Validity { get; set; }
    }
}
