﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SVLotNumbers")]
    public partial class SVLotNumbers
    {
        public int Id { get; set; }
        public int SVID { get; set; }
        public string LotNumber { get; set; }
        public decimal Quantity { get; set; }
        public System.DateTime ValidPeriod { get; set; }
        public string PurchaseBillNo { get; set; }
        public System.DateTime PurchaseBillDate { get; set; }
        public string ReleaseOrderNo { get; set; }
        public System.DateTime ReleaseOrderDate { get; set; }
        public decimal SeedRate { get; set; }
    }
}
