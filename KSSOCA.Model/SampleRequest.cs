﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SampleRequest")]
    public partial class SampleRequest
    {
        public int Id { get; set; }
        public int Form1ID { get; set; }
        public int Producer_ID { get; set; }        
        public string Request { get; set; }
        public DateTime RequestDate { get; set; }
        public string TestType { get; set; }
        public string TestsRequired { get; set; }
          
    }
}
