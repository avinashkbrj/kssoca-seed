﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("SampleRequestTransaction")]
    public partial class SampleRequestTransaction
    {
        public int SampleRequestID { get; set; }
        public int FromID { get; set; }
        public int ToID { get; set; }
        public int TransactionStatus { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string Remarks { get; set; }
        public int IsCurrentAction { get; set; }
    }
}
