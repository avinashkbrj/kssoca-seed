namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SeedAnalysisReport")]
    public partial class SeedAnalysisReport
    {
        public int Id { get; set; }      
        public int LabTestNo { get; set; }
        public decimal PureSeed { get; set; }
        public decimal InertMatter { get; set; }
        public decimal OtherCropSeeds { get; set; }
        public decimal HuskLessSeed { get; set; }
        public decimal ODV { get; set; }
        public decimal TotalWeedSeed { get; set; }
        public decimal ObjectionableWeedSeed { get; set; }
        public decimal Germination { get; set; }
        public decimal HardSeed { get; set; }
        public decimal FreshUngerminatedSeed { get; set; }
        public decimal SeedMoisture { get; set; }
        public decimal SeedBorneDiseases { get; set; }
        public decimal InsectDamage { get; set; }
        public string PreparedBy { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> TestedBy { get; set; }
        public Nullable<System.DateTime> TestedDate { get; set; }
        public int Result { get; set; }
        public string Failed_Tests { get; set; } 
        public int Status { get; set; }
        public int PaymentStatus { get; set; }
        public int RequisitionOfTags { get; set; }
        public int AllotmentsOfTags { get; set; }
        public int NoOfLabels { get; set; }
    }
}
