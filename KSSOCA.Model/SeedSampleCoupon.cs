namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SeedSampleCoupon")]
    public partial class SeedSampleCoupon
    {
        public int Id { get; set; }
        public int Form1Id { get; set; }
        public string LotNo { get; set; }
        public decimal Quantity { get; set; }
        public string SeedTreatment { get; set; }
        public string Chemical { get; set; }
        public decimal PackingSize { get; set; }
        public string Remarks { get; set; }
        public int SampledBy { get; set; }
        public System.DateTime SampledDate { get; set; }
        public string ProcessedOrNot { get; set; }
        public string TestsRequired  { get; set; }
        public string TestType { get; set; }
        public int IsApproved { get; set; }
        public int IsRecieved { get; set; }
       
    }
}
