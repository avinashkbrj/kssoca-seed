namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SourceVerification")]
    public partial class SourceVerification
    {
        public int Id { get; set; }
        public int Producer_Id { get; set; }
        public System.DateTime RegistrationDate { get; set; }
        public int Crop_Id { get; set; }
        public int Variety_Id { get; set; }
        public int ClassSeed_Id { get; set; }
        //public string LotNo { get; set; }
        public decimal QuantityofSeed { get; set; }
        public decimal SeedRate { get; set; }
        public decimal AreainAcre { get; set; }
        public int Season_Id { get; set; }
        public string LocationofSeedStock { get; set; }
        public byte[] ReleaseOrderimage { get; set; }
        public byte[] PurchaseBillimage { get; set; }
        public byte[] BCimage { get; set; }
        public byte[] Tagsimage { get; set; }
        public string TagNumbers { get; set; }
        public int StatusCode { get; set; }
        public int Stk_District_Id { get; set; }
        public int Stk_Taluk_Id { get; set; }
        public string SourceType { get; set; }
        public string FinancialYear { get; set; }
        public string CropType { get; set; }
        public string ProducerRegNo { get; set; }
    }
}
