﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TestMaster")]
    public partial class TestMaster
    {
        public int Id { get; set; }       
        public string Name { get; set; }
        public decimal Charge { get; set; }       
    }
}
