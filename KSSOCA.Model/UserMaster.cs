namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserMaster")]
    public partial class UserMaster
    {
        public int Id { get; set; }
        [Required]
        [StringLength(75)]
        public string EmailId { get; set; }
        [Required]
        [StringLength(125)]
        public string Password { get; set; }
        [StringLength(200)]
        public string Name { get; set; }        
        public int? District_Id { get; set; }
        public int? Taluk_Id { get; set; }
        [StringLength(10)]
        public string MobileNo { get; set; }
        public Nullable<DateTime> RegistrationDate { get; set; }

        public int? Role_Id { get; set; }
        public int Reporting_Id { get; set; }
        public bool? IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public int? LoginAttempt { get; set; }
        public string UserName { get; set; }
        public int Division_Id { get; set; }
    }
}
