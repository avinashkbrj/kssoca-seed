﻿namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserwiseDistrictRights")]
    public partial class UserwiseDistrictRights
    {
        public int Id { get; set; }
        public int? District_Id { get; set; }
        public int? Taluk_Id { get; set; }
        public int? User_Id { get; set; }
        public int? UserRole_Id { get; set; }
    }
}