namespace KSSOCA.Model
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VillageMaster")]
    public partial class VillageMaster
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public int District_Id { get; set; }
        public int Taluk_Id { get; set; }
        public int Hobli_Id { get; set; }

        public int Code { get; set; }
    }
}
