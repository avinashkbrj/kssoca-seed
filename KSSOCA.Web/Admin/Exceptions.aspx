﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Exceptions.aspx.cs" Inherits="KSSOCA.Web.Admin.Exceptions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <h1>Exceptionlist</h1>

        <div>
            <div class="well">

                <asp:GridView runat="server" ID="ExceptionsGridView"
                    AutoGenerateColumns="false"    
                    AllowPaging="true" PageSize="20" AllowSorting="true" DataKeyNames="Logid">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> ..
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Logid" HeaderText="Logid" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ExceptionMsg" HeaderText="Exception Msg" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ExceptionType" HeaderText="Exception Type" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ExceptionSource" HeaderText="Exception Source" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ExceptionURL" HeaderText="Exception URL" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Logdate" HeaderText="Log date" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnCheck" runat="server" Text="Checked" CommandArgument='<%#Eval("Logid")%>' OnClick="btnCheck_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                 <EmptyDataTemplate></EmptyDataTemplate></asp:GridView>
                <br />
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
