﻿using System;
using System.Data;
using KSSOCA.Core.Data;
using System.Web.UI.WebControls;
using KSSOCA.Core.Security;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;
using System.Web.UI;
using System.IO;
using System.Text;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;

namespace KSSOCA.Web.Admin
{
    public partial class Exceptions : System.Web.UI.Page
    {
        Common common = new Common();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindgrid();
            }
        }

        public void bindgrid()
        {
            DataTable data = common.GetData("SELECT  *  FROM  Tbl_ExceptionLoggingToDataBase order by Logdate desc");
            ExceptionsGridView.DataSource = data;
            ExceptionsGridView.DataBind();
            ExceptionsGridView.Visible = true;
        }
        protected void ExportToExcel(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=ProducersList.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(sw);
                //To Export all pages
                // ExceptionsGridView.AllowPaging = false;
                bindgrid();
                ExceptionsGridView.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in ExceptionsGridView.HeaderRow.Cells)
                {
                    cell.BackColor = ExceptionsGridView.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in ExceptionsGridView.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = ExceptionsGridView.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = ExceptionsGridView.RowStyle.BackColor;
                        }
                        // cell.CssClass = "textmode";
                    }
                }

                ExceptionsGridView.RenderControl(hw);

                // style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            common.ExecuteNonQuery("update Tbl_ExceptionLoggingToDataBase set Status=1 where LogId=" + id + "");
            bindgrid();
        }
    }
}