﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExportServerData.aspx.cs" Inherits="KSSOCA.Web.Admin.ExportServerData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .style3 {
            color: #663300;
            font-weight: bold;
            text-decoration: underline;
            font-size: large;
        }

        .style17 {
            color: #006600;
            font-weight: bold;
        }
    </style>
    <div style="width: 100%;" align="center">

        <table>
            <tr>
                <td>


                    <table align="center" border="1" dir="ltr">
                        <tr>
                            <td align="center" colspan="7" class="style17">
                                <h3>Exporting Server data to Excel sheet</h3>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" dir="ltr" class="style17">
                                <asp:Label ID="Label1" runat="server" Style="font-weight: 700"
                                    Text="Select  "></asp:Label>
                            </td>
                            <td align="left" dir="ltr" class="style17">
                                <asp:TextBox ID="txt_column" runat="server" Height="200px" Width="500px" TextMode="MultiLine"></asp:TextBox>
                            </td>
                           
                            <td class="style18">
                                <asp:Label ID="lbl_condition" runat="server" Text="File Name"
                                    Style="font-weight: 700"></asp:Label>
                            </td>
                            <td class="style15">
                                <asp:TextBox ID="txt_FileName" runat="server" TabIndex="5" Height="53px"
                                    TextMode="MultiLine" Width="294px"></asp:TextBox>
                            </td>
                            <td class="style15">
                                <asp:Button ID="Btn_GO" runat="server"
                                    BackColor="White" Height="22px"
                                    Style="font-weight: 700" Text="GO" Width="49px" OnClick="Btn_GO_Click"
                                    TabIndex="3" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" dir="ltr" class="style17" colspan="7">
                                <asp:Label ID="lbl_select_msg" runat="server"
                                    Style="font-weight: 700; color: #CC3300"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:GridView ID="GridView_Result" runat="server" AutoGenerateColumns="False" EnableViewState="false"
            Width="944px">
         <EmptyDataTemplate></EmptyDataTemplate></asp:GridView>
        <br />


        <table class="style11" align="center" border="1" dir="ltr">
            <tr>
                <td align="center" colspan="3">
                    <b>Inserting data to server </b></td>
            </tr>
            <tr>
                <td align="left" dir="ltr" class="style17">
                    <asp:Label ID="lbl_condition0" runat="server" Text="write insert statement  :"
                        Style="font-weight: 700"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="txt_insert_statement" runat="server" TabIndex="5" Height="200px"
                        TextMode="MultiLine" Width="500px"></asp:TextBox>
                </td>
                <td class="style15">
                    <asp:Button ID="Btn_GO_insert" runat="server"
                        BackColor="White" Height="22px"
                        Style="font-weight: 700" Text="Insert" Width="60px" OnClick="Btn_GO_insert_Click"
                        TabIndex="3" />
                </td>
            </tr>
            <tr>
                <td align="center" dir="ltr" class="style17" colspan="3">
                    <asp:Label ID="lbl_insert_msg" runat="server"
                        Style="font-weight: 700; color: #CC3300"></asp:Label>
                </td>
            </tr>
        </table>
        <br />


        <table align="center" border="1" dir="ltr">
            <tr>
                <td align="center" colspan="3">
                    <b>Updating table data to server</b></td>
            </tr>
            <tr>
                <td align="left" dir="ltr" class="style17">
                    <asp:Label ID="lbl_condition1" runat="server" Text="write update condition  :"
                        Style="font-weight: 700"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="txt_update" runat="server" TabIndex="5" Height="200px"
                        TextMode="MultiLine" Width="500px"></asp:TextBox>
                </td>
                <td class="style15">
                    <asp:Button ID="Btn_Update" runat="server"
                        BackColor="White" Height="22px"
                        Style="font-weight: 700" Text="Update" Width="60px" OnClick="Btn_Update_Click"
                        TabIndex="3" />
                </td>
            </tr>
            <tr>
                <td align="center" dir="ltr" class="style17" colspan="3">
                    <asp:Label ID="lbl_update_msg" runat="server"
                        Style="font-weight: 700; color: #CC3300"></asp:Label>
                </td>
            </tr>
        </table>
        <br />


        <table align="center" border="1" dir="ltr">
            <tr>
                <td align="center" colspan="3">
                    <b>Deleting table data to server</b></td>
            </tr>
            <tr>
                <td align="left" dir="ltr" class="style17">
                    <asp:Label ID="lbl_condition2" runat="server" Text="write delete condition  :"
                        Style="font-weight: 700"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="txt_delete" runat="server" TabIndex="5" Height="200px"
                        TextMode="MultiLine" Width="500px"></asp:TextBox>
                </td>
                <td class="style15">
                    <asp:Button ID="Btn_delete" runat="server"
                        BackColor="White" Height="22px"
                        Style="font-weight: 700" Text="Delete" Width="60px" OnClick="Btn_delete_Click"
                        TabIndex="3" />
                </td>
            </tr>
            <tr>
                <td align="center" dir="ltr" class="style17" colspan="3">
                    <asp:Label ID="lbl_delete_msg" runat="server"
                        Style="font-weight: 700; color: #CC3300"></asp:Label>
                </td>
            </tr>
        </table>
        <br />


        <table align="center" border="1" dir="ltr">
            <tr>
                <td align="center" colspan="3">
                    <b>Selecting table data of server </b></td>
            </tr>
            <tr>
                <td align="left" dir="ltr" class="style17">
                    <asp:Label ID="lbl_condition3" runat="server" Text="write select  condition  :"
                        Style="font-weight: 700"></asp:Label>
                </td>
                <td class="style15">
                    <asp:TextBox ID="txt_select_new" runat="server" TabIndex="5" Height="200px"
                        TextMode="MultiLine" Width="500px"></asp:TextBox>
                </td>
                <td class="style15">
                    <asp:Button ID="Btn_select_new" runat="server"
                        BackColor="White" Height="22px"
                        Style="font-weight: 700" Text="Select " Width="60px" OnClick="Btn_select_new_Click"
                        TabIndex="3" />
                </td>
            </tr>
            <tr>
                <td align="center" dir="ltr" class="style17" colspan="3">
                    <asp:Label ID="lbl_select_new_msg" runat="server"
                        Style="font-weight: 700; color: #CC3300"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
    </div>
    <br />
    <asp:GridView ID="grd_select" runat="server">
     <EmptyDataTemplate></EmptyDataTemplate></asp:GridView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
