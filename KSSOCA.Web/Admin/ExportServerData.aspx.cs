﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KSSOCA.Core.Security; 
using System.Collections;
using System.Configuration;
using System.Data; 
using System.Web.Security; 
using System.Web.UI.HtmlControls; 
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace KSSOCA.Web.Admin
{
    public partial class ExportServerData : SecurePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void getTableList()
        {
            // ddlBind.dropdownbind("SELECT name  FROM sys.Tables ", ddl_Tables, "name", "name");
        }
        protected DataTable getdata()
        {
            DataTable dt = new DataTable();
            try
            {

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                string st = string.Format(@" select " + txt_column.Text.Trim()  );

                SqlCommand cmd1 = new SqlCommand(st, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd1);

                da.Fill(dt);
                foreach (DataColumn col in dt.Columns)
                {
                    BoundField field = new BoundField();
                    field.DataField = col.ColumnName;
                    field.HeaderText = col.ColumnName;
                    GridView_Result.Columns.Add(field);
                }
                if (dt.Rows.Count > 0)
                {
                    GridView_Result.DataSource = dt;
                    GridView_Result.DataBind();
                }
            }
            catch (Exception ex)
            {
                // Exception_Catching.Catch_error("Export_Server_Data.aspx", " Getdata()", ex.ToString());
            }
            return dt;
        }



        private void ExporttoExcel(DataTable table)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + txt_FileName.Text.ToString() + ".xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = GridView_Result.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(GridView_Result.Columns[j].HeaderText.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
        protected void Btn_GO_Click(object sender, EventArgs e)
        {
            clear_labels();

            if (txt_column.Text != string.Empty)
            {
                ExporttoExcel(getdata());
            }
            else
            {
                lbl_select_msg.Text = "Please enter some value after select ";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBox", "alert('Please enter some value after select ');", true);
            }

            Btn_GO.Focus();
        }
        protected void Btn_GO_insert_Click(object sender, EventArgs e)
        {
            clear_labels();
            if (txt_insert_statement.Text != string.Empty)
            {
                try
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString);

                    string qry = txt_insert_statement.Text.Trim();
                    SqlCommand cmd = new SqlCommand(qry, con);
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    int RowAffected = cmd.ExecuteNonQuery();
                    if (RowAffected > 0)
                    {
                        lbl_insert_msg.Text = "inserted Succesfully  " + RowAffected + "  rows";
                        txt_insert_statement.Text = string.Empty;
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    lbl_insert_msg.Text = ex.ToString();
                    //Exception_Catching.Catch_error("Export_Server_Data.aspx", " Btn_go_insert_Click()", ex.ToString());
                }
            }
            else
            {
                lbl_insert_msg.Text = "Please enter insert statement";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBox", "alert('Please enter insert statement  ');", true);
            }

            Btn_GO_insert.Focus();
        }

        public void clear_labels()
        {
            lbl_update_msg.Text = string.Empty;
            lbl_insert_msg.Text = string.Empty;
            lbl_select_msg.Text = string.Empty;
            lbl_delete_msg.Text = string.Empty;
            lbl_select_new_msg.Text = string.Empty;

        }
        protected void Btn_Update_Click(object sender, EventArgs e)
        {
            clear_labels();

            if (txt_update.Text != string.Empty)
            {

                try
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString);

                    string qry = txt_update.Text.Trim();
                    SqlCommand cmd = new SqlCommand(qry, con);
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    int RowAffected = cmd.ExecuteNonQuery();
                    if (RowAffected > 0)
                    {
                        lbl_update_msg.Text = "Updated Succesfully  " + RowAffected + "  rows";
                        txt_update.Text = string.Empty;
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    lbl_update_msg.Text = ex.ToString();
                    //  Exception_Catching.Catch_error("Export_Server_Data.aspx", " Btn_Update_Click()", ex.ToString());
                }
            }
            else
            {
                lbl_update_msg.Text = "Please enter update statement";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBox", "alert('Please enter update statement  ');", true);
            }
            Btn_Update.Focus();
        }
        protected void Btn_delete_Click(object sender, EventArgs e)
        {
            clear_labels();

            if (txt_delete.Text != string.Empty)
            {

                try
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString);

                    string qry = txt_delete.Text.Trim();
                    SqlCommand cmd = new SqlCommand(qry, con);
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    int RowAffected = cmd.ExecuteNonQuery();
                    if (RowAffected > 0)
                    {
                        lbl_delete_msg.Text = "Deleted  Succesfully  " + RowAffected + "  rows";
                        txt_delete.Text = string.Empty;
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    lbl_delete_msg.Text = ex.ToString();
                    // Exception_Catching.Catch_error("Export_Server_Data.aspx", " Btn_Update_Click()", ex.ToString());
                }
            }
            else
            {
                lbl_delete_msg.Text = "Please enter delete  statement ";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBox", "alert('Please enter update statement  ');", true);
            }
            Btn_delete.Focus();
        }
        protected void Btn_select_new_Click(object sender, EventArgs e)
        {
            clear_labels();
            if (txt_select_new.Text != string.Empty)
            {
                try
                {
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString);

                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    string st = txt_select_new.Text.Trim();

                    SqlCommand cmd1 = new SqlCommand(st, con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd1);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        grd_select.Visible = true;
                        grd_select.DataSource = dt;
                        grd_select.DataBind();
                    }
                    else
                    {
                        grd_select.Visible = false;
                        grd_select.DataSource = null;
                        lbl_select_new_msg.Text = "No records selected";
                    }
                    con.Close();
                }
                catch (Exception ex)
                {
                    lbl_select_new_msg.Text = ex.ToString();
                }
            }
            else
            {
                lbl_select_new_msg.Text = "Please enter select statement ";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MessageBox", "alert('Please enter select statement  ');", true);
            }

            Btn_select_new.Focus();
        }

    }
}