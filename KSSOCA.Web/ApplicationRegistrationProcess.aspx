﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationRegistrationProcess.aspx.cs" Inherits="KSSOCA.Web.ApplicationRegistrationProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="col-lg-12">
            <h1>INSTRUCTIONS AND PROCEDURES FOR ONLINE APPLICATION FOR REGISTRATION OF SEED PRODUCERS/FIRM/INSTITUTION</h1>
        </div>
        <div class="col-lg-12">
            <ul>
                <li>The Seed Producers should organize minimum 50Acres production program per annum.</li>
                <li>The Seed Producers should enclose VAT-7 Form used by competitive authority.</li>
                <li>Seed Producers should offer his seed for physical verification with KSSOCA official before distribution for Seed Production.</li>
                <li>Before submission of form No.1's the seed producers should ensure the seed grower has used seed from the verified source only for production.</li>
                <li>Seed Producers should advise his seed grower about seed production techniques, isolation distance to be maintained etc., also to keep the documents like bills, certification Tags, Bags safely and produce the same at the time of inspection by KSSOCA Officials.</li>
                <li>The Seed Producers should visit the seed field in advanced & confirm the exact location, date of sowing, Area sown & Isolation distance of the seed plots before submission of form No.1's.</li>
                <li>The Seed Produces should show the seed plot to the Seed Certification Officer for conducting the first inspection in time.</li>
                <li>Seed Producers/Seed Grower this representative should be present at the time processing sampling & final certification work.</li>
            </ul>
        </div>
        <div class="col-lg-12 text-center"><a style="color:#0E8988; background-color:#E7BC0E; padding:10px;" href="UserRegistration.aspx">Simple steps to Apply Online</a></div>
        <div class="col-lg-12">
            <ul>
                <li>Go through sample application.</li>
                <li>Complete the Registration and Fill the online application form.</li>
                <li>Upload photo, signature and left hand thumb impression & self-declaration.</li>
                <li>Download E-challan and make payment.</li>
                <li>After Confirmation of Payment, Download Final Application form.</li>
            </ul>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
