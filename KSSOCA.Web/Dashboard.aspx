﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="KSSOCA.Web.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div align="center">
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                    <asp:Button ID="btnEdit" runat="server" CssClass="btn-primary" Text="Update Details" PostBackUrl="~/Masters/UserMasterCreate.aspx" CausesValidation="false" />
                </div>
                <br />
                <table class="table-condensed" border="1">
                    <tr>
                        <td>Role</td>
                        <td><asp:Label ID="lblrole" runat="server"></asp:Label></td>
                    </tr>
                    <%--<tr>
                        <td>Registration No.</td>
                        <td><asp:Label ID="lblRegNo" runat="server"></asp:Label></td>
                    </tr>--%>
                    <tr>
                        <td>Name</td>
                        <td><asp:Label ID="lblName" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Mobile No</td>
                        <td><asp:Label ID="lblmobile" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>Email ID</td>
                        <td><asp:Label ID="lblemail" runat="server"></asp:Label></td>
                    </tr>
                    <asp:Panel ID="pnlAuthority" runat="server" Visible="false">
                        <tr>
                            <td>Reporting Manager</td>
                            <td><asp:Label ID="lblreportingid" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Districts and Taluks authorised to you</td>
                            <td> 
                                <asp:GridView ID="gvdistricts" runat="server" class="table" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl.No.">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %> .
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="District" HeaderText="District" />
                                        <asp:BoundField DataField="Taluk" HeaderText="Taluk" />
                                    </Columns>
                                 <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
            </div>

        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
