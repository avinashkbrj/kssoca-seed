﻿namespace KSSOCA.Web
{
    using KSSOCA.Core.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;

    public partial class Dashboard : SecurePage
    {
        TalukMasterService talukService;
        DistrictMasterService districtService;
        UserMasterService userMasterService;
        RoleMasterService roleMasterService;
        Common common = new Common();
        public Dashboard()
        {
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
            userMasterService = new UserMasterService();
            roleMasterService = new RoleMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    NameValueCollection nvc = Request.Form;
                    var abc = Page.Request;
                    string nvcvalue = Request.Form["encdata"];
                    if (Request.UrlReferrer != null)
                    {
                        string paymentUrl = Request.UrlReferrer.ToString();
                        if (paymentUrl != null && paymentUrl.Contains("Status"))
                        {
                            int statusIndex = paymentUrl.IndexOf("Status");
                            string status = paymentUrl.Substring(statusIndex, 8);
                            char statusKeyword = status.Last();
                            char statusKeyword1 = status[7];
                            if (statusKeyword1 == 'S')
                            { }
                        }
                        else
                        {
                            NewMethod();
                        }
                    }
                    else
                    {
                        NewMethod();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void NewMethod()
        {
            lblHeading.Text = common.getHeading("DB");
            var user = userMasterService.GetFromAuthCookie();
            lblemail.Text = user.EmailId;
            //lblRegNo.Text = user.Reg_no;
            lblmobile.Text = user.MobileNo;
            lblName.Text = user.Name;
            lblrole.Text = roleMasterService.GetById(Convert.ToInt16(user.Role_Id)).Name;
            lblreportingid.Text = user.Reporting_Id != null ? userMasterService.GetById(Convert.ToInt16(user.Reporting_Id)).Name : "";
            var user1 = userMasterService.GetFromAuthCookie();
        }
    }
}