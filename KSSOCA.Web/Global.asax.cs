﻿using KSSOCA.Core.Data;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace KSSOCA.Web
{
    public class Global : System.Web.HttpApplication
    {
        UserMasterService userService;

        public Global()
        {
            userService = new UserMasterService();
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            //RouteTable.Routes.MapHttpRoute(
            //   name: "DefaultApi",
            //   routeTemplate: "api/{controller}/{id}",
            //   defaults: new { id = System.Web.Http.RouteParameter.Optional }
        // );
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpContext currentContext = HttpContext.Current;
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        FormsIdentity id = HttpContext.Current.User.Identity as FormsIdentity;
                        UserMaster user = userService.GetFromAuthCookie();
                        HttpContext.Current.User = new GenericPrincipal(id, new string[] { user.Role_Id.ToString() });
                    }
                }
            }
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var serverException = Server.GetLastError();
            
            if (serverException is HttpUnhandledException)
                Server.Transfer("~/PageNotFound.aspx" , true);

            else
                Server.Transfer("~/Error.aspx?id="+ serverException.Message, true);
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Session.Abandon();

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}