﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="KSSOCA.Web.Home" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <table width="100%" height="100%"> 
            <tr>
                <td width="50%" align="center" >
                    <asp:Image ID="Image1" runat="server" Height="400" Width="98%" class="img-thumbnail" Style="box-shadow: 5px 5px  10px 5px #A1A1A1;" ImageUrl="~/img/field inspection.jpg" />
                    <cc1:SlideShowExtender ID="SlideShowExtender" runat="server" TargetControlID="Image1"
                        SlideShowServiceMethod="GetImages" ImageTitleLabelID="lblImageTitle" ImageDescriptionLabelID="lblImageDescription"
                        AutoPlay="true" PlayInterval="2000" Loop="true">
                    </cc1:SlideShowExtender>
                </td>
                <td width="50%">
                    <asp:Panel ID="pnlhistory" runat="server" Height="400" Width="100%" class="img-thumbnail" Style="box-shadow: 5px 5px  10px 5px #A1A1A1; overflow-y: auto; overflow-x: hidden; padding: 5px 5px 5px  5px; text-align: justify">
                        &nbsp; &nbsp;
                        <span class="MainHeading" align="center">Welcome to KSSOCA </span>
                        <br />
                        <br />
                        Karnataka State Seed Certification Agency (KSSCA), was established in the year 1974 under Section-8 of the Seeds Act 1966 as an autonomous body and registered under the Karnataka Societies Registration Act 1960 with the objectivies of regulating the quality of seeds certified in the State.The Organization became Operational in January 1975. Since there is Scope for increasing the activities,a new division viz. 
                        <br />
                        <br />
                        Organic Certification Division has been established in the year 2013 (04.01.2013) and the Organisation is renamed as Karnataka State Seed and the Organisation is renamed as "Karnataka State Seed and Organic Certification Agency"(KSSOCA). 
                        <br />
                        <br />
                        There has been an uptrend in the Certification Activities year after year and the Area Registered & the quantity of Seeds Certified has increased from 6315 ha. & 56179 qtls.in 1974 to 53098.00 ha. & 382594.06 qtls. in 2016-2017
                        <br />
                        <br />
                        Seed Certification is a legally sanctioned system designed to secure, maintain and make available certain prescribed standards of genetic identity and purity, physical purity, physiological quality and seed health of notified kinds and varieties including vegetatively propagating materials of superior crop plant varieties.
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <marquee onmousedown="this.stop();" onmouseup="this.start();"> <asp:Image ID="imgstar" runat="server" Width="20px" Height="20px" ImageUrl="~/img/star.gif" />&nbsp;<a style="color: #0E8988;" href="UserRegistration.aspx">On-line Application for the registration of Seed Producing Firm/Seed Processing Unit </a> </marquee>
                    <p></p>
                </td>
            </tr> 
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="50%" align="center">
                    <asp:Panel ID="Panel1" runat="server" Height="300" Width="98%" class="img-thumbnail" Style="overflow-y: auto; overflow-x: hidden; padding: 5px 5px 5px  5px; text-align: left; box-shadow: 5px 5px  10px 5px #A1A1A1;">
                        &nbsp; &nbsp;<span class="MainHeading" align="center">Stages of Seed Certification</span><br />
                        <br />
                        &nbsp; &nbsp;1.Source Verification of Seeds<br />
                        &nbsp; &nbsp;2.Reciept and Scrutiny of Form-I<br />
                        &nbsp; &nbsp;3.Field inspection<br />
                        &nbsp; &nbsp;4.Seed processing,upgrading and sampling<br />
                        &nbsp; &nbsp;5.Seed quality analysis and genetic purity evaluation<br />
                        &nbsp; &nbsp;6.Seed treatment<br />
                        &nbsp; &nbsp;7.Bagging and Tagging<br />
                        &nbsp; &nbsp;8.Grant of certificate<br />
                        &nbsp; &nbsp;9.Issue of form-II(Release order)<br />
                        &nbsp; &nbsp;10.Quality seeds to farmers
                    </asp:Panel>
                </td> 
                <td width="50%" align="center">
                    <asp:Panel ID="Panel2" runat="server" Height="300" Width="98%" class="img-thumbnail" Style="box-shadow: 5px 5px  10px 5px #A1A1A1; overflow-y: auto; overflow-x: hidden; padding: 5px 5px 5px  5px; text-align: left; text-align: justify">
                        &nbsp; &nbsp;<span class="MainHeading" align="center">Goals and Objectives</span><br />
                        <br />
                        Goals
                                <br />
                        1.To provide quality certified seeds to the farmers to increase production & productivity
                                <br />
                        2.Increase area under seed production and increase the availability of certified seeds to the farmer hereby increasing the seed replacement rate(SSR)
                                <br />
                        3.Inspect seed processing plants to see that the admixtures of other kind and varieties are not introduced
                                <br />
                        4.Reduce the percentage of failure of seeds at field,unit as wel as lab level
                                <br />
                        5.Improve the infrastructure facilities of KSSCA for effective and timely certification activities.
                                <br />
                        <br />
                        Objectives
                                <br />
                        <br />
                        1.Certify seeds of any notified kinds or varieties(under Section-5 of Seeds Act 1966)<br />
                        2.Maintain a list of sources of Breeder & Foundation seeds approved by the Central Seed Certification Board;<br />
                        3.Outline the procedure for the submission of appliction for growing,harvesting,processing,storage,labelling and tagging of seeds intended for certification till the end,So as to ensure that seed lot's finally approved for certification are true to variety.
                                <br />
                        4.Verify,upon receipt of an application for certification that variety is eligible for certification,that the seed used for planting is from approved source and the application has been submitted in accordance with the procedure priscribed for it.
                                <br />
                        5.Inspect Seed feilds to ensure that the minimum standards for isolation,rouging,use of male sterility(wherevere applicable) and similar factor are maintained at all times,as well as to ensure that those provided in the standards for certification;
                                <br />
                        6.Inspect seed processing plants to see that the admixtures of other kind and varieties are not introduced;
                                <br />
                        7.Inspect seed lot's produced and take sample as per procedure and have such sample tested to ensure that the seed conforms to the pricribed standards for certification;
                                <br />
                        8.Grant Certificates(including tags,Form-II etc...,)in accordance with the provision of the Seed Act;
                                <br />
                        9.Ensure that action at all stages of seed certification is taken expeditiously. Undertake educational programmes designed to promote the use of certified seeds,including publication listing certified seed growers and source of certified seed.
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr> 
            <tr>

                <td width="50%" align="center" >
                    <asp:Panel ID="Panel4" runat="server" Height="300" Width="98%" class="img-thumbnail" Style="box-shadow: 5px 5px  10px 5px #A1A1A1; overflow-y: auto; overflow-x: hidden; padding: 5px 5px 5px  5px; text-align: justify">
                        &nbsp; &nbsp;<span class="MainHeading" align="center">Important Links</span>
                        <br />
                        <br />
                        <asp:LinkButton ID="link1" runat="server" Font-Bold="true" ForeColor="Navy" Text="1.Seed Net India Portal" PostBackUrl="http://seednet.gov.in/"></asp:LinkButton>

                        <br />
                        <asp:LinkButton ID="link2" runat="server" Font-Bold="true" ForeColor="Navy" Text="2.Agriculture Co-Operation and Farmer Welfare" PostBackUrl="http://agricoop.nic.in/divisiontype/seeds"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkButton1" Font-Bold="true" ForeColor="Navy" runat="server" Text="3.krishi marata vahini" PostBackUrl="https://www.krishimaratavahini.kar.nic.in/"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkButton2" Font-Bold="true" ForeColor="Navy" runat="server" Text="" PostBackUrl=""></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkButton3" Font-Bold="true" ForeColor="Navy" runat="server" Text="" PostBackUrl=""></asp:LinkButton>
                    </asp:Panel>
                </td>
            
                <td width="50%" align="center">
                    <asp:Panel ID="Panel3" runat="server" Height="300" Width="98%" class="img-thumbnail" Style="overflow-y: auto; overflow-x: hidden; padding: 5px 5px 5px  5px; box-shadow: 5px 5px  10px 5px #A1A1A1; text-align: left">
                        &nbsp; &nbsp;<span class="MainHeading" align="center">Latest News / Circular</span><br />
                        <br />
                        1. KSSOCA new building opening ceremony on 01.03.2018<br />
                        2. KSSOCA Online Seed Certification launching on 01.03.2018
                    </asp:Panel>
                </td>
            </tr> 
        </table>
    </div>
</asp:Content>
