﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Launch1.aspx.cs" Inherits="KSSOCA.Web.Launch1" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <title></title>
    <link href="lib/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="lib/bootstrap/css/bootstrap-gridview.css" rel="stylesheet">
    <link href="lib/bootstrap/css/animate.css" rel="stylesheet">
    <style type="text/css">
        .blink {
            width: 200px;
            height: 50px;
            padding: 15px;
            text-align: center;
            line-height: 50px;
            text-shadow: 6px 6px 8px green, 0 0 25px white, 0 0 5px #808080;
        }

        span {
            font-family: Cambria;
            font-size:50pt;
            font-weight: bold;
            color: white;
            animation: blink 10s linear infinite;
        }

        @keyframes blink {
            0% {
                opacity: 0.3;
            }

            40% {
                opacity: 0.6;
            }

            100% {
                opacity: 1;
            }
        }
    </style>

</head>
<body style="background-image: url(../img/glitter7.gif); background-repeat: no-repeat; background-size: 100% 150%;">
    <form id="form1" runat="server">
        <div align="center">
            <br /><audio src="img/Backgroundmusic.mp3" autoplay="autoplay" loop="loop"></audio>
            <br />
            <table>
                <tr>
                    <td align="center" style="vertical-align: middle;" colspan="2"><span class="blink"> 
                        <br />
                        ಕರ್ನಾಟಕ ರಾಜ್ಯ ಬೀಜ ಮತ್ತು ಸಾವಯವ ಪ್ರಮಾಣನ ಸಂಸ್ಥೆ  
                        <br /><br />
                        ಪ್ರಮಾಣೀಕರಣ ತಂತ್ರಾಂಶ ಬಿಡುಗಡೆ ಸಮಾರಂಭ  - 01.03.2018
                    </span></td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;" align="center">
                        <asp:Image ID="imgbuilding" runat="server" Height="500px" Width="800px" CssClass="img-circle" ImageUrl="~/img/BL P.jpg" /></td>
                    <td align="right" style="vertical-align: bottom;">
                        <asp:ImageButton ID="imgclick" runat="server" PostBackUrl="~/Launch2.aspx" ImageUrl="~/img/glitter1.gif" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
