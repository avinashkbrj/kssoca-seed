﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Launch2.aspx.cs" Inherits="KSSOCA.Web.Launch2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .blink {
            width: 200px;
            height: 50px;
            padding: 15px;
            text-align: center;
            line-height: 50px;
            text-shadow: 3px 3px 4px white, 0 0 25px white, 0 0 5px #808080;
        }

        span {
            font-family: Cambria;
            font-size: 28pt;
            font-weight: bold;
            color: white;
            animation: blink 5s linear infinite;
        }

        @keyframes blink {
            0% {
                opacity: 0.3;
            }

            50% {
                opacity: .8;
            }

            100% {
                opacity: 1;
            }
        }
    </style>
</head>
<body style="background-image: url(../img/glitter3.gif); background-repeat: no-repeat; background-size: 100% 150%;">
    <form id="form1" runat="server">
        <div align="center">
            <br />
            <br />
            <span class="blink">Launching KSSOCA Online Seed Certification Website in few Seconds...</span>
        </div>
        <br />
        <div align="center" style="min-height: 700px" width="100%">
            <video width="1" height="1" controls autoplay >
                <source src="img/ezgif-5-efeaf89192.mp4" type="video/mp4">
            </video>
        </div>
    </form>
</body>
</html>
