﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="KSSOCA.Web.Login" %>

<%@ Register Assembly="Recaptcha.Web" Namespace="Recaptcha.Web.UI.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <br />
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <table class="table-condensed" border="1">
                    <tr class="Note">
                        <td colspan="4"> <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            Fields marked with <span style="color: red">*</span> must be filled.
                        </td>
                    </tr>
                    <tr>
                        <td>User Name<span style="color: red">*</span>

                        </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Username" ValidationGroup="SignIn" placeholder="User Name"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="UserNameRequired" ValidationGroup="SignIn" CssClass="text-danger" Display="Dynamic"
                                ControlToValidate="Username" ErrorMessage="Username can't be empty"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr>
                        <td>Password<span style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Password" ValidationGroup="SignIn" TextMode="Password" placeholder="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="PasswordRequired" ValidationGroup="SignIn" CssClass="text-danger" Display="Dynamic"
                                ControlToValidate="Password" ErrorMessage="Password can't be empty"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:CheckBox runat="server" ID="remember" ValidationGroup="SignIn" Text="Remember password" />

                            <%--    <p class="help-block">(if this is a private computer)</p>--%>
                            <br />
                            <asp:Button runat="server" ID="LoginButton" ValidationGroup="Log In"  CssClass="btn-primary"
                                OnClick="LoginButton_Click" Text="Login"></asp:Button>
                        </td>

                    </tr>

                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
