﻿
namespace KSSOCA.Web
{
    using System;
    using System.Web.UI;
    using KSSOCA.Core.Data;
    using System.Data.SqlClient;
    using System.Configuration;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using Recaptcha.Web;

    public partial class Login : System.Web.UI.Page
    {
        UserMasterService userMaster;
        string constr = "";
        Common common = new Common();
        public Login()
        {
            userMaster = new UserMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblHeading.Text = common.getHeading("LOGIN");
            if (Context.User.Identity.IsAuthenticated)
            {
                this.Redirect("Dashboard.aspx");
            }
        }
        protected void LoginButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    //if (String.IsNullOrEmpty(Recaptcha1.Response))
                    //{
                    //    lblMessage.Text = "Captcha cannot be empty.";
                    //}
                    //else
                    //{
                    //    RecaptchaVerificationResult result = Recaptcha1.Verify();

                    //    if (result == RecaptchaVerificationResult.Success)
                    //    {

                    if (userMaster.ValidateLogin(Username.Text, Password.Text, remember.Checked))
                    {
                        this.Redirect("Dashboard.aspx");
                    }
                    else
                    {
                        lblMessage.Text = "Invalid Credential. Please try again.";
                    }
                    //    }
                    //    else if (result == RecaptchaVerificationResult.IncorrectCaptchaSolution)
                    //    {
                    //        lblMessage.Text = "Incorrect captcha response.";
                    //    }
                    //    else
                    //    {
                    //        lblMessage.Text = "Some other problem with captcha.";
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    ApplicationExceptionHandler.Handle(ex);

                    lblMessage.Text = ex.ToString();

                    //lblMessage.Text = "Error occurred. Please contact administrator for more details.";
                }
            }
        }
    }
}