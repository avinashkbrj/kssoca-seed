﻿namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using System.Web.UI.WebControls;
    using Core.Helper;

    public partial class AddBranch : SecurePage
    {
        RoleMasterService roleMasterService;
        UserMasterService userMasterService;
        Common common = new Common();
        public AddBranch()
        {
            roleMasterService = new RoleMasterService();
            userMasterService = new UserMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearControl("C");
        }
        private void InitComponent()
        {
            BindGrid();
            var user = userMasterService.GetFromAuthCookie();
            lblname.Text = user.Name;
        }
        private void BindGrid()
        {
            var user = userMasterService.GetFromAuthCookie();
            DataTable data = userMasterService.GetByReportingOfficer(user.Id, 5).ToDataTable();
            CropMasterGridView.DataSource = data;
            CropMasterGridView.DataBind();
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var usermaster = new UserMaster()
                    {
                        Id = common.GenerateID("UserMaster"),
                        Name = lblname.Text + "-" + Name.Text.Trim(),
                        RegistrationDate = System.DateTime.Now,
                        District_Id = user.District_Id,
                        Taluk_Id = user.Taluk_Id,
                        MobileNo = MobileNumber.Text.Trim(),
                        Role_Id = user.Role_Id,
                        EmailId = EmailId.Text.Trim().ToLower(),
                        Password = Password.Text.Trim(),
                        IsActive = true,
                        UserName = txtusename.Text.Replace(" ", String.Empty).ToLower(),
                        Division_Id = user.Division_Id,
                        Reporting_Id = user.Id,
                    };
                    int result = userMasterService.Register(usermaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("S");
                        lblMessage.Text = "Branch Saved.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save Branch.";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message+ex.InnerException;
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void ClearControl(string ActionType)
        {
            if (ActionType == "U")
            {
                Name.Text = "";
                Name.Text = "";
                txtusename.Text = "";
                EmailId.Text = "";
                MobileNumber.Text = "";
                Password.Text = "";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }
            else if (ActionType == "S")
            {
                Name.Text = "";
                Name.Text = "";
                txtusename.Text = "";
                EmailId.Text = "";
                MobileNumber.Text = "";
                Password.Text = "";
            }
            else if (ActionType == "C")
            {
                Name.Text = "";
                txtusename.Text = "";
                EmailId.Text = "";
                MobileNumber.Text = "";
                Password.Text = "";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }

        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            int deletedRowCount = userMasterService.Delete(id);

            if (deletedRowCount > 0)
            {
                BindGrid();
                lblMessage.Text = "Branch deleted.";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var usermaster = new UserMaster()
                    {
                        Id = hdnId.Value.ToInt(),
                        Name = Name.Text.Trim(),
                        RegistrationDate = System.DateTime.Now,
                        District_Id = user.District_Id,
                        Taluk_Id = user.Taluk_Id,
                        MobileNo = MobileNumber.Text.Trim(),
                        Role_Id = user.Role_Id,
                        EmailId = EmailId.Text.Trim().ToLower(),
                        Password = Password.Text.Trim(),
                        IsActive = true,
                        UserName = txtusename.Text.Replace(" ", String.Empty).ToLower(),
                        Division_Id = user.Division_Id,
                        Reporting_Id = user.Id,
                    };
                    int result = userMasterService.Update(usermaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("U");
                        lblMessage.Text = "Branch updated.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to update Branch.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            hdnId.Value = id.ToString();
            var var = userMasterService.GetById(id);
            Name.Text = var.Name;
            MobileNumber.Text = var.MobileNo;
            EmailId.Text = var.EmailId;
            txtusename.Text = var.UserName;
            Save.Visible = false;
            btnUpdate.Visible = true;
            Cancel.Visible = true;
        }
        protected void MobileNumber_TextChanged(object sender, EventArgs e)
        {
            if (Utility.IsValidMobileNo(MobileNumber.Text))
            {
                bool IsMobileNumberExist = userMasterService.IsMobileNumberExist(MobileNumber.Text);
                if (IsMobileNumberExist)
                {
                    MobileNumber.Text = "";
                    lblMessage.Text = "This mobile number already exist please try another.";
                }
            }
            else
            {
                MobileNumber.Text = "";
                lblMessage.Text = "Enter valid mobile number.";
            }
        }
        protected void txtusename_TextChanged(object sender, EventArgs e)
        {
            bool IsUserNameExist = userMasterService.IsUserNameExist(txtusename.Text);
            if (IsUserNameExist)
            {
                txtusename.Text = "";
                lblMessage.Text = "This user name already exist please try another.";
            }
        }
    }

}