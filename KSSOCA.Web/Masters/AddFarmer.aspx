﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddFarmer.aspx.cs" Inherits="KSSOCA.Web.Masters.AddFarmer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" align="center">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="Farmer/Grower Registration"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Green"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnId" />
            <asp:HiddenField ID="hdnPhoto" runat="server" />
            <asp:HiddenField ID="hdnPhani" runat="server" />
            <asp:HiddenField ID="hdnAdharCard" runat="server" />
            <asp:HiddenField ID="hdnBankPassBook" runat="server" />
        </div>
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr class="SideHeading">
                <td colspan="4">1.Grower Information 
                </td>
            </tr>
            <tr>
                <td width="25%">1.Name <span style="color: red">*</span></td>
                <td width="25%">
                    <asp:TextBox runat="server" CssClass="form-control" ID="NameOfSeedGrower" placeholder="Name of Seed Grower" />
                    <asp:RequiredFieldValidator runat="server" ID="NameOfSeedGrowerRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="NameOfSeedGrower" ErrorMessage="Name of Seed Grower can't be empty"></asp:RequiredFieldValidator>
                </td>
                <td width="25%">2.Father's name<span style="color: red">*</span></td>
                <td width="25%">
                    <asp:TextBox runat="server" CssClass="form-control" ID="FathersName" placeholder="Father Name" />
                    <asp:RequiredFieldValidator runat="server" ID="FathersNameRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="FathersName" ErrorMessage="Father Name can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>3.District</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredDistrict" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="District" ErrorMessage="District Required" InitialValue="0"></asp:RequiredFieldValidator>
                </td>

                <td>4.Taluk</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="Taluk" AutoPostBack="true" OnSelectedIndexChanged="Taluk_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredTaluk" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="Taluk" ErrorMessage="Taluk Required" InitialValue="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>5.Hobli<span style="color: red">*</span></td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlHobli" AutoPostBack="true" OnSelectedIndexChanged="ddlHobli_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredddlHobli" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="ddlHobli" ErrorMessage="Hobli Required" InitialValue="0"></asp:RequiredFieldValidator>

                </td>

                <td>6.Village<span style="color: red;">*</span></td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlvillage">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="Requiredddlvillage" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="ddlvillage" ErrorMessage="Village Required" InitialValue="0"></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td>7.Gram Panchayat<span style="color: red">*</span></td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="GramPanchayat" placeholder="Gram Panchayat" />
                    <asp:RequiredFieldValidator runat="server" ID="GramPanchayatRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="GramPanchayat" ErrorMessage="Gram Panchayat can't be empty"></asp:RequiredFieldValidator>
                </td>

                <td>8.Mobile no.<span style="color: red">*</span></td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="TelephoneOrMobileNumber" placeholder="Telephone / Moble No." onkeypress="return CheckNumber(event);" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="TelephoneOrMobileNumberRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="TelephoneOrMobileNumber" ErrorMessage="Telephone / Mobile No can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="MobileNumberMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[0-9]{10,10}$" ControlToValidate="TelephoneOrMobileNumber"
                        ErrorMessage="Please enter valid mobile number"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>9.Aadhaar number<span style="color: red">*</span></td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="AadhaarNumber" placeholder="Aadhaar Number" onkeypress="return CheckNumber(event);" MaxLength="12" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredAadhaarNumber" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="AadhaarNumber" ErrorMessage="Aadhaar Number can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="RegularExAadhaarNumber" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[0-9]{12,12}$" ControlToValidate="AadhaarNumber"
                        ErrorMessage="Please enter valid Adhaar number"></asp:RegularExpressionValidator>
                </td>

                <td>10.Bank name</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="BankName">
                    </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="RequiredBankName" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="BankName" ErrorMessage="Bank Name Required" InitialValue="0"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>11.IFSC number</td> 
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="IFSCNumber" placeholder="IFSC Number" />
                    <asp:RequiredFieldValidator runat="server" ID="IFSCNumberRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="IFSCNumber" ErrorMessage="IFSC Number can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="IFSCNumberRegExValidator" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$" ControlToValidate="IFSCNumber"
                        ErrorMessage="Please Enter valid IFSC Code"></asp:RegularExpressionValidator>
                </td>

                <td>12.Account number<span style="color: red">*</span></td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="AccountNumber" placeholder="Account Number" onkeypress="return CheckNumber(event);" />
                    <asp:RequiredFieldValidator runat="server" ID="AccountNumberRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="AccountNumber" ErrorMessage="Account Number can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">3.Documents to be uploaded
                </td>
            </tr>
            <tr>
                <td valign="top">1.Grower photo<span style="color: red">*</span> 
                    <asp:FileUpload runat="server" CssClass="form-control" ID="FarmerPhoto" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFarmerPhoto" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="FarmerPhoto" ErrorMessage="Grower Photo Required"></asp:RequiredFieldValidator>
                    <div>
                        <cst:FileSizeValidator runat="server" ID="FarmerPhotoFileSizeValidator" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="FarmerPhoto" ErrorMessage="File size must not exceed {0} KB">
                        </cst:FileSizeValidator>
                        <cst:FileTypeValidator ID="FarmerPhotoTypeValidator" runat="server"
                            ControlToValidate="FarmerPhoto"
                            ForeColor="Red" Display="Dynamic"
                            ErrorMessage="Allowed file types are {0}.">
                        </cst:FileTypeValidator>
                        <asp:Literal ID="LitFarmerPhoto" runat="server" EnableViewState="false"></asp:Literal>
                    </div>
                </td>

                <td valign="top">2.Grower adhar card<span style="color: red">*</span> 
                    <asp:FileUpload runat="server" CssClass="form-control" ID="AdharCard" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredAdharCard" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="AdharCard" ErrorMessage="Grower Adhar Card Required"></asp:RequiredFieldValidator>
                    <div>
                        <cst:FileSizeValidator runat="server" ID="FileSizeValidator1" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="AdharCard" ErrorMessage="File size must not exceed {0} KB">
                        </cst:FileSizeValidator>
                        <cst:FileTypeValidator ID="FarmerSignFileTypeValidator" runat="server"
                            ControlToValidate="AdharCard"
                            ForeColor="Red" Display="Dynamic"
                            ErrorMessage="Allowed file types are {0}.">
                        </cst:FileTypeValidator>
                        <asp:Literal ID="LitAdharCard" runat="server" EnableViewState="false"></asp:Literal>
                    </div>
                </td>
          
                <td valign="top">3.Grower land pahani letter(upload family tree in case of death)<span style="color: red">*</span> 
                    <asp:FileUpload runat="server" CssClass="form-control" ID="Phani" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredPhani" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="Phani" ErrorMessage="Phani document is Required"></asp:RequiredFieldValidator>
                    <div>
                        <cst:FileSizeValidator runat="server" ID="FileSizeValidator2" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="Phani" ErrorMessage="File size must not exceed {0} KB">
                        </cst:FileSizeValidator>
                        <cst:FileTypeValidator ID="PhaniFileTypeValidator" runat="server"
                            ControlToValidate="Phani"
                            ForeColor="Red" Display="Dynamic"
                            ErrorMessage="Allowed file types are {0}.">
                        </cst:FileTypeValidator>
                        <asp:Literal ID="LitPhani" runat="server" EnableViewState="false"></asp:Literal>
                    </div>
                </td>
                <td valign="top">4.Grower bank pass Book<span style="color: red">*</span> 
                    <asp:FileUpload runat="server" CssClass="form-control" ID="BankPassBook" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredBankPassBook" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="BankPassBook"  ErrorMessage="Bank Pass Book is Required"></asp:RequiredFieldValidator>
                    <div>
                        <cst:FileSizeValidator runat="server" ID="FileSizeValidator3" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="BankPassBook" ErrorMessage="File size must not exceed {0} KB">
                        </cst:FileSizeValidator>
                        <cst:FileTypeValidator ID="FileTypeValidator1" runat="server"
                            ControlToValidate="BankPassBook"
                            ForeColor="Red" Display="Dynamic"
                            ErrorMessage="Allowed file types are {0}.">
                        </cst:FileTypeValidator>
                        <asp:Literal ID="LitBankPassBook" runat="server" EnableViewState="false"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click"   />
                    &nbsp;
                     <asp:Button runat="server" CssClass="btn-primary"  
                         ID="btnUpdate" Text="Update" Visible="false" OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure you want to Update the Farmer/Grower ?');" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" Visible="false" /></td>
            </tr>
        </table>
        <br />
        <asp:GridView runat="server" ID="CropMasterGridView" Width="100%"
            AutoGenerateColumns="false" class="table-condensed">
            <Columns>
                <asp:TemplateField HeaderText="Sl.No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %> .
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Farmer/Grower Reg No." />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="MobileNo" HeaderText="Mobile No" />
                <asp:BoundField DataField="FatherName" HeaderText="Father Name" />
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkUpdate" Text="Update" CausesValidation="false" CssClass="btn-default" CommandArgument='<%#Eval("Id")%>' OnClick="lnkUpdate_Click"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkDelete" Text="Delete" CausesValidation="false" CommandArgument='<%#Eval("Id")%>' OnClick="lnkDelete_Click"
                            OnClientClick="return confirm('Are you sure you want to delete?');" Enabled="false"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>-------------Records Not Found-------------------</EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
