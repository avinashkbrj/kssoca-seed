﻿namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using System.Web.UI.WebControls;
    using Core.Helper;

    public partial class AddFarmer : SecurePage
    {
        FarmerDetailsService farmerDetailsService;
        UserMasterService userMasterService;
        Common common = new Common();
        DistrictMasterService districtMasterService;
        TalukMasterService talukMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        BankMasterService bankMasterService;
        public AddFarmer()
        {
            farmerDetailsService = new FarmerDetailsService();
            userMasterService = new UserMasterService();
            districtMasterService = new DistrictMasterService();
            talukMasterService = new TalukMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            bankMasterService = new BankMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearControl("C");
        }
        private void InitComponent()
        {
            BindGrid();
            var banks = bankMasterService.GetAll();
            BankName.BindListItem(banks);
            var districts = districtMasterService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            var taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            var Hoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue));
            ddlHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(Hoblis);
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue), Convert.ToInt16(ddlHobli.SelectedValue));
            ddlvillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }
        private void BindGrid()
        {
            var user = userMasterService.GetFromAuthCookie();
            DataTable data = farmerDetailsService.GetByProducerID(user.Id).ToDataTable();
            CropMasterGridView.DataSource = data;
            CropMasterGridView.DataBind();
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var farmerDetails = new FarmerDetails()
                    {
                        Id = common.GenerateID("FarmerDetails"),
                        Producer_Id = user.Id,
                        Name = NameOfSeedGrower.Text,
                        FatherName = FathersName.Text,
                        Hobli_Id = ddlHobli.SelectedItem.Value.ToInt(),
                        Village_Id = ddlvillage.SelectedItem.Value.ToInt(),
                        GramPanchayath = GramPanchayat.Text,
                        Taluk_Id = Taluk.SelectedItem.Value.ToInt(),
                        District_Id = District.SelectedItem.Value.ToInt(),
                        MobileNo = TelephoneOrMobileNumber.Text,
                        Bank_Id = BankName.SelectedItem.Value.ToInt(),
                        AccountNo = AccountNumber.Text,
                        IFSCCode = IFSCNumber.Text,
                        AadharCardNo = AadhaarNumber.Text,
                        Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : null,
                        Phani = Phani.FileBytes.Length > 0 ? Phani.FileBytes : null,
                        AdharCard = AdharCard.FileBytes.Length > 0 ? AdharCard.FileBytes : null,
                        BankPassBook = BankPassBook.FileBytes.Length > 0 ? BankPassBook.FileBytes : null,
                    };
                    int result = farmerDetailsService.Create(farmerDetails);
                    if (result > 0)
                    {
                        BindGrid();
                       Messaging.SendSMS("Registration of Farmer/Grower - " + NameOfSeedGrower.Text + " Successfuly Completed.", TelephoneOrMobileNumber.Text, "1");
                        ClearControl("S");
                        lblMessage.Text = "Farmer/Grower Added Successfully.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save Farmer/Grower.";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void ClearControl(string ActionType)
        {
            if (ActionType == "U")
            {
                hdnId.Value = "";
                NameOfSeedGrower.Text = "";
                FathersName.Text = "";

                Taluk.ClearSelection();
                District.ClearSelection();
                ddlHobli.ClearSelection();
                ddlvillage.ClearSelection();
                GramPanchayat.Text = "";
                
                TelephoneOrMobileNumber.Text = "";
                BankName.ClearSelection();
                AccountNumber.Text = "";
                IFSCNumber.Text = "";
                AadhaarNumber.Text = "";
                hdnPhoto.Value = "";
                hdnPhani.Value = "";
                hdnAdharCard.Value = "";
                hdnBankPassBook.Value = ""; 

                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }
            else if (ActionType == "S")
            {
                hdnId.Value = "";
                NameOfSeedGrower.Text = "";
                FathersName.Text = "";
                Taluk.ClearSelection();
                District.ClearSelection();
                ddlHobli.ClearSelection();
                ddlvillage.ClearSelection();
                GramPanchayat.Text = "";
                
                TelephoneOrMobileNumber.Text = "";
                BankName.ClearSelection();
                AccountNumber.Text = "";
                IFSCNumber.Text = "";
                AadhaarNumber.Text = "";
                hdnPhoto.Value = "";
                hdnPhani.Value = "";
                hdnAdharCard.Value = "";
                hdnBankPassBook.Value = "";
            }
            else if (ActionType == "C")
            {
                hdnId.Value = "";
                NameOfSeedGrower.Text = "";
                FathersName.Text = "";
                
                GramPanchayat.Text = "";
                Taluk.ClearSelection();
                District.ClearSelection();
                ddlHobli.ClearSelection();
                ddlvillage.ClearSelection();
                TelephoneOrMobileNumber.Text = "";
                BankName.ClearSelection();
                AccountNumber.Text = "";
                IFSCNumber.Text = "";
                AadhaarNumber.Text = "";
                hdnPhoto.Value = "";
                hdnPhani.Value = "";
                hdnAdharCard.Value = "";
                hdnBankPassBook.Value = "";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }

        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            int deletedRowCount = userMasterService.Delete(id);

            if (deletedRowCount > 0)
            {
                BindGrid();
                lblMessage.Text = "Branch deleted.";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var farmerDetails = new FarmerDetails()
                    {
                        Id = hdnId.Value.ToInt(),
                        Name = NameOfSeedGrower.Text,
                        FatherName = FathersName.Text,
                        Hobli_Id = ddlHobli.SelectedItem.Value.ToInt(),
                        Village_Id = ddlvillage.SelectedItem.Value.ToInt(),
                        GramPanchayath = GramPanchayat.Text,
                        Taluk_Id = Taluk.SelectedItem.Value.ToInt(),
                        District_Id = District.SelectedItem.Value.ToInt(),
                        MobileNo = TelephoneOrMobileNumber.Text,
                        Bank_Id = BankName.SelectedItem.Value.ToInt(),
                        AccountNo = AccountNumber.Text,
                        IFSCCode = IFSCNumber.Text,
                        AadharCardNo = AadhaarNumber.Text,
                        Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : Convert.FromBase64String(hdnPhoto.Value),
                        Phani = Phani.FileBytes.Length > 0 ? Phani.FileBytes : Convert.FromBase64String(hdnPhani.Value),
                        AdharCard = AdharCard.FileBytes.Length > 0 ? AdharCard.FileBytes : Convert.FromBase64String(hdnAdharCard.Value),
                        BankPassBook = BankPassBook.FileBytes.Length > 0 ? BankPassBook.FileBytes : Convert.FromBase64String(hdnBankPassBook.Value),
                    };
                    var result = farmerDetailsService.Update(farmerDetails);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("U");
                        lblMessage.Text = "Farmer/Grower details updated.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to update Farmer/Grower.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' /> </a></span>";

            int id = int.Parse((sender as Button).CommandArgument);
            hdnId.Value = id.ToString();
            var farDetls = farmerDetailsService.GetById(id);
            NameOfSeedGrower.Text = farDetls.Name;
            FathersName.Text = farDetls.FatherName;

            District.Items.Clear();
            District.ClearSelection();
            var districts = districtMasterService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            District.Items.FindByValue(farDetls.District_Id.ToString()).Selected = true;

            Taluk.Items.Clear();
            Taluk.ClearSelection(); 
            var taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            Taluk.Items.FindByValue(farDetls.Taluk_Id.ToString()).Selected = true;

            ddlHobli.Items.Clear();
            ddlHobli.ClearSelection();
            var Hoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue));
            ddlHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(Hoblis);
            ddlHobli.Items.FindByValue(farDetls.Hobli_Id.ToString()).Selected = true;

            ddlvillage.ClearSelection();
            ddlvillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue), Convert.ToInt16(ddlHobli.SelectedValue));
            ddlvillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
            ddlvillage.Items.FindByValue(farDetls.Village_Id.ToString()).Selected = true;
            GramPanchayat.Text = farDetls.GramPanchayath;
            TelephoneOrMobileNumber.Text = farDetls.MobileNo;
            BankName.ClearSelection();
            BankName.Items.FindByValue(farDetls.Bank_Id.ToString()).Selected = true;
            AccountNumber.Text = farDetls.AccountNo;
            IFSCNumber.Text = farDetls.IFSCCode;
            AadhaarNumber.Text = farDetls.AadharCardNo;
            hdnPhoto.Value = Serializer.Base64String(farDetls.Photo);
            hdnPhani.Value = Serializer.Base64String(farDetls.Phani);
            hdnAdharCard.Value = Serializer.Base64String(farDetls.AdharCard);
            hdnBankPassBook.Value = Serializer.Base64String(farDetls.BankPassBook);

            if (farDetls.Photo != null && farDetls.Photo.Length > 0)
                LitFarmerPhoto.Text = string.Format(previewString, Serializer.Base64String(farDetls.Photo));
            if (farDetls.Phani != null && farDetls.Phani.Length > 0)
                LitPhani.Text = string.Format(previewString, Serializer.Base64String(farDetls.Phani));
            if (farDetls.AdharCard != null && farDetls.AdharCard.Length > 0)
                LitAdharCard.Text = string.Format(previewString, Serializer.Base64String(farDetls.AdharCard));
            if (farDetls.BankPassBook != null && farDetls.BankPassBook.Length > 0)
                LitBankPassBook.Text = string.Format(previewString, Serializer.Base64String(farDetls.BankPassBook));

            RequiredAdharCard.Enabled = false;
            RequiredBankPassBook.Enabled = false;
            RequiredFarmerPhoto.Enabled = false;
            RequiredPhani.Enabled = false;
            Save.Visible = false;
            btnUpdate.Visible = true;
            Cancel.Visible = true;
        }
        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            var taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);

            ddlHobli.Items.Clear();
            var Hoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue));
            ddlHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(Hoblis);

            ddlvillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue), Convert.ToInt16(ddlHobli.SelectedValue));
            ddlvillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }

        protected void Taluk_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlHobli.Items.Clear();
            var Hoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue));
            ddlHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(Hoblis);

            ddlvillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue), Convert.ToInt16(ddlHobli.SelectedValue));
            ddlvillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }

        protected void ddlHobli_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlvillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(Taluk.SelectedValue), Convert.ToInt16(ddlHobli.SelectedValue));
            ddlvillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }
    }
}