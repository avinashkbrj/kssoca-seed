﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AllotSPU.aspx.cs" Inherits="KSSOCA.Web.Masters.AllotSPU" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" align="center">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="SPU Allotment to SCO's"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Green"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnId" />
        </div>
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr>
                <td>SCO</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlSco" AutoPostBack="true" OnSelectedIndexChanged="ddlSco_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredSCO" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="ddlSco" ErrorMessage="SCO Required" ValidationGroup="v1" InitialValue="0"></asp:RequiredFieldValidator></td>

                <td>Seed Processing Unit</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="ddlSPU">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="ddlSPU" ErrorMessage="SPU Required" ValidationGroup="v1" InitialValue="0"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" ValidationGroup="v1" />
                    &nbsp;
                     <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                         ID="btnUpdate" Text="Update" Visible="false" OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure you want to Update the crop?');" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" Visible="false" /></td>
            </tr>
        </table>
        <br />
        <asp:GridView runat="server" ID="CropMasterGridView" Width="100%"
            AutoGenerateColumns="false" class="table-condensed">
            <Columns>
                <asp:TemplateField HeaderText="Sl.No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %> .
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="SCOName" HeaderText="SCO Name" />
                <asp:BoundField DataField="SPUName" HeaderText="SPU Name" />
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkUpdate" Text="Update" CssClass="btn-default" CommandArgument='<%#Eval("Id")%>' OnClick="lnkUpdate_Click"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>-------------Records Not Found-------------------</EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
