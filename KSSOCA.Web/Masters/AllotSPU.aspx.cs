﻿namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using System.Web.UI.WebControls;
    using Core.Helper;


    public partial class AllotSPU : SecurePage
    {
        RoleMasterService roleMasterService;
        UserMasterService userMasterService;
        Common common = new Common();
        SPURegistrationService sPURegistrationService;
        public AllotSPU()
        {
            roleMasterService = new RoleMasterService();
            userMasterService = new UserMasterService();
            sPURegistrationService = new SPURegistrationService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearControl("C");
        }
        private void InitComponent()
        {
            var user = userMasterService.GetFromAuthCookie();
            var SCO = userMasterService.GetUserList(6);
            ddlSco.BindListItem<UserMaster>(SCO);
            var SPU = sPURegistrationService.GetSPUListBy("A", "", user.Id);
            DataTable _newDataTable = SPU.Select("Reporting_Id=0").CopyToDataTable();
            ddlSPU.BindDropDown(_newDataTable, "SPUId", "Name");
            BindGrid();
        }
        private void BindGrid()
        {
            DataTable data = userMasterService.GetSPUByReportingManager(ddlSco.SelectedValue.ToInt());
            CropMasterGridView.DataSource = data;
            CropMasterGridView.DataBind();
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var usermaster = new UserMaster()
                    {
                        Id = ddlSPU.SelectedValue.Trim().ToInt(),
                        Reporting_Id = ddlSco.SelectedValue.Trim().ToInt(),
                    };
                    int result = userMasterService.Update(usermaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("S");
                        lblMessage.Text = " Saved.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save .";
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void ClearControl(string ActionType)
        {
            var SPU = sPURegistrationService.GetSPUListBy("A", "", 0);
            DataTable _newDataTable = SPU.Select("Reporting_Id=0").CopyToDataTable();
            ddlSPU.BindDropDown(_newDataTable, "SPUId", "Name");
            ddlSPU.SelectedValue = "0";
            ddlSPU.Enabled = true;
            if (ActionType == "U")
            { 
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
               
            }
            else if (ActionType == "S")
            {
                 
            }
            else if (ActionType == "C")
            {
                ddlSPU.SelectedValue = "0";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }

        }
        //protected void lnkDelete_Click(object sender, EventArgs e)
        //{
        //    int id = int.Parse((sender as Button).CommandArgument);
        //    int deletedRowCount = userMasterService.Delete(id);

        //    if (deletedRowCount > 0)
        //    {
        //        BindGrid();
        //        lblMessage.Text = "Branch deleted.";
        //    }
        //}
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var usermaster = new UserMaster()
                    {
                        Id = ddlSPU.SelectedValue.Trim().ToInt(),
                        Reporting_Id = ddlSco.SelectedValue.Trim().ToInt(),
                    };
                    int result = userMasterService.Update(usermaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("U");
                        lblMessage.Text = " updated.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to update .";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            hdnId.Value = id.ToString();
            var var = userMasterService.GetById(id);
            var SPU = sPURegistrationService.GetSPUListBy("A", "", 0); 
            ddlSPU.BindDropDown(SPU, "SPUId", "Name");
            ddlSco.SelectedValue = var.Reporting_Id.ToString();
            ddlSPU.SelectedValue = var.Id.ToString();
            ddlSPU.Enabled = false;
            ddlSPU.CssClass = "form-control";
            Save.Visible = false;
            btnUpdate.Visible = true;
            Cancel.Visible = true;
        }

        protected void ddlSco_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}