﻿
namespace KSSOCA.Web.Masters
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Model;
    using KSSOCA.Core.Security;

    public partial class ClassSeedMasterView : SecurePage
    {
        ClassSeedMasterService classSeedMasterService;
        public ClassSeedMasterView()
        {
            classSeedMasterService = new ClassSeedMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/ClassSeedMasterCreate.aspx");
        }

        public DataTable SelectMethod()
        {
            DataTable data = classSeedMasterService.GetAllClassSeed();
            return data;
        }

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = classSeedMasterService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }

        public bool InsertMethod(ClassSeedMaster classSeed)
        {
            return true;
        }
    }
}