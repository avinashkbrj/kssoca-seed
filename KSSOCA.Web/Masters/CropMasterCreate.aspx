﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CropMasterCreate.aspx.cs" Inherits="KSSOCA.Web.Masters.CropMasterCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/registration.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" align="center">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="Crop Master Entry"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Green"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnId" />
        </div>
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr>
                <td>Crop Type :</td>
                <td>
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbcroptype" CausesValidation="false" runat="server" AutoPostBack="true">
                        <asp:ListItem class="radio-inline" Value="H" Text="Hybrid" Selected="True"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Non-Hybrid"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="Requiredrbcroptype" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbcroptype" ValidationGroup="v1" ErrorMessage="Required Crop Type"></asp:RequiredFieldValidator></td>
                <td>Crop Name :</td>
                <td>
                    <asp:TextBox runat="server" class="form-control" ID="Name" placeholder="Name of the Crop" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ValidationGroup="v1" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Inspection Charge:</td>

                <td>
                    <asp:TextBox runat="server" class="form-control" ID="txtInspection" onkeypress="return CheckDecimal(event);" placeholder="Inspection" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldInspection" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="txtInspection" ValidationGroup="v1" ErrorMessage="Inspection can't be empty"></asp:RequiredFieldValidator></td>
                <td>GOT Charge :</td>
                <td>
                    <asp:TextBox runat="server" class="form-control" ID="txtGOT" Text="0.00" onkeypress="return CheckDecimal(event);" placeholder="GOT" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldGOT" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="txtGOT" ValidationGroup="v1" ErrorMessage="GOT can't be empty"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>Lot Size :</td>
                <td colspan="3">
                    <asp:TextBox runat="server" class="form-control" ID="txtLotSize" onkeypress="return CheckNumber(event);" placeholder="Lot Size" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldLotSize" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="txtLotSize" ValidationGroup="v1" ErrorMessage="Lot Size can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" ValidationGroup="v1" />
                    &nbsp;
                     <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                         ID="btnUpdate" Text="Update" Visible="false" OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure you want to Update the crop?');" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" Visible="false" /></td>
            </tr>
        </table>
        <br />
        <asp:GridView runat="server" ID="CropMasterGridView" Width="100%"
            AutoGenerateColumns="false" class="table-condensed">
            <Columns>
                <asp:TemplateField HeaderText="Sl.No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %> .
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="CropType" HeaderText="Crop Type" />
                <asp:BoundField DataField="Name" HeaderText="Crop Name" />
                <asp:BoundField DataField="Inspection" HeaderText="Inspection Charge(in Rs.)" />
                <asp:BoundField DataField="GOT" HeaderText="GOT Charge(in Rs.)" />
                <asp:BoundField DataField="LotSize" HeaderText="Lot Size(in Qtls.)" />
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkUpdate" Text="Update" CommandArgument='<%#Eval("Id")%>' OnClick="lnkUpdate_Click"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkDelete" Text="Delete" CommandArgument='<%#Eval("Id")%>' OnClick="lnkDelete_Click"
                            OnClientClick="return confirm('Are you sure you want to delete?');"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate></EmptyDataTemplate>
        </asp:GridView>
    </div> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="lib/bootstrap/js/bootstrap.validator.js"></script>
</asp:Content>
