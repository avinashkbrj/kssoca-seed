﻿
namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using System.Web.UI.WebControls;

    public partial class CropMasterCreate :SecurePage
    {
        SeasonMasterService seasonService;
        CropMasterService cropService;

        public CropMasterCreate()
        {
            seasonService = new SeasonMasterService();
            cropService = new CropMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearControl("C");
        }

        private void InitComponent()
        {
            BindGrid();
        }
        private void BindGrid()
        {
            DataTable data = cropService.GetAllCrop();
            CropMasterGridView.DataSource = data;
            CropMasterGridView.DataBind();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var cropMaster = new CropMaster()
                    {
                        Name = Name.Text,
                        GOT = txtGOT.Text.ToDecimal(),
                        Inspection = txtInspection.Text.ToDecimal(),
                        LotSize = txtLotSize.Text.ToInt(),
                        CropType = rbcroptype.SelectedValue.Trim() 
                    };
                    int result = cropService.Register(cropMaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("S");
                        lblMessage.Text = "Crop Saved.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save Crop.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void ClearControl(string ActionType)
        {
            if (ActionType == "U")
            {
                Name.Text = "";
                txtGOT.Text = "0.00";
                txtInspection.Text = "";
                txtLotSize.Text = "";
                rbcroptype.SelectedValue = "N";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }
            else if (ActionType == "S")
            {
                Name.Text = "";
                txtGOT.Text = "0.00";
                txtInspection.Text = "";
                txtLotSize.Text = "";
                rbcroptype.SelectedValue = "N";
            }
            else if (ActionType == "C")
            {
                Name.Text = "";
                txtGOT.Text = "0.00";
                txtInspection.Text = "";
                txtLotSize.Text = "";
                rbcroptype.SelectedValue = "N";
                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
            }

        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            int deletedRowCount = cropService.Delete(id);

            if (deletedRowCount > 0)
            {
                BindGrid();
                lblMessage.Text = "Crop deleted.";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var cropMaster = new CropMaster()
                    {
                        Id = hdnId.Value.ToInt(),
                        Name = Name.Text,
                        GOT = txtGOT.Text.ToDecimal(),
                        Inspection = txtInspection.Text.ToDecimal(),
                        LotSize = txtLotSize.Text.ToInt(),
                        CropType = rbcroptype.SelectedValue.Trim()
                    };
                    int result = cropService.Update(cropMaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("U");
                        lblMessage.Text = "Crop updated.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to update crop.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            hdnId.Value = id.ToString();
            var crop = cropService.GetById(id);
            Name.Text = crop.Name;
            txtGOT.Text = crop.GOT.ToString();
            txtInspection.Text = crop.Inspection.ToString();
            txtLotSize.Text = crop.LotSize.ToString();
            rbcroptype.SelectedValue = crop.CropType;

            Save.Visible = false;
            btnUpdate.Visible = true;
            Cancel.Visible = true;
        }
    }
}