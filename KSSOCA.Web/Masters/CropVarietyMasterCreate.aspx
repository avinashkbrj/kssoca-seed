﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CropVarietyMasterCreate.aspx.cs" Inherits="KSSOCA.Web.Masters.CropVarietyMasterCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/registration.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container" align="center">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="Crop Variety Master Entry"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Green"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnId" />
        </div>
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr>
                <td>Crop Type </td>
                <td>
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbcroptype" CausesValidation="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbcroptype_SelectedIndexChanged">
                        <asp:ListItem class="radio-inline" Value="H" Text="Hybrid" Selected="True"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Non-Hybrid"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="Requiredrbcroptype" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbcroptype" ValidationGroup="v1" ErrorMessage="Required Crop Type"></asp:RequiredFieldValidator></td>

                <td>Crop Name</td>
                <td>
                    <asp:DropDownList runat="server" class="form-control" ID="Crop" AutoPostBack="true" OnSelectedIndexChanged="Crop_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Crop" ValidationGroup="v1" InitialValue="0" ErrorMessage="Required Crop Name"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <asp:Panel ID="pnlparentage" runat="server" Visible="false">
                    <td>Parentage</td>
                    <td>
                        <asp:TextBox runat="server" CssClass="form-control" ID="txtParentage" placeholder="Name of the Crop Parentage" CausesValidation="true" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" CssClass="text-danger" Display="Dynamic"
                            ControlToValidate="txtParentage" ValidationGroup="v1" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                    </td>
                </asp:Panel>
                <td>Variety Name</td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="Name" placeholder="Name of the Crop Variety" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ValidationGroup="v1" ErrorMessage="Crop Variety can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" ValidationGroup="v1" />
                    &nbsp;
                     <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                         ID="btnUpdate" Text="Update" Visible="false" OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure you want to Update the crop?');" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" Visible="false" /></td>
            </tr>
        </table>
        <br />
        <asp:GridView runat="server" ID="GvData" Width="100%"
            AutoGenerateColumns="false" class="table-condensed" OnRowDataBound="GvData_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Sl.No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %> .
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="CropType" HeaderText="Crop Type" />
                <asp:BoundField DataField="CropName" HeaderText="Crop Name" />
                <asp:BoundField DataField="Variety" HeaderText="Variety" />
                <asp:BoundField DataField="NameSV" HeaderText="Parentage" />
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkUpdate" Text="Update" CommandArgument='<%#Eval("Id")%>' OnClick="lnkUpdate_Click"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkDelete" Text="Delete" CommandArgument='<%#Eval("Id")%>' OnClick="lnkDelete_Click"
                            OnClientClick="return confirm('Are you sure you want to delete?');"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>-----Records Not Found-----</EmptyDataTemplate>
        </asp:GridView>
    </div>



</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="lib/bootstrap/js/bootstrap.validator.js"></script>
</asp:Content>
