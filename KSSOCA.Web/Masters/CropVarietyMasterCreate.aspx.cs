﻿using System;
using System.Web.UI;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using System.Web.UI.WebControls;

namespace KSSOCA.Web.Masters
{
    public partial class CropVarietyMasterCreate : SecurePage
    {
        CropVarietyMasterService cropVarietyService;
        CropMasterService cropService;

        public CropVarietyMasterCreate()
        {
            cropVarietyService = new CropVarietyMasterService();
            cropService = new CropMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
                lblMessage.Text = "";
                Name.Focus();
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent()
        {
            if (rbcroptype.SelectedValue == "H")
            { pnlparentage.Visible = true; }
            else pnlparentage.Visible = false;
            var crops = cropService.GetByType(rbcroptype.SelectedValue.Trim());
            Crop.BindListItem<CropMaster>(crops);
            BindGrid();
        }
        private void BindGrid()
        {
            DataTable data = cropVarietyService.GetAllVariety(Crop.SelectedValue.ToInt());
            GvData.DataSource = data;
            GvData.DataBind();
        }
        private void ClearControl(string ActionType)
        {
            if (ActionType == "U")
            {
                Name.Text = "";
                txtParentage.Text = "";

                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true;
                Crop.Enabled = true;
                rbcroptype.Enabled = true;
            }
            else if (ActionType == "S")
            {
                Name.Text = "";
                txtParentage.Text = "";

            }
            else if (ActionType == "C")
            {
                Name.Text = "";
                txtParentage.Text = "";

                btnUpdate.Visible = false;
                Cancel.Visible = false;
                Save.Visible = true; Crop.Enabled = true;
                rbcroptype.Enabled = true;
            }

        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var crop = cropService.GetById(Crop.SelectedValue.ToInt());
                    if (crop.CropType == "N")
                    {
                        txtParentage.Text = Name.Text;
                    }
                    var cropVarietyMaster = new CropVarietyMaster()
                    {
                        Name = Name.Text,
                        Crop_Id = Crop.SelectedItem.Value.ToInt(),
                        NameSV = txtParentage.Text.Trim(),
                    };
                    int result = cropVarietyService.Register(cropVarietyMaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("S");
                        lblMessage.Text = "Crop Variety Details Saved.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            int deletedRowCount = cropVarietyService.Delete(id);

            if (deletedRowCount > 0)
            {
                BindGrid();
                lblMessage.Text = "Variety deleted.";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var crop = cropService.GetById(Crop.SelectedValue.ToInt());
                    if (crop.CropType == "N")
                    {
                        txtParentage.Text = Name.Text;
                    }
                    var cropVarietyMaster = new CropVarietyMaster()
                    {
                        Id = hdnId.Value.ToInt(),
                        Name = Name.Text,
                        Crop_Id = Crop.SelectedItem.Value.ToInt(),
                        NameSV = txtParentage.Text.Trim(),
                    };
                    int result = cropVarietyService.Update(cropVarietyMaster);
                    if (result > 0)
                    {
                        BindGrid();
                        ClearControl("U");
                        lblMessage.Text = "Updated.";
                    }
                    else
                    {
                        lblMessage.Text = "Unable to save.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            int id = int.Parse((sender as Button).CommandArgument);
            hdnId.Value = id.ToString();
            var var = cropVarietyService.GetById(id);
            Name.Text = var.Name;
            txtParentage.Text = var.NameSV;
            Save.Visible = false;
            btnUpdate.Visible = true;
            Cancel.Visible = true;
            Crop.Enabled = false;
            rbcroptype.Enabled = false;
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearControl("C");
        }
        protected void rbcroptype_SelectedIndexChanged(object sender, EventArgs e)
        {
            Crop.Items.Clear();
            var crops = cropService.GetByType(rbcroptype.SelectedValue.Trim());
            Crop.BindListItem<CropMaster>(crops);
            if (rbcroptype.SelectedValue == "H")
            { pnlparentage.Visible = true; }
            else pnlparentage.Visible = false;
            BindGrid();
        }
        protected void Crop_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void GvData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (rbcroptype.SelectedValue == "N" )
            {
                GvData.Columns[4].Visible = false;
            }
            else GvData.Columns[4].Visible = true;
        }
    }
}