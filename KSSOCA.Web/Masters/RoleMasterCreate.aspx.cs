﻿using System;
using System.Web.UI;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;

namespace KSSOCA.Web.Masters
{
    public partial class RoleMasterCreate : SecurePage
    {
        RoleMasterService roleService;
        public RoleMasterCreate()
        {
            roleService = new RoleMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //int captcha;
                    // InitComponent();
                    //CaptchaLabel.Text = GenerateRandomCaptcha(out captcha);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var roleMaster = new RoleMaster()
                    {
                        Name = Name.Text,
                        ShortName = ShortName.Text,
                    };

                    int result = roleService.Register(roleMaster);

                    if (result > 0)
                    {
                        this.Redirect("Masters/RoleMasterView.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/RoleMasterView.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}