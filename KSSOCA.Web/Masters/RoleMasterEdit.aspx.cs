﻿using System;
using System.Web.UI;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;

namespace KSSOCA.Web.Masters
{
    public partial class RoleMasterEdit : SecurePage
    {
        RoleMasterService roleService;
        public RoleMasterEdit()
        {
            roleService = new RoleMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            RoleMaster roleMaster = roleService.GetById(id);

                            if (roleMaster == null)
                            {
                                this.Redirect("Masters/RoleMasterView.aspx", false);
                                Context.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                InitComponent(roleMaster);
                            }
                        }
                    }
                    else
                    {
                        this.Redirect("Masters/RoleMasterView.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(RoleMaster roleMaster)
        {
            // Bind Dropdown
            Id.Value = roleMaster.Id.ToString();
            Name.Text = roleMaster.Name.ToString();
            ShortName.Text = roleMaster.ShortName.ToString();

        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var roleMaster = new RoleMaster()
                    {
                        Id = Id.Value.ToInt(),
                        Name = Name.Text,
                        ShortName = ShortName.Text,
                    };

                    int result = roleService.Update(roleMaster);

                    if (result > 0)
                    {
                        this.Redirect("Masters/RoleMasterView.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/RoleMasterView.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}