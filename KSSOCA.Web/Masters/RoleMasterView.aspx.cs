﻿using System;
using System.Data;
using KSSOCA.Core.Data;
using System.Web.UI.WebControls;
using KSSOCA.Core.Security;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;

namespace KSSOCA.Web.Masters
{
    public partial class RoleMasterView : SecurePage
    {
        RoleMasterService roleMasterService;
        public RoleMasterView()
        {
            roleMasterService = new RoleMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/RoleMasterCreate.aspx");
        }

        public DataTable SelectMethod()
        {
            DataTable data = roleMasterService.GetAllRole();
            return data;
        }

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = roleMasterService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }

        public bool InsertMethod(RoleMaster role)
        {
            return true;
        }
    }
}