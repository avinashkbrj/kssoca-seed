﻿using System;
using System.Web.UI;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;

namespace KSSOCA.Web.Masters
{
    public partial class SeasonMasterEdit : SecurePage
    {
        SeasonMasterService seasonService;
        public SeasonMasterEdit()
        {
            seasonService = new SeasonMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            SeasonMaster seasonMaster = seasonService.GetById(id);

                            if (seasonMaster == null)
                            {
                                this.Redirect("Masters/seasonMasterView.aspx", false);
                                Context.ApplicationInstance.CompleteRequest();
                            }
                            else
                            {
                                InitComponent(seasonMaster);
                            }
                        }
                    }
                    else
                    {
                        this.Redirect("Masters/seasonMasterView.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(SeasonMaster seasonMaster)
        {
            // Bind Dropdown
            Id.Value = seasonMaster.Id.ToString();
            Name.Text = seasonMaster.Name.ToString();

        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var seasonMaster = new SeasonMaster()
                    {
                        Id = Id.Value.ToInt(),
                        Name = Name.Text,
                    };

                    int result = seasonService.Update(seasonMaster);

                    if (result > 0)
                    {
                        this.Redirect("/Masters/SeasonMasterView.aspx", false);
                        Context.ApplicationInstance.CompleteRequest();
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("/Masters/SeasonMasterView.aspx", false);
            Context.ApplicationInstance.CompleteRequest();
        }
    }
}