﻿using System;
using System.Data;
using KSSOCA.Core.Data;
using System.Web.UI.WebControls;
using KSSOCA.Core.Security;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;

namespace KSSOCA.Web.Masters
{
    public partial class SeasonMasterView : SecurePage
    {
        SeasonMasterService seasonMasterService;
        public SeasonMasterView()
        {
            seasonMasterService = new SeasonMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/SeasonMasterCreate.aspx");
            Context.ApplicationInstance.CompleteRequest();
        }

        public DataTable SelectMethod()
        {
            DataTable data = seasonMasterService.GetAllSeason();
            return data;
        }

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = seasonMasterService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }

        public bool InsertMethod(SeasonMaster season)
        {
            return true;
        }
    }
}