﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserMasterCreate.aspx.cs" Inherits="KSSOCA.Web.Masters.UserMasterCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='<%= ResolveUrl("~/css/registration.css") %>' rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">

        <div class="form-horizontal">
            <div class="MainHeading">
                <asp:Label runat="server" ID="lblHeading"> </asp:Label>
            </div>
            <br />
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <asp:Label runat="server" CssClass="text-danger" ID="lblMessage"></asp:Label>
                </div>
            </div>
            <div id="divStatic" runat="server">
                <div class="form-group">
                    <label class="col-lg-4 control-label no-top-padding">Name</label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="Name" placeholder="Name of the User" CausesValidation="true" />
                        <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                            ControlToValidate="Name" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="NameMinMax" CssClass="text-danger" Display="Dynamic"
                            ValidationExpression="^[A-Za-z0-9@&#.(),_\s-]{0,75}$" ControlToValidate="Name"
                            ErrorMessage="Producer Name required min of 5 characters and max of 75"></asp:RegularExpressionValidator>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">Financial Year/ಹಣಕಾಸು ವರ್ಷ<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <asp:TextBox runat="server" CssClass="form-control" ID="FinancialYear" ReadOnly="true"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">District<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">Taluk<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" class="form-control" ID="Taluk">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label">Role<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" class="form-control" ID="Role">
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label">Reporting Manager<span class="text-danger">*</span></label>
                    <div class="col-lg-8">
                        <asp:DropDownList runat="server" class="form-control" ID="ReportingManager">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">
                    Mobile Number  <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" class="form-control" ID="MobileNumber" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="MobileNumberRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="MobileNumber" ErrorMessage="Mobile Number can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="MobileNumberMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[0-9]{10,10}$" ControlToValidate="MobileNumber"
                        ErrorMessage="Please enter valid mobile number"></asp:RegularExpressionValidator>
                </div>
            </div>


            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">
                    User Name  <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" class="form-control" ID="txtusename" placeholder="User Name" MaxLength="25" OnTextChanged="txtusename_TextChanged" AutoPostBack="true" />
                    <asp:RequiredFieldValidator runat="server" ID="Requiredusename" Style="color: red" Display="Dynamic"
                        ControlToValidate="txtusename" ErrorMessage="User Name  Required"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="Regularusename" Style="color: red" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_]{5,25}$" ControlToValidate="MobileNumber"
                        ErrorMessage="Please enter  minimum 5 maximum 25 character of A-Z a-z 0-9"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">
                    Email Id  <span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" class="form-control" ID="EmailId" placeholder="Email Id" />
                    <asp:RequiredFieldValidator runat="server" ID="EmailRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ErrorMessage="Email Id can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="EmailRegEx" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                        ErrorMessage="Please enter valid email id"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Password<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" TextMode="Password" class="form-control" ID="Password" />
                    <asp:RequiredFieldValidator runat="server" ID="PasswordRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Password" ErrorMessage="Password can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="PasswordRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{5,75}$" ControlToValidate="Password"
                        ErrorMessage="Password required min of 5 and max of 20 characters."></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Retype password<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" TextMode="Password" class="form-control" ID="ConfirmPassword" />
                    <asp:CompareValidator runat="server" ID="PasswordMatch" CssClass="text-danger" Display="Dynamic"
                        ControlToCompare="Password" ControlToValidate="ConfirmPassword" ErrorMessage="Password didn't match"></asp:CompareValidator>
                </div>
            </div>

        </div>

        <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
                <%--<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Sign up</button>--%>
                <asp:Button runat="server" CssClass="btn-primary"
                    ID="Signup" Text="Sign up" OnClick="Signup_Click" />
                <asp:Button runat="server" CssClass="btn-primary"
                    ID="btnUpdate" Text="Update Details" OnClick="btnUpdate_Click" />
                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                    ID="CancelButton" Text="Cancel" OnClientClick="JavaScript:window.history.back(1); return false;" />
            </div>
        </div>
        <div class="col-lg-12">
            <asp:Label Visible="false" runat="server" ID="lblSuccess">Registration successful. Go to <a href="Home.aspx">Home</a></asp:Label>
        </div>

        <asp:ObjectDataSource runat="server" ID="UserMasterDataSource" TypeName="KSSOCA.Web.Masters.UserMasterCreate"
            SelectMethod="SelectMethod" DeleteMethod="DeleteMethod">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="id" />
            </UpdateParameters>
        </asp:ObjectDataSource>

        <asp:GridView runat="server" ID="UserMasterGridView" DataSourceID="UserMasterDataSource"
            AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover"
            AllowPaging="true" PageSize="25" AllowSorting="true" DataKeyNames="Id">

            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton runat="server" CommandName="Delete"
                            OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" />
            </Columns>
            <Columns>
                <asp:BoundField DataField="Email" HeaderText="Email" />
            </Columns>
            <Columns>
                <asp:BoundField DataField="Role" HeaderText="Role" />
            </Columns>
            <Columns>
                <asp:BoundField DataField="Mobile No" HeaderText="Mobile No" />
            </Columns>
            <Columns>
                <asp:BoundField DataField="Active" HeaderText="Active" />
            </Columns>
            <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
