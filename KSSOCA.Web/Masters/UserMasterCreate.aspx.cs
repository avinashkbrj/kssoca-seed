﻿
namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    public partial class UserMasterCreate : SecurePage
    {
        TalukMasterService talukService;
        RoleMasterService roleMasterService;
        UserMasterService userMasterService;

        DistrictMasterService districtService;
        Common common = new Common();
        public UserMasterCreate()
        {
            talukService = new TalukMasterService();
            roleMasterService = new RoleMasterService();
            userMasterService = new UserMasterService();
            districtService = new DistrictMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    lblHeading.Text = common.getHeading("UR");
                    InitComponent();
                    //CaptchaLabel.Text = GenerateRandomCaptcha(out captcha);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        public DataTable SelectMethod()
        {
            DataTable data = userMasterService.GetAllUser();
            return data;
        }

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = userMasterService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }
        protected void Signup_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = new UserMaster()
                    {
                        Id = common.GenerateID("UserMaster"),
                        Name = Name.Text.Trim().ToUpper().Replace(" ", ""),
                        //Reg_no = userMasterService.GenerateRegistrationNumber(),
                        RegistrationDate = System.DateTime.Now,
                        District_Id = District.Text.ToNullableInt(),
                        Taluk_Id = Taluk.Text.ToNullableInt(),
                        MobileNo = MobileNumber.Text,
                        Role_Id = Role.SelectedItem.Value.ToInt(),
                        Reporting_Id = ReportingManager.SelectedItem.Value.ToInt(),
                        Division_Id = Convert.ToInt32(userMasterService.GetById(ReportingManager.SelectedItem.Value.ToInt()).Reporting_Id),
                        EmailId = EmailId.Text,
                        Password = Password.Text,
                        IsActive = true,
                        IsAdmin = false,
                        UserName = txtusename.Text.Replace(" ", ""),
                    };

                    int result = userMasterService.Register(userMaster);

                    if (result > 0)
                    {
                        // this.Redirect("Masters/UserMasterView.aspx");
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                // ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void txtusename_TextChanged(object sender, EventArgs e)
        {
            bool IsUserNameExist = userMasterService.IsUserNameExist(txtusename.Text);
            if (IsUserNameExist)
            {
                txtusename.Text = "";
                lblMessage.Text = "This user name already exist please try another.";
            }
        }
        private void InitComponent()
        {
            var user = userMasterService.GetFromAuthCookie();
            FinancialYear.Text = DateTime.Now.ToFinancialYear();
            var roles = roleMasterService.GetAll();
            var reportingManager = userMasterService.GetReportingManager();
            Role.BindListItem<RoleMaster>(roles);
            var districts = districtService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            ReportingManager.BindListItem<UserMaster>(reportingManager);
            if (user.Role_Id != 8)
            {
                Name.Text = user.Name;
                MobileNumber.Text = user.MobileNo;
                EmailId.Text = user.EmailId;
                txtusename.Text = user.UserName;

                UserMasterGridView.Visible = false;
                divStatic.Visible = false;
                btnUpdate.Visible = true;
                Signup.Visible = false;
            }
        }

        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var userMaster = new UserMaster()
                {
                    Id = userMasterService.GetFromAuthCookie().Id,
                    MobileNo = MobileNumber.Text,
                    EmailId = EmailId.Text,
                    Password = Password.Text,
                    UserName = txtusename.Text.Replace(" ", ""),
                };
                int result = userMasterService.Update(userMaster);

                if (result > 0)
                {
                    lblMessage.Text = "Details Saved SuccessFully.";
                    this.Redirect("Logout.aspx");
                }
                else
                {
                    lblMessage.Text = "Error occurred while saving.";
                }
            }
        }
    }
}