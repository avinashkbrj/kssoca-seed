﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserMasterEdit.aspx.cs" Inherits="KSSOCA.Web.Masters.UserMasterEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='<%= ResolveUrl("~/css/registration.css") %>' rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h1>User Registration</h1>
        <div class="form-horizontal">
            
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
                    <asp:HiddenField runat="server" ID="Id" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">Name</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" Id="Name" placeholder="Name of the User" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="NameMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{5,75}$" ControlToValidate="Name"
                        ErrorMessage="Producer Name required min of 5 characters and max of 75"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Registration Number<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" Id="RegistrationNumber" ReadOnly="true" />
                    <asp:RequiredFieldValidator runat="server" ID="RegistrationNumberRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="RegistrationNumber" ErrorMessage="Registration Number can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="RegistrationNumberMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_\s\/-]{3,20}$" ControlToValidate="RegistrationNumber"
                        ErrorMessage="Registration Number required min of 3 characters"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Financial Year/ಹಣಕಾಸು ವರ್ಷ<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" Id="FinancialYear" ReadOnly="true"></asp:TextBox>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Zone<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" Id="Zone">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">District<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" Id="District">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Taluk<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" Id="Taluk">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">Mobile Number (Producer) /<br /> 
                    ಮೊಬೈಲ್ ಸಂಖ್ಯೆ (ನಿರ್ಮಾಪಕ)<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" class="form-control" Id="MobileNumber" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="MobileNumberRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="MobileNumber" ErrorMessage="Mobile Number can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="MobileNumberMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[0-9]{10,10}$" ControlToValidate="MobileNumber"
                        ErrorMessage="Please enter valid mobile number"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">Email Id /<br /> ಇಮೇಲ್ ಸಂಖ್ಯೆ<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" class="form-control" Id="EmailId" placeholder="Email Id" />
                    <asp:RequiredFieldValidator runat="server" ID="EmailRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ErrorMessage="Email Id can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="EmailRegEx" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                        ErrorMessage="Please enter valid email id"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Role<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" Id="Role">
                    </asp:DropDownList>
                </div>
            </div>

             <div class="form-group">
                <label class="col-lg-4 control-label">Reporting Manager<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" Id="ReportingManager">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Password<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" TextMode="Password" class="form-control" Id="Password" />
                    <asp:RegularExpressionValidator runat="server" ID="PasswordRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{5,75}$" ControlToValidate="Password"
                        ErrorMessage="Password required min of 5 and max of 20 characters."></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Retype password<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" TextMode="Password" class="form-control" Id="ConfirmPassword" />
                    <asp:CompareValidator runat="server" ID="PasswordMatch" CssClass="text-danger" Display="Dynamic"
                        ControlToCompare="Password" ControlToValidate="ConfirmPassword" ErrorMessage="Password didn't match"></asp:CompareValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label">Active</label>
                <div class="col-lg-8">
                    <asp:CheckBox runat="server" class="form-control no-border no-background" Id="Active" />
                </div>
            </div>

            <%--<div class="form-group">
                <asp:Label runat="server" class="col-lg-4 control-label" id="CaptchaLabel"></asp:Label>
                <div class="col-lg-2">
                    <asp:TextBox runat="server" class="form-control" Id="captcha" />
                </div>
            </div>--%>

            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <%--<button type="submit" class="btn btn-primary" name="signup" value="Sign up">Sign up</button>--%>
                    <asp:Button runat="server" CssClass="btn btn-primary"
                        Id="Signup" Text="Update" OnClick="Signup_Click" />
                    <asp:Button runat="server" CssClass="btn btn-primary" CausesValidation="false"
                        Id="CancelButton" Text="Cancel" OnClick="CancelButton_Click" />
                </div>
            </div>
            <div class="col-lg-12">
                <asp:Label Visible="false" runat="server" ID="lblSuccess">Registration successful. Go to <a href="Home.aspx">Home</a></asp:Label>
            </div>
        </div>
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>

