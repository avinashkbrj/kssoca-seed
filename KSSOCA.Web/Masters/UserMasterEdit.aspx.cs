﻿
namespace KSSOCA.Web.Masters
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;

    public partial class UserMasterEdit : SecurePage
    {
        TalukMasterService talukService;
        RoleMasterService roleMasterService;
        UserMasterService userMasterService;
         
        DistrictMasterService districtService;

        public UserMasterEdit()
        {
            talukService = new TalukMasterService();
            roleMasterService = new RoleMasterService();
            userMasterService = new UserMasterService();
           
            districtService = new DistrictMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            UserMaster userMaster = userMasterService.GetById(id);

                            if (userMaster == null)
                            {
                                this.Redirect("Masters/UserMasterView.aspx");
                            }
                            else
                            {
                                InitComponent(userMaster);
                            }
                        }
                    }
                    else
                    {
                        this.Redirect("Masters/UserMasterView.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        protected void Signup_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = new UserMaster()
                    {
                        Id = Id.Value.ToInt(),
                        Name = Name.Text + "," + Role.SelectedItem + "," + District.SelectedItem.Text,
                        Reg_no = RegistrationNumber.Text,
                        Year = DateTime.Now.ToFinancialYear(),
                        Division_Id = Zone.Text.ToNullableInt(),
                        District_Id = District.Text.ToNullableInt(),
                        Taluk_Id = Taluk.Text.ToNullableInt(),
                        MobileNo = MobileNumber.Text,
                        Role_Id = Role.SelectedItem.Value.ToInt(),
                        Reporting_Id = ReportingManager.SelectedItem.Value.ToInt(),
                        EmailId = EmailId.Text,
                        Password = Password.Text,
                        IsActive = Active.Checked,
                    };

                    int result = userMasterService.Update(userMaster);

                    if (result > 0)
                    {
                        this.Redirect("Masters/UserMasterView.aspx");
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/UserMasterView.aspx");
        }

        private void InitComponent(UserMaster userMaster)
        {
            // Bind Dropdown
            var roles = roleMasterService.GetAll();
           // var divisions = divisionService.GetAll();
            var taluks = talukService.GetAll();
            var districts = districtService.GetAll();
            var reportingManager = userMasterService.GetReportingManager();

           // Zone.BindListItem<DivisionMaster>(divisions);
            Role.BindListItem<RoleMaster>(roles);
            Taluk.BindListItem<TalukMaster>(taluks);
            District.BindListItem<DistrictMaster>(districts);
            ReportingManager.BindListItem<UserMaster>(reportingManager);

            Id.Value = userMaster.Id.ToString();
            Name.Text = userMaster.Name;
            RegistrationNumber.Text = userMaster.Reg_no;
            FinancialYear.Text = userMaster.Year;
            MobileNumber.Text = userMaster.MobileNo;
            EmailId.Text = userMaster.EmailId;
            Password.Text = "**********";
            Active.Checked = userMaster.IsActive.Value;
            Zone.Items.FindByValue(userMaster.Division_Id.ToString()).Selected = true;
            District.Items.FindByValue(userMaster.District_Id.ToString()).Selected = true;
            Taluk.Items.FindByValue(userMaster.Taluk_Id.ToString()).Selected = true;
            Role.Items.FindByValue(userMaster.Role_Id.ToString()).Selected = true;
            ReportingManager.Items.FindByValue(userMaster.Reporting_Id.ToString()).Selected = true;
        }
    }
}