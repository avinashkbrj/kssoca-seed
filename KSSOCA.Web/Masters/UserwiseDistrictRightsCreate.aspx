﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserwiseDistrictRightsCreate.aspx.cs" Inherits="KSSOCA.Web.Masters.UserwiseDistrictRightsCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/registration.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h1>User wise District Rights</h1>
        <div class="form-horizontal">

            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <asp:Label runat="server" ID="lblMessage"></asp:Label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">User Type<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="ddlUserType" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">User<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="User" AutoPostBack="true" OnSelectedIndexChanged="User_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">District<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4 control-label">Taluk<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="Taluk">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-4">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" />
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" />
                </div>
            </div>
            <div class="col-lg-12">
                <asp:Label Visible="false" runat="server" ID="lblSuccess">User wise District Rights Entry successful. Go to <a href="Home.aspx">Home</a></asp:Label>
            </div>
            <div class="well">
                <asp:ObjectDataSource runat="server" ID="UserwiseDistrictRightsDataSource" TypeName="KSSOCA.Web.Masters.UserwiseDistrictRightsCreate"
                    SelectMethod="SelectMethod" DeleteMethod="DeleteMethod">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:ObjectDataSource>

                <asp:GridView runat="server" ID="UserwiseDistrictRightsGridView"
                    AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover"
                    AllowPaging="true" PageSize="25" AllowSorting="true" DataKeyNames="Id">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="District" HeaderText="District" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Taluk" HeaderText="Taluk" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="User" HeaderText="User" />
                    </Columns>
                  <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="lib/bootstrap/js/bootstrap.validator.js"></script>
</asp:Content>
