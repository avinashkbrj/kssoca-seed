﻿using System;
using System.Web.UI;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;


namespace KSSOCA.Web.Masters
{
    public partial class UserwiseDistrictRightsCreate : SecurePage
    {
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        DistrictMasterService districtService;
        UserMasterService userService;
        TalukMasterService talukMasterService;
        RoleMasterService roleMasterService;

        public UserwiseDistrictRightsCreate()
        {
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            districtService = new DistrictMasterService();
            userService = new UserMasterService();
            talukMasterService = new TalukMasterService();
            roleMasterService = new RoleMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    //int captcha;
                    InitComponent();
                    //CaptchaLabel.Text = GenerateRandomCaptcha(out captcha);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent()
        {
            // Bind Dropdown

            var role = roleMasterService.GetAll();
            ddlUserType.BindListItem<RoleMaster>(role);

            var user = userService.GetUserList(ddlUserType.SelectedValue.ToInt());
            User.BindListItem<UserMaster>(user);

            District.Items.Clear();
            var district = districtService.GetAll();// GetDistrictNotinUserMaster(User.SelectedItem.Value.ToInt());
            District.BindListItem<DistrictMaster>(district);           

            var taluk = talukMasterService.GetTalukNotinUserMaster(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluk);
            bind();

        }
        private void bind()
        {
            DataTable data = userwiseDistrictRightsService.GetUsers(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
            UserwiseDistrictRightsGridView.DataSource = data;
            UserwiseDistrictRightsGridView.DataBind();
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userwiseDistrictRights = new UserwiseDistrictRights()
                    {
                        District_Id = District.SelectedItem.Value.ToInt(),
                        Taluk_Id = Taluk.SelectedValue.ToInt(),
                        User_Id = User.SelectedItem.Value.ToInt(),
                        UserRole_Id = userService.GetById(User.SelectedItem.Value.ToInt()).Role_Id.Value                       
                    };

                    int result = userwiseDistrictRightsService.Register(userwiseDistrictRights);

                    if (result > 0)
                    {
                        bind();
                        // District.Items.Clear();
                        // var district = districtService.GetDistrictNotinUserMaster(User.SelectedItem.Value.ToInt());
                        // District.BindListItem<DistrictMaster>(district);
                        // UserwiseDistrictRightsGridView.DataBind();
                        //// this.Redirect("Masters/UserwiseDistrictRightsView.aspx");
                        Taluk.Items.Clear();
                        var taluk = talukMasterService.GetTalukNotinUserMaster(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
                        Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluk);

                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Masters/UserwiseDistrictRightsView.aspx");
        }

        protected void User_SelectedIndexChanged(object sender, EventArgs e)
        {
            District.Items.Clear();
            var district = districtService.GetAll();
            District.BindListItem<DistrictMaster>(district);
            Taluk.Items.Clear();
            var taluk = talukMasterService.GetTalukNotinUserMaster(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluk);
            bind();
        }
        public bool DeleteMethod(int id)
        {
            int deletedRowCount = userwiseDistrictRightsService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }
        public bool InsertMethod(UserwiseDistrictRights userwiseDistrictRights)
        {
            return true;
        }
        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            var taluk = talukMasterService.GetTalukNotinUserMaster(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluk);
            bind();
        }

        protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
        {
            User.Items.Clear();
            var user = userService.GetUserList(ddlUserType.SelectedValue.ToInt());
            User.BindListItem<UserMaster>(user);

            District.Items.Clear();
            var district = districtService.GetAll();
            District.BindListItem<DistrictMaster>(district);

            Taluk.Items.Clear();
            var taluk = talukMasterService.GetTalukNotinUserMaster(User.SelectedItem.Value.ToInt(), District.SelectedValue.ToInt());
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluk);
            bind();
        }
    }
}