﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FIR_Report.aspx.cs" Inherits="KSSOCA.Web.Reports.FIR_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container-fluid">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr>
                        <td>Get Report By  </td>
                        <td colspan="3">
                            <asp:DropDownList runat="server" ID="ddlfilter" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                                <asp:ListItem Text="Zone" Value="3" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Producer" Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlZone" runat="server" Visible="false">
                        <tr>
                            <td>Zone </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlZone" AutoPostBack="true" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredddlZone" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlZone" ErrorMessage="Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td>ADSC </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlADSC" AutoPostBack="true" OnSelectedIndexChanged="ddlADSC_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredddlADSC" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlADSC" ErrorMessage="Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>SCO </td>
                            <td colspan="3">
                                <asp:DropDownList runat="server" ID="ddlSCO" AutoPostBack="true" OnSelectedIndexChanged="ddlSCO_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredddlSCO" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlSCO" ErrorMessage="Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlProducer" runat="server" Visible="false">
                        <tr>
                            <td>Select Producer </td>
                            <td colspan="3">
                                <asp:DropDownList runat="server" ID="ddlProducer" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredddlProducer" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlProducer" ErrorMessage="Required" InitialValue="0"></asp:RequiredFieldValidator>&nbsp;&nbsp;                      
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td>Time Duration </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDuration" AutoPostBack="true" OnSelectedIndexChanged="ddlDuration_SelectedIndexChanged">
                                <asp:ListItem Text="Financial Year" Value="F" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Date Wise" Value="D"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            <asp:Panel ID="pnlsearchbyDate" runat="server" Visible="false">
                                From Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtFromDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtFromDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtFromDate" ErrorMessage=" Required" ValidationGroup="v1"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                              To Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtToDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtToDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtToDate" ErrorMessage=" Required" ValidationGroup="v1"></asp:RequiredFieldValidator>
                            </asp:Panel>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlFinancialYear" runat="server" Visible="false">
                        <tr>
                            <td>Financial Year </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlfinancialYear" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td>Season </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlseason" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Button ID="btngetSV" runat="server" Text="Get Field Inspection Report" CssClass="btn-primary" ValidationGroup="v1" OnClick="btngetSV_Click" />
                        </td>
                    </tr>

                </table>
                <br />

                <asp:GridView ID="gvSourceVerification" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%" OnRowCreated="gvSourceVerification_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="form1no" HeaderText="Form-I Registration No" />
                        <asp:BoundField DataField="growername" HeaderText="Grower Name" />
                        <asp:BoundField DataField="crop" HeaderText="Crop" />
                        <asp:BoundField DataField="variety" HeaderText="Variety" />
                        <asp:BoundField DataField="class" HeaderText="Class" />
                        <asp:BoundField DataField="Areaoffered" HeaderText="Area Offered" />
                        <asp:BoundField DataField="ActualDateofSowing" HeaderText="Date of Sowing" />
                        <asp:BoundField DataField="InspectedArea" HeaderText="Inspected Area" />
                        <asp:BoundField DataField="ReportNO" HeaderText="Report NO" />
                        <asp:BoundField DataField="EstimatedYield" HeaderText="Estimated Yield" />
                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                        <asp:BoundField DataField="InspectedDate" HeaderText="Inspected Date" />
                    </Columns>

                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>

                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
