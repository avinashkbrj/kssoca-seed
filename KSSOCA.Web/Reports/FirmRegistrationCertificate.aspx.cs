﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using KSSOCA.Core.Helper;

namespace KSSOCA.Web.Reports
{
    public partial class FirmRegistrationCertificate : System.Web.UI.Page
    {
        RegistrationFormService registrationFormService; Common common = new Common();
        RegistrationFormTransactionService registrationFormTransactionService;
        UserMasterService userMasterService;
        TalukMasterService talukService;
        DistrictMasterService districtMasterService;
        public FirmRegistrationCertificate()
        {
            registrationFormService = new RegistrationFormService();
            registrationFormTransactionService = new RegistrationFormTransactionService();
            userMasterService = new UserMasterService();
            talukService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            RegistrationForm registrationForm = registrationFormService.GetById(id);
                            InitComponent(registrationForm);
                        }
                    }
                }
            }
            catch (Exception ex)
            { 
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Dashboard.aspx");
            }
        }
        private void InitComponent(RegistrationForm rf)
        {
            if (rf.StatusCode == 1)
            {
                RegistrationFormTransaction transaction = new RegistrationFormTransaction();
                transaction = registrationFormTransactionService.GetMaxTransactionDetails(rf.Id);
                if (transaction.FromID == 3 && transaction.TransactionStatus == 5)
                {
                    if (rf.RegistrationType == "N")
                    { lblTitle.Text = "SEED PRODUCER'S REGISTRATION"; }
                    else if
                        (rf.RegistrationType == "R") { lblTitle.Text = "SEED PRODUCER'S REGISTRATION"; }
                    var user = userMasterService.GetById(Convert.ToInt16(rf.ProducerID));
                    lblRegNo.Text = rf.RegistrationNo;
                    lblProducerNameAddress.Text = userMasterService.GetById(Convert.ToInt16(rf.ProducerID)).Name + 
                        ", " + rf.Address +", "+talukService.GetBy(Convert.ToInt16(user.District_Id), Convert.ToInt16(user.Taluk_Id)).Name+
                        " Tq "+districtMasterService.GetById(Convert.ToInt16(user.District_Id)).Name+" Dist";
                    lblFinancialYear.Text = Convert.ToDateTime(transaction.TransactionDate).ToFinancialYear().ToString();
                    lblValidity.Text = rf.Validity.Value.ToString("dd/MM/yyyy");
                    lblApprovedDate.Text = Convert.ToString(transaction.TransactionDate.Value.ToString("dd/MM/yyyy"));

                }
                else { this.Redirect("Transactions/FormRegistrationApproval.aspx?Id=" + rf.Id + ""); }
            }
            else
            {
                this.Redirect("Transactions/FormRegistrationApproval.aspx?Id=" + rf.Id + "");
            }
            
        }
    }
}