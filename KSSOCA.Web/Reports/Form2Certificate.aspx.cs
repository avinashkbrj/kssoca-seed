﻿using KSSOCA.Core.Data;
using KSSOCA.Core.Extensions;
using KSSOCA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KSSOCA.Web.Reports
{
    public partial class Form2Certificate : System.Web.UI.Page
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        SeedAnalysisReportService seedAnalysisReportService;
        Form2DetailsService form2DetailsService;
        GeneticPurityReportService geneticPurityReportService;
        public Form2Certificate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            seedAnalysisReportService = new SeedAnalysisReportService();
            form2DetailsService = new Form2DetailsService();
            geneticPurityReportService = new GeneticPurityReportService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            int id = Request.QueryString.Get("Id").ToInt();
            int SSCID = id;
            DecodingService decodingService = new DecodingService();
            SeedSampleCoupon ssc = seedSampleCouponService.GetById(SSCID);
            
            Decoding dec = decodingService.GetBySSCID(SSCID);
            Form1Details f1 = formOneService.ReadById(ssc.Form1Id);
               
            FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
            SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            
            lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
            lblclass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
            lblLotno.Text = ssc.LotNo;
            //lblvalidity.Text = f2.ValidityPeriod.ToString("dd/MM/yyyy");
            lblquantity.Text = ssc.Quantity.ToString();
            lblPackingSize.Text = ssc.PackingSize.ToString();
            lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
            Form2Details f2 = form2DetailsService.GetBy(SSCID);
            lblCertificateNo.Text = Convert.ToString(f2.Form2Number);
            lblvalidity.Text = f2.ValidityPeriod.ToString("dd/MM/yyyy");
            lbltotaltags.Text = f2.TagsIssued;
            lblusedtags.Text = f2.TagsUsed;
            lblunusedtags.Text = f2.TagsUnUsed;
            lblTestNo.Text = dec.Id.ToString();
            SeedAnalysisReport sar = seedAnalysisReportService.GetByLabTestNo(dec.Id);
            lblobjectionable.Text = sar.ObjectionableWeedSeed.ToString();
            lblodv.Text = sar.ODV.ToString();
            lblOtherCrop.Text = sar.OtherCropSeeds.ToString();
            lblpureseed.Text = sar.PureSeed.ToString();
            lblDateofTest.Text = sar.TestedDate?.ToString("dd/MM/yyyy");
            lbldate.Text = DateTime.Now.ToString("dd/MM/yyyy");


            lblSeedMoisture.Text = sar.SeedMoisture.ToString();
            lbltotalweed.Text = sar.TotalWeedSeed.ToString();
            lblInert.Text = sar.InertMatter.ToString();
            lblgermination.Text = sar.Germination.ToString();
            lblGrowerName.Text = fardet.Name;
            lblGrowerVillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name;
            lblGrowerTaluka.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name;
            lblGrowerDistrict.Text = districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name;
            lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
           
            lblstlNo.Text = sar.LabTestNo.ToString();
            lblStlTestDate.Text = sar.TestedDate?.ToString("dd/MM/yyyy");
         
            if (f1.GOTRequiredOrNot == 1)
            {
                GeneticPurityReport gpt = geneticPurityReportService.GetById(ssc.Form1Id);
                if (gpt == null)
                {
                    lblGeneticPurity.Text = "Nil";
                    lblgotNo.Text = "Nil";
                    lblGotTestDate.Text = "Nil";
                    lblGenetic.Text = "Nil";

                }
                else { lblGeneticPurity.Text = gpt.Id.ToString();
                    lblgotNo.Text = gpt.LabTestNo.ToString();
                    lblGotTestDate.Text = gpt.TestedDate?.ToString("dd/MM/yyyy");
                    lblGenetic.Text = gpt.GeneticPurity.ToString();
                }
            }
        }
        protected void btndownload_Click(object sender, EventArgs e)
        {
           
        }
        
    }
}