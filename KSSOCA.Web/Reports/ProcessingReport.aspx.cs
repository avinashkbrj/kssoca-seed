﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using KSSOCA.Core.Helper;
using System.Data;
using System.Data.SqlClient;

namespace KSSOCA.Web.Reports
{
    public partial class ProcessingReport : System.Web.UI.Page
    {
        Common common = new Common();
        SeasonMasterService smService = new SeasonMasterService();
        UserMasterService umService = new UserMasterService();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindGrid();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
            }
        } 
        private void BindGrid()
        {  

            DataTable data = null;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_proc_Report"; 
            data = common.ExecuteStoredProcedure(cmd);

            if (data != null)
            {
                gvSourceVerification.DataSource = data;
                gvSourceVerification.DataBind();
            }
            else
            {
                gvSourceVerification.DataSource = null;
                gvSourceVerification.DataBind();
            }
        } 
        protected void btngetSV_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
            }
        }
        protected void gvSourceVerification_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSourceVerification.PageIndex = e.NewPageIndex;
            gvSourceVerification.DataBind();
        } 
        protected void gvSourceVerification_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //Build custom header.
                GridView oGridView = (GridView)sender;
                GridViewRow oGridViewRow = new GridViewRow(0, 0,
                            DataControlRowType.Header, DataControlRowState.Insert);
                TableHeaderCell oTableCell = new TableHeaderCell();
                oTableCell.Text = "Seed Processing Details Report";
                oTableCell.ColumnSpan = 22;
                oGridViewRow.Cells.Add(oTableCell);

                //oTableCell = new TableHeaderCell();
                //oTableCell.Text = "Seed Certification Charges (in Rs.)";
                //oTableCell.ColumnSpan = 5;
                //oGridViewRow.Cells.Add(oTableCell); 

                //oTableCell = new TableHeaderCell();
                //oTableCell.Text = " ";
                //oTableCell.ColumnSpan = 3;
                //oGridViewRow.Cells.Add(oTableCell);
                oGridView.Controls[0].Controls.AddAt(0, oGridViewRow);
            }
        }
    }
}