﻿namespace KSSOCA.Web.Reports
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Data;
    public partial class SPUList : System.Web.UI.Page
    {
        UserMasterService userMasterService;
        SPURegistrationService spuregistrationService;

        public SPUList()
        {
            userMasterService = new UserMasterService();
            spuregistrationService = new SPURegistrationService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblHeading.Text = "List of Seed Processing Units";
            
        }

        protected void ddlfilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlfilter.SelectedValue == "R")
            {
                pnlsearchbyreg.Visible = true;
                pnlDivision.Visible = false;

            }
            else if (ddlfilter.SelectedValue == "D")
            {
                pnlsearchbyreg.Visible = false;
                pnlDivision.Visible = true;
                ddlDivision.Items.Clear();
                ddlDivision.BindListItem(userMasterService.GetUserList(4));
            }
            else
            {
                pnlsearchbyreg.Visible = false;
                pnlDivision.Visible = false;
                BindGrid(ddlfilter.SelectedValue.Trim(), "", null);
            }
        }

        private void BindGrid(string Type, string RegistrationNo, Nullable<int> DivisionalID)
        {
            var user = userMasterService.GetFromAuthCookie();

            var data = spuregistrationService.GetSPUListBy(Type, RegistrationNo, DivisionalID);
            if (data != null)
            {
                gvProducer.DataSource = data;
                gvProducer.DataBind();
            }
            else
            {
                gvProducer.DataSource = null;
                gvProducer.DataBind();
            }
        }

        protected void btngetformdetails_Click(object sender, EventArgs e)
        {
            BindGrid(ddlfilter.SelectedValue.Trim(), txtRegNo.Text.Trim(), null);
        }

        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGrid(ddlfilter.SelectedValue.Trim(), txtRegNo.Text.Trim(), ddlDivision.SelectedValue.Trim().ToInt());
        }
    }
}