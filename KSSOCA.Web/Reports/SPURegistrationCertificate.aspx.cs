﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using KSSOCA.Core.Helper;
using iTextSharp.text;
using System.IO;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace KSSOCA.Web.Reports
{
    public partial class SPUCertificate : System.Web.UI.Page
    {
        SPURegistrationService SPURegistrationService; Common common = new Common();
        SPURegistrationTransactionService sPURegistrationTransactionService;
        UserMasterService userMasterService; TalukMasterService talukService;
        DistrictMasterService districtMasterService;
        public SPUCertificate()
        {
            SPURegistrationService = new SPURegistrationService();
            sPURegistrationTransactionService = new SPURegistrationTransactionService();
            userMasterService = new UserMasterService(); talukService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {

                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            SPURegistration registrationForm = SPURegistrationService.GetById(id);
                            InitComponent(registrationForm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // this.Redirect("Dashboard.aspx");
            }
        }
        private void InitComponent(SPURegistration rf)
        {
            if (rf.StatusCode == 1)
            {
                var spuuser = userMasterService.GetById(Convert.ToInt32(rf.SPUId));
                SPURegistrationTransaction transaction = new SPURegistrationTransaction();
                transaction = sPURegistrationTransactionService.GetMaxTransactionDetails(rf.Id);
                if (transaction.FromID == 3 && transaction.TransactionStatus == 10)
                {
                    if (rf.RegistrationType == "N")
                    {
                        lblTitle.Text = "SEED PROCESSING UNIT REGISTRATION";
                        fromDate.Text = Convert.ToString(transaction.TransactionDate.Value.ToString("dd/MM/yyyy"));
                    }
                    else if (rf.RegistrationType == "R")
                    {
                        lblTitle.Text = "SEED PROCESSING UNIT RENEWAL";
                        fromDate.Text = "01-08-" + Convert.ToString(Convert.ToDateTime(rf.Validity).Year - 1); //ToFinancialYear().ToString().Substring(0, 4);// System.DateTime.Now.ToFinancialYear().ToString().Substring(5);
                    }
                    lblRegNo.Text = rf.RegistrationNo;
                    lblSPUNo.Text = rf.SPUNO.ToString();
                    lblProducerNameAddress.Text = spuuser.Name + ", " + rf.Address + ", " + talukService.GetBy(Convert.ToInt16(spuuser.District_Id), Convert.ToInt16(spuuser.Taluk_Id)).Name +
                        " Tq, " + districtMasterService.GetById(Convert.ToInt16(spuuser.District_Id)).Name + " Dist.";
                    toDate.Text = Convert.ToString(rf.Validity.Value.ToString("dd/MM/yyyy"));  //rf.ApprovedDate).ToFinancialYear().ToString().Substring(5);
                    lblApprovedDate.Text = Convert.ToString(transaction.TransactionDate.Value.ToString("dd/MM/yyyy"));
                }
                else { this.Redirect("Transactions/SPURegistrationApproval.aspx?Id=" + rf.Id + ""); }
            }
            else
            {
                this.Redirect("Transactions/SPURegistrationApproval.aspx?Id=" + rf.Id + "");
            }
        }

        protected void btndownload_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Panel.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            pnlPrint.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();

        }
    }
}