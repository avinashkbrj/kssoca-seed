﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SamplingReport.aspx.cs" Inherits="KSSOCA.Web.Reports.SamplingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container-fluid">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Seed Sampling Report"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <asp:GridView ID="gvSourceVerification" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="form1Id" HeaderText="Producer Name" />
                        <asp:BoundField DataField="growername" HeaderText="Grower Name" />
                        <asp:BoundField DataField="crop" HeaderText="Crop" />
                        <asp:BoundField DataField="variety" HeaderText="Variety" />
                        <asp:BoundField DataField="class" HeaderText="Class" />
                        <asp:BoundField DataField="LotNo" HeaderText="Lot No" />
                        <asp:BoundField DataField="ProcessedOrNot" HeaderText="Processed Or Not" />
                        <asp:BoundField DataField="test" HeaderText="Test Required" />
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
