﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using KSSOCA.Core.Data;
using KSSOCA.Core.Extensions;

namespace KSSOCA.Web.Reports
{
    public partial class SamplingReport : System.Web.UI.Page
    {
        Common common = new Common();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid(); 
        }
        private void BindGrid()
        {
            DataTable data = null;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_ssc_Report";
            data = common.ExecuteStoredProcedure(cmd);
            if (data != null)
            {
                gvSourceVerification.DataSource = data;
                gvSourceVerification.DataBind();
            }
            else
            {
                gvSourceVerification.DataSource = null;
                gvSourceVerification.DataBind();
            }
        }
    }
}