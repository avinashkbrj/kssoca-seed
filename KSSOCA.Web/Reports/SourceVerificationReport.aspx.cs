﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Model;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using KSSOCA.Core.Helper;
using System.Data;
using System.Data.SqlClient;

namespace KSSOCA.Web.Reports
{
    public partial class SourceVerificationReport : System.Web.UI.Page
    {
        Common common = new Common();
        SeasonMasterService smService = new SeasonMasterService();
        UserMasterService umService = new UserMasterService();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
            }
        }
        private void InitComponent()
        {
            ddlfilter.SelectedValue = "3";
            SetUsers();
            lblHeading.Text = "Source Verification Report";
            var season = smService.GetAll();
            ddlseason.BindListItem<SeasonMaster>(season);

            var financialYear = common.GetFinancialYears();
            ddlfinancialYear.BindDropDown(financialYear, "FinancialYear", "FinancialYear");
            ddlseason.Items.Remove(ddlseason.Items.FindByValue("0"));
            ddlfinancialYear.Items.Remove(ddlfinancialYear.Items.FindByValue("0"));

            var User = umService.GetFromAuthCookie();
            if (User.Role_Id == 1)
            { }
            else if (User.Role_Id == 3)
            { 
                ddlfilter.Enabled = false;
                ddlZone.Enabled = false;
                ddlZone.SelectedValue = User.Id.ToString();
                bindADSC();
            }
            else if (User.Role_Id == 4)
            {
                ddlfilter.Enabled = false;
                ddlZone.Enabled = false;
                ddlADSC.Enabled = false;
                ddlZone.SelectedValue = User.Reporting_Id.ToString();
                bindADSC();
                ddlADSC.SelectedValue = User.Id.ToString();
                bindSCO();
            }
            else if (User.Role_Id == 6)
            {
            } 
        }

        protected void ddlfilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetUsers();
        }

        private void bindZone()
        {
            ddlZone.Items.Clear();
            var userzone = umService.GetUserList(ddlfilter.SelectedValue.ToInt());
            ddlZone.BindListItem<UserMaster>(userzone);
            ddlZone.Items.FindByValue("0").Text = "All";
        }
         
        private void bindADSC()
        {
            ddlADSC.Items.Clear();
            var userADSC = umService.GetByReportingOfficer(ddlZone.SelectedValue.ToInt(), 4);
            ddlADSC.BindListItem<UserMaster>(userADSC);
            ddlADSC.Items.FindByValue("0").Text = "All";
        }
        private void bindSCO()
        {
            ddlSCO.Items.Clear();
            var usersco = umService.GetByReportingOfficer(ddlADSC.SelectedValue.ToInt(), 6);
            ddlSCO.BindListItem<UserMaster>(usersco);
            ddlSCO.Items.FindByValue("0").Text = "All";
        }
        private void SetUsers()
        {
            if (ddlfilter.SelectedValue.Trim().ToInt() == 3)
            {
                pnlZone.Visible = true;
                pnlProducer.Visible = false;
                bindZone();
                bindADSC();
                bindSCO();
            }
            else if (ddlfilter.SelectedValue.Trim().ToInt() == 5)
            {
                pnlZone.Visible = false;
                pnlProducer.Visible = true;

                ddlProducer.Items.Clear();
                var userProducer = umService.GetUserList(ddlfilter.SelectedValue.ToInt());
                ddlProducer.BindListItem<UserMaster>(userProducer);
            }

            if (ddlDuration.SelectedValue == "F")
            {
                pnlFinancialYear.Visible = true;
                pnlsearchbyDate.Visible = false;
            }
            else
            {
                pnlFinancialYear.Visible = false;
                pnlsearchbyDate.Visible = true;
            }
        }

        protected void ddlDuration_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDuration.SelectedValue == "D")
            {
                pnlsearchbyDate.Visible = true;
                pnlFinancialYear.Visible = false;
            }
            else
            {
                pnlsearchbyDate.Visible = false;
                pnlFinancialYear.Visible = true;
            }
        }
        private void BindGrid()
        {
            Nullable<DateTime> fromDate = null, toDate = null;
            if (txtFromDate.Text != "")
            { fromDate = Utility.StartOfDay(Convert.ToDateTime(txtFromDate.Text)); }
            if (txtToDate.Text != "")
            { toDate = Utility.StartOfDay(Convert.ToDateTime(txtToDate.Text)); }

            DataTable data = null;
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sp_SV_Report";
            cmd.Parameters.Add("@RoleID", SqlDbType.Int).Value = getID("R");
            cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = getID("U");
            cmd.Parameters.Add("@PeriodType", SqlDbType.VarChar).Value = ddlDuration.SelectedValue;
            cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = fromDate;
            cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = toDate;
            cmd.Parameters.Add("@SEASON", SqlDbType.Int).Value = ddlseason.SelectedValue.ToInt();
            cmd.Parameters.Add("@FINANCIALYEAR", SqlDbType.VarChar).Value = ddlfinancialYear.SelectedValue.Trim();

            data = common.ExecuteStoredProcedure(cmd);

            if (data != null)
            {
                gvSourceVerification.DataSource = data;
                gvSourceVerification.DataBind();
            }
            else
            {
                gvSourceVerification.DataSource = null;
                gvSourceVerification.DataBind();
            }
        }
        private int getID(string ReturnType)
        {
            int RoleID = 0;
            int UserID = 0;
            if (ddlfilter.SelectedValue.ToInt() == 3)
            {
                if (ddlZone.SelectedValue.ToInt() > 0)
                {
                    if (ddlADSC.SelectedValue.ToInt() > 0)
                    {
                        if (ddlSCO.SelectedValue.ToInt() > 0)
                        {
                            RoleID = 6;
                            UserID = ddlSCO.SelectedValue.ToInt();
                        }
                        else
                        {
                            RoleID = 4; UserID = ddlADSC.SelectedValue.ToInt();
                        }
                    }
                    else
                    {
                        RoleID = 3; UserID = ddlZone.SelectedValue.ToInt();
                    }
                }
                else
                {
                    RoleID = 1;
                    UserID = 3;
                }
            }
            else if (ddlfilter.SelectedValue.ToInt() == 5)
            {
                RoleID = 5;
                UserID = ddlProducer.SelectedValue.ToInt();
            } 
            if (ReturnType == "U")
            {
                return UserID;
            }
            else
            {
                return RoleID;
            }
        }

        protected void btngetSV_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
            }
        }
        protected void gvSourceVerification_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSourceVerification.PageIndex = e.NewPageIndex;
            gvSourceVerification.DataBind();
        }

        protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindADSC();
        }

        protected void ddlADSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindSCO();
        }

        protected void ddlSCO_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void gvSourceVerification_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //Build custom header.
                GridView oGridView = (GridView)sender;
                GridViewRow oGridViewRow = new GridViewRow(0, 0,
                DataControlRowType.Header, DataControlRowState.Insert);
                TableHeaderCell oTableCell = new TableHeaderCell();


                oTableCell.Text = "Report";
                oTableCell.ColumnSpan = 5;
                oGridViewRow.Cells.Add(oTableCell);


                oTableCell = new TableHeaderCell();
                oTableCell.Text = "Total Quantity Of Verified Seed (in Kg)";
                oTableCell.ColumnSpan = 3;
                oGridViewRow.Cells.Add(oTableCell);

                oTableCell = new TableHeaderCell();
                oTableCell.Text = "Probable Area in Acres";
                oTableCell.ColumnSpan = 3;
                oGridViewRow.Cells.Add(oTableCell);

                oTableCell = new TableHeaderCell();
                oTableCell.Text = "Total Area Offered in Acres";
                oTableCell.ColumnSpan = 3;
                oGridViewRow.Cells.Add(oTableCell);

                oTableCell = new TableHeaderCell();
                oTableCell.Text = " ";
                oTableCell.ColumnSpan = 1;
                oGridViewRow.Cells.Add(oTableCell);

                oGridView.Controls[0].Controls.AddAt(0, oGridViewRow);
            }
        }

        
    }
}