﻿
namespace KSSOCA.Web
{
    using System;
    using System.Web;
    using System.Text;
    using System.Web.UI;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Linq;
    using System.Collections.Generic;
    using KSSOCA.Model;

    public partial class Site : System.Web.UI.MasterPage
    {
        UserMasterService userMaster;
        AcessControlService aclDataService;

        public Site()
        {
            userMaster = new UserMasterService();
            aclDataService = new AcessControlService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadMenuItems(); 
        }

        private void LoadMenuItems()
        {
           
            List<ACLDataModel> subMenus;
            StringBuilder sb = new StringBuilder();
            int roleId = HttpContext.Current.User.Identity.IsAuthenticated ? userMaster.GetFromAuthCookie().Role_Id.Value : 0;
            if (roleId > 0)
            {
                divuserinfo.Visible = true;
                lblName.Text = userMaster.GetFromAuthCookie().Name;
                //regno.Text = userMaster.GetFromAuthCookie().Reg_no;
            }
            List<ACLDataModel> menuItems = aclDataService.GetMenuItems(roleId);

            if (userMaster.GetFromAuthCookie() != null)
            {
                if (userMaster.GetFromAuthCookie().Role_Id == 5)
                {
                    if (userMaster.GetFromAuthCookie().Reporting_Id > 0)
                    {
                        foreach (ACLDataModel menu in menuItems)
                        {
                            if (menu.Id == 2)
                            { menu.ShowMenuItem = false; }
                        }
                    }
                }
            }

            foreach (var menuItem in menuItems.Where(m => m.ParentId == null && m.ShowMenuItem).OrderBy(i => i.Ordinal).ToList())
            {
                subMenus = menuItems.FindAll(m => m.ParentId == menuItem.Id && m.ShowMenuItem).OrderBy(i => i.Ordinal).ToList();

                if (subMenus.Count > 0)
                {
                    sb.Append("<li>").AppendLine();
                    sb.AppendFormat("<a href='#'  >{0}</a>", menuItem.Name).AppendLine();
                    sb.Append("<ul >").AppendLine();
                    subMenus.ForEach(m =>
                    {
                        sb.AppendFormat("<li><a href='{0}'>{1}</a></li>", ResolveUrl(m.Url), m.Name).AppendLine();
                    });
                    sb.Append("</ul>").AppendLine();
                    sb.Append("</li>").AppendLine();
                }
                else
                {
                    sb.AppendFormat("<li><a href='{0}'>{1}</a></li>", ResolveUrl(menuItem.Url), menuItem.Name).AppendLine();
                }
            }

            lstMenuItems.InnerHtml = sb.ToString();
        }

        protected void SignInButton_Click(object sender, EventArgs e)
        {
            this.Redirect("Login.aspx", false);
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            this.Redirect("Logout.aspx");
        }
    }
}