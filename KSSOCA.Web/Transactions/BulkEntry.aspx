﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BulkEntry.aspx.cs" Inherits="KSSOCA.Web.Transactions.BulkArriavalEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Seed Bulk Details"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnf1Id" runat="server" />
                    <asp:HiddenField ID="hdnbulkid" runat="server" />
                </div>

                <table class="table-condensed" width="100%" border="1">
                    <%-- <tr class="Note">
						<td colspan="4">
							<asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
						</td>
					</tr>--%>
                    <tr class="SideHeading">
                        <td colspan="4">1. Bulk Details
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Get Bulk Entry Details By :&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlfilter" Width="200px" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                            <asp:ListItem Text="Form-I Registration No." Value="f1"></asp:ListItem>
                            <asp:ListItem Text="Date wise" Value="dt"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="pnlsearchbyf1" runat="server" Visible="true">
                                Enter Form-I Registration No. :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtForm1No" onkeypress="return CheckNumber(event);" placeholder="Form I No" onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ID="txtForm1NoRequiredValidator" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtForm1No" ErrorMessage="Form I No Required" ValidationGroup="v1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
					<asp:Button ID="btngetformdetails" runat="server" Text="Get Form1 Details" CssClass="btn-primary" OnClick="btngetformdetails_Click" ValidationGroup="v1" />
                            </asp:Panel>
                            <asp:Panel ID="pnlsearchbyDate" runat="server" Visible="false">
                                From Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtFromDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtFromDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtFromDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
						   To Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtToDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtToDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtToDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                <asp:Button ID="btngetforms" runat="server" Text="Get Processing Entries" CssClass="btn-primary" ValidationGroup="v3" OnClick="btngetforms_Click" />

                            </asp:Panel>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlform1details" runat="server" Visible="false">
                        <tr>
                            <td colspan="4">
                                <asp:Literal ID="litForm1Details" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr> 
                            <td>Bulk Stock (in Quintal)</td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtBulkStock" onkeypress="return CheckNumber(event);" placeholder="Bulk Stock" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtBulkStock" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtBulkStock" ErrorMessage="Bulk Stock Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                            <td>Bulk Arrival Date<span style="color: red">*</span></td>
                                <td> 
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtBulkArrivalDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtBulkArrivalDate" ErrorMessage="Bulk Arrival Date Required" ></asp:RequiredFieldValidator>
                                </td>

                        </tr>
                         
                        <tr class="SideHeading">
                            <td colspan="4">2. Documents to be uploaded
                            </td>
                        </tr>
                        <tr>

                            <td valign="top">1.Bulk Arrival Letter<span style="color: red">*</span> </td>
                            <td>
                                <asp:FileUpload runat="server" CssClass="form-control" ID="BulkArrivalLetter" accept="image/*" />
                                <asp:RequiredFieldValidator runat="server" ValidationGroup="v2" ID="RequiredBulkArrivalLetter" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="BulkArrivalLetter" ErrorMessage="Bulk Arrival Letter Required"></asp:RequiredFieldValidator>
                                <div>
                                    <cst:FileSizeValidator runat="server" ValidationGroup="v2" ID="BulkArrivalLetterFileSizeValidator" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="BulkArrivalLetter" ErrorMessage="File size must not exceed {0} KB">
                                    </cst:FileSizeValidator>
                                    <cst:FileTypeValidator ID="BulkArrivalLetterTypeValidator" runat="server" ValidationGroup="v2"
                                        ControlToValidate="BulkArrivalLetter"
                                        ForeColor="Red" Display="Dynamic"
                                        ErrorMessage="Allowed file types are {0}.">
                                    </cst:FileTypeValidator>
                                     
                                </div>
                            </td>

                            <td valign="top">2.Undertaking Letter<span style="color: red">*</span> </td>
                            <td>
                                <asp:FileUpload runat="server" CssClass="form-control" ID="UndertakingLetter" accept="image/*" />
                                 <asp:RequiredFieldValidator runat="server" ValidationGroup="v2" ID="RequiredUndertakingLetter" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="UndertakingLetter" ErrorMessage="Undertaking Letter Required"></asp:RequiredFieldValidator>
                                <div>
                                    <cst:FileSizeValidator runat="server" ValidationGroup="v2" ID="FileSizeUndertakingLetter" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="UndertakingLetter" ErrorMessage="File size must not exceed {0} KB">
                                    </cst:FileSizeValidator>
                                    <cst:FileTypeValidator ID="FileTypeUndertakingLetter" runat="server" ValidationGroup="v2"
                                        ControlToValidate="UndertakingLetter"
                                        ForeColor="Red" Display="Dynamic"
                                        ErrorMessage="Allowed file types are {0}.">
                                    </cst:FileTypeValidator>
                                     
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                    ID="Save" Text="Submit" ValidationGroup="v2" OnClick="Save_Click" />
                            </td>
                        </tr>
                    </asp:Panel>

                </table>
                <br />
            </div>
            <div>
                <asp:GridView ID="gvEntries" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvEntries_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No." />
                        <asp:BoundField DataField="GrowerName" HeaderText="Grower Name" />
                        <asp:BoundField DataField="Crop" HeaderText="Crop" />
                        <asp:BoundField DataField="Variety" HeaderText="Variety" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <%-- <asp:BoundField DataField="EstimatedYield" HeaderText="Estimated Yield(in Qntls/Acre)" />
						<asp:BoundField DataField="AcceptedArea" HeaderText="Accepted Area(in acres)" />
						<asp:BoundField DataField="NoOfBags" HeaderText="No Of Bags" />--%>
                        <asp:BoundField DataField="BulkStock" HeaderText="Bulk Stock(in Kg.)" />
                        <%-- <asp:BoundField DataField="SPU" HeaderText="SPU" />
						<asp:BoundField DataField="SentTo" HeaderText="Sent To" />--%>
                        <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" />
                        <%-- <asp:BoundField DataField="AcceptedStock" HeaderText="Accepted Stock(in Kg.)" />
						<asp:BoundField DataField="ArrivalDate" HeaderText="Stock Arrival Date" />--%>
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblReMarks" runat="server" Font-Bold="true" Text='<%# Eval("Remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkdelete" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lnkview" PostBackUrl='<%# "~/Transactions/BulkEntryApproval.aspx?Id="+Eval("ID") %>'>View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------No forms selected-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
            </div>
        </ContentTemplate>
         <Triggers>
   <asp:PostBackTrigger ControlID="Save" />
</Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">


    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
    </script>
</asp:Content>
