﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Data;
    using System.IO;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class BulkArriavalEntry : SecurePage
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        SPURegistrationService SPUMasterService;
        SourceVerificationService sourceverficationservice;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        Common common = new Common();
        FarmerDetailsService farmerDetailsService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        FieldInspectionService fiService;
        BulkEntryService bulkEntryService;
        FieldInspectionService fieldInspectionService;
        BulkEntryTransactionService bulkEntryTransactionService;
        public BulkArriavalEntry()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            SPUMasterService = new SPURegistrationService();
            sourceverficationservice = new SourceVerificationService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            farmerDetailsService = new FarmerDetailsService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            fiService = new FieldInspectionService();
            bulkEntryService = new BulkEntryService();
            fieldInspectionService = new FieldInspectionService();
            bulkEntryTransactionService = new BulkEntryTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            lblMessage.Text = "";
            if (!IsPostBack)
            {
                var user = userMasterService.GetFromAuthCookie();
                //if (user.Role_Id == 5)
                //{
                BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
                // }
            }
        }
        protected void btngetformdetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtForm1No.Text.Trim() != "")
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), txtForm1No.Text.Trim().ToInt(), null, null);
                    if (data != null)
                    {
                        pnlform1details.Visible = false;
                        gvEntries.DataSource = data;
                        gvEntries.DataBind();
                    }
                    else
                    {
                        if (user.Role_Id == 5)
                        {
                            var f1 = formOneService.ReadById(txtForm1No.Text.ToInt());
                            if (f1 != null)
                            {
                                if (f1.Producer_Id == user.Id)
                                {
                                    var far = farmerDetailsService.GetById(f1.FarmerID);
                                    var sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                                    var cm = cropMasterService.GetById(sv.Crop_Id);
                                    var cmv = cropVarietyMasterService.GetById(sv.Variety_Id);
                                    var cls = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced));
                                    var fi = fiService.GetFinalInspection(f1.Id);
                                    if (fi != null)
                                    {
                                        if (fi.StatusCode == 1)
                                        {
                                            litForm1Details.Text = "Grower/Farmer Name:<span style=\"color: Blue;font-weight:bold\">" + far.Name +
                                                             "</span>&nbsp;&nbsp;Grower Registration No:<span style=\"color: Blue;font-weight:bold\">" + f1.Id +
                                                             "</span>  Crop :<span style=\"color: Blue;font-weight:bold\">" + cm.Name +
                                                             "</span>&nbsp;&nbsp;Variety:<span style=\"color: Blue;font-weight:bold\">" + cmv.Name +
                                                             "</span>&nbsp;&nbsp;Class Of the Seed:<span style=\"color: Blue;font-weight:bold\">" + cls.Name +
                                                             "</span>  Estimated Yield (in Quintals):<span style=\"color: Blue;font-weight:bold\">" + fi.EstimatedYield + "</span>";

                                            pnlform1details.Visible = true;
                                            hdnf1Id.Value = f1.Id.ToString();
                                        }
                                        else
                                        {
                                            pnlform1details.Visible = false;
                                            lblMessage.Text = "This Form-I Registration No has been rejected in final field inspection.";
                                        }
                                    }
                                    else { pnlform1details.Visible = false; lblMessage.Text = "Field inspection is not finalised for this Form-I Registration No."; }
                                }
                                else { pnlform1details.Visible = false; lblMessage.Text = "Enter Valid Form-I Registration No."; }

                            }
                            else
                            {
                                lblMessage.Text = "Enter Valid Form-I Registration No.";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Enter Valid Form-I Registration No.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later."; 
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void ddlfilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlfilter.SelectedValue == "f1")
            {
                pnlsearchbyf1.Visible = true;
                pnlsearchbyDate.Visible = false;
            }
            else if (ddlfilter.SelectedValue == "dt")
            {
                pnlsearchbyf1.Visible = false;
                pnlsearchbyDate.Visible = true;
            }
        }
        protected void btngetforms_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BindGrid(0, txtFromDate.Text.ToDateTime().Date, txtToDate.Text.ToDateTime().Date);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                        var user = userMasterService.GetFromAuthCookie();
                        int SPU_ID = formOneService.ReadById(hdnf1Id.Value.ToInt()).SPU_Id; // get SPU_ID associated with form 1
                                                                                            // int spuuserid = Convert.ToInt16(SPUMasterService.GetById(SPU_ID).SPUId); // get SPU owner user id associated with spur
                        int idd = common.GenerateID("BulkEntry");                                                               // int SCOID = Convert.ToInt16(userMasterService.GetById(spuuserid).Reporting_Id); // get SPU owner  reporting id  who is the incharge of SPU.
                        var be = new BulkEntry()
                        {
                            Id = idd,
                            Form1ID = hdnf1Id.Value.ToInt(),
                           // Quintal = txtNoOfBags.Text.ToInt(),
                            BulkStock = txtBulkStock.Text.ToInt(),
                            // Producer_ID = user.Id,
                            SPUId = SPU_ID,
                            BulkArrivalLetter = BulkArrivalLetter.FileBytes.Length > 0 ? BulkArrivalLetter.FileBytes : null,
                            UndertakingLetter = UndertakingLetter.FileBytes.Length > 0 ? UndertakingLetter.FileBytes : null,
                            //  SentTo = SCOID,
                            EntryDate = Convert.ToDateTime(txtBulkArrivalDate.Text),
                            StatusCode = 0,
                        };


                        var result = bulkEntryService.Create(be);
                        if (result > 0)
                        {


                            int ToID = Convert.ToInt32(fieldInspectionService.GetFinalInspection(hdnf1Id.Value.ToInt()).InspectedBy);
                            int TransactionStatus = Convert.ToInt32(userMasterService.GetById(ToID).Role_Id);
                            var svt = new BulkEntryTransaction
                            {
                                Id = common.GenerateID("BulkEntryTransaction"),
                                FromID = user.Id,
                                ToID = ToID,
                                Form1ID = hdnf1Id.Value.ToInt(),
                                TransactionDate = System.DateTime.Now,
                                TransactionStatus = TransactionStatus,
                                IsCurrentAction = 1
                            };
                            int Transactionresult = bulkEntryTransactionService.Create(svt);
                            lblMessage.Text = "Saved successfully.";
                            var f1 = formOneService.ReadById(txtForm1No.Text.ToInt());
                            var farmerDetail = farmerDetailsService.GetById(f1.FarmerID);
                            var svDetails = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                            var cmDetails = cropMasterService.GetById(svDetails.Crop_Id);
                            var cmVariety = cropVarietyMasterService.GetById(svDetails.Variety_Id);
                             
                             
                            Messaging.SendSMS("Bulk Entry of Farmer " + farmerDetail.Name + " Crop "+ cmDetails.Name +" Variety "+ cmVariety.Name + "completed By " + user.Name + "", farmerDetail.MobileNo, "1");

                            pnlform1details.Visible = false;
                            lblMessage.Text = "Bulk Entry saved successfully.";
                            BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
                        }
                        else { lblMessage.Text = "Something went wrong."; }
                    
                    
                
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException;
                ApplicationExceptionHandler.Handle(ex);
                // lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void BindGrid(int form1id, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            var user = userMasterService.GetFromAuthCookie();
            var data = (DataTable)null;
            if (form1id > 0)
            {
                data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, null, null);
            }
            else
            {
                data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, Utility.StartOfDay(Convert.ToDateTime(FromDate)), Utility.EndOfDay(Convert.ToDateTime(ToDate)));
            }
            // var data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, FromDate, ToDate);
            if (data != null)
            {
                gvEntries.DataSource = data;
                gvEntries.DataBind();
            }
            else
            {
                gvEntries.DataSource = null;
                gvEntries.DataBind();
            }
        }



        protected void gvEntries_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var user = userMasterService.GetFromAuthCookie();
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                LinkButton lnkdelete = (e.Row.FindControl("lnkdelete") as LinkButton);
                ///LinkButton lnkUpdate = (e.Row.FindControl("lnkUpdate") as LinkButton);
                if (user.Role_Id == 5)
                {
                    // lnkUpdate.Visible = false;
                }
                else if (user.Role_Id == 6)
                {
                    lnkdelete.Visible = false;
                }
                if (hdnStatusCode.Value.Trim() == "1")
                {
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    lblStatus.Text = "Stock has been accpted.";
                    //lnkUpdate.Visible = false;
                    lnkdelete.Visible = false;
                }
                else if (hdnStatusCode.Value.Trim() == "99")
                {
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    lblStatus.Text = "Stock has been rejected.";
                    //lnkUpdate.Visible = false;
                    lnkdelete.Visible = false;
                }
                else
                {
                    lblStatus.ForeColor = System.Drawing.Color.BurlyWood;
                    lblStatus.Text = "Stock acceptancy details not yet updated.";
                }
            }
        }
    }
}