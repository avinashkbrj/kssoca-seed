﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Data;
    public partial class BulkEntryApproval : SecurePage
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        SPURegistrationService SPUMasterService;
        SourceVerificationService sourceverficationservice;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        Common common = new Common();
        FarmerDetailsService farmerDetailsService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        FieldInspectionService fiService;
        BulkEntryService bulkEntryService;
        FieldInspectionService fieldInspectionService;
        BulkEntryTransactionService bulkEntryTransactionService;

        public BulkEntryApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            SPUMasterService = new SPURegistrationService();
            sourceverficationservice = new SourceVerificationService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            farmerDetailsService = new FarmerDetailsService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            fiService = new FieldInspectionService();
            bulkEntryService = new BulkEntryService();
            fieldInspectionService = new FieldInspectionService();
            bulkEntryTransactionService = new BulkEntryTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = "Bulk Entry";
                //lblHeading.Text = common.getHeading("FRA"); litNote.Text = common.getNote("FRA");
                if (Request.QueryString["Id"] != null) // For Approving  Authorities
                {
                    int BulkId = Request.QueryString.Get("Id").ToInt();
                    InitComponent(BulkId);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
        private void InitComponent(int BulkId)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";

            var user = userMasterService.GetFromAuthCookie();
            var blk = bulkEntryService.GetById(BulkId);
            var f1 = formOneService.ReadById(blk.Form1ID);
            var far = farmerDetailsService.GetById(f1.FarmerID);
            var sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            var cm = cropMasterService.GetById(sv.Crop_Id);
            var cmv = cropVarietyMasterService.GetById(sv.Variety_Id);
            var cls = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced));
            var fi = fiService.GetFinalInspection(f1.Id);
            if (blk.BulkArrivalLetter != null && blk.BulkArrivalLetter.Length > 0)
                LitBulkArrivalLetter.Text = string.Format(previewString, Serializer.Base64String(blk.BulkArrivalLetter));
            if (blk.UndertakingLetter != null && blk.UndertakingLetter.Length > 0)
                LitUndertakingLetter.Text = string.Format(previewString, Serializer.Base64String(blk.UndertakingLetter));
            litForm1Details.Text = "Grower/Farmer Name:<span style=\"color: Blue;font-weight:bold\">" + far.Name +
                             "</span>&nbsp;&nbsp;Grower Registration No:<span style=\"color: Blue;font-weight:bold\">" + f1.Id +
                             "</span>  Crop :<span style=\"color: Blue;font-weight:bold\">" + cm.Name +
                             "</span>&nbsp;&nbsp;Variety:<span style=\"color: Blue;font-weight:bold\">" + cmv.Name +
                             "</span>&nbsp;&nbsp;Class Of the Seed:<span style=\"color: Blue;font-weight:bold\">" + cls.Name +
                             "</span>  Estimated Yield (in Quintals):<span style=\"color: Blue;font-weight:bold\">" + fi.EstimatedYield + "</span>";

            lblSPU.Text = SPUMasterService.GetById(f1.SPU_Id).Name;
            hdnf1Id.Value = f1.Id.ToString();
            hdnbulkid.Value = BulkId.ToString();
            gvstatus.DataSource = bulkEntryTransactionService.GetAllTransactionsBy(f1.Id);
            gvstatus.DataBind();
            lblBulkStock.Text = blk.BulkStock.ToString();
            //lblNoOfBags.Text = blk.Quintal.ToString();
            if (blk.StatusCode == 1)
            {
                pnlBulkAcceptance.Visible = false;
                pnlBulkAcceptanced.Visible = true;

                lblAcceptedQuantity.Text = blk.AcceptedStock.ToString();
                lblbulkAccepted.Text = "Yes";
            }
            else
            {
                if (user.Role_Id == 5)
                {
                    pnlBulkAcceptance.Visible = false;
                    pnlBulkAcceptanced.Visible = false;
                    lblMessage.Text = "Bulk has not yet accepted.";
                }
                else
                {
                    var spuuser = userMasterService.GetById(Convert.ToInt32(SPUMasterService.GetById(blk.SPUId).SPUId));
                    var fieldinspector = userMasterService.GetById(Convert.ToInt32(fi.InspectedBy));
                    int SPUZoneID = Convert.ToInt32(userwiseDistrictRightsService.GetAllBy(Convert.ToInt32(spuuser.District_Id), Convert.ToInt32(spuuser.Taluk_Id), 3).User_Id);
                    int SPUDivisionId = Convert.ToInt32(userwiseDistrictRightsService.GetAllBy(Convert.ToInt32(spuuser.District_Id), Convert.ToInt32(spuuser.Taluk_Id), 4).User_Id);
                    int FieldInspectorZoneID = fieldinspector.Division_Id;
                    int FieldInspectorDivisionId = fieldinspector.Reporting_Id;
                    var transaction = bulkEntryTransactionService.GetMaxTransactionDetails(f1.Id);

                    if (user.Id == fieldinspector.Id && user.Id == transaction.ToID)
                    {
                        if (SPUZoneID == FieldInspectorZoneID)
                        {
                            if (SPUDivisionId == FieldInspectorDivisionId)
                            {
                                if (fieldinspector.Id == spuuser.Reporting_Id)
                                {
                                    pnlBulkAcceptance.Visible = true;
                                    pnlBulkAcceptanced.Visible = false;
                                    hdnSendToID.Value = f1.Producer_Id.ToString();
                                    btnforward.Visible = false; pnlforward.Visible = false;
                                }
                                else
                                {
                                    pnlBulkAcceptance.Visible = false;
                                    pnlBulkAcceptanced.Visible = false;
                                    pnlforward.Visible = true;
                                    btnforward.Visible = true;
                                    hdnSendToID.Value = spuuser.Reporting_Id.ToString();
                                    lblotherdivision.Text = "Forward to " + userMasterService.GetById(spuuser.Reporting_Id).Name + ", The SPU Incharge.";

                                }
                            }
                            else
                            {
                                pnlBulkAcceptance.Visible = false;
                                pnlBulkAcceptanced.Visible = false;
                                btnforward.Visible = true; pnlforward.Visible = true;
                                hdnSendToID.Value = user.Reporting_Id.ToString();
                                lblotherdivision.Text = "Request for bulk transfer to " + userMasterService.GetById(user.Reporting_Id).Name + ", as the spu belongs to other division.";
                                btnforward.Text = "Request";
                            }
                        }
                        else
                        {
                            pnlBulkAcceptance.Visible = false;
                            pnlBulkAcceptanced.Visible = false;
                            btnforward.Visible = true; pnlforward.Visible = true;
                            hdnSendToID.Value = user.Division_Id.ToString();
                            lblotherdivision.Text = "Request for bulk transfer to " + userMasterService.GetById(user.Division_Id).Name + ", as the spu belongs to other Zone.";
                            btnforward.Text = "Request";
                        }
                    }
                    else if (user.Id == transaction.ToID)
                    {
                        //include this 
                        if (user.Role_Id == 6)
                        {
                            pnlBulkAcceptance.Visible = true;
                            pnlBulkAcceptanced.Visible = false;
                            hdnSendToID.Value = f1.Producer_Id.ToString();
                            btnforward.Visible = false; pnlforward.Visible = false;
                        }
                        else
                        {
                            pnlBulkAcceptance.Visible = false;
                            pnlBulkAcceptanced.Visible = false;
                            btnforward.Visible = true; pnlforward.Visible = true;
                            hdnSendToID.Value = spuuser.Reporting_Id.ToString();
                            lblotherdivision.Text = "Approve and forward bulk transfer request to " + userMasterService.GetById(spuuser.Reporting_Id).Name + " ";
                            btnforward.Text = "Approve";
                        }
                    }
                }
            }
        }
        protected void btnBulkAccepted_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int TransactionStatus;
                    var user = userMasterService.GetFromAuthCookie();
                    var be = new BulkEntry()
                    {
                        Id = hdnbulkid.Value.ToInt(),
                        AcceptedStock = txtAccepted.Text.ToInt(),
                        StatusCode = rbIsAccepted.SelectedValue.ToInt(),
                        ArrivalDate = System.DateTime.Now,
                        AcceptedBy = user.Id,
                    };
                    var result = bulkEntryService.Update(be);
                    if (result > 0)
                    {


                        int ToID = hdnSendToID.Value.ToInt();
                        // int TransactionStatus = Convert.ToInt32(userMasterService.GetById(ToID).Role_Id);
                        int statusupdate = rbIsAccepted.SelectedValue.ToInt();
                        if (statusupdate == 1)
                        { TransactionStatus = 5; }
                        else
                        {
                            TransactionStatus = statusupdate;
                        }
                        var svt = new BulkEntryTransaction
                        {
                            Id = common.GenerateID("BulkEntryTransaction"),
                            FromID = user.Id,
                            ToID = ToID,
                            Form1ID = hdnf1Id.Value.ToInt(),
                            TransactionDate = System.DateTime.Now,
                            TransactionStatus = TransactionStatus,
                            IsCurrentAction = 1

                        };
                        int Transactionresult = bulkEntryTransactionService.Create(svt);
                        lblMessage.Text = "Saved successfully.";
                        int BulkId = Request.QueryString.Get("Id").ToInt();
                        var blk = bulkEntryService.GetById(BulkId);
                        var f1 = formOneService.ReadById(blk.Form1ID);
                        var farmer = farmerDetailsService.GetById(f1.FarmerID);
                        var svDetails = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                        var crop = cropMasterService.GetById(svDetails.Crop_Id);
                        var cmvariety = cropVarietyMasterService.GetById(svDetails.Variety_Id);

                        var fi = fiService.GetFinalInspection(f1.Id);
                        if (statusupdate == 1)
                        {
                            Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety.Name + " accepted by " + user.Name + " ", farmer.MobileNo, "1");
                        }
                        else
                        {
                            Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety.Name + " rejected by " + user.Name + " ", farmer.MobileNo, "1");
                        }

                        pnlBulkAcceptance.Visible = false;
                        InitComponent(hdnbulkid.Value.ToInt());

                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
        protected void rbIsAccepted_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbIsAccepted.SelectedValue == "1")
            {
                txtAccepted.Enabled = true;
                txtAccepted.Text = "";
            }
            else
            {
                txtAccepted.Text = "0";
                txtAccepted.Enabled = false;
            }
        }
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            int BulkEntryid = int.Parse((sender as LinkButton).CommandArgument);
            pnlBulkAcceptance.Visible = true;
            hdnbulkid.Value = BulkEntryid.ToString();
            var bulkentry = bulkEntryService.GetById(BulkEntryid);
            hdnf1Id.Value = bulkentry.Form1ID.ToString();
        }
        protected void btnforward_Click(object sender, EventArgs e)
        {
            try
            {
                var user = userMasterService.GetFromAuthCookie();
                int ToID = hdnSendToID.Value.ToInt();
                int TransactionStatus = Convert.ToInt32(userMasterService.GetById(ToID).Role_Id);
                var svt = new BulkEntryTransaction
                {
                    Id = common.GenerateID("BulkEntryTransaction"),
                    FromID = user.Id,
                    ToID = ToID,
                    Form1ID = hdnf1Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    IsCurrentAction = 1
                };
                int Transactionresult = bulkEntryTransactionService.Create(svt);
                lblMessage.Text = "Saved successfully.";
                int BulkId = Request.QueryString.Get("Id").ToInt();
                var blk = bulkEntryService.GetById(BulkId);
                var f1 = formOneService.ReadById(blk.Form1ID);
                var farmer = farmerDetailsService.GetById(f1.FarmerID);
                var svDetails = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                var crop = cropMasterService.GetById(svDetails.Crop_Id);
                var cmvariety = cropVarietyMasterService.GetById(svDetails.Variety_Id);
                var cls = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced));
                var fi = fiService.GetFinalInspection(f1.Id);
                if (user.Role_Id != 5)
                {
                    var spuuser = userMasterService.GetById(Convert.ToInt32(SPUMasterService.GetById(blk.SPUId).SPUId));
                    var fieldinspector = userMasterService.GetById(Convert.ToInt32(fi.InspectedBy));
                    int SPUZoneID = Convert.ToInt32(userwiseDistrictRightsService.GetAllBy(Convert.ToInt32(spuuser.District_Id), Convert.ToInt32(spuuser.Taluk_Id), 3).User_Id);
                    int SPUDivisionId = Convert.ToInt32(userwiseDistrictRightsService.GetAllBy(Convert.ToInt32(spuuser.District_Id), Convert.ToInt32(spuuser.Taluk_Id), 4).User_Id);
                    int FieldInspectorZoneID = fieldinspector.Division_Id;
                    int FieldInspectorDivisionId = fieldinspector.Reporting_Id;
                    var transaction = bulkEntryTransactionService.GetMaxTransactionDetails(f1.Id);

                    if (user.Id == fieldinspector.Id && user.Id == transaction.ToID)
                    {
                        if (SPUZoneID == FieldInspectorZoneID)
                        {
                            if (SPUDivisionId == FieldInspectorDivisionId)
                            {
                                Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety + " forward to " + userMasterService.GetById(spuuser.Reporting_Id).Name + ", The SPU incharge.", farmer.MobileNo, "1");

                            }
                            else
                            {
                                Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety + " request for bulk transfer to " + userMasterService.GetById(user.Reporting_Id).Name + ", as the spu belongs to other division.", farmer.MobileNo, "1");

                            }
                        }
                        else
                        {

                            Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety + " request for bulk transfer to " + userMasterService.GetById(user.Division_Id).Name + ", as the spu belongs to other zone.", farmer.MobileNo, "1");

                        }
                    }
                    else if (user.Id == transaction.ToID)
                    {
                        if (user.Role_Id != 6)
                        {

                            Messaging.SendSMS("Bulk Entry of Farmer- " + farmer.Name + " Crop- " + crop.Name + " Variety- " + cmvariety + " approve and forward bulk transfer request to " + userMasterService.GetById(spuuser.Reporting_Id).Name + "", farmer.MobileNo, "1");


                        }
                    }
                }

                InitComponent(hdnbulkid.Value.ToInt());
                pnlforward.Visible = false;
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
    }
}

