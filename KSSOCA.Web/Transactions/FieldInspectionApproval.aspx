﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FieldInspectionApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.FieldInspectionApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center" class="container">

        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
            <asp:HiddenField ID="Id" runat="server" />
        </div>
         
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr class="Note">
                <td colspan="4">  <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                </td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">1.Field Inspection Details  
                   
                </td>
            </tr>
            <tr>
                <td>1.Inspection Serial No :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblSerialNo" /></td>
                <td>2.Is this inspection is final ? &nbsp;&nbsp;<asp:Label ID="rbIsFinal" Font-Bold="true" runat="server">
                     
                </asp:Label></td>
                <td>3.Last inspection details</td>
                <td>
                    <asp:Label Font-Bold="true" runat="server" ID="lblLastInspectionDetails" /></td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">2.Producer Information
                </td>
            </tr>
            <tr>
                <td width="25%">1.Firm/Producer Name</td>
                <td width="25%">
                    <asp:Label Font-Bold="true" runat="server" ID="lblProducerName" /></td>
                <td width="25%">2.Registration No</td>
                <td width="25%">
                    <asp:Label Font-Bold="true" runat="server" ID="lblProducerRegNo" /></td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">3.Grower Information                       
                </td>
            </tr>

            <tr>
                <td>1.Form-I Registration No.</td>
                <td>
                    <asp:Label Font-Bold="true" runat="server" ID="lblForm1No" /></td>

                <td>2.Grower Registration Date</td>
                <td>
                    <asp:Label Font-Bold="true" runat="server" ID="lblForm1RegDate" /></td>
            </tr>
            <tr>
                <td>3.Name  :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblName" />
                </td>
                <td>4.Father's Name :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="FathersName" />
                </td>
                <td colspan="2">5.Address :&nbsp;&nbsp;a.District :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="District">
                </asp:Label>
                    &nbsp;&nbsp;b.Taluk :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="Taluk">
                    </asp:Label>
                    &nbsp;&nbsp; c.Hobli:&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlHobli">
                    </asp:Label>
                    &nbsp;&nbsp; d.Village :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlvillage">
                    </asp:Label></td>
            </tr>
            <tr>
                <td>6.Mobile No. :&nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="TelephoneOrMobileNumber" />
                </td>
                <td colspan="3">7.Location of Seed plot :&nbsp;:&nbsp;a.Taluk &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotTaluk">
                </asp:Label>
                    &nbsp;&nbsp;b.Hobli :  &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotHobli">
                    </asp:Label>
                    &nbsp;&nbsp;c.Village  &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotVillage">
                    </asp:Label>
                    &nbsp;&nbsp;d.Gram Panchayat :  &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotGramPanchayat" />
                    &nbsp;&nbsp;e.Survey No : 
                        &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotSurveyNumber" />

                </td>
            </tr>
            <tr>
                <td>8.Season :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="Season"></asp:Label></td>
                <td>9.Class of seed offered for certification : &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlClass">
                </asp:Label>
                </td>

                <td>10.Crop :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblCrop" /></td>
                <td>11.Variety :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblVariety" /></td>
            </tr>
            <tr>
                <td>12.Lot No's &nbsp;&nbsp;<asp:GridView ID="gvLotNos" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed Distributed(in Kg.)" />
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
                </td>
                <td>13.Parentage &nbsp;&nbsp;<asp:Label runat="server" ID="txtParentage" />
                </td>

                <td>14.Date of Sowing / Transplanting(As form1): &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ActualDateOfSowingOrTransplanting" />
                </td>
                <td>15.Actual Date of Sowing: &nbsp;&nbsp;<asp:Label runat="server" Font-Bold="true" ID="txtActualDateOfSowing" />
                </td>
            </tr>
            <tr>
                <td>16.Documents Verified: &nbsp;&nbsp;<asp:Label ID="rbDocumentsVerified" Font-Bold="true" runat="server">
                   
                </asp:Label>

                </td>
                <td>17.Crop Stage: &nbsp;&nbsp;<asp:Label ID="rbCropStage" Font-Bold="true" runat="server">
                     
                </asp:Label>
                </td>

                <td>18.Previous Crop:&nbsp;:&nbsp; 
                   <asp:Label ID="lblLastCrop" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td>19.Previous Variety: &nbsp;:&nbsp;
                    <asp:Label ID="lblLastVariety" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>20.Isolation Distance: &nbsp;&nbsp;<asp:Label ID="rbDistanceBetweenSeparation" Font-Bold="true" runat="server">                   
                </asp:Label>
                </td>
                <td>21.Ratio of Male and Female Lines:  &nbsp;&nbsp;<asp:Label runat="server" ID="txtRatioOfLines" Font-Bold="true" /></td>
                <td>22.Male lines around seed plot: &nbsp;&nbsp;<asp:Label runat="server" ID="txtMaleLines" Font-Bold="true" /></td>

                <td>23.Area registered for seed production: &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="AreaOffered" /></td>
            </tr>
            <tr>
                <td>24.Area Inspected(in acres): &nbsp;&nbsp;<asp:Label runat="server" ID="txtAreaInspected" Font-Bold="true" /></td>
                <td>25.Area Rejected(in acres): &nbsp;&nbsp;<asp:Label runat="server" ID="txtAreaRejected" Font-Bold="true" /></td>
                <td>26.Area Accepted(in acres): &nbsp;&nbsp;<asp:Label runat="server" ID="txtAreaAccepted" Font-Bold="true" /></td>
                <td>27.Crop Condition :&nbsp;&nbsp;<asp:Label ID="rbCropStatus" Font-Bold="true" runat="server">                    
                </asp:Label>
                </td>
            </tr>

            <tr class="SideHeading">
                <td colspan="4">28. Field Inspection Details Plant count&nbsp;&nbsp;<asp:Label ID="rbPlantcount" runat="server" Font-Bold="true"> </asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:GridView ID="gvFIDetails" runat="server" AutoGenerateColumns="false" class="table-condensed"
                        AllowPaging="true" PageSize="20">
                        <Columns>
                            <asp:TemplateField HeaderText="Sl.No.">
                                <ItemTemplate>
                                    <asp:Label ID="Id" Font-Bold="true" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Other species of plants Male">
                                <ItemTemplate>
                                    <asp:Label ID="MixedMale" Font-Bold="true" runat="server" Text='<%# Eval("offGenderFemale") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Other species of plants Female">
                                <ItemTemplate>
                                    <asp:Label ID="MixedFemale" Font-Bold="true" runat="server" Text='<%# Eval("offGenderMale") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns> 
                        <Columns>
                            <asp:TemplateField HeaderText="Other species of Straight Variety">
                                <ItemTemplate>
                                    <asp:Label ID="MixedStraight" Font-Bold="true" runat="server" Text='<%# Eval("offPollenFemaleLines") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Plants ready for pollination on female lines">
                                <ItemTemplate>
                                    <asp:Label ID="FemaleReadyToPollen" Font-Bold="true" runat="server" Text='<%# Eval("offPollenFemalePlants") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Spruce plants that pollinate pollen on female lines">
                                <ItemTemplate>
                                    <asp:Label ID="FemaleSparklingPallen" Font-Bold="true" runat="server" Text='<%# Eval("offReceptive") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Other species that pollinate pollen">
                                <ItemTemplate>
                                    <asp:Label ID="OthersPollening" Font-Bold="true" runat="server" Text='<%# Eval("offVariety") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Diseased Male Plant">
                                <ItemTemplate>
                                    <asp:Label ID="DiseasedMale" runat="server" Font-Bold="true" Text='<%# Eval("seedGenderFemale") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Diseased Female Plant">
                                <ItemTemplate>
                                    <asp:Label ID="DiseasedFemale" runat="server" Font-Bold="true" Text='<%# Eval("seedGenderMale") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Diseased Straight variety plant">
                                <ItemTemplate>
                                    <asp:Label ID="DiseasedStraight" runat="server" Font-Bold="true" Text='<%# Eval("seedOtherCrop") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Plants Cannot Separate">
                                <ItemTemplate>
                                    <asp:Label ID="CannotSeparate" runat="server" Font-Bold="true" Text='<%# Eval("seedVariety") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Objectionable Plants">
                                <ItemTemplate>
                                    <asp:Label ID="Objectionable" runat="server" Font-Bold="true" Text='<%# Eval("seedWeedPlants") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>

            <tr>
                <td>29.Status of Seed Plot: &nbsp;&nbsp;<asp:Label ID="rbquality" runat="server" Font-Bold="true">                  
                </asp:Label>
                </td>
                <td>30.Estimated Crop Yield (In Qntls):&nbsp;&nbsp;<asp:Label runat="server" ID="txtEstimatedCropYeild" Font-Bold="true" /></td>
                <td colspan="2">31.Harvesting Month/Portrait :&nbsp;&nbsp;<asp:Label runat="server" ID="txtHarvestingDate" Font-Bold="true" />
                </td>

            </tr>
            <tr>
                <td colspan="2">32.lattitute:&nbsp;&nbsp;<asp:Label ID="lbllat" runat="server" Font-Bold="true"> </asp:Label></td>
                <td colspan="2">33.longitude:&nbsp;&nbsp;<asp:Label ID="lbllong" runat="server" Font-Bold="true"> </asp:Label></td>

            </tr>
            <tr>
                <td colspan="4">34.Suggestions
                    <asp:CheckBoxList ID="chkSuggestion" runat="server"></asp:CheckBoxList></td>
                 
                 
                    
            </tr>
            <tr>
                <td>35.Field Image</td>
                <td>
                    <asp:Literal runat="server" ID="LitFieldImage" EnableViewState="false"></asp:Literal>
                </td>
                <td>36.Field Image<asp:Literal runat="server" ID="LiteralSelfieWithFarmerImage" EnableViewState="false"></asp:Literal></td>
                <td>
                  37.Remarks:  <asp:Label ID="txtremarks" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
            <tr>
                <td>38.Inspection Date</td>
                <td>
                    <asp:Label ID="lblinspectiondate" runat="server" Font-Bold="true"></asp:Label>
                </td>
                <td>39.Inspected By</td>
                <td>
                    <asp:Label ID="lblInspectionBy" runat="server" Font-Bold="true"></asp:Label></td>
            </tr>
        </table>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
