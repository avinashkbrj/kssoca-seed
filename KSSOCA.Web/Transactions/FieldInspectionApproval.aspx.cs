﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Data;
    public partial class FieldInspectionApproval : System.Web.UI.Page
    {
        UserMasterService userMasterService;
        BankMasterService bankMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService; Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService; F1LotNumbersService f1LotNumbersService;
        FieldInspectionSuggestionMasterService fieldInspectionSuggestionMasterService;

        public FieldInspectionApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            bankMasterService = new BankMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            f1LotNumbersService = new F1LotNumbersService();
            fieldInspectionSuggestionMasterService = new FieldInspectionSuggestionMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("FIA");
                litNote.Text = common.getNote("FIA");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int FieldInspectionID = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["FieldInspectionID"] = FieldInspectionID;
                    FieldInspection fi = fieldInspectionService.ReadById(FieldInspectionID);
                    InitComponent(fi);
                }
                else
                {
                    this.Redirect("Dashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(FieldInspection fi)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";

            //Grower Information
            Id.Value = fi.Id.ToString();
            lblSerialNo.Text = fi.InspectionSerialNo.ToString();
            if (fi.IsFinal == 1)
            { rbIsFinal.Text = "Final report"; }
            else { rbIsFinal.Text = "Not a final report"; }
            txtActualDateOfSowing.Text = fi.ActualDateOfSowing.ToString("dd/MM/yyyy");
            rbDocumentsVerified.Text = fi.DocumentsInvestigated;
            rbCropStage.Text = fi.CropStage;
            lblLastCrop.Text = fi.LastCrop;
            lblLastVariety.Text = fi.LastCropVariety;
            txtRatioOfLines.Text = Convert.ToString(fi.FemaleMaleLinesRatio);
            txtMaleLines.Text = fi.MaleLines != null ? Convert.ToString(fi.MaleLines) : "";
            rbDistanceBetweenSeparation.Text = Convert.ToString(fi.DistanceBetweenLines);
            txtAreaInspected.Text = Convert.ToString(fi.InspectedArea);
            txtAreaRejected.Text = Convert.ToString(fi.RejectedArea);
            txtAreaAccepted.Text = Convert.ToString(fi.AcceptedArea);
            rbPlantcount.Text = Convert.ToString(fi.PlantCount);
            rbCropStatus.Text = Convert.ToString(fi.StateOfCrop);
            rbquality.Text = Convert.ToString(fi.Quality);
            txtEstimatedCropYeild.Text = Convert.ToString(fi.EstimatedYield);
            txtHarvestingDate.Text = fi.HarvestingMonth != null ? fi.HarvestingMonth.ToString() + " /" + fi.HarvestingFortNight : " ";
            txtremarks.Text = Convert.ToString(fi.Remarks);
            lblinspectiondate.Text = fi.InspectedDate.Value.ToString("dd/MM/yyyy");
            lblInspectionBy.Text = userMasterService.GetById(Convert.ToInt16(fi.InspectedBy)).Name;
            lbllat.Text = fi.lattitude;
            lbllong.Text = fi.Longitude;
            if (fi.FieldImage != null && fi.FieldImage.Length > 0)
                LitFieldImage.Text = string.Format(previewString, Serializer.Base64String(fi.FieldImage));
            if (fi.SelfieWithFarmerImage != null && fi.SelfieWithFarmerImage.Length > 0)
                LiteralSelfieWithFarmerImage.Text = string.Format(previewString, Serializer.Base64String(fi.SelfieWithFarmerImage));
            //get form 1 details
            Form1Details f1 = formOneService.ReadById(fi.Form1Id);
            lblForm1No.Text = f1.Id.ToString();
            txtParentage.Text = fi.Parentage;
            FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
            lblName.Text = fardet.Name;
            FathersName.Text = fardet.FatherName;
            District.Text = districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name;
            Taluk.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name;
            ddlHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name;
            ddlvillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name;
            TelephoneOrMobileNumber.Text = fardet.MobileNo;
            ddlPlotTaluk.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id)).Name; ;
            ddlPlotHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id)).Name; ;
            ddlPlotVillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id), Convert.ToInt16(f1.PlotVillage_Id)).Name; ;
            LocationOfTheSeedPlotGramPanchayat.Text = f1.PlotGramPanchayath;
            LocationOfTheSeedPlotSurveyNumber.Text = f1.SurveyNo.ToString();
            ddlClass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
            AreaOffered.Text = f1.Areaoffered.ToString();
            ActualDateOfSowingOrTransplanting.Text = f1.ActualDateofSowing != null ? f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy") : "";
            Season.Text = seasonMasterService.GetById(Convert.ToInt16(f1.Season_Id)).Name;
            lblForm1RegDate.Text = f1.RegistrationDate.Value.ToString("dd/MM/yyyy");
            // GetLastInspectionDetails(f1.Id);
            gvLotNos.DataSource = f1LotNumbersService.GetByF1Id(f1.Id);
            gvLotNos.DataBind();
            if (Convert.ToString(fi.Suggestions).Trim() != "")
            {
                var Suggestion = fieldInspectionSuggestionMasterService.GetSuggestions(fi.Suggestions);
                chkSuggestion.BindCheckBoxItem<FieldInspectionSuggestionMaster>(Suggestion);
                for (int counter = 0; counter < chkSuggestion.Items.Count; counter++)
                {
                    chkSuggestion.Items[counter].Selected = true;
                }
                chkSuggestion.Enabled = false;
            }
 
            // SourceVerification Information
            SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
            //lblLotNo.Text = sv.LotNo;

            RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
            lblProducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
            lblProducerRegNo.Text = rf.RegistrationNo;
            BindCropDetailsData(fi.Id);
        }
        private void BindCropDetailsData(int FieldInspectionID)
        {
            string query = "SELECT * FROM  FieldInspectionCropDetails where FieldInspectionId=" + FieldInspectionID;
            DataTable dt = common.GetData(query);
            if (dt.Rows.Count > 0)
            {
                gvFIDetails.DataSource = dt;
                gvFIDetails.DataBind();
            }
        }
    }
}