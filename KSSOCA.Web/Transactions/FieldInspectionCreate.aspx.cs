﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using System.Web.UI.HtmlControls;
    using System.Globalization;

    // using FIR_Service;

    public partial class FieldInspectionCreate : SecurePage
    {
        int otpValue = 0;
        UserMasterService userMasterService;
        BankMasterService bankMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService; Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FieldInspectionSuggestionMasterService fieldInspectionSuggestionMasterService;
        FarmerDetailsService farmerDetailsService;
        F1LotNumbersService f1LotNumbersService;
        public FieldInspectionCreate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            bankMasterService = new BankMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            fieldInspectionSuggestionMasterService = new FieldInspectionSuggestionMasterService();
            farmerDetailsService = new FarmerDetailsService();
            f1LotNumbersService = new F1LotNumbersService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("FIC");
                litNote.Text = common.getNote("FIC"); // Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    if (!IsPostBack)
                    {
                        int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                        Id.Value = Form1ID.ToString();
                        Form1Details f1 = formOneService.ReadById(Form1ID);
                        if (f1 != null)
                        {
                            InitComponent(f1);
                        }
                        else
                        {
                            this.Redirect("Transactions/FieldInspectionView.aspx");
                        }
                    }
                }
                else
                {
                    this.Redirect("Dashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(Form1Details f1)
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.GetInstance(null);
            for (int i = 1; i < 13; i++)
            {
                ddlmonth.Items.Add(new ListItem(info.GetMonthName(i), info.GetMonthName(i)));
            }
            Id.Value = f1.Id.ToString();
            lblForm1No.Text = f1.Id.ToString();
            FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
            lblName.Text = fardet.Name;
            FathersName.Text = fardet.FatherName;
            District.Text = districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name;
            Taluk.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name;
            ddlHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name;
            ddlvillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name;
            TelephoneOrMobileNumber.Text = fardet.MobileNo;
            ddlPlotTaluk.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id)).Name; ;
            ddlPlotHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id)).Name; ;
            ddlPlotVillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id), Convert.ToInt16(f1.PlotVillage_Id)).Name; ;
            LocationOfTheSeedPlotGramPanchayat.Text = f1.PlotGramPanchayath;
            LocationOfTheSeedPlotSurveyNumber.Text = f1.SurveyNo.ToString();
            ddlClass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
            AreaOffered.Text = f1.Areaoffered.ToString();
            ActualDateOfSowingOrTransplanting.Text = f1.ActualDateofSowing != null ? f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy") : "";
            Season.Text = seasonMasterService.GetById(Convert.ToInt16(f1.Season_Id)).Name;
            lblForm1RegDate.Text = f1.RegistrationDate.Value.ToString("dd/MM/yyyy");
            // SourceVerification Information
            SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
            // lblLotNo.Text = sv.LotNo;
            RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
            lblProducerName.Text = rf.OfficialName;
            lblProducerRegNo.Text = rf.RegistrationNo;
            GetLastInspectionDetails(f1.Id);
            BindCropDetailsData();
            var Suggestion = fieldInspectionSuggestionMasterService.GetAll();
            chkSuggestion.BindCheckBoxItem<FieldInspectionSuggestionMaster>(Suggestion);
            //Get last inspection details
            gvLotNos.DataSource = f1LotNumbersService.GetByF1Id(f1.Id);
            gvLotNos.DataBind();
            FieldInspection fi = fieldInspectionService.GetLastInspection(f1.Id);
            if (fi != null)
            {
                //txtActualDateOfSowing.Text = fi.ActualDateOfSowing.ToString("dd/MM/yyyy");
                //rbDocumentsVerified.Items.FindByText(fi.DocumentsInvestigated.ToString().Trim()).Selected = true;
                //// rbCropStage.Items.FindByText(fi.CropStage.ToString().Trim()).Selected = true;
                //txtlastcrop.Text = fi.LastCrop;
                //txtLastVariety.Text = fi.LastCropVariety;
                //txtRatioOfLines.Text = Convert.ToString(fi.FemaleMaleLinesRatio);
                //txtMaleLines.Text = fi.MaleLines != null ? Convert.ToString(fi.MaleLines) : "";
                //// rbDistanceBetweenSeparation.Items.FindByText(fi.DistanceBetweenLines.ToString().Trim()).Selected = true;
                //txtAreaInspected.Text = Convert.ToString(fi.InspectedArea);
                //txtAreaRejected.Text = Convert.ToString(fi.RejectedArea);
                //txtAreaAccepted.Text = Convert.ToString(fi.AcceptedArea);
                ////rbPlantcount.Items.FindByText(fi.PlantCount.ToString().Trim()).Selected = true;
                ////rbCropStatus.Items.FindByText(fi.StateOfCrop.ToString().Trim()).Selected = true;
                ////rbquality.Items.FindByText(fi.Quality.ToString().Trim()).Selected = true;
                //txtEstimatedCropYeild.Text = Convert.ToString(fi.EstimatedYield);
                ////ddlmonth.SelectedValue=
                ////txtHarvestingDate.Text = fi.HarvestingMonth != null ? fi.HarvestingMonth.ToString() : "";
                //txtParentage.Text = fi.Parentage;
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
                    int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                    string Suggestions = common.getCheckedItems(chkSuggestion);

                    var FI = new FieldInspection()
                    {
                        Id = common.GenerateID("FieldInspection"),
                        InspectionSerialNo = lblSerialNo.Text.ToInt(),
                        IsFinal = rbIsFinal.SelectedValue.ToInt(),
                        Form1Id = Form1ID,
                        ActualDateOfSowing = txtActualDateOfSowing.Text.ToDateTime(),
                        DocumentsInvestigated = rbDocumentsVerified.SelectedItem.Text,
                        CropStage = rbCropStage.SelectedItem.Text,
                        LastCrop = txtlastcrop.Text,
                        LastCropVariety = txtLastVariety.Text,
                        FemaleMaleLinesRatio = txtRatioOfLines.Text,
                        MaleLines = txtMaleLines.Text.ToNullableInt(),
                        DistanceBetweenLines = rbDistanceBetweenSeparation.SelectedItem.Text,
                        InspectedArea = txtAreaInspected.Text != "" ? txtAreaInspected.Text.ToDecimal() : "0".ToDecimal(),
                        RejectedArea = txtAreaRejected.Text != "" ? txtAreaRejected.Text.ToDecimal() : "0".ToDecimal(),
                        AcceptedArea = txtAreaAccepted.Text != "" ? txtAreaAccepted.Text.ToDecimal() : "0".ToDecimal(),
                        PlantCount = rbPlantcount.SelectedValue.ToInt(),
                        StateOfCrop = rbCropStatus.SelectedItem.Text,
                        Quality = rbquality.SelectedItem.Text,
                        EstimatedYield = txtEstimatedCropYeildMin.Text + " to " + txtEstimatedCropYeild.Text,
                        HarvestingMonth = ddlmonth.SelectedValue,
                        Suggestions = Suggestions,
                        Remarks = txtremarks.Text,
                        FieldImage = FieldImage.FileBytes,
                        InspectedBy = userMaster.Id,
                        InspectedDate = System.DateTime.Now,
                        Parentage = txtParentage.Text,
                        AreaRejectedDueTo = common.getCheckedItems(chkarearejecteddueto),
                        HarvestingFortNight = rbportrait.SelectedValue.Trim(),
                        lattitude = txtlatt.Text.Trim(),
                        Longitude = txtlong.Text.Trim(),
                        StatusCode = rbacceptreject.SelectedValue.ToInt(),
                        SelfieWithFarmerImage = SelefiWithFarmer.FileBytes 
                    };
                    var result = fieldInspectionService.Create(FI);
                    if (result > 0)
                    {
                        string query = "select Max(Id) from FieldInspection where Form1Id=" + Id.Value + "";
                        int ID = Convert.ToInt16(common.ExecuteScalar(query));
                        InsertCropDetailsData(ID);
                        string mobileno = userMasterService.GetById(formOneService.ReadById(Id.Value.ToInt()).Producer_Id).MobileNo;
                        var farmerDetails = farmerDetailsService.GetById(formOneService.ReadById(Id.Value.ToInt()).FarmerID);
                        Messaging.SendSMS("Field Inspection for Form1 number-" + Id.Value.ToString() + " is successfully completed by " + userMaster.Name + "", mobileno, "1");
                        Messaging.SendSMS("Field Inspection of Farmer-" + farmerDetails.Name + " is successfully completed by " + userMaster.Name + "", farmerDetails.MobileNo, "1");

                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Field inspection records saved successfully.";
                        this.Redirect("Transactions/FieldInspectionApproval.aspx?Id=" + ID);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }
                else
                {
                    lblMessage.Text = "Validation error occurred.";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        private void InsertCropDetailsData(int FieldInspectionId)
        {
            foreach (GridViewRow row in gvFIDetails.Rows)
            {
                Label lblID = (Label)row.FindControl("lblID");
                TextBox MixedMale = (TextBox)row.FindControl("MixedMale");
                TextBox MixedFemale = (TextBox)row.FindControl("MixedFemale");
                TextBox MixedStraight = (TextBox)row.FindControl("MixedStraight");
                TextBox FemaleReadyToPollen = (TextBox)row.FindControl("FemaleReadyToPollen");
                TextBox FemaleSparklingPallen = (TextBox)row.FindControl("FemaleSparklingPallen");
                TextBox OthersPollening = (TextBox)row.FindControl("OthersPollening");
                TextBox DiseasedMale = (TextBox)row.FindControl("DiseasedMale");
                TextBox DiseasedFemale = (TextBox)row.FindControl("DiseasedFemale");
                TextBox DiseasedStraight = (TextBox)row.FindControl("DiseasedStraight");
                TextBox CannotSeparate = (TextBox)row.FindControl("CannotSeparate");
                TextBox Objectionable = (TextBox)row.FindControl("Objectionable");
                if (MixedMale.Text == string.Empty) { MixedMale.Text = "0"; }
                if (MixedFemale.Text == string.Empty) { MixedFemale.Text = "0"; }
                if (MixedStraight.Text == string.Empty) { MixedStraight.Text = "0"; }
                if (FemaleReadyToPollen.Text == string.Empty) { FemaleReadyToPollen.Text = "0"; }
                if (FemaleSparklingPallen.Text == string.Empty) { FemaleSparklingPallen.Text = "0"; }
                if (OthersPollening.Text == string.Empty) { OthersPollening.Text = "0"; }
                if (DiseasedMale.Text == string.Empty) { DiseasedMale.Text = "0"; }
                if (DiseasedFemale.Text == string.Empty) { DiseasedFemale.Text = "0"; }
                if (DiseasedStraight.Text == string.Empty) { DiseasedStraight.Text = "0"; }
                if (CannotSeparate.Text == string.Empty) { CannotSeparate.Text = "0"; }
                if (Objectionable.Text == string.Empty) { Objectionable.Text = "0"; }
                string query = @"insert into FieldInspectionCropDetails 
                                 values('" + lblID.Text.Trim() + "','"
                                 + FieldInspectionId + "','"
                                 + MixedMale.Text + "','"
                                 + MixedFemale.Text + "','"
                                 + MixedStraight.Text + "','"
                                 + FemaleReadyToPollen.Text + "','"
                                 + FemaleSparklingPallen.Text + "','"
                                 + OthersPollening.Text + "','"
                                 + DiseasedMale.Text + "','"
                                 + DiseasedFemale.Text + "','"
                                 + DiseasedStraight.Text + "','"
                                 + CannotSeparate.Text + "','"
                                 + Objectionable.Text + "')";
                common.ExecuteNonQuery(query);
            }
        }
        private void BindCropDetailsData()
        {
            string query = "SELECT top 12 [Id] FROM  BlankRowSelection";
            DataTable dt = common.GetData(query);
            if (dt.Rows.Count > 0)
            {
                gvFIDetails.DataSource = dt;
                gvFIDetails.DataBind();
            }
        }
        private void GetLastInspectionDetails(int Form1Id)
        {
            int InspectionSerialNo = 0;
            string query1 = "SELECT *  FROM  FieldInspection where Form1Id=" + Form1Id + "";
            DataTable dt = common.GetData(query1);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    lblLastInspectionDetails.Text += dt.Rows[i]["InspectionSerialNo"] + ":" + Convert.ToDateTime(dt.Rows[i]["InspectedDate"]).ToString("dd/MM/yyyy") + " ";
                }
                string query = "SELECT ISNULL(Max(InspectionSerialNo), 0)   FROM  FieldInspection where Form1Id=" + Form1Id + "";
                InspectionSerialNo = Convert.ToInt16(common.ExecuteScalar(query));
                if (InspectionSerialNo == 3)
                {
                    rbIsFinal.SelectedValue = "Y";
                    rbIsFinal.Enabled = false;
                }
                else if (InspectionSerialNo == 4)
                {
                    this.Redirect("Transactions/FieldInspectionView.aspx?Id=" + Form1Id);
                }
            }
            lblSerialNo.Text = Convert.ToString(InspectionSerialNo + 1);
        }

        protected void gvFIDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
       
    }
}