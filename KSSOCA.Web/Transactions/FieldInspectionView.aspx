﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FieldInspectionView.aspx.cs" Inherits="KSSOCA.Web.Transactions.FieldInspectionView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" style="min-height: 600px;">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnForm1ID" runat="server" />
                </div>
                
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td>
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div align="left" id="divTxtform1" runat="server">
                                Enter Form-I Registration No :&nbsp;<asp:TextBox runat="server" ID="txtFormNo" placeholder=" Form-I Registration No" onkeypress="return CheckNumber(event);" />&nbsp;
                                <asp:Button runat="server" ID="btngetInspection" class="btn-primary" Text="Get Inspection Details" OnClick="btngetInspection_Click" />&nbsp;&nbsp;
                     <asp:Button runat="server" ID="Add" class="btn-primary" OnClick="Add_Click" Text="Enter Field Inspection Report" Visible="false" />
                            </div>
                        </td>
                    </tr>
                </table>

                <br />
                <div id="divGridview" runat="server" visible="false">
                    <asp:GridView runat="server" ID="FieldInspectionGridView"
                        AutoGenerateColumns="false" class="table-condensed" Width="100%"
                        AllowPaging="true" PageSize="25" OnRowDataBound="FieldInspectionGridView_RowDataBound" OnPageIndexChanging="FieldInspectionGridView_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Sl.No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %> .
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="InspectionSerialNo" HeaderText="Serial No" />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="Id" HeaderText="FIR No." />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="Form1Id" HeaderText="Form-I Registration No." />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name of the Farmer" />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="MobileNumber" HeaderText="Mobile No" />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="District" HeaderText="District" />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="Taluk" HeaderText="Taluk" />
                        </Columns>
                        <Columns>
                            <asp:BoundField DataField="InspectedDate" HeaderText="Inspected Date" />
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("IsFinal") %>' />
                                    <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="lnkView" PostBackUrl='<%# "~/Transactions/FieldInspectionApproval.aspx?Id=" + Eval("Id") %>'>View</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
