﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    public partial class FieldInspectionView : System.Web.UI.Page
    {
        FormOneDetailsService form1Service;
        UserMasterService userMasterService;
        SourceVerificationService sourceVerificationService;
        FieldInspectionService fieldInspectionService;
        Common common = new Common();
        public FieldInspectionView()
        {
            form1Service = new FormOneDetailsService();
            userMasterService = new UserMasterService();
            sourceVerificationService = new SourceVerificationService();
            fieldInspectionService = new FieldInspectionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                lblHeading.Text = common.getHeading("FIV"); litNote.Text = common.getNote("FIV");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                    hdnForm1ID.Value = Form1ID.ToString();
                    getFormDetails(Form1ID);
                }
                else
                {
                    // divTxtform1.Visible = true;
                }
                InitComponent();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                //Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }

        }
        private void InitComponent()
        {
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
            if (roleId == 5)
            {
                Add.Visible = false;
            }
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            int Form1ID = Convert.ToInt16(hdnForm1ID.Value);
            this.Redirect("Transactions/FieldInspectionCreate.aspx?Id=" + Form1ID);
        }
        public bool DeleteMethod(int id)
        {
            int deletedRowCount = form1Service.Delete(id);
            return deletedRowCount > 0 ? true : false;
        }
        public DataTable SelectMethod()
        {
            var data = (DataTable)null;
            if (hdnForm1ID.Value != "")
            {
                var user = userMasterService.GetFromAuthCookie();
                data = fieldInspectionService.ReadByForm1ID(Convert.ToInt16(hdnForm1ID.Value));
                if (data != null)
                {

                    return data;
                }
                else
                    lblMessage.Text = "Inspections not found.";
                return (DataTable)null;
            }
            else
                return (DataTable)null;

        }
        protected void FieldInspectionGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // LinkButton lnkDelete = (e.Row.FindControl("lnkDelete") as LinkButton);

                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
                if (roleId == 6 || roleId == 4)
                {
                    //lnkDelete.Visible = true;
                }
                else
                {
                    //lnkDelete.Visible = false;
                }
                if (hdnStatusCode.Value == "1")
                {
                    Add.Visible = false;
                    lblStatus.Text = "Finalised Report";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    // lnkDelete.Visible = false;
                }
                else
                {
                    lblStatus.Text = "";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        protected void btngetInspection_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFormNo.Text != "")
                {
                    getFormDetails(txtFormNo.Text.ToInt());
                }
                else { lblMessage.Text = "Please enter form1 number."; }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex); this.Redirect("Error.aspx");
            }
        }
        protected void FieldInspectionGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FieldInspectionGridView.PageIndex = e.NewPageIndex;
            FieldInspectionGridView.DataSource = SelectMethod();
            FieldInspectionGridView.DataBind();
        }
        private void getFormDetails(int form1ID)
        {
            var user = userMasterService.GetFromAuthCookie();
            Form1Details f1 = form1Service.ReadByStatus(form1ID);
            if (f1 != null)
            {
                if (user.Role_Id == 5)
                {
                    Add.Visible = false;
                    if (user.Id == f1.Producer_Id)
                    {
                        divGridview.Visible = true;
                    }
                    else
                    {
                        divGridview.Visible = false;
                        lblMessage.Text = "You are not authorised to view this form details.";
                    }
                }
                else if (user.Role_Id == 6 || user.Role_Id == 4)
                {
                    Add.Visible = true;
                    divGridview.Visible = true;
                }
                hdnForm1ID.Value = form1ID.ToString();
                FieldInspectionGridView.DataSource = SelectMethod();
                FieldInspectionGridView.DataBind();
            }
            else
            {
                lblMessage.Text = "Enter valid form 1 number";
                Add.Visible = false;
                FieldInspectionGridView.DataSource = null;
                FieldInspectionGridView.DataBind();
            }
        }
    }
}