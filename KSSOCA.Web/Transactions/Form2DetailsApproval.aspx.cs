﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    public partial class Form2DetailsApproval : System.Web.UI.Page
    {
        int SSCID;
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        SeedAnalysisReportService seedAnalysisReportService;
        Form2DetailsService form2DetailsService;
        GeneticPurityReportService geneticPurityReportService;
        public Form2DetailsApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            seedAnalysisReportService = new SeedAnalysisReportService();
            form2DetailsService = new Form2DetailsService();
            geneticPurityReportService = new GeneticPurityReportService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    lblMessage.Text = "";
                    lblHeading.Text = common.getHeading("F2");
                    litNote.Text = common.getNote("F2");// Short name of the page
                    if (Request.QueryString["Id"] != null)
                    {
                        SSCID = Convert.ToInt16(Request.QueryString["Id"]);
                        Session["SSCID"] = SSCID;
                        InitComponent(SSCID);
                    }
                    else
                    {
                        // this.Redirect("Dashboard.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(int SSCID)
        {

            DecodingService decodingService = new DecodingService();
            SeedSampleCoupon ssc = seedSampleCouponService.GetById(SSCID);
            Form2Details f2 = form2DetailsService.GetBy(SSCID);
            UserMaster user = userMasterService.GetFromAuthCookie();
            if (ssc != null)
            {
                if ((f2.CertificateImage != null) || (f2.CertificateImage == null && user.Role_Id ==6))
                {
                    string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";

                    Decoding dec = decodingService.GetBySSCID(SSCID);
                    Form1Details f1 = formOneService.ReadById(ssc.Form1Id);

                    lblPackingSize.Text = ssc.PackingSize.ToString();
                    if (f1 != null)
                    {
                        FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                        SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                        RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                        FieldInspection fi = fieldInspectionService.GetFinalInspection(ssc.Form1Id);
                        if (fi != null)
                        {
                            //System.Collections.Generic.List<SeedSampleCoupon> ssc = seedSampleCouponService.GetBy(Form1ID);
                            if (ssc != null)
                            {
                                Session["GrowerMobileNo"] = fardet.MobileNo;
                                lblTestNo.Text = dec.Id.ToString();
                                lblForm1NO.Text = ssc.Form1Id.ToString();
                                lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
                                lblGrowerName.Text = fardet.Name + ",District : " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                                    ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                                    ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                                    ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                                    ",Mobile No : " + fardet.MobileNo;
                                lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
                                lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                                lblclass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                                lblLotno.Text = ssc.LotNo;
                                lblquantity.Text = ssc.Quantity.ToString();
                                SeedAnalysisReport sar = seedAnalysisReportService.GetByLabTestNo(dec.Id);
                                if (sar != null)
                                {
                                    lblobjectionable.Text = sar.ObjectionableWeedSeed.ToString();
                                    lblodv.Text = sar.ODV.ToString();
                                    lblOtherCrop.Text = sar.OtherCropSeeds.ToString();
                                    lblpureseed.Text = sar.PureSeed.ToString();
                                    lblSeedMoisture.Text = sar.SeedMoisture.ToString();
                                    lbltotalweed.Text = sar.TotalWeedSeed.ToString();
                                    lblDamage.Text = sar.InsectDamage.ToString();
                                    lblDiseases.Text = sar.SeedBorneDiseases.ToString();
                                    lblFreshungerminated.Text = sar.FreshUngerminatedSeed.ToString();
                                    lblHardSeed.Text = sar.HardSeed.ToString();
                                    lblHuskless.Text = sar.HuskLessSeed.ToString();
                                    lblInert.Text = sar.InertMatter.ToString();
                                    lblgermination.Text = sar.Germination.ToString();
                                    
                                    if (f2 != null)
                                    {
                                        // lblForm2NO.Text = f2.Form2Number;
                                        lblIssuedBy.Text = userMasterService.GetById(Convert.ToInt16(f2.IssuedBy)).Name;
                                        lblIssuedDate.Text = f2.IssuedDate.Value.ToString("dd/MM/yyyy");
                                        lblvalidity.Text = f2.ValidityPeriod.ToString("dd/MM/yyyy");
                                        lbltotaltags.Text = f2.TagsIssued;
                                        lblusedtags.Text = f2.TagsUsed;
                                        lblunusedtags.Text = f2.TagsUnUsed;
                                        lblreleaseOrder.Text = "Release order no.: " + f2.Id.ToString();
                                        lblForm2NO.Text = f2.Form2Number;
                                        //  Save.Visible = false;
                                        divReport.Visible = true;
                                        lnkCertificate.Visible = true;
                                        if (f2.CertificateImage == null && lnkCertificate.Visible == true)
                                        {

                                            FormIIPhoto.Visible = true;
                                            Upload.Visible = true;
                                        }
                                        else
                                        {
                                            if (f2.CertificateImage != null && f2.CertificateImage.Length > 0)
                                            {
                                                LitFormIIPhoto.Text = string.Format(previewString, Serializer.Base64String(f2.CertificateImage));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (userMasterService.GetFromAuthCookie().Role_Id == 4 || userMasterService.GetFromAuthCookie().Role_Id == 6)
                                        {
                                            divReport.Visible = true;
                                            //     Save.Visible = true;
                                            lblIssuedBy.Text = userMasterService.GetFromAuthCookie().Name;
                                            lblIssuedDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                                        }
                                        else
                                        {
                                            lblMessage.Text = "Form II not issued for this form no.";
                                            divReport.Visible = false;
                                            //  Save.Visible = false;
                                        }
                                    }

                                    //if (f1.GOTRequiredOrNot == 1)
                                    //{
                                    //    int sscGpt = SSCID + 1;
                                    //    Decoding decGpt = decodingService.GetBySSCID(sscGpt);
                                    //    GeneticPurityReport gpt = geneticPurityReportService.GetById(dec.Id);
                                    //    if (gpt == null)
                                    //    {
                                    //        lblMessage.Text = "Genetic purity test has not yet completed for this form no.";
                                    //        divReport.Visible = false;
                                    //    }
                                    //    else { lblGeneticPurity.Text = gpt.Id.ToString(); }
                                    //}
                                }
                                else
                                {
                                    lblMessage.Text = "Lab report has not yet completed for this form no.";
                                    divReport.Visible = false;
                                }

                            }
                            else
                            {
                                lblMessage.Text = "Seed Sampling Coupon not entered for this form no.";
                                divReport.Visible = false;
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Field inspection not finalised for this form no.";
                            divReport.Visible = false;
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Enter valid form no.";
                        divReport.Visible = false;
                    }
                }
                else
                {
                    lblMessage.Text = "Form II number not finalised";
                    divReport.Visible = false;
                }
            }
        }
        protected void Upload_Click(object sender, EventArgs e)
        {
            int sid = Convert.ToInt16(Request.QueryString["Id"]);
           
            Form2Details form2Details = form2DetailsService.GetBy(sid);
            var form2issuedBy = userMasterService.GetFromAuthCookie().Name;
            var f2 = new Form2Details
            {
                Id=form2Details.Id, 
                ValidityPeriod = Convert.ToDateTime(lblvalidity.Text),
                TagsIssued = lbltotaltags.Text,
                TagsUsed = lblusedtags.Text,
                TagsUnUsed = lblunusedtags.Text,
                CertificateImage = FormIIPhoto.FileBytes.Length > 0 ? FormIIPhoto.FileBytes : null, 
               
            };
            int certificateUpdate = form2DetailsService.Update(f2);
            if (certificateUpdate > 0)
            {
                lblMessage.Text = "Form II Photo Updated";
                Form1Details f1 = formOneService.ReadById(Convert.ToInt32(lblForm1NO.Text));
                FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                var producerMobileNumber = userMasterService.GetById(f1.Producer_Id).MobileNo;
                Messaging.SendSMS("Form II created successfully, Form2  number-" + lblForm2NO.Text + "  completed by " + form2issuedBy + "", fardet.MobileNo, "1");
                Messaging.SendSMS("Form II created successfully for Form1 number-" + lblForm1NO.Text + " and generated Form2  number-" + lblForm2NO.Text + "  completed by " + form2issuedBy + "", producerMobileNumber, "1");


                this.Redirect("/Transactions/Form2DetailsApproval.aspx?Id=" + Convert.ToInt16(sid));
            }



        }

        
        protected void form2Certificate_Click(object sender, EventArgs e)
        {
            int ssid = Convert.ToInt16(Request.QueryString["Id"]);

            Form2Details f2Details = form2DetailsService.GetBy(ssid);
            this.Redirect("Reports/Form2Certificate.aspx?Id=" + f2Details.SSCID + "");
        }
    }
}