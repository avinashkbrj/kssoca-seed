﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Form2DetailsView.aspx.cs" Inherits="KSSOCA.Web.Transactions.Form2DetailsView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Form-II(Seed Certificate)"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>

                <table class="table-condensed" width="100%" border="1">
                    <%-- <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>--%>
                    <tr class="SideHeading">
                        <td colspan="4">Form - II
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button runat="server" ID="Add" class="btn-primary" Text="Enter New Form-II" Visible="false" PostBackUrl="~/Transactions/Form2DetailsCreate.aspx" />

                        </td>
                        <%-- <td colspan="3">Get Form - II Details By :&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlfilter" Width="200px" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                            <asp:ListItem Text="Form-I Registration No." Value="f1"></asp:ListItem>
                            <asp:ListItem Text="Date wise" Value="dt"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="pnlsearchbyf1" runat="server" Visible="true">
                                Enter Form-I Registration No. :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtForm1No" onkeypress="return CheckNumber(event);" placeholder="Form I No" onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ID="txtForm1NoRequiredValidator" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtForm1No" ErrorMessage="Form I No Required" ValidationGroup="v1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                    <asp:Button ID="btngetformdetails" runat="server" Text="Get Form1 Details" CssClass="btn-primary" OnClick="btngetformdetails_Click" ValidationGroup="v1" />
                            </asp:Panel>
                            <asp:Panel ID="pnlsearchbyDate" runat="server" Visible="false">
                                From Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtFromDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtFromDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtFromDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                           To Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtToDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtToDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtToDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                <asp:Button ID="btngetforms" runat="server" Text="Get Processing Entries" CssClass="btn-primary" ValidationGroup="v3" OnClick="btngetforms_Click" />

                            </asp:Panel>
                        </td>--%>
                    </tr>
                </table>
                <br />
            </div>
            <div>
                <asp:GridView ID="gvEntries" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No." />
                        <asp:BoundField DataField="SSCId" HeaderText="SSC No." />
                        <asp:BoundField DataField="Crop" HeaderText="Crop" />
                        <asp:BoundField DataField="Variety" HeaderText="Variety" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <asp:BoundField DataField="LotNo" HeaderText="Lot No." />
                        <asp:BoundField DataField="ValidityPeriod" HeaderText="Certificate Validity Date" />
                        <asp:BoundField DataField="IssuedDate" HeaderText="Certificate Issued Date" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkview" PostBackUrl='<%# "~/Transactions/Form2DetailsApproval.aspx?Id="+Eval("SSCId") %>'>View</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------No forms selected-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
