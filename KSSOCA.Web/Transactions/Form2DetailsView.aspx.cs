﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Data;
   
    public partial class Form2DetailsView : System.Web.UI.Page
    {
        Form2DetailsService f2s = new Form2DetailsService();
        UserMasterService ums = new UserMasterService();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var user = ums.GetFromAuthCookie();
                    if (user.Role_Id == 6)
                    { Add.Visible = true; }
                    var data = f2s.getf2details(user.Id, Convert.ToInt16(user.Role_Id));
                    if (data != null)
                    {
                        gvEntries.DataSource = data;
                        gvEntries.DataBind();
                    }
                    else
                    {
                        gvEntries.DataSource = data;
                        gvEntries.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
         
    }
}