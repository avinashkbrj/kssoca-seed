﻿<%@ Page Title="" validateRequest="false" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormOneApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.FormOneApproval" %>

  
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <!-- this meta viewport is required for BOLT //-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <!-- BOLT Sandbox/test //-->
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <!-- BOLT Production/Live //-->
    <!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->
       <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">

                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    <asp:Label runat="server" ID="Label1"> </asp:Label>
                    <div id="myDiv" runat="server"></div>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                    <input type="hidden" id="surl" name="surl" value="<%= Session["surl"]%>" />
                    <input type="hidden" id="txnid" name="txnid" value="<%= Session["txnid"]%>" />
                    <input type="hidden" id="hash" name="hash" value="<%= Convert.ToString(Session["hash"])%>" />
                    <input type="hidden" id="amount" name="amount" value="<%= Session["amount"]%>" />
                    <input type="hidden" id="fname" name="fname" value="<%= Session["fname"]%>" />
                    <input type="hidden" id="mobilenumber" name="mobilenumber" value="<%= Session["mobilenumber"]%>" />
                 
                   
                    <asp:LinkButton ID="lnkGoForFieldInspection" runat="server" Text="Go for Field Inspection" OnClick="lnkGoForFieldInspection_Click" Visible="false"></asp:LinkButton>
                </div>
                <br />
                <table width="100%" border="1" class="table-condensed">
                    <%-- <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal> 
                        </td>
                    </tr>--%>
                    <tr>
                       
                        <td colspan="2" class="SideHeading">1.Seed Producer Information                       
                        </td>
                        <td colspan="2">Form-I Registration No.: 
                            <asp:Label Font-Bold="true" runat="server" ID="lblForm1No" />
                            Date :
                            <asp:Label Font-Bold="true" runat="server" ID="lblForm1RegDate" /></td>
                    </tr>
                    <tr>
                        <td width="25%">1.Firm Name</td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="lblProducerName" /></td>

                        <td width="25%">2.Registration No</td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="lblProducerRegNo" /></td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">2.Source Verification Information                       
                        </td>
                    </tr>
                    <tr>
                        <td>1.SV No.</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSourceVerNo" /></td>
                        <td>2.Crop</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblCrop" /></td>
                    </tr>
                    <tr>
                        <td>3.Variety</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblVariety" /></td>
                        <td>4.Source Verification Date</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSVDate" /></td>
                    </tr>
                    <tr>
                        <td colspan="4">5.Lot No's 
                            <br />
                            <asp:GridView ID="gvLotNos" runat="server" class="table-condensed" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> . 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed Distributed(in Kg.)" />
                                    <asp:BoundField DataField="TagNo" HeaderText="Tag No" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                                <FooterStyle Font-Bold="true" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">3.Grower Information                       
                        </td>
                    </tr>

                    <tr>
                        <td>1.Name  </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="NameOfSeedGrower" />

                        </td>

                        <td>2.Father's Name </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="FathersName" />

                        </td>
                    </tr>
                    <tr>
                        <td>3.District</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="District">
                            </asp:Label>
                        </td>

                        <td>4.Taluk</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Taluk">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>5.Hobli</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlHobli">
                            </asp:Label>

                        </td>

                        <td>6.Village </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlvillage">
                            </asp:Label>

                        </td>
                    </tr>
                    <tr>
                        <td>7.Gram Panchayat </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="GramPanchayat" />

                        </td>

                        <td>8.Mobile No. </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="TelephoneOrMobileNumber" />

                        </td>
                    </tr>

                    <tr>
                        <td>9.Aadhaar Number</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="AadhaarNumber" />

                        </td>

                        <td>10.Bank Name</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="BankName">
                            </asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td>11.IFSC Number</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="IFSCNumber" />

                        </td>

                        <td>12.Account Number </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="AccountNumber" />

                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">4.Seed and Seed plot Information</td>
                    </tr>
                    <tr>
                        <td>1.Plot District</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblPlotDistrict">
                            </asp:Label>
                        </td>
                        <td>2.Plot Taluk</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlPlotTaluk">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>3.Plot Hobli </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlPlotHobli">
                            </asp:Label>

                        </td>

                        <td>4.Plot Village </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlPlotVillage">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>5.Plot Gram Panchayat </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotGramPanchayat" />
                        </td>

                        <td>6.Plot Survey No </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotSurveyNumber" />
                        </td>
                    </tr>
                    <tr>
                        <td>7.Class of seed offered for certification </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ddlClass">
                            </asp:Label>
                        </td>

                        <td>8.Area offered for certification (in acres) </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="AreaOffered" />
                        </td>
                    </tr>
                    <tr>
                        <td>9.Quantity of seed distributed (in Kgs.)</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Seeddistributed" />
                        </td>

                        <td>10.Isolcation Distance North to south(in meters)</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="IsolcationDistanceNorthToSouth" />

                        </td>
                    </tr>
                    <tr>
                        <td>11.Isolcation Distance East to west(in meters)</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="IsolcationDistanceEastToWest" />
                        </td>
                        <td>12.Date of sowing/transplanting</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ActualDateOfSowingOrTransplanting" />
                        </td>
                    </tr>
                    <tr>
                        <td>13.Season<span class="text-danger"></span></td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Season">
                            </asp:Label>
                        </td>
                        <td>14.Name of organiser / sub contractor</td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="NameOfTheOrganiserOrSubContrator" />
                        </td>
                    </tr>
                    <tr>
                        <td>15.Seed processing unit </td>
                        <td colspan="3">
                            <asp:Label Font-Bold="true" runat="server" ID="SeedProcessingUnit">
                            </asp:Label>
                        </td>
                    </tr>
                    <%--<tr>
                        <td>16. Farmer Signature</td>
                        <td colspan="3">
                            <asp:Label Font-Bold="true" runat="server" ID="FarmerOtpSignature">
                            </asp:Label>
                        </td>
                    </tr>--%>
 
                    <tr class="SideHeading">
                        <td colspan="4">5.Land Survey Details
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:Label runat="server" ID="lblBhoomiMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                            <asp:GridView ID="gvBhoomi" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="true">
                               <%-- <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="owner" HeaderText="Land Owner" />
                                    <asp:BoundField DataField="survey_no" HeaderText="Survey No." />
                                    <asp:BoundField DataField="survey_Hissa" HeaderText="Survery/Hissa No." />
                                    <asp:BoundField DataField="land_code" HeaderText="Land Code" />
                                    <asp:BoundField DataField="father" HeaderText="Father Name" />
                                    <asp:BoundField DataField="disp_name" HeaderText="Relation" />
                                    <asp:BoundField DataField="owner_cat" HeaderText="Owner Category" />
                                    <asp:BoundField DataField="gunta_cent" HeaderText="Gunta cent" />
                                    <asp:BoundField DataField="extent" HeaderText="Area in acres" />
                                    <asp:BoundField DataField="Surnoc" HeaderText="Survey No cent" />
                                    <asp:BoundField DataField="hissa_no" HeaderText="Hissa No." />
                                    <asp:BoundField DataField="soil_name" HeaderText="Soil Name" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>--%>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">6.Documents uploaded
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">1.Grower Photo &nbsp;&nbsp;<asp:Literal runat="server" ID="litFarmerPhoto" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top">2.Grower Adhar Card/ID Card &nbsp;&nbsp;<asp:Literal runat="server" ID="LitAdharCard" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top">3.Grower Bank PassBook &nbsp;&nbsp;<asp:Literal runat="server" ID="LitPassBook" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top">4.Pahani &nbsp;&nbsp;<asp:Literal runat="server" ID="LitPhani" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">5.Purchase Bill with Farmer signature &nbsp;&nbsp;<asp:Literal runat="server" ID="LitPurchaseBill" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top" colspan="2">6.Vamshavruksha &nbsp;&nbsp;<asp:Literal runat="server" ID="LitVamshavruksha" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">7.Service Charge Details &nbsp;&nbsp;&nbsp;&nbsp;  
                            <asp:Label ID="lblmessagePayment" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlPaymentDetails" runat="server" Visible="false">
                        <tr>
                            <td>1.Registration fee : Rs.<asp:Label ID="lblRegFeespaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>2.Inspection charge : Rs.<asp:Label ID="lblInspectionChargepaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>3.STL charge : Rs.<asp:Label ID="lblSTLChargepaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>4.GPT charge : Rs.<asp:Label ID="lblGPTChargepaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>5.Other charge : Rs.<asp:Label ID="lblOtherChargepaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>6.Total amount paid : Rs.<asp:Label ID="lblTotalPaid" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;</td>
                            <td>7.Payment mode :
                                <asp:Label Font-Bold="true" runat="server" ID="lblPaymentMode" />
                            </td>
                            <td>8.Payment date :
                                <asp:Label ID="lblPaymentDate" Font-Bold="true" runat="server"></asp:Label></td>

                        </tr>
                        <tr>
                            <td colspan="2">9.Cheque No./DD No./BankTransaction ID :
                                <asp:Label Font-Bold="true" runat="server" ID="lblBankTransactionID" />
                                <asp:Literal runat="server" ID="LitCheck" EnableViewState="false"></asp:Literal></td>
                             <td>10.Cheque Date :
                                <asp:Label Font-Bold="true" runat="server" ID="lblcheckDate" />
                                <asp:Literal runat="server" ID="Literal1" EnableViewState="false"></asp:Literal></td>
                             
                        </tr>

                    </asp:Panel>
                    <asp:Panel ID="pnlPayment" runat="server" Visible="false">
                        <tr>
                            <td>1.Registration fee :Rs.<asp:Label ID="lblRegFees" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>2.Inspection charge : Rs.<asp:Label ID="lblInspectionCharge" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>3.STL charge :Rs.<asp:Label ID="lblSTLCharge" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                            </td>
                            <td>4.GPT charge : Rs.<asp:Label ID="lblGPTCharge" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;<br />
                            </td>
                        </tr>
                        <tr>
                            <td>5.Other charge : Rs.<asp:Label ID="lblOtherCharge" runat="server" Font-Bold="true"></asp:Label>/-&nbsp;&nbsp;
                                
                            </td>
                            <td>6.Total amount to be paid:Rs.<asp:Label ID="lblamounttobepaid" runat="server" Font-Bold="true"></asp:Label>/-
                                <asp:Label ID="lblpaymenttype" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnPaymentType" runat="server" />
                            </td>
                            <td colspan="2">7.Payment mode&nbsp;&nbsp;
                                <asp:RadioButtonList ID="rbPaymentMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbPaymentMode_SelectedIndexChanged">
                                    <asp:ListItem Text="DD/Cheque&nbsp;&nbsp;" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="e-Payment" Value="O"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <asp:Panel ID="pnlCheck" runat="server">
                            <tr>
                                <td>8.Cheque/DD number<span style="color: red">*</span></td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCheckNo" placeholder="Cheque Number" TextMode="Number" onkeypress="return CheckNumber(event);" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredCheckNo" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtCheckNo" ErrorMessage="Cheque Number required"></asp:RequiredFieldValidator>
                                </td>
                                 <td>10.Date of Cheque<span style="color: red">*</span></td>
                                <td>
                                  <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDateofCheque" placeholder="Cheque Date" onkeydown="return false" />
                                    <asp:RequiredFieldValidator runat="server" ID="DateofChequeRequiredValidator" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtDateofCheque" ErrorMessage="Date Field can't be empty"></asp:RequiredFieldValidator>
                
                                    
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnCheck" Text="Save Payment Details" OnClick="btnCheck_Click" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlOnline" runat="server">
                            <tr>
                                <td colspan="4" align="center">
                                     <input type="submit" CssClass="btn-primary"   value="Pay Online" onclick="launchBOLT(); return false;" /> 
                                  <%--   <asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnPayOnline" Text="Pay Online" OnClick="btnPayOnline_Click" />--%></td>
 
                            </tr>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                        <tr>
                            <td>Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="80%" Height="100px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ErrorMessage="Give Remarks" Enabled="false" ValidationGroup="v1"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Allot SCO for Field Inspection</td>
                            <td colspan="3">
                                <asp:DropDownList runat="server" class="form-control" ID="ddlsco">
                                </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="Requiredddlsco" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlsco" ErrorMessage="Please select SCO" InitialValue="0" ValidationGroup="v1"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                        <asp:Panel ID="pnlreje" runat="server" Visible="false">
                            <tr>
                                <td>Rejection Type</td>
                                <td colspan="3">
                                    <asp:RadioButtonList ID="rbRejectionType" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Application Reject&nbsp;&nbsp;" Value="A" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Payment Reject" Value="P">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button runat="server" CssClass="btn-danger" CausesValidation="false"
                                    ID="btnReject" Text="Request to Resubmit" OnClick="btnReject_Click" ValidationGroup="v1" />
                                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                    ID="Save" Text="Approve and Send to SCO" OnClick="Save_Click" ValidationGroup="v1" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr class="SideHeading">
                        <td colspan="4">8.Application Status &nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Note: Sl.No. 1 is the current status of the application.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class=" table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Transaction Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Transaction Date" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                             
                         
                            <%--<input type="submit" value="Pay" onclick="launchBOLT(); return false;" />--%>
                                
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCheck" />
            <asp:PostBackTrigger ControlID="btnReject" />
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>

<script type="text/javascript">
    $(document).ready(function () {
        createHash(); 
       
         
    });
    //--
</script>	

<script type="text/javascript"><!--
    function launchBOLT() {
        //var encrypteds = CryptoJS.AES.encrypt("xpTnZUWfnB", "Secret Passphrase");
        //var encryptedk = CryptoJS.AES.encrypt("dT2l4KMG", "Secret Passphrase");
        //alert(encrypteds);
        //alert(encryptedk);
       
        var encryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var encryptedKey =  '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';
       
        var decryptedSalt = CryptoJS.AES.decrypt(encryptedSalt, "Secret Passphrase");
        var decryptedKey = CryptoJS.AES.decrypt(encryptedKey, "Secret Passphrase");
        
        var transId = $('#txnid').val();
        var hashid1 = $('#hash').val();
         
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        
        bolt.launch({
            key: decryptedKey.toString(CryptoJS.enc.Utf8),
            txnid: transId,
            hash: hashid1.toString(),
            amount: amt,
            firstname: fname,
            email: "avi.ksrj@gmail.com",
            phone: mobilenumber,
            productinfo: "KSSOCA Payment",
            udf5: "BOLT_KIT_ASP.NET",
            surl: $('#surl').val(),
            furl: $('#surl').val()
        }, { responseHandler: function (BOLT) {
            console.log(BOLT.response.txnStatus);
            if (BOLT.response.txnStatus != 'CANCEL') {
                //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
		'<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                    '<input type=\"hidden\" name=\"salt\" value=\"' + decryptedSalt.toString(CryptoJS.enc.Utf8) + '\" />' +
		'<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
		'<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
		'<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
		'<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
		'<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
		'<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
		'<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
		'<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
		'<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
		'</form>';
                var form = jQuery(fr);
                jQuery('body').append(form);
                form.submit();
            }
        },
            catchException: function (BOLT) {
                alert(BOLT.message);
            }
        });
    }

    function createHash() {
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        var transId = $('#txnid').val();
        var hashEncryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var hashEncryptedKey = '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var hashDecryptedSalt = CryptoJS.AES.decrypt(hashEncryptedSalt, "Secret Passphrase");
        var hashDecryptedKey = CryptoJS.AES.decrypt(hashEncryptedKey, "Secret Passphrase");
        $.ajax({
            url: '/Transactions/Hash.aspx',
            type: 'post',
            
            data: JSON.stringify({
                key: hashDecryptedKey.toString(CryptoJS.enc.Utf8),
                salt: hashDecryptedSalt.toString(CryptoJS.enc.Utf8),
                txnid: transId,
                amount: amt,
                pinfo: "KSSOCA Payment",
                fname: fname,
                email: "avi.ksrj@gmail.com",
                mobile: mobilenumber,
                udf5: "BOLT_KIT_ASP.NET"
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (json) {
               
                if (json['error']) {
                    $('#alertinfo').html('<i class="fa fa-info-circle"></i>' + json['error']);
                }
                else if (json['success']) {
                    $('#hash').val(json['success']);
                   
                }
            }
        });
       
    }

    
</script>	

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
