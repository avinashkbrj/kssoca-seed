﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using Bhoomi;
    using System.Xml;
    using System.Collections.Specialized;
    using System.Text;
    using System.Web;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Collections;
    using System.Security.Cryptography;
    using System.Web.UI;
    using System.Collections.Generic;
    using System.Web.Helpers;

    public partial class FormOneApproval : SecurePage
    {
        public string action1 = string.Empty;
        public string hash1 = string.Empty;
        public string txnid1 = string.Empty;

        UserMasterService userMasterService;
        BankMasterService bankMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService; Common common = new Common(); SPURegistrationService spuRegistrationService;
        SourceVerificationTransactionService sourceVerificationTransactionService;
        Form1DetailsTransactionService form1DetailsTransactionService;
        Form1PaymentDetailService form1PaymentDetailService;
        FarmerDetailsService farmerDetailsService;
        F1LotNumbersService f1LotNumbersService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        CommonPaymentMethods commonPaymentMethods;
        public FormOneApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            bankMasterService = new BankMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();

            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            spuRegistrationService = new SPURegistrationService();
            sourceVerificationTransactionService = new SourceVerificationTransactionService();
            form1DetailsTransactionService = new Form1DetailsTransactionService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            farmerDetailsService = new FarmerDetailsService();
            f1LotNumbersService = new F1LotNumbersService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            commonPaymentMethods = new CommonPaymentMethods();
        }
          protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                
                lblHeading.Text = common.getHeading("F1A"); //litNote.Text = common.getNote("F1A");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    
                        int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                        Session["Form1ID"] = Form1ID;
                        Form1Details f1 = formOneService.ReadById(Form1ID);
                    string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                    //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                    surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                    Session.Add("surl", surl);


                    int txnid = form1PaymentDetailService.GetBy(Form1ID).Id;
                    Session.Add("txnid", txnid);
                   
                    if (!IsPostBack)
                    {
                        NameValueCollection nvc = Request.Form;
                        if (nvc.Count > 0)
                        {
                            string bank_txn= Request.Form["txnid"];
                            string amount = Request.Form["amount"];
                            string f_code = Request.Form["status"];

                            EPayment_Response(amount, bank_txn, f_code, f1, sender, e);
                        }
                    }
                     
                        InitComponent(f1);
                    
                }
                else
                {
                    this.Redirect("Dashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(Form1Details f1)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
            var userMaster = userMasterService.GetFromAuthCookie();

            Id.Value = f1.Id.ToString();
            lblForm1No.Text = f1.Id.ToString();
            FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
            NameOfSeedGrower.Text = fardet.Name;
            Session.Add("fname", NameOfSeedGrower.Text);
            FathersName.Text = fardet.FatherName;
            District.Text = districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name;
            Taluk.Text = talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name;

            ddlHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name;
            ddlvillage.Text = villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name;

            GramPanchayat.Text = fardet.GramPanchayath;
            TelephoneOrMobileNumber.Text = fardet.MobileNo;
            Session.Add("mobilenumber", userMaster.MobileNo);
            BankName.Text = bankMasterService.GetById(Convert.ToInt16(fardet.Bank_Id)).Name;
            AccountNumber.Text = fardet.AccountNo;
            IFSCNumber.Text = fardet.IFSCCode;
            AadhaarNumber.Text = fardet.AadharCardNo;

            lblPlotDistrict.Text = districtMasterService.GetById(Convert.ToInt16(f1.PlotDistrict_Id)).Name;
            ddlPlotTaluk.Text = talukMasterService.GetBy(Convert.ToInt16(f1.PlotDistrict_Id), Convert.ToInt16(f1.PlotTaluk_Id)).Name; ;
            ddlPlotHobli.Text = HobliMasterService.GetBy(Convert.ToInt16(f1.PlotDistrict_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id)).Name; ;
            ddlPlotVillage.Text = villageMasterService.GetBy(Convert.ToInt16(f1.PlotDistrict_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id), Convert.ToInt16(f1.PlotVillage_Id)).Name; ;
            LocationOfTheSeedPlotGramPanchayat.Text = f1.PlotGramPanchayath;
            LocationOfTheSeedPlotSurveyNumber.Text = f1.SurveyNo.ToString() + f1.HissaNo;
            ddlClass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
            AreaOffered.Text = f1.Areaoffered.ToString();
            Seeddistributed.Text = f1.SeedDistributed.ToString();
            IsolcationDistanceNorthToSouth.Text = f1.DistanceNorthSouth.ToString();
            IsolcationDistanceEastToWest.Text = f1.DistanceEastWest.ToString();
            ActualDateOfSowingOrTransplanting.Text = f1.ActualDateofSowing != null ? f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy") : "";
            NameOfTheOrganiserOrSubContrator.Text = f1.Organiser;

            gvLotNos.DataSource = f1LotNumbersService.GetByF1Id(f1.Id);
            gvLotNos.DataBind();

            if (f1.SPU_Id != 0)
            {
                SeedProcessingUnit.Text = spuRegistrationService.GetById(Convert.ToInt16(f1.SPU_Id)).Name;
            }
            if (fardet.Photo != null && fardet.Photo.Length > 0)
                litFarmerPhoto.Text = string.Format(previewString, Serializer.Base64String(fardet.Photo));

            if (fardet.BankPassBook != null && fardet.BankPassBook.Length > 0)
                LitPassBook.Text = string.Format(previewString, Serializer.Base64String(fardet.BankPassBook));

            if (fardet.Phani != null && fardet.Phani.Length > 0)
                LitPhani.Text = string.Format(previewString, Serializer.Base64String(fardet.Phani));
            if (fardet.AdharCard != null && fardet.AdharCard.Length > 0)
                LitAdharCard.Text = string.Format(previewString, Serializer.Base64String(fardet.AdharCard));

            if (f1.PurchaseBill != null && f1.PurchaseBill.Length > 0)
                LitPurchaseBill.Text = string.Format(previewString, Serializer.Base64String(f1.PurchaseBill));
            if (f1.Vamshavruksha != null && f1.Vamshavruksha.Length > 0)
                LitVamshavruksha.Text = f1.Vamshavruksha != null?string.Format(previewString, Serializer.Base64String(f1.Vamshavruksha)) : "NIL";

            RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
            lblProducerName.Text = userMasterService.GetById(Convert.ToInt16(f1.Producer_Id)).Name;
            
            ////if (f1.FarmerOtp >0)
            ////{
            ////    FarmerOtpSignature.Text = "Verified by farmer through OTP";
            ////}
            ////else
            ////{
            ////    FarmerOtpSignature.Text = "OTP verification not completed";
            ////}
            // Initiate Source verification details
            SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            lblSourceVerNo.Text = f1.SourceVarification_Id.ToString();
            lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
            lblProducerRegNo.Text = sv.ProducerRegNo;

            lblSVDate.Text = sourceVerificationTransactionService.GetMaxTransactionDetails(sv.Id).TransactionDate.Value.ToString("dd/MM/yyyy");


            gvstatus.DataSource = form1DetailsTransactionService.GetAllTransactionsBy(f1.Id);
            gvstatus.DataBind();
            int paymentstatus = CheckPaymentDetails(Convert.ToInt16(f1.Id));
            if (paymentstatus == 1)
            {
                pnlPaymentDetails.Visible = true;
            }
            else
            {
                pnlPaymentDetails.Visible = false;
                lblmessagePayment.Text = "Payment details not updated yet.";
            }
            Form1DetailsTransaction transaction = new Form1DetailsTransaction();
            transaction = form1DetailsTransactionService.GetMaxTransactionDetails(f1.Id);
            int ActionRole_Id = transaction.ToID;
            if (userMaster.Role_Id == 5) // SPU Permission
            {
                pnlRemarks.Visible = false;
                if (transaction.TransactionStatus == 0)
                {
                    btnEditApplication.Visible = true;
                    btnEditApplication.PostBackUrl = "~/Transactions/FormOneCreate.aspx?Form1Id=" + f1.Id;
                }
                if (paymentstatus == 0)
                {
                    pnlPaymentDetails.Visible = false;
                    pnlPayment.Visible = true;
                }
                else if (paymentstatus == 1)
                {
                    pnlPaymentDetails.Visible = true;
                    pnlPayment.Visible = false;
                }
                if (f1.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
                checkPaymentMode();
            }
            else
            {
                var scos = userMasterService.GetByReportingOfficer(userMaster.Id, 6);
                ddlsco.BindListItem<UserMaster>(scos);

                pnlPayment.Visible = false;
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                }
                else
                {
                    pnlRemarks.Visible = false;
                }
                if (paymentstatus == 0)
                {
                    pnlRemarks.Visible = false;
                }
            }
            if (f1.StatusCode == 1)
            {
                pnlRemarks.Visible = false;
                pnlPayment.Visible = false;
            }
            try
            {
                Service1SoapClient bho = new Service1SoapClient();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(bho.GetAgriInfo(Convert.ToInt16(fardet.District_Id),
                                               Convert.ToInt16(f1.PlotTaluk_Id),
                                               Convert.ToInt16(f1.PlotHobli_Id),
                                               Convert.ToInt16(f1.PlotVillage_Id),
                                               Convert.ToInt16(f1.SurveyNo)));
                DataSet ds = new DataSet();
                XmlNodeReader xmlreader = new XmlNodeReader(xmlDoc);
                ds.ReadXml(xmlreader, XmlReadMode.Auto);
              //  DataRow[] foundRows = ds.Tables[0].Select("survey_no = '" + f1.SurveyNo + f1.HissaNo + "'", " survey_no DESC", DataViewRowState.CurrentRows);
                gvBhoomi.DataSource = ds;
                gvBhoomi.DataBind();
            }
            catch (Exception ex)
            {
                lblBhoomiMessage.Text = ex.Message;// "Unable to fetch Survey details from Bhoomi data.";
            }
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
            }
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                    Form1Details f1 = formOneService.ReadById(Form1ID);
                    FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
                    string mobileno = userMasterService.GetById(formOneService.ReadById(Id.Value.ToInt()).Producer_Id).MobileNo;
                    int TransactionStatus = 0;
                    int ToID = 0;
                    var sourceVerificationDetails = sourceverficationservice.GetById(f1.SourceVarification_Id);
                    if (RoleID == 4)
                    {
                        ToID = Convert.ToInt16(ddlsco.SelectedValue);
                        //ToID = Convert.ToInt16(formOneService.ReadById(Convert.ToInt16(Id.Value.ToInt())).Producer_Id);
                        int FarmerID = Convert.ToInt16(formOneService.ReadById(Convert.ToInt16(Id.Value.ToInt())).FarmerID);
                        FinalApproval(Id.Value.ToInt(), FarmerID);
                        TransactionStatus = 5; //  

                        //else
                        //{
                        //    ToID = Convert.ToInt16(userMaster.Reporting_Id);
                        //    TransactionStatus = Convert.ToInt16(userMasterService.GetById(Convert.ToInt16(userMaster.Reporting_Id)).Role_Id);
                        //}
                        var rft = new Form1DetailsTransaction
                        {
                            Id = common.GenerateID("Form1DetailsTransaction"),
                            FromID = userMaster.Id,
                            ToID = ToID,
                            Form1DetailsID = Id.Value.ToInt(),
                            TransactionDate = System.DateTime.Now,
                            TransactionStatus = TransactionStatus,
                            Remarks = txtRemarks.Text,
                            IsCurrentAction = 1
                        };
                        int IsCurrentUserSet = common.setIsCurrentAction("Form1DetailsTransaction", "Form1DetailsID", Id.Value.ToInt());
                        if (IsCurrentUserSet > 0)
                        {
                            int result = form1DetailsTransactionService.Create(rft);
                            if (result == 1)
                            {
                                lblMessage.ForeColor = System.Drawing.Color.Green;
                                lblMessage.Text = "Approved successfully.";
                                Messaging.SendSMS("Form 1 for Tag Number "+ sourceVerificationDetails.TagNumbers + " Approved By " + userMaster.Name + "", mobileno, "1");
                                Messaging.SendSMS("Form 1 for Tag Number "+ sourceVerificationDetails.TagNumbers + " Approved By " + userMaster.Name + "", fardet.MobileNo, "1");
                                //Messaging.SendSMS("firm registration approved by " + userMaster.Name + "",   MobileNumber.Text, "1");
                                Page_Load(sender, e);
                            }
                            else
                            {
                                lblMessage.Text = "Error occurred. Please contact administrator.";
                                Save.Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        Save.Enabled = false;
                        // lblMessage.Text = "Error occurred. Please contact administrator.";
                    }

                }
                catch (Exception ex)
                {
                    ApplicationExceptionHandler.Handle(ex);
                    lblMessage.Text = "Error occurred. Please contact administrator.";
                }
            }
            else
            {
            }
        }
        private int FinalApproval(int Form1Id, int FarmerID)
        {
            string query = @"update FarmerDetails set StatusCode=1  where Id= " + FarmerID + " update Form1Details set StatusCode=1  where Id= " + Form1Id;
            return common.ExecuteNonQuery(query);
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                string mobileno = userMasterService.GetById(formOneService.ReadById(Id.Value.ToInt()).Producer_Id).MobileNo;
                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMaster.Role_Id);
                int TransactionStatus = 0;
                Form1Details f1 = formOneService.ReadById(Id.Value.ToInt());
                var sourceDetails = sourceverficationservice.GetById(f1.SourceVarification_Id);
                if (rbRejectionType.SelectedValue == "A")
                {
                    Messaging.SendSMS("Form 1 for Tag Number "+ sourceDetails.TagNumbers + " has been rejected by " + userMaster.Name + "", mobileno, "1");
                    // Messaging.SendSMS("firm registration rejected by " + userMaster.Name + "",  MobileNumber.Text, "1");
                    TransactionStatus = 0;
                }
                else if (rbRejectionType.SelectedValue == "P")
                {
                    TransactionStatus = 100; // 100 indicates payment rejection
                    int i = RejectPayment(Convert.ToInt16(Id.Value.ToInt()));
                    if (i == 1)
                    {
                        //Messaging.SendSMS("SPU registration payment rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                        Messaging.SendSMS("Form 1 for Tag Number " + sourceDetails.TagNumbers + " payment rejected by " + userMaster.Name + "", mobileno, "1");
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }
                var rft = new Form1DetailsTransaction
                {
                    Id = common.GenerateID("Form1DetailsTransaction"),
                    FromID = userMaster.Id,
                    ToID = Convert.ToInt16(f1.Producer_Id),
                    Form1DetailsID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus, // indicates rejection
                    Remarks = txtRemarks.Text,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("Form1DetailsTransaction", "Form1DetailsID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = form1DetailsTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Rejected successfully.";
                        //Messaging.SendSMS("firm registration rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                        Messaging.SendSMS("Form 1 for Tag Number " + sourceDetails.TagNumbers + " rejected by " + userMaster.Name + "", mobileno, "1");

                        Page_Load(sender, e);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        private int RejectPayment(int Form1ID)
        {
            string query = "update Form1PaymentDetails set PaymentStatus='R' where UserID= " + Form1ID;
            return common.ExecuteNonQuery(query);
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Dashboard.aspx");
        }
        protected void lnkGoForFieldInspection_Click(object sender, EventArgs e)
        {
            this.Redirect("Transactions/FieldInspectionView.aspx?Id=" + Id.Value.ToInt() + "");
        }
        private int CheckPaymentDetails(int Form1ID)
        {
            //lblamounttobepaid.Text = common.getFees("SPU").ToString();
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View </a></span>";
            Form1PaymentDetails pd = form1PaymentDetailService.GetBy(Form1ID);
            if (pd != null)
            {
                string PaymentStatus = Convert.ToString(pd.PaymentStatus);
                if (PaymentStatus == "Y")
                {
                    lblTotalPaid.Text = Convert.ToString(pd.AmountPaid);
                    lblRegFeespaid.Text = pd.RegFees != null ? pd.RegFees.ToString() : "0.00";
                    lblGPTChargepaid.Text = pd.GPTCharge != null ? pd.GPTCharge.ToString() : "0.00";
                    lblSTLChargepaid.Text = pd.STLCharge != null ? pd.STLCharge.ToString() : "0.00";
                    lblOtherChargepaid.Text = Convert.ToString(pd.OtherCharge);
                    lblInspectionChargepaid.Text = Convert.ToString(pd.InspectionCharge);
                    lblPaymentDate.Text = pd.PaymentDate.Value.ToString("dd/MM/yyyy");
                    lblPaymentMode.Text = pd.PaymentMode == "C" ? "Cheque/DD" : "e-Payment";
                    lblBankTransactionID.Text = Convert.ToString(pd.BankTransactionID);
                    lblcheckDate.Text = pd.ChequeDate?.ToString("dd/MM/yyyy");
                    
                   // byte[] check = pd.PaySlip;
                   // if (check != null && check.Length > 0)
                     //   LitCheck.Text = string.Format(previewString, Serializer.Base64String(check));
                    return 1;
                }
                else if (PaymentStatus == "N")
                {
                    getPaymentetails(Form1ID);
                    return 0;
                }
                else if (PaymentStatus == "R")
                {
                    getPaymentetails(Form1ID);
                }
                return 0;
            }
            else
            {
                getPaymentetails(Form1ID);
                return 0;
            }
        }
        private void getPaymentetails(int Form1ID)
        {
            Form1Details f1 = formOneService.ReadById(Form1ID);
            FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
            SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
            CropMaster cm = cropMasterService.GetById(sv.Crop_Id);
            if (farDet.StatusCode == 1)
            { lblRegFees.Text = "0"; }
            else { lblRegFees.Text = common.getFees("F1").ToString(); }
            lblSTLCharge.Text = "150.00";
            decimal InspectionCharge = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
            lblInspectionCharge.Text = InspectionCharge.ToString("N2");
            if (Convert.ToInt16(cm.GOT) == 0)
            {
                if (f1.ClassSeedtoProduced == 2 || f1.ClassSeedtoProduced == 3)
                {
                    lblGPTCharge.Text = common.getFees("GOT").ToString();
                }
                else { lblGPTCharge.Text = "0.00"; }
            }
            else
            {
                lblGPTCharge.Text = cm.GOT.ToString();
            }
            lblOtherCharge.Text = common.getFees("OTHER").ToString();
            lblamounttobepaid.Text = Convert.ToString(
                                     Convert.ToDecimal(lblRegFees.Text) +
                                     Convert.ToDecimal(lblSTLCharge.Text) +
                                     Convert.ToDecimal(lblGPTCharge.Text) +
                                     Convert.ToDecimal(lblInspectionCharge.Text) +
                                     Convert.ToDecimal(lblOtherCharge.Text));
            Session.Add("amount", lblamounttobepaid.Text);
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            
            decimal AmountToPay = lblamounttobepaid.Text.ToDecimal();
            int PaymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(Id.Value.ToInt())).Id;
            var form1SourceDetails = formOneService.ReadById(Id.Value.ToInt());
            var farmerDetails=farmerDetailsService.GetById(form1SourceDetails.FarmerID);
            var tagNumber = sourceverficationservice.GetById(form1SourceDetails.SourceVarification_Id).TagNumbers;
            var masterDetails  = userMasterService.GetFromAuthCookie();
            var pd = new Form1PaymentDetails
            {
                Id = PaymentId,
                AmountPaid = AmountToPay,
                BankTransactionID = txtCheckNo.Text,
                PaymentDate = System.DateTime.Now,
                PaymentMode = rbPaymentMode.SelectedValue,
                PaymentStatus = "Y",
                ChequeDate = Convert.ToDateTime(txtDateofCheque.Text), 
                Form1ID = Id.Value.ToInt(),
                GPTCharge = lblGPTCharge.Text.ToDecimal(),
                InspectionCharge = lblInspectionCharge.Text.ToDecimal(),
                OtherCharge = lblOtherCharge.Text.ToDecimal(),
                RegFees = lblRegFees.Text.ToDecimal(),
                STLCharge = lblSTLCharge.Text.ToDecimal()
            };

         
            int result = form1PaymentDetailService.Update(pd);
            if (result == 1)
            {
                ResendPaymentDetails(form1SourceDetails.Id);
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "Payment details saved successfully.";
                Messaging.SendSMS("Form 1 Created for Source Tag Number "+ tagNumber + "", farmerDetails.MobileNo, "1");
                lblmessagePayment.Text = "";
                Page_Load(sender, e);
            }
        }
        private void ResendPaymentDetails(int formid)
        {
           
            Form1DetailsTransaction transaction = new Form1DetailsTransaction();
            transaction = form1DetailsTransactionService.GetMaxTransactionDetails(formid);
            if (transaction.TransactionStatus == 100)
            {
                int ToID = transaction.FromID;
                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                var userMaster = userMasterService.GetFromAuthCookie();
                var rft = new Form1DetailsTransaction
                {
                    Id = common.GenerateID("Form1DetailsTransaction"),
                    FromID = userMaster.Id,
                    ToID = ToID,
                    Form1DetailsID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("Form1DetailsTransaction", "Form1DetailsID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = form1DetailsTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.Text = "Payment details saved successfully.";
                       
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }
            }
        }
        
        protected void EPayment_Response(string amount,string bank_txn, string f_code, Form1Details f1, object sender, EventArgs e)
        {
            CheckPaymentDetails(Convert.ToInt16(f1.Id));
            if (f_code != null)
            {
                if (f_code == "success")
                {
                    lblMessage.Text = "Payment Success";
                    int paymentId= form1PaymentDetailService.GetBy(Convert.ToInt16(f1.Id)).Id;
                    var pd = new Form1PaymentDetails
                    {
                        Id = paymentId, // Paymentdetails table ID
                        AmountPaid = Convert.ToDecimal(amount),
                        BankTransactionID = bank_txn,
                        PaymentDate = System.DateTime.Now,
                        PaymentMode = "O",
                        PaymentStatus = "Y",
                        Form1ID = f1.Id,
                        GPTCharge = lblGPTCharge.Text.ToDecimal(),
                        InspectionCharge = lblInspectionCharge.Text.ToDecimal(),
                        OtherCharge = lblOtherCharge.Text.ToDecimal(),
                        RegFees = lblRegFees.Text.ToDecimal(),
                        STLCharge = lblSTLCharge.Text.ToDecimal() 
                        
                    };
                    int result = form1PaymentDetailService.Update(pd);
                    if (result == 1)
                    {
                         
                        ResendPaymentDetails(f1.Id);
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Payment details saved successfully.";

                       Response.Redirect("/Transactions/FormOneApproval.aspx?Id=" + f1.Id);
                    }
                }
                else
                {
                    lblMessage.Text = "Payment Failed";
                }
            }

        } 
     
    }
}