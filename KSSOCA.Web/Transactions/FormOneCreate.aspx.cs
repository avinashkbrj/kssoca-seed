﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Linq;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;
    using System.Web.UI;
    using Newtonsoft.Json;
    using System.Windows.Forms;

    public partial class FormOneCreate : SecurePage
    {
        int otpValue = 0;
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        DistrictMasterService districtMasterService;
        TalukMasterService talukMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        SeasonMasterService seasonMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SPURegistrationService seedProcessingUnitMasterService;
        SourceVerificationService sourceverficationservice;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        Form1DetailsTransactionService form1DetailsTransactionService;
        Form1PaymentDetailService form1PaymentDetailService;
        Common common = new Common();
        FarmerDetailsService farmerDetailsService;
        SVLotNumbersService svLotNumbersService;
        F1LotNumbersService f1LotNumbersService;
        public FormOneCreate()
        {
           
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            districtMasterService = new DistrictMasterService();
            talukMasterService = new TalukMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            seasonMasterService = new SeasonMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            seedProcessingUnitMasterService = new SPURegistrationService();
            sourceverficationservice = new SourceVerificationService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            form1DetailsTransactionService = new Form1DetailsTransactionService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            farmerDetailsService = new FarmerDetailsService(); svLotNumbersService = new SVLotNumbersService();
            f1LotNumbersService = new F1LotNumbersService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
             
                lblHeading.Text = common.getHeading("F1C");  // Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    if (Convert.ToInt32(Request.QueryString["Id"]) > 0)
                    {
                        int SourceVerificationID = Convert.ToInt16(Request.QueryString["Id"]);
                        hdnSourceVerificationID.Value = SourceVerificationID.ToString();
                        SourceVerification sv = sourceverficationservice.ReadById(SourceVerificationID);
                        if (!Page.IsPostBack)
                        {
                            if (sv != null)
                            {
                                InitComponent(sv);
                                checkForEligibility();
                            }
                            else
                            {
                                this.Redirect("Transactions/SourceVerificationView.aspx");
                            }
                        }
                    }
                    else
                    {
                        this.Redirect("Transactions/SourceVerificationView.aspx");
                    }
                }
                else if (Request.QueryString["Form1Id"] != null)
                {
                    if (Convert.ToInt32(Request.QueryString["Form1Id"]) > 0)
                    {
                        if (!Page.IsPostBack)
                        {
                            hdnF1ID.Value = Request.QueryString["Form1Id"];
                            int SourceVerificationID = formOneService.ReadById(hdnF1ID.Value.ToInt()).SourceVarification_Id;
                            hdnSourceVerificationID.Value = SourceVerificationID.ToString();

                            SourceVerification sv = sourceverficationservice.ReadById(SourceVerificationID);
                            if (sv != null)
                            {
                                InitComponent(sv);
                            }
                            else
                            {
                                this.Redirect("Transactions/SourceVerificationView.aspx");
                            }
                        }
                    }
                    else { this.Redirect("Transactions/SourceVerificationView.aspx"); }
                }
                else
                {
                    this.Redirect("Transactions/SourceVerificationView.aspx");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false; ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void checkForEligibility()
        {
            int SourceVerificationID = Convert.ToInt16(hdnSourceVerificationID.Value);
            decimal SeedDistributed = Convert.ToDecimal(common.ExecuteScalar("select ISNULL(sum(SeedDistributed), 0) from [Form1Details] where SourceVarification_Id=" + SourceVerificationID));
            decimal SourceVerified = sourceverficationservice.ReadById(SourceVerificationID).QuantityofSeed;
            if (SeedDistributed >= SourceVerified)
            {
                lblMessage.Text = "Not allowed to fill form 1 because you have distributed all the seed verified under the source verification no " + SourceVerificationID;
                Save.Enabled = false;
            }
            else
            {
                decimal DistributionEligibility = SourceVerified - SeedDistributed;
                hdnDistributionEligibility.Value = DistributionEligibility.ToString();
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    //lblotpMessage.Text = "";
                    if (ddlFarmers.SelectedValue != "0")
                    {
                        var userMaster = userMasterService.GetFromAuthCookie();
                        int FarmerID = ddlFarmers.SelectedValue.ToInt();
                        int SourceVerificationID = Convert.ToInt16(hdnSourceVerificationID.Value);
                        var form1Details = new Form1Details()
                        {
                            Id = common.GenerateID("Form1Details"),
                            Producer_Id = userMaster.Id,
                            SourceVarification_Id = SourceVerificationID,
                            FarmerID = FarmerID,
                            PlotTaluk_Id = ddlPlotTaluk.SelectedValue.ToInt(),
                            PlotHobli_Id = ddlPlotHobli.SelectedValue.ToInt(),
                            PlotVillage_Id = ddlPlotVillage.SelectedValue.ToInt(),
                            PlotGramPanchayath = LocationOfTheSeedPlotGramPanchayat.Text,
                            SurveyNo = LocationOfTheSeedPlotSurveyNumber.Text.ToInt(),
                            ClassSeedtoProduced = ddlClass.SelectedValue.ToInt(),
                            Areaoffered = AreaOffered.Text.ToDecimal(),
                            SeedDistributed = Seeddistributed.Text.ToDecimal(),
                            DistanceNorthSouth = IsolcationDistanceNorthToSouth.Text != "" ? IsolcationDistanceNorthToSouth.Text.ToDecimal() : 0,
                            DistanceEastWest = IsolcationDistanceEastToWest.Text != "" ? IsolcationDistanceEastToWest.Text.ToDecimal() : 0,
                            ActualDateofSowing = ActualDateOfSowingOrTransplanting.Text.ToNullableDateTime(),
                            Organiser = NameOfTheOrganiserOrSubContrator.Text,
                            SPU_Id = SeedProcessingUnit.Items.Count != 0 ? SeedProcessingUnit.SelectedItem.Value.ToInt() : 0,
                            Season_Id = Season.SelectedItem.Value.ToInt(),
                            StatusCode = 0,
                            RegistrationDate = System.DateTime.Now,
                            GOTRequiredOrNot = Convert.ToInt16(hdnGOTRequiredOrNot.Value),
                            PlotDistrict_Id = District.SelectedValue.ToInt(),
                            PurchaseBill = fu_PurchaseBill.FileBytes.Length > 0 ? fu_PurchaseBill.FileBytes : null,
                            HissaNo= txtHissaNo.Text.Trim(),
                            //FarmerOtp = Convert.ToInt16(otpFromFarmer.Text),
                            Vamshavruksha= fu_vamshavruksha.FileBytes.Length > 0 ? fu_vamshavruksha.FileBytes : null,
                        };

                        var result = formOneService.Create(form1Details);

                        if (result > 0)
                        {
                            string query = "select Max(Id) from form1Details where Producer_Id=" + userMaster.Id + "";
                            int Form1ID = Convert.ToInt16(common.ExecuteScalar(query));
                            DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
                            if (insertLotNumbers(SourceVerificationID, Form1ID, _dt) > 0)
                            {
                                int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                       (Convert.ToInt16(District.SelectedValue.Trim()),
                                       Convert.ToInt16(ddlPlotTaluk.SelectedValue.Trim()), 4).User_Id); // get SCO
                                var svt = new Form1DetailsTransaction
                                {
                                    Id = common.GenerateID("Form1DetailsTransaction"),
                                    FromID = userMaster.Id,
                                    ToID = ToID,
                                    Form1DetailsID = Form1ID,
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = 4,
                                    IsCurrentAction = 1
                                };
                                int Transactionresult = form1DetailsTransactionService.Create(svt);
                                lblMessage.Text = "Saved successfully.";
                                if (Transactionresult == 1)
                                {
                                    var pd = new Form1PaymentDetails
                                    {
                                        Id = common.GenerateID("Form1PaymentDetails"),
                                        Form1ID = Form1ID,
                                        PaymentStatus = "N",
                                    };
                                    int Paymentresult = form1PaymentDetailService.Create(pd);
                                    if (Paymentresult == 1)
                                    {
                                        this.Redirect("Transactions/FormOneApproval.aspx?Id=" + Form1ID, false);
                                    }
                                    else
                                    {
                                        lblMessage.Text = "Error occurred while saving data.";
                                    }
                                }
                            }
                            else
                            {
                                //refresh page
                            }
                        }
                        else
                            lblMessage.Text = "Error occurred while saving record.";
                    }
                    else
                        lblMessage.Text = "Please select farmer.";
                }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    if (Request.QueryString["Form1Id"] != null)
                    {
                        var form1Details = new Form1Details()
                        {
                            Id = hdnF1ID.Value.ToInt(),
                            Producer_Id = userMaster.Id,
                            FarmerID = ddlFarmers.SelectedValue.ToInt(),
                            PlotTaluk_Id = ddlPlotTaluk.SelectedValue.ToInt(),
                            PlotHobli_Id = ddlPlotHobli.SelectedValue.ToInt(),
                            PlotVillage_Id = ddlPlotVillage.SelectedValue.ToInt(),
                            PlotGramPanchayath = LocationOfTheSeedPlotGramPanchayat.Text,
                            SurveyNo = LocationOfTheSeedPlotSurveyNumber.Text.ToInt(),
                            ClassSeedtoProduced = ddlClass.SelectedValue.ToInt(),
                            Areaoffered = AreaOffered.Text.ToDecimal(),
                            SeedDistributed = Seeddistributed.Text.ToDecimal(),
                            DistanceNorthSouth = IsolcationDistanceNorthToSouth.Text != "" ? IsolcationDistanceNorthToSouth.Text.ToDecimal() : 0,
                            DistanceEastWest = IsolcationDistanceEastToWest.Text != "" ? IsolcationDistanceEastToWest.Text.ToDecimal() : 0,
                            ActualDateofSowing = ActualDateOfSowingOrTransplanting.Text.ToNullableDateTime(),
                            Organiser = NameOfTheOrganiserOrSubContrator.Text,
                            SPU_Id = SeedProcessingUnit.Items.Count != 0 ? SeedProcessingUnit.SelectedItem.Value.ToInt() : 0,
                            Season_Id = Season.SelectedItem.Value.ToInt(),
                            StatusCode = 0,
                            RegistrationDate = System.DateTime.Now,
                            GOTRequiredOrNot = Convert.ToInt16(hdnGOTRequiredOrNot.Value),
                            PlotDistrict_Id = District.SelectedValue.ToInt(),
                            PurchaseBill = fu_PurchaseBill.FileBytes.Length > 0 ? fu_PurchaseBill.FileBytes : null,
                            HissaNo = txtHissaNo.Text.Trim(),
                            FarmerOtp =0,
                            Vamshavruksha = fu_vamshavruksha.FileBytes.Length > 0 ? fu_vamshavruksha.FileBytes : null,
                        };
                        var result = formOneService.Update(form1Details);
                        if (result > 0)
                        {
                            DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
                            int SourceVerificationID = Convert.ToInt16(hdnSourceVerificationID.Value);
                            if (insertLotNumbers(SourceVerificationID, hdnF1ID.Value.ToInt(), _dt) > 0)
                            {
                                Form1DetailsTransaction transaction = new Form1DetailsTransaction();
                                transaction = form1DetailsTransactionService.GetMaxTransactionDetails(hdnF1ID.Value.ToInt());
                                int ToID = transaction.FromID; // Resend to last transaction sent id
                                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                                var svt = new Form1DetailsTransaction
                                {
                                    Id = common.GenerateID("Form1DetailsTransaction"),
                                    FromID = userMaster.Id,
                                    ToID = ToID,
                                    Form1DetailsID = hdnF1ID.Value.ToInt(),
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = TransactionStatus,
                                    IsCurrentAction = 1
                                };
                                int IsCurrentUserSet = common.setIsCurrentAction("Form1DetailsTransaction", "Form1DetailsID", hdnF1ID.Value.ToInt());
                                if (IsCurrentUserSet > 0)
                                {
                                    int Transactionresult = form1DetailsTransactionService.Create(svt);
                                    lblMessage.Text = "Saved successfully.";
                                    this.Redirect("Transactions/FormOneApproval.aspx?Id=" + hdnF1ID.Value.ToInt(), false);
                                }
                            }
                        }
                        else
                            lblMessage.Text = "Error occurred while saving record.";
                    }
                }
                else
                {
                    lblMessage.Text = "Please check properly";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        private void InitComponent(SourceVerification sv)
        {
            var seasons = seasonMasterService.GetAll();
            var seedProcessingUnits = seedProcessingUnitMasterService.GetAll();

            var districts = districtMasterService.GetAll();
            District.BindListItem<DistrictMaster>(districts);

            var taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            ddlPlotTaluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);

            var PlotHoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue));
            ddlPlotHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(PlotHoblis);

            var PLotVillages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue), Convert.ToInt16(ddlPlotHobli.SelectedValue));
            ddlPlotVillage.Bind_T_H_V_ListItem<VillageMaster>(PLotVillages);

            Season.BindListItem<SeasonMaster>(seasons);
            Season.SelectedValue = sv.Season_Id.ToString();
            Season.Enabled = false;
            Season.CssClass = "form-control";
            SeedProcessingUnit.BindListItem<SPURegistration>(seedProcessingUnits);

            var ProducedIDs = classSeedMasterService.GetAllClassProducedIDs(sv.ClassSeed_Id);
            var classproduced = classSeedMasterService.GetAllClassProduced(Convert.ToString(ProducedIDs.Rows[0]["ClassProduced"]));
            ddlClass.BindListItem(classproduced);
            var Farmers = farmerDetailsService.GetByProducerID(sv.Producer_Id);
            ddlFarmers.BindListItem<FarmerDetails>(Farmers);
            int GOTRequiredOrNot = cropMasterService.GetById(sv.Crop_Id).GOT > 0 ? 1 : 0;
            hdnGOTRequiredOrNot.Value = Convert.ToString(ddlClass.SelectedValue.ToInt() == 2 ? 1 : ddlClass.SelectedValue.ToInt() == 3 ? 1 : GOTRequiredOrNot); // For foundation class 
            hdnSeedRate.Value = sv.SeedRate.ToString();
            var LotNos = svLotNumbersService.GetBySVId(sv.Id);
            ddllots.BindLotItem(LotNos);
            lblquantityleft.Text = "0.00";
            lblCropDetails.Text = "Source Verification No:" + sv.Id + ", Crop:" + cropMasterService.GetById(sv.Crop_Id).Name + ", Variety:" + cropVarietyMasterService.GetById(sv.Variety_Id).Name + ", Source Class:" + classSeedMasterService.GetById(sv.ClassSeed_Id).Name;
            
            if (Request.QueryString["Form1Id"] != null)
            {

                if (hdnF1ID.Value.ToInt() > 0)
                {
                    Form1Details f1 = formOneService.ReadById(hdnF1ID.Value.ToInt());
                    FarmerDetails farDetls = farmerDetailsService.GetById(f1.FarmerID);
                    Form1DetailsTransaction transaction = new Form1DetailsTransaction();
                    transaction = form1DetailsTransactionService.GetMaxTransactionDetails(f1.Id);
                    if (transaction != null)
                    {
                        if (transaction.TransactionStatus == 0 && transaction.ToID == userMasterService.GetFromAuthCookie().Id) //TransactionStatus 0 indicates application is  rejected needs to be edited
                        {
                            lblMessage.Text = "Application has been rejected.";
                            btnUpdate.Visible = true;
                            Save.Visible = false;
                            btnViewApplication.Visible = true;
                            btnViewApplication.PostBackUrl = "~/Transactions/FormOneApproval.aspx?Id=" + hdnF1ID.Value.ToInt();

                            var lots = f1LotNumbersService.GetByF1Id(f1.Id);
                            gvLotNos.DataSource = lots;
                            gvLotNos.DataBind();
                            if (lots.Count > 0)
                            {
                                hdnLotno.Value = JsonConvert.SerializeObject(lots);
                            }

                            District.ClearSelection();
                            District.Items.FindByValue(farDetls.District_Id.ToString()).Selected = true;
                            ddlFarmers.SelectedValue = f1.FarmerID.ToString();
                            ddlFarmers.Enabled = false;

                            ddlPlotTaluk.Items.Clear();
                            ddlPlotTaluk.ClearSelection();
                            taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(farDetls.District_Id));

                            ddlPlotTaluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
                            ddlPlotTaluk.Items.FindByValue(f1.PlotTaluk_Id.ToString()).Selected = true;


                            ddlPlotHobli.Items.Clear();
                            ddlPlotHobli.ClearSelection();
                            PlotHoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue));
                            ddlPlotHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(PlotHoblis);
                            ddlPlotHobli.Items.FindByValue(f1.PlotHobli_Id.ToString()).Selected = true;

                            ddlPlotVillage.Items.Clear();
                            ddlPlotVillage.ClearSelection();
                            PLotVillages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue), Convert.ToInt16(ddlPlotHobli.SelectedValue));
                            ddlPlotVillage.Bind_T_H_V_ListItem<VillageMaster>(PLotVillages);
                            ddlPlotVillage.Items.FindByValue(f1.PlotVillage_Id.ToString()).Selected = true;

                            LocationOfTheSeedPlotGramPanchayat.Text = f1.PlotGramPanchayath;
                            LocationOfTheSeedPlotSurveyNumber.Text = f1.SurveyNo.ToString();
                            txtHissaNo.Text = f1.HissaNo.ToString();
                            ddlClass.SelectedValue = f1.ClassSeedtoProduced.ToString();
                            AreaOffered.Text = f1.Areaoffered.ToString();
                            Seeddistributed.Text = f1.SeedDistributed.ToString();
                            IsolcationDistanceNorthToSouth.Text = f1.DistanceNorthSouth.ToString();
                            IsolcationDistanceEastToWest.Text = f1.DistanceEastWest.ToString();
                            ActualDateOfSowingOrTransplanting.Text = f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy");
                            NameOfTheOrganiserOrSubContrator.Text = f1.Organiser;
                            hdnDistributionEligibility.Value = f1.SourceVarification_Id.ToString();
                            if (f1.SPU_Id != 0)
                            {
                                SeedProcessingUnit.ClearSelection();
                                SeedProcessingUnit.Items.FindByValue(f1.SPU_Id.ToString()).Selected = true;
                            }
                            Season.SelectedItem.Value = f1.Season_Id.ToString();
                        }
                    }
                }
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Transactions/FormOneView.aspx?Id=" + Convert.ToInt64(hdnSourceVerificationID.Value) + "");
        }
        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            var taluks = talukMasterService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            ddlPlotTaluk.Items.Clear();
            ddlPlotTaluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            ddlPlotHobli.Items.Clear();
            var PlotHoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue));
            ddlPlotHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(PlotHoblis);
            ddlPlotVillage.Items.Clear();
            var PlotVillages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue), Convert.ToInt16(ddlPlotHobli.SelectedValue));
            ddlPlotVillage.Bind_T_H_V_ListItem<VillageMaster>(PlotVillages);
        }

        protected void ddlPlotTaluk_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPlotHobli.Items.Clear();
            var Hoblis = HobliMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue));
            ddlPlotHobli.Bind_T_H_V_ListItem<TalukHobliMaster>(Hoblis);
            ddlPlotVillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue), Convert.ToInt16(ddlPlotHobli.SelectedValue));
            ddlPlotVillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }

        protected void ddlPlotHobli_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPlotVillage.Items.Clear();
            var Villages = villageMasterService.GetAllBy(Convert.ToInt16(District.SelectedValue), Convert.ToInt16(ddlPlotTaluk.SelectedValue), Convert.ToInt16(ddlPlotHobli.SelectedValue));
            ddlPlotVillage.Bind_T_H_V_ListItem<VillageMaster>(Villages);
        }

        protected void AreaOffered_TextChanged(object sender, EventArgs e)
        {
            decimal area = AreaOffered.Text.ToDecimal();
            SourceVerification sv = sourceverficationservice.ReadById(Convert.ToInt16(hdnSourceVerificationID.Value));
            decimal? seedrate = sv.SeedRate;
            Seeddistributed.Text = (area * seedrate).ToString();
            decimal DistributionEligibility = Convert.ToDecimal(hdnDistributionEligibility.Value);
            if (Convert.ToDecimal(Seeddistributed.Text) > DistributionEligibility)
            {
                Seeddistributed.Text = "";
                AreaOffered.Text = "";
                lblMessage.Text = "You have only " + DistributionEligibility + " Kg source as the seed rate is " + seedrate + ", please add the area eligible for the the source " + DistributionEligibility + " Kg. ";
            }
        }
        protected void ddlFarmers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFarmers.SelectedValue != "0")
            {
                Session.Remove("otpValue");
                //btnResendOtp.Visible = false;
                //lblotpMessage.Text = "";
                //btnSendOtp.Text = "Send OTP";
                //otpFromFarmer.Text = "";
                FarmerDetails fardet = farmerDetailsService.GetById(ddlFarmers.SelectedValue.ToInt());
                lblFarmerDetails.Text = "Reg No:" + fardet.Id + "Farmer Name:" + fardet.Name + ", District: " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                                    ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                                    ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                                    ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                                    ",Mobile No : " + fardet.MobileNo;
            }
            else { lblFarmerDetails.Text = ""; }
        }
        protected void btnadd_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvLotNos.Visible == true)
                {
                    decimal qtyseed;
                    string lotnumber = ddllots.SelectedItem.Text.Trim();
                    if (QuantityOfSeed.Text == null || QuantityOfSeed.Text == "")
                    {
                        qtyseed = 0;
                    }
                    else
                    {
                        qtyseed = Convert.ToDecimal(QuantityOfSeed.Text);
                    }
                    string tagno = txtTagNo.Text;
                    DataTable _dt = new DataTable();
                    _dt.Columns.Add("LotNumber", typeof(string));
                    _dt.Columns.Add("Quantity", typeof(decimal));
                    _dt.Columns.Add("TagNo", typeof(string));
                    _dt.Rows.Add
                           (lotnumber, qtyseed, tagno);
                    string tbldata = common.DataTableToJSON(_dt);

                    if (hdnLotno.Value.Trim() != "")
                    {
                        hdnLotno.Value = hdnLotno.Value.Replace(']', ',');
                        tbldata = tbldata.Replace('[', ' ');
                    }
                    hdnLotno.Value += tbldata;
                    _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);

                    gvLotNos.DataSource = _dt;
                    gvLotNos.DataBind();
                    CalculateArea();
                }
                QuantityOfSeed.Text = "";
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                Save.Enabled = false; ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void CalculateArea()
        {
            DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
            if (_dt != null)
            {
                decimal total = Convert.ToDecimal(_dt.Compute("SUM(Quantity)", string.Empty));
                //  decimal AvgSeedRate = Convert.ToDecimal(_dt.Compute("SUM(SeedRate)", string.Empty));
                lblTotal.Text = Convert.ToInt64(total).ToString();
                if (lblTotal.Text != "0")
                {
                    decimal SeedQuantity = Convert.ToDecimal(total);// _dt.AsEnumerable().Sum(row => row.Field<decimal>("Quantity"));
                    decimal SeedRate = Convert.ToDecimal(hdnSeedRate.Value);
                    lblSeedRate.Text = hdnSeedRate.Value;
                    decimal area = SeedQuantity / SeedRate;
                    AreaOffered.Text = Math.Round((Double)area, 2).ToString();
                    Seeddistributed.Text = SeedQuantity.ToString();
                }
            }
            else
            {
                lblTotal.Text = "0";
                AreaOffered.Text = "0";
                Seeddistributed.Text = "0";
            }
        }
        protected void ddllots_SelectedIndexChanged(object sender, EventArgs e)
        {
            decimal quantityleft = svLotNumbersService.QuantityLeft(hdnSourceVerificationID.Value.ToInt(), ddllots.SelectedItem.Text.Trim());
            lblquantityleft.Text = quantityleft.ToString();
        }
        protected void QuantityOfSeed_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(QuantityOfSeed.Text) > Convert.ToDecimal(lblquantityleft.Text))
            {
                lblMessage.Text = "Seed quantity left for this Lot Number is " + lblquantityleft.Text + " Kg.";
                QuantityOfSeed.Text = "";
            }
        }
        private int insertLotNumbers(int svid, int F1ID, DataTable dtLotsData)
        {
            int recordCount = 0;
            var svLotNumbers = new List<F1LotNumbers>();

            for (int i = 0; i < dtLotsData.Rows.Count; i++)
            {
                svLotNumbers.Add(new F1LotNumbers()
                {
                    LotNumber = Convert.ToString(dtLotsData.Rows[i]["LotNumber"]).Trim().ToUpper(),
                    Quantity = Convert.ToDecimal(dtLotsData.Rows[i]["Quantity"]),
                    TagNo = Convert.ToString(dtLotsData.Rows[i]["TagNo"]),
                    SVID = svid,
                    F1ID = F1ID
                });
            }
            return recordCount += f1LotNumbersService.Update(svLotNumbers, F1ID).Count;
        }
        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btn = (LinkButton)(sender);
                string lotnumber = btn.CommandArgument;
                DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
                for (int i = _dt.Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = _dt.Rows[i];
                    if (dr["TagNo"].ToString() == lotnumber)
                    {
                        dr.Delete();
                    }
                }
                if (_dt.Rows.Count > 0)
                {
                    hdnLotno.Value = common.DataTableToJSON(_dt);
                }
                else
                {
                    hdnLotno.Value = "";
                }
                gvLotNos.DataSource = _dt;
                gvLotNos.DataBind();
                CalculateArea();
               // f1LotNumbersService.DeleteByFormIdLotNo(hdnF1ID.Value.ToInt(), lotnumber);
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false; ApplicationExceptionHandler.Handle(ex);
            }
        }
        ////protected void btnSendOtp_Click(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
               
        ////        if (btnSendOtp.Text == "Send OTP")
        ////        {
        ////            if (ddlFarmers.SelectedIndex > 0)
        ////            {
        ////                SendOTP();
        ////                btnSendOtp.Text = "Verify OTP";
        ////                btnResendOtp.Visible = true;
        ////                lblotpMessage.Text = "OTP sent to farmer. Please check..";
        ////            }
        ////            else
        ////            {
        ////                lblotpMessage.Text = "Please select farmer to send message";
        ////            }
        ////        }
        ////        else if(btnSendOtp.Text == "Verify OTP")
        ////        {
        ////            if (otpFromFarmer.Text != "")
        ////            {
        ////                int farmerOtp = Convert.ToInt32(otpFromFarmer.Text);
        ////                if (Session["otpValue"] != null)
        ////                {
        ////                    int otp = Convert.ToInt32(Session["otpValue"]);
        ////                    if (otp == farmerOtp)
        ////                    {
        ////                        lblotpMessage.Text = "Verified successfully";
        ////                        btnResendOtp.Visible = false;
        ////                        btnSendOtp.Visible = false;
        ////                    }
        ////                    else
        ////                    {
        ////                        lblotpMessage.Text = "Please enter correct OTP";
        ////                    }
        ////                }
        ////                else
        ////                {
        ////                    lblotpMessage.Text = "Please enter correct OTP";
        ////                }
        ////            }
        ////            else
        ////            {
        ////                lblotpMessage.Text = "Please enter OTP";
        ////            }
        ////        }

        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        lblMessage.Text = "Something went wrong, Please try again later.";
        ////        Save.Enabled = false; ApplicationExceptionHandler.Handle(ex);
        ////    }
        ////}
        ////protected void ReSendOtp_Click(object sender, EventArgs e)
        ////{
        ////    try
        ////    {
        ////        SendOTP();
        ////        btnSendOtp.Text = "Verify OTP";
        ////        btnResendOtp.Visible = true;
        ////        lblotpMessage.Text = "OTP sent to farmer. Please check..";
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        lblMessage.Text = "Something went wrong, Please try again later.";
        ////        Save.Enabled = false; ApplicationExceptionHandler.Handle(ex);
        ////    }
        ////}

        ////private void SendOTP()
        ////{
            
        ////    FarmerDetails fardet = farmerDetailsService.GetById(ddlFarmers.SelectedValue.ToInt());
        ////    Random random = new Random();
        ////    otpValue = random.Next(1001, 9999);
        ////    Session["otpValue"] = otpValue;
        ////    Messaging.SendSMS("Your OTP is " + otpValue + " for KSSOCA FormI verification. Please provide to SPF if you agree to Form1 Regn", fardet.MobileNo, "1");
        ////}
    }
}