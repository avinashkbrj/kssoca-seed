﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormOneEdit.aspx.cs" Inherits="KSSOCA.Web.Transactions.FormOneEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href='<%= ResolveUrl("~/css/registration.css")%>' rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h1>Form1</h1>

        <div class="form-group">
            <div class="col-lg-8 col-lg-offset-4">
                <asp:Label runat="server" ID="lblMessage"></asp:Label>
                <asp:HiddenField runat="server" ID="Id" />
            </div>
        </div>

        <div class="form-horizontal">

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Registration Number</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="FarmerRegistrationNumber" placeholder="Farmer Registration Number" ReadOnly="true" />
                    <asp:RequiredFieldValidator runat="server" ID="FarmerRegistrationNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="FarmerRegistrationNumber" ErrorMessage="Farmer Registration Number can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <%--            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Farmer Registration Date<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="FarmerRegistrationDate" placeholder="Farmer Registration Date" />
                    <asp:RequiredFieldValidator runat="server" ID="FarmerRegistrationDateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="FarmerRegistrationDate" ErrorMessage="Farmer Registration Date can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>--%>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Name of Seed Grower<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="NameOfSeedGrower" placeholder="Name of Seed Grower" />
                    <asp:RequiredFieldValidator runat="server" ID="NameOfSeedGrowerRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="NameOfSeedGrower" ErrorMessage="Name of Seed Grower can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Father Name<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="FathersName" placeholder="Father Name" />
                    <asp:RequiredFieldValidator runat="server" ID="FathersNameRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="FathersName" ErrorMessage="Father Name can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Village<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="Village" placeholder="Village Name" />
                    <asp:RequiredFieldValidator runat="server" ID="VillageFieldValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Village" ErrorMessage="Village can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Gram Panchayat<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="GramPanchayat" placeholder="Gram Panchayat" />
                    <asp:RequiredFieldValidator runat="server" ID="GramPanchayatRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="GramPanchayat" ErrorMessage="Gram Panchayat can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">District</label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="District">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Taluk</label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="Taluk">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Telephone / Moble No.<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="TelephoneOrMobileNumber" placeholder="Telephone / Moble No." />
                    <asp:RequiredFieldValidator runat="server" ID="TelephoneOrMobileNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="TelephoneOrMobileNumber" ErrorMessage="Telephone / Moble No can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Aadhaar Number</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="AadhaarNumber" placeholder="Aadhaar Number" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="AadhaarNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="AadhaarNumber" ErrorMessage="Aadhaar Number can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Bank Name</label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="BankName">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">IFSC Number</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="IFSCNumber" placeholder="IFSC Number" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="IFSCNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="IFSCNumber" ErrorMessage="IFSC Number can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Account Number<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="AccountNumber" placeholder="Account Number" />
                    <asp:RequiredFieldValidator runat="server" ID="AccountNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="AccountNumber" ErrorMessage="Account Number can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Seed Plot Village<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="LocationOfTheSeedPlotVillage" placeholder="Location of the Seed Plot Village" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="LocationOfTheSeedPlotVillage" ErrorMessage="Location of the Seed Plot Village can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Plot Gram Panchayat<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="LocationOfTheSeedPlotGramPanchayat" placeholder="Location of the Seed Plot Gram Panchayat" />
                    <asp:RequiredFieldValidator runat="server" ID="LocationOfTheSeedPlotGramPanchayatRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="LocationOfTheSeedPlotGramPanchayat" ErrorMessage="Location of the Seed Plot Gram Panchayat can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Seed Plot Taluk<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="LocationOfTheSeedPlotTaluk">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Crop<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="Crop">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Variety<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="Variety">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Seed Plot Survey No<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="LocationOfTheSeedPlotSurveyNumber" placeholder="Location of the Seed Plot Survey Number" />
                    <asp:RequiredFieldValidator runat="server" ID="LocationOfTheSeedPlotSurveyNumberRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="LocationOfTheSeedPlotSurveyNumber" ErrorMessage="Location of the Seed Plot Survey Number can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

<%--            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Crop Certificate<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="CropOfferedForCertificate" placeholder="Crop offered for Certificate" />
                    <asp:RequiredFieldValidator runat="server" ID="CropOfferedForCertificateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="CropOfferedForCertificate" ErrorMessage="Crop offered for Certificate can't be empty"></asp:RequiredFieldValidator>

                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Variety Certificate<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="VarietyOfferedForCertificate" placeholder="Variety offered for Certificate" />
                    <asp:RequiredFieldValidator runat="server" ID="VarietyOfferedForCertificateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="VarietyOfferedForCertificate" ErrorMessage="Variety offered for Certificate can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="VarietyOfferedForCertificateRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_\s\/-]{1,15}$" ControlToValidate="VarietyOfferedForCertificate"
                        ErrorMessage="Variety Certificate max of 15 characters"></asp:RegularExpressionValidator>
                </div>
            </div>--%>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Foundation Stage<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="ClassOfSeedFoundationStage" placeholder="Class of Seed (Foundation Stage)" />
                    <asp:RequiredFieldValidator runat="server" ID="ClassOfSeedFoundationStageRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="ClassOfSeedFoundationStage" ErrorMessage="Class of Seed (Foundation Stage) can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="ClassOfSeedFoundationStageRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_\s\/-]{1,15}$" ControlToValidate="ClassOfSeedFoundationStage"
                        ErrorMessage="Foundation Stage max of 15 characters"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Certified Stage<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="ClassOfSeedCertifiedStage" placeholder="Class of Seed (Certified Stage)" />
                    <asp:RequiredFieldValidator runat="server" ID="ClassOfSeedCertifiedStageRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="ClassOfSeedCertifiedStage" ErrorMessage="Class of Seed (Certified Stage) can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="ClassOfSeedCertifiedStageRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_\s\/-]{1,15}$" ControlToValidate="ClassOfSeedCertifiedStage"
                        ErrorMessage="Certified Stage max of 15 characters"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label no-top-padding">Area offered<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="AreaOffered" placeholder="Area offered in (hectares)" />
                    <asp:RequiredFieldValidator runat="server" ID="AreaOfferedRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="AreaOffered" ErrorMessage="Area offered in (hectares) can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>

            <%--Source Verification Id--%>
            <%--<div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Source of Seed<span class="text-danger">*</span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="SourceOfSeed" placeholder="Source of Seed" />
                    <asp:RequiredFieldValidator runat="server" ID="SourceOfSeedRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="SourceOfSeed" ErrorMessage="Source of Seed can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>--%>

            <%--<div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Lot Numbers<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="LotNumbers" placeholder="Lot Numbers" />
                    <asp:RequiredFieldValidator runat="server" ID="LotNumbersRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="LotNumbers" ErrorMessage="Lot Numbers can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>--%>

            <%--<div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Tag Numbers<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="TagNumbers" placeholder="Tag Numbers" />
                    <asp:RequiredFieldValidator runat="server" ID="TagNumbersRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="TagNumbers" ErrorMessage="Tag Numbers can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>--%>

            <%--<div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Source Verification Date<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="SourceVerificationDate" placeholder="Source Verification Date" />
                    <asp:RequiredFieldValidator runat="server" ID="SourceVerificationDateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="SourceVerificationDate" ErrorMessage="Source Verification Date can't be empty"></asp:RequiredFieldValidator>
                </div>
            </div>--%>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Order Number / Date</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="ReleaseOrderNumberOrDate" placeholder="Release Order Number / Date" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="ReleaseOrderNumberOrDateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="ReleaseOrderNumberOrDate" ErrorMessage="Release Order Number / Date can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Validity Period</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="ValidityPeriod" placeholder="Validity Period" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="ValidityPeriodRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="ValidityPeriod" ErrorMessage="Validity Period can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label no-top-padding">Seed distributed<br />
                    (Qty in Kgs.)</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="Seeddistributed" placeholder="Seed distributed" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="SeeddistributedRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Seeddistributed" ErrorMessage="Seed distributed can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Bill Number and Date</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="BillNumberAndDate" placeholder="Bill Number and Date" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="BillNumberAndDateRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="BillNumberAndDate" ErrorMessage="Bill Number and Date can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">North to South</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="IsolcationDistanceNorthToSouth" placeholder="Isolcation Distance North to South" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="IsolcationDistanceNorthToSouthRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="IsolcationDistanceNorthToSouth" ErrorMessage="Isolcation Distance North to South can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">East to West</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="IsolcationDistanceEastToWest" placeholder="Isolcation Distance East to West" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="IsolcationDistanceEastToWestRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="IsolcationDistanceEastToWest" ErrorMessage="Isolcation Distance East to West FIR can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Date of Sowing / Transplanting</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control datepicker" ID="ActualDateOfSowingOrTransplanting" placeholder="Actual Date of Sowing / Transplanting" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="ActualDateOfSowingOrTransplantingRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="ActualDateOfSowingOrTransplanting" ErrorMessage="Actual Date of Sowing / Transplanting can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Organiser / Sub Contrator</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="NameOfTheOrganiserOrSubContrator" placeholder="Name of the Organiser / Sub Contrator" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="NameOfTheOrganiserOrSubContratorRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="NameOfTheOrganiserOrSubContrator" ErrorMessage="Name of the Organiser / Sub Contrator can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Seed Processing Unit<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="SeedProcessingUnit">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Registration Fee</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="RegistrationFee" placeholder="Registration Fee" />
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Inspection Fee<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="InspectionFee" placeholder="Inspection Fee" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="InspectionFeeRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="InspectionFee" ErrorMessage="Inspection Fee can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">STL Fee<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="STLFee" placeholder="STL Fee" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="STLFeeRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="STLFee" ErrorMessage="STL Fee can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">GPT Fee<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="GPTFee" placeholder="GPT Fee" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="GPTFeeRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="GPTFee" ErrorMessage="GPT Fee can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Other Charges<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="OtherCharges" placeholder="Other Charges" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="OtherChargesRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="OtherCharges" ErrorMessage="Other Charges can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Total<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="Total" placeholder="Total" />
                    <%--<asp:RequiredFieldValidator runat="server" ID="TotalRequiredValidator" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Total" ErrorMessage="Total can't be empty"></asp:RequiredFieldValidator>--%>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Season<span class="text-danger"></span></label>
                <div class="col-lg-8">
                    <asp:DropDownList runat="server" class="form-control" ID="Season">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="form-group col-lg-6">
                <label class="col-lg-4 control-label">Remarks</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" ID="Remarks" placeholder="Remarks" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <asp:Button runat="server" CssClass="btn btn-primary"
                        ID="SaveButton" Text="Save" OnClick="SaveButton_Click" />
                    <asp:Button runat="server" CssClass="btn btn-primary" CausesValidation="false"
                        ID="CancelButton" Text="Cancel" OnClick="CancelButton_Click" />
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
