﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;

    public partial class FormOneEdit : SecurePage
    {
        UserMasterService userService;
        BankMasterService bankMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SeedProcessUnitMasterService seedProcessingUnitMasterService;

        public FormOneEdit()
        {
            userService = new UserMasterService();
            cropMasterService = new CropMasterService();
            bankMasterService = new BankMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            seedProcessingUnitMasterService = new SeedProcessUnitMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();

                        if (id > 0)
                        {
                            Form1Details form1Details = formOneService.ReadById(id);

                            if (form1Details == null)
                            {
                                this.Redirect("Transactions/FormOneView.aspx");
                            }
                            else
                            {
                                InitComponent(form1Details);
                            }
                        }
                    }
                    else
                    {
                         this.Redirect("Transactions/FormOneView.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var form1Details = new Form1Details()
                    {
                        Id = Id.Value.ToInt(),
                        Producer_Id = userService.GetFromAuthCookie().Id,
                        Name = NameOfSeedGrower.Text,
                        FatherName = FathersName.Text,
                       // Village =  .SelectedItem.Value.ToInt(),
                        GramPanchayath = GramPanchayat.Text,
                        Taluk_Id = Taluk.SelectedItem.Value.ToInt(),
                        District_Id = District.SelectedItem.Value.ToInt(),
                        MobileNo = TelephoneOrMobileNumber.Text,
                        Bank_Id = BankName.SelectedItem.Value.ToInt(),
                        AccountNo = AccountNumber.Text,
                        IFSCCode = IFSCNumber.Text,
                        AadharCardNo = AadhaarNumber.Text,
                        LocationPlotVillage = LocationOfTheSeedPlotVillage.Text,
                        LocationPlotGramPanchayath = LocationOfTheSeedPlotGramPanchayat.Text,
                        LocationPlotTaluk_Id = LocationOfTheSeedPlotTaluk.SelectedItem.Value.ToInt(),
                        SurveyNo = LocationOfTheSeedPlotSurveyNumber.Text,
                        Crop_Id = Crop.SelectedItem.Value.ToInt(),
                        Variety_Id = Variety.SelectedItem.Value.ToInt(),
                        FoundationStage = ClassOfSeedFoundationStage.Text,
                        CertifiedStage = ClassOfSeedCertifiedStage.Text,
                        AreainHectare = AreaOffered.Text,
                        //SourceVarification_Id
                        DistanceNorthSouth = IsolcationDistanceNorthToSouth.Text.ToDecimal(),
                        DistanceEastWest = IsolcationDistanceEastToWest.Text.ToDecimal(),
                        ActualDateofSowing = ActualDateOfSowingOrTransplanting.Text.ToNullableDateTime(),
                        Organiser = NameOfTheOrganiserOrSubContrator.Text,
                        SPU_Id = SeedProcessingUnit.SelectedItem.Value.ToNullableInt(),
                        GPTCharge = GPTFee.Text.ToDecimal(),
                        OtherCharge = OtherCharges.Text.ToDecimal(),
                        Total = Total.Text.ToDecimal(),
                        // ChequeNo = 
                        // ChequeDate = 
                        // Form1ReceiptDate = 
                        Season_Id = Season.SelectedItem.Value.ToInt(),
                        // CRNo =
                        // CRDate = 
                        // CRAmount =
                        // DocImage = 
                        Remarks = Remarks.Text,
                        Status = 1,
                        UserId = userService.GetFromAuthCookie().Id
                    };

                    var result = formOneService.Update(form1Details);

                    if (result > 0)
                        lblMessage.Text = "Saved successfully.";
                    else
                        lblMessage.Text = "Error occurred while saving record.";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            this.Redirect("Transactions/FormOneView.aspx");
        }

        private void InitComponent(Form1Details form1Details)
        {
            var banks = bankMasterService.GetAll();
            var crops = cropMasterService.GetAll();
            var taluk = talukMasterService.GetAll();
            var seasons = seasonMasterService.GetAll();
            var district = districtMasterService.GetAll();
            var cropVariety = cropVarietyMasterService.GetAll();
            var seedProcessingUnits = seedProcessingUnitMasterService.GetAll();

            Crop.BindListItem<CropMaster>(crops);
            Taluk.BindListItem<TalukMaster>(taluk);
            BankName.BindListItem<BankMaster>(banks);
            Season.BindListItem<SeasonMaster>(seasons);
            District.BindListItem<DistrictMaster>(district);
            Variety.BindListItem<CropVarietyMaster>(cropVariety);
            LocationOfTheSeedPlotTaluk.BindListItem<TalukMaster>(taluk);
            SeedProcessingUnit.BindListItem<SeedProcessUnitMaster>(seedProcessingUnits);

            Id.Value = form1Details.Id.ToString();
            FarmerRegistrationNumber.Text = userService.GetFromAuthCookie().Reg_no;
            NameOfSeedGrower.Text = form1Details.Name;
            FathersName.Text = form1Details.FatherName;
            //Village.Text = form1Details.Village;
            GramPanchayat.Text = form1Details.GramPanchayath;
            Taluk.Items.FindByValue((form1Details.Taluk_Id ?? 1).ToString()).Selected = true;
            District.Items.FindByValue((form1Details.District_Id ?? 1).ToString()).Selected = true;
            TelephoneOrMobileNumber.Text = form1Details.MobileNo;
            BankName.Items.FindByValue((form1Details.Bank_Id ?? 1).ToString()).Selected = true;
            AccountNumber.Text = form1Details.AccountNo;
            IFSCNumber.Text = form1Details.IFSCCode;
            AadhaarNumber.Text = form1Details.AadharCardNo;
            LocationOfTheSeedPlotVillage.Text = form1Details.LocationPlotVillage;
            LocationOfTheSeedPlotGramPanchayat.Text = form1Details.LocationPlotGramPanchayath;
            LocationOfTheSeedPlotTaluk.Items.FindByValue((form1Details.LocationPlotTaluk_Id ?? 1).ToString()).Selected = true;
            LocationOfTheSeedPlotSurveyNumber.Text = form1Details.SurveyNo;
            Crop.Items.FindByValue(form1Details.Crop_Id.ToString()).Selected = true;
            Variety.Items.FindByValue(form1Details.Variety_Id.ToString()).Selected = true;
            ClassOfSeedFoundationStage.Text = form1Details.FoundationStage;
            ClassOfSeedCertifiedStage.Text = form1Details.CertifiedStage;
            AreaOffered.Text = form1Details.AreainHectare;
            //SourceVarification_Id
            IsolcationDistanceNorthToSouth.Text = form1Details.DistanceNorthSouth.ToString();
            IsolcationDistanceEastToWest.Text = form1Details.DistanceEastWest.ToString();
            ActualDateOfSowingOrTransplanting.Text = (form1Details.ActualDateofSowing ?? DateTime.Now).ToString("dd/MM/yyyy");
            NameOfTheOrganiserOrSubContrator.Text = form1Details.Organiser;
            SeedProcessingUnit.Items.FindByValue((form1Details.SPU_Id ?? 1).ToString()).Selected = true;
            GPTFee.Text = form1Details.GPTCharge.ToString();
            OtherCharges.Text = form1Details.OtherCharge.ToString();
            Total.Text = form1Details.Total.ToString();
            // ChequeNo = 
            // ChequeDate = 
            // Form1ReceiptDate = 
            Season.Items.FindByValue((form1Details.Season_Id ?? 1).ToString()).Selected = true;
            // CRNo =
            // CRDate = 
            // CRAmount =
            // DocImage = 
            Remarks.Text = form1Details.Remarks;
        }
    }
}