﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    using System.Linq;
    using System.Web;
    using System.Text;
    using System.Configuration;
    using System.Security.Cryptography;
    using System.Web.UI;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;

    public partial class FormOneView : SecurePage
    {
     
        OnlinePayment formOnePayment;
        UserMasterService userMasterService;
        FormOneDetailsService form1Service;
        SourceVerificationService sourceVerificationService;
        Common common = new Common();
        CommonPaymentMethods commonPaymentMethods;
        Form1PaymentDetailService form1PaymentDetailService;
        FarmerDetailsService farmerDetailsService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        Form1DetailsTransactionService form1DetailsTransactionService;
        public FormOneView()
        {
            form1Service = new FormOneDetailsService();
            userMasterService = new UserMasterService();
            sourceVerificationService = new SourceVerificationService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            farmerDetailsService = new FarmerDetailsService();
            cropMasterService = new CropMasterService();
            commonPaymentMethods = new CommonPaymentMethods();
            formOnePayment = new OnlinePayment();
            formOneService = new FormOneDetailsService();
            form1DetailsTransactionService = new Form1DetailsTransactionService();
        }
        
            protected void Page_Load(object sender, EventArgs e)
        {
            try
            {  
                lblHeading.Text = common.getHeading("F1V"); litNote.Text = common.getNote("F1V"); // Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                    //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                    surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                    Session.Add("surl", surl);
                    
                    if (!IsPostBack)
                    {
                        NameValueCollection nvc = Request.Form;
                        if (nvc.Count > 0)
                        {
                           
                            EPayment_Response(nvc, sender, e);
                        }
                    } 
                    int sourceverificationID = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["SVID"] = sourceverificationID;
                }
                else
                {

                }
                if (!IsPostBack)
                {
                    InitComponent();
                } 
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";  
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent()
        {
            Random random = new Random();
            var userMaster=userMasterService.GetFromAuthCookie();
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
            int txnid= random.Next(1001, 9999);
            Session.Add("txnid", txnid);
            Session.Add("mobilenumber", userMaster.MobileNo);
            Session.Add("fname", userMaster.Name);
            if (roleId == 5)
            { 
                Add.Visible = true; 
                Session.Add("emailid", userMaster.EmailId);
                FormOneGridView.DataSource = SelectMethod();
                FormOneGridView.DataBind();
            }
            else
            {
                Add.Visible = false;
                FormOneGridView.DataSource = SelectMethod();
                FormOneGridView.DataBind();
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            int sourceverificationID = Convert.ToInt16(Request.QueryString["Id"]);
            this.Redirect("Transactions/FormOneCreate.aspx?Id=" + sourceverificationID);
        }

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = form1Service.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }

        public DataTable SelectMethod()
        {
            var user = userMasterService.GetFromAuthCookie();
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);

            if (roleId == 5)
            {
                if (Session["SVID"] != null)
                {
                    var data = form1Service.ReadBySourceVerificationId(Convert.ToInt16(Session["SVID"]));
                    return data;
                }
                return null;
            }
            else
            {
                var data = form1Service.ReadAllByUserAuthority(user.Id);
                return data;
            }
        }

        protected void FormOneGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // LinkButton lnkDelete = (e.Row.FindControl("lnkDelete") as LinkButton);
                LinkButton lnkFieldInspection = (e.Row.FindControl("lnkFieldInspection") as LinkButton);
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                HiddenField hdnPaymentStatus = (e.Row.FindControl("hdnPaymentStatus") as HiddenField);
                CheckBox chkPayment = (e.Row.FindControl("chkPayment") as CheckBox);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
                Label lblAmountPaid = (e.Row.FindControl("lblAmountPaid") as Label);

                if (roleId == 5)
                {
                    // lnkDelete.Visible = true;
                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;

                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        chkPayment.Enabled = true;
                        chkPayment.Checked = false;

                        lblAmountPaid.Text = "Tick here to pay";
                        lblStatus.ForeColor = System.Drawing.Color.Blue;
                    }

                }
                else
                {
                    
                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;
                        lblAmountPaid.Visible = true;
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                    }
                    else
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = false;
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        lblAmountPaid.Text = "Not yet paid";
                    }
                }

                if (hdnStatusCode.Value == "1")
                {
                    lblStatus.Text = "Verified";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    //  lnkDelete.Visible = false;
                    lnkFieldInspection.Visible = true;
                }
                else
                {
                    lblStatus.Text = "Not Verified";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    lnkFieldInspection.Visible = false;
                }

            }
        }
        protected void chkPayment_CheckedChanged(object sender, EventArgs e)
        {
            List<OnlinePayment> formOnePayments = new List<OnlinePayment>(); 
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7]
            { new DataColumn("Form1ID"),
                new DataColumn("RegFees"),
                new DataColumn("InspectionCharge"),
                new DataColumn("STLCharge"),
                new DataColumn("GPTCharge"),
                new DataColumn("OtherCharge"),
                new DataColumn("Total",typeof(decimal) )
             });
            foreach (GridViewRow row in FormOneGridView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    OnlinePayment formOnePayment = new OnlinePayment();
                    CheckBox chkRow = (row.Cells[8].FindControl("chkPayment") as CheckBox);
                    if (chkRow.Checked && chkRow.Enabled)
                    {
                        string Form1ID = row.Cells[1].Text;
                        string RegFees = "";
                        string InspectionCharge = "";
                        string STLCharge = "150.00";
                        string GPTCharge = "";
                        string OtherCharge = "";
                        decimal Total = 0; 
                        Form1Details f1 = form1Service.ReadById(Form1ID.ToInt());
                        FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID); 
                        SourceVerification sv = sourceVerificationService.ReadById(f1.SourceVarification_Id);
                        CropMaster cm = cropMasterService.GetById(sv.Crop_Id);
                        if (farDet.StatusCode == 1) { RegFees = "0"; }
                        else { RegFees = common.getFees("F1").ToString(); }
                        decimal InspectionAmout = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
                        InspectionCharge = decimal.Round(InspectionAmout, 2, MidpointRounding.AwayFromZero).ToString();// InspectionAmout.ToString();

                        if (Convert.ToInt16(cm.GOT) == 0)
                        {
                            if (f1.ClassSeedtoProduced == 2 || f1.ClassSeedtoProduced == 3)
                            {
                                GPTCharge = common.getFees("GOT").ToString();
                            }
                            else
                            { GPTCharge = "0.00"; }
                        }
                        else
                        {
                            GPTCharge = cm.GOT.ToString();
                        }
                        OtherCharge = common.getFees("OTHER").ToString();
                        Total = Convert.ToDecimal(
                                     Convert.ToDecimal(RegFees) +
                                     Convert.ToDecimal(InspectionCharge) +
                                     Convert.ToDecimal(STLCharge) +
                                     Convert.ToDecimal(GPTCharge) +
                                     Convert.ToDecimal(OtherCharge));
                       
                        dt.Rows.Add(Form1ID, RegFees, InspectionCharge, STLCharge, GPTCharge, OtherCharge, decimal.Round(Total, 2, MidpointRounding.AwayFromZero));
                        formOnePayment.RegNumber = Form1ID.ToInt();
                        formOnePayment.RegFees = Convert.ToDecimal(RegFees);
                        formOnePayment.InspectionCharge = Convert.ToDecimal(InspectionCharge);
                        formOnePayment.STLCharge = Convert.ToDecimal(STLCharge);
                        formOnePayment.GPTCharge = Convert.ToDecimal(GPTCharge);
                        formOnePayment.OtherCharge = Convert.ToDecimal(OtherCharge);
                        formOnePayment.TotalAmountPaid = Convert.ToDecimal(Total);

                        formOnePayments.Add(formOnePayment);
                    }
                }
            }
            Session.Add("paymentDetails", formOnePayments);
            lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            Session.Add("amount", lblTotal.Text);
            if (Convert.ToDecimal(lblTotal.Text) > 0)
            {
                pnlPayment.Visible = true;
            }
            else { pnlPayment.Visible = false; }
            gvPayment.DataSource = dt;
            gvPayment.DataBind();
            checkPaymentMode();
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
            }
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                // byte[] a = fu_Check.FileBytes;
                SavePayment(rbPaymentMode.SelectedValue);
            }
        }
        private void BindPaymentGrid(string Form1IDs)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[7]
            { new DataColumn("Form1ID"),
                new DataColumn("RegFees"),
                new DataColumn("InspectionCharge"),
                new DataColumn("STLCharge"),
                new DataColumn("GPTCharge"),
                new DataColumn("OtherCharge"),
                new DataColumn("Total",typeof(decimal) )
             });

            string[] FormIDs = Form1IDs.Split('_');
            foreach (string ID in FormIDs)
            {
                string Form1ID = ID;
                string RegFees = "";
                string InspectionCharge = "";
                string STLCharge = "150.00";
                string GPTCharge = "";
                string OtherCharge = "";
                decimal Total = 0;

                Form1Details f1 = form1Service.ReadById(Form1ID.ToInt());
               
                FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
                SourceVerification sv = sourceVerificationService.ReadById(f1.SourceVarification_Id);
                CropMaster cm = cropMasterService.GetById(sv.Crop_Id);
                if (farDet.StatusCode == 1) { RegFees = "0"; }
                else { RegFees = common.getFees("F1").ToString(); }
                decimal InspectionAmout = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
                InspectionCharge = decimal.Round(InspectionAmout, 2, MidpointRounding.AwayFromZero).ToString();// InspectionAmout.ToString();

                if (Convert.ToInt16(cm.GOT) == 0)
                {
                    if (f1.ClassSeedtoProduced == 2 || f1.ClassSeedtoProduced == 3)
                    {
                        GPTCharge = common.getFees("GOT").ToString();
                    }
                    else
                    { GPTCharge = "0.00"; }
                }
                else
                {
                    GPTCharge = cm.GOT.ToString();
                }
                OtherCharge = common.getFees("OTHER").ToString();
                Total = Convert.ToDecimal(
                             Convert.ToDecimal(RegFees) +
                             Convert.ToDecimal(InspectionCharge) +
                             Convert.ToDecimal(STLCharge) +
                             Convert.ToDecimal(GPTCharge) +
                             Convert.ToDecimal(OtherCharge));

                dt.Rows.Add(Form1ID, RegFees, InspectionCharge, STLCharge, GPTCharge, OtherCharge, decimal.Round(Total, 2, MidpointRounding.AwayFromZero));

            }
            // lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            if (Convert.ToDecimal(lblTotal.Text) > 0)
            {
                pnlPayment.Visible = true;
            }
            else { pnlPayment.Visible = false; }
            gvPayment.DataSource = dt;
            gvPayment.DataBind();
            SavePayment("O");
        }
        private void SavePayment(string PaymentMode)
        {
            try
            {
                int result = 0;
                foreach (GridViewRow row in gvPayment.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int Form1ID = Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text);
                        decimal RegFees = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[2].Text);
                        decimal InspectionCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[3].Text);
                        decimal STLCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[4].Text);
                        decimal GPTCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[5].Text);
                        decimal OtherCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[6].Text);
                        decimal Total = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[7].Text);

                        int PaymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(Form1ID)).Id;
                        var pd = new Form1PaymentDetails
                        {
                            Id = PaymentId,
                            AmountPaid = Total,
                            BankTransactionID = txtCheckNo.Text,
                            PaymentDate = System.DateTime.Now,
                            PaymentMode = PaymentMode,
                            PaymentStatus = "Y", 
                            ChequeDate = Convert.ToDateTime(txtDateofCheque.Text),
                            //PaySlip = fu_Check.FileBytes != null ? fu_Check.FileBytes : null,
                            Form1ID = Form1ID,
                            GPTCharge = GPTCharge,
                            InspectionCharge = InspectionCharge,
                            OtherCharge = OtherCharge,
                            RegFees = RegFees,
                            STLCharge = STLCharge,
                        };
                        result = form1PaymentDetailService.Update(pd);
                    }
                }
                if (result > 0)
                {
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Payment details saved successfully.";
                    gvPayment.DataSource = null;
                    gvPayment.DataBind();
                    gvPayment.Visible = false;
                    lblTotal.Text = "0.00";
                    pnlPayment.Visible = false;
                    FormOneGridView.DataSource = SelectMethod();
                    FormOneGridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
 

        protected void EPayment_Response(System.Collections.Specialized.NameValueCollection nvc, object sender, EventArgs e)
        {
            int idValue=Convert.ToInt32(Request.QueryString["Id"]);
            List <OnlinePayment> formOnePaymentsRetrieve = new List<OnlinePayment>(); ;
            string bank_txn = Request.Form["mihpayid"];
            string amount = Request.Form["amount"];
            string f_code = Request.Form["status"];
            if(Session["paymentDetails"] !=null)
            {
                 formOnePaymentsRetrieve = (List<OnlinePayment>)Session["paymentDetails"];
            }
            if (f_code != null)
            {
                if (f_code == "success")
                {
                    foreach (var payments in formOnePaymentsRetrieve)
                    {

                        int Form1IDs = payments.RegNumber;
                        Form1Details f1 = formOneService.ReadById(Form1IDs);
                        lblMessage.Text = "Payment Success";
                        int paymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(f1.Id)).Id;
                        var pd = new Form1PaymentDetails
                        {
                            Id = paymentId, // Paymentdetails table ID
                            AmountPaid = payments.TotalAmountPaid,
                            BankTransactionID = bank_txn,
                            PaymentDate = System.DateTime.Now,
                            PaymentMode = "O",
                            PaymentStatus = "Y",
                            Form1ID = f1.Id,
                            GPTCharge = payments.GPTCharge,
                            InspectionCharge = payments.InspectionCharge,
                            OtherCharge = payments.OtherCharge,
                            RegFees = payments.RegFees,
                            STLCharge = payments.STLCharge 

                        };
                        int result = form1PaymentDetailService.Update(pd);
                        if (result == 1)
                        {

                            ResendPaymentDetails(f1.Id);
                           
                        }

                    }
                    Session.Remove("paymentDetails");
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Payment details saved successfully."; 
                    this.Redirect("/Transactions/FormOneView.aspx?Id=" + idValue);
                }
                else
                {
                    lblMessage.Text = "Payment Failed";
                }
            }
        }
        private void ResendPaymentDetails(int formid)
        {

            Form1DetailsTransaction transaction = new Form1DetailsTransaction();
            transaction = form1DetailsTransactionService.GetMaxTransactionDetails(formid);
            if (transaction.TransactionStatus == 100)
            {
                int ToID = transaction.FromID;
                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                var userMaster = userMasterService.GetFromAuthCookie();
                var rft = new Form1DetailsTransaction
                {
                    Id = common.GenerateID("Form1DetailsTransaction"),
                    FromID = userMaster.Id,
                    ToID = ToID,
                    Form1DetailsID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("Form1DetailsTransaction", "Form1DetailsID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = form1DetailsTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.Text = "Payment details saved successfully.";

                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }
            }
        }
        private string getForm1IDs()
        {
            string Form1IDs = "";
            foreach (GridViewRow row in gvPayment.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Form1IDs += Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text) + "_";
                }
            }
            return Form1IDs.Remove(Form1IDs.Length - 1); ;
        }

 

        protected void FormOneGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            FormOneGridView.PageIndex = e.NewPageIndex;
            FormOneGridView.DataBind();
        }
    }
}