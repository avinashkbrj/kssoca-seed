﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormRegistrationApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.RegistrationFormApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <!-- this meta viewport is required for BOLT //-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <!-- BOLT Sandbox/test //-->
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <!-- BOLT Production/Live //-->
    <!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->
       <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

    <script type="text/javascript">
        function PrintPanel() {
           <%-- var panel = document.getElementById("<%=DivMain.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            //  printWindow.document.write('<img src="../img/bc.png">'  );
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();--%>
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" id="DivMain" runat="server" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading">Registration of Seed Producer </asp:Label>
                </div>
                <div>
                    <asp:Label Font-Bold="true" runat="server" ID="lblMessage" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                    <asp:HiddenField ID="RegCropOfferred" runat="server" />
                    <input type="hidden" id="surl" name="surl" value="<%= Session["surl"]%>" />
                    <input type="hidden" id="txnid" name="txnid" value="<%= Session["txnid"]%>" />
                    <input type="hidden" id="hash" name="hash" value="<%= Convert.ToString(Session["hash"])%>" />
                    <input type="hidden" id="amount" name="amount" value="<%= Session["amount"]%>" />
                    <input type="hidden" id="fname" name="fname" value="<%= Session["fname"]%>" />
                    <input type="hidden" id="mobilenumber" name="mobilenumber" value="<%= Session["mobilenumber"]%>" />
                    <input type="hidden" id="email" name="emailid" value="<%= Session["emailid"]%>" />
                </div>

                <br />
                <table border="1" class="table-condensed" width="100%">

                    <tr class="SideHeading">
                        <td colspan="4">1.Basic details &nbsp;&nbsp; 
                            <asp:LinkButton ID="lnkCertificate" runat="server" Text="View Certificate" CssClass="text-success" OnClick="lnkCertificate_Click" Visible="false"></asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">a.Online Application Date  
                        </td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="RegistrationDate"></asp:Label></td>
                        <td width="25%">b.Online Registration Number </td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="RegistrationNumber"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>c.Name of Firm/Institution </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="ProducerName" />
                        </td>

                        <td>d.Name of the Producer  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Name" />
                        </td>
                    </tr>
                    <tr>
                        <td>e.Mobile Number </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="MobileNumber" /></td>

                        <td>f.Email Id </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="EmailId" /></td>
                    </tr>
                    <tr>
                        <td>g.District </td>
                        <td>
                            <asp:DropDownList runat="server" ID="District" AutoPostBack="true" Visible="false">
                            </asp:DropDownList>
                            <asp:Label Font-Bold="true" runat="server" ID="lblDistrict" /></td>

                        <td>h.Taluk </td>
                        <td>
                            <asp:DropDownList runat="server" ID="Taluk" Visible="false">
                            </asp:DropDownList>
                            <asp:Label Font-Bold="true" runat="server" ID="lblTaluk" /></td>
                    </tr>
                    <tr>
                        <td>i.Address location
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Address" /></td>

                        <td>j.Aadhaar Number </td>
                        <td colspan="3">
                            <asp:Label Font-Bold="true" runat="server" ID="AadharNumber" /></td>

                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">2.Firm/Institution details                       
                        </td>
                    </tr>
                    <tr>
                        <td>a.Tin/CGST/KGST Number  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="TinNumber" /></td>

                        <td>b.Season in which area will be offered for certification  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSeason" />
                        </td>
                    </tr>
                    <tr>
                        <td>c.Previous Registration No </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="OldRegistrationNumber" /></td>

                        <td>d.Total Area offered(in Acres) & Quantity Certified during the previous year  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="TotalArea" /></td>
                    </tr>
                    <tr>
                        <td colspan="4">e.Name of the Seed Processing Unit where processing will be undertaken &nbsp;                       
                        
                            <asp:Label Font-Bold="true" runat="server" ID="lblSPU" /></td>
                    </tr>
                    <tr>
                        <td colspan="4" valign="top">f. Probable Locations,Crop & Extent of area of the crop               
                            which will be offered for certification                       
                          
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvcrops" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false" OnRowDataBound="gvcrops_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> . 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Crop">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdncropid" runat="server" Value='<%# Eval("Crop_Id") %>' />
                                            <asp:Label Font-Bold="true" ID="lblcropname" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Location" HeaderText="Location" />
                                    <asp:BoundField DataField="Area" HeaderText="Area (In Acres)" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">3.Documents submitted &nbsp;&nbsp; 
                        </td>
                    </tr>
                    <tr>

                        <td valign="top">a.Producer Photo &nbsp;&nbsp;
                            <asp:Literal runat="server" ID="FarmerPhotoPreview" EnableViewState="false"> 
                            </asp:Literal>
                        </td>
                        <td valign="top">b.Covering letter with Signature & Seal&nbsp;&nbsp;<asp:Literal runat="server" ID="LitSignature" EnableViewState="false"></asp:Literal>
                        </td>
                        <%-- <td valign="top" colspan="2">c.Seal  &nbsp;&nbsp;<asp:Literal runat="server" ID="LitSeal" EnableViewState="false"></asp:Literal>
                        </td>--%>

                        <td valign="top">c.Vat/KGST/CGST Certificate &nbsp;&nbsp;
                            <asp:Literal runat="server" ID="VatCertificatePreview" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top">d.Consent letter of SPU owner/Unit Ownership document &nbsp;&nbsp;<asp:Literal runat="server" ID="UnitConsentLetterPreview" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" align="center">
                            <asp:Label ID="lblmessagePayment" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlPaymentDetails" runat="server" Visible="false">
                        <tr class="SideHeading">
                            <td colspan="4">4.Service Charge Details                       
                            </td>
                        </tr>
                        <tr>
                            <td>a.Registration Fee paid(in Rs.)</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblRegistrationFee" /></td>
                            <td>b.Payment Mode</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblPaymentMode" /></td>
                        </tr>
                        <tr>
                            <td>c.Cheque No./DD No./BankTransaction ID</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblBankTransactionID" />
                                &nbsp;&nbsp;
                                <asp:Literal runat="server" ID="LitCheck" EnableViewState="false"></asp:Literal></td>
                            <td>d.Payment Date.</td>
                            <td>
                                <asp:Label ID="lblPaymentDate" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlPayment" runat="server" Visible="false">
                        <tr class="SideHeading">
                            <td colspan="4">4.Service Charge Details                     
                            </td>
                        </tr>
                        <tr>
                            <td>a.Payment Mode</td>
                            <td>
                                <asp:RadioButtonList ID="rbPaymentMode" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rbPaymentMode_SelectedIndexChanged">
                                    <asp:ListItem Text="&nbsp;DD/Cheque&nbsp;" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="&nbsp;e-Payment&nbsp;" Value="O"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>b.Amount to be paid</td>
                            <td>Rs.<asp:Label ID="lblamounttobepaid" runat="server" Font-Bold="true"></asp:Label>/-
                                <asp:Label ID="lblpaymenttype" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnPaymentType" runat="server" />
                            </td>
                        </tr>
                        <asp:Panel ID="pnlCheck" runat="server">
                            <tr>
                                <td>c.Cheque/DD/Transaction Number<span style="color: red">*</span></td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCheckNo" placeholder="Cheque Number" MaxLength="30" onkeypress="return CheckNumber(event);" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredCheckNo" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtCheckNo" ErrorMessage="Cheque Number required"></asp:RequiredFieldValidator>
                                </td>
                                <td>d.Cheque/DD/Transaction slip <span style="color: red">*</span></td>
                                <td>
                                    <asp:FileUpload runat="server" CssClass="form-control" ID="fu_Check" accept="image/*" />
                                    <asp:RequiredFieldValidator runat="server" ID="Requiredfu_Check" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="fu_Check" ErrorMessage="Cheque Number required"></asp:RequiredFieldValidator>
                                    <cst:FileTypeValidator runat="server" ID="FileTypeCheck" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="fu_Check" ErrorMessage="Allowed file types are {0}.">
                                    </cst:FileTypeValidator>
                                      
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnCheck" Text="Save Payment Details" OnClick="btnCheck_Click" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlOnline" runat="server">
                            <tr>
                                <td colspan="4" align="center">
                                     <input type="submit" CssClass="btn-primary"   value="Pay Online" onclick="launchBOLT(); return false;" /> 
                                    <%--<asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnPayOnline" Text="Pay Online" OnClick="btnPayOnline_Click" /></td>--%>
                            </tr>
                        </asp:Panel>
                    </asp:Panel>

                    <tr class="SideHeading">
                        <td colspan="4">5.Application Status &nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" PostBackUrl="~/Transactions/FormRegistrationCreate.aspx" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Note: Sl.No. 1 is the current status of the application.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Date" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                        <tr>
                            <td valign="top">Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="100%" Height="100px" placeholder="Write something here for your action..."></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ErrorMessage="Give Remarks" Enabled="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Reject Type</td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rbRejectionType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Reject Application&nbsp;&nbsp;" Value="A">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Reject Payment" Value="P">
                                    </asp:ListItem>
                                </asp:RadioButtonList><asp:Label ID="lblrejecttype" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button runat="server" CssClass="btn-danger" CausesValidation="false"
                                    ID="btnReject" Text="Request to Resubmit" OnClick="btnReject_Click" />
                                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                                    ID="Save" Text="Approve" OnClick="Save_Click" />

                        </tr>
                    </asp:Panel>
                </table>
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCheck" />

            <asp:PostBackTrigger ControlID="Save" />

        </Triggers>
    </asp:UpdatePanel>

    <script type="text/javascript">
        $(document).ready(function () {
            createHash();
            


        });
    
</script>	

<script type="text/javascript"> 
    function launchBOLT() {
        //var encryptedk = CryptoJS.AES.encrypt("juvhC4rb", "Secret Passphrase");


        var encryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var encryptedKey =  '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var decryptedSalt = CryptoJS.AES.decrypt(encryptedSalt, "Secret Passphrase");
        var decryptedKey = CryptoJS.AES.decrypt(encryptedKey, "Secret Passphrase");

        var transId = $('#txnid').val();
        var hashid1 = $('#hash').val();

        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
         
        bolt.launch({
            key: decryptedKey.toString(CryptoJS.enc.Utf8),
            txnid: transId,
            hash: hashid1.toString(),
            amount: amt,
            firstname: fname,
            email: $('#emailid').val(),
            phone: mobilenumber,
            productinfo: "KSSOCA Payment",
            udf5: "BOLT_KIT_ASP.NET",
            surl: $('#surl').val(),
            furl: $('#surl').val()
        }, {
            responseHandler: function (BOLT) {
                console.log(BOLT.response.txnStatus);
                if (BOLT.response.txnStatus != 'CANCEL') {
                    //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                    var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                        '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                        '<input type=\"hidden\" name=\"salt\" value=\"' + decryptedSalt.toString(CryptoJS.enc.Utf8) + '\" />' +
                        '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                        '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                        '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                        '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                        '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                        '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                        '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                        '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                        '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                        '</form>';
                    var form = jQuery(fr);
                    jQuery('body').append(form);
                    form.submit();
                }
            },
                catchException: function (BOLT) {
                    alert(BOLT.message);
                }
            });
    }

    function createHash() {
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        var transId = $('#txnid').val();
        var emailid = $('#emailid').val();
        var hashEncryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var hashEncryptedKey = '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var hashDecryptedSalt = CryptoJS.AES.decrypt(hashEncryptedSalt, "Secret Passphrase");
        var hashDecryptedKey = CryptoJS.AES.decrypt(hashEncryptedKey, "Secret Passphrase");
       
        $.ajax({
            url: '/Transactions/Hash.aspx',
            type: 'post',

            data: JSON.stringify({
                key: hashDecryptedKey.toString(CryptoJS.enc.Utf8),
                salt: hashDecryptedSalt.toString(CryptoJS.enc.Utf8),
                txnid: transId,
                amount: amt,
                pinfo: "KSSOCA Payment",
                fname: fname,
                email: emailid,
                mobile: mobilenumber,
                udf5: "BOLT_KIT_ASP.NET"
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (json) {

                if (json['error']) {
                    $('#alertinfo').html('<i class="fa fa-info-circle"></i>' + json['error']);
                }
                else if (json['success']) {
                    $('#hash').val(json['success']);

                }
            }
        });

    }


</script>	

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
