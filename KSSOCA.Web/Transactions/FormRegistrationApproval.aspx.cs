﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using KSSOCA.Core.Validators;
    using System.Web.UI.WebControls;
    using System.Data;

    using System.Collections.Specialized;
    using System.Text;
    using System.Web;
    using System.Configuration;
    using System.Net;

    public partial class RegistrationFormApproval : SecurePage
    {
        TalukMasterService talukService;
        UserMasterService userMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        RegistrationFormService registrationService;
        RegCropOfferredService regCropOfferredService;
        CropMasterService cropMasterService;
        SPURegistrationService spuRegistrationService;
        RegistrationFormTransactionService registrationFormTransactionService;
        PaymentDetailService paymentDetailService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;

        Common common = new Common();
        public RegistrationFormApproval()
        {
            talukService = new TalukMasterService();
            userMasterService = new UserMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            registrationService = new RegistrationFormService();
            regCropOfferredService = new RegCropOfferredService();
            cropMasterService = new CropMasterService();
            spuRegistrationService = new SPURegistrationService();
            registrationFormTransactionService = new RegistrationFormTransactionService();
            paymentDetailService = new PaymentDetailService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                 
                if (Request.QueryString["Id"] != null) // For Approving  Authorities
                {
                    int id = Request.QueryString.Get("Id").ToInt();

                    if (id > 0)
                    {
                       
                        RegistrationForm registrationForm = registrationService.GetById(id);

                        List<RegCropOffered> regCropOfferred = regCropOfferredService.GetByRegistrationId(registrationForm.Id);

                        if (registrationForm == null)
                        {
                            this.Redirect("Transactions/FormRegistrationView.aspx");
                        }
                        else
                        {
                            InitComponent(registrationForm, regCropOfferred);
                        }
                    }
                }
                else // For Producers
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    if (userMaster.Role_Id == 5)
                    {
                        string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                        //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                        surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                        Session.Add("surl", surl);
                        RegistrationForm registrationForm = registrationService.GetByUserId(userMaster.Id);
                        List<RegCropOffered> regCropOfferred = regCropOfferredService.GetByRegistrationId(registrationForm.Id);
                        if (registrationForm == null)
                        {
                            this.Redirect("Transactions/FormRegistrationCreate.aspx");
                        }
                        else
                        {
                            InitComponent(registrationForm, regCropOfferred);
                            if (!IsPostBack)
                            {
                                NameValueCollection nvc = Request.Form;
                                if (nvc.Count > 0)
                                {
                                    EPayment_Response(nvc, registrationForm, sender, e);
                                }
                            }

                        }

                    }
                    else
                    {
                        this.Redirect("Dashboard.aspx");
                    }

                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex); this.Redirect("Error.aspx");
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {

                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMaster.Role_Id);
                int TransactionStatus = 0;
                int ToID = 0;
                if (RoleID == 1)
                {
                    ToID = Convert.ToInt16(registrationService.GetById(Convert.ToInt16(Id.Value.ToInt())).ProducerID);
                    FinalApproval(Id.Value.ToInt());
                    TransactionStatus = 5; // As no reporting manager for director , send the status back to producer
                }
                else
                {
                    ToID = Convert.ToInt16(userMaster.Reporting_Id);
                    TransactionStatus = Convert.ToInt16(userMasterService.GetById(Convert.ToInt16(userMaster.Reporting_Id)).Role_Id);
                }
                var rft = new RegistrationFormTransaction
                {
                    FromID = userMaster.Id,
                    ToID = ToID,
                    RegistrationFormID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    Remarks = txtRemarks.Text,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("RegistrationFormTransaction", "RegistrationFormID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = registrationFormTransactionService.Create(rft);
                    if (result == 1)
                    {
                        var destDetails=userMasterService.GetById(ToID);
                        if (userMaster.Role_Id == 4)//ADSC
                        { lblMessage.Text = "Forwarded to DDSC successfully."; }
                        else if (userMaster.Role_Id == 3) // DDSC
                        { lblMessage.Text = "Recommended to Director successfully."; }
                        else if (userMaster.Role_Id == 1) // Director
                        { lblMessage.Text = "Approved successfully."; }
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        Messaging.SendSMS("Dear user, Your firm registration Has Been " + lblMessage.Text + " by " + userMaster.Name + "", MobileNumber.Text, "1");
                       // Messaging.SendSMS("New Seed Processing Form Application Recieved From " + lblMessage.Text + " by " + userMaster.Name + "", destDetails.MobileNo, "1");
                        Page_Load(sender, e);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
        private int FinalApproval(int RegFormId)
        {
            string RegNo = registrationService.GenerateRegistrationNumber();
            DateTime validity = Convert.ToDateTime("31/03/" + RegNo.Substring(RegNo.Length - 4));
            var rf = new RegistrationForm
            {
                Id = Id.Value.ToInt(),                
                RegistrationNo = RegNo,
                Validity = validity,
                StatusCode = 1
            };
            return registrationService.Update(rf);
            
        }
        private void InitComponent(RegistrationForm registrationForm, List<RegCropOffered> cropOfferred)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' /> </a></span>";

            var userMasterDetails = userMasterService.GetById(Convert.ToInt16(registrationForm.ProducerID));
             

            var seasons = seasonMasterService.GetAll();
            var districts = districtMasterService.GetAll();
            var seedProcessingUnits = spuRegistrationService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            Id.Value = registrationForm.Id.ToString();
            RegistrationNumber.Text = registrationForm.RegistrationNo;
            ProducerName.Text = userMasterDetails.Name;
            AadharNumber.Text = registrationForm.AadhaarNumber;
            Address.Text = registrationForm.Address;
            MobileNumber.Text = userMasterDetails.MobileNo;
            EmailId.Text = userMasterDetails.EmailId;
            TinNumber.Text = registrationForm.TinNo;
            Name.Text = registrationForm.OfficialName;
            OldRegistrationNumber.Text = registrationForm.PrevRegNo;
            TotalArea.Text = registrationForm.TotalArea;
            // RegistrationFee.Text = rftTableAdapter.GetRegistrationFees().ToString();

            District.Items.FindByValue(userMasterDetails.District_Id.ToString()).Selected = true;
            lblDistrict.Text = District.SelectedItem.ToString();

            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            Taluk.Items.FindByValue(userMasterDetails.Taluk_Id.ToString()).Selected = true;
            lblTaluk.Text = Taluk.SelectedItem.ToString();

            lblSeason.Text = seasonMasterService.GetSeasonsBy(registrationForm.Season_Id);
            if (registrationForm.SeedProcessUnit_Id != 0)
            {
                lblSPU.Text = spuRegistrationService.GetById(Convert.ToInt16(registrationForm.SeedProcessUnit_Id)).Name;
            }
            RegistrationDate.Text = registrationForm.RegistrationDate == null ? string.Empty : registrationForm.RegistrationDate.Value.ToString("dd/MM/yyyy");


            if (registrationForm.Photo != null && registrationForm.Photo.Length > 0)  
                FarmerPhotoPreview.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Photo));

            if (registrationForm.VATImage != null && registrationForm.VATImage.Length > 0)
                VatCertificatePreview.Text = string.Format(previewString, Serializer.Base64String(registrationForm.VATImage));

            if (registrationForm.LetterImage != null && registrationForm.LetterImage.Length > 0)
                UnitConsentLetterPreview.Text = string.Format(previewString, Serializer.Base64String(registrationForm.LetterImage));

            if (registrationForm.Sign != null && registrationForm.Sign.Length > 0)
                LitSignature.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Sign));

            //if (registrationForm.Seal != null && registrationForm.Seal.Length > 0)
            //    LitSeal.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Seal));

            Session.Add("fname", ProducerName.Text);
            Session.Add("mobilenumber", MobileNumber.Text);
            Session.Add("emailid", EmailId.Text);
            Session.Add("txnid", Id.Value);
            gvcrops.DataSource = cropOfferred;
            gvcrops.DataBind();
            gvstatus.DataSource = registrationFormTransactionService.GetAllTransactionsBy(registrationForm.Id);
            gvstatus.DataBind();
            int paymentstatus = CheckPaymentDetails(Convert.ToInt16(registrationForm.ProducerID));
            if (paymentstatus == 1)
            {
                pnlPaymentDetails.Visible = true;
            }
            else
            {
                pnlPaymentDetails.Visible = false;
                lblmessagePayment.Text = "Payment details not updated yet.";
            }
            var userMaster = userMasterService.GetFromAuthCookie();

            RegistrationFormTransaction transaction = new RegistrationFormTransaction();
            transaction = registrationFormTransactionService.GetMaxTransactionDetails(registrationForm.Id);
            int ActionRole_Id = transaction.ToID;
            Session.Add("amount", lblamounttobepaid.Text);
            if (userMaster.Role_Id == 5) // SPU Permission
            {
                pnlRemarks.Visible = false;
                if (transaction.TransactionStatus == 0)
                {
                    btnEditApplication.Visible = true;
                }
                if (paymentstatus == 0)
                {
                    pnlPaymentDetails.Visible = false;
                    pnlPayment.Visible = true;
                }
                else if (paymentstatus == 1)
                {
                    pnlPaymentDetails.Visible = true;
                    pnlPayment.Visible = false;
                }
                if (registrationForm.ProducerID != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
                checkPaymentMode();
            }
            else
            {
                pnlPayment.Visible = false;
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                    if (lblPaymentMode.Text == "Online")
                    {
                        foreach (ListItem li in rbRejectionType.Items)
                        {
                            if (li.Text == "Reject Payment")
                            {
                                li.Attributes.Add("style", "display:none");
                            }
                        } 
                    }
                    if (userMaster.Role_Id == 4)//ADSC
                    { Save.Text = "Forward to DDSC"; }
                    else if (userMaster.Role_Id == 3) // DDSC
                    { Save.Text = "Recommend to Director"; }
                    else if (userMaster.Role_Id == 1) // Director
                    { Save.Text = "Approve Firm"; }
                }
                else
                {
                    pnlRemarks.Visible = false;
                }
                if (paymentstatus == 0)
                {
                    pnlRemarks.Visible = false;
                }
            }
            if (registrationForm.StatusCode == 1)
            {
                lnkCertificate.Visible = true;
                pnlRemarks.Visible = false;
                pnlPayment.Visible = false;
            }
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
            }
        }
        protected void gvcrops_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField Crop_Id = (e.Row.FindControl("hdncropid") as HiddenField);
                Label lblcropname = (e.Row.FindControl("lblcropname") as Label);
                string cropname = cropMasterService.GetById(Convert.ToInt16(Crop_Id.Value)).Name;
                lblcropname.Text = cropname;
            }
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        protected void lnkCertificate_Click(object sender, EventArgs e)
        {
            this.Redirect("Reports/FirmRegistrationCertificate.aspx?Id=" + Id.Value.ToInt() + "");
        }
        private int CheckPaymentDetails(int UserID)
        {
            //lblamounttobepaid.Text = common.getFees("SPU").ToString();
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View </a></span>";
            PaymentDetails pd = paymentDetailService.GetBy(UserID);
            if (pd != null)
            {
                string PaymentStatus = Convert.ToString(pd.PaymentStatus);
                if (PaymentStatus == "Y")
                {
                    lblRegistrationFee.Text = Convert.ToString(pd.AmountPaid);
                    lblPaymentDate.Text = pd.PaymentDate.Value.ToString("dd/MM/yyyy");
                    lblPaymentMode.Text = Convert.ToString(pd.PaymentMode) == "C" ? "Cheque/DD" : "Online";
                    lblBankTransactionID.Text = Convert.ToString(pd.BankTransactionID);
                    byte[] check = pd.PaySlip;
                    if (check != null && check.Length > 0)
                        LitCheck.Text = string.Format(previewString, Serializer.Base64String(check));
                    return 1;
                }
                else if (PaymentStatus == "N")
                {
                    if (pd.PaymentType != null)
                    {
                        hdnPaymentType.Value = "RFR"; // Renewal
                        lblamounttobepaid.Text = common.getFees("RFR").ToString(); // Renewal fees
                        return 0;
                    }
                    else
                    {
                        hdnPaymentType.Value = "RF"; // first time registration
                        lblamounttobepaid.Text = common.getFees("RF").ToString(); // Registartion fees
                        return 0;
                    }
                }
                else if (PaymentStatus == "R")
                {
                    lblamounttobepaid.Text = common.getFees(pd.PaymentType).ToString();
                    hdnPaymentType.Value = pd.PaymentType;
                }
                return 0;
            }
            else
            {
                lblamounttobepaid.Text = common.getFees("SPU").ToString();
                return 0;
            }
         
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                decimal AmountToPay = lblamounttobepaid.Text.ToDecimal();
                int PaymentId = paymentDetailService.GetBy(Convert.ToInt16(registrationService.GetById(Id.Value.ToInt()).ProducerID)).Id;
                var pd = new PaymentDetails
                {
                    Id = PaymentId,
                    AmountPaid = AmountToPay,
                    BankTransactionID = txtCheckNo.Text,
                    PaymentDate = System.DateTime.Now,
                    PaymentMode = rbPaymentMode.SelectedValue,
                    PaymentType = "RF",
                    PaymentTypeRefID = Id.Value.ToInt(),
                    UserID = userMasterService.GetFromAuthCookie().Id,
                    PaymentStatus = "Y",
                    PaySlip = fu_Check.FileBytes
                };
                int result = paymentDetailService.Update(pd);
                if (result == 1)
                {
                    ResendPaymentDetails(PaymentId);
                    var userMaster = userMasterService.GetFromAuthCookie();

                    int adscId = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                              (Convert.ToInt16(userMaster.District_Id),
                               Convert.ToInt16(userMaster.Taluk_Id), 6).User_Id);
                    var adscDetails = userMasterService.GetById(adscId);
                    Messaging.SendSMS("New Seed Producing Firm application recieved from " + ProducerName.Text + " ", adscDetails.MobileNo, "1");

                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Payment details saved successfully.";
                    lblmessagePayment.Text = "";
                    Page_Load(sender, e);
                }
            }
        }
        private void ResendPaymentDetails(int txnid)
        {
            RegistrationFormTransaction transaction = new RegistrationFormTransaction();
            transaction = registrationFormTransactionService.GetMaxTransactionDetails(Id.Value.ToInt());
            if (transaction.TransactionStatus == 100)
            {
                int ToID = transaction.FromID;
                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                var userMaster = userMasterService.GetFromAuthCookie();
                var rft = new RegistrationFormTransaction
                {
                    FromID = userMaster.Id,
                    ToID = ToID,
                    RegistrationFormID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("RegistrationFormTransaction", "RegistrationFormID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = registrationFormTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.Text = "Payment details saved successfully.";
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }
            }
        }
        
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (rbRejectionType.SelectedValue == "A" || rbRejectionType.SelectedValue == "P")
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMaster.Role_Id);
                    int TransactionStatus = 0;
                    RegistrationForm rf = registrationService.GetById(Id.Value.ToInt());
                    if (rbRejectionType.SelectedValue == "A")
                    {
                        Messaging.SendSMS("Firm Registration Rejected By " + userMaster.Name + "", MobileNumber.Text, "1");
                        TransactionStatus = 0;
                    }
                    else if (rbRejectionType.SelectedValue == "P")
                    {
                        TransactionStatus = 100; // 100 indicates payment rejection
                        int i = RejectPayment(Convert.ToInt16(rf.ProducerID));
                        if (i == 1)
                        { Messaging.SendSMS("SPU registration payment rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                    var rft = new RegistrationFormTransaction
                    {
                        FromID = userMaster.Id,
                        ToID = Convert.ToInt16(rf.ProducerID),
                        RegistrationFormID = Id.Value.ToInt(),
                        TransactionDate = System.DateTime.Now,
                        TransactionStatus = TransactionStatus, // indicates rejection
                        Remarks = txtRemarks.Text,
                        IsCurrentAction = 1
                    };
                    int IsCurrentUserSet = common.setIsCurrentAction("RegistrationFormTransaction", "RegistrationFormID", Id.Value.ToInt());
                    if (IsCurrentUserSet > 0)
                    {
                        int result = registrationFormTransactionService.Create(rft);
                        if (result == 1)
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Rejected successfully.";
                            Page_Load(sender, e);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                }
                else { lblrejecttype.Text = "Please select rejection type."; }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
        private int RejectPayment(int UserID)
        {
            string query = "update PaymentDetails set PaymentStatus='R' where UserID= " + UserID;
            return common.ExecuteNonQuery(query);
        }
        protected void EPayment_Response(NameValueCollection nvc, RegistrationForm registrationForm, object sender, EventArgs e)
        {
            string bank_txn = Request.Form["txnid"];
            string amount = Request.Form["amount"];
            string f_code = Request.Form["status"];
            string transaction_id = Request.Form["mihpayid"];
            if (f_code != null)
            {
                CheckPaymentDetails(Convert.ToInt16(registrationForm.ProducerID));

                if (Convert.ToString(f_code).Trim().ToLower() == "success")
                {
                    lblMessage.Text = "Payment Success";
                    int PaymentId = paymentDetailService.GetBy(Convert.ToInt16(registrationService.GetById(Id.Value.ToInt()).ProducerID)).Id;
                    var pd = new PaymentDetails
                    {
                        Id = PaymentId, // Paymentdetails table ID
                        AmountPaid = amount.ToDecimal(),
                        BankTransactionID = transaction_id,
                        PaymentDate = System.DateTime.Now,
                        PaymentMode = "O",
                        PaymentType = hdnPaymentType.Value,
                        PaymentTypeRefID = registrationForm.Id,
                        UserID = Convert.ToInt16(registrationForm.ProducerID),
                        PaymentStatus = "Y"
                    };
                    int result = paymentDetailService.Update(pd);
                    if (result == 1)
                    {
                        ResendPaymentDetails(Convert.ToInt16(bank_txn));
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Payment details saved successfully.";
                        
                        this.Redirect("/Transactions/FormRegistrationApproval.aspx");
                    }
                }
                else
                {
                    lblMessage.Text = "Payment Failed";
                }
            }
        }
       
    }
}