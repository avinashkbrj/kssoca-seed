﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="FormRegistrationCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.FormRegistrationCreate" %>

<%@ Register Namespace="KSSOCA.Core.Validators" TagPrefix="cst" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    &nbsp;&nbsp;<asp:Button ID="btnViewApplication" runat="server" class="btn-primary" Text="View Application" Visible="false" PostBackUrl="~/Transactions/FormRegistrationApproval.aspx" CausesValidation="false" />                    
                    <asp:HiddenField ID="RegCropOfferred" runat="server" />
                    <asp:HiddenField ID="hdnFarmerPhoto" runat="server" />
                    <asp:HiddenField ID="hdnVatCertificate" runat="server" />
                    <asp:HiddenField ID="hdnUnitConsentLetter" runat="server" />
                    <asp:HiddenField ID="hdnfu_Sign" runat="server" />
                    <asp:HiddenField ID="hdnfu_seal" runat="server" />
                </div>
                
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>

                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Producers' Basic details                       
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">1.Name of Firm/Institution<span style="color: red">*</span></td>
                        <td width="25%">
                            <asp:TextBox runat="server" CssClass="form-control" ID="ProducerName" placeholder="Producer Name" />

                        </td>

                        <td width="25%">2.Email Id<span style="color: red">*</span></td>
                        <td width="25%">
                            <asp:TextBox runat="server" class="form-control" ID="EmailId" placeholder="Email Id" />
                        </td>
                    </tr>
                    <tr>
                        <td>3.Mobile Number</td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="MobileNumber" MaxLength="10" placeholder="Mobile Number" onkeypress="return CheckNumber(event);" />
                        </td>

                        <td>4.District</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtDistrict" CausesValidation="true" /> 
                        </td>
                    </tr>
                    <tr>
                        <td>5.Taluk</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtTaluk" CausesValidation="true" />

                        </td>

                        <td>6.Name of the Official<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Name" placeholder="Official Name" CausesValidation="true" onkeypress="return CheckName(event);" />
                            <asp:RequiredFieldValidator runat="server" ID="NameRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="Name" ErrorMessage="Official Name Required"></asp:RequiredFieldValidator>

                        </td>
                    </tr>
                    <tr>
                        <td>7.Aadhaar Number<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="AadharNumber" MaxLength="12" placeholder="Aadhaar Number" onkeypress="return CheckNumber(event);" />
                            <asp:RequiredFieldValidator runat="server" ID="AdharRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AadharNumber" ErrorMessage="Adhar Card number Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="AadhaarNumberRegularEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[0-9]{12,12}$" ControlToValidate="AadharNumber"
                                ErrorMessage="Please Enter Valid Aadhaar number"></asp:RegularExpressionValidator>
                        </td>

                        <td>8.Address location<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Address" placeholder="Address" TextMode="MultiLine" Width="90%" />
                            <asp:RequiredFieldValidator runat="server" ID="AddressRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="Address" ErrorMessage="Address Required"></asp:RequiredFieldValidator> 
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Firm/Institution details                       
                        </td>
                    </tr>
                    <tr>
                        <td>9.Tin/CGST/KGST Number<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="TinNumber" MaxLength="15" placeholder="Tin Number" />
                            <asp:RequiredFieldValidator runat="server" ID="TinNumberRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="TinNumber" ErrorMessage="Tin/CGST/KGST Number Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="TinNumberRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9]{1,20}$" ControlToValidate="MobileNumber"
                                ErrorMessage="Please Enter Valid Number"></asp:RegularExpressionValidator>
                        </td> 
                        <td>10.Season in which area will be offered for certification<span style="color: red">*</span></td>
                        <td>
                            <%--<asp:DropDownList runat="server" CssClass="form-control" ID="Seasons">
                            </asp:DropDownList>--%>
                            <asp:CheckBoxList runat="server" RepeatDirection="Horizontal" CssClass="radio-inline" ID="chkSeasons"></asp:CheckBoxList>
                            <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredSeasons" Style="color: red" Display="Dynamic"
                                ControlToValidate="chkSeasons" ErrorMessage="Season Required" ></asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>11.Previous Registration No if already a registered Seed Producer</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="OldRegistrationNumber" placeholder="Old Registration Number" />
                            <asp:RegularExpressionValidator runat="server" ID="OldRegistrationNumberRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9_\s-/]{3,15}$" ControlToValidate="OldRegistrationNumber"
                                ErrorMessage="Old Registration required min of 3 and max of 15 characters"></asp:RegularExpressionValidator>
                        </td>

                        <td>12.Total Area offered(in Acres) & Quantity Certified during the previous year </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TotalArea" placeholder="Total Area" />
                            <asp:RegularExpressionValidator runat="server" ID="TotalAreaRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9_\s-]{1,15}$" ControlToValidate="TotalArea"
                                ErrorMessage="TotalArea required min of 1 characters"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">13.Name of the Seed Processing Unit where processing will be undertaken<span style="color: red">*</span></td>
                        <td colspan="2">
                            <asp:DropDownList runat="server" class="form-control" ID="SeedProcessingUnit">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredSeedProcessingUnit" Style="color: red" Display="Dynamic"
                                ControlToValidate="SeedProcessingUnit" ErrorMessage="Seed Processing Unit Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top">14.Probable Locations,Crop &amp; Extent of area of the crop which will be offered for certification<span style="color: red">*</span></td>
                        <td colspan="2">
                            <table id="dynamicTable" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">Crop</th>
                                        <th class="text-center">Location</th>
                                        <th class="text-center">Area (In Acres)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                                <% 
                                    if (this.EnableSave)

                                    { %>
                            <a id="add_row" class="btn btn-default pull-left">Add Crop</a> 
                            <a id="delete_row" class="btn btn-default pull-left">Delete Crop</a> <%} %></td>
                    </tr> 
                    <tr class="SideHeading">
                        <td colspan="4">Documents to be submitted                       
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">15.Producer Photo<span style="color: red">*</span>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="FarmerPhoto" accept="image/*" />

                            <asp:RequiredFieldValidator runat="server" ID="RequiredFarmerPhoto" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="Please Upload Producer Photo"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FarmerPhotoSizeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FarmerPhotoTypeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitFarmerPhoto" EnableViewState="false"></asp:Literal>
                        </td>  

                        <td valign="top">16.Covering letter with Signature & Seal<span style="color: red">*</span>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_Sign" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="Requiredfu_Sign" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_Sign" ErrorMessage="Please Upload Signature"></asp:RequiredFieldValidator>

                            <cst:FileSizeValidator runat="server" id="FileSizefu_Sign" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_Sign" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator id="FileTypefu_Sign" runat="server"
                                ControlToValidate="fu_Sign"
                                ForeColor="Red" Display="Dynamic"
                                ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitSignature" EnableViewState="false"></asp:Literal>
                        </td>

                       <%-- <td>17.Stampped  of the firm<span style="color: red">*</span>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_seal" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="Requiredfu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="Please upload Stampped Seal"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizefu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypefu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="Litfu_seal" EnableViewState="false"></asp:Literal>
                        </td>--%>
                        <td valign="top">17.Vat/GST Certificate<span style="color: red">*</span>
                            <asp:FileUpload ID="VatCertificate" runat="server" accept="image/*" CssClass="form-control" />
                            <asp:RequiredFieldValidator ID="RequiredVatCertificate" runat="server" ControlToValidate="VatCertificate" Display="Dynamic" ErrorMessage="Please Upload Vat/GST Certificate" ForeColor="Red"></asp:RequiredFieldValidator>
                            <cst:FileTypeValidator ID="VatCertificateTypeValidator" runat="server" ControlToValidate="VatCertificate" Display="Dynamic" ErrorMessage="Allowed file types are {0}." ForeColor="Red">
                            </cst:FileTypeValidator>
                            <asp:Literal ID="LitVatCertificate" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                     
                        <td valign="top" >18.Consent letter of SPU owner/Unit Ownership document<span style="color: red">*</span>
                            <asp:FileUpload ID="UnitConsentLetter" runat="server" accept="image/*" CssClass="form-control" />
                            <asp:RequiredFieldValidator ID="RequiredUnitConsentLetter" runat="server" ControlToValidate="UnitConsentLetter" Display="Dynamic" ErrorMessage="Please Upload Unit Consent Letter" ForeColor="Red"></asp:RequiredFieldValidator>
                            <cst:FileTypeValidator ID="UnitConsentLetterTypeValidator" runat="server" ControlToValidate="UnitConsentLetter" Display="Dynamic" ErrorMessage="Allowed file types are {0}." ForeColor="Red">
                            </cst:FileTypeValidator>
                            <asp:Literal ID="LitUnitConsentLetter" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:CheckBox ID="chk_Declare" runat="server" Checked="true" />
                            I here by declare that the above information furnished is true to the best of my knowledge and belief and if any information is found false/in correct I am agreeable to accept any action by KSSOCA. </td>
                    </tr>
                </table>
            </div>
            <br />
            <div>
                <div align="center">
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="btnUpdate" Text="Re-Save" OnClick="btnUpdate_Click" Visible="false" />
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="Save" Text="Save" OnClick="Save_Click" />

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="Save" />

        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript" src='<%= ResolveUrl("~/js/formRegistrationScript.js") %>'></script>
</asp:Content>
