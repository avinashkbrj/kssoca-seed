﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Data;
    public partial class FormRegistrationCreate : SecurePage
    {
        TalukMasterService talukService;
        UserMasterService userMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        RegistrationFormService registrationService;
        RegCropOfferredService regCropOfferredService;
        SPURegistrationService seedProcessUnitMasterService;
        RegistrationForm registrationForm;
        Common common = new Common();
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        RegistrationFormTransactionService registrationFormTransactionService;
        PaymentDetailService paymentDetailService;
        public bool EnableSave { get; set; }

        public FormRegistrationCreate()
        {
            talukService = new TalukMasterService();
            userMasterService = new UserMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            registrationService = new RegistrationFormService();
            regCropOfferredService = new RegCropOfferredService();
            seedProcessUnitMasterService = new SPURegistrationService();
            EnableSave = true;
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            registrationFormTransactionService = new RegistrationFormTransactionService();
            paymentDetailService = new PaymentDetailService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    lblHeading.Text = common.getHeading("FRC");
                    litNote.Text = common.getNote("FRC");// FRC is Short name of the page                   
                    var user = userMasterService.GetFromAuthCookie();
                    registrationForm = registrationService.GetByUserId(userMasterService.GetFromAuthCookie().Id);
                    if (registrationForm != null)
                    {
                        List<RegCropOffered> rco = regCropOfferredService.GetByRegistrationId(registrationForm.Id);
                        RegistrationFormTransaction transaction = new RegistrationFormTransaction();
                        transaction = registrationFormTransactionService.GetMaxTransactionDetails(registrationForm.Id);
                        if (transaction != null)
                        {
                            if (transaction.TransactionStatus == 0 && transaction.ToID == userMasterService.GetFromAuthCookie().Id) //TransactionStatus 0 indicates application is  rejected needs to be edited
                            {
                                lblMessage.Text = "Application has been rejected.";
                                InitComponent(registrationForm, rco);
                                btnUpdate.Visible = true;
                                Save.Visible = false;
                                btnViewApplication.Visible = true;
                            }
                            else  if (transaction.TransactionStatus == 5) //TransactionStatus 0 indicates application is  rejected needs to be edited
                            {
                                if (registrationForm.Validity >= System.DateTime.Now)
                                {
                                    this.Redirect("Transactions/FormRegistrationApproval.aspx", false);
                                }
                                else
                                {
                                    lblMessage.Text = "Firm Validity Expired.";
                                    common.ExpireFirm(user.Id, registrationForm.Id);
                                    InitComponent(registrationForm, rco);
                                    btnUpdate.Visible = true;
                                    Save.Visible = false;
                                }
                            }
                            //else if (registrationForm.Validity <= System.DateTime.Now)
                            //{

                            //}
                            else
                            {
                                this.Redirect("Transactions/FormRegistrationApproval.aspx", false);
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Firm Validity Expired.";
                            common.ExpireFirm(user.Id, registrationForm.Id);
                            InitComponent(registrationForm, rco);
                            btnUpdate.Visible = true;
                            Save.Visible = false;
                        }
                    }
                    else
                    {
                        InitComponent();
                    }
                }
                else
                {
                    var regCropOfferred = new List<RegCropOffered>();
                    if (Request.Form["cropId[]"] != null)
                    {
                        string[] cropId = Request.Form["cropId[]"].Split(",".ToCharArray());
                        string[] crop = Request.Form["crop[]"].Split(",".ToCharArray());
                        string[] location = Request.Form["location[]"].Split(",".ToCharArray());
                        string[] area = Request.Form["area[]"].Split(",".ToCharArray());

                        for (int i = 0; i < crop.Length; i++)
                        {
                            regCropOfferred.Add(new RegCropOffered()
                            {
                                Crop_Id = crop[i].ToInt(),
                                Area = area[i],
                                Location = location[i],

                            });
                        }
                    }
                    RegCropOfferred.Value = regCropOfferredService.ConvertToJson(regCropOfferred);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                // this.Redirect("Error.aspx");
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    string[] crop = Request.Form["crop[]"].Split(",".ToCharArray());
                    string[] location = Request.Form["location[]"].Split(",".ToCharArray());
                    string[] area = Request.Form["area[]"].Split(",".ToCharArray());
                    var userMaster = userMasterService.GetFromAuthCookie();
                    var regNoWithDate = userMaster.Id + "/" + DateTime.Now.ToFinancialYear();
                    var registrationForm = new RegistrationForm
                    {
                        
                        RegistrationNo = Convert.ToString(regNoWithDate),
                        ProducerID = userMaster.Id,
                        RegistrationDate = System.DateTime.Now,
                        AadhaarNumber = AadharNumber.Text,
                        Address = Address.Text,
                        TinNo = TinNumber.Text,
                        OfficialName = Name.Text,
                        Season_Id = common.getCheckedItems(chkSeasons),
                        PrevRegNo = OldRegistrationNumber.Text,
                        SeedProcessUnit_Id = SeedProcessingUnit.Items.Count != 0 ? SeedProcessingUnit.SelectedItem.Value.ToNullableInt() : 0,
                        TotalArea = TotalArea.Text,
                        Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : null,
                        VATImage = VatCertificate.FileBytes.Length > 0 ? VatCertificate.FileBytes : null,
                        LetterImage = UnitConsentLetter.FileBytes.Length > 0 ? UnitConsentLetter.FileBytes : null,
                        Sign = fu_Sign.FileBytes.Length > 0 ? fu_Sign.FileBytes : null,
                       // Seal = fu_seal.FileBytes.Length > 0 ? fu_seal.FileBytes : null,
                        StatusCode = 0,
                        RegistrationType = "N", // N indicates new registration
                        Validity = null,
                    };
                    int recordCount = registrationService.Create(registrationForm);
                    if (recordCount > 0)
                    {
                        registrationForm = registrationService.GetByUserId(userMasterService.GetFromAuthCookie().Id);
                        var regCropOfferred = new List<RegCropOffered>();

                        for (int i = 0; i < crop.Length; i++)
                        {
                            regCropOfferred.Add(new RegCropOffered()
                            {
                                RegistrationForm_Id = registrationForm.Id,
                                Crop_Id = crop[i].ToInt(),
                                Area = area[i],
                                Location = location[i]
                            });
                        }

                        recordCount += regCropOfferredService.Create(regCropOfferred).Count;

                        if (recordCount > 0)
                        {
                            int RegistrationFormID = registrationService.GetByUserId(userMaster.Id).Id;
                            int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                      (Convert.ToInt16(userMaster.District_Id),
                                       Convert.ToInt16(userMaster.Taluk_Id), 4).User_Id);
                            var rft = new RegistrationFormTransaction
                            {
                                FromID = userMaster.Id,
                                ToID = ToID,
                                RegistrationFormID = RegistrationFormID,
                                TransactionDate = System.DateTime.Now,
                                TransactionStatus = 4,
                                IsCurrentAction = 1
                            };
                            int result = registrationFormTransactionService.Create(rft);
                            if (result == 1)
                            {
                                var pd = new PaymentDetails
                                {
                                    UserID = userMaster.Id,
                                    PaymentStatus = "N",
                                };
                                int Paymentresult = paymentDetailService.Create(pd);
                                if (Paymentresult == 1)
                                {
                                    this.Redirect("Transactions/FormRegistrationApproval.aspx", false);
                                }
                                else
                                {
                                    lblMessage.Text = "Error occurred while saving data.";
                                }
                            }
                            else
                            {
                                lblMessage.Text = "Error occurred while saving data.";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred while saving data.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
        private void ClearForm()
        {
            //RegistrationNumber.Text = string.Empty;
            ProducerName.Text = string.Empty;
            Address.Text = string.Empty;
            MobileNumber.Text = string.Empty;
            EmailId.Text = string.Empty;
            TinNumber.Text = string.Empty;

            Name.Text = string.Empty;
            OldRegistrationNumber.Text = string.Empty;
            TotalArea.Text = string.Empty;
            AadharNumber.Text = string.Empty;
        }

        private void InitComponent(RegistrationForm registrationForm = null, List<RegCropOffered> cropOfferred = null)
        {
            var seasons = seasonMasterService.GetAll();
            var seedProcessingUnits = seedProcessUnitMasterService.GetAll();
            chkSeasons.BindCheckBoxItem<SeasonMaster>(seasons);
            SeedProcessingUnit.BindListItem<SPURegistration>(seedProcessingUnits);
            var userData = userMasterService.GetFromAuthCookie();
            //RegistrationNumber.Text = userData.Reg_no;
            ProducerName.Text = userData.Name;
            MobileNumber.Text = userData.MobileNo;
            EmailId.Text = userData.EmailId;
            var districts = districtMasterService.GetAll();
            txtDistrict.Text = districtMasterService.GetById(Convert.ToInt16(userData.District_Id)).Name;
            txtTaluk.Text = talukService.GetBy(Convert.ToInt16(userData.District_Id), Convert.ToInt16(userData.Taluk_Id.ToString())).Name;

            //ProducerName.ReadOnly = true;
            //MobileNumber.ReadOnly = ProducerName.ReadOnly;
            //EmailId.ReadOnly = MobileNumber.ReadOnly;
            txtDistrict.ReadOnly = true;
            txtTaluk.ReadOnly = true;

            if (registrationForm != null)
            {
                string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";

                AadharNumber.Text = registrationForm.AadhaarNumber;
                Address.Text = registrationForm.Address;
                TinNumber.Text = registrationForm.TinNo;
                Name.Text = registrationForm.OfficialName;
                //Seasons.SelectedValue = registrationForm.Season_Id.ToString();
                OldRegistrationNumber.Text = registrationForm.PrevRegNo;
                if (registrationForm.SeedProcessUnit_Id != 0)
                {
                    SeedProcessingUnit.Items.FindByValue(registrationForm.SeedProcessUnit_Id.ToString()).Selected = true;
                }
                TotalArea.Text = registrationForm.TotalArea;
                hdnFarmerPhoto.Value = registrationForm.Photo!=null?Serializer.Base64String(registrationForm.Photo):"";
                hdnVatCertificate.Value = registrationForm.VATImage!=null?Serializer.Base64String(registrationForm.VATImage) : "";
                hdnUnitConsentLetter.Value = registrationForm.LetterImage != null ? Serializer.Base64String(registrationForm.LetterImage) : "";
                hdnfu_Sign.Value = registrationForm.Sign != null ? Serializer.Base64String(registrationForm.Sign) : "";
                hdnfu_seal.Value = registrationForm.Seal != null ? Serializer.Base64String(registrationForm.Seal) : "";

                if (registrationForm.Photo != null && registrationForm.Photo.Length > 0)
                    LitFarmerPhoto.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Photo));
                if (registrationForm.Sign != null && registrationForm.Sign.Length > 0)

                    LitSignature.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Sign));
                //if (registrationForm.Seal != null && registrationForm.Seal.Length > 0)
                //    Litfu_seal.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Seal));
                if (registrationForm.VATImage != null && registrationForm.VATImage.Length > 0)
                    LitVatCertificate.Text = string.Format(previewString, Serializer.Base64String(registrationForm.VATImage));
                if (registrationForm.LetterImage != null && registrationForm.LetterImage.Length > 0)
                    LitUnitConsentLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.LetterImage));

                RequiredFarmerPhoto.Enabled = false;
                Requiredfu_Sign.Enabled = false;
                //Requiredfu_seal.Enabled = false;
                RequiredVatCertificate.Enabled = false;
                RequiredUnitConsentLetter.Enabled = false;
            }
            RegCropOfferred.Value = regCropOfferredService.ConvertToJson(cropOfferred);
        }
        private void saveName(int userid)
        {
            string query = @"update usermaster set name ='" + ProducerName.Text.Trim() + "' where id =" + userid;
            common.ExecuteNonQuery(query);
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string[] crop = Request.Form["crop[]"].Split(",".ToCharArray());
                string[] location = Request.Form["location[]"].Split(",".ToCharArray());
                string[] area = Request.Form["area[]"].Split(",".ToCharArray());
                var userMaster = userMasterService.GetFromAuthCookie();
                RegistrationForm rf = registrationService.GetByUserId(userMaster.Id);
                var registrationForm = new RegistrationForm
                {
                    Id = rf.Id,
                    //RegistrationNo = userMaster.Reg_no,
                    ProducerID = userMaster.Id,
                    RegistrationDate = System.DateTime.Now,
                    AadhaarNumber = AadharNumber.Text,
                    Address = Address.Text,
                    TinNo = TinNumber.Text,
                    OfficialName = Name.Text,
                    Season_Id = common.getCheckedItems(chkSeasons),
                    PrevRegNo = OldRegistrationNumber.Text,
                    SeedProcessUnit_Id = SeedProcessingUnit.Items.Count != 0 ? SeedProcessingUnit.SelectedItem.Value.ToNullableInt() : 0,
                    TotalArea = TotalArea.Text,
                    Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : Convert.FromBase64String(hdnFarmerPhoto.Value),
                    VATImage = VatCertificate.FileBytes.Length > 0 ? VatCertificate.FileBytes : Convert.FromBase64String(hdnVatCertificate.Value),
                    LetterImage = UnitConsentLetter.FileBytes.Length > 0 ? UnitConsentLetter.FileBytes : Convert.FromBase64String(hdnUnitConsentLetter.Value),
                    Sign = fu_Sign.FileBytes.Length > 0 ? fu_Sign.FileBytes : Convert.FromBase64String(hdnfu_Sign.Value),
                   // Seal = fu_seal.FileBytes.Length > 0 ? fu_seal.FileBytes : Convert.FromBase64String(hdnfu_seal.Value),

                };
                int recordCount = registrationService.Update(registrationForm);
                if (recordCount > 0)
                {
                    saveName(Convert.ToInt32(rf.ProducerID));
                    var regCropOfferred = new List<RegCropOffered>();

                    for (int i = 0; i < crop.Length; i++)
                    {
                        regCropOfferred.Add(new RegCropOffered()
                        {
                            RegistrationForm_Id = rf.Id,
                            Crop_Id = crop[i].ToInt(),
                            Area = area[i],
                            Location = location[i]
                        });
                    }

                    recordCount += regCropOfferredService.Update(regCropOfferred, registrationForm.Id).Count;

                    if (recordCount > 0)
                    {
                        RegistrationFormTransaction transaction = new RegistrationFormTransaction();
                        transaction = registrationFormTransactionService.GetMaxTransactionDetails(registrationForm.Id);
                        int RegistrationFormID = registrationService.GetByUserId(userMaster.Id).Id;
                        int ToID = 0;
                        int TransactionStatus = 0;
                        if (transaction != null)
                        {
                            ToID = transaction.FromID; // Resend to last transaction sent id
                            TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                        }
                        else if (transaction == null) // transaction null indicates the application set for renewal
                        {
                            string qy = "update RegistrationForm set RegistrationType='R' where Id = " + RegistrationFormID; // Update as Renewal
                            common.ExecuteNonQuery(qy);
                            ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                  (Convert.ToInt16(userMaster.District_Id),
                                   Convert.ToInt16(userMaster.Taluk_Id), 4).User_Id);
                            TransactionStatus = 4;
                        }
                        var rft = new RegistrationFormTransaction
                        {
                            FromID = userMaster.Id,
                            ToID = ToID,
                            RegistrationFormID = RegistrationFormID,
                            TransactionDate = System.DateTime.Now,
                            TransactionStatus = TransactionStatus,
                            IsCurrentAction = 1
                        };
                        int IsCurrentUserSet = common.setIsCurrentAction("RegistrationFormTransaction", "RegistrationFormID", RegistrationFormID);
                        int result = registrationFormTransactionService.Create(rft);
                        if (result == 1)
                        {
                            this.Redirect("Transactions/FormRegistrationApproval.aspx", false);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred while saving data.";
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }


            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                this.Redirect("Error.aspx");
            }
        }
    }
}