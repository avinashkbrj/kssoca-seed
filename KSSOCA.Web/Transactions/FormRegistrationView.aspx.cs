﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using System.Security.Permissions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using System.Web.UI.WebControls;

    public partial class FormRegistrationView : SecurePage
    {
        RegistrationFormService registrationFormService;
        UserMasterService userMasterService; Common common = new Common();

        public FormRegistrationView()
        {
            registrationFormService = new RegistrationFormService();
            userMasterService = new UserMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("FRV"); litNote.Text = common.getNote("FRV");
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later."; 
                Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent()
        {

        }

        public DataTable SelectMethod()
        {
            int userId = userMasterService.GetFromAuthCookie().Id;
           // int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
            return registrationFormService.ReadAllByUserAuthority(userId);
        }

        protected void FormRegistrationGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                if (hdnStatusCode.Value == "1")
                {
                    lblStatus.Text = "Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatus.Text = "Not approved";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }
}