﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneticPurityReportApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.GeneticPurityReportApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center"  class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Genetic Purity Report"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
              
                <br />
                <div id="divReport" runat="server" visible="true">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td colspan="4">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>

                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">Genetic Purity Report                      
                            </td>
                        </tr>

                        <tr>
                            <td>1.Name of the seed producer
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblproducerName" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>2.Name & Address of seed grower:
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblGrowerName" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">3.Crop</td>
                            <td width="25%">
                                <asp:Label ID="lblCrop" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td width="25%">4.Variety</td>
                            <td width="25%">
                                <asp:Label ID="lblVariety" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>5.Class of Seed</td>
                            <td>
                                <asp:Label ID="lblclass" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>6.Lot No.</td>
                            <td>
                                <asp:Label ID="lblLotno" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>7.Quantity of Seed</td>
                            <td>
                                <asp:Label ID="lblquantity" runat="server" Font-Bold="true"></asp:Label></td>
                            <td>8.Lab Test No</td>
                            <td>
                                <asp:Label ID="lblTestNo" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>9.Form-I Registration No:
                            </td>
                            <td>
                                <asp:Label ID="lblForm1NO" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                             <td>10.Number of label required:
                            </td>
                            <td>
                                <asp:Label ID="lblNumberOfLabelReq" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>11.Genetic Purity
                            </td>
                            <td>
                                <asp:Label ID="lblGenetic" runat="server" Font-Bold="true"></asp:Label>
                            </td>

                            <td>12.Result
                            </td>
                            <td>
                                <asp:Label ID="lblResult" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>15.Remarks</td>
                            <td>
                                <asp:Label ID="lblRemarks" runat="server" Font-Bold="true"></asp:Label>
                            </td>

                            <td>16.Prepared By</td>
                            <td>
                                <asp:Label ID="lblPreparedBy" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>17.Verified By</td>
                            <td>
                                <asp:Label ID="lblCheckedBy" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>18.Verified Date

                            </td>
                            <td>
                                <asp:Label ID="lblCheckedDate" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <asp:Panel ID="TagDatails" runat="server" Visible="false"> 
                         <tr>
                            <td valign="top">Requisition of tags</td>
                            <td colspan="3">
                                <asp:Label Font-Bold="true" runat="server" ID="lblRequisitionOfTags" />
                            </td>
                        </tr>
                            <tr>
                            <td valign="top">Allotments of tags</td>
                            <td colspan="3">
                               <asp:Label Font-Bold="true" runat="server" ID="lblAllotmentsOfTags" />
                            </td>
                        </tr>
 
                        
                    </asp:Panel>  
                      <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                        <tr>
                            <td valign="top">Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="100%" Height="100px"></asp:TextBox>
                                <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredFieldValidatortxtRemarks" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ErrorMessage="Give Remarks"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                         <asp:Panel ID="pnltags" runat="server" Visible="false">
                                <tr>
                                <td valign="top">Requisition of tags</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtRequisitionOfTags" runat="server" CssClass="form-control" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredtxtRequisitionOfTags" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtRequisitionOfTags" ErrorMessage="Give Requisition of tags"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                                <tr>
                                <td valign="top">Allotments of tags</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtAllotmentsOfTags" runat="server" CssClass="form-control" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredtxtAllotmentsOfTags" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtAllotmentsOfTags" ErrorMessage="Give Allotments of tags"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                       </asp:Panel>
                        <tr>
                            <td colspan="4" align="center">
                               
                                <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1" CausesValidation="true"
                                    ID="Save" Text="Approve GOT Test" OnClick="Save_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr class="SideHeading">
                        <td colspan="4">5.Application Status &nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" PostBackUrl="~/Transactions/FormRegistrationCreate.aspx" />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Note: Sl.No. 1 is the current status of the application.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Date" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    </table>
                </div>
                </div>
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
