﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class GeneticPurityReportApproval : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        GeneticPurityReportService geneticPurityReportService;
        DecodingService decodingService;
        TestMasterService testMasterService;
        GeneticPurityReportTransactionService geneticPurityReportTransactionService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        public GeneticPurityReportApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            geneticPurityReportService = new GeneticPurityReportService();
            decodingService = new DecodingService();
            testMasterService = new TestMasterService();
            geneticPurityReportTransactionService = new GeneticPurityReportTransactionService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            lblHeading.Text = common.getHeading("GPTA"); litNote.Text = common.getNote("GPTA");// Short name of the page
            if (Request.QueryString["Id"] != null)
            {
                int SSC_ID = Convert.ToInt16(Request.QueryString["Id"]);
                InitComponent(SSC_ID);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {

                var user = userMasterService.GetFromAuthCookie();
                int SId = Convert.ToInt16(Request.QueryString["Id"]);
                Form1Details form1 = formOneService.ReadById(Convert.ToInt32(lblForm1NO.Text));
                var userDetails = userMasterService.GetById(form1.Producer_Id);
                var reportId = geneticPurityReportService.GetByLabTestNo(Convert.ToInt32(lblTestNo.Text)).Id;
                var decodingDetails = decodingService.GetById(Convert.ToInt32(lblTestNo.Text));
                var sscDetails = seedSampleCouponService.GetById(decodingDetails.SSC_Id);
                int tranactionId = common.GenerateID("GeneticPurityReportTransaction");
                int ToID = 0;
                int status = 1;
                if (user.Role_Id == 4)
                {
                    ToID = sscDetails.SampledBy;
                }
                if (user.Role_Id == 6)
                {
                    ToID = userDetails.Id;
                    objsqlconn.Open();
                    string updateGeneticPurityReport = "Update GeneticPurityReport set status='" + status + "',RequisitionOfTags='" + txtRequisitionOfTags.Text + "',AllotmentsOfTags='" + txtAllotmentsOfTags.Text + "'   where LabTestNo= " + lblTestNo.Text;
                    SqlCommand cmdUpdateGeneticPurityReport = new SqlCommand(updateGeneticPurityReport, objsqlconn);
                    cmdUpdateGeneticPurityReport.ExecuteNonQuery();
                }
                // int ToID = userDetails.Id;


                var gpt = new GeneticPurityReportTransaction
                {
                    Id = tranactionId,
                    FromID = user.Id,
                    ToID = ToID,
                    ReportID = reportId,
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = 5,
                    Remarks = txtRemarks.Text,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("GeneticPurityReportTransaction", "ReportID", reportId);
                if (IsCurrentUserSet > 0)
                {
                    var result = geneticPurityReportTransactionService.Create(gpt);
                    if (result == 1)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "GOT result details saved successfully.";
                        this.Redirect("Transactions/GeneticPurityReportApproval.aspx?Id=" + SId);
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(int SSC_ID)
        {
            var userMaster = userMasterService.GetFromAuthCookie();
            Decoding dec = decodingService.GetBySSCID(SSC_ID);
            GeneticPurityReport sar = geneticPurityReportService.GetByLabTestNo(dec.Id);
            if (sar != null)
            {
                lblCheckedBy.Text = userMasterService.GetById(Convert.ToInt16(sar.TestedBy)).Name;
                lblPreparedBy.Text = sar.PreparedBy;
                lblCheckedDate.Text = sar.TestedDate.Value.ToString("dd/MM/yyyy");
                lblRemarks.Text = sar.Remarks;
                lblTestNo.Text = sar.LabTestNo.ToString();
                lblGenetic.Text = sar.GeneticPurity.ToString();
                lblResult.Text = sar.Result;
                lblTestNo.Text = sar.LabTestNo.ToString();
                lblNumberOfLabelReq.Text = sar.NoOfLabels.ToString();
                if (sar.RequisitionOfTags != 0 && sar.AllotmentsOfTags !=0)
                {
                    TagDatails.Visible = true;
                    lblRequisitionOfTags.Text = sar.RequisitionOfTags.ToString();
                    lblAllotmentsOfTags.Text = sar.AllotmentsOfTags.ToString();
                }
            }
            SeedSampleCoupon ssc = seedSampleCouponService.GetById(dec.SSC_Id);
            Form1Details f1 = formOneService.ReadById(ssc.Form1Id);
            if (f1 != null)
            {
                FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                FieldInspection fi = fieldInspectionService.GetFinalInspection(ssc.Form1Id);
                if (fi != null)
                {
                    lblForm1NO.Text = ssc.Form1Id.ToString();
                    lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
                    lblGrowerName.Text = fardet.Name + ",District : " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                        ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                        ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                        ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                        ",Mobile No : " + fardet.MobileNo;
                    lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
                    lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                    lblclass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                    lblLotno.Text = ssc.LotNo;
                    lblquantity.Text = ssc.Quantity.ToString(); 
                    gvstatus.DataSource = geneticPurityReportTransactionService.GetAllTransactionsBy(sar.Id);
                    gvstatus.DataBind();
                    GeneticPurityReportTransaction transaction = new GeneticPurityReportTransaction();
                    transaction = geneticPurityReportTransactionService.GetMaxTransactionDetails(sar.Id);
                    if (transaction != null)
                    {
                        int ActionRole_Id = transaction.ToID;
                        if (userMaster.Role_Id == 5)
                        {
                            pnlRemarks.Visible = false;
                            if (transaction.TransactionStatus == 0)
                            {
                                //  btnEditApplication.Visible = true;
                                //  btnEditApplication.PostBackUrl = "~/Transactions/SourceVerificationCreate.aspx?Id=" + sv.Id;
                            }
                            if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                            {
                                this.Redirect("Unauthorized.aspx");
                            }
                        }
                        else
                        {
                            if (userMaster.Id == ActionRole_Id)
                            {
                                if (sar.PaymentStatus == 1)
                                {
                                    pnlRemarks.Visible = true;
                                    if (userMaster.Role_Id == 6)
                                    {
                                        pnltags.Visible = true;
                                    }
                                    else
                                    {
                                        pnltags.Visible = false;
                                    }
                                }
                                else
                                {
                                    lblMessage.Text = "Payments not updated";
                                }
                            }
                            else
                            {
                                pnlRemarks.Visible = false;
                            }
                        }
                    }
                    
                }
                else
                {
                    lblMessage.Text = "Field inspection not finalised for this form no.";
                    divReport.Visible = false;
                }
            }
            else
            {
                lblMessage.Text = "Enter valid form no.";
                divReport.Visible = false;
            }

        }
    }
}