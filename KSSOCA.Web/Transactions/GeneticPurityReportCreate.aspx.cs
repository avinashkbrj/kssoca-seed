﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class GeneticPurityReportCreate : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService; 
        SeasonMasterService seasonMasterService; 
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        GeneticPurityReportService geneticPurityReportService;
        DecodingService decodingService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        GeneticPurityReportTransactionService geneticPurityReportTransactionService;
        public GeneticPurityReportCreate()
        {
            userMasterService = new UserMasterService();
            seasonMasterService = new SeasonMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            geneticPurityReportService = new GeneticPurityReportService();
            decodingService = new DecodingService();
            userwiseDistrictRightsService= new UserwiseDistrictRightsService();
            geneticPurityReportTransactionService = new GeneticPurityReportTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        try
        {
            lblMessage.Text = "";
                lblHeading.Text = common.getHeading("GPT");
                litNote.Text = common.getNote("GPT");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int LabTestNo = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["LabTestNo"] = LabTestNo;
                    txttestno.Text = LabTestNo.ToString();
                    lblCheckedBy.Text = userMasterService.GetFromAuthCookie().Name;
                    lblCheckedDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    
                   

                }
                else
                {
                    // this.Redirect("Dashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int gotRequired = 0;
                    int LabTestNumber = Convert.ToInt16(Request.QueryString["Id"]);
                    var decodingDetails = decodingService.GetById(LabTestNumber);
                    var seedSampleCouponDetails = seedSampleCouponService.GetById(decodingDetails.SSC_Id);
                    Form1Details form1 = formOneService.ReadById(seedSampleCouponDetails.Form1Id);
                    var userDetails = userMasterService.GetById(form1.Producer_Id);
                    var currentUser = userMasterService.GetFromAuthCookie();
                    if (txttestno.Text != "")
                    {
                        var sar = new GeneticPurityReport
                        {
                            LabTestNo = txttestno.Text.ToInt(),
                            PreparedBy = txtPreparedBy.Text,
                            Remarks = txtRemarks.Text,
                            TestedBy = userMasterService.GetFromAuthCookie().Id,
                            TestedDate = System.DateTime.Now,
                            GeneticPurity = txtGenetic.Text.ToDecimal(),
                            Result = rbResult.SelectedValue,
                            status = 0,
                            PaymentStatus = 0,
                            NoOfLabels=Convert.ToInt32(txtNumberOfLabelsRequired.Text)
                        };
                        var result = geneticPurityReportService.Create(sar);
                        var reportId = geneticPurityReportService.GetByLabTestNo(txttestno.Text.ToInt());
                        int tranactionId = common.GenerateID("GeneticPurityReportTransaction");
                        int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                            (Convert.ToInt16(userDetails.District_Id),
                                             Convert.ToInt16(userDetails.Taluk_Id), 4).User_Id);
                        int TransactionStatus = 4;
                        if (result > 0)
                        {
                            if (rbResult.SelectedValue.ToInt() == 1)
                            {
                                var gpt = new GeneticPurityReportTransaction
                                {
                                    Id = tranactionId,
                                    FromID = currentUser.Id,
                                    ToID = ToID,
                                    ReportID = reportId.Id,
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = TransactionStatus,
                                    Remarks = "",
                                    IsCurrentAction = 1
                                };
                                var resultTransaction = geneticPurityReportTransactionService.Create(gpt);
                                objsqlconn.Open();
                                string queryGOTRequirment = "Update TestingPayment set GotRequiredOrNot='" + gotRequired + "'   where Form1Id=@formId ";

                                SqlCommand cmdqueryGOTRequirment = new SqlCommand(queryGOTRequirment, objsqlconn);
                                cmdqueryGOTRequirment.Parameters.AddWithValue("@formId", seedSampleCouponDetails.Form1Id);

                                int resultGOTRequirment = cmdqueryGOTRequirment.ExecuteNonQuery();
                                objsqlconn.Close();
                                if (resultTransaction > 0 && resultGOTRequirment > 0)
                                {

                                    Messaging.SendSMS("GOT test passed for Form1 number -" + seedSampleCouponDetails.Form1Id + "", userDetails.MobileNo, "1");

                                    ConfirmationMessage();
                                }
                            }
                            else
                            {
                                Messaging.SendSMS("GOT test failed  " + rbResult.SelectedValue + " for Form1 number -" + seedSampleCouponDetails.Form1Id + "", userDetails.MobileNo, "1");
                                ConfirmationMessage();
                            }
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Please save all the details.";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later."; 
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void ConfirmationMessage()
        {
            lblMessage.ForeColor = System.Drawing.Color.Green;
            lblMessage.Text = "GOT result details saved successfully.";
            this.Redirect("Transactions/GeneticPurityReportView.aspx");
        }
    }
}