﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Configuration;
    using System.Data.SqlClient;

    public partial class GeneticPurityReportView : SecurePage
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        Common common = new Common();
        DecodingService decodingService;
        TestMasterService testMasterService;
        SeedSampleCouponService seedSampleCouponService;
        public GeneticPurityReportView()
        {
            userMasterService = new UserMasterService();
            decodingService = new DecodingService();
            testMasterService = new TestMasterService();
            seedSampleCouponService = new SeedSampleCouponService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitComponent();
            }
        }
        private void InitComponent()
        {
            var user = userMasterService.GetFromAuthCookie();

            if (user.Role_Id == 11 && user.Id == 2075) // Authorised only to bangalore Decoding officer.
            {
                lblHeading.Text = "Sample Coding/Decoding";
                divDecoding.Visible = true;
                ddlGOT.BindListItem(userMasterService.GetUserList(9));  // 9 gives gots
            }
            else if (user.Role_Id == 9 || user.Role_Id == 4 || user.Role_Id == 6)
            {
                lblHeading.Text = "Samples for GOT Test";
                divDecoding.Visible = false;
            }
            else { lblHeading.Text = "Genetic Purity Report"; }
            DecodingGridView.DataSource = SelectMethod();
            DecodingGridView.DataBind();
        }
        protected void btngenerateLabNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (seedSampleCouponService.IsValidCoupan(txtsscNo.Text.ToInt(), "GOT"))
                {
                    if (!decodingService.IsSSCIdExists(txtsscNo.Text.ToInt()))
                    {
                        var sscid = txtsscNo.Text.ToInt();
                        SeedSampleCoupon sscDetailsGot = seedSampleCouponService.GetById(sscid);
                        if (sscDetailsGot.IsRecieved == 1)
                        {
                            var user = userMasterService.GetFromAuthCookie();
                            var decoding = new Decoding
                            {
                                DecodedBy = user.Id,
                                DecodedDate = System.DateTime.Now,
                                SSC_Id = sscid,
                                Sent_To = Convert.ToInt16(ddlGOT.SelectedValue.Trim()),
                            };
                            int recordCount = decodingService.Create(decoding);
                            int tranactionSTL = common.GenerateID("Decoding") - 1;
                            objsqlconn.Open();
                            string updateTestingPayment = "Update TestingPayment set GOTTestID='" + tranactionSTL + "' where Form1Id= " + sscDetailsGot.Form1Id;
                            SqlCommand cmdTestingPayment = new SqlCommand(updateTestingPayment, objsqlconn);
                            cmdTestingPayment.ExecuteNonQuery();
                            objsqlconn.Close();
                            if (recordCount > 0)
                            {
                                lblMessage.Text = "GOT Lab  No generated successfully.";
                                txtsscNo.Text = "";
                                DecodingGridView.DataSource = SelectMethod();
                                DecodingGridView.DataBind();
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Please get recieval update from producer";
                        }
                    }
                    else { lblMessage.Text = "This Coupon Number is already encoded.Enter valid Coupon Number."; }
                }
                else { lblMessage.Text = "This Coupon Number does not exist or not a GOT Coupon.Enter valid Coupon Number."; }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        public DataTable SelectMethod()
        {
            var data = (DataTable)null; 
            var user = userMasterService.GetFromAuthCookie(); 
            data = decodingService.ReadByUser(Convert.ToInt16(user.Role_Id), user.Id, "GOT"); 
            
            if (data != null)
            {
                if (user.Role_Id == 4)
                {
                     return data;
                }
                else
                {
                    return data;
                }
              
            }
            else
                lblMessage.Text = "Records not found.";
            return (DataTable)null;
        }

        protected void DecodingGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var user = userMasterService.GetFromAuthCookie();
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                HiddenField hdnTestsRequired = (e.Row.FindControl("hdnTestsRequired") as HiddenField);
                Label lblTestsRequired = (e.Row.FindControl("lblTestsRequired") as Label);
                LinkButton lnkCreate = (e.Row.FindControl("lnkCreate") as LinkButton);
                LinkButton lnkView = (e.Row.FindControl("lnkView") as LinkButton);
                if (user.Role_Id == 9)
                {
                    DecodingGridView.Columns[1].Visible = false;
                    if (hdnStatusCode.Value.Trim() == "1")
                    {
                        lblStatus.ForeColor = System.Drawing.Color.Green; lblStatus.Text = "Pass";
                        lnkView.Visible = true;
                        lnkCreate.Visible = false;
                    }
                    else if (hdnStatusCode.Value.Trim() == "0")
                    {
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        lblStatus.Text = "Fail";
                        lnkView.Visible = true;
                        lnkCreate.Visible = false;
                    }
                    else
                    {
                        lblStatus.ForeColor = System.Drawing.Color.BurlyWood;
                        lblStatus.Text = "Pending";
                        lnkView.Visible = false;
                        lnkCreate.Visible = true;
                    }
                }
                else
                {
                    lnkCreate.Visible = false;
                    if (hdnStatusCode.Value.Trim() == "1")
                    {
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        lblStatus.Text = "Pass";
                        lnkView.Visible = true;
                    }
                    else if (hdnStatusCode.Value.Trim() == "0")
                    {
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        lblStatus.Text = "Fail";
                        lnkView.Visible = true;
                    }
                    else
                    {
                        lblStatus.ForeColor = System.Drawing.Color.BurlyWood;
                        lblStatus.Text = "Pending";
                        lnkView.Visible = false;
                    }
                }

                lblTestsRequired.Text = testMasterService.TestsRequired(hdnTestsRequired.Value);
            }
        }
        protected void GeneticPutiryReportGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GeneticPutiryReportGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DecodingGridView.PageIndex = e.NewPageIndex;
            DecodingGridView.DataSource = SelectMethod();
            DecodingGridView.DataBind();
        }
    }
}