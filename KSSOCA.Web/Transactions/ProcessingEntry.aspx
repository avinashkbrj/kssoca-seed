﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProcessingEntry.aspx.cs" Inherits="KSSOCA.Web.Transactions.ProcessingEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <!-- this meta viewport is required for BOLT //-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <!-- BOLT Sandbox/test //-->
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <!-- BOLT Production/Live //-->
    <!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Seed Processing Details"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnf1Id" runat="server" />
                    <asp:HiddenField ID="hdnbulkid" runat="server" />
                      <input type="hidden" id="surl" name="surl" value="<%= Session["surl"]%>" />
                    <input type="hidden" id="txnid" name="txnid" value="<%= Session["txnid"]%>" />
                    <input type="hidden" id="hash" name="hash" value="<%= Convert.ToString(Session["hash"])%>" />
                    <input type="hidden" id="amount" name="amount" value="<%= Session["amount"]%>" />
                    <input type="hidden" id="fname" name="fname" value="<%= Session["fname"]%>" />
                    <input type="hidden" id="mobilenumber" name="mobilenumber" value="<%= Session["mobilenumber"]%>" />
                </div>
               
                <br />
                <table class="table-condensed" width="50%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Bulk Processing Details
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Get Processing Entry By :&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlfilter" Width="200px" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                            <asp:ListItem Text="Form-I Registration No." Value="f1"></asp:ListItem>
                            <asp:ListItem Text="Date wise" Value="dt"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="pnlsearchbyf1" runat="server" Visible="true">
                                Form-I Registration No. :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtForm1No" onkeypress="return CheckNumber(event);" placeholder="Form I No" onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ID="txtForm1NoRequiredValidator" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtForm1No" ErrorMessage="Form I No Required" ValidationGroup="v1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                    <asp:Button ID="btngetformdetails" runat="server" Text="Get Form1 Details" CssClass="btn-primary" OnClick="btngetformdetails_Click" ValidationGroup="v1" />
                            </asp:Panel>
                            <asp:Panel ID="pnlsearchbyDate" runat="server" Visible="false">
                                From Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtFromDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtFromDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtFromDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                           To Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtToDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtToDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtToDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                <asp:Button ID="btngetforms" runat="server" Text="Get Processing Entries" CssClass="btn-primary" ValidationGroup="v3" OnClick="btngetforms_Click" />

                            </asp:Panel>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlform1details" runat="server" Visible="false">
                        <tr>
                            <td colspan="4">
                                <asp:Literal ID="litForm1Details" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td>Moisture (in %)<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtMoisture" onkeypress="return CheckDecimal(event);" placeholder="Moisture" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtMoisture" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtMoisture" ErrorMessage="Moisture Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                            <td>Date of Cleaning<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtDateofCleaning" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtDateofCleaning" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtDateofCleaning" ErrorMessage="Bulk Stock Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>

                        </tr>
                        <tr>
                            <td>Screen Rejection(in Kgs)<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtScreenRejection" onkeypress="return CheckNumber(event);" placeholder="Screen Rejection" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtScreenRejection" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtScreenRejection" ErrorMessage="Screen Rejection Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                            <td>Blown Off(in Kgs)<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtBlownOff" onkeypress="return CheckNumber(event);" placeholder="Blown Off" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtBlownOff" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtBlownOff" ErrorMessage="Blown Off Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>Total Rejection(in Kgs)<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtTotalRejection" onkeypress="return CheckNumber(event);" placeholder="Total Rejection" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredTotalRejection" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtTotalRejection" ErrorMessage="Total Rejection Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                            <td>Quantity of Cleaned Seed (in Quintal)<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtCleanedSeed" onkeypress="return CheckNumber(event);" placeholder="Quantity of Cleaned Seed" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtCleanedSeed" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtCleanedSeed" ErrorMessage="Quantity of Cleaned Seed Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>No of Bags<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtGunnyBags" onkeypress="return CheckNumber(event);" placeholder="No of Gunny Bags" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtGunnyBags" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtGunnyBags" ErrorMessage="No of Gunny Bags Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                            <td>Lot Number<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtLotNumber" placeholder="Lot Number" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtLotNumber" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtLotNumber" ErrorMessage="Lot Number Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                    ID="Save" Text="Save Processing Details" ValidationGroup="v2" OnClick="Save_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                </table>
                <br />
            </div>
            <div>
                <asp:GridView ID="gvEntries" runat="server" class="table-condensed" Font-Size="8pt" AllowSorting="true" AutoGenerateColumns="false" Width="100%" OnRowDataBound="gvEntries_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No." SortExpression="Form1ID" />
                        <asp:BoundField DataField="GrowerName" HeaderText="Grower Name" />
                        <asp:BoundField DataField="Crop" HeaderText="Crop" />
                        <asp:BoundField DataField="Variety" HeaderText="Variety" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <asp:BoundField DataField="AcceptedStock" HeaderText="Accepted Stock(in Kgs.)" />
                        <asp:BoundField DataField="Moisture" HeaderText="Moisture(in %)" />
                        <asp:BoundField DataField="DateOfCleaning" HeaderText="Date Of Cleaning(in Kgs)" />
                        <asp:BoundField DataField="ScreenRejection" HeaderText="Screen Rejection(in Kgs)" />
                        <asp:BoundField DataField="BlownOff" HeaderText="Blown Off(in Kg.)" />
                        <asp:BoundField DataField="TotalRejection" HeaderText="Total Rejection(in Kg.)" />
                        <asp:BoundField DataField="CleanedSeed" HeaderText="Quantity of Cleaned Seed(in Kg.)" />
                        <asp:BoundField DataField="Bags" HeaderText="No of Gunny Bags" />
                        <asp:BoundField DataField="LotNumber" HeaderText="LotNumber" />
                        <asp:BoundField DataField="EntryDate" HeaderText="Processing Entry Date" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Service Charge Details">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnPaymentStatus" runat="server" Value='<%# Eval("PaymentStatus") %>' />
                                <asp:CheckBox ID="chkPayment" runat="server" Enabled="false" OnCheckedChanged="chkPayment_CheckedChanged" AutoPostBack="true" />
                                <asp:Label runat="server" ID="lblAmountPaid" Font-Bold="true" Text='<%# Eval("AmountPaid") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <%-- <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblReMarks" runat="server" Font-Bold="true" Text='<%# Eval("Remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkdelete" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lnkUpdate" CommandArgument='<%#Eval("ID") %>'>Enter Stock Acceptance details</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>--%>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
            </div><br />
            <table>
                <tr>
                    <td>
                        <asp:GridView ID="gvPayment" runat="server" class="table-condensed" AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl.No.">
                                    <ItemTemplate>
                                        <%#Container.DataItemIndex+1 %> . 
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <Columns>
                                <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No." />
                                <asp:BoundField DataField="STLCharge" HeaderText="STL Charge(in Rs.)" />
                                <asp:BoundField DataField="GPTCharge" HeaderText="GPT Charge(in Rs.)" />
                                <asp:BoundField DataField="ProcessingCharge" HeaderText="Processing Charge(in Rs.)" />
                                <asp:BoundField DataField="ClothBagCharge" HeaderText="Cloth Bag Charge(in Rs.)" />
                                <asp:BoundField DataField="PolytheneCharge" HeaderText="Polythene Cover Charge(in Rs.)" />
                                <asp:BoundField DataField="CourierCharge" HeaderText="Courier Charge(in Rs.)" />
                                <asp:BoundField DataField="Total" HeaderText="Total(in Rs.)" />
                            </Columns>
                            <EmptyDataTemplate>----------No forms selected-----------</EmptyDataTemplate>
                            <FooterStyle Font-Bold="true" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table-condensed" border="1">
                            <asp:Panel ID="pnlPayment" runat="server" Visible="false">
                                Total Amount to be paid (in Rs.) :
                                            <asp:Label runat="server" ID="lblTotal" Font-Bold="true" Text="0"></asp:Label>
                                <tr>
                                    <td>Payment Mode&nbsp;</td>
                                    <td colspan="4">
                                        <asp:RadioButtonList ID="rbPaymentMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                            OnSelectedIndexChanged="rbPaymentMode_SelectedIndexChanged">
                                            <asp:ListItem Text="&nbsp;DD/Cheque&nbsp;&nbsp;" Value="C" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="&nbsp;e-Payment" Value="O"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlCheck" runat="server">
                                    <tr>
                                        <td>Cheque Number <span style="color: red">*</span></td>
                                        <td>
                                            <asp:TextBox runat="server" CssClass="form-control" ID="txtCheckNo" placeholder="Cheque Number" TextMode="Number"
                                                onkeypress="return CheckNumber(event);" />
                                            <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredCheckNo" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="txtCheckNo" ErrorMessage="Cheque Number required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td>Cheque <span style="color: red">*</span></td>
                                        <td>
                                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_Check" accept="image/*" />
                                            <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="Requiredfu_Check" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="fu_Check" ErrorMessage="Cheque   required"></asp:RequiredFieldValidator>
                                            <cst:FileTypeValidator runat="server" ID="FileTypeCheck" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="fu_Check" ErrorMessage="Allowed file types are {0}.">
                                            </cst:FileTypeValidator>
                                              <cst:FileSizeValidator runat="server" id="fu_CheckSizeValidator" ForeColor="Red" Display="Dynamic"
                                                  ControlToValidate="fu_Check" ErrorMessage="File size must not exceed {0} KB">
                                               </cst:FileSizeValidator>
                                        </td>
                                        <td>
                                            <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                                                ID="btnCheck" Text="Save Payment Details" OnClick="btnCheck_Click" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlOnline" runat="server">
                                    <tr>
                                        <td align="center" colspan="5">
                                             <input type="submit" CssClass="btn-primary"   value="Pay Online" onclick="launchBOLT(); return false;" /> 
                                          </td>
                                    </tr>
                                </asp:Panel>
                            </asp:Panel>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCheck" />
            <%--<asp:PostBackTrigger ControlID="gvEntries" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {



        });

    //--
</script>	

<script type="text/javascript"><!--
    function launchBOLT() {
        //  var encryptedk = CryptoJS.AES.encrypt("juvhC4rb", "Secret Passphrase");
        var getHash = createHash();
        var actualHashValue = getHash['responseJSON']['success'];

        var encryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var encryptedKey =  '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var decryptedSalt = CryptoJS.AES.decrypt(encryptedSalt, "Secret Passphrase");
        var decryptedKey = CryptoJS.AES.decrypt(encryptedKey, "Secret Passphrase");

        var transId = $('#txnid').val();


        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();

        bolt.launch({
            key: decryptedKey.toString(CryptoJS.enc.Utf8),
            txnid: transId,
            hash: actualHashValue.toString(),
            amount: amt,
            firstname: fname,
            email: "avi.ksrj@gmail.com",
            phone: mobilenumber,
            productinfo: "KSSOCA Payment",
            udf5: "BOLT_KIT_ASP.NET",
            surl: $('#surl').val(),
            furl: $('#surl').val()
        }, {
                responseHandler: function (BOLT) {
                    console.log(BOLT.response.txnStatus);
                    if (BOLT.response.txnStatus != 'CANCEL') {
                        //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                        var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                            '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                            '<input type=\"hidden\" name=\"salt\" value=\"' + decryptedSalt.toString(CryptoJS.enc.Utf8) + '\" />' +
                            '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                            '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                            '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                            '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                            '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                            '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                            '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                            '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                            '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                            '</form>';
                        var form = jQuery(fr);
                        jQuery('body').append(form);
                        form.submit();
                    }
                },
                catchException: function (BOLT) {
                    alert(BOLT.message);
                }
            });
    }

    function createHash() {
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        var transId = $('#txnid').val();
        var hashEncryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var hashEncryptedKey = '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var hashDecryptedSalt = CryptoJS.AES.decrypt(hashEncryptedSalt, "Secret Passphrase");
        var hashDecryptedKey = CryptoJS.AES.decrypt(hashEncryptedKey, "Secret Passphrase");
        var temp = "";
        return $.ajax({
            url: '/Transactions/Hash.aspx',
            type: 'post',
            async: false,
            data: JSON.stringify({
                key: hashDecryptedKey.toString(CryptoJS.enc.Utf8),
                salt: hashDecryptedSalt.toString(CryptoJS.enc.Utf8),
                txnid: transId.toString(),
                amount: amt,
                pinfo: "KSSOCA Payment",
                fname: fname,
                email: "avi.ksrj@gmail.com",
                mobile: mobilenumber,
                udf5: "BOLT_KIT_ASP.NET"
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (json) {

                if (json['error']) {
                    $('#alertinfo').html('<i class="fa fa-info-circle"></i>' + json['error']);
                }
                else if (json['success']) {
                    $('#hash').val(json['success']);
                    temp = json['success'];
                }
            }
        });
        return temp;
    }


</script>	
</asp:Content>
