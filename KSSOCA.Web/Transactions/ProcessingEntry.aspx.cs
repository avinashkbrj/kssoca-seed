﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Helper;
    using System.Data;
    using System.Text;
    using System.Configuration;
    using System.Collections.Specialized;

    public partial class ProcessingEntry : System.Web.UI.Page
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        SPURegistrationService SPUMasterService;
        SourceVerificationService sourceverficationservice;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        Common common = new Common();
        FarmerDetailsService farmerDetailsService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        FieldInspectionService fiService;
        BulkEntryService bulkEntryService;
        BulkProcessingEntryService bulkProcessingEntryService;
        ProcessingPaymentDetailsService processingPaymentDetailsService;
        Form1PaymentDetailService form1PaymentDetailService;
        BulkEntryTransactionService bulkEntryTransactionService;
        public ProcessingEntry()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            SPUMasterService = new SPURegistrationService();
            sourceverficationservice = new SourceVerificationService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            farmerDetailsService = new FarmerDetailsService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            fiService = new FieldInspectionService();
            bulkEntryService = new BulkEntryService();
            bulkProcessingEntryService = new BulkProcessingEntryService();
            processingPaymentDetailsService = new ProcessingPaymentDetailsService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            bulkEntryTransactionService = new BulkEntryTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            if (!IsPostBack)
            {
                string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                Session.Add("surl", surl);
                Random random = new Random();
                var user = userMasterService.GetFromAuthCookie();
                int txnid = random.Next(1001, 9999);
                Session.Add("txnid", txnid);
                Session.Add("mobilenumber", user.MobileNo);
                Session.Add("fname", user.Name);
                NameValueCollection nvc = Request.Form;
                if (nvc.Count > 0)
                {

                    EPayment_Response(nvc, sender, e);
                }
                //if (user.Role_Id == 5)
                //{
                //txtFromDate.Text = System.DateTime.Now.Date.ToString() + " 23:59:59";
                //txtToDate.Text = System.DateTime.Now.Date.ToString();
                BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
                //}
            }
        }
        private void BindGrid(int form1id, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            var user = userMasterService.GetFromAuthCookie();
            var data = (DataTable)null;
            if (form1id > 0)
            {
                data = bulkProcessingEntryService.GetBulkProcessingDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, null, null);
            }
            else
            {
                data = bulkProcessingEntryService.GetBulkProcessingDetails
                       (user.Id, Convert.ToInt16(user.Role_Id), form1id,
                       Utility.StartOfDay(Convert.ToDateTime(FromDate)),
                       Utility.EndOfDay(Convert.ToDateTime(ToDate)));
            }
            if (data != null)
            {
                gvEntries.DataSource = data;
                gvEntries.DataBind();
            }
            else
            {
                gvEntries.DataSource = null;
                gvEntries.DataBind();
            }
        }
        protected void btngetformdetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtForm1No.Text.Trim() != "")
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var data = bulkProcessingEntryService.GetBulkProcessingDetails(user.Id, Convert.ToInt16(user.Role_Id), txtForm1No.Text.Trim().ToInt(), null, null);
                    if (data != null)
                    {
                        pnlform1details.Visible = false;
                        gvEntries.DataSource = data;
                        gvEntries.DataBind();
                    }
                    else
                    {
                        var blk = bulkEntryService.GetbyForm1ID(txtForm1No.Text.ToInt());
                        if (blk != null)//&& blk.SentTo == user.Id)
                        {
                            if (user.Role_Id == 6 && user.Id == blk.AcceptedBy)
                            {
                                var Processingblk = bulkProcessingEntryService.GetbyForm1ID(txtForm1No.Text.ToInt());
                                if (Processingblk == null)
                                {
                                    var f1 = formOneService.ReadById(txtForm1No.Text.ToInt());
                                    var far = farmerDetailsService.GetById(f1.FarmerID);
                                    var sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                                    var cm = cropMasterService.GetById(sv.Crop_Id);
                                    var cmv = cropVarietyMasterService.GetById(sv.Variety_Id);
                                    var cls = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced));
                                    var fi = fiService.GetFinalInspection(f1.Id);
                                    if (fi != null)
                                    {
                                        if (fi.StatusCode == 1)
                                        {
                                            litForm1Details.Text = "Grower/Farmer Name:<span style=\"color: Blue;font-weight:bold\">" + far.Name +
                                                             "</span>&nbsp;&nbsp;Grower Registration No:<span style=\"color: Blue;font-weight:bold\">" + f1.Id +
                                                             "</span>  Crop :<span style=\"color: Blue;font-weight:bold\">" + cm.Name +
                                                             "</span>&nbsp;&nbsp;Variety:<span style=\"color: Blue;font-weight:bold\">" + cmv.Name +
                                                             "</span>&nbsp;&nbsp;Class Of the Seed:<span style=\"color: Blue;font-weight:bold\">" + cls.Name +
                                                             "</span>  Accepted Seed Quantity(in Kgs.):<span style=\"color: Blue;font-weight:bold\">" + blk.AcceptedStock + "</span>";

                                            pnlform1details.Visible = true;
                                            hdnf1Id.Value = f1.Id.ToString();
                                        }
                                        else
                                        {
                                            pnlform1details.Visible = false;
                                            lblMessage.Text = "This Form-I Registration No has been rejected in final field inspection.";
                                        }
                                    }
                                    else
                                    {
                                        pnlform1details.Visible = false;
                                        lblMessage.Text = "Field inspection is not finalised for this Form-I Registration No.";
                                    }
                                }
                                else
                                {
                                    pnlform1details.Visible = false; lblMessage.Text = "Enter valid Form-I Registration No. i.e Processing entry has already done or this Form-I Registration No does not belongs to you.";

                                }
                            }
                            else
                            {
                                pnlform1details.Visible = false;
                                lblMessage.Text = "Enter valid Form-I Registration No. i.e Bulk Entry has not yet completed for this Form-I Registration No or this form 1 does not belongs to you.";
                            }
                        }
                        else
                        {
                             lblMessage.Text = "Processing entry has not yet done for this Form-I Registration No.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var user = userMasterService.GetFromAuthCookie();
                var be = new BulkProcessingEntry()
                {
                    Id = bulkProcessingEntryService.GenerateID(),
                    Bags = txtGunnyBags.Text.ToInt(),
                    BlownOff = txtBlownOff.Text.ToInt(),
                    CleanedSeed = txtCleanedSeed.Text.ToInt(),
                    DateOfCleaning = txtDateofCleaning.Text.ToDateTime(),
                    EntryDate = System.DateTime.Now,
                    Form1ID = hdnf1Id.Value.ToInt(),
                    LotNumber = txtLotNumber.Text.Replace(" ", "").ToUpper(),
                    Moisture = txtMoisture.Text.ToDecimal(),
                    ProcessedBy = user.Id,
                    ScreenRejection = txtScreenRejection.Text.ToInt(),
                    TotalRejection = txtTotalRejection.Text.ToInt()
                };
                var result = bulkProcessingEntryService.Create(be);
                if (result > 0)
                {
                    pnlform1details.Visible = false;
                    var form1 = formOneService.ReadById(txtForm1No.Text.ToInt());
                    var farmer = farmerDetailsService.GetById(form1.FarmerID);
                    var sv = sourceverficationservice.ReadById(form1.SourceVarification_Id);
                    var crop = cropMasterService.GetById(sv.Crop_Id);
                    var cmvariety = cropVarietyMasterService.GetById(sv.Variety_Id);
                     
                    Messaging.SendSMS("Processing Entry of Farmer-" + farmer.Name + " Crop-" + crop.Name + " Variety-" + cmvariety.Name + ",Total Seed Rejected-"+ txtTotalRejection.Text.ToInt() + "Total Seed After Processing-"+ txtCleanedSeed.Text.ToInt() + "Kg completed by " + user.Name + "", farmer.MobileNo, "1");
                    lblMessage.Text = "Processing Entry saved successfully.";
                    BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
                }
                else { lblMessage.Text = "Something went wrong."; }
            }
        }
        protected void gvEntries_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField hdnPaymentStatus = (e.Row.FindControl("hdnPaymentStatus") as HiddenField);
                CheckBox chkPayment = (e.Row.FindControl("chkPayment") as CheckBox);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
                Label lblAmountPaid = (e.Row.FindControl("lblAmountPaid") as Label);

                if (roleId == 5)
                {
                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;

                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                        lblAmountPaid.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        chkPayment.Enabled = true;
                        chkPayment.Checked = false;

                        lblAmountPaid.Text = "Tick here to pay";
                        lblAmountPaid.ForeColor = System.Drawing.Color.Blue;
                    }

                }
                else
                {

                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;
                        lblAmountPaid.Visible = true;
                        lblAmountPaid.ForeColor = System.Drawing.Color.Green;
                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                    }
                    else
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = false;
                        lblAmountPaid.ForeColor = System.Drawing.Color.Red;
                        lblAmountPaid.Text = "Not yet paid";
                    }
                }
            }
        }
        protected void ddlfilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlfilter.SelectedValue == "f1")
            {
                pnlsearchbyf1.Visible = true;
                pnlsearchbyDate.Visible = false;
            }
            else if (ddlfilter.SelectedValue == "dt")
            {
                pnlsearchbyf1.Visible = false;
                pnlsearchbyDate.Visible = true;
            }
        }
        protected void btngetforms_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BindGrid(0, txtFromDate.Text.ToDateTime().Date, txtToDate.Text.ToDateTime().Date);
            }
        }
        private decimal getSTLCharge(decimal CleanedSeed, int LotSize, int Form1ID)
        {
            decimal STLCharge = 0;
            decimal IsDecimal = CleanedSeed % LotSize; // to get per lot rate
            int NoOfLots = Convert.ToInt32(CleanedSeed) / LotSize;
            if (IsDecimal > 0)
            {
                NoOfLots += 1;
            }
            return STLCharge = Convert.ToDecimal(((NoOfLots * 150) - form1PaymentDetailService.GetBy(Form1ID).STLCharge));// Substitute the already paid amount.
        }
        private decimal getGOTCharge(decimal CleanedSeed, int LotSize, decimal GPTCharge, int Form1ID)
        {
            decimal GOTCharge = 0;
            decimal IsDecimal = CleanedSeed % LotSize; // to get per lot rate
            int NoOfLots = Convert.ToInt32(CleanedSeed) / LotSize;
            if (IsDecimal > 0)
            {
                NoOfLots += 1;
            }
            if (GPTCharge > 0)
            {
                return GOTCharge = Convert.ToDecimal(((NoOfLots * GPTCharge) - form1PaymentDetailService.GetBy(Form1ID).GPTCharge));// Substitute the already paid amount.
            }
            else return 0;
        }
        private decimal getClothBagCharge(decimal CleanedSeed, int LotSize, decimal GPTCharge)
        {
            decimal NoOfBags = 0;
            decimal IsDecimal = CleanedSeed % LotSize; // to get per lot rate
            int NoOfLots = Convert.ToInt32(CleanedSeed) / LotSize;
            if (IsDecimal > 0)
            {
                NoOfLots += 1;
            }

            if (GPTCharge > 0)
            {
                NoOfBags = NoOfLots * 2 + NoOfLots;
            }
            else
            {
                NoOfBags = NoOfLots + NoOfLots;
            }

            return NoOfBags * common.getFees("CLTHBAG");
        }
        private decimal getCourierCharge(decimal CleanedSeed, int LotSize, decimal GPTCharge)
        {
            decimal NoOfBags = 0;
            decimal IsDecimal = CleanedSeed % LotSize; // to get per lot rate
            int NoOfLots = Convert.ToInt32(CleanedSeed) / LotSize;
            if (IsDecimal > 0)
            {
                NoOfLots += 1;
            }

            if (GPTCharge > 0)
            {
                NoOfBags = NoOfLots * 2 + NoOfLots;
            }
            else
            {
                NoOfBags = NoOfLots + NoOfLots;
            }

            return NoOfBags * common.getFees("COURR");
        }
        private decimal getProcessingCharge(decimal CleanedSeed, string CropType)
        {
            decimal IsDecimal = CleanedSeed % 1; // to get per quintal rate
            if (IsDecimal > 0)
            {
                return (Convert.ToInt32(CleanedSeed) + 1) * common.getFees("PROC_" + CropType);
            }
            else return Convert.ToInt32(CleanedSeed) * common.getFees("PROC_" + CropType);
        }
        protected void chkPayment_CheckedChanged(object sender, EventArgs e)
        {
            List<OnlinePayment> processingEntryPayments = new List<OnlinePayment>();
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[8]
            { new DataColumn("Form1ID"),
                new DataColumn("STLCharge",typeof(decimal)),
                new DataColumn("GPTCharge",typeof(decimal)),
                new DataColumn("ProcessingCharge",typeof(decimal)),
                new DataColumn("ClothBagCharge",typeof(decimal)),
                new DataColumn("PolytheneCharge",typeof(decimal)),
                new DataColumn("CourierCharge",typeof(decimal)),
                new DataColumn("Total",typeof(decimal) )
             });
            foreach (GridViewRow row in gvEntries.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    OnlinePayment processingEntryPayment = new OnlinePayment();
                    CheckBox chkRow = (row.Cells[16].FindControl("chkPayment") as CheckBox);
                    if (chkRow.Checked && chkRow.Enabled)
                    {
                        string Form1ID = row.Cells[1].Text;
                        decimal STLCharge = 0;
                        decimal GPTCharge = 0;
                        decimal ProcessingCharge = 0;
                        decimal ClothBagCharge = 0;
                        decimal PolytheneCharge = 0;
                        decimal CourierCharge = 0;
                        decimal Total = 0;
                        decimal CleanedSeed = row.Cells[12].Text.ToDecimal() / 100; // convert to quintal
                        Form1Details f1 = formOneService.ReadById(Form1ID.ToInt());
                        FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
                        SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                        CropMaster cm = cropMasterService.GetById(sv.Crop_Id);

                        STLCharge = getSTLCharge(CleanedSeed, cm.LotSize, Form1ID.ToInt());
                        GPTCharge = getGOTCharge(CleanedSeed, cm.LotSize, cm.GOT, Form1ID.ToInt());
                        ProcessingCharge = getProcessingCharge(CleanedSeed, cm.CropType);
                        ClothBagCharge = getClothBagCharge(CleanedSeed, cm.LotSize, cm.GOT);
                        PolytheneCharge = common.getFees("POLYBAG");
                        CourierCharge = getCourierCharge(CleanedSeed, cm.LotSize, cm.GOT); 
                        Total = Convert.ToDecimal(
                                     Convert.ToDecimal(STLCharge) +
                                     Convert.ToDecimal(GPTCharge) +
                                     Convert.ToDecimal(ProcessingCharge) +
                                     Convert.ToDecimal(ClothBagCharge) +
                                     Convert.ToDecimal(PolytheneCharge) +
                                     Convert.ToDecimal(CourierCharge));

                      
                        dt.Rows.Add(Form1ID,
                           decimal.Round(STLCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(GPTCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(ProcessingCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(ClothBagCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(PolytheneCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(CourierCharge, 2, MidpointRounding.AwayFromZero),
                           decimal.Round(Total, 2, MidpointRounding.AwayFromZero));

                        processingEntryPayment.RegNumber = Form1ID.ToInt();
                        processingEntryPayment.InspectionCharge = Convert.ToDecimal(ProcessingCharge);
                        processingEntryPayment.STLCharge = Convert.ToDecimal(STLCharge);
                        processingEntryPayment.GPTCharge = Convert.ToDecimal(GPTCharge);
                        processingEntryPayment.ClothBagCharge = Convert.ToDecimal(ClothBagCharge);
                        processingEntryPayment.PolytheneCharge = Convert.ToDecimal(PolytheneCharge);
                        processingEntryPayment.CourierCharge = Convert.ToDecimal(CourierCharge);
                        processingEntryPayment.TotalAmountPaid = Convert.ToDecimal(Total);
                        processingEntryPayments.Add(processingEntryPayment);
                    }
                }
            }
            Session.Add("processingEntryPaymentDetails", processingEntryPayments);
            lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            Session.Add("amount", lblTotal.Text);
            if (Convert.ToDecimal(lblTotal.Text) > 0)
            {
                pnlPayment.Visible = true;
            }
            else { pnlPayment.Visible = false; }
            gvPayment.DataSource = dt;
            gvPayment.DataBind();
            checkPaymentMode();
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
            }
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                byte[] a = fu_Check.FileBytes;
                SavePayment(rbPaymentMode.SelectedValue);
            }
        }
        private void BindPaymentGrid(string Form1IDs)
        {
            //DataTable dt = new DataTable();
            //dt.Columns.AddRange(new DataColumn[8]
            //{ new DataColumn("Form1ID"),
            //    new DataColumn("STLCharge"),
            //    new DataColumn("GPTCharge"),
            //    new DataColumn("ProcessingCharge"),
            //    new DataColumn("ClothBagCharge"),
            //    new DataColumn("PolytheneCharge"),
            //    new DataColumn("CourierCharge"),
            //    new DataColumn("Total",typeof(decimal))
            // });
            //string[] FormIDs = Form1IDs.Split('_');
            //foreach (string ID in FormIDs)
            //{
            //    string Form1ID = ID;
            //    string STLCharge = "150.00";
            //    string GPTCharge = "";
            //    string ProcessingCharge = "";
            //    string ClothBagCharge = "";
            //    string PolytheneCharge = "";
            //    string CourierCharge = "";
            //    decimal Total = 0;
            //    Form1Details f1 = formOneService.ReadById(Form1ID.ToInt());
            //    FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
            //    SourceVerification sv = sourceVerificationService.ReadById(f1.SourceVarification_Id);
            //    CropMaster cm = cropMasterService.GetById(sv.Crop_Id);



            //    if (farDet.StatusCode == 1)
            //    {
            //        RegFees = "0";
            //    }
            //    else
            //    {
            //        RegFees = common.getFees("F1").ToString();
            //    }
            //    decimal InspectionAmout = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
            //    InspectionCharge = decimal.Round(InspectionAmout, 2, MidpointRounding.AwayFromZero).ToString();// InspectionAmout.ToString();

            //    if (Convert.ToInt16(cm.GOT) == 0)
            //    {
            //        if (f1.ClassSeedtoProduced == 2 || f1.ClassSeedtoProduced == 3)
            //        {
            //            GPTCharge = common.getFees("GOT").ToString();
            //        }
            //        else
            //        { GPTCharge = "0.00"; }
            //    }
            //    else
            //    {
            //        GPTCharge = cm.GOT.ToString();
            //    }
            //    OtherCharge = common.getFees("OTHER").ToString();
            //    Total = Convert.ToDecimal(
            //                 Convert.ToDecimal(RegFees) +
            //                 Convert.ToDecimal(InspectionCharge) +
            //                 Convert.ToDecimal(STLCharge) +
            //                 Convert.ToDecimal(GPTCharge) +
            //                 Convert.ToDecimal(OtherCharge));

            //    dt.Rows.Add(Form1ID, STLCharge, GPTCharge, OtherCharge, decimal.Round(Total, 2, MidpointRounding.AwayFromZero));

            //}
            //// lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            //if (Convert.ToDecimal(lblTotal.Text) > 0)
            //{
            //    pnlPayment.Visible = true;
            //}
            //else { pnlPayment.Visible = false; }
            //gvPayment.DataSource = dt;
            //gvPayment.DataBind();
            //SavePayment("O");
        }
        private void SavePayment(string PaymentMode)
        {
            int result = 0;
            foreach (GridViewRow row in gvPayment.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    int Form1ID = Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text);
                    decimal STLCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[2].Text);
                    decimal GPTCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[3].Text);
                    decimal ProcessingCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[4].Text);
                    decimal ClothBagCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[5].Text);
                    decimal PolytheneCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[6].Text);
                    decimal CourierCharge = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[7].Text);
                    decimal Total = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[8].Text);

                    //int PaymentId = processingPaymentDetailsService.GetBy(Convert.ToInt16(Form1ID)).Id;
                    var pd = new ProcessingPaymentDetails
                    {
                        AmountPaid = Total,
                        BankTransactionID = txtCheckNo.Text,
                        PaymentDate = System.DateTime.Now,
                        PaymentMode = PaymentMode,
                        PaymentStatus = "Y",
                        Form1ID = Form1ID,
                        GPTCharge = GPTCharge,
                        STLCharge = STLCharge,
                        ClothBagCharge = ClothBagCharge,
                        CourierCharge = CourierCharge,
                        PolytheneCharge = PolytheneCharge,
                        ProcessingCharge = ProcessingCharge,

                    };
                    result = processingPaymentDetailsService.Create(pd);
                }
            }
            if (result > 0)
            {
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "Payment Details Saved successfully.";
                gvPayment.DataSource = null;
                gvPayment.DataBind();
                // gvPayment.Visible = false;
                lblTotal.Text = "0.00";
                pnlPayment.Visible = false;
                BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
            }
        }
        
        protected void EPayment_Response(System.Collections.Specialized.NameValueCollection nvc, object sender, EventArgs e)
        {
            
            List<OnlinePayment> pEntryPaymentsRetrieve = new List<OnlinePayment>(); ;
            string bank_txn = Request.Form["mihpayid"];
            string amount = Request.Form["amount"];
            string status = Request.Form["status"];
            if (Session["processingEntryPaymentDetails"] != null)
            {
                pEntryPaymentsRetrieve = (List<OnlinePayment>)Session["processingEntryPaymentDetails"];
            }
            if (status != null)
            {
                if (status == "success")
                {
                    foreach (var payments in pEntryPaymentsRetrieve)
                    {

                        int Form1IDs = payments.RegNumber;
                        Form1Details f1 = formOneService.ReadById(Form1IDs);
                        lblMessage.Text = "Payment Success";
                        int paymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(f1.Id)).Id;
                        var pd = new ProcessingPaymentDetails
                        {
                            AmountPaid = payments.TotalAmountPaid,
                            BankTransactionID = bank_txn,
                            PaymentDate = System.DateTime.Now,
                            PaymentMode = "O",
                            PaymentStatus = "Y",
                            PaySlip = fu_Check.FileBytes != null ? fu_Check.FileBytes : null,
                            Form1ID = Form1IDs,
                            GPTCharge = payments.GPTCharge,
                            STLCharge = payments.STLCharge,
                            ClothBagCharge = payments.ClothBagCharge,
                            CourierCharge = payments.CourierCharge,
                            PolytheneCharge = payments.PolytheneCharge,
                            ProcessingCharge = payments.ProcessingCharge,

                        };
                        int  result = processingPaymentDetailsService.Create(pd);
                        if (result > 0)
                        { 
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Payment Details Saved successfully.";
                        gvPayment.DataSource = null;
                        gvPayment.DataBind();
                        // gvPayment.Visible = false;
                        lblTotal.Text = "0.00";
                        pnlPayment.Visible = false;
                        BindGrid(0, System.DateTime.Now.Date, System.DateTime.Now.Date);
                         
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Payment failed";
                }
            }
        }
        private string getForm1IDs()
        {
            string Form1IDs = "";
            foreach (GridViewRow row in gvPayment.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Form1IDs += Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text) + "_";
                }
            }
            return Form1IDs.Remove(Form1IDs.Length - 1); ;
        }
       
        
    }
}