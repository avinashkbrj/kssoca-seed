﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using KSSOCA.Core.Validators;
    using System.Web.UI.WebControls;
    using System.Data;
    using System.Collections.Specialized;
    using System.Text;
    using System.Web;
    using System.Configuration;
    public partial class SPURegistrationApproval : System.Web.UI.Page
    {
        TalukMasterService talukService;
        UserMasterService userMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        SPURegistrationService sPURegistrationService;
        CropMasterService cropMasterService;
        Common common = new Common();
        SPURegistrationTransactionService sPURegistrationTransactionService;
        PaymentDetailService paymentDetailService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        public SPURegistrationApproval()
        {
            talukService = new TalukMasterService();
            userMasterService = new UserMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            sPURegistrationService = new SPURegistrationService();
            cropMasterService = new CropMasterService();
            sPURegistrationTransactionService = new SPURegistrationTransactionService();
            paymentDetailService = new PaymentDetailService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("SPUA"); litNote.Text = common.getNote("SPUA");
               
                string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                Session.Add("surl", surl);
               
                if (Request.QueryString["Id"] != null)
                {
                    int id = Request.QueryString.Get("Id").ToInt();

                    if (id > 0)
                    {
                        SPURegistration registrationForm = sPURegistrationService.GetById(id);

                        if (registrationForm == null)
                        {
                            this.Redirect("Dashboard.aspx");
                        }
                        else
                        {
                            InitComponent(registrationForm);
                            if (!IsPostBack)
                            {
                                NameValueCollection nvc = Request.Form;
                                if (nvc.Count > 0)
                                {
                                    EPayment_Response(nvc, registrationForm, sender, e);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    if (userMaster.Role_Id == 10)
                    {
                        SPURegistration registrationForm = sPURegistrationService.GetByUserId(userMaster.Id);

                        if (registrationForm == null)
                        {
                            this.Redirect("Transactions/FormRegistrationCreate.aspx");
                        }
                        else
                        {
                            InitComponent(registrationForm);
                            if (!IsPostBack)
                            {
                                NameValueCollection nvc = Request.Form;
                                if (nvc.Count > 0)
                                {
                                    EPayment_Response(nvc, registrationForm, sender, e);
                                }
                            }
                        }
                    }
                    else
                    {
                        this.Redirect("Dashboard.aspx");
                    }

                }
            }
            catch (Exception ex)
            {

                lblMessage.Text = "Error occurred. Please contact administrator.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {

                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMaster.Role_Id);
                int TransactionStatus = 0;
                int ToID = 0;
                if (RoleID == 1)
                {
                    if (txtSPUNO.Text.Trim() != "")
                    {
                        msgtxtSPUNO.Text = "";
                        ToID = Convert.ToInt16(sPURegistrationService.GetById(Convert.ToInt16(Id.Value.ToInt())).SPUId);
                        FinalApproval(txtSPUNO.Text.ToInt());
                        TransactionStatus = 10; // As no reporting manager for director , send the status back to producer
                        TransactionMethodCreate(sender, e, userMaster, RoleID, TransactionStatus, ToID);
                    }
                    else
                    {
                        msgtxtSPUNO.Text = "Please enter SPU No.";
                    }
                }
                else
                {
                    ToID = Convert.ToInt16(userMaster.Reporting_Id);
                    TransactionStatus = Convert.ToInt16(userMasterService.GetById(Convert.ToInt16(userMaster.Reporting_Id)).Role_Id);
                }
                if (RoleID != 1)
                {
                    TransactionMethodCreate(sender, e, userMaster, RoleID, TransactionStatus, ToID);
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }

        private void TransactionMethodCreate(object sender, EventArgs e, UserMaster userMaster, int RoleID, int TransactionStatus, int ToID)
        {
            var rft = new SPURegistrationTransaction
            {
                FromID = userMaster.Id,
                ToID = ToID,
                SPURegistrationID = Id.Value.ToInt(),
                TransactionDate = System.DateTime.Now,
                TransactionStatus = TransactionStatus,
                Remarks = txtRemarks.Text,
                IsCurrentAction = 1
            };

            int IsCurrentUserSet = common.setIsCurrentAction("SPURegistrationTransaction", "SPURegistrationID", Id.Value.ToInt());
            if (IsCurrentUserSet > 0)
            {
                int result = sPURegistrationTransactionService.Create(rft);
                if (result == 1)
                {
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Approved successfully.";
                    if (RoleID == 1)
                    {
                        Messaging.SendSMS("Seed Processing Unit registration approved by " + userMaster.Name + " and SPU number is -" + txtSPUNO.Text + "", MobileNumber.Text, "1");

                    }
                    else
                    {
                        Messaging.SendSMS("Seed Processing Unit registration approved by " + userMaster.Name + "", MobileNumber.Text, "1");

                    }
                  
                    Page_Load(sender, e);
                }
                else
                {
                    lblMessage.Text = "Error occurred. Please contact administrator.";
                    Save.Enabled = false;
                }

            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Dashboard.aspx");
        }
        private void InitComponent(SPURegistration registrationForm)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
            var seasons = seasonMasterService.GetAll();
            var districts = districtMasterService.GetAll();
            var userMasterDetails = userMasterService.GetById(Convert.ToInt16(registrationForm.SPUId));
            District.BindListItem<DistrictMaster>(districts);
            Id.Value = registrationForm.Id.ToString();
            RegistrationNumber.Text = registrationForm.RegistrationNo;
            ProducerName.Text = userMasterDetails.Name;
            AadharNumber.Text = registrationForm.AadhaarNumber;
            Address.Text = registrationForm.Address;
            MobileNumber.Text = userMasterDetails.MobileNo;
            Name.Text = registrationForm.OfficialName;
            OldRegistrationNumber.Text = registrationForm.Last_Renewal;
            District.Items.FindByValue(userMasterDetails.District_Id.ToString()).Selected = true;
            lblDistrict.Text = District.SelectedItem.ToString();
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            Taluk.Items.FindByValue(userMasterDetails.Taluk_Id.ToString()).Selected = true;
            lblTaluk.Text = Taluk.SelectedItem.ToString();
            RegistrationDate.Text = registrationForm.RegistrationDate == null ? string.Empty : registrationForm.RegistrationDate.Value.ToString("dd/MM/yyyy");
            if (registrationForm.Photo != null && registrationForm.Photo.Length > 0)
                SPUPhoto.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Photo));

            if (registrationForm.Sign != null && registrationForm.Sign.Length > 0)
                SPUSign.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Sign));

            if (registrationForm.Seal != null && registrationForm.Seal.Length > 0)
                SPUSeal.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Seal));

            if (registrationForm.RentLetter != null && registrationForm.RentLetter.Length > 0)
                RentLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.RentLetter));

            if (registrationForm.BuildingMap != null && registrationForm.BuildingMap.Length > 0)
                BuildingMap.Text = string.Format(previewString, Serializer.Base64String(registrationForm.BuildingMap));

            if (registrationForm.PhaniLetter != null && registrationForm.PhaniLetter.Length > 0)
                PhaniLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.PhaniLetter));

            if (registrationForm.CurrentBill != null && registrationForm.CurrentBill.Length > 0)
                CurrentBill.Text = string.Format(previewString, Serializer.Base64String(registrationForm.CurrentBill));

            if (registrationForm.CentralRegLetter != null && registrationForm.CentralRegLetter.Length > 0)
                CentralRegLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.CentralRegLetter));

            if (registrationForm.GramPanchayatLetter != null && registrationForm.GramPanchayatLetter.Length > 0)
                GramPanchayatLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.GramPanchayatLetter));

            if (registrationForm.AirPollutionDepLetter != null && registrationForm.AirPollutionDepLetter.Length > 0)
                AirPollutionDepLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.AirPollutionDepLetter));

            if (registrationForm.PurchaseLetter != null && registrationForm.PurchaseLetter.Length > 0)
                PurchaseLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.PurchaseLetter));

            if (registrationForm.IDCard != null && registrationForm.IDCard.Length > 0)
                IDCard.Text = string.Format(previewString, Serializer.Base64String(registrationForm.IDCard));

            gvParticulars.DataSource = getParticularDetails(registrationForm.Id);
            gvParticulars.DataBind();
            gvstatus.DataSource = sPURegistrationTransactionService.GetAllTransactionsBy(registrationForm.Id);
            gvstatus.DataBind();
            int txnid = paymentDetailService.GetBy(Convert.ToInt16(sPURegistrationService.GetById(Id.Value.ToInt()).SPUId)).Id;
            Session.Add("txnid", txnid);
            Session.Add("fname", ProducerName.Text);
            Session.Add("mobilenumber", MobileNumber.Text);
            Session.Add("emailid", userMasterDetails.EmailId);
          
            int paymentstatus = CheckPaymentDetails(Convert.ToInt16(registrationForm.SPUId));
            if (paymentstatus == 1)
            {
                pnlPaymentDetails.Visible = true;
            }
            else
            {
                pnlPaymentDetails.Visible = false;
                lblMessage.Text = "Payment details not updated yet.";
            }
            var userMaster = userMasterService.GetFromAuthCookie();

            SPURegistrationTransaction transaction = new SPURegistrationTransaction();
            transaction = sPURegistrationTransactionService.GetMaxTransactionDetails(registrationForm.Id);
            int ActionRole_Id = transaction.ToID;
            lblSPUNo.Text = registrationForm.SPUNO.ToString();
            Session.Add("amount", lblamounttobepaid.Text);
            if (userMaster.Role_Id == 10) // SPU Permission
            {
                pnlRemarks.Visible = false;
                if (transaction.TransactionStatus == 0)
                {
                    btnEditApplication.Visible = true;
                }
                if (paymentstatus == 0)
                {
                    pnlPaymentDetails.Visible = false;
                    pnlPayment.Visible = true;
                }
                else if (paymentstatus == 1)
                {
                    pnlPaymentDetails.Visible = true;
                    pnlPayment.Visible = false;
                }
                if (registrationForm.SPUId != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
                checkPaymentMode();
            }
            else
            {
                pnlPayment.Visible = false;
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                    if(lblPaymentMode.Text =="Online")
                    {
                        foreach (ListItem li in rbRejectionType.Items)
                        {
                            if (li.Text == "Reject Payment")
                            {
                                li.Attributes.Add("style", "display:none");
                            }
                        }


                    }
                    if (userMaster.Role_Id == 6)//ADSC
                    { Save.Text = "Forward to ADSC"; }
                    else if (userMaster.Role_Id == 4)//ADSC
                    { Save.Text = "Forward to DDSC"; }
                    else if (userMaster.Role_Id == 3) // DDSC
                    { Save.Text = "Recommend to Director"; }
                    else if (userMaster.Role_Id == 1) // Director
                    { Save.Text = "Approve SPU"; }
                    if (userMaster.Role_Id == 1) // assigning or modifying the spu no is only authorised to director
                    {
                        txtSPUNO.Visible = true;
                        lblSPUNo.Visible = false;
                        if (registrationForm.SPUNO != "0")
                        {
                            lblSPUNo.Text = registrationForm.SPUNO.ToString();
                            txtSPUNO.Text = registrationForm.SPUNO.ToString();
                            txtSPUNO.Visible = false;
                            lblSPUNo.Visible = true;
                        }
                    }
                    else
                    {
                        txtSPUNO.Visible = false;
                        lblSPUNo.Visible = true;
                    }
                    if (paymentstatus == 0)
                    {
                        pnlRemarks.Visible = false;
                    }
                }
                else
                {
                    lblSPUNo.Visible = true;
                    pnlRemarks.Visible = false;
                }
            }
            if (registrationForm.StatusCode == 1)
            {
                lnkCertificate.Visible = true;
                pnlRemarks.Visible = false;
                pnlPayment.Visible = false;
            }
        }
        private DataTable getParticularDetails(int SPU_Reg_Id)
        {
            string query = @"select Name,[No],[Type],[Make],[Capacity],[Working_Condition]from SPUParticularsOffered po
                             inner join[SPUParticularsMaster] pm on po.SPUParticular_Id = pm.Id
                             where po.SPU_Reg_Id =" + SPU_Reg_Id;
            DataTable dt = common.GetData(query);
            return dt;
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
            }
        }
        protected void lnkCertificate_Click(object sender, EventArgs e)
        {
            this.Redirect("Reports/SPURegistrationCertificate.aspx?Id=" + Id.Value.ToInt() + "");
        }
        private int CheckPaymentDetails(int UserID)
        {
            //lblamounttobepaid.Text = common.getFees("SPU").ToString();
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View </a></span>";
            PaymentDetails pd = paymentDetailService.GetBy(UserID);
            if (pd != null)
            {
                string PaymentStatus = Convert.ToString(pd.PaymentStatus);
                if (PaymentStatus == "Y")
                {
                    lblRegistrationFee.Text = Convert.ToString(pd.AmountPaid);
                    lblPaymentDate.Text = pd.PaymentDate.Value.ToString("dd/MM/yyyy"); 
                    lblPaymentMode.Text = Convert.ToString(pd.PaymentMode) == "C" ? "Cheque/DD" : "Online";
                    lblBankTransactionID.Text = Convert.ToString(pd.BankTransactionID);
                    byte[] check = pd.PaySlip;
                    if (check != null && check.Length > 0)
                        LitCheck.Text = string.Format(previewString, Serializer.Base64String(check));
                    return 1;
                }
                else if (PaymentStatus == "N")
                {
                    if (pd.PaymentType != null)
                    {
                        hdnPaymentType.Value = "SPUR"; // Renewal

                        if (System.DateTime.Now.Month > 8)
                        {
                            lblamounttobepaid.Text = (Convert.ToDecimal(common.getFees("SPUR")) + Convert.ToDecimal(common.getFees("SPUL"))).ToString();
                        }
                        else
                            lblamounttobepaid.Text = common.getFees("SPUR").ToString(); // Renewal fees
                        return 0;
                    }
                    else
                    {
                        hdnPaymentType.Value = "SPU"; // first time registration
                        lblamounttobepaid.Text = common.getFees("SPU").ToString(); // Registartion fees
                        return 0;
                    }
                }
                else if (PaymentStatus == "R")
                {
                    lblamounttobepaid.Text = common.getFees(pd.PaymentType).ToString();
                    hdnPaymentType.Value = pd.PaymentType;
                }
                return 0;
            }
            else
            {
                lblamounttobepaid.Text = common.getFees("SPU").ToString();
                return 0;
            }
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                decimal AmountToPay = lblamounttobepaid.Text.ToDecimal();
                int PaymentId = paymentDetailService.GetBy(Convert.ToInt16(sPURegistrationService.GetById(Id.Value.ToInt()).SPUId)).Id;
                var pd = new PaymentDetails
                {
                    Id = PaymentId,
                    AmountPaid = AmountToPay,
                    BankTransactionID = txtCheckNo.Text,
                    PaymentDate = System.DateTime.Now,
                    PaymentMode = rbPaymentMode.SelectedValue,
                    PaymentType = hdnPaymentType.Value,
                    PaymentTypeRefID = Id.Value.ToInt(),
                    UserID = userMasterService.GetFromAuthCookie().Id,
                    PaymentStatus = "Y",
                    PaySlip = fu_Check.FileBytes
                };
                int result = paymentDetailService.Update(pd);
                if (result == 1)
                {
                    ResendPaymentDetails();
                    var userMaster = userMasterService.GetFromAuthCookie();
                    
                    int scoId = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                              (Convert.ToInt16(userMaster.District_Id),
                               Convert.ToInt16(userMaster.Taluk_Id), 6).User_Id);
                    var scoDetails = userMasterService.GetById(scoId);
                     Messaging.SendSMS("New Seed Processing Unit application recieved from "+ ProducerName.Text + " ", scoDetails.MobileNo, "1");

                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "Payment Details saved successfully.";
                    Page_Load(sender, e);
                }
            }
        }
       
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMaster.Role_Id);
                int TransactionStatus = 0;
                SPURegistration rf = sPURegistrationService.GetById(Id.Value.ToInt());
                if (rbRejectionType.SelectedValue == "A")
                {
                    Messaging.SendSMS("SPU Registration rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                    TransactionStatus = 0;
                }
                else if (rbRejectionType.SelectedValue == "P")
                {
                    TransactionStatus = 100; // 100 indicates payment rejection
                    int i = RejectPayment(Convert.ToInt16(rf.SPUId));
                    if (i == 1)
                    { Messaging.SendSMS("SPU Registration payment rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }
                var rft = new SPURegistrationTransaction
                {
                    FromID = userMaster.Id,
                    ToID = Convert.ToInt16(rf.SPUId), // get userid
                    SPURegistrationID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus, // indicates rejection
                    Remarks = txtRemarks.Text,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("SPURegistrationTransaction", "SPURegistrationID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = sPURegistrationTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Rejected successfully.";
                        Page_Load(sender, e);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        private int RejectPayment(int UserID)
        {
            string query = "update PaymentDetails set PaymentStatus='R' where UserID= " + UserID;
            return common.ExecuteNonQuery(query);
        }
        private int FinalApproval(int SPUNO)
        {
            string RegNo = sPURegistrationService.GenerateRegistrationNumber();
            DateTime validity = Convert.ToDateTime("31/07/" + RegNo.Substring(RegNo.Length - 4));
            string sp = "";
            var spur = sPURegistrationService.GetById(Id.Value.ToInt());
            var user = userMasterService.GetById(Convert.ToInt32(spur.SPUId));
            if (SPUNO <= 99 && SPUNO > 9)
            {
                sp = "0" + SPUNO;
            }
            else if (SPUNO < 10)
            {
                sp = "00" + SPUNO;
            }
            else
            {
                sp = SPUNO.ToString();
            }


            var spu = new SPURegistration
            {
                Id = Id.Value.ToInt(),
                SPUNO = sp,
                RegistrationNo = RegNo,
                Validity = validity,
                StatusCode = 1,
                Name = sp + " - " + user.Name.ToUpper() + ", Address :" + spur.Address
            };
            return sPURegistrationService.Update(spu);
            //string query = "update SPURegistration set StatusCode=1,RegistrationNo='" + RegNo + "',SPUNO=" + SPUNO + ",validity='" + validity + "', where Id= " + SPUId;
            //return common.ExecuteNonQuery(query);
        }
        private void ResendPaymentDetails()
        {
            SPURegistrationTransaction transaction = new SPURegistrationTransaction();
            transaction = sPURegistrationTransactionService.GetMaxTransactionDetails(Id.Value.ToInt());
            if (transaction.TransactionStatus == 100)
            {
                int ToID = transaction.FromID;
                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                var userMaster = userMasterService.GetFromAuthCookie();
                var rft = new SPURegistrationTransaction
                {
                    FromID = userMaster.Id,
                    ToID = ToID,
                    SPURegistrationID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("SPURegistrationTransaction", "SPURegistrationID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = sPURegistrationTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.Text = " saved data.";
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }
            }
        }

        protected void EPayment_Response(NameValueCollection nvc, SPURegistration registrationForm, object sender, EventArgs e)
        {
            string bank_txn = Request.Form["txnid"];
            string amount = Request.Form["amount"];
            string f_code = Request.Form["status"];
            string transaction_id = Request.Form["mihpayid"];
            if (f_code != null)
            {
                CheckPaymentDetails(Convert.ToInt16(registrationForm.SPUId));
                if (Convert.ToString(f_code).Trim().ToLower() == "success")
                {
                    lblMessage.Text = "Payment Success.";
                    var pd = new PaymentDetails
                    {
                        Id = bank_txn.ToInt(), // Paymentdetails table ID
                        AmountPaid = amount.ToDecimal(),
                        BankTransactionID = transaction_id,
                        PaymentDate = System.DateTime.Now,
                        PaymentMode = "O",
                        PaymentType = hdnPaymentType.Value,
                        PaymentTypeRefID = registrationForm.Id,
                        UserID = Convert.ToInt16(registrationForm.SPUId),
                        PaymentStatus = "Y",
                    };
                    int result = paymentDetailService.Update(pd);
                    if (result == 1)
                    {
                        ResendPaymentDetails();
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Payment Details Saved successfully.";
                        this.Redirect("/Transactions/SPURegistrationApproval.aspx");
                    }
                }
                else
                {
                    lblMessage.Text = "Payment Failed";
                }
            }

        } 
    }
}