﻿<%@ Page Title="" Language="C#" EnableEventValidation="true" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SPURegistrationCreate.aspx.cs"
    Inherits="KSSOCA.Web.Transactions.SPURegistrationCreate" %>

<%@ Register Namespace="KSSOCA.Core.Validators" TagPrefix="cst" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upnl">
        <ProgressTemplate>
            <div class="modal">
                <div class="center">
                    <img alt="" src="img/loading.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <br />
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    &nbsp;&nbsp;<asp:Button ID="btnViewApplication" runat="server" Text="View Application Status" CssClass="btn-primary" Visible="false" PostBackUrl="~/Transactions/SPURegistrationApproval.aspx" />
                    <br />
                    <asp:HiddenField ID="ParticularsOfferred" runat="server" />
                </div> 
                <br /> 
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>  
                            <asp:Label runat="server" ID="lblErrorMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">1.Basic Details
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">1.Registration Number</td>
                        <td width="25%">
                            <asp:TextBox runat="server" CssClass="form-control" ID="RegistrationNumber"></asp:TextBox>
                        </td>

                        <td width="25%">2.Name of Seed Processing Unit</td>
                        <td width="25%">
                            <asp:TextBox runat="server" CssClass="form-control" ID="ProducerName" placeholder="SPU Name" />
                        </td>
                    </tr>
                    <tr>
                        <td>3.Email Id</td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="EmailId" placeholder="Email Id" />
                            <asp:RequiredFieldValidator runat="server" ID="EmailRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="EmailId" ErrorMessage="Email Id can't be empty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="EmailRegEx" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="EmailId" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                                ErrorMessage="Please enter valid email id"></asp:RegularExpressionValidator>
                        </td>
                        <td>4.Mobile Number</td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="MobileNumber" MaxLength="10" placeholder="Mobile Number" TextMode="Number" onkeypress="return CheckNumber(event);" />
                            <asp:RequiredFieldValidator runat="server" ID="MobileNumberRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="MobileNumber" ErrorMessage="Mobile Number can't be empty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="MobileNumberMinMax" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[0-9]{10,10}$" ControlToValidate="MobileNumber"
                                ErrorMessage="Please enter valid mobile number"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>5.District</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtDistrict" CausesValidation="true" />
                            <asp:DropDownList runat="server" class="form-control" ID="District" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>6.Taluk</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtTaluk" CausesValidation="true" />
                            <asp:DropDownList runat="server" class="form-control" ID="Taluk" Visible="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>7.Name of the SP Unit Owner<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Name" placeholder="Official Name" CausesValidation="true" />
                            <asp:RequiredFieldValidator runat="server" ID="NameRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="Name" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="NameRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9@&#.,()_\s-]{3,75}$" ControlToValidate="Name"
                                ErrorMessage="Name required min of 3 characters and max 75 characters"></asp:RegularExpressionValidator>
                        </td>
                        <td>8.Address<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Address" TextMode="MultiLine" />
                            <asp:RequiredFieldValidator runat="server" ID="AddressRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="Address" ErrorMessage="Address can't be empty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9@&#,:/.()_\s-]{5,200}$" ControlToValidate="Address"
                                ErrorMessage="Enter valid address. allowed characters in address are A-Za-z0-9@&#,:/.()_\s-"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>9.Aadhaar Number<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="AadharNumber" MaxLength="12" placeholder="Aadhaar Number"   onkeypress="return CheckNumber(event);" />
                             <asp:RequiredFieldValidator runat="server" ID="AdharRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AadharNumber" ErrorMessage="Adhar Card number can't be empty"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="AadhaarNumberRegularEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[0-9]{12,12}$" ControlToValidate="AadharNumber"
                                ErrorMessage="Please enter valid Aadhaar number"></asp:RegularExpressionValidator>
                        </td>
                        <td>10.SPU Number(If already a registered SPU)</td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="ParmRegNumber" MaxLength="15" placeholder="SPU Number"   onkeypress="return CheckNumber(event);" />
                            <%--<asp:RegularExpressionValidator runat="server" ID="TinNumberRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[0-9]{1,20}$" ControlToValidate="ParmRegNumber"
                                ErrorMessage="Please enter valid tin number"></asp:RegularExpressionValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>11.Last renewal certificate no</td>
                        <td colspan="3">
                            <asp:TextBox runat="server" CssClass="form-control" ID="OldRegistrationNumber" placeholder="Old Registration Number" />

                        </td>
                    </tr>

                    <tr class="SideHeading">
                        <td colspan="4">2.Documents to be uploaded
                        </td>
                    </tr>
                    <tr>
                        <td>1.Seed processing unit owner Photo<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="FarmerPhoto" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredPhoto" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="Please upload the Photo"></asp:RequiredFieldValidator>

                             <cst:FileSizeValidator runat="server" id="FarmerPhotoSizeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FarmerPhotoTypeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FarmerPhoto" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>


                            <asp:Literal runat="server" ID="LitPhoto" EnableViewState="false"></asp:Literal>
                        </td>

                        <td>2.Signature<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_Sign" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="Requiredfu_Sign" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_Sign" ErrorMessage="Please upload the Signature"></asp:RequiredFieldValidator>

                            <cst:FileSizeValidator runat="server" id="FileSizefu_Sign" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_Sign" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator id="FileTypefu_Sign" runat="server"
                                ControlToValidate="fu_Sign"
                                ForeColor="Red" Display="Dynamic"
                                ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="Litfu_Sign" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>3.Stampped Seal<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_seal" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="Requiredfu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="Please upload the Stampped Seal"></asp:RequiredFieldValidator>

                            <cst:FileSizeValidator runat="server" id="FileSizefu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypefu_seal" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_seal" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="Litfu_seal" EnableViewState="false"></asp:Literal>
                        </td>

                        <td>4.Seed Processing Unit Rental Agreement/Ownership document<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="RentLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredRentLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="RentLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizeRentLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="RentLetter" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator id="RentLetterTypeValidator" runat="server"
                                ControlToValidate="RentLetter"
                                ForeColor="Red" Display="Dynamic"
                                ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="LitRentLetter" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>5.Building Approval letter from gram panchayat/taluk panchayat/nagar sabha<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="BuildingMap" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredBuildingMap" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="BuildingMap" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                             <cst:FileSizeValidator runat="server" id="FileSizeBuildingMap" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="BuildingMap" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator id="BuildingMapTypeValidator" runat="server"
                                ControlToValidate="BuildingMap"
                                ForeColor="Red" Display="Dynamic"
                                ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="LitBuildingMap" EnableViewState="false"></asp:Literal>
                        </td>


                        <td>6.Phani Letter<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="PhaniLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredPhaniLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PhaniLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizePhaniLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PhaniLetter" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator1" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PhaniLetter" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="LitPhaniLetter" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>7.Certificate from Local Supply company /Electricity Bill<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="CurrentBill" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredCurrentBill" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CurrentBill" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizeCurrentBill" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CurrentBill" ErrorMessage="File size must not exceed {0} KB">
                             </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator2" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CurrentBill" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="LitCurrentBill" EnableViewState="false"></asp:Literal>
                        </td>

                        <td>8.Registration letter from District industrial centre <span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="CentralRegLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredCentralRegLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CentralRegLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizeCentralRegLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CentralRegLetter" ErrorMessage="File size must not exceed {0} KB">
                           </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator3" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="CentralRegLetter" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitCentralRegLetter" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>9.Permission letter from Gram Pachayat/Local Authority <span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="GramPanchayatLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredGramPanchayatLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="GramPanchayatLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>

                            <cst:FileSizeValidator runat="server" id="FileSizeGramPanchayatLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="GramPanchayatLetter" ErrorMessage="File size must not exceed {0} KB">
                           </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator4" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="GramPanchayatLetter" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>

                            <asp:Literal runat="server" ID="LitGramPanchayatLetter" EnableViewState="false"></asp:Literal>
                        </td>

                        <td>10.Certificate from Air Pollution Department<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="AirPollutionDepLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredAirPollutionDepLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AirPollutionDepLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                             <cst:FileSizeValidator runat="server" id="FileSizeAirPollutionDepLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AirPollutionDepLetter" ErrorMessage="File size must not exceed {0} KB">
                           </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeAirPollutionDepLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AirPollutionDepLetter" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitAirPollutionDepLetter" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>11.All Machinery Purchase Bill<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="PurchaseLetter" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredPurchaseLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PurchaseLetter" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FileSizePurchaseLetter" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PurchaseLetter" ErrorMessage="File size must not exceed {0} KB">
                           </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator6" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="PurchaseLetter" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitPurchaseLetter" EnableViewState="false"></asp:Literal>
                        </td>

                        <td>12.Any Government ID Proof (Adhar Card/Voter ID/Pan Card/Driving License)<span style="color: red">*</span></td>
                        <td>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="IDCard" accept="image/*" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredIDCard" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="IDCard" ErrorMessage="Please upload the document"></asp:RequiredFieldValidator>
                             <cst:FileSizeValidator runat="server" id="FileSizeIDCard" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="IDCard" ErrorMessage="File size must not exceed {0} KB">
                           </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FileTypeValidator7" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="IDCard" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitIDCard" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">3.Particulars of Seed Processing Machineries installed in the Seed Processing Unit.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvParticulars" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover"
                                AllowPaging="true" PageSize="20" DataKeyNames="Id">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> . 
                                           <asp:HiddenField ID="hdnId" runat="server" Value='<%# Eval("Id") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtNo" runat="server" CssClass="form-control" Text='<%# Eval("No") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtType" runat="server" CssClass="form-control" Text='<%# Eval("Type") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <Columns>
                                    <asp:TemplateField HeaderText="Make">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMake" runat="server" CssClass="form-control" Text='<%# Eval("Make") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Capacity">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCapacity" runat="server" CssClass="form-control" Text='<%# Eval("Capacity") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Working Condition">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtWorkingCondition" runat="server" CssClass="form-control" Text='<%# Eval("Working_Condition") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div>
                <div align="center">
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="btnUpdate" Text="Re-Save" OnClick="btnUpdate_Click" Visible="false" />
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="Save" Text="Save" OnClick="Save_Click" />

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="Save" />

        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript" src='<%= ResolveUrl("~/js/SPURegistrationScript.js") %>'></script>
</asp:Content>
