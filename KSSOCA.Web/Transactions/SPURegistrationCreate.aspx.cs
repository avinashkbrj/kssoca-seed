﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Web.UI.HtmlControls;
    using System.Data;

    public partial class SPURegistrationCreate : SecurePage
    {
        TalukMasterService talukService;
        UserMasterService userMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        SPURegistrationService sPURegistrationService;
        SPURegistration registrationForm;
        Common common = new Common();
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        SPURegistrationTransactionService sPURegistrationTransactionService;
        PaymentDetailService paymentDetailService;
        public SPURegistrationCreate()
        {
            talukService = new TalukMasterService();
            userMasterService = new UserMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            sPURegistrationService = new SPURegistrationService();
            EnableSave = true;
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            sPURegistrationTransactionService = new SPURegistrationTransactionService();
            paymentDetailService = new PaymentDetailService();
        }
        public bool EnableSave { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("SPUC"); litNote.Text = common.getNote("SPUC");
                if (!Page.IsPostBack)
                {
                    registrationForm = sPURegistrationService.GetByUserId(userMasterService.GetFromAuthCookie().Id);

                    if (registrationForm != null)
                    {
                        List<SPURegistrationTransaction> rco = sPURegistrationTransactionService.GetByRegistrationFormId(registrationForm.Id);
                        SPURegistrationTransaction transaction = new SPURegistrationTransaction();
                        transaction = sPURegistrationTransactionService.GetMaxTransactionDetails(registrationForm.Id);
                        if (transaction != null)
                        {
                            if (transaction.TransactionStatus == 0 && transaction.ToID == userMasterService.GetFromAuthCookie().Id) //TransactionStatus 0 indicates application is  rejected needs to be edited
                            {

                                InitComponent(registrationForm);
                                btnUpdate.Visible = true;
                                Save.Visible = false;
                                btnViewApplication.Visible = true;
                            }

                            else if (transaction.TransactionStatus == 10) //TransactionStatus 0 indicates application is  rejected needs to be edited
                            {
                                if (registrationForm.Validity >= System.DateTime.Now)
                                {
                                    this.Redirect("Transactions/SPURegistrationApproval.aspx", false);
                                }
                                else
                                {
                                    lblMessage.Text = "SPU Validity Expired.";
                                   // common.ExpireUnit(transaction.Id, registrationForm.Id);
                                    common.ExpireUnit(registrationForm.Id);
                                    InitComponent(registrationForm, rco);
                                    btnUpdate.Visible = true;
                                    Save.Visible = false;
                                }
                            }
                            else
                            {
                                this.Redirect("Transactions/SPURegistrationApproval.aspx", false);
                            }
                        }
                        else if (transaction == null) // transaction set for renewal
                        {
                            lblMessage.Text = "SPU Validity Expired.";
                            common.ExpireUnit(registrationForm.Id);
                            InitComponent(registrationForm, rco);
                            btnUpdate.Visible = true;
                            Save.Visible = false;
                        }
                    }
                    else
                    {
                        InitComponent();
                    }

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    var nextYear = DateTime.Now.Year + 1;
                   
                    var regNoWithDate = userMaster.Id + "/" + DateTime.Now.ToFinancialYear();
                    var registrationForm = new SPURegistration
                    {
                        RegistrationNo = regNoWithDate,
                        SPUId = userMaster.Id,
                        RegistrationDate = System.DateTime.Now,
                        Name = (ParmRegNumber.Text.Trim() != "" ? ParmRegNumber.Text.ToInt().ToString() : "000") + " - " + ProducerName.Text.Trim().ToUpper() + ", Address:" + Address.Text.Trim(),
                        AadhaarNumber = AadharNumber.Text,
                        Address = Address.Text,
                        SPUNO = ParmRegNumber.Text.Trim() != "" ? ParmRegNumber.Text.ToInt().ToString() : "0",
                        OfficialName = Name.Text,
                        Last_Renewal = OldRegistrationNumber.Text.Trim() == ""? OldRegistrationNumber.Text : "",
                        Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : null,
                        Sign = fu_Sign.FileBytes.Length > 0 ? fu_Sign.FileBytes : null,
                        Seal = fu_seal.FileBytes.Length > 0 ? fu_seal.FileBytes : null,
                        RentLetter = RentLetter.FileBytes.Length > 0 ? RentLetter.FileBytes : null,
                        BuildingMap = BuildingMap.FileBytes.Length > 0 ? BuildingMap.FileBytes : null,
                        PhaniLetter = PhaniLetter.FileBytes.Length > 0 ? PhaniLetter.FileBytes : null,
                        CurrentBill = CurrentBill.FileBytes.Length > 0 ? CurrentBill.FileBytes : null,
                        CentralRegLetter = CentralRegLetter.FileBytes.Length > 0 ? CentralRegLetter.FileBytes : null,
                        GramPanchayatLetter = GramPanchayatLetter.FileBytes.Length > 0 ? GramPanchayatLetter.FileBytes : null,
                        AirPollutionDepLetter = AirPollutionDepLetter.FileBytes.Length > 0 ? AirPollutionDepLetter.FileBytes : null,
                        PurchaseLetter = PurchaseLetter.FileBytes.Length > 0 ? PurchaseLetter.FileBytes : null,
                        IDCard = IDCard.FileBytes.Length > 0 ? IDCard.FileBytes : null,
                        StatusCode = 0,
                        RegistrationType = "N", // N indicates new registration
                        Validity = null
                    };
                    int recordCount = sPURegistrationService.Create(registrationForm);

                    if (recordCount > 0)
                    {
                        saveName(Convert.ToInt32(userMaster.Id));
                        int SPURegistrationID = sPURegistrationService.GetByUserId(userMaster.Id).Id;
                        addParticulars(SPURegistrationID);
                        int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                  (Convert.ToInt16(userMaster.District_Id),
                                   Convert.ToInt16(userMaster.Taluk_Id), 6).User_Id);
                        var rft = new SPURegistrationTransaction
                        {
                            FromID = userMaster.Id,
                            ToID = ToID,
                            SPURegistrationID = SPURegistrationID,
                            TransactionDate = System.DateTime.Now,
                            TransactionStatus = 6, // The 6 indicates the role id of SCO
                            IsCurrentAction = 1
                        };
                        int result = sPURegistrationTransactionService.Create(rft);
                        if (result == 1)
                        {
                            var pd = new PaymentDetails
                            {
                                UserID = userMaster.Id,
                                PaymentStatus = "N",
                            };
                            int Paymentresult = paymentDetailService.Create(pd);
                            if (Paymentresult == 1)
                            {
                                this.Redirect("Transactions/SPURegistrationApproval.aspx", false);
                            }
                            else
                            {
                                lblMessage.Text = "Error occurred while saving data.";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred while saving data.";
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }

                }
            }
            catch (Exception ex)
            {
              
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Unable to save data. Please contact administrator.";
                Save.Enabled = false;
            }
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ClearForm();
            this.Redirect("~/Dashboard.aspx");
        }
        private void ClearForm()
        {
            RegistrationNumber.Text = string.Empty;
            ProducerName.Text = string.Empty;
            Address.Text = string.Empty;
            MobileNumber.Text = string.Empty;
            EmailId.Text = string.Empty;

            District.SelectedIndex = 0;
            Taluk.SelectedIndex = 0;
            Name.Text = string.Empty;
            OldRegistrationNumber.Text = string.Empty;

            AadharNumber.Text = string.Empty;
        }
        private void InitComponent(SPURegistration registrationForm = null, List<SPURegistrationTransaction> cropOfferred = null)
        {
            var seasons = seasonMasterService.GetAll();
            var userData = userMasterService.GetFromAuthCookie();
            //RegistrationNumber.Text = userData.Reg_no;
            ProducerName.Text = userData.Name;
            MobileNumber.Text = userData.MobileNo;
            EmailId.Text = userData.EmailId;
            var districts = districtMasterService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            District.Items.FindByValue(userData.District_Id.ToString()).Selected = true;
            txtDistrict.Text = District.SelectedItem.Text;
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            Taluk.Items.FindByValue(userData.Taluk_Id.ToString()).Selected = true;
            txtTaluk.Text = Taluk.SelectedItem.Text;
            RegistrationNumber.ReadOnly = true;
            RegistrationNumber.ReadOnly = true;
            MobileNumber.ReadOnly = true; ;
            EmailId.ReadOnly = MobileNumber.ReadOnly;
            txtDistrict.ReadOnly = true;
            txtTaluk.ReadOnly = true;
            District.Enabled = false;

            if (registrationForm == null)
            {
                BindParticulars();
            }
            if (registrationForm != null)
            {
                string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";

                AadharNumber.Text = registrationForm.AadhaarNumber;
                Address.Text = registrationForm.Address;
                ParmRegNumber.Text = registrationForm.SPUNO != null ? registrationForm.SPUNO.ToString() : "";
                Name.Text = registrationForm.OfficialName;
                OldRegistrationNumber.Text = registrationForm.Last_Renewal;
                Session["Photo"] = registrationForm.Photo;
                Session["Sign"] = registrationForm.Sign;
                Session["Seal"] = registrationForm.Seal;
                Session["RentLetter"] = registrationForm.RentLetter;
                Session["BuildingMap"] = registrationForm.BuildingMap;
                Session["PhaniLetter"] = registrationForm.PhaniLetter;
                Session["CurrentBill"] = registrationForm.CurrentBill;
                Session["CentralRegLetter"] = registrationForm.CentralRegLetter;
                Session["GramPanchayatLetter"] = registrationForm.GramPanchayatLetter;
                Session["AirPollutionDepLetter"] = registrationForm.AirPollutionDepLetter;
                Session["PurchaseLetter"] = registrationForm.PurchaseLetter;
                Session["IDCard"] = registrationForm.IDCard;
                if (registrationForm.Photo != null && registrationForm.Photo.Length > 0)
                    LitPhoto.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Photo));
                if (registrationForm.Sign != null && registrationForm.Sign.Length > 0)
                    Litfu_Sign.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Sign));
                if (registrationForm.Seal != null && registrationForm.Seal.Length > 0)
                    Litfu_seal.Text = string.Format(previewString, Serializer.Base64String(registrationForm.Seal));
                if (registrationForm.RentLetter != null && registrationForm.RentLetter.Length > 0)
                    LitRentLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.RentLetter));
                if (registrationForm.BuildingMap != null && registrationForm.BuildingMap.Length > 0)
                    LitBuildingMap.Text = string.Format(previewString, Serializer.Base64String(registrationForm.BuildingMap));
                if (registrationForm.PhaniLetter != null && registrationForm.PhaniLetter.Length > 0)
                    LitPhaniLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.PhaniLetter));
                if (registrationForm.CurrentBill != null && registrationForm.CurrentBill.Length > 0)
                    LitCurrentBill.Text = string.Format(previewString, Serializer.Base64String(registrationForm.CurrentBill));
                if (registrationForm.CentralRegLetter != null && registrationForm.CentralRegLetter.Length > 0)
                    LitCentralRegLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.CentralRegLetter));
                if (registrationForm.GramPanchayatLetter != null && registrationForm.GramPanchayatLetter.Length > 0)
                    LitGramPanchayatLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.GramPanchayatLetter));
                if (registrationForm.AirPollutionDepLetter != null && registrationForm.AirPollutionDepLetter.Length > 0)
                    LitAirPollutionDepLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.AirPollutionDepLetter));
                if (registrationForm.PurchaseLetter != null && registrationForm.PurchaseLetter.Length > 0)
                    LitPurchaseLetter.Text = string.Format(previewString, Serializer.Base64String(registrationForm.PurchaseLetter));
                if (registrationForm.IDCard != null && registrationForm.IDCard.Length > 0)
                    LitIDCard.Text = string.Format(previewString, Serializer.Base64String(registrationForm.IDCard));
                RequiredPhoto.Enabled = false;
                Requiredfu_Sign.Enabled = false;
                Requiredfu_seal.Enabled = false;
                RequiredRentLetter.Enabled = false;

                RequiredBuildingMap.Enabled = false;
                RequiredPhaniLetter.Enabled = false;
                RequiredCurrentBill.Enabled = false;
                RequiredCentralRegLetter.Enabled = false;

                RequiredGramPanchayatLetter.Enabled = false;
                RequiredAirPollutionDepLetter.Enabled = false;
                RequiredPurchaseLetter.Enabled = false;
                RequiredIDCard.Enabled = false;
                GetParticularsDataBack(registrationForm.Id);
            }
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Session["message"] = "Please Check Uploaded photos";
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    SPURegistration rf = sPURegistrationService.GetByUserId(userMasterService.GetFromAuthCookie().Id);
                    var registrationForm = new SPURegistration
                    {
                        Id = rf.Id,
                        //RegistrationNo = userMaster.Reg_no,
                        StatusCode = 0,
                        SPUId = userMaster.Id,
                        RegistrationDate = System.DateTime.Now,
                        Name = (ParmRegNumber.Text.Trim() != "" ? ParmRegNumber.Text.ToInt().ToString() : "000") + " - " + ProducerName.Text.Trim().ToUpper() + ", Address:" + Address.Text.Trim(),
                        AadhaarNumber = AadharNumber.Text,
                        Address = Address.Text,
                        SPUNO = ParmRegNumber.Text.Trim() != "" ? ParmRegNumber.Text.ToInt().ToString() : "0",
                        OfficialName = Name.Text,
                        Last_Renewal = OldRegistrationNumber.Text,
                        Photo = FarmerPhoto.FileBytes.Length > 0 ? FarmerPhoto.FileBytes : (byte[])Session["Photo"],
                        Sign = fu_Sign.FileBytes.Length > 0 ? fu_Sign.FileBytes : (byte[])Session["Sign"],
                        Seal = fu_seal.FileBytes.Length > 0 ? fu_seal.FileBytes : (byte[])Session["Seal"],
                        RentLetter = RentLetter.FileBytes.Length > 0 ? RentLetter.FileBytes : (byte[])Session["RentLetter"],
                        BuildingMap = BuildingMap.FileBytes.Length > 0 ? BuildingMap.FileBytes : (byte[])Session["BuildingMap"],
                        PhaniLetter = PhaniLetter.FileBytes.Length > 0 ? PhaniLetter.FileBytes : (byte[])Session["PhaniLetter"],
                        CurrentBill = CurrentBill.FileBytes.Length > 0 ? CurrentBill.FileBytes : (byte[])Session["CurrentBill"],
                        CentralRegLetter = CentralRegLetter.FileBytes.Length > 0 ? CentralRegLetter.FileBytes : (byte[])Session["CentralRegLetter"],
                        GramPanchayatLetter = GramPanchayatLetter.FileBytes.Length > 0 ? GramPanchayatLetter.FileBytes : (byte[])Session["GramPanchayatLetter"],
                        AirPollutionDepLetter = AirPollutionDepLetter.FileBytes.Length > 0 ? AirPollutionDepLetter.FileBytes : (byte[])Session["AirPollutionDepLetter"],
                        PurchaseLetter = PurchaseLetter.FileBytes.Length > 0 ? PurchaseLetter.FileBytes : (byte[])Session["PurchaseLetter"],
                        IDCard = IDCard.FileBytes.Length > 0 ? IDCard.FileBytes : (byte[])Session["IDCard"],
                    };
                    int recordCount = sPURegistrationService.Update(registrationForm);

                    if (recordCount > 0)
                    {
                        saveName(Convert.ToInt32(rf.SPUId));
                        SPURegistrationTransaction transaction = new SPURegistrationTransaction();
                        transaction = sPURegistrationTransactionService.GetMaxTransactionDetails(registrationForm.Id);
                        int SPURegistrationID = sPURegistrationService.GetByUserId(userMaster.Id).Id;
                        int ToID = 0;
                        int TransactionStatus = 0;
                        //if (transaction != null)
                        //{
                        //    ToID = transaction.FromID; // Resend to last transaction sent id
                        //    TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                        //}
                        //else if (transaction == null) // transaction null indicates the application set for renewal
                        //{
                        string qy = "update SPURegistration set RegistrationType='R',StatusCode=0 where Id = " + SPURegistrationID; // Update as Renewal
                        common.ExecuteNonQuery(qy);
                        ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                              (Convert.ToInt16(userMaster.District_Id),
                               Convert.ToInt16(userMaster.Taluk_Id), 6).User_Id);
                        TransactionStatus = 6;

                        // }
                        var rft = new SPURegistrationTransaction
                        {
                            FromID = userMaster.Id,
                            ToID = ToID,
                            SPURegistrationID = SPURegistrationID,
                            TransactionDate = System.DateTime.Now,
                            TransactionStatus = TransactionStatus,
                            IsCurrentAction = 1,

                        };
                        int IsCurrentUserSet = common.setIsCurrentAction("SPURegistrationTransaction", "SPURegistrationID", SPURegistrationID);
                        //if (IsCurrentUserSet > 0)
                        //{
                        addParticulars(SPURegistrationID);
                        int result = sPURegistrationTransactionService.Create(rft);

                        if (result == 1)
                        {
                           // var pd = new PaymentDetails
                           // {
                           //     UserID = userMaster.Id,
                           //     PaymentType = "SPU",
                           //     PaymentStatus = "N",
                           // };
                           //// PaymentDetails pay = paymentDetailService.GetById(userMaster.Id);
                           //// paymentDetailService.Delete(pay.Id);
                           // int Paymentresult = paymentDetailService.up(pd);
                           // if (Paymentresult == 1)
                           // {
                               this.Redirect("Transactions/SPURegistrationApproval.aspx", false);
                           // }
                           // else
                           // {
                           //     lblMessage.Text = "Error occurred while saving data.";
                           // }
                        }
                        //}
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred while saving data.";
                    }
                }
                else
                {
                  
                    this.Redirect("Transactions/SPURegistrationCreate.aspx", false);
                    if (Session["message"] != null)
                    {
                        lblErrorMessage.Text = Session["message"].ToString();
                        //lblErrorMessage.Text = "Please Check Uploaded photos";
                    }
                    Response.Write("<script>alert('login successful');</script>");
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Unable to save data. Please contact administrator.";
            }
        }
        private void saveName(int userid)
        {
            string query = @"update usermaster set name ='" + ProducerName.Text.Trim() + "' where id =" + userid;
            common.ExecuteNonQuery(query);
        }
        private void addParticulars(long SPU_ID)
        {
            string qy = "delete from SPUParticularsOffered where SPU_Reg_Id = " + SPU_ID;
            common.ExecuteNonQuery(qy);
            foreach (GridViewRow row in gvParticulars.Rows)
            {
                HiddenField hdnId = (HiddenField)row.FindControl("hdnId"); //spu particular master id
                TextBox txtNo = (TextBox)row.FindControl("txtNo");
                TextBox txtType = (TextBox)row.FindControl("txtType");
                TextBox txtMake = (TextBox)row.FindControl("txtMake");
                TextBox txtCapacity = (TextBox)row.FindControl("txtCapacity");
                TextBox txtWorkingCondition = (TextBox)row.FindControl("txtWorkingCondition");
                string query = @"insert into SPUParticularsOffered " +
                                 "values(" + SPU_ID + "," + hdnId.Value + ",'" + txtNo.Text + "','" + txtType.Text + "','" + txtMake.Text + "','" + txtCapacity.Text + "','" + txtWorkingCondition.Text + "')";
                common.ExecuteNonQuery(query);
            }
        }

        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(5000);
            Taluk.Items.Clear();
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.BindListItem<TalukMaster>(taluks);
        }
        private void BindParticulars()
        {
            string query = "SELECT [Id] ,[Name] ,'' as [No],'' as[Type],'' as[Make],'' as[Capacity] ,'' as[Working_Condition] FROM  SPUParticularsMaster";
            DataTable dt = common.GetData(query);
            if (dt.Rows.Count > 0)
            {
                gvParticulars.DataSource = dt;
                gvParticulars.DataBind();
            }
        }
        private void GetParticularsDataBack(int SPUID)
        {
            string query = @"SELECT [SPUParticular_Id] as Id,spum.Name,[No],[Type],[Make],[Capacity],[Working_Condition]
                      FROM  [SPUParticularsOffered] spuo
                      inner join  SPUParticularsMaster spum on spuo.SPUParticular_Id=spum.Id where spuo.SPU_Reg_Id=" + SPUID;
            DataTable dt = common.GetData(query);
            if (dt.Rows.Count > 0)
            {
                gvParticulars.DataSource = dt;
                gvParticulars.DataBind();
            }
        }
    }
}