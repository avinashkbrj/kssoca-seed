﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using System.Security.Permissions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using System.Web.UI.WebControls;
    public partial class SPURegistrationView : System.Web.UI.Page
    {
        SPURegistrationService SPURegistrationService;
        UserMasterService userMasterService;
        Common common = new Common();
        public SPURegistrationView()
        {
            SPURegistrationService = new SPURegistrationService();
            userMasterService = new UserMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    lblHeading.Text = common.getHeading("SPUV"); litNote.Text = common.getNote("SPUV");
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                //Save.Enabled = false;
                Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent()
        {

        }

        public DataTable SelectMethod()
        {
            int userId = userMasterService.GetFromAuthCookie().Id;
            // int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
            return SPURegistrationService.ReadAllByUserAuthority(userId);
        }

        protected void FormRegistrationGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                if (hdnStatusCode.Value == "1")
                {
                    lblStatus.Text = "Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblStatus.Text = "Not Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
    }
}