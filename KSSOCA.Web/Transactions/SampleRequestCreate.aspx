﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SampleRequestCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.SampleRequestCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnf1Id" runat="server" />
                    <asp:HiddenField ID="hdnbulkid" runat="server" />
                </div>
                 
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Sample Request
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Get Processing Entry By :&nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlfilter" Width="200px" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                            <asp:ListItem Text="Form-I Registration No." Value="f1"></asp:ListItem>
                            <asp:ListItem Text="Date wise" Value="dt"></asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="pnlsearchbyf1" runat="server" Visible="true">
                                Form-I Registration No. :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtForm1No" onkeypress="return CheckNumber(event);" placeholder="Form I No" onpaste="return false" />
                                <asp:RequiredFieldValidator runat="server" ID="txtForm1NoRequiredValidator" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtForm1No" ErrorMessage="Form I No Required" ValidationGroup="v1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                    <asp:Button ID="btngetformdetails" runat="server" Text="Get Form1 Details" CssClass="btn-primary" OnClick="btngetformdetails_Click" ValidationGroup="v1" />
                            </asp:Panel>
                            <asp:Panel ID="pnlsearchbyDate" runat="server" Visible="false">
                                From Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtFromDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtFromDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtFromDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                           To Date :&nbsp;&nbsp;<asp:TextBox runat="server" ID="txtToDate" onkeypress="return false;" placeholder="Date" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtToDate" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtToDate" ErrorMessage="Bulk Stock Required" ValidationGroup="v3"></asp:RequiredFieldValidator>
                                <asp:Button ID="btngetforms" runat="server" Text="Get Processing Entries" CssClass="btn-primary" ValidationGroup="v3" OnClick="btngetforms_Click" />

                            </asp:Panel>
                        </td>
                    </tr>
                    <asp:Panel ID="pnlform1details" runat="server" Visible="false">
                        <tr>
                            <td colspan="4">
                                <asp:Literal ID="litForm1Details" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td>Request  
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtRequest"   placeholder="Request" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtRequest" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRequest" ErrorMessage="Request Required" ValidationGroup="v2"></asp:RequiredFieldValidator>
                            </td>                             
                            <td>19.Test Type                            
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbTestType"
                                    runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbTestType_SelectedIndexChanged">
                                    <asp:ListItem class="radio-inline" Value="STL" Text="STL" Selected="True"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="GOT" Text="GOT"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>20.Tests Required
                            </td>
                            <td>
                                <asp:CheckBoxList ID="chkTests" runat="server" RepeatLayout="Flow" class="radio-inline" RepeatDirection="Vertical" Font-Bold="true" AutoPostBack="true"  ></asp:CheckBoxList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredchkTests" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="chkTests" ErrorMessage="Required Tests"></asp:RequiredFieldValidator>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                        ID="Save" Text="Submit" ValidationGroup="v2" OnClick="Save_Click" />
                                </td>
                            </tr>
                    </asp:Panel>

                </table>
                <br />
            </div>
            <div>
                <asp:GridView ID="gvEntries" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%"   >
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No." />
                        <asp:BoundField DataField="GrowerName" HeaderText="Grower Name" />
                        <asp:BoundField DataField="Crop" HeaderText="Crop" />
                        <asp:BoundField DataField="Variety" HeaderText="Variety" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <asp:BoundField DataField="EstimatedYield" HeaderText="Estimated Yield(in Qntls/Acre)" />
                        <asp:BoundField DataField="AcceptedArea" HeaderText="Accepted Area(in acres)" />
                        <asp:BoundField DataField="NoOfBags" HeaderText="Quintal" />
                        <asp:BoundField DataField="BulkStock" HeaderText="Bulk Stock(in Kg.)" />
                        <asp:BoundField DataField="SPU" HeaderText="SPU" />
                        <asp:BoundField DataField="SentTo" HeaderText="Sent To" />
                        <asp:BoundField DataField="EntryDate" HeaderText="Entry Date" />
                        <asp:BoundField DataField="AcceptedStock" HeaderText="Accepted Stock(in Kg.)" />
                        <asp:BoundField DataField="ArrivalDate" HeaderText="Stock Arrival Date" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                <asp:Label ID="lblStatus" runat="server" Font-Bold="true"></asp:Label>
                                <br />
                                <asp:Label ID="lblReMarks" runat="server" Font-Bold="true" Text='<%# Eval("Remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkdelete" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lnkUpdate" CommandArgument='<%#Eval("ID") %>'  >Enter Stock Acceptance details</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------No forms selected-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
