﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using System.Web.UI.HtmlControls;
    public partial class SampleRequestCreate : System.Web.UI.Page
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        SPURegistrationService SPUMasterService;
        SourceVerificationService sourceverficationservice;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        Common common = new Common();
        FarmerDetailsService farmerDetailsService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        FieldInspectionService fiService;
        BulkEntryService bulkEntryService;
        TestMasterService testMasterService;

        public SampleRequestCreate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            SPUMasterService = new SPURegistrationService();
            sourceverficationservice = new SourceVerificationService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            farmerDetailsService = new FarmerDetailsService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            fiService = new FieldInspectionService();
            bulkEntryService = new BulkEntryService();
            testMasterService = new TestMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            getTests(rbTestType.SelectedValue.Trim());
        }
        private void getTests(string TestType)
        {
            chkTests.Items.Clear();
            chkTests.BindCheckBoxItem(testMasterService.GetTestsByType(TestType));
            chkTests.Enabled = true;
        }
        protected void rbTestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            getTests(rbTestType.SelectedValue.Trim());
        }
        protected void btngetformdetails_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtForm1No.Text.Trim() != "")
                {
                    var user = userMasterService.GetFromAuthCookie();
                    var data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), txtForm1No.Text.Trim().ToInt(), null, null);
                    if (data != null)
                    {
                        var f1 = formOneService.ReadById(txtForm1No.Text.ToInt());
                        if (f1.Producer_Id == user.Id)
                        {
                            var far = farmerDetailsService.GetById(f1.FarmerID);
                            var sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                            var cm = cropMasterService.GetById(sv.Crop_Id);
                            var cmv = cropVarietyMasterService.GetById(sv.Variety_Id);
                            var cls = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced));
                            var fi = fiService.GetFinalInspection(f1.Id);
                            if (fi != null)
                            {
                                var bulkentry = bulkEntryService.GetbyForm1ID(txtForm1No.Text.Trim().ToInt());
                                if (bulkentry.StatusCode == 1)
                                {
                                    litForm1Details.Text =
                                                     "Grower/Farmer Name:<span style=\"color: Blue;font-weight:bold\">" + far.Name +
                                                     "</span>&nbsp;&nbsp;Grower Registration No:<span style=\"color: Blue;font-weight:bold\">" + f1.Id +
                                                     "</span>  Crop :<span style=\"color: Blue;font-weight:bold\">" + cm.Name +
                                                     "</span>&nbsp;&nbsp;Variety:<span style=\"color: Blue;font-weight:bold\">" + cmv.Name +
                                                     "</span>&nbsp;&nbsp;Class Of the Seed:<span style=\"color: Blue;font-weight:bold\">" + cls.Name +
                                                     "</span>  Accepted Seed (in Kgs):<span style=\"color: Blue;font-weight:bold\">" + bulkentry.AcceptedStock + "</span>";

                                    pnlform1details.Visible = true;
                                    hdnf1Id.Value = f1.Id.ToString();
                                }
                                else
                                {
                                    pnlform1details.Visible = false;
                                    lblMessage.Text = "Bulk has been rejected.";
                                }
                            }
                            else
                            {
                                pnlform1details.Visible = false;
                                lblMessage.Text = "Field inspection is not finalised for this Form-I Registration No.";
                            }

                        }
                        else
                        {
                            //pnlform1details.Visible = false;
                            //gvEntries.DataSource = data;
                            //gvEntries.DataBind();
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void ddlfilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlfilter.SelectedValue == "f1")
            {
                pnlsearchbyf1.Visible = true;
                pnlsearchbyDate.Visible = false;
            }
            else if (ddlfilter.SelectedValue == "dt")
            {
                pnlsearchbyf1.Visible = false;
                pnlsearchbyDate.Visible = true;
            }
        }
        protected void btngetforms_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BindGrid(0, txtFromDate.Text.ToDateTime().Date, txtToDate.Text.ToDateTime().Date);
            }
        }
        private void BindGrid(int form1id, Nullable<DateTime> FromDate, Nullable<DateTime> ToDate)
        {
            var user = userMasterService.GetFromAuthCookie();
            var data = (DataTable)null;
            if (form1id > 0)
            {
                data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, null, null);
            }
            else
            {
                data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, Utility.StartOfDay(Convert.ToDateTime(FromDate)), Utility.EndOfDay(Convert.ToDateTime(ToDate)));
            }
            // var data = bulkEntryService.GetBulkDetails(user.Id, Convert.ToInt16(user.Role_Id), form1id, FromDate, ToDate);
            if (data != null)
            {
                gvEntries.DataSource = data;
                gvEntries.DataBind();
            }
            else
            {
                gvEntries.DataSource = null;
                gvEntries.DataBind();
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {

        }
    }
}