﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Configuration;
    using System.Data.SqlClient;

    public partial class SeedAnalysisReportApproval : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        SeedAnalysisReportService seedAnalysisReportService;
        DecodingService decodingService;
        TestMasterService testMasterService;
        SeedAnalysisReportTransactionService seedAnalysisReportTransactionService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;

        public SeedAnalysisReportApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            seedAnalysisReportService = new SeedAnalysisReportService();
            decodingService = new DecodingService();
            testMasterService = new TestMasterService();
            seedAnalysisReportTransactionService = new SeedAnalysisReportTransactionService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                lblHeading.Text = common.getHeading("SAR"); litNote.Text = common.getNote("SAR");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int SSC_ID = Convert.ToInt16(Request.QueryString["Id"]);
                    InitComponent(SSC_ID);
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                // Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            var user = userMasterService.GetFromAuthCookie();
            int SId = Convert.ToInt16(Request.QueryString["Id"]);
            Form1Details form1 = formOneService.ReadById(Convert.ToInt32(lblForm1NO.Text));
            var userDetails = userMasterService.GetById(form1.Producer_Id);
            var reportId = seedAnalysisReportService.GetByLabTestNo(Convert.ToInt32(lblTestNo.Text));
            var decodingDetails=decodingService.GetById(Convert.ToInt32(lblTestNo.Text));
            var sscDetails = seedSampleCouponService.GetById(decodingDetails.SSC_Id);
            int tranactionId = common.GenerateID("SeedAnalysisReportTransaction");
            int ToID = 0;
            int status = 1;
            if (user.Role_Id == 4)
            {
                ToID = sscDetails.SampledBy;
            }
            if (user.Role_Id == 6)
            {
                ToID = userDetails.Id;
                objsqlconn.Open();
                string updateSeedAnalysisReport = "Update SeedAnalysisReport set status='" + status + "', RequisitionOfTags='" + txtRequisitionOfTags.Text + "',AllotmentsOfTags='" + txtAllotmentsOfTags.Text + "' where LabTestNo= " + lblTestNo.Text;
                SqlCommand cmdUpdateSeedAnalysisReport = new SqlCommand(updateSeedAnalysisReport, objsqlconn);
                cmdUpdateSeedAnalysisReport.ExecuteNonQuery();
                objsqlconn.Close();
            }
            // int ToID = userDetails.Id;


            var gpt = new SeedAnalysisReportTransaction
            {
                Id = tranactionId,
                FromID = user.Id,
                ToID = ToID,
                ReportID = reportId.Id,
                TransactionDate = System.DateTime.Now,
                TransactionStatus = 5,
                Remarks = txtRemarks.Text,
                IsCurrentAction = 1
            };
            int IsCurrentUserSet = common.setIsCurrentAction("SeedAnalysisReportTransaction", "ReportID", reportId.Id);
            if (IsCurrentUserSet > 0)
            {
               
                var result = seedAnalysisReportTransactionService.Create(gpt);
                if (result == 1)
                {
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                    lblMessage.Text = "STL result details saved successfully.";
                    this.Redirect("Transactions/SeedAnalysisReportApproval.aspx?Id=" + SId);
                }
            }
        }
        private void InitComponent(int SSC_ID)
        {
            var userMaster = userMasterService.GetFromAuthCookie();
            Decoding dec = decodingService.GetBySSCID(SSC_ID);
            SeedAnalysisReport sar = seedAnalysisReportService.GetByLabTestNo(dec.Id);// .GetByLabTestNo(LabTestNo);
            if (sar != null)
            {
                lblCheckedBy.Text = userMasterService.GetById(Convert.ToInt16(sar.TestedBy)).Name;
                lblPreparedBy.Text = sar.PreparedBy;
                lblCheckedDate.Text = sar.TestedDate.Value.ToString("dd/MM/yyyy");
                lblRemarks.Text = sar.Remarks;
                lblTestNo.Text =dec.Id.ToString();
                lblobjectionable.Text = sar.ObjectionableWeedSeed.ToString();
                lblodv.Text = sar.ODV.ToString();
                lblOtherCrop.Text = sar.OtherCropSeeds.ToString();
                lblpureseed.Text = sar.PureSeed.ToString();
                lblSeedMoisture.Text = sar.SeedMoisture.ToString();
                lbltotalweed.Text = sar.TotalWeedSeed.ToString();
                lblDamage.Text = sar.InsectDamage.ToString();
                lblDiseases.Text = sar.SeedBorneDiseases.ToString();
                lblFreshungerminated.Text = sar.FreshUngerminatedSeed.ToString();
                lblHardSeed.Text = sar.HardSeed.ToString();
                lblHuskless.Text = sar.HuskLessSeed.ToString();
                lblInert.Text = sar.InertMatter.ToString();
                lblgermination.Text = sar.Germination.ToString();
                lblNumberOfLabelReq.Text = sar.NoOfLabels.ToString();
                if (sar.RequisitionOfTags != 0 && sar.AllotmentsOfTags != 0)
                {
                    TagDatails.Visible = true;
                    lblRequisitionOfTags.Text = sar.RequisitionOfTags.ToString();
                    lblAllotmentsOfTags.Text = sar.AllotmentsOfTags.ToString();
                }
                lblresult.Text = sar.Result == 1 ? "Pass" : "Fail. (Tests Failed :" + testMasterService.TestsRequired(sar.Failed_Tests) + ")";
            }
            SeedSampleCoupon ssc = seedSampleCouponService.GetById(dec.SSC_Id);
            Form1Details f1 = formOneService.ReadById(ssc.Form1Id);
            if (f1 != null)
            {
                if (f1.StatusCode == 1)
                {
                    FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                    SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                    RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                    FieldInspection fi = fieldInspectionService.GetFinalInspection(f1.Id);
                    if (fi != null)
                    {
                        lblForm1NO.Text = f1.Id.ToString();
                        lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
                        lblGrowerName.Text = fardet.Name + ",District : " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                            ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                            ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                            ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                            ",Mobile No : " + fardet.MobileNo;
                        lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
                        lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                        lblclass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                        lblLotno.Text = ssc.LotNo;
                        lblquantity.Text = ssc.Quantity.ToString();
                        gvstatus.DataSource = seedAnalysisReportTransactionService.GetAllTransactionsBy(sar.Id);
                        gvstatus.DataBind();
                        SeedAnalysisReportTransaction transaction = new SeedAnalysisReportTransaction();
                        transaction = seedAnalysisReportTransactionService.GetMaxTransactionDetails(sar.Id);
                        if (transaction != null)
                        {
                            int ActionRole_Id = transaction.ToID;
                            if (userMaster.Role_Id == 5)
                            {
                                pnlRemarks.Visible = false;
                                if (transaction.TransactionStatus == 0)
                                {
                                    //  btnEditApplication.Visible = true;
                                    //  btnEditApplication.PostBackUrl = "~/Transactions/SourceVerificationCreate.aspx?Id=" + sv.Id;
                                }
                                if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                                {
                                    this.Redirect("Unauthorized.aspx");
                                }
                            }
                            else
                            {
                                if (userMaster.Id == ActionRole_Id)
                                {
                                    if (sar.PaymentStatus == 1)
                                    {
                                        pnlRemarks.Visible = true;
                                        if (userMaster.Role_Id == 6)
                                        {
                                            pnltags.Visible = true;
                                        }
                                        else
                                        {
                                            pnltags.Visible = false;
                                        }
                                    }
                                    else
                                    {
                                        lblMessage.Text = "Payments not Updated";
                                    }
                                }
                                else
                                {
                                    pnlRemarks.Visible = false;
                                }
                            }
                        }
                        
                    }
                }
                var user = userMasterService.GetFromAuthCookie();
                if (user.Role_Id == 7)
                {
                    divForm1Details.Visible = false;
                }
                else { divForm1Details.Visible = true; }
            }
        }
    }
}