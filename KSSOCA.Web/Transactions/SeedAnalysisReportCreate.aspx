﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeedAnalysisReportCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.SeedAnalysisReportCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Seed Analysis Report"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
               
                <br />

                <div id="divReport" runat="server">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td colspan="4">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">Seed Analysis Report                      
                            </td>
                        </tr>
                        <tr>
                            <td>Lab Test No./Decoding No.</td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="txttestno" placeholder=" Enter Lab Test No./Decoding No." CssClass="form-control" TextMode="Number" onkeypress="return CheckNumber(event);" Enabled="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">1.Physical Purity
                            </td>
                        </tr>
                        <tr>
                            <td>a.Pure Seed</td>
                            <td>
                                <asp:TextBox ID="txtpureseed" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                            <td>b.Inert Matter</td>
                            <td>
                                <asp:TextBox ID="txtInert" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>c.Other Crop Seeds </td>
                            <td>
                                <asp:TextBox ID="txtOtherCrop" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                            <td>d.Huskless Seed</td>
                            <td>
                                <asp:TextBox ID="txtHuskless" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>e.ODV</td>
                            <td>
                                <asp:TextBox ID="txtodv" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                            <td>f.Total Weed Seeds</td>
                            <td>
                                <asp:TextBox ID="txttotalweed" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>g.Objectionable weed Seeds</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtobjectionable" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>

                        </tr>
                        <tr>
                            <td>2.Germination
                            </td>
                            <td>
                                <asp:TextBox ID="txtgermination" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>

                            <td>a.Hard Seed
                            </td>
                            <td>
                                <asp:TextBox ID="txtHardSeed" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>b.Fresh ungerminated seed</td>
                            <td>
                                <asp:TextBox ID="txtFreshungerminated" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>

                            <td>3.Seed Moisture</td>
                            <td>
                                <asp:TextBox ID="txtSeedMoisture" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>4.Seed borne Diseases</td>
                            <td>
                                <asp:TextBox ID="txtDiseases" runat="server" Text="0.00" CssClass="form-control" TextMode="Number" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>

                            <td>5.Insect Damage</td>
                            <td>
                                <asp:TextBox ID="txtDamage" runat="server" Text="0.00" CssClass="form-control" onkeypress="return CheckDecimal(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>6.Remarks</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" CssClass="form-control" runat="server"></asp:TextBox>

                            </td>
                            <td>7.Prepared By</td>
                            <td>
                                <asp:TextBox ID="txtPreparedBy" CssClass="form-control" runat="server"></asp:TextBox>

                            </td>
                        </tr>
                      
                        <tr> 
                            <td>Result :</td>
                            <td>
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbResult" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbResult_SelectedIndexChanged">
                                    <asp:ListItem class="radio-inline" Value="1" Text="Pass" Selected="true"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="0" Text="Fail"></asp:ListItem>
                                </asp:RadioButtonList></td>
                            <asp:Panel ID="pnlTest" runat="server" Visible="false">
                                <td>18.Tests Failed
                                </td>
                                <td>
                                    <asp:CheckBoxList ID="chkTests" runat="server" RepeatLayout="Flow" class="radio-inline" RepeatDirection="Vertical" Font-Bold="true"></asp:CheckBoxList>
                                </td>
                            </asp:Panel>
                            
                        </tr>
                        <tr>
                             <td>8.Number of tags required<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox ID="txtNumberOfLabelsRequired" runat="server" CssClass="form-control" onkeypress="return CheckDecimal(event);"></asp:TextBox>
                                 <asp:RequiredFieldValidator runat="server" ID="txtNumberOfLabelsRequiredRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtNumberOfLabelsRequired" ErrorMessage="Number of labels required"></asp:RequiredFieldValidator>

                            </td>
                            <td>9.Checked By</td>
                            <td>
                                <asp:Label ID="lblCheckedBy" runat="server" Font-Bold="true"></asp:Label>

                            </td>
                           
                        </tr>
                        <tr>
                             <td>10.Checked Date
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblCheckedDate" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                    </table>
                    <br />
                    <div>
                        <div align="center">
                            <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                ID="Save" Text="Save" OnClick="Save_Click" />
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div id="divGridview" runat="server">
                <asp:GridView ID="gvReports" runat="server" CssClass="table table-condensed" AutoGenerateColumns="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
