﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Configuration;
    using System.Data.SqlClient;

    public partial class SeedAnalysisReportCreate : SecurePage
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        SeedAnalysisReportService seedAnalysisReportService;
        TestMasterService testMasterService;
        DecodingService decodingService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        SeedAnalysisReportTransactionService seedAnalysisReportTransactionService;
        public SeedAnalysisReportCreate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            seedAnalysisReportService = new SeedAnalysisReportService();
            testMasterService = new TestMasterService();
            decodingService = new DecodingService();
            userwiseDistrictRightsService= new UserwiseDistrictRightsService();
            seedAnalysisReportTransactionService = new SeedAnalysisReportTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Id"] != null)
            {
                int LabTestNo = Convert.ToInt16(Request.QueryString["Id"]);
                Session["LabTestNo"] = LabTestNo;
                txttestno.Text = LabTestNo.ToString();
                lblCheckedBy.Text = userMasterService.GetFromAuthCookie().Name;
                lblCheckedDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                lblMessage.Text = "";
                lblHeading.Text = common.getHeading("SAR"); litNote.Text = common.getNote("SAR"); 
            }
            else
            { //redirect 
            }
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int status = 1;
                int LabTestNumber = Convert.ToInt16(Request.QueryString["Id"]);
                var decodingDetails = decodingService.GetById(LabTestNumber);
                var seedSampleCuoponDetails = seedSampleCouponService.GetById(decodingDetails.SSC_Id);
                Form1Details form1 = formOneService.ReadById(seedSampleCuoponDetails.Form1Id);
                var userDetails = userMasterService.GetById(form1.Producer_Id);
             
                if (txttestno.Text != "")
                {
                    string FailedTests = ""; 
                    if (rbResult.SelectedValue.ToInt() == 1)
                    {
                        FailedTests = "";
                    }
                    else if (rbResult.SelectedValue.ToInt() == 0)
                    { FailedTests = common.getCheckedItems(chkTests); }

                    var sar = new SeedAnalysisReport
                    {
                        FreshUngerminatedSeed = txtFreshungerminated.Text.ToDecimal(),
                        Germination = txtgermination.Text.ToDecimal(),
                        HardSeed = txtHardSeed.Text.ToDecimal(),
                        HuskLessSeed = txtHuskless.Text.ToDecimal(),
                        InertMatter = txtInert.Text.ToDecimal(),
                        InsectDamage = txtDamage.Text.ToDecimal(),
                        LabTestNo = txttestno.Text.ToInt(),
                        ObjectionableWeedSeed = txtobjectionable.Text.ToDecimal(),
                        ODV = txtodv.Text.ToDecimal(),
                        OtherCropSeeds = txtOtherCrop.Text.ToDecimal(),
                        PreparedBy = txtPreparedBy.Text,
                        PureSeed = txtpureseed.Text.ToDecimal(),
                        Remarks = txtRemarks.Text,
                        SeedBorneDiseases = txtSeedMoisture.Text.ToDecimal(),
                        SeedMoisture = txtSeedMoisture.Text.ToDecimal(),
                        TestedBy = userMasterService.GetFromAuthCookie().Id,
                        TestedDate = System.DateTime.Now,
                        TotalWeedSeed = txttotalweed.Text.ToDecimal(),
                        Result = rbResult.SelectedValue.ToInt(),
                        Failed_Tests = FailedTests,
                        Status = 0,
                        PaymentStatus=0,
                        NoOfLabels= Convert.ToInt32(txtNumberOfLabelsRequired.Text)

                    };
                    var result = seedAnalysisReportService.Create(sar);
                    var reportId = seedAnalysisReportService.GetByLabTestNo(txttestno.Text.ToInt());
                    int tranactionId = common.GenerateID("SeedAnalysisReportTransaction");
                    var currentUser = userMasterService.GetFromAuthCookie();
                    int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                        (Convert.ToInt16(userDetails.District_Id),
                                         Convert.ToInt16(userDetails.Taluk_Id), 4).User_Id);
                    int TransactionStatus = 4;
                    if (result > 0)
                    {
                        if (rbResult.SelectedValue.ToInt() == 1)
                        {
                            Messaging.SendSMS("STL Test passed for Form1 Number -" + seedSampleCuoponDetails.Form1Id + "", userDetails.MobileNo, "1");
                            var sareport = new SeedAnalysisReportTransaction
                            {
                                Id = tranactionId,
                                FromID = currentUser.Id,
                                ToID = ToID,
                                ReportID = reportId.Id,
                                TransactionDate = System.DateTime.Now,
                                TransactionStatus = TransactionStatus,
                                Remarks = "",
                                IsCurrentAction = 1
                            };
                            var resultTransaction = seedAnalysisReportTransactionService.Create(sareport);
                            if (resultTransaction == 1)
                            {
                                objsqlconn.Open();
                                string updateTestingPayment = "Update TestingPayment set Enable ='" + status + "' where Form1Id= " + seedSampleCuoponDetails.Form1Id;
                                SqlCommand cmdUpdateTestingPayment = new SqlCommand(updateTestingPayment, objsqlconn);
                                cmdUpdateTestingPayment.ExecuteNonQuery();
                                objsqlconn.Close();
                                lblMessage.ForeColor = System.Drawing.Color.Green;
                                lblMessage.Text = "Seed Analysis Report details saved successfully ";

                                txttestno.Text = "";

                                this.Redirect("Transactions/SeedAnalysisReportView.aspx");
                            }
                        }
                        else if (rbResult.SelectedValue.ToInt() == 0)
                        {
                           
                            Messaging.SendSMS("STL Test Failed  " + chkTests.SelectedValue + " For Form1 Number -" + seedSampleCuoponDetails.Form1Id + "", userDetails.MobileNo, "1");
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Seed Analysis Report details saved successfully ";
                            this.Redirect("Transactions/SeedAnalysisReportView.aspx");
                        }
                        
                    }
                }
                else { lblMessage.Text = "Please enter lab test number."; }
            }
            else
            {
                lblMessage.Text = "Please fill all the details.";
            }
        }       

        protected void rbResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rbResult.SelectedValue == "1")
            { pnlTest.Visible = false; chkTests.Items.Clear(); }
            else if (rbResult.SelectedValue == "0")
            {
                pnlTest.Visible = true;
                
                chkTests.BindCheckBoxItem(testMasterService.GetAll());
            }
        }
    }
}