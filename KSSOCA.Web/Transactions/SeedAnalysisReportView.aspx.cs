﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SeedAnalysisReportView : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        Common common = new Common();
        DecodingService decodingService;
        TestMasterService testMasterService;
        SeedSampleCouponService seedSampleCouponService;
        public SeedAnalysisReportView()
        {
            userMasterService = new UserMasterService();
            decodingService = new DecodingService();
            testMasterService = new TestMasterService();
            seedSampleCouponService = new SeedSampleCouponService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitComponent();
            }
        }
        private void InitComponent()
        {
            var user = userMasterService.GetFromAuthCookie();
            if (user.Role_Id == 11)
            {
                divDecoding.Visible = true;
                lblHeading.Text = "Sample Coding/Decoding";
            }
            else if (user.Role_Id == 7)
            {
                divDecoding.Visible = false;
                lblHeading.Text = "Sample Sent for Testing";
            }
            else
            {
                lblHeading.Text = "Seed Analysis Report";
                divDecoding.Visible = false;
            }
            DecodingGridView.DataSource = SelectMethod();
            DecodingGridView.DataBind();
        }

        protected void btngenerateLabNo_Click(object sender, EventArgs e)
        {
            try
            {
                if (seedSampleCouponService.IsValidCoupan(txtsscNo.Text.ToInt(), "STL"))
                {
                    if (!decodingService.IsSSCIdExists(txtsscNo.Text.ToInt()))
                    {

                        var sscid=txtsscNo.Text.ToInt();
                        SeedSampleCoupon sscDetailsStl = seedSampleCouponService.GetById(sscid);
                         
                        if (sscDetailsStl.IsRecieved == 1)
                        {
                            var user = userMasterService.GetFromAuthCookie();
                            var decoding = new Decoding
                            {
                                DecodedBy = user.Id,
                                DecodedDate = System.DateTime.Now,
                                SSC_Id = sscid,
                                Sent_To = Convert.ToInt16(user.Reporting_Id),
                            };
                            int recordCount = decodingService.Create(decoding);
                            int tranactionSTL = common.GenerateID("Decoding") - 1;
                            objsqlconn.Open();
                            string updateTestingPayment = "Update TestingPayment set STLTestID='" + tranactionSTL + "' where Form1Id= " + sscDetailsStl.Form1Id;
                            SqlCommand cmdTestingPayment = new SqlCommand(updateTestingPayment, objsqlconn);
                            cmdTestingPayment.ExecuteNonQuery();
                            objsqlconn.Close();
                            if (recordCount > 0)
                            {
                                lblMessage.Text = "Lab Test No generated successfully.";
                                txtsscNo.Text = "";
                                DecodingGridView.DataSource = SelectMethod();
                                DecodingGridView.DataBind();
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Please get recieval update from producer";
                        }
                    }
                    else { lblMessage.Text = "This Coupon Number is already encoded.Enter valid Coupon Number."; }
                }
                else { lblMessage.Text = "This Coupon Number does not exist or not a STL Coupon.Enter valid Coupon Number."; }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator";
            }
        }
        public DataTable SelectMethod()
        {
            var data = (DataTable)null;
            var user = userMasterService.GetFromAuthCookie();
            data = decodingService.ReadByUser(Convert.ToInt16(user.Role_Id), user.Id, "STL");//  
            if (data != null)
            {
                return data;
            }
            else
                lblMessage.Text = "Records not found.";
            return (DataTable)null;
        }

        protected void DecodingGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var user = userMasterService.GetFromAuthCookie();
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                HiddenField hdnTestsRequired = (e.Row.FindControl("hdnTestsRequired") as HiddenField);
                Label lblTestsRequired = (e.Row.FindControl("lblTestsRequired") as Label);
                LinkButton lnkCreate = (e.Row.FindControl("lnkCreate") as LinkButton);
                LinkButton lnkView = (e.Row.FindControl("lnkView") as LinkButton);
                if (user.Role_Id == 7)
                {
                    DecodingGridView.Columns[1].Visible = false;
                    if (hdnStatusCode.Value == "1")
                    { lblStatus.ForeColor = System.Drawing.Color.Green; lblStatus.Text = "Pass"; lnkView.Visible = true; lnkCreate.Visible = false; }
                    else if (hdnStatusCode.Value == "0")
                    { lblStatus.ForeColor = System.Drawing.Color.Red; lblStatus.Text = "Fail"; lnkView.Visible = true; lnkCreate.Visible = false; }
                    else
                    { lblStatus.ForeColor = System.Drawing.Color.BurlyWood; lblStatus.Text = "Pending"; lnkView.Visible = false; lnkCreate.Visible = true; }
                }
                else
                {
                    lnkCreate.Visible = false;
                    if (hdnStatusCode.Value == "1")
                    { lblStatus.ForeColor = System.Drawing.Color.Green; lblStatus.Text = "Pass"; lnkView.Visible = true; }
                    else if (hdnStatusCode.Value == "0")
                    { lblStatus.ForeColor = System.Drawing.Color.Red; lblStatus.Text = "Fail"; lnkView.Visible = true; }
                    else
                    { lblStatus.ForeColor = System.Drawing.Color.BurlyWood; lblStatus.Text = "Pending"; lnkView.Visible = false; }
                }

                lblTestsRequired.Text = testMasterService.TestsRequired(hdnTestsRequired.Value);
            }
        }
        protected void SeedAnalysisReportGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DecodingGridView.PageIndex = e.NewPageIndex;
            DecodingGridView.DataSource = SelectMethod();
            DecodingGridView.DataBind();
        }
    }
}