﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeedSampleCouponApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.SeedSampleCouponApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlCoupan.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
        function PrintPanelCoupon() {
            var panel = document.getElementById("<%=upnl.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>

                <br />
                <div id="divCoupan" runat="server" visible="false">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td colspan="4">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">Seed Sapmling Information                       
                            </td>
                        </tr>
                        <tr>
                            <td width="25%">1.Sampling Center  </td>
                            <td width="25%">
                                <asp:Label ID="lblSamplingCenter" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td width="25%">2.SPU No  </td>
                            <td width="25%">
                                <asp:Label ID="lblSPUNo" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>3.Name of the seed producer
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblproducerName" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>4.Name & Address of seed grower:
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblGrowerName" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>5.Form-I Registration No.
                            </td>
                            <td>
                                <asp:Label ID="lblForm1NO" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>6.Crop
                            </td>
                            <td>
                                <asp:Label ID="lblCrop" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>7.Variety
                            </td>
                            <td>
                                <asp:Label ID="lblVariety" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>8.Class of seed
                            </td>
                            <td>
                                <asp:Label ID="lblClass" runat="server" Font-Bold="true"></asp:Label>
                            </td>

                        </tr>
                        <tr>
                            <td>9.Final Field Inspection No and Date
                            </td>
                            <td>
                                <asp:Label ID="lblFieldInspection" runat="server" Font-Bold="true"></asp:Label>
                            </td>

                            <td>10.Area Accepted(in acres)
                            </td>
                            <td>
                                <asp:Label ID="lblAcceptedArea" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>11.Expected Yield(in Qntls)
                            </td>
                            <td>
                                <asp:Label ID="lblExpectedYield" runat="server" Font-Bold="true"></asp:Label>
                            </td>

                            <td>12.Lot No.
                            </td>
                            <td>

                                <asp:Label ID="lbllotno" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>13.Quantity (in Qntls)
                            </td>
                            <td>
                                <asp:Label ID="lblQuantity" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>14.Processed/Unprocessed</td>
                            <td>
                                <asp:Label ID="lblProcessed" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>15.Seed Treatment
                            </td>
                            <td>
                                <asp:Label ID="lbltreatment" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>16.Name of the chemical
                            </td>
                            <td>
                                <asp:Label ID="lblchemical" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>17.Packing size
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblpackingsize" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>18.Tests Required
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lblTests" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>19. Sampled By
                            </td>
                            <td>
                                <asp:Label ID="lblSampledBy" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>20.Sampled Date
                            </td>
                            <td>
                                <asp:Label ID="lblSampledDate" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                      <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                        <tr>
                            <td>Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="80%" Height="100px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ErrorMessage="Give Remarks" Enabled="false" ValidationGroup="v1"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                          <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1" CausesValidation="true"
                                        ID="Save" Text="Approve Seed Sample Coupon" OnClick="Save_Click" />
                                </td>
                            </tr>
                        </asp:Panel>
                         
                        <asp:Panel ID="pnlreje" runat="server" Visible="false">
                            <tr>
                                <td>Rejection Type</td>
                                <td colspan="3">
                                    <asp:RadioButtonList ID="rbRejectionType" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Reject Application&nbsp;&nbsp;" Value="A" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Reject Payment" Value="P">
                                        </asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </asp:Panel>
                        
                    </asp:Panel>
                    <tr class="SideHeading">
                        <td colspan="4">8.Application Status &nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Note: Sl.No. 1 is the current status of the application.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class=" table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Transaction Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Transaction Date" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                             
                         
                            <%--<input type="submit" value="Pay" onclick="launchBOLT(); return false;" />--%>
                                
                        </td>
                    </tr>
                    </table>


                </div>
                <br />
                <asp:Panel ID="pnlCoupan" runat="server" Visible="false">
                    <table class="table-condensed" width="50%" border="1" borderstyle="Dashed">
                        <tr class="Note">
                            <td colspan="2">Seed Sampling Coupon (only this information to be sent to Labs.) 
                            </td>
                        </tr>
                        <tr>
                            <td>Seed Sampling Coupon No.</td>
                            <td>
                                <asp:Label ID="LabelSSCNO" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Seed Sampling Date</td>
                            <td>
                                <asp:Label ID="LabelDAte" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Crop</td>
                            <td>
                                <asp:Label ID="LabelCrop" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Variety</td>
                            <td>
                                <asp:Label ID="LabelVAriety" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Class</td>
                            <td>
                                <asp:Label ID="LabelClass" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Test Required</td>
                            <td>
                                <asp:Label ID="LabelTestRequired" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                        
                    </table>
                </asp:Panel>

                <br />
                <asp:Button ID="btnprintcoupon" runat="server" Text="Print Coupon" CssClass="btn-primary" OnClientClick="return PrintPanelCoupon();" Visible="false" />
                <asp:Button ID="btnPrint" runat="server" Text="Print Coupon" CssClass="btn-primary" OnClientClick="return PrintPanel();" />
            </div>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
