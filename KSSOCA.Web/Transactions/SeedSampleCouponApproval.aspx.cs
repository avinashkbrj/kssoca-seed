﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SeedSampleCouponApproval : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlDataReader dataReader;
        SqlConnection objsqlconn = new SqlConnection(conn);
        string connectionString = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        TestMasterService testMasterService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        public SeedSampleCouponApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            testMasterService = new TestMasterService();
            userwiseDistrictRightsService= new UserwiseDistrictRightsService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                lblHeading.Text = common.getHeading("SSC"); litNote.Text = common.getNote("SSC");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int SSCID = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["SSCID"] = SSCID;
                    SeedSampleCoupon sscoupon = seedSampleCouponService.GetById(SSCID);
                    int actionid = GetSSCTransaction(sscoupon); 
                   InitComponent(SSCID); 
                }
                else
                {
                    // this.Redirect("Dashboard.aspx");
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(int SSCID)
        {
            SeedSampleCoupon ssc = seedSampleCouponService.GetById(SSCID);
            Form1Details f1 = formOneService.ReadById(ssc.Form1Id);
            var userMaster = userMasterService.GetFromAuthCookie();
            if (f1 != null)
            {
                FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                FieldInspection fi = fieldInspectionService.GetFinalInspection(ssc.Form1Id);
                if (fi != null)
                {
                    lblSamplingCenter.Text = sPURegistrationService.GetById(Convert.ToInt16(f1.SPU_Id)).Name;
                    lblSPUNo.Text = sPURegistrationService.GetById(Convert.ToInt16(f1.SPU_Id)).SPUNO.ToString();
                    lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
                    lblGrowerName.Text = fardet.Name + ",District : " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                        ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                        ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                        ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                        ",Mobile No : " + fardet.MobileNo;
                    lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
                    lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                    lblClass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                    lblForm1NO.Text = f1.Id.ToString();
                    lblFieldInspection.Text = "FIR No:" + fi.Id.ToString() + " , Date :" + fi.InspectedDate.Value.ToString("dd/MM/yyyy");
                    lblAcceptedArea.Text = fi.AcceptedArea.ToString();
                    lblExpectedYield.Text = fi.EstimatedYield.ToString();

                    lblTests.Text = testMasterService.TestsRequired(ssc.TestsRequired);
                    if (ssc != null)
                    {
                        lbllotno.Text = ssc.LotNo;
                        lblQuantity.Text = ssc.Quantity.ToString();
                        lbltreatment.Text = ssc.SeedTreatment;
                        lblProcessed.Text = ssc.ProcessedOrNot;
                        lblchemical.Text = ssc.Chemical;
                        lblSampledDate.Text = ssc.SampledDate.ToString("dd/MM/yyyy");
                        lblSampledBy.Text = userMasterService.GetById(ssc.SampledBy).Name;
                        lblpackingsize.Text = ssc.PackingSize.ToString();

                        LabelSSCNO.Text = ssc.Id.ToString();
                        LabelDAte.Text = ssc.SampledDate.ToString("dd/MM/yyyy");
                        LabelCrop.Text = lblCrop.Text;
                        LabelVAriety.Text = lblVariety.Text;
                        LabelClass.Text = lblClass.Text;
                        LabelTestRequired.Text = lblTests.Text;
                        if (userMasterService.GetFromAuthCookie().Role_Id == 6)
                        { pnlCoupan.Visible = true; }
                        using (SqlConnection con = new SqlConnection(connectionString))
                        {
                            String tranactionQuery = @"SELECT um1.Name FromID, um2.Name as ToId, tsm.Status as TransactionStatus, [TransactionDate],[Remarks]  
                            FROM  SeedSampleCouponTransaction  as ssct
                            inner join  UserMaster um1 on ssct.FromID = um1.Id 
                            inner join UserMaster um2 on ssct.ToID = um2.Id
                            inner join TransactionStatusMaster tsm on tsm.Id = ssct.TransactionStatus
                            where ssct.SSCId = '" + ssc.Id + "' order by ssct.Id desc";
                            SqlCommand transactionCmd = new SqlCommand(tranactionQuery, con);
                            con.Open();
                            gvstatus.DataSource = transactionCmd.ExecuteReader();
                            gvstatus.DataBind();
                            con.Close();
                        }
                    }
                    else
                    {

                        lblSampledBy.Text = userMasterService.GetFromAuthCookie().Name;
                        lblSampledDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    }
                    divCoupan.Visible = true;
                    btnprintcoupon.Visible = true;
                    btnPrint.Visible = false;
                    int actionid = GetSSCTransaction(ssc);
                    if (userMaster.Role_Id == 5)
                    {
                        int sscRecievedGot=0;
                        int sscRecievedGotId=0;
                        var sscRecievedData = seedSampleCouponService.GetBy(f1.Id);
                        int sscRecievedStl = sscRecievedData[0].IsRecieved;
                        int sscRecievedStlId = sscRecievedData[0].Id;
                        if (sscRecievedData.Count > 1)
                        {
                             sscRecievedGot = sscRecievedData[1].IsRecieved;
                             sscRecievedGotId = sscRecievedData[1].Id;
                        }
                        //if ((sscRecievedStl == 1 && sscRecievedGot == 1) || (sscRecievedStl == 1 && sscRecievedGot == 0))
                        if (sscRecievedStl == 1 && sscRecievedGot == 1)
                        {

                            pnlRemarks.Visible = false;

                            if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                            {
                                this.Redirect("Unauthorized.aspx");
                            }
                        }
                        else
                        {
                            if ((sscRecievedStlId == SSCID && sscRecievedStl != 1) || (sscRecievedGotId == SSCID & sscRecievedGot != 1))
                            {
                                pnlRemarks.Visible = true;
                                Save.Text = "Seed Sample Coupon Recieved";
                            }
                            else
                            {
                                pnlRemarks.Visible = false;
                                
                            }
                            
                        }
                       
                    }
                    else
                    {
                        if (userMaster.Id == actionid)
                        {
                            pnlRemarks.Visible = true;
                            btnprintcoupon.Visible = false;
                        }
                        else
                        {
                            pnlRemarks.Visible = false;
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Field inspection not finalised for this form no.";
                    divCoupan.Visible = false;
                    btnprintcoupon.Visible = false;
                }

            }
            else
            {
                lblMessage.Text = "Enter valid form no.";
                divCoupan.Visible = false;
                btnprintcoupon.Visible = false;
            }

        }

        private int GetSSCTransaction(SeedSampleCoupon ssc)
        {
            int actionid = 0;
            using (SqlConnection con = new SqlConnection(conn))
            {
                String maxId = @"SELECT top 1 * FROM SeedSampleCouponTransaction where 
                                     Form1Id = '" + ssc.Form1Id + "' and SSCId = '" + ssc.Id + "' order by Id  desc";
                SqlCommand maxTransactionCmd = new SqlCommand(maxId, con);
                con.Open();
                dataReader = maxTransactionCmd.ExecuteReader();
                dataReader.Read();
                actionid = Convert.ToInt32(dataReader["ToID"]);


            }

            return actionid;
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                int SscId = Convert.ToInt16(Request.QueryString["Id"]);
                SeedSampleCoupon sscDetails = seedSampleCouponService.GetById(SscId);
                var userId = userMasterService.GetFromAuthCookie();


                int isCurrentAction = 1;
                int statusCode = 5;
                Form1Details form1Details = formOneService.ReadById(sscDetails.Form1Id);
                UserMaster umDetails = userMasterService.GetById(form1Details.Producer_Id);
                if (Save.Text == "Seed Sample Coupon Recieved")
                {
                    objsqlconn.Open();
                    string querySSCTransaction = "Insert into SeedSampleCouponTransaction(Form1Id,SSCId,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction) Values(@Form1Id,@SSCId,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction)";
                    SqlCommand cmdSeedSSCTransaction = new SqlCommand(querySSCTransaction, objsqlconn);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@Form1Id", form1Details.Id);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@SSCId", sscDetails.Id);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@FromID", userId.Id);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@ToID", userId.Id);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@TransactionStatus", statusCode);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@Remarks", txtRemarks.Text);
                    cmdSeedSSCTransaction.Parameters.AddWithValue("@IsCurrentAction", isCurrentAction);
                    int result = cmdSeedSSCTransaction.ExecuteNonQuery();
                    UpdateRecieveSSC(objsqlconn, sscDetails.Id);
                    objsqlconn.Close();
                   
                    this.Redirect("/Transactions/SeedSampleCouponView.aspx");
                }
                else
                {
                   

                    int IsCurrentUserSet = common.setIsCurrentAction("SeedSampleCouponTransaction", "SSCID", SscId);
                    if (IsCurrentUserSet > 0)
                    {
                        objsqlconn.Open();
                        string querySeedSampleCouponTransaction = "Insert into SeedSampleCouponTransaction(Form1Id,SSCId,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction) Values(@Form1Id,@SSCId,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction)";
                        SqlCommand cmdSeedSampleCouponTransaction = new SqlCommand(querySeedSampleCouponTransaction, objsqlconn);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@Form1Id", form1Details.Id);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@SSCId", sscDetails.Id);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@FromID", userId.Id);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@ToID", form1Details.Producer_Id);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@TransactionStatus", statusCode);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@Remarks", txtRemarks.Text);
                        cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@IsCurrentAction", isCurrentAction);
                        int result = cmdSeedSampleCouponTransaction.ExecuteNonQuery();

                        UpdateSSC(objsqlconn, sscDetails.Id); 
                        objsqlconn.Close();
                        SeedSampleCoupon ssc = seedSampleCouponService.GetById(SscId);
                        Form1Details form1 = formOneService.ReadById(ssc.Form1Id);
                        FarmerDetails farmerdetails = farmerDetailsService.GetById(form1.FarmerID);
                        var userDetails = userMasterService.GetById(form1.Producer_Id);
                        var adsc = userMasterService.GetFromAuthCookie().Name;
                        Messaging.SendSMS("Seed Sample Coupon Approved for Farmer -" + farmerdetails.Name + " Crop " + lblCrop.Text + " Variety " + lblVariety.Text + " for Form1 Number-" + lblForm1NO.Text + " By " + adsc + ".", userDetails.MobileNo, "1");

                    }


                    this.Redirect("/Transactions/SeedSampleCouponView.aspx");
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        private static void UpdateSSC(SqlConnection objsqlconn, int sscid)
        {
            int approveStatus = 1;
            string approvalUpdate = "Update SeedSampleCoupon set IsApproved='" + approveStatus + "'  where Id =" + sscid;
            SqlCommand cmdapprovalUpdate = new SqlCommand(approvalUpdate, objsqlconn);
            cmdapprovalUpdate.ExecuteNonQuery();
        }
        private static void UpdateRecieveSSC(SqlConnection objsqlconn, int sscid)
        {
            int recievesStatus = 1;
            string approvalUpdate = "Update SeedSampleCoupon set IsRecieved='" + recievesStatus + "'  where Id =" + sscid;
            SqlCommand cmdapprovalUpdate = new SqlCommand(approvalUpdate, objsqlconn);
            cmdapprovalUpdate.ExecuteNonQuery();
        }
    }
}