﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Data;
    using KSSOCA.Core.Helper;
    using System.Web.UI.WebControls;
    using System.Collections.Generic;
    using System.Security.Permissions;
    using System.Web.UI.HtmlControls;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SeedSampleCouponCreate1 : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection con = new SqlConnection(conn);  
        SqlDataReader dataReader;
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        FieldInspectionService fieldInspectionService;
        FarmerDetailsService farmerDetailsService;
        SeedSampleCouponService seedSampleCouponService;
        SPURegistrationService sPURegistrationService;
        TestMasterService testMasterService;
        Common common = new Common();
        BulkProcessingEntryService bulkProcessingEntryService;
        public SeedSampleCouponCreate1()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            farmerDetailsService = new FarmerDetailsService();
            seedSampleCouponService = new SeedSampleCouponService();
            sPURegistrationService = new SPURegistrationService();
            testMasterService = new TestMasterService();
            bulkProcessingEntryService = new BulkProcessingEntryService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("SSC");
                litNote.Text = common.getNote("SSC"); // Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    if (!IsPostBack)
                    {
                        int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                        Session["Form1ID"] = Form1ID;
                        InitComponent(Form1ID);
                    }
                }
                else
                {
                    int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["Form1ID"] = Form1ID;
                    InitComponent(Form1ID);
                    /// this.Redirect("Dashboard.aspx");
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent(int Form1ID)
        {
            Form1Details f1 = formOneService.ReadById(Form1ID);
            if (f1 != null)
            {
                FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                FieldInspection fi = fieldInspectionService.GetFinalInspection(Form1ID);
                if (fi != null)
                {
                    lblSamplingCenter.Text = sPURegistrationService.GetById(Convert.ToInt16(f1.SPU_Id)).Name;
                    lblSPUNo.Text = sPURegistrationService.GetById(Convert.ToInt16(f1.SPU_Id)).SPUNO.ToString();
                    lblproducerName.Text = userMasterService.GetById(f1.Producer_Id).Name;
                    lblGrowerName.Text = fardet.Name + ",District : " + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name +
                        ",Taluk : " + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name +
                        ",Hobli : " + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name +
                        ",Village : " + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name +
                        ",Mobile No : " + fardet.MobileNo;
                    lblCrop.Text = cropMasterService.GetById(sv.Crop_Id).Name;
                    lblVariety.Text = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                    lblClass.Text = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                    lblForm1NO.Text = f1.Id.ToString();
                    lblFieldInspection.Text = "FIR No:" + fi.Id.ToString() + " , Date :" + fi.InspectedDate.Value.ToString("dd/MM/yyyy");
                    lblAcceptedArea.Text = fi.AcceptedArea.ToString();
                    lblExpectedYield.Text = fi.EstimatedYield.ToString();
                    // lblLotSize.Text = "Germination,Physical Purity,Moisture,Seed Health,ODV,Vigour,Insect Damage" + (f1.GOTRequiredOrNot.ToString() == "1" ? ",Genetic Purity" : "");

                    lblLotSize.Text = cropMasterService.GetById(sv.Crop_Id).LotSize.ToString();
                    //getTests(rbTestType.SelectedValue.Trim());
                    var user = userMasterService.GetFromAuthCookie();
                    lblSampledBy.Text = user.Name;
                    lblSampledDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                    var pd = bulkProcessingEntryService.GetbyForm1ID(f1.Id);
                    txtQuantity.Text = (pd.CleanedSeed / 100).ToString(); // Convert to Quintal
                    txtQuantity.ReadOnly = txtlotno.ReadOnly = true;
                    txtlotno.Text = pd.LotNumber;

                }
                else
                {
                    lblMessage.Text = "Field inspection not finalised for this form no.";
                    //divCoupan.Visible = false;
                }

            }
            else
            {
                lblMessage.Text = "Enter valid form no.";
                //divCoupan.Visible = false;
            }
        }
        private void GenerateCoupans()
        {
            int TotalQuantity = Convert.ToInt32(txtQuantity.Text);
            int LotSize = Convert.ToInt32(lblLotSize.Text);
            int IsDecimal = TotalQuantity % LotSize;
            int Form1Id = Convert.ToInt16(lblForm1NO.Text.Trim());
            int NoOfLots = TotalQuantity / LotSize;
            int tranactionGOT = 0;
            int tranactionSTL = 0;
            if (IsDecimal > 0)
            {
                NoOfLots += 1;
            }
            string[] Lots = new string[NoOfLots];
            for (int i = 0; i < NoOfLots; i++)
            {
                if (i == 0)
                {
                    Lots[i] = txtlotno.Text.Trim();
                }
                else
                {
                    Lots[i] = txtlotno.Text.Trim() + "/" + i;
                }
            }
            int GOTRequiredOrNot = formOneService.ReadById(Convert.ToInt16(Session["Form1ID"])).GOTRequiredOrNot;
            int LeftQuantity = TotalQuantity;
            for (int i = 0; i < Lots.Length; i++)
            {
                int Quantity = 0;
                if (LeftQuantity > LotSize)
                {
                    Quantity = LotSize;
                }
                else
                {
                    Quantity = LeftQuantity;
                }
                string LotNo = Lots[i];
                var sscSTL = new SeedSampleCoupon
                {
                    Chemical = txtchemical.Text.Trim(),
                    Form1Id = Form1Id,
                    LotNo = LotNo,
                    PackingSize = txtpackingsize.Text.Trim().ToDecimal(),
                    ProcessedOrNot = rbProcessed.SelectedItem.Text.Trim(),
                    Quantity = Quantity,
                    SampledBy = userMasterService.GetFromAuthCookie().Id,
                    SampledDate = System.DateTime.Now,
                    SeedTreatment = rbtreatment.SelectedItem.Text.Trim(),
                    TestsRequired = "1",
                    TestType = "STL",

                }; var resultSTL = seedSampleCouponService.Create(sscSTL);
                tranactionSTL = common.GenerateID("SeedSampleCoupon") - 1;
                string testType = "STL";
                using (SqlConnection con = new SqlConnection(conn))
                {

                    String tranactionQuerySTL = @"select Id from SeedSampleCoupon where TestType='"+ testType + "' and  Form1Id='" + Convert.ToInt16(lblForm1NO.Text.Trim())+"'";
                    SqlCommand transactionCmdSTL = new SqlCommand(tranactionQuerySTL, con);
                    con.Open();
                    dataReader = transactionCmdSTL.ExecuteReader();
                    dataReader.Read();
                    int SSCIdSTL = Convert.ToInt32(dataReader["Id"]);
                    
                    con.Close();
                    SaveSSCTransaction(Form1Id, SSCIdSTL);
                }

                if (GOTRequiredOrNot == 1)
                {
                    var sscGOT = new SeedSampleCoupon
                    {
                        Chemical = txtchemical.Text.Trim(),
                        Form1Id = Form1Id,
                        LotNo = LotNo,
                        PackingSize = txtpackingsize.Text.Trim().ToDecimal(),
                        ProcessedOrNot = rbProcessed.SelectedItem.Text.Trim(),
                        Quantity = Quantity,
                        SampledBy = userMasterService.GetFromAuthCookie().Id,
                        SampledDate = System.DateTime.Now,
                        SeedTreatment = rbtreatment.SelectedItem.Text.Trim(),
                        TestsRequired = "5",
                        TestType = "GOT",

                    }; var resultGOT = seedSampleCouponService.Create(sscGOT);
                    tranactionGOT = common.GenerateID("SeedSampleCoupon") - 1;
                     
                    String tranactionQueryGOT = @"select Id from SeedSampleCoupon where TestType='GOT' and  Form1Id='" + Convert.ToInt16(lblForm1NO.Text.Trim()) + "'";
                    SqlCommand transactionCmdGOT = new SqlCommand(tranactionQueryGOT, con);
                    con.Open();
                    dataReader = transactionCmdGOT.ExecuteReader();
                    dataReader.Read();
                    int SSCIdGOT = Convert.ToInt32(dataReader["Id"]); 
                    con.Close();
                    SaveSSCTransaction(Form1Id, SSCIdGOT);
                    
                }

                con.Open();
                int enableStatus = 0;
                string queryGOTPayment = "Insert into TestingPayment(Form1Id,STLTestID,GOTTestID,PaymentStatus,Enable,GotRequiredOrNot) Values(@Form1Id,@STLTestID,@GOTTestID,@PaymentStatus,@Enable,@GotRequiredOrNot)";
                SqlCommand cmdGOTTransaction = new SqlCommand(queryGOTPayment, con);
                cmdGOTTransaction.Parameters.AddWithValue("@Form1Id", Form1Id);
                cmdGOTTransaction.Parameters.AddWithValue("@STLTestID", tranactionSTL);
                cmdGOTTransaction.Parameters.AddWithValue("@GOTTestID", tranactionGOT);
                cmdGOTTransaction.Parameters.AddWithValue("@PaymentStatus", 'N');
                cmdGOTTransaction.Parameters.AddWithValue("@Enable", enableStatus);
                cmdGOTTransaction.Parameters.AddWithValue("@GotRequiredOrNot", GOTRequiredOrNot);
                int resultPayment = cmdGOTTransaction.ExecuteNonQuery();
                con.Close();
                LeftQuantity = LeftQuantity - Quantity;
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    GenerateCoupans();
                    var from1id = Convert.ToInt16(Request.QueryString["Id"]);
                    Form1Details form1 = formOneService.ReadById(from1id);
                    FarmerDetails farmerdetails = farmerDetailsService.GetById(form1.FarmerID);
                    var userDetails = userMasterService.GetById(form1.Producer_Id);
                    var sco = userMasterService.GetFromAuthCookie().Name;
                   

                    bool isSent = Messaging.SendSMS("Seed Sample Coupon Created for Farmer -" + farmerdetails.Name + " Crop " + lblCrop.Text + " Variety " + lblVariety.Text + " for Form1 Number-" + lblForm1NO.Text + " By " + sco + ". Waiting for approval from ADSC", userDetails.MobileNo, "1");
                    if (isSent)
                    {
                        this.Redirect("Transactions/SeedSampleCouponView.aspx");
                    }
                }
                else
                {
                    lblMessage.Text = "Please save all the details.";
                }
            }
            catch (Exception ex)
            { lblMessage.Text = ex.Message; }
        }

        private void SaveSSCTransaction(int from1id, int sscId)
        {
            var scoId = userMasterService.GetFromAuthCookie();

            int tranactionId = common.GenerateID("SeedSampleCouponTransaction");
            int isCurrentAction = 1;
            int statusCode = 4;
            Form1Details form1Details = formOneService.ReadById(from1id);
            FarmerDetails fardet = farmerDetailsService.GetById(form1Details.FarmerID);
            int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                            (Convert.ToInt16(fardet.District_Id),
                                Convert.ToInt16(fardet.Taluk_Id), 4).User_Id);
            SqlConnection objsqlconn = new SqlConnection(conn);
            objsqlconn.Open();
            string querySeedSampleCouponTransaction = "Insert into SeedSampleCouponTransaction(Form1Id,SSCId,FromID,ToID,TransactionStatus,TransactionDate,IsCurrentAction) Values(@Form1Id,@SSCId,@FromID,@ToID,@TransactionStatus,@TransactionDate,@IsCurrentAction)";
            SqlCommand cmdSeedSampleCouponTransaction = new SqlCommand(querySeedSampleCouponTransaction, objsqlconn);
           
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@Form1Id", from1id);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@SSCId",  sscId);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@FromID", scoId.Id);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@ToID", ToID);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@TransactionStatus", statusCode);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
            cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@IsCurrentAction", isCurrentAction);
           // cmdSeedSampleCouponTransaction.Parameters.AddWithValue("@IsApproved", 0);
            int result = cmdSeedSampleCouponTransaction.ExecuteNonQuery();

            objsqlconn.Close();
        }

        protected void rbTestType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // getTests(rbTestType.SelectedValue.Trim());
        }
        private void getTests(string TestType)
        {
            //chkTests.Items.Clear();
            //chkTests.BindCheckBoxItem(testMasterService.GetTestsByType(TestType));
            //chkTests.Enabled = true;
        }

        protected void chkTests_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (chkTests.SelectedValue == "1")
            //{
            //    for (int j = 0; j < chkTests.Items.Count; j++)
            //    {
            //        chkTests.Items[j].Selected = false;
            //    }
            //    chkTests.SelectedValue = "1";
            //}
            //else
            //{
            //    for (int k = 0; k < chkTests.Items.Count; k++)
            //    {
            //        if (chkTests.Items[k].Value == "1")
            //        {
            //            chkTests.Items[k].Selected = false;
            //        }
            //    }
            //}            
        }
    }
}