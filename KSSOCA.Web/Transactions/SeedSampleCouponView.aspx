﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeedSampleCouponView.aspx.cs" Inherits="KSSOCA.Web.Transactions.SeedSampleCouponView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" style="min-height: 600px;">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
               
                <br />
                <div align="left" id="divTxtform1" runat="server" style="width: 100%">
                   Enter Form-I Registration No. : <asp:TextBox runat="server" ID="txtFormNo" placeholder=" Form-I Registration No." onkeypress="return CheckNumber(event);" />&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btngetInspection" class="btn-primary" Text="Get Seed Sampling Coupons" OnClick="btngetInspection_Click" />&nbsp;&nbsp;&nbsp;
                    <%--<asp:Button runat="server" ID="Add" class="btn-primary" OnClick="Add_Click" Text="Enter New Seed Sampling Coupon" Visible="false" />
         --%>       </div>
                <br />
                <div id="divGridview" runat="server" visible="false">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td>
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                                <asp:GridView runat="server" ID="SeedSamplingCoupanGridView"
                                    AutoGenerateColumns="false" class="table-condensed" Width="100%"
                                    AllowPaging="true" PageSize="25" OnRowDataBound="SeedSamplingCoupanGridViewGridView_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl.No.">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %> .
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="ID" HeaderText="SEED SAMPLING COUPON NUMBER" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="Form1Id" HeaderText="Form-I Registration No." />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="CROPNAME" HeaderText="CROP NAME" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="VARIETYNAME" HeaderText="VARIETY NAME" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="LotNo" HeaderText="Lot No" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="SampledDate" HeaderText="Sampled Date" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="TestType" HeaderText="Test Type" />
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField HeaderText="STL LAB Status">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("TESTRESULT") %>' />
                                                <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>|
                                                <asp:LinkButton runat="server" ID="lnkViewSTL" PostBackUrl='<%# "~/Transactions/SeedAnalysisReportApproval.aspx?Id=" + Eval("ID") %>'>View Result</asp:LinkButton>|
                                                <asp:LinkButton runat="server" ID="lnkReSample"  CommandArgument='<%# Eval("ID") %>' OnClick="lnkReSample_Click" Visible="false"  >Generate Sample Coupon for Retest</asp:LinkButton>                                       
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField HeaderText="GOT Status">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnStatusCode2" runat="server" Value='<%# Eval("GOTRESULT") %>' />
                                                <asp:Label runat="server" ID="lblStatus2" Font-Bold="true"> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="lnkView" PostBackUrl='<%# "~/Transactions/SeedSampleCouponApproval.aspx?Id=" + Eval("Id") %>'>View</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                                </asp:GridView>

                               
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
