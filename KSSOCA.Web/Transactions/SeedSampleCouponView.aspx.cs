﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SeedSampleCouponView : System.Web.UI.Page
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlDataReader dataReader;
        SqlConnection objsqlconn = new SqlConnection(conn);
        FormOneDetailsService form1Service;
        UserMasterService userMasterService;
        SourceVerificationService sourceVerificationService;
        SeedSampleCouponService seedSampleCouponService;
        // FieldInspectionService fieldInspectionService;
        Common common = new Common();
        TestMasterService testMasterService;
        public SeedSampleCouponView()
        {
            form1Service = new FormOneDetailsService();
            userMasterService = new UserMasterService();
            sourceVerificationService = new SourceVerificationService();
            seedSampleCouponService = new SeedSampleCouponService();
            // fieldInspectionService = new FieldInspectionService();
            testMasterService = new TestMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("SSC"); litNote.Text = common.getNote("SSC");// Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int Form1ID = Convert.ToInt16(Request.QueryString["Id"]);
                    Session["Form1ID"] = Form1ID;
                   
                    getFormDetails(Form1ID);
                }
                else
                {
                    if (Session["Form1ID"] != null)
                    {
                        getFormDetails(Convert.ToInt16(Session["Form1ID"]));
                    }
                    else
                    {
                        SeedSamplingCoupanGridView.DataSource = null;
                        SeedSamplingCoupanGridView.DataBind();
                    }
                }

                InitComponent();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";

                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent()
        {
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
            //if (roleId == 5)
            //{
            //    Add.Visible = false;
            //}

        }
        private int GetSSCTransaction(SeedSampleCoupon ssc)
        {
            int isApproved = 0;
            using (objsqlconn)
            {
                String maxId = @"SELECT top 1 * FROM SeedSampleCouponTransaction where 
                                     Form1Id = '" + ssc.Form1Id + "' and SSCId = '" + ssc.Id + "' order by Id  desc";
                SqlCommand maxTransactionCmd = new SqlCommand(maxId, objsqlconn);
                
                dataReader = maxTransactionCmd.ExecuteReader();
                dataReader.Read();
                isApproved = Convert.ToInt32(dataReader["IsApproved"]);


            }

            return isApproved;
        }
        //private void calculateAmount()
        //{
        //    decimal AmountToBePaid = 0;
        //    decimal AmountPaid = 0;
        //    int form1Id = Convert.ToInt16(Session["Form1ID"]);
        //    List<SeedSampleCoupon> ssc = seedSampleCouponService.GetBy(form1Id);
        //    for (int i = 0; i < ssc.Count; i++)
        //    {
        //        List<TestMaster> tests = testMasterService.GetTestsBy(ssc[i].TestsRequired);
        //        for (int j = 0; j < tests.Count; j++)
        //        {
        //            AmountToBePaid += tests[j].Charge;
        //        }
        //    }
        //    AmountPaid = common.getLabPaymentByForm1(form1Id);
        //    decimal DiffAmt = AmountToBePaid - AmountPaid;
        //    if (DiffAmt == 0)
        //    {
        //        pnlPayment.Visible = false;
        //        pnlPaymentDetails.Visible = true;
        //    }
        //    else if (DiffAmt > 0)
        //    {
        //        pnlPayment.Visible = true;
        //        pnlPaymentDetails.Visible = true;
        //        lblamounttobepaid.Text = DiffAmt.ToString();
        //    }
        //}
        
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            //  checkPaymentMode();
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            int Form1ID = Convert.ToInt16(Session["Form1ID"]);
            this.Redirect("Transactions/SeedSampleCouponCreate.aspx?Id=" + Form1ID);
        }
        public bool DeleteMethod(int id)
        {
            int deletedRowCount = form1Service.Delete(id);
            return deletedRowCount > 0 ? true : false;
        }
        public DataTable SelectMethod(int form1ID)
        {
            var data = (DataTable)null;
            if (Session["Form1ID"] != null)
            {
                int form1Id = Convert.ToInt16(Session["Form1ID"]);
                var user = userMasterService.GetFromAuthCookie();
                data = seedSampleCouponService.ReadByForm1ID(form1Id);
               List<SeedSampleCoupon> sscData = seedSampleCouponService.GetBy(form1ID);
                if (data != null)
                {
                    int sscStl = sscData[0].IsApproved;
                    int sscGot=0;
                    if (sscData.Count > 1)
                    {
                         sscGot = sscData[1].IsApproved;
                    }
                    if (user.Role_Id == 6 || user.Role_Id == 4)
                    {
                        return data;
                    }

                    else if (user.Role_Id == 5)
                    {
                        if ((sscStl == 1 && sscGot == 1) || (sscStl == 1 && sscGot == 0))
                        {
                            return data;
                        }
                        //    // Add.Visible = false;
                        else
                        {
                            return null;
                        }
                    }
                }
                else
                {
                    if (user.Role_Id == 6 || user.Role_Id == 4)
                    {
                        int Form1ID = Convert.ToInt16(Session["Form1ID"]);
                        this.Redirect("Transactions/SeedSampleCouponCreate.aspx?Id=" + Form1ID);
                    }
                }
                return (DataTable)null;
            }
            else return (DataTable)null;

        }
        protected void SeedSamplingCoupanGridViewGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                HiddenField hdnStatusCode2 = (e.Row.FindControl("hdnStatusCode2") as HiddenField);
                Label lblStatus2 = (e.Row.FindControl("lblStatus2") as Label);
                LinkButton lnkViewSTL = (e.Row.FindControl("lnkViewSTL") as LinkButton);
                LinkButton lnkReSample = (e.Row.FindControl("lnkReSample") as LinkButton);

                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);

                if (roleId == 6 || roleId == 4)
                {
                    //lnkDelete.Visible = true;
                }
                else
                {
                    //lnkDelete.Visible = false;
                }
                if (hdnStatusCode.Value == "1")
                {
                    lblStatus.Text = "Pass";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    lnkViewSTL.Visible = true;
                }
                else if (hdnStatusCode.Value == "0")
                {
                    lblStatus.Text = "Fail";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    lnkViewSTL.Visible = true;
                    lnkReSample.Visible = true;
                }
                else
                {
                    lblStatus.Text = "";
                    lblStatus.ForeColor = System.Drawing.Color.Cyan;
                    lnkViewSTL.Visible = false;
                }
                if (hdnStatusCode2.Value == "1")
                {
                    lblStatus2.Text = "Pass";
                    lblStatus2.ForeColor = System.Drawing.Color.Green;
                }
                if (hdnStatusCode2.Value == "0")
                {
                    lblStatus2.Text = "Pending";
                    lblStatus2.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lblStatus2.Text = "";
                    lblStatus2.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        protected void btngetInspection_Click(object sender, EventArgs e)
        {
            if (txtFormNo.Text != "")
            {
                //int actionid = GetSSCTransaction(sscoupon);
                getFormDetails(txtFormNo.Text.ToInt());
            }
            else { lblMessage.Text = "Please enter Form1 Number."; }
        }
        //private void checkPaymentMode()
        //{
        //    if (rbPaymentMode.SelectedValue == "C")
        //    {
        //        pnlCheck.Visible = true;
        //        pnlOnline.Visible = false;
        //    }
        //    else if (rbPaymentMode.SelectedValue == "O")
        //    {
        //        pnlCheck.Visible = false;
        //        pnlOnline.Visible = true;
        //    }
        //}
        private void getFormDetails(int form1ID)
        {
            var user = userMasterService.GetFromAuthCookie();
            Form1Details f1 = form1Service.ReadByStatus(form1ID);
           
            if (f1 != null)
            {
                if (user.Role_Id == 5)
                {
                    List<SeedSampleCoupon> sscData = seedSampleCouponService.GetBy(form1ID);
                    // Add.Visible = false;
                    if (user.Id == f1.Producer_Id)
                    {
                        divGridview.Visible = true;
                    }
                    else
                    {
                        divGridview.Visible = false;
                        lblMessage.Text = "You are not authorised to view this form details.";
                    }
                }
                else if (user.Role_Id == 6 || user.Role_Id == 4)
                {
                   // Add.Visible = true;
                    divGridview.Visible = true;
                }
                Session["Form1ID"] = form1ID;
                SeedSamplingCoupanGridView.DataSource = SelectMethod(form1ID);
                SeedSamplingCoupanGridView.DataBind();
                // calculateAmount(); checkPaymentMode();
            }
            else
            {
                lblMessage.Text = "Enter valid Form1 number";
               // Add.Visible = false;
                SeedSamplingCoupanGridView.DataSource = null;
                SeedSamplingCoupanGridView.DataBind();
            }
        }

        protected void lnkReSample_Click(object sender, EventArgs e)
        {
            // Generate Sample Coupan
        }
    }
}