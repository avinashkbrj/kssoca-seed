﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using KSSOCA.Core.Validators;
    using System.Web.UI.WebControls;
    using System.Web.UI;
    using System.Data;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SeedTransferRequestApproval : SecurePage
    {
        
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        SeasonMasterService seasonMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceVerificationService;
        RegistrationFormService registrationFormService;
        DistrictMasterService districtMasterService; Common common = new Common();
        SourceVerificationTransactionService sourceVerificationTransactionService;
        SVLotNumbersService svLotNumbersService;
        TalukMasterService talukService;
        DistrictMasterService districtService;
        SeedTransferTransactionService seedTransferTransactionService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        public SeedTransferRequestApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            seasonMasterService = new SeasonMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            sourceVerificationService = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            districtMasterService = new DistrictMasterService();
            sourceVerificationTransactionService = new SourceVerificationTransactionService();
            svLotNumbersService = new SVLotNumbersService();
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
            seedTransferTransactionService = new SeedTransferTransactionService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("STRC"); litNote.Text = common.getNote("STRC");
                if (Request.QueryString["Id"] != null && Request.QueryString["lotid"] != null)
                {
                    int id = Request.QueryString.Get("Id").ToInt();
                    int lotId = Request.QueryString.Get("lotid").ToInt();
                    int lotId1 = Request.QueryString["lotid"].ToInt();


                    if (id > 0)
                    {
                        SourceVerification sourceverification = sourceVerificationService.ReadById(id);
                        if (sourceverification != null)
                        { 
                                InitComponent(sourceverification);  
                        }
                        else
                        {
                            lblMessage.Text = "SourceVerification not found";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false; Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(SourceVerification sv)
        {
           
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
            var userMaster = userMasterService.GetFromAuthCookie();

            Id.Value = sv.Id.ToString(); //source verification id to hidden field
            // registration details
            RegistrationForm registrationForm = registrationFormService.GetByUserId(sv.Producer_Id);
            PrRegistrationNumber.Text = sv.ProducerRegNo;
            var ProducerUserDetails = userMasterService.GetById(Convert.ToInt16(registrationForm.ProducerID));
            FirmProducerName.Text = ProducerUserDetails.Name;
            PrName.Text = registrationForm.OfficialName;
            EmailId.Text = ProducerUserDetails.EmailId;
            MobileNumber.Text = ProducerUserDetails.MobileNo;
            lblDistrict.Text = districtMasterService.GetById(Convert.ToInt16(ProducerUserDetails.District_Id)).Name;
            // Source verification details
            lblCrop.Text = cropMasterService.GetById(Convert.ToInt16(sv.Crop_Id)).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(Convert.ToInt16(sv.Variety_Id)).NameSV;
            int tid = Request.QueryString.Get("tid").ToInt();
            int lotId = Request.QueryString.Get("lotid").ToInt();
           
                String tranactionQuery = @"SELECT top 1 sv.Id,sv.LotNumber , st.QuantityofSeed as Quantity, sv.SeedRate,sv.ValidPeriod,sv.PurchaseBillNo, sv.PurchaseBillDate,sv.ReleaseOrderNo,sv.ReleaseOrderDate
                           FROM  SeedBranchTransfer as st
                          inner join  SVLotNumbers as sv on st.LotId = sv.Id
                          inner join SeedBranchTransferTransaction as sbt on sbt.SeedBranchTransferID = st.Id
                          where sbt.SeedBranchTransferID='" + tid + "'";
                SqlCommand transactionCmd = new SqlCommand(tranactionQuery, objsqlconn);
                objsqlconn.Open();
                gvLotNos.DataSource = transactionCmd.ExecuteReader();
                gvLotNos.DataBind();
                objsqlconn.Close();
            



            //gvLotNos.DataSource = svLotNumbersService.GetBySVId(sv.Id);
            //gvLotNos.DataBind();
            SetLabelNames(sv);


            // lblQuantitySeed.Text = Convert.ToInt32(sv.QuantityofSeed).ToString();

           // lblArea.Text = sv.AreainAcre.ToString();
            lblSeason.Text = seasonMasterService.GetById(Convert.ToInt16(sv.Season_Id)).Name;
            lblSeedLocation.Text = sv.LocationofSeedStock + " Taluk:" + talukService.GetBy(sv.Stk_District_Id, sv.Stk_Taluk_Id).Name + ", District:" + districtMasterService.GetById(sv.Stk_District_Id).Name;
            txttagnumbers.Text = sv.TagNumbers;
            lblClassSeed.Text = classSeedMasterService.GetById(Convert.ToInt16(sv.ClassSeed_Id)).Name;
            lblCropTypeName.Text = sv.CropType == "H" ? "Parentage" : "Variety";
            lblCropType.Text = sv.CropType == "H" ? "Hybrid" : "Non-Hybrid";
            lblRefNo.Text = sv.Id.ToString();
            SqlDataReader destinationvalue;
            
            String tranactionQuerySeed = @"Select um.Name as Name from SeedBranchTransfer sbt join SeedBranchTransferTransaction sbtt on sbt.Id = sbtt.SeedBranchTransferID
                join UserMaster um on um.Id = sbt.DestinationLocation 
                where sbtt.SeedBranchTransferID = '" + tid + "'";
            SqlCommand transactionCmdSeed = new SqlCommand(tranactionQuerySeed, objsqlconn);
            objsqlconn.Open();
            destinationvalue = transactionCmdSeed.ExecuteReader();
            destinationvalue.Read();
            string destinationFirm = Convert.ToString(destinationvalue["Name"]);
            transferLocation.Text = destinationFirm;
            objsqlconn.Close();
            



            RegistrationDate.Text = sv.RegistrationDate.ToString("dd/MM/yyyy");
            if (sv.ReleaseOrderimage != null && sv.ReleaseOrderimage.Length > 0)
                LitReleaseOrder.Text = string.Format(previewString, Serializer.Base64String(sv.ReleaseOrderimage));

            if (sv.PurchaseBillimage != null && sv.PurchaseBillimage.Length > 0)
                LitPuchaseBill.Text = string.Format(previewString, Serializer.Base64String(sv.PurchaseBillimage));

            if (sv.BCimage != null && sv.BCimage.Length > 0)
                litbreedercertificate.Text = string.Format(previewString, Serializer.Base64String(sv.BCimage));

            if (sv.Tagsimage != null && sv.Tagsimage.Length > 0)
                litTags.Text = string.Format(previewString, Serializer.Base64String(sv.Tagsimage));
 
            String tranactionQueryStatus = @"SELECT um1.Name FromID, um2.Name as ToId, tsm.Status as TransactionStatus, [TransactionDate],[Remarks]  
                        FROM  SeedBranchTransferTransaction  as sput
                        inner join  UserMaster um1 on sput.FromID = um1.Id 
                        inner join UserMaster um2 on sput.ToID = um2.Id
                        inner join TransactionStatusMaster tsm on tsm.Id = sput.TransactionStatus
                        where sput.SeedBranchTransferID = '" + tid + "' order by sput.Id desc";
            SqlCommand transactionCmdStatus = new SqlCommand(tranactionQueryStatus, objsqlconn);
            objsqlconn.Open();
            gvstatus.DataSource = transactionCmdStatus.ExecuteReader();
            gvstatus.DataBind();
            objsqlconn.Close();
            
            SqlDataReader dataReader;
            var Output = " ";
            int actionid;
            int roleid;
             
            String maxId = @"Select * from SeedBranchTransferTransaction where Id IN (SELECT MAX(Id) FROM SeedBranchTransferTransaction where 
                                    SourceVerificationID = '" + sv.Id + "' and SeedBranchTransferID='" + tid + "')";
            SqlCommand maxTransactionCmd = new SqlCommand(maxId, objsqlconn);
            objsqlconn.Open();
            dataReader = maxTransactionCmd.ExecuteReader();
            dataReader.Read();
            actionid = Convert.ToInt32(dataReader["ToID"]);
            objsqlconn.Close();
            // Output = Output + dataReader.GetValue(0) + "-" + dataReader.GetValue(1) + "</br>";
            //actionid = Convert.ToInt32(dataReader.GetValue(3)); 
            SourceVerificationTransaction transaction = new SourceVerificationTransaction();
            transaction = sourceVerificationTransactionService.GetMaxTransactionDetails(sv.Id);
            int ActionRole_Id = actionid;

            if (userMaster.Role_Id == 5)
            {
                pnlRemarks.Visible = false;
                if (transaction.TransactionStatus == 0)
                {
                  //  btnEditApplication.Visible = true;
                  //  btnEditApplication.PostBackUrl = "~/Transactions/SourceVerificationCreate.aspx?Id=" + sv.Id;
                }
                if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
            }
            else
            {
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                }
                else
                {
                    pnlRemarks.Visible = false;
                }
            }
        }
        private void SetLabelNames(SourceVerification sv)
        {
            lblsourcetype.Text = sv.SourceType == "O" ? "Own" : sv.SourceType == "P" ? "Purchsed" : "Pending GOT";

            if (sv.ClassSeed_Id == 1)
            {
                gvLotNos.HeaderRow.Cells[4].Text = "Date of Test";
                gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                gvLotNos.HeaderRow.Cells[7].Text = "Breeder Certificate No.";
                gvLotNos.HeaderRow.Cells[8].Text = "Date of Breeder Certificate ";
                lblDocReleaseOrder.Text = "Breeder Certificate";
                lblDocPurchaseBill.Text = "Purchase Bill";

            }
            else if (sv.ClassSeed_Id != 0)
            {

                if (sv.SourceType == "O")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order";
                    lblDocPurchaseBill.Text = "Stock Reciept Note";

                }
                else if (sv.SourceType == "P")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order";
                    lblDocPurchaseBill.Text = "Purchase Bill";
                }
                else if (sv.SourceType == "G")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill/Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill/Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order Under taking letter";
                    lblDocPurchaseBill.Text = "Purchase Bill/Stock Reciept Note";
                }
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                int lotResult, statusCode;
                int tid = Request.QueryString.Get("tid").ToInt();
                var userMaster = userMasterService.GetFromAuthCookie();
                string conn = "";
                 
                SqlDataReader dataReaderDest, drTransaction;
                
               
                String transactionDetails = @"Select top 1  * from SeedBranchTransferTransaction  where SeedBranchTransferID ='"+tid+"' order by id desc";
                SqlCommand transactionDetailsCmd = new SqlCommand(transactionDetails, objsqlconn);
                
                objsqlconn.Open();
                drTransaction = transactionDetailsCmd.ExecuteReader();
                drTransaction.Read();
                int transactionstatus = Convert.ToInt32(drTransaction["TransactionStatus"]);
                objsqlconn.Close();
                int lotId = Request.QueryString.Get("lotid").ToInt();

                int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
                int TransactionStatus = 0;
                int ToID = 0;
                int svid = Request.QueryString.Get("Id").ToInt();
                SVLotNumbers svLotNumber = svLotNumbersService.GetById(lotId);

                SourceVerification sv = sourceVerificationService.ReadById(svid);
                String destinationLocation = @"Select * from SeedBranchTransfer where Id = '" + tid + "'";
                SqlCommand destinationCmd = new SqlCommand(destinationLocation, objsqlconn);
                objsqlconn.Open();
                dataReaderDest = destinationCmd.ExecuteReader();
                dataReaderDest.Read();
                int destination = Convert.ToInt32(dataReaderDest["DestinationLocation"]);
                int qtyofSeed = Convert.ToInt32(dataReaderDest["QuantityofSeed"]);
                int sourceId = Convert.ToInt32(dataReaderDest["Producer_Id"]);
                objsqlconn.Close();
                decimal acreinacre = qtyofSeed / sv.SeedRate;
                var masterService = userMasterService.GetById(destination);
                var sourceDetails = userMasterService.GetById(sourceId);
                int IsCurrentAction = 1;
                int transactionId = common.GenerateID("SeedBranchTransferTransaction");
                String Remarks = txtRemarks.Text;
                if (transactionstatus == 8)
                {
                    ToID = destination;
                    TransactionStatus = 5;
                    string querySeedBTransaction = "Insert into SeedBranchTransferTransaction(ID,SourceVerificationID,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction,SeedBranchTransferID) Values(@ID,@SourceVerificationID,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction,@SeedBranchTransferID)";
                    SqlCommand cmdSeedBranchTransaction = new SqlCommand(querySeedBTransaction, objsqlconn);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ID", transactionId);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SourceVerificationID", sv.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@FromID", userMaster.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ToID", ToID);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionStatus", TransactionStatus);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@Remarks", Remarks);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@IsCurrentAction", IsCurrentAction);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SeedBranchTransferID", tid);
                    int IsCurrentUserSet = common.setIsCurrentAction("SeedBranchTransferTransaction", "SeedBranchTransferID", tid);
                    int sourceVID = common.GenerateID("SourceVerification");
                    if (IsCurrentUserSet > 0)
                    {
                        statusCode = 5;
                        objsqlconn.Open();
                        int result = cmdSeedBranchTransaction.ExecuteNonQuery();
                        UpdateTransferStatus(statusCode, objsqlconn, tid);
                        objsqlconn.Close();

                        if (result == 1)
                        {
                            int svresult = sourceVerificationService.Create(new Model.SourceVerification()
                            {
                                Id = sourceVID,
                                Producer_Id = destination,
                                Crop_Id = sv.Crop_Id,
                                Variety_Id = sv.Variety_Id,
                                ClassSeed_Id = sv.ClassSeed_Id,
                                QuantityofSeed = qtyofSeed,
                                AreainAcre = acreinacre,
                                SeedRate = sv.SeedRate,
                                Season_Id = sv.Season_Id,
                                LocationofSeedStock = sv.LocationofSeedStock,
                                RegistrationDate = System.DateTime.Now,
                                TagNumbers = sv.TagNumbers,
                                ReleaseOrderimage = sv.ReleaseOrderimage,
                                PurchaseBillimage = sv.PurchaseBillimage,
                                BCimage = sv.BCimage,
                                Tagsimage = sv.Tagsimage,
                                Stk_District_Id = masterService.District_Id.GetValueOrDefault(),
                                Stk_Taluk_Id = masterService.Taluk_Id.GetValueOrDefault(),
                                SourceType = sv.SourceType,
                                StatusCode = 1,
                                FinancialYear = System.DateTime.Now.ToFinancialYear(),
                                CropType = "H",
                                ProducerRegNo = sv.ProducerRegNo

                            });
                            if (svresult > 0)
                            {


                                string svLotInsert = "Insert into SVLotNumbers(SVID,LotNumber,SeedRate, Quantity,PurchaseBillDate,PurchaseBillNo,ReleaseOrderDate, ReleaseOrderNo,ValidPeriod) Values(@SVID,@LotNumber,@SeedRate,@Quantity,@PurchaseBillDate,@PurchaseBillNo,@ReleaseOrderDate, @ReleaseOrderNo,@ValidPeriod)";
                                SqlCommand cmdsvLotInsert = new SqlCommand(svLotInsert, objsqlconn);
                                cmdsvLotInsert.Parameters.AddWithValue("@SVID", sourceVID);
                                cmdsvLotInsert.Parameters.AddWithValue("@LotNumber", svLotNumber.LotNumber);
                                cmdsvLotInsert.Parameters.AddWithValue("@SeedRate", svLotNumber.SeedRate);
                                cmdsvLotInsert.Parameters.AddWithValue("@Quantity", qtyofSeed);
                                cmdsvLotInsert.Parameters.AddWithValue("@PurchaseBillDate", svLotNumber.PurchaseBillDate);
                                cmdsvLotInsert.Parameters.AddWithValue("@PurchaseBillNo", svLotNumber.PurchaseBillNo);
                                cmdsvLotInsert.Parameters.AddWithValue("@ReleaseOrderDate", svLotNumber.ReleaseOrderDate);
                                cmdsvLotInsert.Parameters.AddWithValue("@ReleaseOrderNo", svLotNumber.ReleaseOrderNo);
                                cmdsvLotInsert.Parameters.AddWithValue("@ValidPeriod", svLotNumber.ValidPeriod);
                                objsqlconn.Open();
                                int svlotresult = cmdsvLotInsert.ExecuteNonQuery();
                                objsqlconn.Close();


                                var rft = new SourceVerificationTransaction
                                {
                                    Id = common.GenerateID("SourceVerificationTransaction"),
                                    FromID = sourceId,
                                    ToID = destination,
                                    SourceVerificationID = sourceVID,
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = 2,
                                    Remarks = "",
                                    IsCurrentAction = 1
                                };


                                int svtresult = sourceVerificationTransactionService.Create(rft);
                                if (svtresult == 1)
                                {

                                    lblMessage.ForeColor = System.Drawing.Color.Green;
                                    lblMessage.Text = "Approved successfully.";
                                    Messaging.SendSMS("Source Transfer Request to "+ masterService.Name + " Has Been Approved By " + userMaster.Name + "", MobileNumber.Text, "1");
                                    Messaging.SendSMS("Source Recieved From "+ sourceDetails.Name + " Has Been Approved By " + userMaster.Name + " Quantity "+ qtyofSeed + "", masterService.MobileNo, "1");
                                    Page_Load(sender, e);
                                }
                            }

                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }

                }
                else{

                    TransactionStatus = 8;
                  
                    ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                             (Convert.ToInt16(masterService.District_Id),
                                              Convert.ToInt16(masterService.Taluk_Id), 4).User_Id);
                    //    ToID = Convert.ToInt16(userMaster.Reporting_Id);
                    //    TransactionStatus = Convert.ToInt16(userMasterService.GetById(Convert.ToInt16(userMaster.Reporting_Id)).Role_Id);
                    //}
                    int FromID = userMaster.Id;

                    var transferringLocation = userMasterService.GetById(ToID).Name;
                    string querySeedBranchTransaction = "Insert into SeedBranchTransferTransaction(ID,SourceVerificationID,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction,SeedBranchTransferID) Values(@ID,@SourceVerificationID,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction,@SeedBranchTransferID)";
                    SqlCommand cmdSeedBranchTransaction = new SqlCommand(querySeedBranchTransaction, objsqlconn);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ID", transactionId);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SourceVerificationID", sv.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@FromID", userMaster.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ToID", ToID);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionStatus", TransactionStatus);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@Remarks", Remarks);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@IsCurrentAction", IsCurrentAction);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SeedBranchTransferID", tid);
                    int IsCurrentUserSetSBT = common.setIsCurrentAction("SeedBranchTransferTransaction", "SeedBranchTransferID", tid);
                    if (IsCurrentUserSetSBT > 0)
                    {
                        statusCode = 9;
                        objsqlconn.Open();
                        int result = cmdSeedBranchTransaction.ExecuteNonQuery();
                        UpdateTransferStatus(statusCode, objsqlconn, tid);
                        objsqlconn.Close();
                        if (result == 1)
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Approved successfully.";
                            Messaging.SendSMS("Seed Trasnfer Request to "+ masterService.Name + " has been approved by " + userMaster.Name + " and transferred to "+ transferringLocation + "", MobileNumber.Text, "1");
                            Page_Load(sender, e);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }

      
        private static void UpdateTransferStatus(int statusCode, SqlConnection objsqlconn, int tid)
        {
            string statusUpdate = "Update SeedBranchTransfer set StatusCode='" + statusCode + "'  where Id =" + tid;
            SqlCommand cmdstatusUpdate = new SqlCommand(statusUpdate, objsqlconn);
            cmdstatusUpdate.ExecuteNonQuery();
        }

        private int FinalApproval(int SourceVerificationId)
        {
            string query = "update SourceVerification set StatusCode=1  where Id= " + SourceVerificationId;
            return common.ExecuteNonQuery(query);
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Dashboard.aspx");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                   

                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMaster.Role_Id);
                    int tid = Request.QueryString.Get("tid").ToInt();
                    SourceVerification sv = sourceVerificationService.ReadById(Id.Value.ToInt());

                    int tranactionId = common.GenerateID("SeedBranchTransferTransaction");
                    int FromID = userMaster.Id;
                    int ToID = Convert.ToInt16(sv.Producer_Id);
                    int TransactionStatus = 99;
                    int IsCurrentAction = 1;
                    String Remarks = txtRemarks.Text;
                    DateTime TransactionDate = System.DateTime.Now;
                    int statusCode = 99;

                    string querySeedBranchTransaction = "Insert into SeedBranchTransferTransaction(ID,SourceVerificationID,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction,SeedBranchTransferID) Values(@ID,@SourceVerificationID,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction,@SeedBranchTransferID)";
                    SqlCommand cmdSeedBranchTransaction = new SqlCommand(querySeedBranchTransaction, objsqlconn);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ID", tranactionId);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SourceVerificationID", sv.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@FromID", userMaster.Id);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@ToID", ToID);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionStatus", TransactionStatus);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@Remarks", Remarks);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@IsCurrentAction", IsCurrentAction);
                    cmdSeedBranchTransaction.Parameters.AddWithValue("@SeedBranchTransferID", tid);
                    
                    int IsCurrentUserSet = common.setIsCurrentAction("SeedBranchTransferTransaction", "SeedBranchTransferID", tid);
                    if (IsCurrentUserSet > 0)
                    {
                        //int result = sourceVerificationTransactionService.Create(rft);
                        objsqlconn.Open();
                        int result = cmdSeedBranchTransaction.ExecuteNonQuery();
                        objsqlconn.Close();
                        if (result == 1)
                        {
                            int id = Request.QueryString.Get("Id").ToInt();
                            int lotId = Request.QueryString.Get("lotid").ToInt();
                            var svDetails = svLotNumbersService.GetById(lotId);


                            SqlDataReader dataReader;
                            // int actionid;


                            String branchTransfer = @"Select * from SeedBranchTransfer where Id = '" + tid + "'";
                            SqlCommand branchTransferCmd = new SqlCommand(branchTransfer, objsqlconn);
                            objsqlconn.Open();
                            dataReader = branchTransferCmd.ExecuteReader();
                            dataReader.Read();
                            int quantityDeducted = Convert.ToInt32(dataReader["QuantityofSeed"]);
                            int uId = Convert.ToInt32(dataReader["SourceVerificationID"]);
                            int destinationLocation = Convert.ToInt32(dataReader["DestinationLocation"]);
                            objsqlconn.Close();
                            ////    // Output = Output + dataReader.GetValue(0) + "-" + dataReader.GetValue(1) + "</br>";
                            ////    //actionid = Convert.ToInt32(dataReader.GetValue(3));

                            ////}

                            //decimal transferringQty = Convert.ToDecimal(tansferQuantity.Text);
                            var destinationDetails=userMasterService.GetById(destinationLocation);
                            decimal updatedQty = sv.QuantityofSeed + quantityDeducted;
                            decimal updateLotQty = svDetails.Quantity + quantityDeducted;
                            //int userid = Convert.ToInt16(SeedB.SelectedValue);
                            decimal seedRate = sv.SeedRate;
                            //// decimal seedQutantity = sv.QuantityofSeed;
                            decimal area = updatedQty / seedRate;
                            objsqlconn.Open();
                            string updateSourceVerification = "Update SourceVerification set QuantityofSeed='" + updatedQty + "',AreainAcre='" + area + "'   where Id= " + sv.Id;
                            SqlCommand cmdUpdateSourceVerification = new SqlCommand(updateSourceVerification, objsqlconn);
                            cmdUpdateSourceVerification.ExecuteNonQuery();
                            string querySVLotNumbers = "Update SVLotNumbers set Quantity='" + updateLotQty + "'  where Id =" + svDetails.Id;
                            SqlCommand cmdSVLotNumbers = new SqlCommand(querySVLotNumbers, objsqlconn);
                            cmdSVLotNumbers.ExecuteNonQuery();
                            string statusUpdate = "Update SeedBranchTransfer set StatusCode='" + statusCode + "'  where Id =" + tid;
                            SqlCommand cmdstatusUpdate = new SqlCommand(statusUpdate, objsqlconn);
                            cmdstatusUpdate.ExecuteNonQuery();
                            objsqlconn.Close();
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Rejected successfully.";
                            Messaging.SendSMS("Seed Transfer Request to "+ destinationDetails.Name + " rejected by " + userMaster.Name + "", userMaster.MobileNo, "1");
                            Page_Load(sender, e);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Validation error occurred.";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }

        private int insertLotNumbers(int svid, DataTable dtLotsData)
        {
            int recordCount = 0;
            var svLotNumbers = new List<SVLotNumbers>();
            for (int i = 0; i < dtLotsData.Rows.Count; i++)
            {
                svLotNumbers.Add(new SVLotNumbers()
                {
                    LotNumber = Convert.ToString(dtLotsData.Rows[i]["LotNumber"]).Trim().ToUpper().Replace(" ", ""),
                    SeedRate = Convert.ToDecimal(dtLotsData.Rows[i]["SeedRate"]),
                    Quantity = Convert.ToDecimal(dtLotsData.Rows[i]["Quantity"]),
                    PurchaseBillDate = Convert.ToDateTime(dtLotsData.Rows[i]["PurchaseBillDate"]),
                    PurchaseBillNo = Convert.ToString(dtLotsData.Rows[i]["PurchaseBillNo"]),
                    ReleaseOrderDate = Convert.ToDateTime(dtLotsData.Rows[i]["ReleaseOrderDate"]),
                    ReleaseOrderNo = Convert.ToString(dtLotsData.Rows[i]["ReleaseOrderNo"]),
                    ValidPeriod = Convert.ToDateTime(dtLotsData.Rows[i]["ValidPeriod"]),
                    SVID = svid

                });
            }
            return recordCount += svLotNumbersService.Update(svLotNumbers, svid).Count;
        }
    }
}