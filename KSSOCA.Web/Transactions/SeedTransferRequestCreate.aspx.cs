﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using KSSOCA.Core.Validators;
    using System.Web.UI.WebControls;
    using System.Web.UI;
    using System.Data;
    using System.Configuration;
    using System.Data.SqlClient;

    public partial class SeedTransferRequestCreate : SecurePage
    {
      
        static string CS = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(CS);
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        SeasonMasterService seasonMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceVerificationService;
        RegistrationFormService registrationFormService;
        DistrictMasterService districtMasterService; Common common = new Common();
        SourceVerificationTransactionService sourceVerificationTransactionService;
        SVLotNumbersService svLotNumbersService;
        TalukMasterService talukService;
        DistrictMasterService districtService;
        UserwiseDistrictRightsService userwiseDistrictRightsService;
        SeedTransferTransactionService seedTransferTransactionService;
        public SeedTransferRequestCreate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            seasonMasterService = new SeasonMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            sourceVerificationService = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            districtMasterService = new DistrictMasterService();
            sourceVerificationTransactionService = new SourceVerificationTransactionService();
            svLotNumbersService = new SVLotNumbersService();
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            seedTransferTransactionService = new SeedTransferTransactionService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("STRC"); //litNote.Text = common.getNote("SVA");
                if (Request.QueryString["Id"] != null)
                {
                    int id = Request.QueryString.Get("Id").ToInt();

                    if (id > 0)
                    {
                        SVLotNumbers svLotNumber = svLotNumbersService.GetById(id);
                        SourceVerification sourceverification = sourceVerificationService.ReadById(svLotNumber.SVID);
                        
                        if (sourceverification != null)
                        {
                            InitComponent(sourceverification);
                            
                        }
                        else
                        {
                            lblMessage.Text = "Source Verification not found";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false; Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(SourceVerification sv)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
            var userMaster = userMasterService.GetFromAuthCookie();

             
            //District.Items.Clear();
            var districts = districtService.GetAll();
            if (!IsPostBack)
            {
                District.BindListItem<DistrictMaster>(districts);
                talukDropDownList();
                seedTranferDestinationDropdown();
            }
            
          
            Id.Value = sv.Id.ToString(); //source verification id to hidden field
            // registration details
            RegistrationForm registrationForm = registrationFormService.GetByUserId(sv.Producer_Id);
            PrRegistrationNumber.Text = sv.ProducerRegNo;
            var ProducerUserDetails = userMasterService.GetById(Convert.ToInt16(registrationForm.ProducerID));
            FirmProducerName.Text = ProducerUserDetails.Name;
            PrName.Text = registrationForm.OfficialName;
            EmailId.Text = ProducerUserDetails.EmailId;
            MobileNumber.Text = ProducerUserDetails.MobileNo;
            lblDistrict.Text = districtMasterService.GetById(Convert.ToInt16(ProducerUserDetails.District_Id)).Name;
            // Source verification details
            lblCrop.Text = cropMasterService.GetById(Convert.ToInt16(sv.Crop_Id)).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(Convert.ToInt16(sv.Variety_Id)).NameSV;
            //int lotno= svLotNumbersService.GetBySVId(sv.Id);
           
            int id = Request.QueryString.Get("Id").ToInt();
            SVLotNumbers svLotNumber = svLotNumbersService.GetById(id);
            if(svLotNumber.Quantity==0)
            {
                saveTransferRequest.Enabled = false;
                tansferQuantity.Visible = false;
                SeedBranchTansfer.Enabled = false;
                District.Enabled = false;
                Taluk.Enabled = false;
            }
            gvLotNos.DataSource = svLotNumbersService.GetByLotByID(id);
            gvLotNos.DataBind();
            SetLabelNames(sv);
            lblQuantitySeed.Text = Convert.ToInt32(sv.QuantityofSeed).ToString();

            lblArea.Text = sv.AreainAcre.ToString();
            lblSeason.Text = seasonMasterService.GetById(Convert.ToInt16(sv.Season_Id)).Name;
            lblSeedLocation.Text = sv.LocationofSeedStock + " Taluk:" + talukService.GetBy(sv.Stk_District_Id, sv.Stk_Taluk_Id).Name + ", District:" + districtMasterService.GetById(sv.Stk_District_Id).Name;
            txttagnumbers.Text = sv.TagNumbers;
            lblClassSeed.Text = classSeedMasterService.GetById(Convert.ToInt16(sv.ClassSeed_Id)).Name;
            lblCropTypeName.Text = sv.CropType == "H" ? "Parentage" : "Variety";
            lblCropType.Text = sv.CropType == "H" ? "Hybrid" : "Non-Hybrid";
            lblRefNo.Text = sv.Id.ToString();
            RegistrationDate.Text = sv.RegistrationDate.ToString("dd/MM/yyyy");
            
            SourceVerificationTransaction transaction = new SourceVerificationTransaction();
            transaction = sourceVerificationTransactionService.GetMaxTransactionDetails(sv.Id);
            int ActionRole_Id = transaction.ToID;
            if (userMaster.Role_Id == 5)
            {
                pnlRemarks.Visible = false;
              
                if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
            }
            else
            {
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                }
                else
                {
                    pnlRemarks.Visible = false;
                }
            }
        }
        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            talukDropDownList();

        }

        private void talukDropDownList()
        {
         
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
        }

        protected void Taluk_SelectedIndexChanged(object sender, EventArgs e)
        {
            SeedBranchTansfer.Items.Clear();

            seedTranferDestinationDropdown();
            
        }

        private void seedTranferDestinationDropdown()
        {
           
            SqlDataReader dr = null;

            String tranactionQuery = @"Select u.Name as name,U.Id as id from UserMaster as u join DistrictMaster as d on u.District_Id = d.Id
                    join TalukMaster as t on t.Id = u.Taluk_Id
                    where u.District_Id='" + District.SelectedValue + "' and u.Taluk_Id='" + Taluk.SelectedValue + "' and Role_Id =5";
            SqlCommand transactionCmd = new SqlCommand(tranactionQuery, objsqlconn);
            objsqlconn.Open();
            dr = transactionCmd.ExecuteReader();
            SeedBranchTansfer.DataSource = dr;
            SeedBranchTansfer.DataTextField = "Name";
            SeedBranchTansfer.DataValueField = "Id";
            SeedBranchTansfer.DataBind();
            SeedBranchTansfer.Items.Insert(0, new ListItem("--Select--", "0"));
            objsqlconn.Close();


        }

        private void SetLabelNames(SourceVerification sv)
        {
            lblsourcetype.Text = sv.SourceType == "O" ? "Own" : sv.SourceType == "P" ? "Purchsed" : "Pending GOT";

            if (sv.ClassSeed_Id == 1)
            {
                gvLotNos.HeaderRow.Cells[4].Text = "Date of Test";
                gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                gvLotNos.HeaderRow.Cells[7].Text = "Breeder Certificate No.";
                gvLotNos.HeaderRow.Cells[8].Text = "Date of Breeder Certificate ";
                //lblDocReleaseOrder.Text = "Breeder Certificate";
                //lblDocPurchaseBill.Text = "Purchase Bill";

            }
            else if (sv.ClassSeed_Id != 0)
            {

                if (sv.SourceType == "O")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";
 

                }
                else if (sv.SourceType == "P")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";
 
                }
                else if (sv.SourceType == "G")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill/Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill/Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                 
                }
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                
                string dt = DateTime.Now.ToString("yyyy-MM-dd");
                
                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
                
                int TransactionStatus = 0;
                int ToID = 0;
                int id = Request.QueryString.Get("Id").ToInt();
                SVLotNumbers svLotNumber = svLotNumbersService.GetById(id);
                //int sco;
                SourceVerification sv = sourceVerificationService.ReadById(svLotNumber.SVID);
                if (RoleID == 5)
                {
 
                    lblSeedLocation.Text = sv.LocationofSeedStock + " Taluk:" + talukService.GetBy(sv.Stk_District_Id, sv.Stk_Taluk_Id).Name + ", District:" + districtMasterService.GetById(sv.Stk_District_Id).Name;
                    ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                         (Convert.ToInt16(sv.Stk_District_Id),
                                          Convert.ToInt16(sv.Stk_Taluk_Id), 4).User_Id);
                    
                    
                    TransactionStatus = 4; //  
                }
                
                int tranactionId = common.GenerateID("SeedBranchTransferTransaction");
                int isCurrentAction = 1;
                
                decimal transferringQty=Convert.ToDecimal(tansferQuantity.Text);
                decimal updatedQty = sv.QuantityofSeed - transferringQty;
                decimal updateLotQty=svLotNumber.Quantity - transferringQty;
                int userid = Convert.ToInt16(SeedBranchTansfer.SelectedValue);
                decimal seedRate = sv.SeedRate;
               
                decimal area = updatedQty / seedRate;

                var targetDivision = userMasterService.GetById(userid);
            
                objsqlconn.Open();
                string querySourceVerification = "Update SourceVerification set QuantityofSeed='" + updatedQty + "',AreainAcre='" + area + "'   where Id= "+ sv.Id;
                SqlCommand cmdSourceVerification = new SqlCommand(querySourceVerification, objsqlconn);
                cmdSourceVerification.ExecuteNonQuery();
                string querySVLotNumbers = "Update SVLotNumbers set Quantity='" + updateLotQty + "'  where Id =" + svLotNumber.Id;
                SqlCommand cmdSVLotNumbers = new SqlCommand(querySVLotNumbers, objsqlconn);
                cmdSVLotNumbers.ExecuteNonQuery();
                
                string querySeedBranchTransfer = "Insert into SeedBranchTransfer(QuantityofSeed,LocationofSeedStock,Producer_Id,SourceVerificationID,DateofRequest,DestinationLocation,LotNo,LotId,StatusCode) Values(@QuantityofSeed,@LocationofSeedStock,@Producer_Id,@SourceVerificationID,@DateofRequest,@DestinationLocation,@LotNo,@LotId,@StatusCode)";
                int statusCode = 4;
                SqlCommand cmdSeedBranchTransfer = new SqlCommand(querySeedBranchTransfer, objsqlconn);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@QuantityofSeed", transferringQty);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@LocationofSeedStock", sv.LocationofSeedStock);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@Producer_Id", sv.Producer_Id);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@SourceVerificationID", sv.Id);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@DateofRequest", System.DateTime.Now);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@DestinationLocation", userid);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@LotNo", svLotNumber.LotNumber);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@LotId", svLotNumber.Id);
                cmdSeedBranchTransfer.Parameters.AddWithValue("@StatusCode", statusCode);

                cmdSeedBranchTransfer.ExecuteNonQuery();
               
                string query = "select Max(Id) from SeedBranchTransfer where Producer_Id=" + userMaster.Id + "";
                int transferID = Convert.ToInt16(common.ExecuteScalar(query));

                string querySeedBranchTransaction = "Insert into SeedBranchTransferTransaction(ID,SourceVerificationID,FromID,ToID,TransactionStatus,TransactionDate,Remarks,IsCurrentAction,SeedBranchTransferID) Values(@ID,@SourceVerificationID,@FromID,@ToID,@TransactionStatus,@TransactionDate,@Remarks,@IsCurrentAction,@SeedBranchTransferID)";
                SqlCommand cmdSeedBranchTransaction = new SqlCommand(querySeedBranchTransaction, objsqlconn);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@ID", tranactionId);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@SourceVerificationID", sv.Id);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@FromID", userMaster.Id);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@ToID", ToID);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionStatus", TransactionStatus);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@TransactionDate", System.DateTime.Now);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@Remarks", "LotNumber "+svLotNumber.LotNumber);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@IsCurrentAction", isCurrentAction);
                cmdSeedBranchTransaction.Parameters.AddWithValue("@SeedBranchTransferID", transferID);
                 
                objsqlconn.Close();

                //   SqlCommand objcmd = new SqlCommand("Insert into SeedBranchTransfer(QuantityofSeed,LocationofSeedStock,Producer_Id,SourceVerificationID,DateofRequest,DestinationLocation) Values('" + transferringQty + "','" + sv.LocationofSeedStock + "','" + sv.Producer_Id + "','" + sv.Id + "','" + System.DateTime.Now + "','" + userid + "')", objsqlconn);


                objsqlconn.Open();
                    int result = cmdSeedBranchTransaction.ExecuteNonQuery();
                    objsqlconn.Close(); 
                    if (result == 1)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Transfer Request Created successfully.";
                        Messaging.SendSMS("Verified Source Tag Number "+sv.TagNumbers + " has been requested to transfer to "+ targetDivision.Name + " by " + userMaster.Name + "", userMaster.MobileNo, "1");
                        this.Redirect("Transactions/SeedTransferRequestApproval.aspx?Id=" + sv.Id+"&lotid="+ id+"&tid="+ transferID + "", false);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                 

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        private int FinalApproval(int SourceVerificationId)
        {
            string query = "update SourceVerification set StatusCode=1  where Id= " + SourceVerificationId;
            return common.ExecuteNonQuery(query);
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Dashboard.aspx");
        }

        protected void Quantity_TextChanged(object sender, EventArgs e)
        {

            int lotId = Request.QueryString.Get("id").ToInt();
            SVLotNumbers svLotQty = svLotNumbersService.GetById(lotId);

            int qtyofSeed = Convert.ToInt16(tansferQuantity.Text);

            if (qtyofSeed > svLotQty.Quantity)
            {
                errorMessage.Text = "Qunatity entered Should be less than or equal to existing quantity";
                tansferQuantity.Text = "";

            }
            else
            {
                errorMessage.Text = "";
            }

        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMaster.Role_Id);

                    SourceVerification sv = sourceVerificationService.ReadById(Id.Value.ToInt());
                    var rft = new SourceVerificationTransaction
                    {
                        Id = common.GenerateID("SourceVerificationTransaction"),
                        FromID = userMaster.Id,
                        ToID = Convert.ToInt16(sv.Producer_Id),
                        SourceVerificationID = Id.Value.ToInt(),
                        TransactionDate = System.DateTime.Now,
                        TransactionStatus = 0, // indicates rejection
                        Remarks = txtRemarks.Text,
                        IsCurrentAction = 1
                    };
                    int IsCurrentUserSet = common.setIsCurrentAction("SourceVerificationTransaction", "SourceVerificationID", Id.Value.ToInt());
                    if (IsCurrentUserSet > 0)
                    {
                        int result = sourceVerificationTransactionService.Create(rft);
                        if (result == 1)
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Rejected successfully.";
                            Messaging.SendSMS("Source Transfer Request Tag Number "+ sv.TagNumbers +" has been rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                            Page_Load(sender, e);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Validation error occurred.";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
    }
}