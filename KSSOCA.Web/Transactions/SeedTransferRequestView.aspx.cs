﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    using System.Linq;
    using System.Web;
    using System.Text;
    using System.Configuration;
    using System.Data.SqlClient;

    public partial class SeedTransferRequestView : SecurePage
    {
        UserMasterService userMasterService;
        FormOneDetailsService form1Service;
        SourceVerificationService sourceVerificationService;
        Common common = new Common();

        Form1PaymentDetailService form1PaymentDetailService;
        FarmerDetailsService farmerDetailsService;
        CropMasterService cropMasterService;

        public SeedTransferRequestView()
        {
            form1Service = new FormOneDetailsService();
            userMasterService = new UserMasterService();
            sourceVerificationService = new SourceVerificationService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            farmerDetailsService = new FarmerDetailsService();
            cropMasterService = new CropMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("STRL"); //litNote.Text = common.getNote("STRL"); // Short name of the page
                if (Request.QueryString["Id"] != null)
                {
                    int sourceverificationID = 9036;
                    Session["SVID"] = sourceverificationID;
                }
                else
                {

                }
                if (!IsPostBack)
                {
                    InitComponent();
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                //Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent()
        {

            var u = userMasterService.GetFromAuthCookie();
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);  
            SeedTransferGridView.DataSource = SelectMethod();
            SeedTransferGridView.DataBind();
            
        }


        protected void SeedTransferGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
              //  HiddenField hdnId = (e.Row.FindControl("hdnID") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);

                if (hdnStatusCode.Value == "9")
                {
                    lblStatus.Text = "Tranfer Request Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    //lnkDelete.Enabled = false;
                }

                else if (hdnStatusCode.Value == "4")
                {
                    lblStatus.Text = "Sent To ADSC";
                    lblStatus.ForeColor = System.Drawing.Color.Orange;
                }
                else if (hdnStatusCode.Value == "8")
                {
                    lblStatus.Text = "Transfer Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else if (hdnStatusCode.Value == "5")
                {
                    lblStatus.Text = "Approved";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                }
                else   
                {
                    lblStatus.Text = "Request Rejected";
                    lblStatus.ForeColor = System.Drawing.Color.Red;


                    // lnkForm1.Enabled = false;
                } 

            }
        } 
        public DataTable SelectMethod()
        {

            string CS = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
            var user = userMasterService.GetFromAuthCookie();
            int userId = user.Id;
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);

            if (roleId == 4)
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    String tranactionQuery = @"SELECT  sbt.Id, sbt.SourceVerificationID,um.MobileNo,dm.Name as District, tm.Name as Taluk, 
                       sbt.QuantityofSeed as Quantity, sbt.DateofRequest as RequestDate, um.Name as SourceLocation,
					   um1.Name as Destinationlocation,sbt.LotId as lotid,  sbt.StatusCode as StatusCode
                           FROM  SeedBranchTransfer as sbt
						 inner join SeedBranchTransferTransaction stt on stt.SeedBranchTransferID = sbt.Id   
                         inner join  SourceVerification sv on sbt.SourceVerificationID = sv.Id
						 inner join UserMaster um on sv.Producer_Id = um.Id
						 inner join DistrictMaster dm on um.District_Id = dm.Id
						 inner join TalukMaster tm on um.Taluk_Id = tm.Id
						 inner join  UserMaster um1 on sbt.DestinationLocation = um1.Id
						 where stt.ToID=@uid";
                    SqlCommand transactionCmd = new SqlCommand(tranactionQuery, con);
                    transactionCmd.Parameters.AddWithValue("@uid", userId);
                    con.Open();
                    var dataReader = transactionCmd.ExecuteReader();
                    dataReader.Read();
                    DataTable dt = new DataTable();
                    dt.Load(dataReader);
                     
                    
                    return dt;
                }
            }
            else
            {
                using (SqlConnection con = new SqlConnection(CS))
                {
                    String tranactionQuery = @"SELECT  sbt.Id, sbt.SourceVerificationID,um.MobileNo,dm.Name as District, tm.Name as Taluk, 
                       sbt.QuantityofSeed as Quantity, sbt.DateofRequest as RequestDate, um.Name as SourceLocation,
					   um1.Name as Destinationlocation,sbt.LotId as lotid, sbt.StatusCode as StatusCode
                           FROM  SeedBranchTransfer as sbt
						 inner join SeedBranchTransferTransaction stt on stt.SeedBranchTransferID = sbt.Id   
                         inner join  SourceVerification sv on sbt.SourceVerificationID = sv.Id
						 inner join UserMaster um on sv.Producer_Id = um.Id
						 inner join DistrictMaster dm on um.District_Id = dm.Id
						 inner join TalukMaster tm on um.Taluk_Id = tm.Id
						 inner join  UserMaster um1 on sbt.DestinationLocation = um1.Id
						 where stt.FromID='" + userId + "'";
                    SqlCommand transactionCmd = new SqlCommand(tranactionQuery, con);
                    con.Open();
                    var dataReader = transactionCmd.ExecuteReader();
                    
                    DataTable dt = new DataTable();
                    dt.Load(dataReader);
                    return dt;
                }
            }
        }
        protected void SeedTransferGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SeedTransferGridView.PageIndex = e.NewPageIndex;
            SeedTransferGridView.DataSource = SelectMethod();
            SeedTransferGridView.DataBind();
        }

    }
}