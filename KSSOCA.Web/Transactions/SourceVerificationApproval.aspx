﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SourceVerificationApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.SourceVerificationApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlprint.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>SPU Registration Certificate</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <asp:Panel ID="pnlprint" runat="server">
                    <div class="MainHeading">
                        <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    </div>
                    <br />
                    <div>
                        <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                        <asp:HiddenField ID="Id" runat="server" />
                    </div>
                    <br />
                    <%--class="table-condensed" border="1" --%>
                    <table width="100%" class="table-condensed" border="1">

                        <tr>
                            <td colspan="4" class="Note">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="2" class="SideHeading">1.Producer's Details
                            </td>
                            <td colspan="2">SV No.:<asp:Label Font-Bold="true" ID="lblRefNo" runat="server"></asp:Label>
                                &nbsp;
                            Date:
                            <asp:Label Font-Bold="true" runat="server" ID="RegistrationDate"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td width="25%">1.Producer's Registration Number </td>
                            <td width="25%">
                                <asp:Label Font-Bold="true" runat="server" ID="PrRegistrationNumber"></asp:Label>
                            </td>

                            <td width="25%">2.Name of Firm/Institution </td>
                            <td width="25%">
                                <asp:Label Font-Bold="true" runat="server" ID="FirmProducerName" />
                            </td>
                        </tr>
                        <tr>
                            <td>3.Name of the Producer(Official)  
                            </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="PrName" />
                            </td>

                            <td>4.Mobile Number </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="MobileNumber" /></td>
                        </tr>
                        <tr>
                            <td>5.Email Id </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="EmailId" /></td>
                            <td>6.District </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblDistrict" /></td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">2.Source Details
                            </td>
                        </tr>
                        <tr>
                            <td>1.Crop  
                            </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblCrop" /></td>
                            <td>2.<asp:Label ID="lblCropTypeName" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblVariety" /></td>
                        </tr>
                        <tr>
                            <td>3.Class of Seed</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblClassSeed" /></td>
                            <td>4.Area Eligible for certification(in Acres) </td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblArea" />&nbsp; Acres</td>
                        </tr>
                        <tr>
                            <td colspan="4">5.Lot  Details.  &nbsp;&nbsp;&nbsp;   Source Type:<asp:Label ID="lblsourcetype" runat="server" Font-Bold="true"></asp:Label>
                                &nbsp;&nbsp;&nbsp;   Crop Type:<asp:Label ID="lblCropType" runat="server" Font-Bold="true"></asp:Label>
                                <br />
                                 
                                <asp:GridView ID="gvLotNos" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl.No.">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %> . 
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                                        <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed(in Kg.)" NullDisplayText="--" />
                                        <asp:BoundField DataField="SeedRate" HeaderText="Seed Rate(Kg/Acre)" NullDisplayText="--" />
                                        <asp:TemplateField HeaderText="Date of Validity/Test">
                                            <ItemTemplate>
                                                <asp:Label ID="lblValidPeriod" runat="server"
                                                    Text='<%#Eval("ValidPeriod", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="PurchaseBillNo" HeaderText="Purchase bill/SR No." NullDisplayText="--" />
                                        <asp:TemplateField HeaderText="Purchase bill/SR Date.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPurchaseBillDate" runat="server"
                                                    Text='<%#Eval("PurchaseBillDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="ReleaseOrderNo" HeaderText="Release Order/BC No." NullDisplayText="--" />
                                        <asp:TemplateField HeaderText="Release Order/BC Date.">
                                            <ItemTemplate>
                                                <asp:Label ID="lblReleaseOrderDate" runat="server"
                                                    Text='<%#Eval("ReleaseOrderDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                         

                                              <asp:HyperLinkField Text="Transfer"   DataNavigateUrlFields="Id" HeaderText="Seed Transfer"
                                           DataNavigateUrlFormatString="~/Transactions/SeedTransferRequestCreate.aspx?Id={0}" ></asp:HyperLinkField>
                                       
                                    </Columns>
                                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                                    <FooterStyle Font-Bold="true" />
                                </asp:GridView>
                                
                                <br />

                                6.Total Quantity of Seed :
                            <asp:Label Font-Bold="true" runat="server" ID="lblQuantitySeed" />Kg.
                            </td>

                        </tr>

                        <tr>
                            <td valign="top">7.Season  :
                            <asp:Label Font-Bold="true" runat="server" ID="lblSeason" /></td>
                            <td valign="top" colspan="3">8.Location of the Seed Stock : 
                            <asp:Label Font-Bold="true" runat="server" ID="lblSeedLocation" /></td>

                        </tr>
                        <tr>
                            <td colspan="4" valign="top">9.Tag Numbers
                            <br />
                                <asp:TextBox ID="txttagnumbers" runat="server" Height="100px" TextMode="MultiLine" Width="100%" Enabled="false">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">3.Documents Submitted</td>
                        </tr>
                        <tr>
                            <td valign="top">1.&nbsp;<asp:Label runat="server" ID="lblDocReleaseOrder"></asp:Label>
                                &nbsp;&nbsp;
                            <asp:Literal runat="server" ID="LitReleaseOrder" EnableViewState="false"></asp:Literal>
                            </td>
                            <td valign="top">2.&nbsp;<asp:Label runat="server" ID="lblDocPurchaseBill"></asp:Label>
                                &nbsp;&nbsp;
                        <asp:Literal runat="server" ID="LitPuchaseBill" EnableViewState="false"></asp:Literal>
                            </td>

                            <td valign="top">3.&nbsp;BSP -III 
                         &nbsp;&nbsp;
                        <asp:Literal runat="server" ID="litbreedercertificate" EnableViewState="false"></asp:Literal>
                            </td>
                            <td valign="top">4.&nbsp;Tag Image (any 1 for each lot) 
                        &nbsp;&nbsp;
                        <asp:Literal runat="server" ID="litTags" EnableViewState="false"></asp:Literal>
                            </td>

                        </tr>

                        <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                            <tr>
                                <td valign="top">Remarks</td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="100%" Height="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtRemarks" ErrorMessage="Give Remarks"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-danger" ValidationGroup="v1" CausesValidation="true"
                                        ID="btnReject" Text="Request to Resubmit" OnClick="btnReject_Click" />
                                    <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1" CausesValidation="true"
                                        ID="Save" Text="Verify" OnClick="Save_Click" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr class="SideHeading">
                            <td colspan="4">4.Application Status&nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">Note: Sl.No. 1 is the current status of the application.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText=" Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText=" Date & Time" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
            </div>
        </ContentTemplate>
        <%--  <Triggers>
            <asp:PostBackTrigger ControlID="Save" />

        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
