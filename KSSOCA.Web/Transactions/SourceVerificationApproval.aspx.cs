﻿namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Collections.Generic;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using KSSOCA.Model;
    using KSSOCA.Core.Helper;
    using KSSOCA.Core.Validators;
    using System.Web.UI.WebControls;
    using System.Web.UI;
    using System.Data;
    using System.Data.SqlClient;
    using System.Configuration;

    public partial class SourceVerificationApproval : SecurePage
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        SeasonMasterService seasonMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceVerificationService;
        RegistrationFormService registrationFormService;
        DistrictMasterService districtMasterService; Common common = new Common();
        SourceVerificationTransactionService sourceVerificationTransactionService;
        SVLotNumbersService svLotNumbersService;
        TalukMasterService talukService;
        DistrictMasterService districtService;
        public SourceVerificationApproval()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            seasonMasterService = new SeasonMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            sourceVerificationService = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            districtMasterService = new DistrictMasterService();
            sourceVerificationTransactionService = new SourceVerificationTransactionService();
            svLotNumbersService = new SVLotNumbersService();
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("SVA"); litNote.Text = common.getNote("SVA");
                if (Request.QueryString["Id"] != null)
                {
                    int id = Request.QueryString.Get("Id").ToInt();

                    if (id > 0)
                    {
                        SourceVerification sourceverification = sourceVerificationService.ReadById(id);
                        if (sourceverification != null)
                        {
                            InitComponent(sourceverification);
                            
                        }
                        else
                        {
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                Save.Enabled = false; Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent(SourceVerification sv)
        {
            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
            var userMaster = userMasterService.GetFromAuthCookie();

            Id.Value = sv.Id.ToString(); //source verification id to hidden field
            // registration details
            RegistrationForm registrationForm = registrationFormService.GetByUserId(sv.Producer_Id);
            PrRegistrationNumber.Text = sv.ProducerRegNo;
            var ProducerUserDetails = userMasterService.GetById(Convert.ToInt16(registrationForm.ProducerID));
            FirmProducerName.Text = ProducerUserDetails.Name;
            PrName.Text = registrationForm.OfficialName;
            EmailId.Text = ProducerUserDetails.EmailId;
            MobileNumber.Text = ProducerUserDetails.MobileNo;
            lblDistrict.Text = districtMasterService.GetById(Convert.ToInt16(ProducerUserDetails.District_Id)).Name;
            // Source verification details
            lblCrop.Text = cropMasterService.GetById(Convert.ToInt16(sv.Crop_Id)).Name;
            lblVariety.Text = cropVarietyMasterService.GetById(Convert.ToInt16(sv.Variety_Id)).NameSV;
             
                
                gvLotNos.DataSource = svLotNumbersService.GetBySVId(sv.Id);
                gvLotNos.DataBind();
                SetLabelNames(sv);
             
            
            lblQuantitySeed.Text = Convert.ToInt32(sv.QuantityofSeed).ToString();

            lblArea.Text = sv.AreainAcre.ToString();
            lblSeason.Text = seasonMasterService.GetById(Convert.ToInt16(sv.Season_Id)).Name;
            lblSeedLocation.Text = sv.LocationofSeedStock + " Taluk:" + talukService.GetBy(sv.Stk_District_Id, sv.Stk_Taluk_Id).Name + ", District:" + districtMasterService.GetById(sv.Stk_District_Id).Name;
            txttagnumbers.Text = sv.TagNumbers;
            lblClassSeed.Text = classSeedMasterService.GetById(Convert.ToInt16(sv.ClassSeed_Id)).Name;
            lblCropTypeName.Text = sv.CropType == "H" ? "Parentage" : "Variety";
            lblCropType.Text = sv.CropType == "H" ? "Hybrid" : "Non-Hybrid";
            lblRefNo.Text = sv.Id.ToString();
            RegistrationDate.Text = sv.RegistrationDate.ToString("dd/MM/yyyy");
            if (sv.ReleaseOrderimage != null && sv.ReleaseOrderimage.Length > 0)
                LitReleaseOrder.Text = string.Format(previewString, Serializer.Base64String(sv.ReleaseOrderimage));

            if (sv.PurchaseBillimage != null && sv.PurchaseBillimage.Length > 0)
                LitPuchaseBill.Text = string.Format(previewString, Serializer.Base64String(sv.PurchaseBillimage));

            if (sv.BCimage != null && sv.BCimage.Length > 0)
                litbreedercertificate.Text = string.Format(previewString, Serializer.Base64String(sv.BCimage));

            if (sv.Tagsimage != null && sv.Tagsimage.Length > 0)
                litTags.Text = string.Format(previewString, Serializer.Base64String(sv.Tagsimage));

            gvstatus.DataSource = sourceVerificationTransactionService.GetAllTransactionsBy(sv.Id);
            gvstatus.DataBind();

            SourceVerificationTransaction transaction = new SourceVerificationTransaction();
            transaction = sourceVerificationTransactionService.GetMaxTransactionDetails(sv.Id);
            int ActionRole_Id = transaction.ToID; 
            if (userMaster.Role_Id == 5)
            {
                pnlRemarks.Visible = false;
                if (transaction.TransactionStatus == 0)
                {
                    btnEditApplication.Visible = true;
                    btnEditApplication.PostBackUrl = "~/Transactions/SourceVerificationCreate.aspx?Id=" + sv.Id;
                }
                if (sv.Producer_Id != userMasterService.GetFromAuthCookie().Id)
                {
                    this.Redirect("Unauthorized.aspx");
                }
            }
            else
            {
                if (userMaster.Id == ActionRole_Id)
                {
                    pnlRemarks.Visible = true;
                }
                else
                {
                    pnlRemarks.Visible = false;
                }
            }
        }
        private void SetLabelNames(SourceVerification sv)
        {
            lblsourcetype.Text = sv.SourceType == "O" ? "Own" : sv.SourceType == "P" ? "Purchsed" : "Pending GOT";
            var uMaster = userMasterService.GetFromAuthCookie();
           // var lotno = svLotNumbersService.GetById(sv.Id);

            string CS = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ConnectionString;
           

            if (sv.StatusCode == 1  && uMaster.Role_Id ==5)
                    {
                        gvLotNos.HeaderRow.Cells[9].Visible = true;
                        for (int i = 0; i < gvLotNos.Rows.Count; i++)
                        {
                   
                        gvLotNos.Rows[i].Cells[9].Visible = true; 
                        }
                    }
                    else
                    {
                        gvLotNos.HeaderRow.Cells[9].Visible = false;
                        for (int i = 0; i < gvLotNos.Rows.Count; i++)
                        {

                            gvLotNos.Rows[i].Cells[9].Visible = false;

                        }
                    }
                 
              
            if (sv.ClassSeed_Id == 1)
            {
                gvLotNos.HeaderRow.Cells[4].Text = "Date of Test";
                gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                gvLotNos.HeaderRow.Cells[7].Text = "Breeder Certificate No.";
                gvLotNos.HeaderRow.Cells[8].Text = "Date of Breeder Certificate ";
                lblDocReleaseOrder.Text = "Breeder Certificate";
                lblDocPurchaseBill.Text = "Purchase Bill/ST Note/Invoice";
               
            }
            else if (sv.ClassSeed_Id != 0)
            {

                if (sv.SourceType == "O")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order";
                    lblDocPurchaseBill.Text = "Stock Reciept Note";

                }
                else if (sv.SourceType == "P")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill Number";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order";
                    lblDocPurchaseBill.Text = "Purchase Bill/ST Note/Invoice";
                }
                else if (sv.SourceType == "G")
                {
                    gvLotNos.HeaderRow.Cells[4].Text = "Date of Validity";
                    gvLotNos.HeaderRow.Cells[5].Text = "Purchase Bill/Stock Reciept Note No.";
                    gvLotNos.HeaderRow.Cells[6].Text = "Purchase Bill/Stock Reciept Note Date";
                    gvLotNos.HeaderRow.Cells[7].Text = "Release Order No.";
                    gvLotNos.HeaderRow.Cells[8].Text = "Date of Release Order";

                    lblDocReleaseOrder.Text = "Release Order Under taking letter";
                    lblDocPurchaseBill.Text = "Purchase Bill/Stock Reciept Note";
                }
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                var userMaster = userMasterService.GetFromAuthCookie();
                int RoleID = Convert.ToInt16(userMasterService.GetFromAuthCookie().Role_Id);
                int TransactionStatus = 0;
                int ToID = 0;
                var sv=sourceVerificationService.ReadById(Convert.ToInt16(Id.Value.ToInt()));
               
                if (RoleID == 6)
                {
                    ToID = Convert.ToInt16(sourceVerificationService.ReadById(Convert.ToInt16(Id.Value.ToInt())).Producer_Id);
                    FinalApproval(Id.Value.ToInt());
                    TransactionStatus = 5; //  
                }
               
                var rft = new SourceVerificationTransaction
                {
                    Id = common.GenerateID("SourceVerificationTransaction"),
                    FromID = userMaster.Id,
                    ToID = ToID,
                    SourceVerificationID = Id.Value.ToInt(),
                    TransactionDate = System.DateTime.Now,
                    TransactionStatus = TransactionStatus,
                    Remarks = txtRemarks.Text,
                    IsCurrentAction = 1
                };
                int IsCurrentUserSet = common.setIsCurrentAction("SourceVerificationTransaction", "SourceVerificationID", Id.Value.ToInt());
                if (IsCurrentUserSet > 0)
                {
                    int result = sourceVerificationTransactionService.Create(rft);
                    if (result == 1)
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Green;
                        lblMessage.Text = "Approved successfully.";
                        Messaging.SendSMS("Source Verification Tag Number "+sv.TagNumbers+" Has Been Approved By " + userMaster.Name + "", MobileNumber.Text, "1");
                        Page_Load(sender, e);
                    }
                    else
                    {
                        lblMessage.Text = "Error occurred. Please contact administrator.";
                        Save.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
        private int FinalApproval(int SourceVerificationId)
        {
            string query = "update SourceVerification set StatusCode=1  where Id= " + SourceVerificationId;
            return common.ExecuteNonQuery(query);
        }
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Dashboard.aspx");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    int RoleID = Convert.ToInt16(userMaster.Role_Id);

                    SourceVerification sv = sourceVerificationService.ReadById(Id.Value.ToInt());
                    var rft = new SourceVerificationTransaction
                    {
                        Id = common.GenerateID("SourceVerificationTransaction"),
                        FromID = userMaster.Id,
                        ToID = Convert.ToInt16(sv.Producer_Id),
                        SourceVerificationID = Id.Value.ToInt(),
                        TransactionDate = System.DateTime.Now,
                        TransactionStatus = 0, // indicates rejection
                        Remarks = txtRemarks.Text,
                        IsCurrentAction = 1
                    };
                    int IsCurrentUserSet = common.setIsCurrentAction("SourceVerificationTransaction", "SourceVerificationID", Id.Value.ToInt());
                    if (IsCurrentUserSet > 0)
                    {
                        int result = sourceVerificationTransactionService.Create(rft);
                        if (result == 1)
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Rejected successfully.";
                            Messaging.SendSMS("Source Verification Tag Number " + sv.TagNumbers + " has been rejected by " + userMaster.Name + "", MobileNumber.Text, "1");
                            Page_Load(sender, e);
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred. Please contact administrator.";
                            Save.Enabled = false;
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Validation error occurred.";
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }
    }
}