﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SourceVerificationCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.SourceVerificationCreate" %>

<%@ Register Namespace="KSSOCA.Core.Validators" TagPrefix="cst" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='<%= ResolveUrl("~/css/registration.css")%>' rel="stylesheet" />
    <link href="../css/UpdateProgress.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upnl">
        <ProgressTemplate>
            <div class="modal">
                <div class="center">
                    <img alt="" src="img/loading.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnLotno" runat="server" />
                    <asp:HiddenField ID="hdnReleaseOrderimage" runat="server" />
                    <asp:HiddenField ID="hdnPurchaseBillimage" runat="server" />
                    <asp:HiddenField ID="hdnBCimage" runat="server" />
                    <asp:HiddenField ID="hdnTagsimage" runat="server" />
                    <asp:HiddenField ID="hdnAvgSeedRate" runat="server" />
                </div>

                <br />
                <div class="table-responsive">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td colspan="4">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">1.Basic Details of Seed Source &nbsp;&nbsp;<asp:Button ID="btnViewApplication" runat="server" Text="View Application Status" Visible="false" CausesValidation="false" />
                            </td>
                        </tr> 
                        <tr>
                            <td width="25%">1.Crop Type<span style="color: red">*</span></td>
                            <td width="25%">
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbcroptype" CausesValidation="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbcroptype_SelectedIndexChanged">
                                    <asp:ListItem class="radio-inline" Value="H" Text="Hybrid" Selected="True"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="N" Text="Non-Hybrid"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator runat="server" ID="Requiredrbcroptype" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="rbcroptype" ErrorMessage="Crop Type Required"></asp:RequiredFieldValidator>
                            </td>
                            <td width="25%">2.Crop<span style="color: red">*</span></td>
                            <td width="25%">
                                <asp:DropDownList runat="server" CssClass="form-control" ID="Crop" AutoPostBack="true" OnSelectedIndexChanged="Crop_SelectedIndexChanged">
                                </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="RequiredCrop" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="Crop" ErrorMessage="Crop Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>3.Variety<span style="color: red">*</span></td>
                            <td>
                                <asp:DropDownList runat="server" CssClass="form-control" ID="Variety" AutoPostBack="true" OnSelectedIndexChanged="Variety_SelectedIndexChanged">
                                </asp:DropDownList>
                                
                                <asp:Label ID="lblCropType" runat="server" Font-Bold="true"></asp:Label>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredVariety" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="Variety" ErrorMessage="Variety Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td>4.Class of Seed<span style="color: red">*</span></td>
                            <td>
                                <asp:DropDownList runat="server" CssClass="form-control dynamic-classSeed" ID="ClassSeed" AutoPostBack="true" OnSelectedIndexChanged="ClassSeed_SelectedIndexChanged">
                                </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="RequiredClassSeed" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ClassSeed" ErrorMessage="Required Class of Seed" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>

                            <td>5.Season<span style="color: red">*</span></td>
                            <td>
                                <asp:DropDownList runat="server" CssClass="form-control" ID="Season">
                                </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="RequiredSeason" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="Season" ErrorMessage="Season Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                            <td colspan="2">
                                <span style="color:red;font-weight:bold;font-family:Verdana"> ಸೂಚನೆ : ಯಾವುದೇ ಬೆಳೆ/ತಳಿ ಯು ಪಟ್ಟಿಯಲ್ಲಿ ಇಲ್ಲದಿದ್ದರೆ ದಯವಿಟ್ಟು ತಿಳಿಸಿ. KSSOCA HQ - 9448990357</span> 
                            </td>
                        </tr> 
                        <tr>
                            <td colspan="4">6.Source Details<span style="color: red">*</span>&nbsp;&nbsp;Source Type :
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbSourceType" CausesValidation="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbSourceType_SelectedIndexChanged">
                                    <asp:ListItem class="radio-inline" Value="P" Text="Purchased" Selected="True"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="O" Text="Own seed"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="G" Text="Pending GOT"></asp:ListItem>
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredSourceType" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="rbSourceType" ErrorMessage="Required Source Type"></asp:RequiredFieldValidator>
                                <table id="tbllotdetals" runat="server" visible="false" class="table-condensed" width="100%" border="1">
                                    <tr>
                                        <th>Lot Number<span style="color: red">*</span></th>
                                        <th>Seed Rate(Kg/Acre)</th>
                                        <th>Quantity of Seed (in Kg)<span style="color: red">*</span></th>
                                        <th>
                                            <asp:Label runat="server" ID="lblValidPeriod"></asp:Label><span style="color: red">*</span></th>
                                        <th>
                                            <asp:Label runat="server" ID="LabelPurchaseBillNumber"></asp:Label><span style="color: red">*</span>
                                        </th>
                                        <th>
                                            <asp:Label runat="server" ID="LabelPurchaseBillDate"></asp:Label><span style="color: red">*</span></th>

                                        <th>
                                            <asp:Label runat="server" ID="LabelReleaseOrderNumber"></asp:Label><span style="color: red">*</span></th>
                                        <th>
                                            <asp:Label runat="server" ID="LabelReleaseOrderDate"></asp:Label><span style="color: red">*</span></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="LotNumber" placeholder="" Style="text-transform: uppercase" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredLotNumber" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="LotNumber" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" class="form-control" ID="txtseedrate" onkeypress="return CheckDecimal(event);" placeholder="" />
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="txtseedrate" ErrorMessage="Required" ValidationGroup='v1'></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="QuantityOfSeed" placeholder="" onkeypress="return CheckDecimal(event);" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredQuantityOfSeed" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="QuantityOfSeed" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ValidationGroup='v1' runat="server" ErrorMessage="Enter valid number" ForeColor="Red" Display="Dynamic" ControlToValidate="QuantityOfSeed"
                                                ValidationExpression="^[1-9]\d*(\.\d+)?$"></asp:RegularExpressionValidator></td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="ValidPeriod" placeholder="Date" onkeydown="return false" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="ValidPeriodRequired" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="ValidPeriod" ErrorMessage="Required"></asp:RequiredFieldValidator></td>

                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="PurchaseBillNumber" placeholder=" " onkeypress="return CheckNumber(event);" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredPurchaseBillNumber" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="PurchaseBillNumber" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="PurchaseBillDate" placeholder="Date" onkeydown="return false" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredPurchaseBillDate" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="PurchaseBillDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="ReleaseOrderNumber" placeholder=" " onkeypress="return CheckNumber(event);" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredReleaseOrderNumber" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="ReleaseOrderNumber" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox runat="server" CssClass="form-control" ID="ReleaseOrderDate" placeholder="Date" onkeydown="return false" />
                                            <asp:RequiredFieldValidator ValidationGroup='v1' runat="server" ID="RequiredReleaseOrderDate" ForeColor="Red" Display="Dynamic"
                                                ControlToValidate="ReleaseOrderDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:Button ID="btnadd" ValidationGroup='v1' runat="server" Text="Add" CssClass="btn-primary" OnClick="btnadd_Click" CausesValidation="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9">
                                            <asp:GridView ID="gvLotNos" runat="server" class="table-condensed" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sl.No.">
                                                        <ItemTemplate>
                                                            <%#Container.DataItemIndex+1 %> . 
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <Columns>
                                                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                                                    <asp:BoundField DataField="SeedRate" HeaderText="Seed Rate(Kg/Acre)" />
                                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed(in Kg.)" />
                                                    <asp:TemplateField HeaderText="Date of Validity/Test">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblValidPeriod" runat="server"
                                                                Text='<%#Eval("ValidPeriod", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="PurchaseBillNo" HeaderText="Purchase bill/SR No." />

                                                    <asp:TemplateField HeaderText="Purchase bill/SR Date.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPurchaseBillDate" runat="server"
                                                                Text='<%#Eval("PurchaseBillDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ReleaseOrderNo" HeaderText="Release Order/BC No." />

                                                    <asp:TemplateField HeaderText="Release Order/BC Date.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReleaseOrderDate" runat="server"
                                                                Text='<%#Eval("ReleaseOrderDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" Text="delete" CausesValidation="false" CommandArgument='<%#Eval("LotNumber") %>' OnClick="lnkdelete_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>----------Source not Added-----------</EmptyDataTemplate>
                                                <FooterStyle Font-Bold="true" />
                                            </asp:GridView>
                                            Note: Click on Add Button after entering the lot no and quantity for each lot. 
                                            <br />
                                            Total Quantity :
                                            <asp:Label runat="server" ID="lblTotal" Font-Bold="true" Text="0"></asp:Label>
                                            Kg.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>7.Area Eligible(in Acre) <span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" class="form-control" ID="AreainAcre" placeholder="Area" ReadOnly="true" />
                                <asp:RequiredFieldValidator runat="server" ID="AreainAcreRequired" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="AreainAcre" ErrorMessage="Area in Acre can't be empty"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ForeColor="Red" Display="Dynamic"
                                    ValidationExpression="(?<=^|)\d+(\.\d+)?(?=$|)" ControlToValidate="AreainAcre"
                                    ErrorMessage="Area in Acre must be number"></asp:RegularExpressionValidator>
                            </td>
                            <td>8.Stock Located District/State <span style="color: red">*</span></td>
                            <td>
                                <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredDistrict" Style="color: red" Display="Dynamic"
                                    ControlToValidate="District" ErrorMessage="District Required" InitialValue="0"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>9.Stock Located Taluk <span class="text-danger">*</span></td>
                            <td>
                                <asp:DropDownList runat="server" class="form-control" ID="Taluk">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredTaluk" Style="color: red" Display="Dynamic"
                                    ControlToValidate="Taluk" ErrorMessage="Taluk Required" InitialValue="0"></asp:RequiredFieldValidator></td>

                            <td>10.Full Address of the Seed Stock Located<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox runat="server" class="form-control" ID="LocationSeedStock" />
                                <asp:RequiredFieldValidator runat="server" ID="LocationSeedStockRequired" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="LocationSeedStock" ErrorMessage="Location can't be empty"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">11.Tag Numbers<span style="color: red">*</span></td>
                            <td colspan="3">
                                <asp:TextBox runat="server" ID="TagNumbers" placeholder="Tag Numbers" AutoPostBack="true" TextMode="MultiLine" Width="100%" Height="100px" OnTextChanged="TagNumbers_TextChanged" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredTagNumbers" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="TagNumbers" ErrorMessage="Tag Numbers can't be empty"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">2.Documents to be uploaded
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">1.
                                <asp:Label runat="server" Text="Release Order" ID="lblDocReleaseOrder"></asp:Label><span style="color: red">*</span>

                                <asp:FileUpload runat="server" CssClass="form-control" ID="ReleaseOrder" accept="image/*" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredReleaseOrder" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ReleaseOrder" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <cst:FileTypeValidator id="ReleaseOrderTypeValidator" runat="server"
                                    ControlToValidate="ReleaseOrder"
                                    ForeColor="Red" Display="Dynamic"
                                    ErrorMessage="Allowed file types are {0}.">
                                </cst:FileTypeValidator>
                                <cst:FileSizeValidator runat="server" id="FileSizefu_Sign" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ReleaseOrder" ErrorMessage="File size must not exceed {0} KB">
                                </cst:FileSizeValidator>
                                <asp:Literal ID="LitReleaseOrder" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                            <td valign="top">2.
                                <asp:Label runat="server" Text="Purchase Bill/ST Note/Invoice" ID="lblDocPurchaseBill"></asp:Label><span style="color: red">*</span>

                                <asp:FileUpload runat="server" CssClass="form-control" ID="PurchaseBill" accept="image/*" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredPurchaseBill" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="PurchaseBill" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <cst:FileSizeValidator runat="server" id="FileSizeValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="PurchaseBill" ErrorMessage="File size must not exceed {0} KB">
                                </cst:FileSizeValidator>
                                <cst:FileTypeValidator id="PurchaseBillFileSizeValidatorTypeValidator" runat="server"
                                    ControlToValidate="PurchaseBill"
                                    ForeColor="Red" Display="Dynamic"
                                    ErrorMessage="Allowed file types are {0}.">
                                </cst:FileTypeValidator>
                                <asp:Literal ID="LitPurchaseBill" runat="server" EnableViewState="false"></asp:Literal>
                            </td>

                            <td valign="top">3.Sample Tags Image<span style="color: red">*</span>
                                <asp:FileUpload runat="server" CssClass="form-control" ID="TagsImage" accept="image/*" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredTagsImage" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="TagsImage" ErrorMessage="Required"></asp:RequiredFieldValidator>
                                <cst:FileSizeValidator runat="server" id="FileSizeValidator2" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="TagsImage" ErrorMessage="File size must not exceed {0} KB">
                                </cst:FileSizeValidator>
                                <cst:FileTypeValidator id="TagsImageTypeValidator" runat="server"
                                    ControlToValidate="TagsImage"
                                    ForeColor="Red" Display="Dynamic"
                                    ErrorMessage="Allowed file types are {0}.">
                                </cst:FileTypeValidator><asp:Literal ID="LitTagsImage" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                            <td valign="top">4.BSP-III<asp:Label runat="server" ForeColor="Red" Text="*" ID="lblBSP" Visible="false"></asp:Label>
                                <asp:FileUpload runat="server" CssClass="form-control" ID="BreederCertification" accept="image/*" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredBreederCertification" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="BreederCertification" ErrorMessage="Required" Enabled="false"></asp:RequiredFieldValidator>
                                <cst:FileSizeValidator runat="server" id="FileSizeValidator3" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="BreederCertification" ErrorMessage="File size must not exceed {0} KB">
                                </cst:FileSizeValidator>
                                <cst:FileTypeValidator id="BreederCertificationTypeValidator" runat="server"
                                    ControlToValidate="BreederCertification"
                                    ForeColor="Red" Display="Dynamic"
                                    ErrorMessage="Allowed file types are {0}.">
                                </cst:FileTypeValidator>
                                <asp:Literal ID="LitBreederCertification" runat="server" EnableViewState="false"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <div>
                        <div align="center">
                            <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                ID="btnUpdate" Text="Re-Save" OnClick="btnUpdate_Click" Visible="false" />
                            <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                ID="SorceVerificationSave" Text="Save & Send" OnClick="Save_Click" />
                        </div>
                        <span class="error" style="color: Red; display: none">* Input digits (0 - 9)</span>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="SorceVerificationSave" />
            <asp:PostBackTrigger ControlID="btnUpdate" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
    </script>
    <script type="text/javascript">
        $(function () {
            $('#btnadd').click(function () {
                debugger;
                var ValidPeriod = new date($('#ValidPeriod').text());
                var ReleaseOrderDate = new date($('#ReleaseOrderDate').text());
                var PurchaseBillDate = new date($('#PurchaseBillDate').text());

                var lblValidPeriod = new date($('#lblValidPeriod').text());
                var lblReleaseOrderDate = new date($('#LabelReleaseOrderDate').text());
                var lblPurchaseBillDate = new date($('#LabelPurchaseBillDate').text());

                var TodayDate = new Date();
                if (ValidPeriod > TodayDate) {
                    if (PurchaseBillDate <= TodayDate) {
                        if (ReleaseOrderDate < PurchaseBillDate) {
                        }
                        else {
                            alert('Please Enter Valid ' + lblReleaseOrderDate + '');
                            $('#ReleaseOrderDate').focus();
                            return false;
                        }
                    }
                    else {
                        alert('Please Enter Valid ' + lblPurchaseBillDate + '');
                        $('#PurchaseBillDate').focus();
                        return false;
                    }
                }
                else {
                    alert('Please Enter Valid ' + lblValidPeriod + '');
                    $('#ValidPeriod').focus();
                    return false;
                }
                if (date1 < date2) {
                    alert('Please Enter Date time Greater than Current Date time');
                    $('#txtdate').focus();
                    return false;
                }
            })
        })
    </script>

    <script type="text/javascript" src='<%= ResolveUrl("~/js/SourceVerification.js") %>'></script>
    <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        $(function () {
            $("#TextBox").bind("keypress", function (e) {
                var keyCode = e.which ? e.which : e.keyCode
                var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
                $(".error").css("display", ret ? "none" : "inline");
                return ret;
            });
            $("#TextBox").bind("paste", function (e) {
                return false;
            });
            $("#TextBox").bind("drop", function (e) {
                return false;
            });
        });
    </script>--%>

    <%--<script type="text/javascript">
        var dynamicClassSeed = $(".dynamic-classSeed");

        $(document).ready(function () {
            displayDynamicControl(dynamicClassSeed);
        });

        $(dynamicClassSeed).on('change', function () {
            displayDynamicControl(this);
        });

        function displayDynamicControl(ctrl) {
            var _selectedValue = $(':selected', ctrl).val();
            $('.dynamic-control').slideUp('slow')
            $('.' + _selectedValue).slideDown('slow');
            //$('.dynamic-control').hide()
            //$('.' + _selectedValue).show();
        }
    </script>--%>
    <%-- <script type="text/javascript">
        $('.form-control datepicker').keydown(function (e) {
            e.preventDefault();
            return false;
        });</script>--%>
</asp:Content>
