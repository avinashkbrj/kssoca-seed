﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Security;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Data;
    using System.Collections.Generic;
    using KSSOCA.Core.Helper;
    using System.Web.Script.Serialization;
    using System.Web.UI.WebControls;

    public partial class SourceVerificationCreate : SecurePage
    {
        UserMasterService userMasterService;
        CropMasterService cropMasterService;
        SeasonMasterService seasonMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceVerificationService;
        RegistrationFormService registrationFormService;
        UserwiseDistrictRightsService userwiseDistrictRightsService; Common common = new Common();
        SourceVerificationTransactionService sourceVerificationTransactionService;
        SVLotNumbersService svLotNumbersService;
        TalukMasterService talukService;
        DistrictMasterService districtService;
        public SourceVerificationCreate()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            seasonMasterService = new SeasonMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            sourceVerificationService = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
            sourceVerificationTransactionService = new SourceVerificationTransactionService();
            svLotNumbersService = new SVLotNumbersService();
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Save.Attributes.Add("onclick", "if(typeof (Page_ClientValidate) === 'function' && !Page_ClientValidate()){return false;} this.disabled = true;this.value = 'Working...';" + ClientScript.GetPostBackEventReference(Save, null) + ";");
            try
            {
                if (!Page.IsPostBack)
                {
                    lblHeading.Text = common.getHeading("SVC"); litNote.Text = common.getNote("SVC");
                    RegistrationForm registrationForm = registrationFormService.GetByUserId(userMasterService.GetFromAuthCookie().Id);
                    int firmregapprovalstatus = 0;
                    if (registrationForm != null)
                    {
                        firmregapprovalstatus = registrationForm.StatusCode;
                    }
                    if (firmregapprovalstatus != 0)
                    {
                        if (firmregapprovalstatus == 1)
                        {
                            if (!Page.IsPostBack)
                            {
                                if (registrationFormService.CheckForRegistration(userMasterService.GetFromAuthCookie()) == 0)
                                {
                                    this.Redirect("Transactions/SourceVerificationView.aspx");
                                }
                                InitComponent();
                                SetLabelNames();
                            }
                            // hdnLotno.Value = regCropOfferredService.ConvertToJson(lotnos);
                        }
                        else
                        {
                            this.Redirect("Transactions/FormRegistrationApproval.aspx");
                        }
                    }
                    else
                    {
                        this.Redirect("Transactions/FormRegistrationCreate.aspx");
                    }
                    lblMessage.Text = "";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                SorceVerificationSave.Enabled = false;
                Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            SorceVerificationSave.Attributes.Add("OnClick", "if(typeof (Page_ClientValidate) === 'function' && !Page_ClientValidate()){return false;} this.disabled = true;this.value = 'Working...';" + ClientScript.GetPostBackEventReference(SorceVerificationSave, null) + ";");

            try
            {
                SorceVerificationSave.Visible = false;
                SorceVerificationSave.Enabled = false;
                if (Page.IsValid)
                {
                    
                    DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
                    if (_dt != null)
                    {
                        var userMaster = userMasterService.GetFromAuthCookie();
                        var rf = registrationFormService.GetByUserId(userMaster.Id);
                        int result = sourceVerificationService.Create(new Model.SourceVerification()
                        {
                            Id = common.GenerateID("SourceVerification"),
                            Producer_Id = userMaster.Id,
                            Crop_Id = Crop.SelectedItem.Value.ToInt(),
                            Variety_Id = Variety.SelectedItem.Value.ToInt(),
                            ClassSeed_Id = ClassSeed.SelectedItem.Value.ToInt(),
                            QuantityofSeed = lblTotal.Text.ToDecimal(),
                            AreainAcre = AreainAcre.Text.ToDecimal(),
                            SeedRate = hdnAvgSeedRate.Value.ToDecimal(),
                            Season_Id = Season.SelectedItem.Value.ToInt(),
                            LocationofSeedStock = LocationSeedStock.Text,
                            RegistrationDate = System.DateTime.Now,
                            TagNumbers = TagNumbers.Text,
                            ReleaseOrderimage = ReleaseOrder.FileBytes.Length > 0 ? ReleaseOrder.FileBytes : null,
                            PurchaseBillimage = PurchaseBill.FileBytes.Length > 0 ? PurchaseBill.FileBytes : null,
                            BCimage = BreederCertification.FileBytes.Length > 0 ? BreederCertification.FileBytes : null,
                            Tagsimage = TagsImage.FileBytes.Length > 0 ? TagsImage.FileBytes : null,
                            Stk_District_Id = District.SelectedValue.ToInt(),
                            Stk_Taluk_Id = Taluk.SelectedValue.ToInt(),
                            SourceType = rbSourceType.SelectedValue,
                            StatusCode = 0,
                            FinancialYear = System.DateTime.Now.ToFinancialYear(),
                            CropType = "H",
                            ProducerRegNo = rf.RegistrationNo

                        });
                        if (result > 0)
                        {
                            string query = "select Max(Id) from SourceVerification where Producer_Id=" + userMaster.Id + "";
                            int SourceVerificationID = Convert.ToInt16(common.ExecuteScalar(query));
                            if (insertLotNumbers(SourceVerificationID, _dt) > 0)
                            {
                                int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                          (Convert.ToInt16(District.SelectedValue),
                                           Convert.ToInt16(Taluk.SelectedValue), 6).User_Id); // get SCO
                                var scoMobileNumber = userMasterService.GetById(ToID).MobileNo;
                                var svt = new SourceVerificationTransaction
                                {
                                    Id = common.GenerateID("SourceVerificationTransaction"),
                                    FromID = userMaster.Id,
                                    ToID = ToID,
                                    SourceVerificationID = SourceVerificationID,
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = 6,
                                    IsCurrentAction = 1
                                };
                                int Transactionresult = sourceVerificationTransactionService.Create(svt);
                                Messaging.SendSMS("Source has been sent by " + userMaster.Name + " Tag Number " + TagNumbers.Text + " for verification ", scoMobileNumber, "1");
                                ClearControl();
                                lblMessage.Text = "Saved successfully.";
                                this.Redirect("Transactions/SourceVerificationApproval.aspx?Id=" + SourceVerificationID, false);
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred while saving record.";
                        }
                    }
                    else
                    {
                        lblMessage.Text = "Please enter lot details.";
                    }
                }
                else
                {
                    lblMessage.Text = "Validation error occurred.";
                    SorceVerificationSave.Attributes.Add("onclick", "if(typeof (Page_ClientValidate) === 'function' && !Page_ClientValidate()){return false;} this.disabled = false;this.value = 'Save';" + ClientScript.GetPostBackEventReference(SorceVerificationSave, null) + ";");

                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = ex.Message + ex.InnerException;
            }
        }
        private int insertLotNumbers(int svid, DataTable dtLotsData)
        {
            int recordCount = 0;
            var svLotNumbers = new List<SVLotNumbers>();
            for (int i = 0; i < dtLotsData.Rows.Count; i++)
            {
                svLotNumbers.Add(new SVLotNumbers()
                {
                    LotNumber = Convert.ToString(dtLotsData.Rows[i]["LotNumber"]).Trim().ToUpper().Replace(" ", ""),
                    SeedRate = Convert.ToDecimal(dtLotsData.Rows[i]["SeedRate"]),
                    Quantity = Convert.ToDecimal(dtLotsData.Rows[i]["Quantity"]),
                    PurchaseBillDate = Convert.ToDateTime(dtLotsData.Rows[i]["PurchaseBillDate"]),
                    PurchaseBillNo = Convert.ToString(dtLotsData.Rows[i]["PurchaseBillNo"]),
                    ReleaseOrderDate = Convert.ToDateTime(dtLotsData.Rows[i]["ReleaseOrderDate"]),
                    ReleaseOrderNo = Convert.ToString(dtLotsData.Rows[i]["ReleaseOrderNo"]),
                    ValidPeriod = Convert.ToDateTime(dtLotsData.Rows[i]["ValidPeriod"]),
                    SVID = svid

                });
            }
            return recordCount += svLotNumbersService.Update(svLotNumbers, svid).Count;
        }
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
                if (_dt != null)
                {
                    var userMaster = userMasterService.GetFromAuthCookie();
                    if (Request.QueryString["Id"] != null)
                    {
                        int id = Request.QueryString.Get("Id").ToInt();
                        int result = sourceVerificationService.Update(new Model.SourceVerification()
                        {
                            Id = id,
                            Producer_Id = userMaster.Id,
                            Crop_Id = Crop.SelectedItem.Value.ToInt(),
                            Variety_Id = Variety.SelectedItem.Value.ToInt(),
                            ClassSeed_Id = ClassSeed.SelectedItem.Value.ToInt(),
                            AreainAcre = AreainAcre.Text.ToDecimal(),
                            SeedRate = hdnAvgSeedRate.Value.ToDecimal(),
                            Season_Id = Season.SelectedItem.Value.ToInt(),
                            LocationofSeedStock = LocationSeedStock.Text,
                            TagNumbers = TagNumbers.Text,
                            ReleaseOrderimage = ReleaseOrder.FileBytes.Length > 0 ? ReleaseOrder.FileBytes : Convert.FromBase64String(hdnReleaseOrderimage.Value),
                            PurchaseBillimage = PurchaseBill.FileBytes.Length > 0 ? PurchaseBill.FileBytes : Convert.FromBase64String(hdnPurchaseBillimage.Value),
                            BCimage = BreederCertification.FileBytes.Length > 0 ? BreederCertification.FileBytes : Convert.FromBase64String(hdnBCimage.Value),
                            Tagsimage = TagsImage.FileBytes.Length > 0 ? TagsImage.FileBytes : Convert.FromBase64String(hdnTagsimage.Value),
                            StatusCode = 0,
                            RegistrationDate = System.DateTime.Now,
                            QuantityofSeed = lblTotal.Text.ToDecimal(),
                            SourceType = rbSourceType.SelectedValue,
                            Stk_District_Id = District.SelectedValue.ToInt(),
                            Stk_Taluk_Id = Taluk.SelectedValue.ToInt(),
                        });
                        if (result > 0)
                        {
                            if (insertLotNumbers(id, _dt) > 0)
                            {
                                int SourceVerificationID = Request.QueryString.Get("Id").ToInt();
                                SourceVerificationTransaction transaction = new SourceVerificationTransaction();
                                transaction = sourceVerificationTransactionService.GetMaxTransactionDetails(SourceVerificationID);
                                int ToID = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                        (Convert.ToInt16(District.SelectedValue),
                                         Convert.ToInt16(Taluk.SelectedValue), 6).User_Id);
                                int TransactionStatus = Convert.ToInt16(userMasterService.GetById(transaction.FromID).Role_Id); // Get Last transaction status
                                var scoMobileNumber = userMasterService.GetById(ToID).MobileNo;
                                var svt = new SourceVerificationTransaction
                                {
                                    Id = common.GenerateID("SourceVerificationTransaction"),
                                    FromID = userMaster.Id,
                                    ToID = ToID,
                                    SourceVerificationID = SourceVerificationID,
                                    TransactionDate = System.DateTime.Now,
                                    TransactionStatus = TransactionStatus,
                                    IsCurrentAction = 1
                                };
                                int IsCurrentUserSet = common.setIsCurrentAction("SourceVerificationTransaction", "SourceVerificationID", SourceVerificationID);
                                if (IsCurrentUserSet > 0)
                                {
                                    int Transactionresult = sourceVerificationTransactionService.Create(svt);
                                    Messaging.SendSMS("Source has been sent by " + userMaster.Name + " Tag Number " + TagNumbers.Text + " for Re-Verification ", scoMobileNumber, "1");
                                    lblMessage.Text = "Saved successfully.";
                                    this.Redirect("Transactions/SourceVerificationApproval.aspx?Id=" + SourceVerificationID, false);
                                }
                            }
                            else
                            {
                                lblMessage.Text = "Error occurred while saving record.";
                            }
                        }
                        else
                        {
                            lblMessage.Text = "Error occurred while saving record.";
                        }
                    }
                }
                else
                {
                    lblMessage.Text = "Invalid Source verification Cannot update data.";
                }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                lblMessage.Text = "Error occurred. Please contact administrator.";
            }
        }

        private void SetLabelNames()
        {
            if (ClassSeed.SelectedValue != "0")
            { tbllotdetals.Visible = true; }
            else { tbllotdetals.Visible = false; }


            if (ClassSeed.SelectedValue != "0")
            {
                lblBSP.Visible = false;
                //foreach (ListItem item in rbSourceType.Items)
                //{
                //    if (item.Value == "O" || item.Value == "G")
                //    {
                //        item.Enabled = true;
                //    }
                //}
                if (rbSourceType.SelectedValue == "O")
                {
                    lblValidPeriod.Text = "Date of Validity";
                    LabelReleaseOrderNumber.Text = "Release Order No.";
                    LabelReleaseOrderDate.Text = "Date of Release Order";
                    LabelPurchaseBillDate.Text = "Stock Reciept Note Date";
                    LabelPurchaseBillNumber.Text = "Stock Reciept Note No.";
                    lblDocReleaseOrder.Text = "Release Order";

                    lblDocPurchaseBill.Text = "Stock Reciept Note";
                    RequiredBreederCertification.Enabled = false;
                }
                else if (rbSourceType.SelectedValue == "P")
                {
                    lblValidPeriod.Text = "Date of Validity";
                    LabelReleaseOrderNumber.Text = "Release Order No.";
                    LabelReleaseOrderDate.Text = "Date of Release Order";
                    LabelPurchaseBillDate.Text = "Purchase Bill Date";
                    LabelPurchaseBillNumber.Text = "Purchase Bill No.";
                    lblDocReleaseOrder.Text = "Release Order";
                    RequiredBreederCertification.Enabled = false;
                    lblDocPurchaseBill.Text = "Purchase Bill/ST Note/Invoice";
                }
                else if (rbSourceType.SelectedValue == "G")
                {
                    lblValidPeriod.Text = "Date of Validity";
                    LabelReleaseOrderNumber.Text = "Release Order No.";
                    LabelReleaseOrderDate.Text = "Date of Release Order";
                    LabelPurchaseBillDate.Text = "Purchase Bill/Stock Reciept Note Date";
                    LabelPurchaseBillNumber.Text = "Purchase Bill/Stock Reciept Note No.";
                    lblDocReleaseOrder.Text = "Release Order Under taking letter";
                    RequiredBreederCertification.Enabled = false;
                    lblDocPurchaseBill.Text = "Purchase Bill/Stock Reciept Note";
                }
            }
            if (ClassSeed.SelectedValue == "1")
            {
                lblValidPeriod.Text = "Date of Test";
                LabelReleaseOrderNumber.Text = "Breeder Certificate No.";
                LabelReleaseOrderDate.Text = "Date of Breeder Certificate ";
                // LabelPurchaseBillDate.Text = "Purchase Bill Date";
                //LabelPurchaseBillNumber.Text = "Purchase Bill Number";
                lblDocReleaseOrder.Text = "Breeder Certificate";
                // lblDocPurchaseBill.Text = "Purchase Bill";
                RequiredBreederCertification.Enabled = true;
                lblBSP.Visible = true;
 
            }
        }

        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);

        }
        private void InitComponent()
        {
            var seasons = seasonMasterService.GetAll();
            var classSeed = classSeedMasterService.GetAllClassforCertification();
            Season.BindListItem<SeasonMaster>(seasons);
            ClassSeed.BindListItem<ClassSeedMaster>(classSeed);

            var crops = cropMasterService.GetByType(rbcroptype.SelectedValue.Trim());
            Crop.BindListItem<CropMaster>(crops);
 
            bindVariety(Crop.SelectedValue.ToInt());

            lblCropType.Text =   "";
            var districts = districtService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
            if (Request.QueryString["Id"] != null)
            {
                int id = Request.QueryString.Get("Id").ToInt();
                if (id > 0)
                {
                    SourceVerification sv = sourceVerificationService.ReadById(id);
                    SourceVerificationTransaction transaction = new SourceVerificationTransaction();
                    transaction = sourceVerificationTransactionService.GetMaxTransactionDetails(sv.Id);
                    if (transaction != null)
                    {
                        if (transaction.TransactionStatus == 0 && transaction.ToID == userMasterService.GetFromAuthCookie().Id) //TransactionStatus 0 indicates application is  rejected needs to be edited
                        {
                            lblMessage.Text = "Application has been rejected.";
                            btnUpdate.Visible = true;
                            SorceVerificationSave.Visible = false;
                            btnViewApplication.Visible = true;
                            btnViewApplication.PostBackUrl = "~/Transactions/SourceVerificationApproval.aspx?Id=" + id;
                            string previewString = "<span><a class='popupImage' href='javascript:return void;'><img class='imageresource img-responsive' src='data:image/jpeg;base64,{0}' />View</a></span>";
                            lblCropType.Text = sv.CropType == "H" ? "Parentage" : "Variety";
                            hdnAvgSeedRate.Value = sv.SeedRate.ToString();
                            Crop.Items.Clear();
                            rbcroptype.SelectedValue = sv.CropType;
                            crops = cropMasterService.GetByType(sv.CropType);
                            Crop.BindListItem<CropMaster>(crops);
                            //Crop.Items.FindByValue(sv.Crop_Id.ToString()).Selected = true; //SelectedItem.Value = sv.Crop_Id.ToString(); 
                            Crop.SelectedValue = sv.Crop_Id.ToString();
                            bindVariety(Crop.SelectedValue.ToInt());
                            //Variety.Items.FindByValue(sv.Variety_Id.ToString()).Selected = true;
                            Variety.SelectedValue = sv.Variety_Id.ToString();
                            ClassSeed.ClearSelection();
                            ClassSeed.Items.FindByValue(sv.ClassSeed_Id.ToString()).Selected = true;
                            DataTable _dt = svLotNumbersService.GetBySVId(sv.Id).ToDataTable();
                            gvLotNos.DataSource = _dt;
                            gvLotNos.DataBind();
                            hdnLotno.Value = common.DataTableToJSON(_dt);
                            decimal total = Convert.ToInt32(_dt.Compute("SUM(Quantity)", string.Empty));
                            lblTotal.Text = Convert.ToInt64(total).ToString();
                            AreainAcre.Text = sv.AreainAcre.ToString();
                            //  txtseedrate.Text = Convert.ToInt32(sv.SeedRate).ToString();
                            Season.ClearSelection();
                            Season.Items.FindByValue(sv.Season_Id.ToString()).Selected = true;
                            LocationSeedStock.Text = sv.LocationofSeedStock;
                            TagNumbers.Text = sv.TagNumbers;
                            District.ClearSelection();
                            District.Items.FindByValue(sv.Stk_District_Id.ToString()).Selected = true;

                            Taluk.Items.Clear();
                            Taluk.ClearSelection();
                            taluks = talukService.GetAllbyDistID(Convert.ToInt16(sv.Stk_District_Id));
                            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
                            Taluk.Items.FindByValue(sv.Stk_Taluk_Id.ToString()).Selected = true;
                            hdnReleaseOrderimage.Value = sv.ReleaseOrderimage != null ? Convert.ToBase64String(sv.ReleaseOrderimage) : "";
                            hdnPurchaseBillimage.Value = sv.PurchaseBillimage != null ? Convert.ToBase64String(sv.PurchaseBillimage) : "";
                            hdnBCimage.Value = sv.BCimage != null ? Convert.ToBase64String(sv.BCimage) : "";
                            hdnTagsimage.Value = sv.Tagsimage != null ? Convert.ToBase64String(sv.Tagsimage) : "";

                            if (sv.ReleaseOrderimage != null && sv.ReleaseOrderimage.Length > 0)
                            {
                                LitReleaseOrder.Text = string.Format(previewString, Serializer.Base64String(sv.ReleaseOrderimage));
                                RequiredReleaseOrder.Enabled = false;
                            }
                            if (sv.PurchaseBillimage != null && sv.PurchaseBillimage.Length > 0)
                            {
                                LitPurchaseBill.Text = string.Format(previewString, Serializer.Base64String(sv.PurchaseBillimage));
                                RequiredPurchaseBill.Enabled = false;
                            }
                            if (sv.BCimage != null && sv.BCimage.Length > 0)
                            {
                                LitBreederCertification.Text = string.Format(previewString, Serializer.Base64String(sv.BCimage));
                                RequiredBreederCertification.Enabled = false;
                            }
                            if (sv.Tagsimage != null && sv.Tagsimage.Length > 0)
                                LitTagsImage.Text = string.Format(previewString, Serializer.Base64String(sv.Tagsimage));
                            RequiredTagsImage.Enabled = false;
                        }
                        else
                        {
                            this.Redirect("Transactions/SourceVerificationApproval.aspx?Id=" + id, false);
                        }
                    }
                    else if (transaction == null) // transaction set for renewal
                    {
                        //Transaction
                    }
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Redirect("Transactions/SourceVerificationView.aspx");
        }
        protected void Crop_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindVariety(Crop.SelectedValue.ToInt());
        }
        private void bindVariety(int CropId)
        {
            Variety.Items.Clear();
            var cropVariety = cropVarietyMasterService.GetAllByCropID(Crop.SelectedValue.ToInt());
            Variety.BindListItem<CropVarietyMaster>(cropVariety);

            //var cropVariety = cropVarietyMasterService.GetvarietySV(CropId);

            //Variety.DataSource = cropVariety;
            //Variety.DataValueField = "id";
            //Variety.DataTextField = "Name";
            //Variety.DataBind();

            //Variety.Items.Insert(0, new ListItem("-------Select-------", "0"));
            //Variety.SelectedIndex = 0;

            //  Variety.Items.FindByValue("0").Text = "-----Select-----";

        }
        //protected void txtseedrate_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtseedrate.Text != "")
        //    {
        //        CalculateArea();
        //    }
        //    else
        //    { AreainAcre.Text = "0"; }
        //}
        //protected void QuantityOfSeed_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtseedrate.Text != "")
        //    {
        //        CalculateArea();
        //    }
        //}
        protected void TagNumbers_TextChanged(object sender, EventArgs e)
        {

        }
        protected void btnadd_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                if (gvLotNos.Visible == true)
                {
                    DataTable _dt = new DataTable();
                    _dt.Columns.Add("LotNumber", typeof(string));
                    _dt.Columns.Add("SeedRate", typeof(decimal));
                    _dt.Columns.Add("Quantity", typeof(decimal));
                    _dt.Columns.Add("ValidPeriod", typeof(DateTime));
                    _dt.Columns.Add("ReleaseOrderNo", typeof(string));
                    _dt.Columns.Add("ReleaseOrderDate", typeof(DateTime));
                    _dt.Columns.Add("PurchaseBillNo", typeof(string));
                    _dt.Columns.Add("PurchaseBillDate", typeof(DateTime));

                    _dt.Rows.Add
                           (LotNumber.Text.ToUpper(),
                           Convert.ToDecimal(txtseedrate.Text),
                           Convert.ToDecimal(QuantityOfSeed.Text),
                           ValidPeriod.Text.ToDateTime(),
                           ReleaseOrderNumber.Text,
                           ReleaseOrderDate.Text.ToDateTime(),
                           PurchaseBillNumber.Text,
                           PurchaseBillDate.Text.ToDateTime()
                           );
                    string tbldata = common.DataTableToJSON(_dt);

                    if (hdnLotno.Value.Trim() != "")
                    {
                        hdnLotno.Value = hdnLotno.Value.Replace(']', ',');
                        tbldata = tbldata.Replace('[', ' ');
                    }
                    hdnLotno.Value += tbldata;
                    _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);

                    gvLotNos.DataSource = _dt;
                    gvLotNos.DataBind();
                    CalculateArea();
                }
                LotNumber.Text = "";
                txtseedrate.Text = "";
                QuantityOfSeed.Text = "";
                ValidPeriod.Text = "";
                ReleaseOrderNumber.Text = "";
                ReleaseOrderDate.Text = "";
                PurchaseBillNumber.Text = "";
                PurchaseBillDate.Text = "";
            }
        }


        protected void rbSourceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetLabelNames();
        }

        protected void ClassSeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetLabelNames();
        }

        protected void lnkdelete_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string lotnumber = btn.CommandArgument;
            DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
            for (int i = _dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = _dt.Rows[i];
                if (dr["LotNumber"].ToString() == lotnumber)
                {
                    dr.Delete();
                }
            }
            if (_dt.Rows.Count > 0)
            {
                hdnLotno.Value = common.DataTableToJSON(_dt);
            }
            else { hdnLotno.Value = ""; }
            gvLotNos.DataSource = _dt;
            gvLotNos.DataBind();
            CalculateArea();
        }

        private void CalculateArea()
        {
            DataTable _dt = Serializer.JsonDeSerialize<DataTable>(hdnLotno.Value);
            if (_dt != null)
            {
                decimal total = Convert.ToDecimal(_dt.Compute("SUM(Quantity)", string.Empty));
                decimal AvgSeedRate = Convert.ToDecimal(_dt.Compute("SUM(SeedRate)", string.Empty));
                lblTotal.Text = Convert.ToInt64(total).ToString();
                if (lblTotal.Text != "0")
                {
                    decimal SeedQuantity = Convert.ToDecimal(total);// _dt.AsEnumerable().Sum(row => row.Field<decimal>("Quantity"));
                    decimal SeedRate = Convert.ToDecimal(AvgSeedRate / Convert.ToDecimal(_dt.Rows.Count));

                    decimal area = SeedQuantity / SeedRate;
                    AreainAcre.Text = Math.Round((Double)area, 2).ToString();
                    hdnAvgSeedRate.Value = SeedRate.ToString();
                }
            }
            else
            {
                lblTotal.Text = "0";
                AreainAcre.Text = "0";
            }
        }

        protected void rbcroptype_SelectedIndexChanged(object sender, EventArgs e)
        {
            Crop.Items.Clear();
            var crops = cropMasterService.GetByType(rbcroptype.SelectedValue.Trim());
            Crop.BindListItem<CropMaster>(crops);

            Variety.Items.Clear();
            var cropVariety = cropVarietyMasterService.GetAllByCropID(Crop.SelectedValue.ToInt());
            Variety.BindListItem<CropVarietyMaster>(cropVariety);
            lblCropType.Text = "";
        }

        protected void Variety_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cropVariety = cropVarietyMasterService.GetById(Variety.SelectedValue.ToInt());
            lblCropType.Text = rbcroptype.SelectedValue.Trim() == "H" ? "Parentage:" + cropVariety.NameSV : "";
        }
        private void ClearControl()
        {


            lblTotal.Text = "0";
            AreainAcre.Text = "0";
            LocationSeedStock.Text = "";
            lblCropType.Text = "";



        }
    }
}