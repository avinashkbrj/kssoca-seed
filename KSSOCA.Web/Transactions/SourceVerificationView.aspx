﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SourceVerificationView.aspx.cs" Inherits="KSSOCA.Web.SourceVerificationView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" style="min-height: 600px;">

                <div   class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                     
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnType" runat="server" />
                </div>

                <div align="left">
                   
                        <asp:Button runat="server" ID="Add" class="btn-primary" OnClick="Add_Click" Text="Request for New Source Verification " />
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                   
                    <tr>
                        <td>  
                            <asp:GridView runat="server" ID="SourceVerificationGridView" 
                                AutoGenerateColumns="false" class="table-condensed" Width="100%"
                                OnRowDataBound="SourceVerificationGridView_RowDataBound" OnRowCommand="SourceVerificationGridView_RowCommand"
                                AllowPaging="true" PageSize="25" AllowSorting="true" OnPageIndexChanging="SourceVerificationGridView_PageIndexChanging"  >
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="Id" HeaderText="SV No." />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="NAME" HeaderText="Firm Name" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="CROPNAME" HeaderText="Crop" />
                                </Columns>
                                 <Columns>
                                    <asp:BoundField DataField="Variety" HeaderText="Variety" />
                                </Columns>
                                 <Columns>
                                    <asp:BoundField DataField="Class" HeaderText="Class" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="QuantityofSeed" HeaderText="Seed Quantity(in Kg.)" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="districtName" HeaderText="Stock Located District " />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="talukName" HeaderText="Stock Located Taluk" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Date of Request" />
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                            <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:HyperLinkField Text="View" DataNavigateUrlFields="Id"
                                        DataNavigateUrlFormatString="~/Transactions/SourceVerificationApproval.aspx?Id={0}"></asp:HyperLinkField>
                                </Columns>
                                <Columns >
                                    <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CommandName="Delete" ID="lnkDelete"
                                                OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lnkForm1" PostBackUrl='<%# "~/Transactions/FormOneView.aspx?Id=" + Eval("Id") %>'>Form 1</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
