﻿
namespace KSSOCA.Web
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using System.Web.UI.WebControls;
    using KSSOCA.Core.Security;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Model;
    public partial class SourceVerificationView : SecurePage
    {
        SourceVerificationService sourceVerificationService;
        UserMasterService userMasterService;
        Common common = new Common();
        RegistrationFormService registrationFormService;
        SourceVerificationTransactionService svtService;

        public SourceVerificationView()
        {
            sourceVerificationService = new SourceVerificationService();
            userMasterService = new UserMasterService();
            registrationFormService = new RegistrationFormService();
            svtService = new SourceVerificationTransactionService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["Type"] != null)
                {
                     
                    hdnType.Value = Request.QueryString["type"].ToString().Trim();
                }
                else { hdnType.Value = "C"; }
                lblHeading.Text = common.getHeading("SVV"); //litNote.Text = common.getNote("SVV");
                InitComponent();
            }
            catch (Exception ex)
            {
                
                lblMessage.Text = "Something went wrong, Please try again later.";
                //Save.Enabled = false;
                Core.Exceptions.ApplicationExceptionHandler.Handle(ex);
            }
        }

        private void InitComponent()
        {
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
            if (roleId == 5)
            {
                Add.Visible = true;
            }
            else
            {
                Add.Visible = false;
            }
            if (registrationFormService.CheckForRegistration(userMasterService.GetFromAuthCookie()) == 0)
            {
                if (roleId == 5)
                {
                    Add.Visible = false;
                    lblMessage.Text = "Firm not registered yet.";
                }
            } 
            BindGrid();
        }

        public void BindGrid()
        {
            var user = userMasterService.GetFromAuthCookie(); 
            var data= sourceVerificationService.GetSourceVerifications(Convert.ToInt16(user.Role_Id), user.Id, hdnType.Value);
            SourceVerificationGridView.DataSource = data;
            SourceVerificationGridView.DataBind();
        }
       

        public bool DeleteMethod(int id)
        {
            int deletedRowCount = sourceVerificationService.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }
        protected void Add_Click(object sender, EventArgs e)
        {
            this.Redirect("Transactions/SourceVerificationCreate.aspx");
        }
        protected void SourceVerificationGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkDelete = (e.Row.FindControl("lnkDelete") as LinkButton);
                LinkButton lnkForm1 = (e.Row.FindControl("lnkForm1") as LinkButton);
                HiddenField hdnStatusCode = (e.Row.FindControl("hdnStatusCode") as HiddenField);
                HiddenField hdnId = (e.Row.FindControl("hdnID") as HiddenField);
                Label lblStatus = (e.Row.FindControl("lblStatus") as Label);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
                if (roleId == 5)
                {
                    lnkDelete.Visible = true;
                   
                }
                else
                {
                    lnkDelete.Visible = false;
                    lnkForm1.Visible = false;
                }
                if (hdnStatusCode.Value == "1")
                {
                    lblStatus.Text = "Verified";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    lnkDelete.Visible = false;
                    //lnkDelete.Enabled = false;

                }
                else
                {
                    SourceVerificationTransaction svt = svtService.GetMaxTransactionDetails(hdnId.Value.ToInt());
                    if (svt != null)
                    {
                        if (svt.TransactionStatus != 0)
                        {
                            lblStatus.Text = common.getTransactionStatus(svt.TransactionStatus);
                            lblStatus.ForeColor = System.Drawing.Color.DarkOrange;
                        }
                        else
                        {
                            lblStatus.Text = "Request Rejected";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    else
                    {
                        lblStatus.Text = "Invalid request.";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }

                    lnkForm1.Visible = false;
                    // lnkForm1.Enabled = false;
                }

            }
        }

        protected void SourceVerificationGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void SourceVerificationGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            SourceVerificationGridView.PageIndex = e.NewPageIndex;
            SourceVerificationGridView.DataBind();
        }
    }
}