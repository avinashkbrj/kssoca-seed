﻿
namespace KSSOCA.Web.Transactions
{
    using System;
    using System.Data;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Security;
    using System.Web.UI.WebControls;
    using KSSOCA.Model;
    using System.Linq;
    using System.Web;
    using System.Text;
    using System.Configuration;
    using System.Security.Cryptography;
    using System.Web.UI;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.SqlClient;

    public partial class TestingPaymentView : SecurePage
    {
        static string conn = ConfigurationManager.ConnectionStrings["KSSOCAConnectionString"].ToString();
        SqlConnection objsqlconn = new SqlConnection(conn);
        DecodingService decodingService;
        OnlinePayment formOnePayment;
        UserMasterService userMasterService;
        FormOneDetailsService form1Service;
        SourceVerificationService sourceVerificationService;
        Common common = new Common();
        CommonPaymentMethods commonPaymentMethods;
        Form1PaymentDetailService form1PaymentDetailService;
        FarmerDetailsService farmerDetailsService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        Form1DetailsTransactionService form1DetailsTransactionService;
        SeedAnalysisReportService seedAnalysisReportService;
        GeneticPurityReportService geneticPurityReportService;

        public TestingPaymentView()
        {
            form1Service = new FormOneDetailsService();
            userMasterService = new UserMasterService();
            sourceVerificationService = new SourceVerificationService();
            form1PaymentDetailService = new Form1PaymentDetailService();
            farmerDetailsService = new FarmerDetailsService();
            cropMasterService = new CropMasterService();
            commonPaymentMethods = new CommonPaymentMethods();
            formOnePayment = new OnlinePayment();
            formOneService = new FormOneDetailsService();
            form1DetailsTransactionService = new Form1DetailsTransactionService();
            decodingService = new DecodingService();
            geneticPurityReportService = new GeneticPurityReportService();
            seedAnalysisReportService = new SeedAnalysisReportService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            { 
                lblHeading.Text = common.getHeading("STP");  // Short name of the page
                
                string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "http://" : "http://";
                //string surl = ((HttpContext.Current.Request.ServerVariables["HTTPS"] != "" && HttpContext.Current.Request.ServerVariables["HTTP_HOST"] != "off") || HttpContext.Current.Request.ServerVariables["SERVER_PORT"] == "443") ? "https://" : "http://";
                surl += HttpContext.Current.Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.ServerVariables["REQUEST_URI"];
                Session.Add("surl", surl);
                   
                if (!IsPostBack)
                {
                    NameValueCollection nvc = Request.Form;
                    if (nvc.Count > 0)
                    {
                        EPayment_Response(nvc, sender, e);
                    }
                } 
                      
                if (!IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Something went wrong, Please try again later.";
                //Save.Enabled = false;
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        private void InitComponent()
        {
            Random random = new Random();
            var userMaster = userMasterService.GetFromAuthCookie();
            int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
            int txnid = random.Next(1001, 9999);
            Session.Add("txnid", txnid);
            Session.Add("mobilenumber", userMaster.MobileNo);
            Session.Add("fname", userMaster.Name);
            if (roleId == 5)
            {

                TestingPaymentGridView.DataSource = SelectMethod();
                TestingPaymentGridView.DataBind();
            }
            else
            {

                TestingPaymentGridView.DataSource = SelectMethod();
                TestingPaymentGridView.DataBind();
            }
        }


        public bool DeleteMethod(int id)
        {
            int deletedRowCount = form1Service.Delete(id);

            return deletedRowCount > 0 ?
                true : false;
        }

        public DataTable SelectMethod()
        {
            //var data = (DataTable)null;
            
            var user = userMasterService.GetFromAuthCookie();
             
            String tranactionQuery = @"select * from TestingPayment t join Form1Details f on t.Form1Id= f.Id join UserMaster u on u.Id= f.Producer_Id where t.Enable=1 and t.GotRequiredOrNot=0 and u.Id=@uid";
            SqlCommand transactionCmd = new SqlCommand(tranactionQuery, objsqlconn);
            transactionCmd.Parameters.AddWithValue("@uid", user.Id);

            objsqlconn.Open();
            DataTable data = new DataTable();
            var dataReader = transactionCmd.ExecuteReader();

            data.Load(dataReader);
 
            if (data != null)
            {
                return data;
            }
            else
                lblMessage.Text = "Records not found.";
            return (DataTable)null;
        }

        protected void TestingPaymentGridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // LinkButton lnkDelete = (e.Row.FindControl("lnkDelete") as LinkButton);
              
                
                
                HiddenField hdnPaymentStatus = (e.Row.FindControl("hdnPaymentStatus") as HiddenField);
                CheckBox chkPayment = (e.Row.FindControl("chkPayment") as CheckBox);
                int roleId = Convert.ToInt32(userMasterService.GetFromAuthCookie().Role_Id);
                Label lblAmountPaid = (e.Row.FindControl("lblAmountPaid") as Label);

                if (roleId == 5)
                {
                    // lnkDelete.Visible = true;
                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;

                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                       // lblStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        chkPayment.Enabled = true;
                        chkPayment.Checked = false;

                        lblAmountPaid.Text = "Tick here to pay";
                       // lblStatus.ForeColor = System.Drawing.Color.Blue;
                    }

                }
                else
                {
                    //lnkDelete.Visible = false;
                    if (hdnPaymentStatus.Value == "Y")
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = true;
                        lblAmountPaid.Visible = true;
                      //  lblStatus.ForeColor = System.Drawing.Color.Green;
                        lblAmountPaid.Text = "Amount Paid:Rs." + lblAmountPaid.Text + "/-";
                    }
                    else
                    {
                        chkPayment.Enabled = false;
                        chkPayment.Checked = false;
                     //   lblStatus.ForeColor = System.Drawing.Color.Red;
                        lblAmountPaid.Text = "Not yet paid";
                    }
                }

                

            }
        }
        protected void chkPayment_CheckedChanged(object sender, EventArgs e)
        {
            List<OnlinePayment> formOnePayments = new List<OnlinePayment>();

            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3]
            { new DataColumn("Form1ID"),
                new DataColumn("RegFees"), 
                new DataColumn("Total",typeof(decimal) )
             });
            foreach (GridViewRow row in TestingPaymentGridView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    OnlinePayment formOnePayment = new OnlinePayment();
                    CheckBox chkRow = (row.Cells[4].FindControl("chkPayment") as CheckBox);
                    if (chkRow.Checked && chkRow.Enabled)
                    {
                        string Form1ID = row.Cells[1].Text;
                        string StlID = row.Cells[2].Text;
                        string GotId = row.Cells[3].Text;
                        string RegFees = ""; 
                        decimal Total = 0; 
                        Form1Details f1 = form1Service.ReadById(Form1ID.ToInt());
                        FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
                        SeedAnalysisReport sar = seedAnalysisReportService.GetByLabTestNo(Convert.ToInt32(StlID));
                        GeneticPurityReport gpr = geneticPurityReportService.GetByLabTestNo(Convert.ToInt32(GotId));
                        SourceVerification sv = sourceVerificationService.ReadById(f1.SourceVarification_Id);
                        CropMaster cm = cropMasterService.GetById(sv.Crop_Id);
                        int totalTag = sar.NoOfLabels + gpr.NoOfLabels;
                        int totalAmountForTag = totalTag * 4;
                        if (farDet.StatusCode == 1) { RegFees = totalAmountForTag.ToString();}
                        else { RegFees = totalAmountForTag.ToString(); }
                        decimal InspectionAmout = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
                       
                        Total = Convert.ToDecimal(
                                     Convert.ToDecimal(RegFees));

                        dt.Rows.Add(Form1ID, RegFees, decimal.Round(Total, 2, MidpointRounding.AwayFromZero));
                        formOnePayment.RegNumber = Form1ID.ToInt();
                        formOnePayment.RegFees = Convert.ToDecimal(RegFees); 
                        formOnePayment.TotalAmountPaid = Convert.ToDecimal(Total);
                        formOnePayment.StlID = Convert.ToInt32(StlID);
                        formOnePayment.GotId = Convert.ToInt32(GotId);
                        formOnePayments.Add(formOnePayment);
                    }
                }
            }
            Session.Add("paymentDetails", formOnePayments);
            lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            Session.Add("amount", lblTotal.Text);
            if (Convert.ToDecimal(lblTotal.Text) > 0)
            {
                pnlPayment.Visible = true;
            }
            else { pnlPayment.Visible = false; }
            gvPayment.DataSource = dt;
            gvPayment.DataBind();
            checkPaymentMode();
        }
        protected void rbPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkPaymentMode();
        }
        private void checkPaymentMode()
        {
            if (rbPaymentMode.SelectedValue == "C")
            {
                pnlCheck.Visible = true;
                pnlOnline.Visible = false;
            }
            else if (rbPaymentMode.SelectedValue == "O")
            {
                pnlCheck.Visible = false;
                pnlOnline.Visible = true;
              
            }
        }
        protected void btnCheck_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                // byte[] a = fu_Check.FileBytes;
                SavePayment(rbPaymentMode.SelectedValue);
            }
        }
        private void BindPaymentGrid(string Form1IDs)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3]
            { new DataColumn("Form1ID"),
                new DataColumn("RegFees"), 
                new DataColumn("Total",typeof(decimal) )
             });

            string[] FormIDs = Form1IDs.Split('_');
            foreach (string ID in FormIDs)
            {
                string Form1ID = ID;
                string RegFees = ""; 
                decimal Total = 0;

                Form1Details f1 = form1Service.ReadById(Form1ID.ToInt());

                FarmerDetails farDet = farmerDetailsService.GetById(f1.FarmerID);
                SourceVerification sv = sourceVerificationService.ReadById(f1.SourceVarification_Id);
                CropMaster cm = cropMasterService.GetById(sv.Crop_Id);
                if (farDet.StatusCode == 1) { RegFees = "0"; }
                else { RegFees = common.getFees("GOT").ToString(); }
                decimal InspectionAmout = Convert.ToDecimal(cm.Inspection * f1.Areaoffered);
               
                Total = Convert.ToDecimal(
                             Convert.ToDecimal(RegFees));

                dt.Rows.Add(Form1ID, RegFees, decimal.Round(Total, 2, MidpointRounding.AwayFromZero));

            }
            // lblTotal.Text = dt.AsEnumerable().Sum(row => row.Field<decimal>("Total")).ToString();
            if (Convert.ToDecimal(lblTotal.Text) > 0)
            {
                pnlPayment.Visible = true;
            }
            else { pnlPayment.Visible = false; }
            gvPayment.DataSource = dt;
            gvPayment.DataBind();
            SavePayment("O");
        }
        private void SavePayment(string PaymentMode)
        {
            int result = 0, resultSar=0,resultGpr=0;
            SqlDataReader dataReader;
            foreach (GridViewRow row in gvPayment.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    int Form1ID = Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text);
                    decimal RegFees = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[2].Text);
                    decimal Total = Convert.ToDecimal(gvPayment.Rows[row.RowIndex].Cells[3].Text);
                    int paymentStatus = 1;
                    int PaymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(Form1ID)).Id;

                    String testingPayment = @"Select * from TestingPayment where Form1Id = '" + Form1ID + "'";
                    SqlCommand testingPaymentCmd = new SqlCommand(testingPayment, objsqlconn);
                    objsqlconn.Open();
                    dataReader = testingPaymentCmd.ExecuteReader();
                    dataReader.Read();
                    int stlId = Convert.ToInt32(dataReader["STLTestID"]);
                    int gotId = Convert.ToInt32(dataReader["GOTTestID"]);

                    objsqlconn.Close();

                    //String stlTestDetails = @"Select * from Decoding where SSC_Id = '" + stlId + "'";
                    //SqlCommand stlTestDetailsCmd = new SqlCommand(stlTestDetails, objsqlconn);
                    //objsqlconn.Open();
                    //dataReaderStl = stlTestDetailsCmd.ExecuteReader();
                    //dataReaderStl.Read();
                    //int STLID = Convert.ToInt32(dataReaderStl["Id"]);
                    //objsqlconn.Close();

                    //String stlTestDetails1 = @"Select * from Decoding where SSC_Id = '" + gotId + "'";
                    //SqlCommand stlTestDetailsCmd1 = new SqlCommand(stlTestDetails1, objsqlconn);
                    //objsqlconn.Open();
                    //dataReaderGot = stlTestDetailsCmd1.ExecuteReader();
                    //dataReaderGot.Read();
                    //int STLID1 = Convert.ToInt32(dataReaderGot["Id"]);
                    //objsqlconn.Close();


                    //string queryTestPayment = "Insert into TestingPayment(Form1Id,STLTestID,GOTTestID,PaymentType,PaymentMode,AmountPaid,PaySlip,PaymentStatus,PaymentDate) Values(@Form1Id,@STLTestID,@GOTTestID,@PaymentType,@PaymentMode,@AmountPaid,@PaySlip,@PaymentStatus,@PaymentDate)";
                    SqlCommand cmdTestPayment = UpdatePayment(PaymentMode, Form1ID, Total);
                    objsqlconn.Open();
                    result = cmdTestPayment.ExecuteNonQuery();
                    objsqlconn.Close();
                    SqlCommand cmdSeedAnalysisReport = UpdateSeedAnalysisReport(paymentStatus, stlId);
                    if (result >0)
                    {
                        objsqlconn.Open();
                        resultSar = cmdSeedAnalysisReport.ExecuteNonQuery();
                        objsqlconn.Close();
                    }
                    if (gotId != 0)
                    {
                        SqlCommand cmdGeneticPurityReport = UpdateGeneticReport(paymentStatus, gotId);
                        objsqlconn.Open();
                        resultGpr = cmdGeneticPurityReport.ExecuteNonQuery();
                        objsqlconn.Close();
                    }
                }
            }
            if (result > 0 && resultSar > 0)
            {
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "Payment Details Saved successfully.";
                gvPayment.DataSource = null;
                gvPayment.DataBind();
                gvPayment.Visible = false;
                lblTotal.Text = "0.00";
                pnlPayment.Visible = false;
                TestingPaymentGridView.DataSource = SelectMethod();
                TestingPaymentGridView.DataBind();
            }
        }

        private SqlCommand UpdateGeneticReport(int paymentStatus, int gotId)
        {
            string updateGeneticPurityReport = "Update GeneticPurityReport set PaymentStatus='" + paymentStatus + "'   where LabTestNo= " + gotId;
            SqlCommand cmdGeneticPurityReport = new SqlCommand(updateGeneticPurityReport, objsqlconn);
            return cmdGeneticPurityReport;
        }

        private SqlCommand UpdateSeedAnalysisReport(int paymentStatus, int stlId)
        {
            string updateSeedAnalysisReport = "Update SeedAnalysisReport set PaymentStatus='" + paymentStatus + "'   where LabTestNo= " + stlId;
            SqlCommand cmdSeedAnalysisReport = new SqlCommand(updateSeedAnalysisReport, objsqlconn);
            return cmdSeedAnalysisReport;
        }

        private SqlCommand UpdatePayment(string PaymentMode, int Form1ID, decimal Total)
        {
            string updateTestPayment = "Update TestingPayment set PaymentType=@PaymentType,PaymentMode=@PaymentMode,AmountPaid=@AmountPaid,PaySlip=@PaySlip,PaymentStatus=@PaymentStatus,PaymentDate=@PaymentDate   where Form1Id= @Form1Id";
            SqlCommand cmdTestPayment = new SqlCommand(updateTestPayment, objsqlconn);
            cmdTestPayment.Parameters.AddWithValue("@PaymentType", PaymentMode);
            cmdTestPayment.Parameters.AddWithValue("@PaymentMode", PaymentMode);
            cmdTestPayment.Parameters.AddWithValue("@AmountPaid", Total);
            cmdTestPayment.Parameters.AddWithValue("@PaySlip", fu_Check.FileBytes);
            cmdTestPayment.Parameters.AddWithValue("@PaymentStatus", "Y");
            cmdTestPayment.Parameters.AddWithValue("@PaymentDate", System.DateTime.Now);
             
            cmdTestPayment.Parameters.AddWithValue("@Form1ID", Form1ID);
            return cmdTestPayment;
        }

        protected void EPayment_Response(System.Collections.Specialized.NameValueCollection nvc, object sender, EventArgs e)
        {
            int idValue = Convert.ToInt32(Request.QueryString["Id"]);
            List<OnlinePayment> testingPaymentsRetrieve = new List<OnlinePayment>();
            string bank_txn = Request.Form["mihpayid"];
            string amount = Request.Form["amount"];
            string f_code = Request.Form["status"];
            int resultgot, resultstl;
            SqlCommand cmdGPR;
            if (Session["paymentDetails"] != null)
            {
                testingPaymentsRetrieve = (List<OnlinePayment>)Session["paymentDetails"];
            }
            if (f_code != null)
            {
                if (f_code == "success")
                {
                    foreach (var payments in testingPaymentsRetrieve)
                    {

                        int Form1IDs = payments.RegNumber;
                        Form1Details f1 = formOneService.ReadById(Form1IDs);
                        lblMessage.Text = "Payment Success";
                        int paymentId = form1PaymentDetailService.GetBy(Convert.ToInt16(f1.Id)).Id;
                        SqlCommand cmdTestPayment = UpdatePayment("O", Form1IDs, payments.TotalAmountPaid);
                        SqlCommand cmdSAR = UpdateSeedAnalysisReport(1, payments.StlID);
                        if (payments.GotId > 0)
                        {
                            cmdGPR = UpdateGeneticReport(1, payments.GotId);
                            objsqlconn.Open();
                            resultgot = cmdGPR.ExecuteNonQuery();
                            objsqlconn.Close();

                        }
                        objsqlconn.Open(); 
                        int resultPay = cmdTestPayment.ExecuteNonQuery(); 
                        objsqlconn.Close();
                        if(resultPay==1)
                        {
                            objsqlconn.Open();
                            resultstl = cmdSAR.ExecuteNonQuery(); 
                            objsqlconn.Close();
                            if (resultstl == 1)
                            {
                                Session.Remove("paymentDetails");
                                lblMessage.ForeColor = System.Drawing.Color.Green;
                                lblMessage.Text = "Payment Details Saved successfully.";
                                this.Redirect("/Transactions/TestingPaymentView.aspx");
                            }
                        }
                    }
                    
                }
                else
                {
                    lblMessage.Text = "Payment Failed";
                }
            }
        }
        
        private string getForm1IDs()
        {
            string Form1IDs = "";
            foreach (GridViewRow row in gvPayment.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Form1IDs += Convert.ToInt32(gvPayment.Rows[row.RowIndex].Cells[1].Text) + "_";
                }
            }
            return Form1IDs.Remove(Form1IDs.Length - 1); ;
        }



        protected void TestingPaymentGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
            TestingPaymentGridView.PageIndex = e.NewPageIndex;
            TestingPaymentGridView.DataSource = SelectMethod();
            TestingPaymentGridView.DataBind();
        }
    }
}