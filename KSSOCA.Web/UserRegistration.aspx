﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="UserRegistration.aspx.cs" Inherits="KSSOCA.Web.UserRegistration"
    EnableEventValidation="true" ValidateRequest="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/registration.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <br />
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />

                <table class="table-condensed " border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>1.Registering as (ನೋಂದಣಿ ಪ್ರಕಾರ)</td>
                        <td>
                            <asp:RadioButtonList runat="server" ID="rbType" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Seed Producing Firm &nbsp;&nbsp;" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Seed Processing Unit" Value="10"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredrbType" Style="color: red" Display="Dynamic"
                                ControlToValidate="rbType" ErrorMessage="Registration type is required."></asp:RequiredFieldValidator>
                    </tr>
                    <tr>
                        <td>2.Name of the Institution (ಸಂಸ್ಥೆಯ ಹೆಸರು) <span style="color: red">*</span></td>
                        <td>
                            M/s.<asp:TextBox runat="server" CssClass="form-control" ID="ProducerName" placeholder="Name of the Institution" CausesValidation="true" />
                            <asp:RequiredFieldValidator runat="server" ID="ProducerNameRequired" Style="color: red" Display="Dynamic"
                                ControlToValidate="ProducerName" ErrorMessage="Name of the Institution Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="ProducerNameMinMax" Style="color: red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9./ ]{5,75}$" ControlToValidate="ProducerName"
                                ErrorMessage="Name of the institution required min of 5 characters"></asp:RegularExpressionValidator>
                        </td>
                    </tr>

                    <tr>
                        <td>3.Mobile Number(ಮೊಬೈಲ್ ಸಂಖ್ಯೆ)  <span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="MobileNumber"   placeholder="Mobile Number" onkeypress="return CheckNumber(event);" AutoPostBack="true" OnTextChanged="MobileNumber_TextChanged"  MaxLength="10" />
                            <asp:RequiredFieldValidator runat="server" ID="MobileNumberRequired" Style="color: red" Display="Dynamic"
                                ControlToValidate="MobileNumber" ErrorMessage="Mobile Number Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidatorMobileNumber" Style="color: red" Display="Dynamic"
                                ValidationExpression="^[0-9]{10}$" ControlToValidate="MobileNumber"
                                ErrorMessage="Mobile number must be 10 digits"></asp:RegularExpressionValidator>
                            </td>
                    </tr>

                    <tr>
                        <td>4.Email Id (ಇಮೇಲ್ ವಿಳಾಸ)<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="EmailId" placeholder="Email Id" />
                            <asp:RequiredFieldValidator runat="server" ID="EmailRequired" Style="color: red" Display="Dynamic"
                                ControlToValidate="EmailId" ErrorMessage="Email Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="EmailRegEx" Style="color: red" Display="Dynamic"
                                ControlToValidate="EmailId" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                                ErrorMessage="Please enter valid email id"></asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td>5.District(ಜಿಲ್ಲೆ)<span style="color: red">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredDistrict" Style="color: red" Display="Dynamic"
                                ControlToValidate="District" ErrorMessage="District Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>6.Taluk(ತಾಲ್ಲೂಕು)<span class="text-danger">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="Taluk">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredTaluk" Style="color: red" Display="Dynamic"
                                ControlToValidate="Taluk" ErrorMessage="Taluk Required" InitialValue="0"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td>7.User Name <span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" class="form-control" ID="txtusename" placeholder="User Name" MaxLength="25"   OnTextChanged="txtusename_TextChanged" AutoPostBack="true" />
                            <asp:RequiredFieldValidator runat="server" ID="Requiredusename" Style="color: red" Display="Dynamic"
                                ControlToValidate="txtusename" ErrorMessage="User Name  Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="Regularusename" Style="color: red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9_]{5,30}$" ControlToValidate="txtusename"
                                ErrorMessage="Please enter  minimum 5 maximum 25 character of A-Z a-z 0-9"></asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td>8.Password(ಪಾಸ್ವರ್ಡ್)<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" TextMode="Password" class="form-control" ID="Password" placeholder="Password" />
                            <asp:RequiredFieldValidator runat="server" ID="PasswordRequired" Style="color: red" Display="Dynamic"
                                ControlToValidate="Password" ErrorMessage="Password Required"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="PasswordRegEx" Style="color: red" Display="Dynamic"
                                ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{8,75}$" ControlToValidate="Password"
                                ErrorMessage="Password required min of 8 and max of 20 characters."></asp:RegularExpressionValidator></td>
                    </tr>
                    <tr>
                        <td>9.Retype password(ಪಾಸ್ವರ್ಡ್ ಮತ್ತೆ ಟೈಪ್ ಮಾಡಿ)<span style="color: red" >*</span></td>
                        <td>
                            <asp:TextBox runat="server" TextMode="Password" class="form-control" ID="ConfirmPassword"  placeholder="Retype password"/>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredConfirmPassword" Style="color: red" Display="Dynamic"
                                ControlToValidate="ConfirmPassword" ErrorMessage="Confirm Password Required"></asp:RequiredFieldValidator>
                            <asp:CompareValidator runat="server" ID="PasswordMatch" Style="color: red" Display="Dynamic"
                                ControlToCompare="Password" ControlToValidate="ConfirmPassword" ErrorMessage="Password didn't match"></asp:CompareValidator></td>
                    </tr>
                    <tr>

                        <td colspan="2" align="center">
                            <asp:Button runat="server" CssClass="btn-primary"
                                ID="Signup" Text="Register" OnClick="Signup_Click" /></td>
                    </tr>
                </table>

            </div>
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="Signup" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="lib/bootstrap/js/bootstrap.validator.js"></script>
</asp:Content>
