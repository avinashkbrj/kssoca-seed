﻿namespace KSSOCA.Web
{
    using System;
    using System.Web.UI;
    using KSSOCA.Model;
    using KSSOCA.Core.Data;
    using KSSOCA.Core;
    using KSSOCA.Core.Exceptions;
    using KSSOCA.Core.Extensions;
    using KSSOCA.Core.Helper;
    using System.Web.UI.HtmlControls;

    public partial class UserRegistration : System.Web.UI.Page
    {

        TalukMasterService talukService;
        DistrictMasterService districtService;
        UserMasterService userMasterService;
        Common common = new Common();
        UserwiseDistrictRightsService userwiseDistrictRightsService;

        public UserRegistration()
        {
            talukService = new TalukMasterService();
            districtService = new DistrictMasterService();
            userMasterService = new UserMasterService();
            userwiseDistrictRightsService = new UserwiseDistrictRightsService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblHeading.Text = common.getHeading("UR");
                litNote.Text = common.getNote("UR");
                lblMessage.Text = "";
                if (!Page.IsPostBack)
                {
                    InitComponent();
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }
        protected void Signup_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    int reportingId;
                    bool isEmailIdExist = userMasterService.IsEmailIdExist(EmailId.Text);
                    if (!isEmailIdExist)
                    {
                        if (rbType.SelectedValue == "10")
                        {
                            // string RegistrationNumber = userMasterService.GenerateRegistrationNumber();
                            reportingId = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy
                                    (Convert.ToInt16(District.Text.ToNullableInt()),
                                     Convert.ToInt16(Taluk.Text.ToNullableInt()), 6).User_Id);
                        }
                        else
                        {
                            reportingId = 0;
                        }
                        var userMaster = new UserMaster()
                        {
                            Id = common.GenerateID("UserMaster"),
                            Name = "M/s. " + ProducerName.Text.Trim(),
                            RegistrationDate = System.DateTime.Now,
                            District_Id = District.Text.ToNullableInt(),
                            Taluk_Id = Taluk.Text.ToNullableInt(),
                            MobileNo = MobileNumber.Text.Trim(),
                            Role_Id = rbType.SelectedValue.ToInt(),
                            EmailId = EmailId.Text.Trim().ToLower(),
                            Password = Password.Text.Trim(),
                            IsActive = true,
                            UserName = txtusename.Text.Replace(" ", String.Empty).ToLower(),
                            Division_Id = Convert.ToInt16(userwiseDistrictRightsService.GetAllBy(Convert.ToInt16(District.SelectedValue.Trim()), Convert.ToInt16(Taluk.SelectedValue.Trim()), 3).User_Id),
                            Reporting_Id = reportingId,
                        };

                        int result = userMasterService.Register(userMaster);
                        if (result == 1)
                        {
                            Messaging.SendSMS("User Registered to KSSOCA successfully.Your User Name is " + txtusename.Text.Replace(" ", String.Empty).ToLower() + " and Password is " + Password.Text + ". Please login using these User id and and Password and do necessary actions for the seed certification process.", MobileNumber.Text, "1");
                            Page_Load(sender, e);
                            lblMessage.ForeColor = System.Drawing.Color.Green;
                            lblMessage.Text = "Registration Success.";
                        }
                        else
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            lblMessage.Text = "Something went wrong, Please try again later.";
                        }
                    }
                    else
                    {
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        lblMessage.Text = "Email id already exist. Please enter another email.";
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
            }
        }

        private string GenerateRandomCaptcha(out int sum)
        {
            var random = new Random();
            int num1 = random.Next(1, 10);
            int num2 = random.Next(1, 10);

            sum = num1 + num2;
            return string.Format("{0} + {1}", num1, num2);
        }

        private void InitComponent()
        {
            var districts = districtService.GetAll();
            District.BindListItem<DistrictMaster>(districts);
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
        }

        protected void District_SelectedIndexChanged(object sender, EventArgs e)
        {
            Taluk.Items.Clear();
            var taluks = talukService.GetAllbyDistID(Convert.ToInt16(District.SelectedValue));
            Taluk.Bind_T_H_V_ListItem<TalukMaster>(taluks);
        }
        protected void MobileNumber_TextChanged(object sender, EventArgs e)
        {
            if (Utility.IsValidMobileNo(MobileNumber.Text))
            {
                bool IsMobileNumberExist = userMasterService.IsMobileNumberExist(MobileNumber.Text);
                if (IsMobileNumberExist)
                {
                    MobileNumber.Text = "";
                    lblMessage.Text = "This mobile number already exist please try another.";
                }
            }
            else
            {
                MobileNumber.Text = "";
                lblMessage.Text = "Enter valid mobile number.";
            }
        }
        protected void txtusename_TextChanged(object sender, EventArgs e)
        {
            bool IsUserNameExist = userMasterService.IsUserNameExist(txtusename.Text);
            if (IsUserNameExist)
            {
                txtusename.Text = "";
                lblMessage.Text = "This user name already exist please try another.";
            }
        }
    }
}