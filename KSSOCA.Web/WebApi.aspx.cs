﻿
namespace KSSOCA.Web
{
    using System;
    using KSSOCA.Core.Data;
    using KSSOCA.Core.Helper;
    using System.Web.Services;

    public partial class WebApi : System.Web.UI.Page
    {
        [WebMethod(BufferResponse = true, CacheDuration = 86400)]
        public static string GetAllCrops()
        {
            string a = Serializer.JsonSerialize(new CropMasterService().GetAllCrop());
            return a;
        }
        [WebMethod(BufferResponse = true, CacheDuration = 86400)]
        public static string GetAllParticulars()
        {
            return Serializer.JsonSerialize(new CropMasterService().GetAll());
        }
    }
}