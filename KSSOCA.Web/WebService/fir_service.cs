﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using KSSOCA.Core;
using KSSOCA.Data;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using KSSOCA.Core.Helper;
using Newtonsoft.Json;


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class fir_service : System.Web.Services.WebService
{
    UserMasterService userMasterService;
    BankMasterService bankMasterService;
    CropMasterService cropMasterService;
    FormOneDetailsService formOneService;
    TalukMasterService talukMasterService;
    SeasonMasterService seasonMasterService;
    DistrictMasterService districtMasterService;
    TalukHobliMasterService HobliMasterService;
    VillageMasterService villageMasterService;
    ClassSeedMasterService classSeedMasterService;
    CropVarietyMasterService cropVarietyMasterService;
    SourceVerificationService sourceverficationservice;
    RegistrationFormService registrationFormService;
    Common common = new Common();
    FieldInspectionService fieldInspectionService;
    FieldInspectionSuggestionMasterService fieldInspectionSuggestionMasterService;
    FarmerDetailsService farmerDetailsService;
    F1LotNumbersService f1LotNumbersService;


    public fir_service()
    {
        userMasterService = new UserMasterService();
        cropMasterService = new CropMasterService();
        bankMasterService = new BankMasterService();
        formOneService = new FormOneDetailsService();
        talukMasterService = new TalukMasterService();
        seasonMasterService = new SeasonMasterService();
        districtMasterService = new DistrictMasterService();
        classSeedMasterService = new ClassSeedMasterService();
        cropVarietyMasterService = new CropVarietyMasterService();
        villageMasterService = new VillageMasterService();
        HobliMasterService = new TalukHobliMasterService();
        sourceverficationservice = new SourceVerificationService();
        registrationFormService = new RegistrationFormService();
        fieldInspectionService = new FieldInspectionService();
        fieldInspectionSuggestionMasterService = new FieldInspectionSuggestionMasterService();
        farmerDetailsService = new FarmerDetailsService();
        f1LotNumbersService = new F1LotNumbersService();
    }

    public bool ValidateSecurityCode(string SecurityCode)
    {
        if (SecurityCode == "Kssoca_#264@")
            return true;
        else return false;
    }

    [WebMethod]
    public UserMaster GetLoginDetails(string UserName, string password, string SecurityCode)
    {
        if (ValidateSecurityCode(SecurityCode))
        {
            if (userMasterService.ValidateLogin(UserName, password, false))
            {
                UserMaster User = userMasterService.GetbyUserName(UserName);
                if (User.Role_Id == 4 || User.Role_Id == 6)
                {
                    var returningUser = new UserMaster()
                    {
                        Name = User.Name,
                        MobileNo = User.MobileNo,
                        EmailId = User.EmailId,
                        Id = User.Id
                    };
                    return returningUser;
                }
                else return null;
            }
            else return null;
        }
        else return null;
    }

    [WebMethod]
    public List<FieldInspectionSuggestionMaster> GetSuggestionMaster(string SecurityCode)
    {
        if (ValidateSecurityCode(SecurityCode))
        {
            return fieldInspectionSuggestionMasterService.GetAll();
        }
        else
            return null;
    }

    [WebMethod]
    public Form1Data GetForm1Details(int Form1ID, string SecurityCode)
    {
        try
        {
            if (ValidateSecurityCode(SecurityCode))
            {
                Form1Details f1 = formOneService.ReadById(Form1ID);
                if (f1 != null)
                {
                    FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                    SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                    RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                    UserMaster um = userMasterService.GetById(f1.Producer_Id);
                    string FarmerAddress = " District :" + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name
                     + ", Taluk:" + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name
                     + ", Hobli:" + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name
                     + ", Village:" + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name
                     + ", Mobile Number:" + fardet.MobileNo;
                    string LocationOfSeedPlot = " District :" + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name
                     + ", Taluk:" + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id)).Name
                     + ", Hobli:" + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id)).Name
                     + ", Village:" + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id), Convert.ToInt16(f1.PlotVillage_Id)).Name
                     + ", Gram Panchayat:" + f1.PlotGramPanchayath;
                    int InspectionNo = GetLastInspectionDetails(f1.Id);
                    string LotNos = f1LotNumbersService.LotNumbers(f1.Id);
                    if (InspectionNo > 0 && f1.StatusCode == 1)
                    {
                        var Suggestion = fieldInspectionSuggestionMasterService.GetAll();
                        Form1Data fi = new Form1Data();
                        fi.Form1ID = f1.Id;
                        fi.InspectionSerialNo = GetLastInspectionDetails(f1.Id).ToString();
                        fi.FarmerAddress = FarmerAddress;
                        fi.FarmerName = fardet.Name;
                        fi.FatherName = fardet.FatherName;
                        fi.SurveyNo = f1.SurveyNo.ToString();
                        fi.ClassOfSeed = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                        fi.AreaRegistered = f1.Areaoffered.ToString();
                        fi.DateOfSowingAsForm1 = f1.ActualDateofSowing != null ? f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy") : "";
                        fi.Season = seasonMasterService.GetById(Convert.ToInt16(f1.Season_Id)).Name;
                        fi.Crop = cropMasterService.GetById(sv.Crop_Id).Name;
                        fi.Variety = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                        fi.LotNo = LotNos;
                        fi.ProducerName = um.Name;
                        fi.ProducerRegNo = rf.RegistrationNo;
                        fi.LocationOfSeedPlot = LocationOfSeedPlot;
                        return fi;
                    }
                    return null;
                }
                else { return null; }
            }
            else { return null; }
        }
        catch (Exception ex)
        {
            ApplicationExceptionHandler.Handle(ex);
            return null;
        }
    }

    public int GetLastInspectionDetails(int Form1Id)
    {
        try
        {
            int InspectionSerialNo = 0;
            string query1 = "SELECT *  FROM  FieldInspection where Form1Id=" + Form1Id + "";
            DataTable dt = common.GetData(query1);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToInt16(dt.Rows[i]["IsFinal"]) > 0)
                    {
                        return 0;
                    }

                    // lblLastInspectionDetails.Text += dt.Rows[i]["InspectionSerialNo"] + ":" + Convert.ToDateTime(dt.Rows[i]["InspectedDate"]).ToString("dd/MM/yyyy") + " ";
                }
                string query = "SELECT ISNULL(Max(InspectionSerialNo), 0)   FROM  FieldInspection where Form1Id=" + Form1Id + "";
                InspectionSerialNo = Convert.ToInt16(common.ExecuteScalar(query));
            }
            return InspectionSerialNo + 1;
        }
        catch (Exception ex)
        {
            ApplicationExceptionHandler.Handle(ex);
            return 0;
        }
    }

    [WebMethod]
    public int SaveFieldInspection(int Form1ID, int IsFinal,
        DateTime ActualDateOfSowing,
        string DocumentsInvestigated,
        string CropStage, string LastCrop,
        string LastCropVariety, string FemaleMaleLinesRatio,
        int MaleLines, string DistanceBetweenLines, decimal InspectedArea,
        decimal RejectedArea, decimal AcceptedArea, string AreaRejectedDueTo, int PlantCount,
        string StateOfCrop, string Quality,
        decimal EstimatedYield, DateTime HarvestingDate, string Parentage,
        string Suggestions, string Remarks, byte[] FieldImage, string lattitude, string Longitude,
        int UserID, DateTime InspectedDate, string FIRCountDetails, string SecurityCode)
    {
        try
        {
            int InspectionSerialNo = GetLastInspectionDetails(Form1ID);
            if (InspectionSerialNo > 0)
            {
                var FI = new FieldInspection()
                {
                    InspectionSerialNo = InspectionSerialNo,
                    IsFinal = IsFinal,
                    Form1Id = Form1ID,
                    ActualDateOfSowing = ActualDateOfSowing,
                    DocumentsInvestigated = DocumentsInvestigated,
                    CropStage = CropStage,
                    LastCrop = LastCrop,
                    LastCropVariety = LastCropVariety,
                    FemaleMaleLinesRatio = FemaleMaleLinesRatio,
                    MaleLines = MaleLines,
                    DistanceBetweenLines = DistanceBetweenLines,
                    InspectedArea = InspectedArea,
                    RejectedArea = RejectedArea,
                    AcceptedArea = AcceptedArea,
                    PlantCount = PlantCount,
                    StateOfCrop = StateOfCrop,
                    Quality = Quality,
                    EstimatedYield = EstimatedYield.ToString(),
                    HarvestingDate = HarvestingDate,
                    Suggestions = Suggestions,
                    Remarks = Remarks,
                    FieldImage = FieldImage,
                    InspectedBy = UserID,
                    InspectedDate = System.DateTime.Now,
                    Parentage = Parentage,
                    lattitude = lattitude,
                    Longitude = Longitude,
                    AreaRejectedDueTo = AreaRejectedDueTo,
                    IPAddress = common.GetIPAddress(),
                };
                int resultCount = fieldInspectionService.Create(FI);
                if (resultCount > 0)
                {
                    string query = "select Max(Id) from FieldInspection where Form1Id=" + Form1ID + "";
                    int ID = Convert.ToInt16(common.ExecuteScalar(query));
                    InsertCropDetailsData(ID, FIRCountDetails);
                }
                else
                {
                    return 0;
                }
                return resultCount;
            }
            return 0;
        }
        catch (Exception ex)
        {
            string query = "select Max(Id) from FieldInspection where Form1Id=" + Form1ID + "";
            int ID = Convert.ToInt16(common.ExecuteScalar(query));
            fieldInspectionService.Delete(ID);
            ApplicationExceptionHandler.Handle(ex);
            return 0;
        }
    }

    public void InsertCropDetailsData(int FieldInspectionId, string FIRCountDetails)
    {

        DataTable dt = JsonConvert.DeserializeAnonymousType(FIRCountDetails, new { field_inspection_details = default(DataTable) }).field_inspection_details;
        for (int i = 1; i <= dt.Rows.Count; i++)
        {
            string lblID = i.ToString();
            decimal MixedMale = Convert.ToDecimal(dt.Rows[i]["offGenderFemale"]);
            decimal MixedFemale = Convert.ToDecimal(dt.Rows[i]["offGenderMale"]);
            decimal MixedStraight = Convert.ToDecimal(dt.Rows[i]["offPollenFemaleLines"]);
            decimal FemaleReadyToPollen = Convert.ToDecimal(dt.Rows[i]["offPollenFemalePlants"]);
            decimal FemaleSparklingPallen = Convert.ToDecimal(dt.Rows[i]["offReceptive"]);
            decimal OthersPollening = Convert.ToDecimal(dt.Rows[i]["offVariety"]);
            decimal DiseasedMale = Convert.ToDecimal(dt.Rows[i]["seedGenderFemale"]);
            decimal DiseasedFemale = Convert.ToDecimal(dt.Rows[i]["seedGenderMale"]);
            decimal DiseasedStraight = Convert.ToDecimal(dt.Rows[i]["seedOtherCrop"]);
            decimal CannotSeparate = Convert.ToDecimal(dt.Rows[i]["seedVariety"]);
            decimal Objectionable = Convert.ToDecimal(dt.Rows[i]["seedWeedPlants"]);
            string query = @"insert into FieldInspectionCropDetails 
                                 values('" + lblID + "','"
                             + FieldInspectionId + "','"
                             + MixedMale + "','"
                             + MixedFemale + "','"
                             + MixedStraight + "','"
                             + FemaleReadyToPollen + "','"
                             + FemaleSparklingPallen + "','"
                             + OthersPollening + "','"
                             + DiseasedMale + "','"
                             + DiseasedFemale + "','"
                             + DiseasedStraight + "','"
                             + CannotSeparate + "','"
                             + Objectionable + "')";
            common.ExecuteNonQuery(query);
        }

    }

}
