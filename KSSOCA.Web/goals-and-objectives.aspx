﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="goals-and-objectives.aspx.cs" Inherits="KSSOCA.Web.goals_and_objectives" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid nopadding">
        <div class="container">
            <h1>About Us</h1>
            <p>Karnataka State Seed Certification Agency was established in the year 1974 under  section-8 of the Seeds act 1966 as an autonomous body and registered under the  karnataka societies registration act 1960 with the objectives of regulating the quality  of seeds certified in the state.</p>
            <h3>Seed Certification </h3>

            <p>Seed Certification is a legally sanctioned system designed to secure, maintain and  make available certain prescribed standards of genetic identity and purity, physical  purity, physiological quality and seed health of notified kinds and varieties including  vegetatively propagating materials of superior crop plant varieties.</p>
            <h3>Stages of Seed Certification </h3>
            <div>
                <ol start="1">
                    <li>Source Verification of Seeds</li>
                    <li>Reciept and Securiety of Form-I</li>
                    <li>Field inspection</li>
                    <li>Seed processing,upgrading and sampling
                    <li>Seed quality analysis and genetic purity evaluation</li> 
                    <li>Seed treatment</li>
                    <li>Bagging and Tagging</li>
                    <li>Grant of certificate</li>
                    <li>Issue of form-II(Realse order)</li>
                    <li>Quality seeds to farmers</li>
                </ol>
            </div>

            <h2>Goals</h2>
            <ul>
                <li>To provide quality certified seeds to the farmers to increase production &amp; productivity</li>
                <li>Increase area under seed production and increase the availability of certified seeds to the farmer hereby increasing 
                    the seed replacement rate(SSR)</li>
                <li>Inspect seed processing plants to see that the admixtures of other kind and varieties are not introduced</li>
                <li>Reduce the percentage of failure of seeds at field,unit as wel as lab level</li>
                <li>Improve the infrastructure facilities of KSSCA for effective and timely certification activities.</li>
            </ul>
            <h2>Objectives</h2>
            <ul>
                <li>Certify seeds of any notified kinds or varieties(under Section-5 of Seeds Act 1966)</li>
                <li>Maintain a list of sources of Breeder &amp; Foundation seeds approved by the Central Seed Certification Board;</li>
                <li>Outline the procedure for the submission of appliction for growing,harvesting,processing,storage,labelling and tagging of seeds intended for certification till the end,So as to ensure that seed lot's finally approved for certification are true to variety.</li>
                <li>Verify,upon receipt of an application for certification that variety is eligible for certification,that the seed used for planting is from approved source and the application has been submitted in accordance with the procedure priscribed for it.</li>
                <li>Inspect Seed feilds to ensure that the minimum standards for isolation,rouging,use of male sterility(wherevere applicable) and similar factor are maintained at all times,as well as to ensure that those provided in the standards for certification;</li>
                <li>Inspect seed processing plants to see that the admixtures of other kind and varieties are not introduced;</li>
                <li>Inspect seed lot's produced and take sample as per procedure and have such sample tested to ensure that the seed conforms to the pricribed standards for certification;</li>
                <li>Grant Certificates(including tags,Form-II etc...,)in accordance with the provision of the Seed Act;</li>
                <li>Ensure that action at all stages of seed certification is taken expeditiously. Undertake educational programmes designed to promote the use of certified seeds,including publication listing certified seed growers and source of certified seed.</li>
            </ul>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>