﻿(function ($) {
    $(document).ready(function () {

        var dynamicTemplate;
        var ParticularData;
        var dynamicTable = $("#dynamicTable");

        bindParticularData();



      

        $("#add_row").click(function () {
            var row = $("tbody tr", dynamicTable);
            var data = {
                "ParticularData": ParticularData, "templateData":
                [{   
                    'SPUParticular_Id': 1,
                    'No': "",
                    'Type': "",
                    'Make': "",
                    'Capacity': "",
                    'Working_Condition': "",
                }]
            };

            var _htmlTemplate = prepareTemplate(data);

            if (row.length <= 0) {
                row = $("tbody", dynamicTable);
                $("tbody", dynamicTable).html(_htmlTemplate);
            }
            else {
                $(_htmlTemplate).insertAfter($(row).last());
            }
        });

        $("#delete_row").click(function () {
            $("tbody tr", dynamicTable).last().remove();
        });

        function prepareTemplate(data) {

            var ParticularData = data.ParticularData;
            var templateData = data.templateData;
            var tableRowsHtml = "";

            $.each(templateData, function (index, data) {
                var Particulars = document.createElement('select');
                var SPUParticular_Id = document.createElement('input');
                var No = document.createElement('input');               
                var Type = document.createElement('input');
                var Make = document.createElement('input');
                var Capacity = document.createElement('input');
                var Working_Condition = document.createElement('input');

                Particulars.className = 'form-control';
                SPUParticular_Id.type = 'hidden';
                No.className = 'form-control';
                Type.className = 'form-control';
                Make.className = 'form-control';
                Capacity.className = 'form-control';
                Working_Condition.className = 'form-control';

                Particulars.name = 'Particulars[]';
                SPUParticular_Id.name = 'SPUParticular_Id[]';
                No.name = 'No[]';
                Type.name = 'Type[]';
                Make.name = 'Make[]';
                Capacity.name = 'Capacity[]';
                Working_Condition.name = 'Working_Condition[]';

                SPUParticular_Id.defaultValue = (data.SPUParticular_Id == null) ? "" : data.Id;
                No.defaultValue = (data.No == null) ? "" : data.No;
                Type.defaultValue = (data.Type == null) ? "" : data.Type;
                Make.defaultValue = (data.Make == null) ? "" : data.Make;
                Capacity.defaultValue = (data.Capacity == null) ? "" : data.Capacity;
                Working_Condition.defaultValue = (data.Working_Condition == null) ? "" : data.Working_Condition;

                for (var i = 0; i < ParticularData.length; i++) {
                    var options = document.createElement('option');
                    options.value = ParticularData[i].Id;
                    options.text = ParticularData[i].Name;

                    if (options.value == data.Crop_Id)
                        options.defaultSelected = true;

                    crops.appendChild(options);
                }

                var tr = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');
                var td4 = document.createElement('td');
                var td5 = document.createElement('td');
                var td6 = document.createElement('td');

                td1.appendChild(SPUParticular_Id);
                td1.appendChild(Particulars);
                td2.appendChild(No);
                td3.appendChild(Type);
                td4.appendChild(Make);
                td5.appendChild(Capacity);
                td6.appendChild(Working_Condition);

                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);
                tr.appendChild(td4);
                tr.appendChild(td5);
                tr.appendChild(td6);
                tableRowsHtml += tr.outerHTML;
            });

            return tableRowsHtml;
        }

        function bindParticularData() {
            var options = {
                type: "POST",
                url: siteRoot + 'WebApi.aspx/GetAllCrops',
                async: true,
                cache: true,
                contentType: "application/json; charset=utf-8",
                failure: function (err) { console.log(err); },
                success: function (response) {
                    if (response != null && response.d != null) {
                        ParticularData = $.parseJSON(response.d);
                        var templateData = $.parseJSON($('#ContentPlaceHolder1_ParticularsOfferred').val() || "{}");

                        var data = { "ParticularData": ParticularData, "templateData": templateData };
                        $("tbody", dynamicTable).html(prepareTemplate(data));
                    }
                }
            };
            $.ajax(options);
        };
    });
})(jQuery);