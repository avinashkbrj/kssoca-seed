﻿(function ($) {
    $(document).ready(function () {
        var dynamicTemplate;
        var cropData;
        var dynamicTable = $("#dynamicTable");

        $("#add_row").click(function () {
            var row = $("tbody tr", dynamicTable);
            var data = {
                "cropData": cropData, "templateData":
                [{
                    'LotNo': "",
                    'Quantity': "",
                }]
            };
            alert($("#hdnLotno").defaultValue);
            var _htmlTemplate = prepareTemplate(data);

            if (row.length <= 0) {
                row = $("tbody", dynamicTable);
                $("tbody", dynamicTable).html(_htmlTemplate);
            }
            else {
                $(_htmlTemplate).insertAfter($(row).last());
            }
        });         

        $("#delete_row").click(function () {
            $("tbody tr", dynamicTable).last().remove();
        });
        
        function prepareTemplate(data) {

            var cropData = data.cropData;
            var templateData = data.templateData;
            var tableRowsHtml = "";

            $.each(templateData, function (index, data) {
                var LotNo = document.createElement('input');
                var Quantity = document.createElement('input').attr("type", "number");
                Quantity.className = 'form-control';                
                LotNo.className = 'form-control';
                Quantity.name = 'Quantity[]';
                LotNo.name = 'LotNo[]';
                 
                LotNo.defaultValue = (data.LotNo == "") ? "Please lot no" : data.LotNo;
                Quantity.defaultValue = (data.Quantity == "") ? "Please enter value" : data.Quantity;
                //Quantity.onkeypress(
                //    function CheckDecimal(evt) {
                //        {
                //            evt = (evt) ? evt : window.event;
                //            var code = (evt.which) ? evt.which : evt.keyCode;
                //            if ((code < 46 || code > 57)) return false;
                //        }
                //    }
                //    );
               
                var tr = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                td1.appendChild(Quantity);
                td2.appendChild(LotNo);
                tr.appendChild(td1);
                tr.appendChild(td2);
                tableRowsHtml += tr.outerHTML;
            });
            return tableRowsHtml;
        }
    });
})(jQuery);