﻿(function ($) {
    $(document).ready(function () {

        // init datepicker
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy'
        });

        // Show Preview
        $('.showPreview').on('change', function (e) {
            showPreview(this);
        });


    });

    function showPreview(input) {
        if (input.files && input.files[0]) {
            if (input.files[0].type.match('image.*')) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var span = document.createElement('span');
                    span.innerHTML = '<img class="thumb" src="' + e.target.result + '" title="' + escape(input.files[0].name) + '" />';
                    $(input).next('span').remove();
                    $(span).insertAfter(input);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        else {
            $(input).next('span').remove();
        }
    }
})(jQuery);