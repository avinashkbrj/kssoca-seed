﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddBranch.aspx.cs" Inherits="KSSOCA.Web.Masters.AddBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container" align="center">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="Branch Registration"> </asp:Label>
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Green"></asp:Label>
            <asp:HiddenField runat="server" ID="hdnId" />
        </div>
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr>
                <td>Brnch </td>
                <td>
                    <asp:Label ID="lblname" runat="server" Font-Bold="true"></asp:Label>-
                    <asp:TextBox runat="server" CssClass="form-control" ID="Name" placeholder="Branch" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ErrorMessage="Name can't be empty" ValidationGroup="v1"></asp:RequiredFieldValidator></td>
           
                <td>Mobile Number</td>
                <td>
                    <asp:TextBox runat="server" class="form-control" ID="MobileNumber" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="MobileNumberRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="MobileNumber" ErrorMessage="Mobile Number can't be empty" ValidationGroup="v1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="MobileNumberMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[0-9]{10,10}$" ControlToValidate="MobileNumber"
                        ErrorMessage="Please enter valid mobile number" ValidationGroup="v1"></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td>Email ID</td>
                <td><asp:TextBox runat="server" class="form-control" ID="EmailId" placeholder="Email Id" />
                    <asp:RequiredFieldValidator runat="server" ID="EmailRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ErrorMessage="Email Id can't be empty" ValidationGroup="v1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="EmailRegEx" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="EmailId" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                        ErrorMessage="Please enter valid email id" ValidationGroup="v1"></asp:RegularExpressionValidator></td>
            
                <td>User Name</td>
                <td>
                    <asp:TextBox runat="server" class="form-control" ID="txtusename" placeholder="User Name" MaxLength="25" OnTextChanged="txtusename_TextChanged" AutoPostBack="true" />
                    <asp:RequiredFieldValidator runat="server" ID="Requiredusename" Style="color: red" Display="Dynamic"
                        ControlToValidate="txtusename" ErrorMessage="User Name  Required" ValidationGroup="v1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="Regularusename" Style="color: red" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9_]{5,25}$" ControlToValidate="MobileNumber"
                        ErrorMessage="Please enter  minimum 5 maximum 25 character of A-Z a-z 0-9" ValidationGroup="v1"></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td>Password</td>
                <td colspan="3">
                    <asp:TextBox runat="server" TextMode="Password" class="form-control" ID="Password" />
                    <asp:RequiredFieldValidator runat="server" ID="PasswordRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Password" ErrorMessage="Password can't be empty" ValidationGroup="v1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="PasswordRegEx" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{5,75}$" ControlToValidate="Password" ValidationGroup="v1"
                        ErrorMessage="Password required min of 5 and max of 20 characters."></asp:RegularExpressionValidator></td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" ValidationGroup="v1" />
                    &nbsp;
                     <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                         ID="btnUpdate" Text="Update" Visible="false" OnClick="btnUpdate_Click" OnClientClick="return confirm('Are you sure you want to Update the crop?');" />
                    &nbsp;
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" Visible="false" /></td>
            </tr>
        </table>
        <br />
        <asp:GridView runat="server" ID="CropMasterGridView" Width="100%"
            AutoGenerateColumns="false" class="table-condensed">
            <Columns>
                <asp:TemplateField HeaderText="Sl.No.">
                    <ItemTemplate>
                        <%#Container.DataItemIndex+1 %> .
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Crop Type" />
                <asp:BoundField DataField="MobileNo" HeaderText="Crop Name" />
                <asp:BoundField DataField="EmailId" HeaderText="Inspection Charge(in Rs.)" /> 
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkUpdate" Text="Update" CssClass="btn-default" CommandArgument='<%#Eval("Id")%>' OnClick="lnkUpdate_Click"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button runat="server" ID="lnkDelete" Text="Delete" CommandArgument='<%#Eval("Id")%>' OnClick="lnkDelete_Click"
                            OnClientClick="return confirm('Are you sure you want to delete?');" Enabled="false"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>-------------Records Not Found-------------------</EmptyDataTemplate>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
