﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RoleMasterCreate.aspx.cs" Inherits="KSSOCA.Web.Masters.RoleMasterCreate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="css/registration.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="container">
        <h1>Role Master</h1>
        <div class="form-horizontal">
            
            <div class="form-group">
                <div class="col-lg-8 col-lg-offset-4">
                    <asp:Label runat="server" ID="lblMessage"></asp:Label>

                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">Short Role Name</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" Id="ShortName" placeholder="ShortName of the Role" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{3,15}$" ControlToValidate="Name"
                        ErrorMessage="Short Role Name required min of 3 characters and max of 75"></asp:RegularExpressionValidator>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label no-top-padding">Role Name</label>
                <div class="col-lg-8">
                    <asp:TextBox runat="server" CssClass="form-control" Id="Name" placeholder="Name of the Role" CausesValidation="true" />
                    <asp:RequiredFieldValidator runat="server" ID="NameRequired" CssClass="text-danger" Display="Dynamic"
                        ControlToValidate="Name" ErrorMessage="Name can't be empty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="NameMinMax" CssClass="text-danger" Display="Dynamic"
                        ValidationExpression="^[A-Za-z0-9@&#.()_\s-]{5,50}$" ControlToValidate="Name"
                        ErrorMessage="Role Name required min of 5 characters and max of 75"></asp:RegularExpressionValidator>
                </div>
            </div>

            
            <div class="form-group">
                <div class="col-lg-4 col-lg-offset-4">
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Save" Text="Save" OnClick="Save_Click" />
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                        ID="Cancel" Text="Cancel" OnClick="Cancel_Click" />
                </div>
            </div>
        <div class="col-lg-12">
                <asp:Label Visible="false" runat="server" ID="lblSuccess">Role Master Entry successful. Go to <a href="/index.aspx">Home</a></asp:Label>
        </div>
            </div>
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
     <script src="lib/bootstrap/js/bootstrap.validator.js"></script>
</asp:Content>
