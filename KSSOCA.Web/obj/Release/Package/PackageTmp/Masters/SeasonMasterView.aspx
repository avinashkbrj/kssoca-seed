﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeasonMasterView.aspx.cs" Inherits="KSSOCA.Web.Masters.SeasonMasterView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h1>Season Master</h1>

        <div class="container">
            <div class="btn-toolbar">
                <asp:Button runat="server" ID="Add" class="btn btn-primary" OnClick="Add_Click" Text="Add New" />
            </div>

            <div class="well">
                <asp:ObjectDataSource runat="server" ID="SeasonMasterDataSource" TypeName="KSSOCA.Web.Masters.SeasonMasterView"
                    SelectMethod="SelectMethod" DeleteMethod="DeleteMethod">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="id" />
                    </UpdateParameters>
                </asp:ObjectDataSource>

                <asp:GridView runat="server" ID="SeasonMasterGridView" DataSourceID="SeasonMasterDataSource"
                    AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover"
                    AllowPaging="true" PageSize="25" AllowSorting="true" DataKeyNames="Id">
                    <Columns>
                        <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="Id"
                            DataNavigateUrlFormatString="SeasonMasterEdit.aspx?Id={0}">
                        </asp:HyperLinkField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns><asp:BoundField DataField="Season" HeaderText="Season" /> </Columns>
                    <Columns><asp:BoundField DataField="FromMonth" HeaderText="From Month" /> </Columns>
                    <Columns><asp:BoundField DataField="ToMonth" HeaderText="ToMonth" /> </Columns>
                 <EmptyDataTemplate></EmptyDataTemplate></asp:GridView>

            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
