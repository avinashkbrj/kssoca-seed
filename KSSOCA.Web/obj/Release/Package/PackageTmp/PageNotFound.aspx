﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PageNotFound.aspx.cs" Inherits="KSSOCA.Web.PageNotFound" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <h1>Page not found</h1>
        <br />
        <h4>The page you are requested is not found on the server or something went wrong.</h4>
        <h4>If you have any questions, please contact the site administrator.</h4>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
