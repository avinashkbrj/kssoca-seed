﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Form2Certificate.aspx.cs" Inherits="KSSOCA.Web.Reports.Form2Certificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlPrint.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>SPU Registration Certificate</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center">
        <br />

        <asp:Panel ID="pnlPrint" runat="server" Width="750px" Height="1000px">
            <asp:Panel ID="pnlcontent" Width="810px" Height="1000px" runat="server" BorderStyle="Solid" BorderColor="Gray" BorderWidth="15px">
                <table Width="100%" style="text-align: center;">
                    <tr style="font-weight: bold; color: navy; font-size: 10pt">
                        <td style="float:left;padding: 10px;" >  <asp:Image ID="imgLogo" runat="server" Width="40px" Height="40px" ImageUrl="../img/newKSSCA-LOGO.gif" /> </td> 
                          
                       <td> KARNATAKA STATE SEED AND ORGANIC CERTIFICATION AGENCY </td>
                        <td style="float:right;"><asp:Image ID="Image1" runat="server" Width="40px" Height="40px" ImageUrl="../img/organic logo.jpg" /> 
                       </td> 

                    </tr>
                    
                </table>
                 <table  style="width:100%;text-align: center;margin-top: -17px;">
                    <tr>
                        <td colspan="2" style="font-weight: bold; color: navy; font-size: 8pt">ಕರ್ನಾಟಕ ರಾಜ್ಯ  ಬೀಜ ಮತ್ತು ಸಾವಯವ ಪ್ರಮಾಣನ ಸಂಸ್ಥೆ</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold; color: navy; font-size: 8pt">CERTIFICATE IN FORM II (RELEASE ORDER)</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold; color: navy; font-size: 8pt">(Granted under sub-section (3) section 9 of the Seed Act,1966)</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold; color: navy; font-size: 8pt">(A GOVT. OF KARNATAKA UDERTAKING)</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-weight: bold; color: navy; font-size: 8pt">ಬ್ಯಾಪ್ಟಿಸ್ಟ್ ಆಸ್ಪತ್ರೆ ಎದುರು, ಬಳ್ಳಾರಿ ರಸ್ತೆ, ಹೆಬ್ಬಾಳ,ಕೆ.ಎ.ಐ.ಸಿ ಆವರಣ, ಬೆಂಗಳೂರು -560 024</td>
                    </tr>
                </table>
                <table  style="width:100%;margin-top: 14px;">
                    <tr>
                        <td style="width:50%;padding: 4px;text-align: left;font-weight: bold; color: navy; font-size: 10pt">CERTIFICATE NO : <asp:Label ID="lblCertificateNo" runat="server" Font-Bold="true"></asp:Label></td>
                        
                        <td style="width:50%;text-align: right;font-weight: bold; color: navy; font-size: 10pt">Date :  <asp:Label ID="lbldate" runat="server" Font-Bold="true"></asp:Label></td>
                        
                    </tr>
                </table>
                 <div style="text-align: justify;font-weight: bold; color: navy;padding: 4px;padding-top:4px; font-size: 10pt">This is to certify that the seed lot to which this certificate pertains has been produced according to and found to confirm
                            to the standards prescribed for Certification under the Seed Act, 1966. The details of the lot or given below:      
                 </div>
                 <table  style="width:100%;">
                    <tr>
                        <td style="width:50%;text-align: left;padding: 6px; font-size: 9pt">Crop : <asp:Label ID="lblCrop" runat="server" Font-Bold="true"></asp:Label></td>
                        
                        <td style="width:50%; font-size: 9pt">Variety : <asp:Label ID="lblVariety" runat="server" Font-Bold="true"></asp:Label></td>
                        
                    </tr>
                     <tr>
                        <td style="width:50%;text-align: left;padding: 6px; font-size: 9pt">Class of seed : <asp:Label ID="lblclass" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%; font-size: 9pt">Lot No : <asp:Label ID="lblLotno" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width:50%;text-align: left;padding: 6px; font-size: 9pt">Quantity of seed in the lot(kg) : <asp:Label ID="lblquantity" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">Packing size Kg : <asp:Label ID="lblPackingSize" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                </table>
                <table  style="width:100%;">
                    
                     <tr>
                        <td style="width:50%;text-align: left;padding: 6px; font-size: 9pt">Validity of certificate  : <asp:Label ID="lblvalidity" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%; font-size: 9pt">Lab report No :<asp:Label ID="lblTestNo" runat="server" Font-Bold="true"></asp:Label></td>
                        
                    </tr>
                     <tr>
                        <td style="width:50%;text-align: left;padding: 6px; font-size: 9pt">Date of test :<asp:Label ID="lblDateofTest" runat="server" Font-Bold="true"></asp:Label></td>
                         <td style="width:50%;font-size: 9pt">Genetic purity test report No  :<asp:Label ID="lblGeneticPurity" runat="server" Font-Bold="true"></asp:Label></td>
                     </tr>
                     <tr>
                        <td style="float:left;padding: 6px;font-size: 9pt">Total No.of tags issued with Sl.No : <asp:Label ID="lbltotaltags" runat="server" Font-Bold="true"></asp:Label></td>
                     </tr>
                     <tr>
                        <td style="float:left;padding: 6px; font-size: 9pt">Total No.of tags used with Sl.No :<asp:Label ID="lblusedtags" runat="server" Font-Bold="true"></asp:Label></td>
                     </tr>
                    <tr>
                        <td style="float:left;padding: 6px;font-size: 9pt">Total No.of tags unused with Sl.No :<asp:Label ID="lblunusedtags" runat="server" Font-Bold="true"></asp:Label></td>
                     </tr>
                </table>
                 <table  style="width:100%;">
                    <tr>
                        <td style="width:50%;padding: 6px;float:left; font-size: 9pt">1. Pure Seed :<asp:Label ID="lblpureseed" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">6. Objectionable weed seed(Nos/Kg) :<asp:Label ID="lblobjectionable" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width:50%;float:left;padding: 6px;font-size: 9pt">2. Inert matter : <asp:Label ID="lblInert" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">7. Total weed seeds(max) : <asp:Label ID="lbltotalweed" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width:50%;float:left;padding: 6px;font-size: 9pt">3. Germination :<asp:Label ID="lblgermination" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">8. Genetic Purity :<asp:Label ID="lblGenetic" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width:50%;float:left;padding: 6px;font-size: 9pt">4. Moisture : <asp:Label ID="lblSeedMoisture" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">9.ODV (Nos/Kg) :<asp:Label ID="lblodv" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="float:left;padding: 6px; font-size: 9pt">5. Other crop seeds(Nos/Kg) : <asp:Label ID="lblOtherCrop" runat="server" Font-Bold="true"></asp:Label></td>
                         
                    </tr>
                     <tr>
                        <td style="width:50%;float:left;padding: 6px; font-size: 9pt">Growers Name :<asp:Label ID="lblGrowerName" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">Village : <asp:Label ID="lblGrowerVillage" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width:50%;float:left;padding: 6px; font-size: 9pt">Taluka : <asp:Label ID="lblGrowerTaluka" runat="server" Font-Bold="true"></asp:Label></td>
                        <td style="width:50%;font-size: 9pt">District :<asp:Label ID="lblGrowerDistrict" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                     <tr>
                        <td style="float:left;padding: 6px; font-size: 9pt">Producer's Name : <asp:Label ID="lblproducerName" runat="server" Font-Bold="true"></asp:Label></td>
                        
                    </tr>
                </table>
                 <div style=" text-align: justify; font-weight: bold; font-size: 9pt;margin-top: 8px;padding: 5px;">Certification is Valid upto a period of nine month from the date seed testing
                            provided the seed is stored under ideal conditions. Use of seed after expiry of the validity period by any one is entirely at his risk and the seed
                            Certification agency shall not be responsible to him for any damage to the seed. No one should accept the seed if the Certification tag or seal is absent or has been
                            tempered with the person to whom this certification is granted is entitled for a Certification tag for each conatiner in the lot and shall follow the provisions for labeling under 
                            the Act.
                        
                    </div>
                 <table  style="width:100%;">
                   <%-- <tr style="font-weight: bold; color: navy; font-family: Algerian; font-size: 16pt">
                        <td colspan="2">
                            <br />
                            <asp:Label ID="Label4" runat="server"></asp:Label></td>
                    </tr>--%>
                   
                    <tr>
                        <td colspan="1" style="padding: 6px;">Signature of the Producer/Grower :
                    <asp:Label ID="Label5" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="1"  style="padding: 6px;">Note : @Mentioned whereever necessary</td>
                    </tr>
                     
                    <tr style="text-align: left">
                        <td style="text-align:left; padding: 6px;font-size: 9pt">
                            <br />
                            Ref: Director K.S.S.O.C.A
                            <br />
                            STL Letter No & Date : <asp:Label ID="lblstlNo" runat="server"></asp:Label> & <asp:Label ID="lblStlTestDate" runat="server"></asp:Label>
                            <br />
                            GOT Letter No & Date : <asp:Label ID="lblgotNo" runat="server"></asp:Label> & <asp:Label ID="lblGotTestDate" runat="server"></asp:Label>
                            <asp:Label ID="Label6" runat="server" Font-Bold="true"></asp:Label>
                           
                        </td> <td align="right" style="vertical-align: top; font-weight: bold; font-size: 8pt"><span id="kssoca" style="margin-right:30px;"> For KARNATAKA STATE SEED AND ORAGANIC CERTIFICATION AGENCY </span><br /><br />
                        <span style="margin-right: 244px;font-weight: bold;">Sd /-</span><br />
                        <span style="margin-right: 200px;font-weight: bold;">(Seed Certification Officer)<span>
                        </span></span>
                        </td>
                          
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
        </asp:Panel>
        <br />
        <asp:Button ID="btndownload" runat="server" Text="Download" OnClick="btndownload_Click" Visible="false" />
        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>

