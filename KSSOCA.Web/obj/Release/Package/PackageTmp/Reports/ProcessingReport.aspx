﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProcessingReport.aspx.cs" Inherits="KSSOCA.Web.Reports.ProcessingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container-fluid">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Seed Processing Details"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <asp:GridView ID="gvSourceVerification" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%" OnRowCreated="gvSourceVerification_RowCreated">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ProdName" HeaderText="Producer Name" />
                        <asp:BoundField DataField="ProducerRegNo" HeaderText="Producer Reg No." />
                        <asp:BoundField DataField="growername" HeaderText="Grower Name" />
                        <asp:BoundField DataField="crop" HeaderText="Crop" />
                        <asp:BoundField DataField="variety" HeaderText="Variety" />
                        <asp:BoundField DataField="class" HeaderText="Class" />
                        <asp:BoundField DataField="Areaoffered" HeaderText="Area Offered" />
                        <asp:BoundField DataField="InspectedDate" HeaderText="Inspected Area" />
                        <asp:BoundField DataField="AcceptedArea" HeaderText="Accepted Area" />
                        <asp:BoundField DataField="ReportNO" HeaderText="FIR Report No." />
                        <asp:BoundField DataField="EstimatedYield" HeaderText="Estimated Yield" />
                        <asp:BoundField DataField="ArrivalDate" HeaderText="Bulk Arrival Date" />
                        <asp:BoundField DataField="BulkStock" HeaderText="Bulk Stock" />
                        <asp:BoundField DataField="Moisture" HeaderText="Moisture" />
                        <asp:BoundField DataField="DateOfCleaning" HeaderText="Date Of Cleaning" />
                        <asp:BoundField DataField="ScreenRejection" HeaderText="Screen Rejection" />
                        <asp:BoundField DataField="BlownOff" HeaderText="Blown Off" />
                        <asp:BoundField DataField="TotalRejection" HeaderText="Total Rejection" />
                        <asp:BoundField DataField="CleanedSeed" HeaderText="Cleaned Seed" />
                        <asp:BoundField DataField="Bags" HeaderText="No of Gunny Bags" />
                        <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
