﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SPUList.aspx.cs" Inherits="KSSOCA.Web.Reports.SPUList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center">
                <%--class="container"--%>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                     
                    <tr>
                        <td width="30%">Get Seed Processing Units By :&nbsp;&nbsp;
                            <asp:DropDownList runat="server" ID="ddlfilter" Width="200px" Height="25px" AutoPostBack="true" OnSelectedIndexChanged="ddlfilter_SelectedIndexChanged">
                                <asp:ListItem Text="Registration No." Value="R"></asp:ListItem>
                                <asp:ListItem Text="Division wise." Value="D"></asp:ListItem>
                                <asp:ListItem Text="All SPU" Value="A"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <%-- </tr>
                    <tr>--%>
                        <td width="70%">
                            <asp:Panel ID="pnlDivision" runat="server" Visible="false">
                                Select Divisional Office. 
                                <asp:DropDownList runat="server" ID="ddlDivision" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredddlDivision" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="ddlDivision" ErrorMessage="Divisional Office is Required" InitialValue="0"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                       
                            </asp:Panel>
                            <asp:Panel ID="pnlsearchbyreg" runat="server" Visible="true">
                                Registration No.:&nbsp;&nbsp;<span style="color:red; font-weight:bold">*</span><asp:TextBox runat="server" ID="txtRegNo" placeholder="Registration No." />
                                <asp:RequiredFieldValidator runat="server" ID="txtRegNoRequiredValidator" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRegNo" ErrorMessage="Registration No is Required" ValidationGroup="v1"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                                <asp:Button ID="btngetformdetails" runat="server" Text="Get Producer Details" CssClass="btn-primary" ValidationGroup="v1" OnClick="btngetformdetails_Click" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                </table>
                <br />
            </div>
            <div>
                <asp:GridView ID="gvProducer" runat="server" class="table-condensed" AutoGenerateColumns="false" Width="100%">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="ProName" HeaderText="Seed Processing Unit Name" />
                        <asp:BoundField DataField="SPUNO" HeaderText="SPU No." />
                        <asp:BoundField DataField="RegistrationNo" HeaderText="Registration No." />
                        <asp:BoundField DataField="MobileNo" HeaderText="Mobile No." />
                        <asp:BoundField DataField="Address" HeaderText="Address" />
                        <asp:BoundField DataField="DivisionalOffice" HeaderText="Divisional Office" />
                        <asp:BoundField DataField="Validity" HeaderText="Validity date of registration" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkview" runat="server" Text="View full details" PostBackUrl='<%# "~/Transactions/SPURegistrationApproval.aspx?Id=" + Eval("Id") %>'></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>

                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
