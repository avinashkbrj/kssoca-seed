﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SPURegistrationCertificate.aspx.cs" Inherits="KSSOCA.Web.Reports.SPUCertificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function PrintPanel() {
            var panel = document.getElementById("<%=pnlPrint.ClientID %>");
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>SPU Registration Certificate</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center">
        <br />

        <asp:Panel ID="pnlPrint" runat="server" Width="730px">
            <asp:Panel ID="pnlcontent" Width="510px" runat="server" BorderStyle="Solid" BorderColor="Gray" BorderWidth="20px">
                <table width="100%" style="font-size: medium; text-align: center; padding:10px 10px 10px 10px" >
                    <tr style="font-weight: bold; color: navy; font-size: 10pt">
                        <td colspan="2">
                            <br />
                            KARNATAKA STATE SEED AND ORGANIC CERTIFICATION AGENCY</td>
                    </tr>
                    <tr>
                        <td colspan="2">BANGALORE</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:Image ID="imgLogo" runat="server" Width="200px" Height="200px" ImageUrl="../img/newKSSCA-LOGO.gif" />
                        </td>
                    </tr>
                    <tr style="font-weight: bold; color: navy; font-family: Algerian; font-size: 16pt">
                        <td colspan="2">
                            <br />
                            <asp:Label ID="lblTitle" runat="server"></asp:Label></td>
                    </tr>
                    <tr style="font-weight: bold; color: navy; font-family: 'Kunstler Script'; font-size: 36pt">
                        <td colspan="2">Certificate</td>
                    </tr>
                    <tr>
                        <td colspan="2">Online Registration No :
                    <asp:Label ID="lblRegNo" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr style="text-align: justify">
                        <td colspan="2">
                            <br />
                            &nbsp;&nbsp;&nbsp;The SPU No :  
                            <asp:Label ID="lblSPUNo" runat="server" Font-Bold="true"></asp:Label>
                            pertaining to Seed Processing Unit of  
                            <asp:Label ID="lblProducerNameAddress" runat="server" Font-Bold="true"></asp:Label>
                             is hereby registered by Karnataka State Seed and Organic Certification Agency for the period from
                    <asp:Label ID="fromDate" runat="server" Font-Bold="true"></asp:Label>
                            to
                        <asp:Label ID="toDate" runat="server" Font-Bold="true"></asp:Label>
                            for processing and further certification of seed lots of crop varieties/ hybrids notified under <span style="font-weight: bold">Section-05 </span> of the <span style="font-weight: bold">Seeds Act,1966. </span>
                        </td>
                    </tr>
                    <tr style="text-align: left">
                        <td align="left">
                            <br />
                            Bangalore
                    <br />
                            Dated :
                    <asp:Label ID="lblApprovedDate" runat="server" Font-Bold="true"></asp:Label>
                           
                        </td> <td align="right" style="vertical-align: top; font-weight: bold; font-size: 16pt">Director&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
        </asp:Panel>
        <br />
        <asp:Button ID="btndownload" runat="server" Text="Download" OnClick="btndownload_Click" Visible="false" />
        <asp:Button ID="btnPrint" runat="server" Text="Print" OnClientClick="return PrintPanel();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
