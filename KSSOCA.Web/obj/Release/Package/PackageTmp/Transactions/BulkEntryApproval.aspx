﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BulkEntryApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.BulkEntryApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="hdnf1Id" runat="server" />
                    <asp:HiddenField ID="hdnbulkid" runat="server" />
                    <asp:HiddenField ID="hdnSendToID" runat="server" />
                </div>
                <br />
                <table width="100%" border="1" class="table-condensed">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Literal ID="litForm1Details" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Stock In Quintal</td>
                        <td>
                            <asp:Label runat="server" Font-Bold="true" ID="lblNoOfBags" />

                        </td>
                        <td>Bulk Stock (in Kgs.)</td>
                        <td>
                            <asp:Label runat="server" Font-Bold="true" ID="lblBulkStock" />
                        </td>
                    </tr>
                    <tr>
                        <td>Seed Processing Unit
                        </td>
                        <td colspan="3">
                            <asp:Label runat="server" Font-Bold="true" ID="lblSPU" />
                        </td>
                    </tr>
                    <asp:Panel ID="pnlBulkAcceptance" runat="server" Visible="false">
                        <tr>
                            <%-- <td>Form-I Registration No.</td>
                            <td>
                                <asp:Label runat="server" ID="lblform1no" Font-Bold="true"> </asp:Label></td>--%>
                            <td>Bulk Seed Accepted ?</td>
                            <td>
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbIsAccepted" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbIsAccepted_SelectedIndexChanged">
                                    <asp:ListItem class="radio-inline" Value="1" Text="Yes"></asp:ListItem>
                                    <asp:ListItem class="radio-inline" Value="99" Text="No"></asp:ListItem>
                                </asp:RadioButtonList><asp:RequiredFieldValidator runat="server" ID="RequiredrbIsAccepted" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="rbIsAccepted" ValidationGroup="v3" ErrorMessage="Please select Bulk Seed Accepted or not"></asp:RequiredFieldValidator></td>
                            <td>Accepted Bulk Quantity (in Kgs.)</td>
                            <td>
                                <asp:TextBox runat="server" ID="txtAccepted" onkeypress="return CheckNumber(event);" placeholder="Accepted Bulk Quantity" Enabled="false" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredtxtAccepted" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtAccepted" ErrorMessage="Accepted Bulk Quantity" ValidationGroup="v3"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="80%" Height="100px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ValidationGroup="v3" ErrorMessage="Give Remarks" Enabled="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnBulkAccepted" runat="server" Text="Accept Bulk" CssClass="btn-primary"
                                    ValidationGroup="v3" OnClick="btnBulkAccepted_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="pnlBulkAcceptanced" runat="server" Visible="false">
                        <tr>
                            <td>Bulk Seed Accepted ?</td>
                            <td>
                                <asp:Label runat="server" Font-Bold="true" ID="lblbulkAccepted" />
                            <td>Accepted Bulk Quantity (in Kgs.)</td>
                            <td>
                                <asp:Label runat="server" Font-Bold="true" ID="lblAcceptedQuantity" />
                            </td>
                        </tr>


                    </asp:Panel>
                    <asp:Panel ID="pnlforward" runat="server" Visible="false">
                        <tr> 
                            <td colspan="4" align="center">
                                <asp:Label runat="server" Font-Bold="true" ID="lblotherdivision" />
                                <asp:Button ID="btnforward" runat="server" Text="Forward" CssClass="btn-primary"
                                    Visible="false" OnClick="btnforward_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                     <tr class="SideHeading">
                        <td colspan="4">Documents submitted                       
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">a. Bulk Arrival Letter
                            <asp:Literal runat="server" ID="LitBulkArrivalLetter" EnableViewState="false"></asp:Literal>
                        </td>
                        <td valign="top" colspan="3"> b. Undertaking Letter
                            <asp:Literal runat="server" ID="LitUndertakingLetter" EnableViewState="false"></asp:Literal>
                        </td>  
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Bulk Status &nbsp;&nbsp;<asp:Button ID="btnEditApplication" runat="server" Text="Edit Application" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Note: Sl.No. 1 is the current status.
                            &nbsp;&nbsp;
                            <asp:GridView ID="gvstatus" runat="server" class=" table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Transaction Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Transaction Date" />
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </ContentTemplate>
        <%--<Triggers>
            <asp:PostBackTrigger ControlID="btnCheck" />
            <asp:PostBackTrigger ControlID="btnReject" />
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
