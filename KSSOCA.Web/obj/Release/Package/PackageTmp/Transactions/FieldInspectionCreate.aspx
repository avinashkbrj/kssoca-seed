﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FieldInspectionCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.FieldInspectionCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='<%= ResolveUrl("~/css/registration.css")%>' rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center" class="container">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading"> </asp:Label>
        </div>
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
            <asp:HiddenField ID="Id" runat="server" />
        </div>
        
        <br />
        <table class="table-condensed" width="100%" border="1">
            <tr class="Note">
                <td colspan="4">
                    <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                </td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">1.Field Inspection Details
                </td>
            </tr>
            <tr>
                <td>1.Inspection Serial No &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblSerialNo" /></td>
                <td colspan="3">2.Is this inspection is final ?
                    
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbIsFinal" runat="server">
                        <asp:ListItem class="radio-inline" Value="1" Text="Yes"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="0" Text="No"></asp:ListItem>
                    </asp:RadioButtonList><asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbIsFinal" ErrorMessage="Please select inspection is final or not"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>Last inspection details(Inspection Serial No : Date)</td>
                <td colspan="3">
                    <asp:Label Font-Bold="true" runat="server" ID="lblLastInspectionDetails" /></td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">2.Producer Information
                </td>
            </tr>
            <tr>
                <td width="25%">1. Producer Name</td>
                <td width="25%">
                    <asp:Label Font-Bold="true" runat="server" ID="lblProducerName" /></td>
                <td width="25%">2.Producer Registration No</td>
                <td width="25%">
                    <asp:Label Font-Bold="true" runat="server" ID="lblProducerRegNo" /></td>
            </tr>
            <tr class="SideHeading">
                <td colspan="4">3.Grower Information                       
                </td>
            </tr>
            <tr>
                <td colspan="2">1.Form-I Registration No. &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblForm1No" />
                    2. Date of Registration &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblForm1RegDate" /></td>
                <td>3.Name  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblName" />
                </td>
                <td>4.Father's Name :&nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="FathersName" />
                </td>
            </tr>
            <tr>
                <td colspan="4">5.Address&nbsp;:&nbsp;a.District &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="District">
                </asp:Label>
                    &nbsp;&nbsp;b.Taluk &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="Taluk">
                    </asp:Label>
                    &nbsp;&nbsp;c.Hobli&nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlHobli">
                    </asp:Label>
                    &nbsp;&nbsp;d.Village  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlvillage">
                    </asp:Label>&nbsp;&nbsp;e.Mobile No.  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="TelephoneOrMobileNumber"></asp:Label></td>
            </tr>
            <tr>
                <td colspan="4">6.Location of Seed plot&nbsp;:&nbsp; a.Taluk &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotTaluk">
                </asp:Label>
                    &nbsp;&nbsp;b.Hobli  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotHobli">
                    </asp:Label>
                    &nbsp;&nbsp;c.Village  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlPlotVillage">
                    </asp:Label>
                    &nbsp;&nbsp;d.Gram Panchayat  &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotGramPanchayat" />
                    &nbsp;&nbsp;e.Survey No
                        &nbsp;&nbsp;<asp:Label Font-Bold="true" runat="server" ID="LocationOfTheSeedPlotSurveyNumber" />

                </td>
            </tr>
            <tr>
                <td>7.Season &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="Season"></asp:Label></td>
                <td colspan="2">8.Class of seed offered for certification &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ddlClass">
                </asp:Label>
                </td>

                <td>9.Crop &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblCrop" /></td>
            </tr>
            <tr>
                <td>10.Variety &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="lblVariety" /></td>

                <td colspan="2">11.Lot No of Seed Sourced &nbsp;:&nbsp;<asp:GridView ID="gvLotNos" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> . 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed Distributed(in Kg.)" />
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    <FooterStyle Font-Bold="true" />
                </asp:GridView>
                </td>
                <td>12.Date of Sowing / Transplanting(As per form1) &nbsp;:&nbsp;<asp:Label Font-Bold="true" runat="server" ID="ActualDateOfSowingOrTransplanting" />
                </td>
            </tr>
            <%--<tr class="SideHeading">
                <td colspan="4">4.Field Inspection Details                       
                </td>
            </tr>--%>
            <tr>
                <td>13.Parentage </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtParentage" placeholder="Parentage" />
                     <asp:RegularExpressionValidator runat="server" ID="TotalParentageRegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^([0-9]|[1-9][0-9]|100)$" ControlToValidate="txtParentage"
                                ErrorMessage="Percenatge must be below 100%"></asp:RegularExpressionValidator>
                </td>
                <td>14.Actual Date of Sowing </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtActualDateOfSowing" placeholder="Date" onkeydown="return false" />
                    <asp:RequiredFieldValidator runat="server" ID="ActualDateOfSowingOrTransplantingRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="txtActualDateOfSowing" ErrorMessage="Actual Date of Sowing / Transplanting can't be empty"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">15.Documents Verified for Source&nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbDocumentsVerified" runat="server">
                        <asp:ListItem class="radio-inline" Value="T" Text="Tag"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="B" Text="Bag"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="R" Text="Reciept"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="O" Text="Other"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredrbDocumentsVerified" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbDocumentsVerified" ErrorMessage="Please select document verified type"></asp:RequiredFieldValidator>
                </td>
                <td colspan="2">16.Crop Stage&nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbCropStage" runat="server">
                        <asp:ListItem class="radio-inline" Value="P" Text="Vegetative"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="F" Text="Flowering"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="S" Text="Seed Setting"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="H" Text="Pre-Harvest"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredrbCropStage" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbCropStage" ErrorMessage="Please select Crop Stage"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td>17.Previous Crop </td>
                <td>
                    <asp:TextBox ID="txtlastcrop" runat="server" CssClass="form-control"></asp:TextBox></td>
                <td>18.Previous Variety </td>
                <td>
                    <asp:TextBox ID="txtLastVariety" runat="server" CssClass="form-control"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2">19.Isolation Distance &nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbDistanceBetweenSeparation" runat="server">
                        <asp:ListItem class="radio-inline" Value="S" Text="Satisfied"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Not Satisfied"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredrbDistanceBetweenSeparation" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbDistanceBetweenSeparation" ErrorMessage="Please select  Distance Between Separation"></asp:RequiredFieldValidator>

                </td>
                <td>20.Ratio of Male and Female Lines  </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtRatioOfLines" placeholder="Ratio Of Lines" /></td>
            </tr>
            <tr>
                <td>21.Male lines around seed plot </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtMaleLines" placeholder="Male Lines" onkeypress="return CheckNumber(event);" /></td>
                <td colspan="2">14.Crop Status &nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbCropStatus" runat="server">
                        <asp:ListItem class="radio-inline" Value="L" Text="Low"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Normal"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="M" Text="Medium"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="H" Text="High"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredrbCropStatus" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbCropStatus" ErrorMessage="Please select Crop Status"></asp:RequiredFieldValidator></td>

            </tr>
            <tr>
                <td>22.Area registered for seed production </td>
                <td>
                    <asp:Label Font-Bold="true" runat="server" ID="AreaOffered" /></td>
                <td>22.Area Inspected(in acres)  </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAreaInspected" placeholder="Inspected" onkeypress="return CheckDecimal(event);" />
                </td>
            </tr>
            <tr>
                <td>23.Area Rejected(in acres) </td>
                <td>
                    <asp:TextBox runat="server" ID="txtAreaRejected" placeholder="Rejected" CssClass="form-control" onkeypress="return CheckDecimal(event);" /></td>

                <td colspan="2">24.Area Rejected due to &nbsp;:&nbsp;
                    <asp:CheckBoxList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="chkarearejecteddueto" runat="server">
                        <asp:ListItem class="radio-inline" Value="I" Text="Inadequate isolation"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Not Sown"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="H" Text="Harrowed"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="F" Text="Flooding"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="O" Text="OffTypes"></asp:ListItem>

                    </asp:CheckBoxList>
                    <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbCropStatus" ErrorMessage="Please select Crop Status"></asp:RequiredFieldValidator>--%></td>
            </tr>
            <tr>
                <td>25.Area Accepted(in acres)&nbsp;&nbsp; </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtAreaAccepted" placeholder="Accepted" onkeypress="return CheckDecimal(event);" /></td>
            </tr>

            <tr class="SideHeading">
                <td colspan="4">26.Field Inspection Details.  Plants observed in each count  
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbPlantcount" runat="server">
                        <asp:ListItem class="radio-inline" Value="100" Text="100"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="500" Text="500"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="1000" Text="1000"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredrbPlantcount" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbPlantcount" ErrorMessage="Please select Plant count"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:GridView ID="gvFIDetails" runat="server" AutoGenerateColumns="false" Width="100%"
                        AllowPaging="true" PageSize="20" OnRowDataBound="gvFIDetails_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Sl.No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Off Type plants Male">
                                <ItemTemplate>
                                    <asp:TextBox ID="MixedMale" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Off Type plants Female">
                                <ItemTemplate>
                                    <asp:TextBox ID="MixedFemale" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Off Type plants Variety">
                                <ItemTemplate>
                                    <asp:TextBox ID="MixedStraight" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Receptive Silk in Female Lines">
                                <ItemTemplate>
                                    <asp:TextBox ID="FemaleReadyToPollen" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Pollen Shedding Plants/Heads in Female Line">
                                <ItemTemplate>
                                    <asp:TextBox ID="FemaleSparklingPallen" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Pollen Shedding Off Types Plants/Heads">
                                <ItemTemplate>
                                    <asp:TextBox ID="OthersPollening" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Seed Borne Disease Plants/Heads Male">
                                <ItemTemplate>
                                    <asp:TextBox ID="DiseasedMale" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Seed Borne Disease Plants/Heads FeMale">
                                <ItemTemplate>
                                    <asp:TextBox ID="DiseasedFemale" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Seed Borne Disease Plants/Heads Variety">
                                <ItemTemplate>
                                    <asp:TextBox ID="DiseasedStraight" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="InSeperable other Crop Plants/Heads">
                                <ItemTemplate>
                                    <asp:TextBox ID="CannotSeparate" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <Columns>
                            <asp:TemplateField HeaderText="Objectionable Weed Plants">
                                <ItemTemplate>
                                    <asp:TextBox ID="Objectionable" runat="server" Width="100px" onkeypress="return CheckNumber(event);"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">27.Status of Seed Plot  &nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbquality" runat="server">
                        <asp:ListItem class="radio-inline" Value="S" Text="Satisfied"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="N" Text="Not Satisfied"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="Requiredrbquality" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbquality" ErrorMessage="Please select Quality"></asp:RequiredFieldValidator></td>
                <td>28.Estimated Crop Yield (Qntls/Acre.)</td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtEstimatedCropYeildMin" onkeypress="return CheckNumber(event);" placeholder="Min" />
                    To
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtEstimatedCropYeild" onkeypress="return CheckNumber(event);" placeholder="Max" />
                </td>
            </tr>
            <tr>
                <td>29.Harvesting Month </td>
                <td>
                    <%--<asp:TextBox runat="server" CssClass="form-control datepicker" ID="txtHarvestingDate" placeholder="Date of Harvesting" onkeydown="return false" />--%>
                    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control"></asp:DropDownList>
                </td>
                <td colspan="2">
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbportrait" runat="server">
                        <asp:ListItem class="radio-inline" Value="First Fort Night" Text="First Fort Night"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="Second Fort Night" Text="Second Fort Night"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFortNight" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbportrait" ErrorMessage="Please select  Fort Night"></asp:RequiredFieldValidator>

                </td>
            </tr>
            <tr>
                <td>30.lattitude 
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtlatt" placeholder="lattitude" />
                </td>
                <td>31.longitude 
                    <asp:TextBox runat="server" CssClass="form-control" ID="txtlong" placeholder="longitude" />
                </td> <td colspan="2">27.Seed Plot Accepted / Rejected &nbsp;:&nbsp;
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" ID="rbacceptreject" runat="server">
                        <asp:ListItem class="radio-inline" Value="1" Text="Fully /Partially Accepted"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="0" Text="Fully Rejected"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:RequiredFieldValidator runat="server" ID="Requiredrbacceptreject" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="rbacceptreject" ErrorMessage="Please select Seed Plot Accepted / Rejected"></asp:RequiredFieldValidator></td>
            </tr> 
            <tr>
                <td>32.OTP Verification </td>
                <td>
                    <asp:TextBox runat="server" CssClass="form-control" ID="otpFromFarmer" MaxLength="4" placeholder="OTP" onkeypress="return CheckNumber(event);" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidatorOtpFromFarmer" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="otpFromFarmer" ErrorMessage="Please Verifty the OTP First"></asp:RequiredFieldValidator>
                    <asp:Label runat="server" ID="lblotpMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                            
                </td>
                <td colspan="2">
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false" ID="btnSendOtp" Text="Send OTP" OnClick="btnSendOtp_Click" OnClientClick="javascript:scroll(0,0);" />    
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"  ID="btnResendOtp" Text="Re-Send OTP" OnClick="ReSendOtp_Click" OnClientClick="javascript:scroll(0,0);" Visible="false"/>    
              </td>
            </tr>
             <tr> <td colspan="2">33.Field Image :<asp:FileUpload runat="server" CssClass="form-control" ID="FieldImage" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldImage" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="FieldImage" ErrorMessage="Field Image Required"></asp:RequiredFieldValidator>
                  <cst:FileSizeValidator runat="server" id="FieldImageSizeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FieldImage" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>   
                 <cst:FileTypeValidator ID="FarmerSignFileTypeValidator" runat="server"
                        ControlToValidate="FieldImage"
                        ForeColor="Red" Display="Dynamic"
                        ErrorMessage="Allowed file types are {0}.">
                    </cst:FileTypeValidator>
                </td>
                <td colspan="2">34.Selfie with Farmer Image:<asp:FileUpload runat="server" CssClass="form-control" ID="SelefiWithFarmer" accept="image/*" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldSelefiWithFarmer" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="SelefiWithFarmer" ErrorMessage="Selfie With Framer Image Required"></asp:RequiredFieldValidator>
                     <cst:FileSizeValidator runat="server" id="FileSizeSelefiWithFarmer" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="SelefiWithFarmer" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>  
                    <cst:FileTypeValidator ID="SelefiWithFarmerFileTypeValidator" runat="server"
                        ControlToValidate="SelefiWithFarmer"
                        ForeColor="Red" Display="Dynamic"
                        ErrorMessage="Allowed file types are {0}.">
                    </cst:FileTypeValidator></td>
                  </tr>
            <tr > 
                 <td colspan="4" >
                    <br />
                    35.Remarks : 
                    <br />
                    <asp:TextBox ID="txtremarks" runat="server" TextMode="MultiLine" Width="90%" Height="100px"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:CheckBoxList ID="chkSuggestion" runat="server" RepeatDirection="Vertical"></asp:CheckBoxList>
                </td>
            </tr>
        </table>
        <br />
    </div>
    <br />
    <div>
        <div align="center">
            <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                ID="Save" Text="Submit" OnClick="Save_Click" />

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server"> <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
    </script>
</asp:Content>
