﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Form2DetailsApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.Form2DetailsApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>--%>
    <div align="center" class="container">
        <div class="MainHeading">
            <asp:Label runat="server" ID="lblHeading" Text="Seed Certificate ( Form-II )"> </asp:Label>
           
        </div>
        <br />
        <div>
            <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
            <asp:Label runat="server" ID="SSCID"> </asp:Label>
        </div>
 
        <br />
        <div id="divReport" runat="server" visible="false">
            <table class="table-condensed" width="100%" border="1">
                <tr class="Note">
                    <td colspan="4">
                        <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                    </td>
                </tr>
                <tr class="SideHeading">
                     <asp:HiddenField ID="Id" runat="server" />
                    <td colspan="4">Certificate in FORM-II   &nbsp; &nbsp; 
                         <%-- <asp:LinkButton runat="server" CssClass="text-success" ID="LinkButton1"  PostBackUrl='<%# "~/Reports/Form2Certificate.aspx?Id="+Eval("SSCId") %>'>View Certificate</asp:LinkButton>
                          
                          --%>
                        <asp:LinkButton ID="lnkCertificate" runat="server" Text="View Certificate" CssClass="text-success" OnClick="form2Certificate_Click" Visible="false" CausesValidation="false"></asp:LinkButton>                 
                    </td>
                </tr>

                <tr>
                    <td>1.Name of the seed producer
                    </td>
                    <td colspan="3">
                        <asp:Label ID="lblproducerName" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>2.Name & Address of seed grower:
                    </td>
                    <td colspan="3">
                        <asp:Label ID="lblGrowerName" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="25%">3.Crop</td>
                    <td width="25%">
                        <asp:Label ID="lblCrop" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td width="25%">4.Variety</td>
                    <td width="25%">
                        <asp:Label ID="lblVariety" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>5.Class of Seed</td>
                    <td>
                        <asp:Label ID="lblclass" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>6.Lot No.</td>
                    <td>
                        <asp:Label ID="lblLotno" runat="server" Font-Bold="true"></asp:Label></td>
                </tr>
                <tr>
                    <td>7.Quantity of Seed(in Kgs)</td>
                    <td>
                        <asp:Label ID="lblquantity" runat="server" Font-Bold="true"></asp:Label></td>
                    <td>
                        Packing Size
                    </td>
                    <td>
                        <asp:Label ID="lblPackingSize" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>8.Form-I Registration No:
                    </td>
                    <td>
                        <asp:Label ID="lblForm1NO" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>Form-II Registration No:
                    </td>
                    <td>
                        <asp:Label ID="lblForm2NO" runat="server" Font-Bold="true"></asp:Label>
                    </td> 
                </tr>
                <tr>
                    <td>9.Lab Report No</td>
                    <td>
                        <asp:Label ID="lblTestNo" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>10.Genetic Purity test report No
                    </td>
                    <td>
                        <asp:Label ID="lblGeneticPurity" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">11. Physical Purity
                    </td>
                </tr>
                <tr>
                    <td>a.Pure Seed(%)</td>
                    <td>
                        <asp:Label ID="lblpureseed" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>b.Inert Matter(%)</td>
                    <td>
                        <asp:Label ID="lblInert" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>c.Other Crop Seeds (%)</td>
                    <td>
                        <asp:Label ID="lblOtherCrop" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>d.Huskless Seed(%)</td>
                    <td>
                        <asp:Label ID="lblHuskless" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>e.ODV(%)</td>
                    <td>
                        <asp:Label ID="lblodv" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                    <td>f.Total Weed Seeds(%)</td>
                    <td>
                        <asp:Label ID="lbltotalweed" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>g.Objectionable weed Seeds(%)</td>
                    <td colspan="3">
                        <asp:Label ID="lblobjectionable" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td>12.Germination
                    </td>
                    <td>
                        <asp:Label ID="lblgermination" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td>a.Hard Seed(%)
                    </td>
                    <td>
                        <asp:Label ID="lblHardSeed" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>b.Fresh ungerminated seed(%)</td>
                    <td>
                        <asp:Label ID="lblFreshungerminated" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td>13.Seed Moisture(%)</td>
                    <td>
                        <asp:Label ID="lblSeedMoisture" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>14.Dead Seeds(%)</td>
                    <td>
                        <asp:Label ID="lblDiseases" runat="server" Font-Bold="true"></asp:Label>
                    </td>

                    <td>15.Insect Damage(%)</td>
                    <td>
                        <asp:Label ID="lblDamage" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblreleaseOrder" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                     <td>16.Certificate valid upto</td>
                    <td colspan="3"> 
                        <asp:Label ID="lblvalidity" runat="server" Font-Bold="true"></asp:Label></td>
                </tr>
                
                <tr>
                    <td>17.Total No. of tags issued with Sl.No</td>
                    <td> 
                        <asp:Label ID="lbltotaltags" runat="server" Font-Bold="true"></asp:Label></td>
                       <td>18.Total No. of tags used with Sl.No</td>
                    <td colspan="3"> 
                        <asp:Label ID="lblusedtags" runat="server" Font-Bold="true"></asp:Label></td>
                </tr>
                <tr>
                 
                </tr>
                <tr>
                    <td>19.Total No. of tags unused with Sl.No</td>
                    <td> 
                        <asp:Label ID="lblunusedtags" runat="server" Font-Bold="true"></asp:Label></td>
                     <td>20.Certificate Issued By</td>
                    <td>
                        <asp:Label ID="lblIssuedBy" runat="server" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                   
                    <td>21.Certificate Issued Date
                    </td>
                    <td>
                        <asp:Label ID="lblIssuedDate" runat="server" Font-Bold="true"></asp:Label></td>
                     <td valign="top">Form II Certificate Upload<span style="color: red">*</span>
                           </td>
                   <td>        
                        <asp:FileUpload runat="server" CssClass="form-control" ID="FormIIPhoto" accept="image/*" Visible="false"/>

                            <asp:RequiredFieldValidator runat="server" ID="RequiredFormIIPhoto" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FormIIPhoto" ErrorMessage="Please Upload Form II Scanned Copy"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" id="FormIIPhotoValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FormIIPhoto" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" id="FormIIPhotoTypeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="FormIIPhoto" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                       <asp:Literal runat="server" ID="LitFormIIPhoto" EnableViewState="false"></asp:Literal>
                  </td>  
                </tr>
                
            </table>
            <br />
            <div>
                <div align="center">
                 
                    <asp:Button runat="server" CssClass="btn-primary"
                        ID="Upload" Text="Upload" OnClick="Upload_Click" Visible="false" />
                </div>
            </div>
        </div>
        <br />
    </div>
    <%-- </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>

