﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FormOneCreate.aspx.cs"  
    Inherits="KSSOCA.Web.Transactions.FormOneCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href='<%= ResolveUrl("~/css/registration.css")%>' rel="stylesheet" />
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    <asp:HiddenField ID="hdnSourceVerificationID" runat="server" />
                    <asp:HiddenField ID="hdnLotno" runat="server" />
                    <asp:HiddenField ID="hdnGOTRequiredOrNot" runat="server" />
                    <asp:HiddenField ID="hdnDistributionEligibility" runat="server" />
                    <asp:HiddenField ID="hdnF1ID" runat="server" />
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                    &nbsp;&nbsp;<asp:Button ID="btnViewApplication" CssClass="btn-primary" runat="server" Text="View Application" Display="Dynamic" Visible="false" CausesValidation="false" />
                    <asp:HiddenField ID="hdnSeedRate" runat="server" />
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr>
                        <td>Select Farmer/Grower</td>
                        <td>
                            <asp:DropDownList ID="ddlFarmers" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlFarmers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlFarmers" ErrorMessage="Farmer Required" InitialValue="0"></asp:RequiredFieldValidator></td>
                        <td colspan="2">
                            <asp:LinkButton ID="lnkCreateNew" runat="server" ForeColor="Navy" Font-Bold="true" BorderStyle="Ridge" CausesValidation="false" PostBackUrl="~/Masters/AddFarmer.aspx">Register New Farmer/Grower Here</asp:LinkButton></td>
                    </tr>
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Label ID="lblFarmerDetails" runat="server"></asp:Label>
                            <br />
                            <asp:Label ID="lblCropDetails" runat="server"></asp:Label>

                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Seed & Seed plot Information </td>
                    </tr>
                    <tr>
                        <td width="25%">1.District</td>
                        <td width="25%">
                            <asp:DropDownList runat="server" class="form-control" ID="District" AutoPostBack="true" OnSelectedIndexChanged="District_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredDistrict" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="District" ErrorMessage="District Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                        <td width="25%">2.Taluk</td>
                        <td width="25%">
                            <asp:DropDownList runat="server" class="form-control" ID="ddlPlotTaluk" AutoPostBack="true" OnSelectedIndexChanged="ddlPlotTaluk_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredddlPlotTaluk" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlPlotTaluk" ErrorMessage="Plot Taluk Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>3.Hobli<span style="color: red">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlPlotHobli" AutoPostBack="true" OnSelectedIndexChanged="ddlPlotHobli_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredddlPlotHobli" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlPlotHobli" ErrorMessage="Plot Hobli Required" InitialValue="0"></asp:RequiredFieldValidator>

                        </td>

                        <td>4.Village<span style="color: red">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlPlotVillage">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredddlPlotVillage" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlPlotVillage" ErrorMessage="Plot Village Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>5.Gram panchayat<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="LocationOfTheSeedPlotGramPanchayat" placeholder="Location of the Seed Plot Gram Panchayat" />
                            <asp:RequiredFieldValidator runat="server" ID="LocationOfTheSeedPlotGramPanchayatRequiredValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="LocationOfTheSeedPlotGramPanchayat" ErrorMessage="Location of the Seed Plot Gram Panchayat can't be empty"></asp:RequiredFieldValidator>
                        </td>

                        <td>6.Survey no.<span style="color: red">*</span>
                            <asp:TextBox runat="server" CssClass="form-control" ID="LocationOfTheSeedPlotSurveyNumber" onkeypress="return CheckNumber(event);" placeholder="Location of the Seed Plot Survey Number" />
                            <asp:RequiredFieldValidator runat="server" ID="LocationOfTheSeedPlotSurveyNumberRequiredValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="LocationOfTheSeedPlotSurveyNumber" ErrorMessage="Location of the Seed Plot Survey Number can't be empty"></asp:RequiredFieldValidator>
                        </td> 
                        <td>Hissa No. <span style="color: red">*</span><asp:TextBox runat="server" CssClass="form-control" ID="txtHissaNo"   placeholder="Hissa Number" />
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtHissaNo" ErrorMessage="Hissa Number"></asp:RequiredFieldValidator>
                      </td>
                    </tr>
                    <tr>
                        <td>7.Class of seed offered for certification<span style="color: red">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlClass">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredddlClass" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddlClass" ErrorMessage="Class Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table>
                                <tr>
                                    <td>8.Select Lot No.<span style="color: red">*</span></td>
                                    <td>
                                        <asp:DropDownList ID="ddllots" runat="server" AutoPostBack="true" class="form-control" OnSelectedIndexChanged="ddllots_SelectedIndexChanged"></asp:DropDownList>
                                     <asp:RequiredFieldValidator runat="server" ID="lotnumerRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ddllots" ErrorMessage="Lot Number Required" InitialValue="0"></asp:RequiredFieldValidator>
                                    </td>
                                    <td>9.Quantiy of Seed to be distributed(in Kg)<span style="color: red">*</span></td>
                                    <td>
                                        <asp:TextBox runat="server" class="form-control" ID="QuantityOfSeed" placeholder="Quantiy of Seed (in Kg)" onkeypress="return CheckDecimal(event);" AutoPostBack="true" OnTextChanged="QuantityOfSeed_TextChanged" InitialValue="0.0"/>
                                        <asp:RequiredFieldValidator runat="server" ID="QuantityOfSeedRequiredField" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="QuantityOfSeed" ErrorMessage="Quantiy of Seed to be distributed Required" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ValidationGroup="v1" runat="server" ErrorMessage="Enter valid number" ForeColor="Red" Display="Dynamic" ControlToValidate="QuantityOfSeed"
                                            ValidationExpression="^[1-9]\d*(\.\d+)?$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>10.Tag No.</td>
                                    <td>
                                        <asp:TextBox runat="server" class="form-control" ID="txtTagNo" placeholder="Tag No"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnadd" ValidationGroup="v1" runat="server" Text="Add" CssClass="btn-primary" OnClick="btnadd_Click" OnClientClick="javascript:scroll(0,0);" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">Quantity verified/left for the selected lot no
                            <asp:Label ID="lblquantityleft" runat="server" Font-Bold="true"></asp:Label>Kg.
                            <asp:GridView ID="gvLotNos" runat="server" class="table-condensed" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> . 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="LotNumber" HeaderText="Lot Number" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity Of Seed(in Kg.)" />
                                    <asp:BoundField DataField="TagNo" HeaderText="Tag No" />
                                </Columns>
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdelete" runat="server" Text="delete" CausesValidation="false" CommandArgument='<%#Eval("TagNo") %>' OnClick="lnkdelete_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                                <FooterStyle Font-Bold="true" />
                            </asp:GridView>
                            <br />
                            Total Quantity :
                                            <asp:Label runat="server" ID="lblTotal" Font-Bold="true" Text="0"></asp:Label>
                            Kg.
                            Seed Rate :
                            <asp:Label runat="server" ID="lblSeedRate" Font-Bold="true" Text="0"></asp:Label>
                            Kg/Acre.
                        </td>
                    </tr>
                    <tr>
                        <td>11.Area offered for certification (in acres)<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="AreaOffered" placeholder="Area offered in (acres)" TextMode="Number" ReadOnly="true" onkeydown="return false" onkeypress="return CheckDecimal(event);" AutoPostBack="true" OnTextChanged="AreaOffered_TextChanged" />
                            <asp:RequiredFieldValidator runat="server" ID="AreaOfferedRequiredValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="AreaOffered" ErrorMessage="Area offered in (acres) can't be empty"></asp:RequiredFieldValidator>
                        </td>

                        <td>12.Quantity of seed distributed ( in Kgs.)</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="Seeddistributed" placeholder="Seed distributed" ReadOnly="true" onkeydown="return false" />
                        </td>
                    </tr>
                    <tr>
                        <td>13.Isolation distance north to south(in meters)</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="IsolcationDistanceNorthToSouth" placeholder="Isolcation Distance North to South" onkeypress="return CheckNumber(event);" />
                            <%--<asp:RequiredFieldValidator runat="server" ID="IsolcationDistanceNorthToSouthRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="IsolcationDistanceNorthToSouth" ErrorMessage="Isolcation Distance North to South can't be empty"></asp:RequiredFieldValidator>--%>
                        </td>

                        <td>14.Isolation distance east to west(in meters)</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="IsolcationDistanceEastToWest" placeholder="Isolcation Distance East to West" onkeypress="return CheckNumber(event);" />
                            <%--<asp:RequiredFieldValidator runat="server" ID="IsolcationDistanceEastToWestRequiredValidator" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="IsolcationDistanceEastToWest" ErrorMessage="Isolcation Distance East to West FIR can't be empty"></asp:RequiredFieldValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>15.Name of organiser / sub contractor</td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="NameOfTheOrganiserOrSubContrator" placeholder="Name of the Organiser / Sub Contrator" />

                        </td>
                        <td>16.Season<span class="text-danger"></span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="Season">
                            </asp:DropDownList><asp:RequiredFieldValidator runat="server" ID="RequiredSeason" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="Season" ErrorMessage="Season Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>17.Probable date of sowing/transplanting<span style="color: red">*</span></td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="ActualDateOfSowingOrTransplanting" placeholder="Date" onkeydown="return false" />
                            <asp:RequiredFieldValidator runat="server" ID="ActualDateOfSowingOrTransplantingRequiredValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="ActualDateOfSowingOrTransplanting" ErrorMessage="Probable Date of Sowing / Transplanting can't be empty"></asp:RequiredFieldValidator>
                        </td>
                        <td>18.Seed processing unit<span class="text-danger">*</span></td>
                        <td>
                            <asp:DropDownList runat="server" class="form-control" ID="SeedProcessingUnit">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredSeedProcessingUnit" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="SeedProcessingUnit" ErrorMessage="Seed Processing Unit Required" InitialValue="0"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>OTP Verification<span style="color: red">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" CssClass="form-control" ID="otpFromFarmer" MaxLength="4" placeholder="OTP" onkeypress="return CheckNumber(event);"/>
                             <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidatorOtpFromFarmer" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="otpFromFarmer" ErrorMessage="Please Verifty the OTP First"></asp:RequiredFieldValidator>
                         <asp:Label runat="server" ID="lblotpMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                            
                        </td>
                        <td colspan="2">
                            
                         
                            <asp:Button ID="btnSendOtp" runat="server" Text="Send OTP" CssClass="btn-primary" OnClick="btnSendOtp_Click" OnClientClick="javascript:scroll(0,0);" CausesValidation="false" />
                            <asp:Button ID="btnResendOtp" runat="server" Text="Re-Send OTP" CssClass="btn-primary" OnClick="ReSendOtp_Click" OnClientClick="javascript:scroll(0,0);" CausesValidation="false" Visible="false"/>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2">19.Purchase Bill<span style="color: red">*</span>
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_PurchaseBill" accept="image/*" /> 
                            <asp:RequiredFieldValidator runat="server" ID="RequiredPurchaseBill" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_PurchaseBill" ErrorMessage="Please Upload Purchase Bill"></asp:RequiredFieldValidator>
                            <cst:FileSizeValidator runat="server" ID="PurchaseBillSizeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_PurchaseBill" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" ID="PurchaseBillTypeValidator" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_PurchaseBill" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitPurchaseBill" EnableViewState="false"></asp:Literal>
                        </td>
                          <td valign="top" colspan="2">20.Vamshavruksha(In case of Death)
                            <asp:FileUpload runat="server" CssClass="form-control" ID="fu_vamshavruksha" accept="image/*" /> 
                            <cst:FileSizeValidator runat="server" ID="FileSizeValidatorVamshavruksha" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_vamshavruksha" ErrorMessage="File size must not exceed {0} KB">
                            </cst:FileSizeValidator>
                            <cst:FileTypeValidator runat="server" ID="FileTypeValidatorVamshavruksha" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="fu_vamshavruksha" ErrorMessage="Allowed file types are {0}.">
                            </cst:FileTypeValidator>
                            <asp:Literal runat="server" ID="LitVamshavruksha" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                </table>
 
                <br />
            </div>
            <br />
            <div>
                <div align="center">
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="btnUpdate" Text="Re-Save" OnClick="btnUpdate_Click" Visible="false" />
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="Save" Text="Save" OnClick="Save_Click" />
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpdate" />
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
     
    </script>
</asp:Content>
