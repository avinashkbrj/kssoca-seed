﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="FormOneView.aspx.cs" Inherits="KSSOCA.Web.Transactions.FormOneView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <!-- this meta viewport is required for BOLT //-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <!-- BOLT Sandbox/test //-->
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <!-- BOLT Production/Live //-->
    <!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center" style="min-height: 600px;">
        <asp:UpdatePanel ID="upnl" runat="server">
            <ContentTemplate>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                     <input type="hidden" id="surl" name="surl" value="<%= Session["surl"]%>" />
                    <input type="hidden" id="txnid" name="txnid" value="<%= Session["txnid"]%>" />
                    <input type="hidden" id="hash" name="hash" value="<%= Convert.ToString(Session["hash"])%>" />
                    <input type="hidden" id="amount" name="amount" value="<%= Session["amount"]%>" />
                    <input type="hidden" id="fname" name="fname" value="<%= Session["fname"]%>" />
                    <input type="hidden" id="mobilenumber" name="mobilenumber" value="<%= Session["mobilenumber"]%>" />
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">

                    <tr class="Note">
                        <td colspan="2">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                            <br />

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button runat="server" ID="Add" class="btn-primary" OnClick="Add_Click" Text="Add New Form-I" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gvPayment" runat="server" class="table-condensed" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> . 
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="Form1ID" HeaderText="Form-I Registration No" />
                                    <asp:BoundField DataField="RegFees" HeaderText="Reg Fees(in Rs.)" />
                                    <asp:BoundField DataField="InspectionCharge" HeaderText="Inspection Charge(in Rs.)" />
                                    <asp:BoundField DataField="STLCharge" HeaderText="STL Charge(in Rs.)" />
                                    <asp:BoundField DataField="GPTCharge" HeaderText="GPT Charge(in Rs.)" />
                                    <asp:BoundField DataField="OtherCharge" HeaderText="Other Charge(in Rs.)" />
                                    <asp:BoundField DataField="Total" HeaderText="Total(in Rs.)" />
                                </Columns>
                                <EmptyDataTemplate>----------No forms selected-----------</EmptyDataTemplate>
                                <FooterStyle Font-Bold="true" />
                            </asp:GridView>
                        </td>
                        <td>
                            <table class="table-condensed" border="1">
                                <asp:Panel ID="pnlPayment" runat="server" Visible="false">
                                    <tr>
                                        <td colspan="3">Total Amount to be paid (in Rs.) :
                                            <asp:Label runat="server" ID="lblTotal" Font-Bold="true" Text="0"></asp:Label>&nbsp;&nbsp;Payment Mode :
                                            <asp:RadioButtonList ID="rbPaymentMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                OnSelectedIndexChanged="rbPaymentMode_SelectedIndexChanged">
                                                <asp:ListItem Text="&nbsp;DD/Cheque&nbsp;&nbsp;" Value="C" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="&nbsp;e-Payment" Value="O"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <asp:Panel ID="pnlCheck" runat="server">
                                        <tr>
                                            <td>Cheque/DD Number  :
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtCheckNo" placeholder="Cheque/DD Number" TextMode="Number"
                                                    onkeypress="return CheckNumber(event);" />
                                                <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredCheckNo" ForeColor="Red" Display="Dynamic"
                                                    ControlToValidate="txtCheckNo" ErrorMessage="Cheque/DD Number required"></asp:RequiredFieldValidator>
                                            </td>
                                             <td>IFSC Code  :
                                                <asp:TextBox runat="server"  ID="txtIFSCCode" placeholder="IFSC Code" CssClass="form-control"
                                                     />
                                                <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="RequiredIFSCCode" ForeColor="Red" Display="Dynamic"
                                                    ControlToValidate="txtIFSCCode" ErrorMessage="IFSC Code required"></asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator runat="server" ValidationGroup="v1" ID="IFSCNumberRegExValidator" CssClass="text-danger" Display="Dynamic" ForeColor="Red"
                                                 ValidationExpression="^[A-Z|a-z]{4}[0][a-zA-Z0-9]{6}$" ControlToValidate="txtIFSCCode"
                                                  ErrorMessage="Please Enter valid IFSC Code"></asp:RegularExpressionValidator>
                                            </td>
                                             <td colspan="2">Date of Cheque  :
                                                  
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtDateofCheque" placeholder="Date" onkeydown="return false" />
                                                <asp:RequiredFieldValidator ValidationGroup="v1" runat="server" ID="DateofChequeRequiredValidator" ForeColor="Red" Display="Dynamic"
                                                    ControlToValidate="txtDateofCheque" ErrorMessage="Cheque Date required"></asp:RequiredFieldValidator>
                        
                                            </td> 
                                            
                                            <td>
                                                <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1"
                                                    ID="btnCheck" Text="Save Payment Details" OnClick="btnCheck_Click" />
                                            </td>
                                        </tr>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlOnline" runat="server">
                                        <tr>
                                            <td align="center" colspan="5">
                                                <input type="submit" CssClass="btn-primary"   value="Pay Online" onclick="launchBOLT(); return false;" /> 
                             
                                               <%-- <asp:Button runat="server" CssClass="btn-primary" ValidationGroup="v1" CausesValidation="false"
                                                    ID="btnPayOnline" Text="Pay Online" OnClick="btnPayOnline_Click" /></td>--%>
                                        </tr>
                                    </asp:Panel>
                                </asp:Panel>
                            </table>
                        </td>
                    </tr>
                </table>
                 
                <br />
                <asp:GridView runat="server" ID="FormOneGridView"
                    AutoGenerateColumns="false" class="table-condensed" Width="100%"
                    AllowPaging="true" PageSize="25" OnRowDataBound="FormOneGridView_RowDataBound" OnPageIndexChanging="FormOneGridView_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> .
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="Form-I Registration No." />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="SourceVarification_Id" HeaderText="Source Verification No." />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Name of the Farmer" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="MobileNumber" HeaderText="Mobile No" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="District" HeaderText="District" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="Taluk" HeaderText="Taluk" />
                    </Columns>
                    <Columns>
                        <asp:BoundField DataField="RegistrationDate" HeaderText="Registration Date" />
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Service Charge Details">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnPaymentStatus" runat="server" Value='<%# Eval("PaymentStatus") %>' />
                                <asp:CheckBox ID="chkPayment" runat="server" Enabled="false" OnCheckedChanged="chkPayment_CheckedChanged" AutoPostBack="true" />
                                <asp:Label runat="server" ID="lblAmountPaid" Font-Bold="true" Text='<%# Eval("AmountPaid") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns>
                        <asp:TemplateField HeaderText="Application Status">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <Columns> 
                        <asp:TemplateField>
                            <ItemTemplate>

                                <asp:LinkButton ID="lnkview" runat="server" Text="view" ForeColor="Navy" Font-Bold="true" PostBackUrl='<%# "~/Transactions/FormOneApproval.aspx?Id=" + Eval("Id") %>'></asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lnkFieldInspection" PostBackUrl='<%# "~/Transactions/FieldInspectionView.aspx?Id=" + Eval("Id") %>'>Field Inspection Details</asp:LinkButton>
                                |
                                <asp:LinkButton runat="server" ID="lnkDelete" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete?');" Visible="false">Delete</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnCheck" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.10.0.min.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/jquery-ui.min.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.9.2/themes/blitzer/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
      
        //On Page Load.
        $(function () {
            SetDatePicker();
        });

        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetDatePicker();
                }
            });
        };
        function SetDatePicker() {
            $("[placeholder=Date]").datepicker({
                dateFormat: 'dd-mm-yy',
                showOn: 'button',
                buttonImageOnly: true,
                buttonImage: '../img/cal.png'
            });

        }
    </script>
 <script type="text/javascript">
     $(document).ready(function () {
        
         

     });
    
    //--
</script>	

<script type="text/javascript"><!--
    function launchBOLT() {
      //  var encryptedk = CryptoJS.AES.encrypt("juvhC4rb", "Secret Passphrase");
        var getHash = createHash();
        var actualHashValue = getHash['responseJSON']['success'];
      
        var encryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var encryptedKey =  '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var decryptedSalt = CryptoJS.AES.decrypt(encryptedSalt, "Secret Passphrase");
        var decryptedKey = CryptoJS.AES.decrypt(encryptedKey, "Secret Passphrase");

        var transId = $('#txnid').val();
        

        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
         
        bolt.launch({
            key: decryptedKey.toString(CryptoJS.enc.Utf8),
            txnid: transId,
            hash: actualHashValue.toString(),
            amount: amt,
            firstname: fname,
            email: "avi.ksrj@gmail.com",
            phone: mobilenumber,
            productinfo: "KSSOCA Payment",
            udf5: "BOLT_KIT_ASP.NET",
            surl: $('#surl').val(),
            furl: $('#surl').val()
        }, {
            responseHandler: function (BOLT) {
                console.log(BOLT.response.txnStatus);
                if (BOLT.response.txnStatus != 'CANCEL') {
                    //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                    var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                        '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                        '<input type=\"hidden\" name=\"salt\" value=\"' + decryptedSalt.toString(CryptoJS.enc.Utf8) + '\" />' +
                        '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                        '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                        '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                        '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                        '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                        '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                        '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                        '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                        '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                        '</form>';
                    var form = jQuery(fr);
                    jQuery('body').append(form);
                    form.submit();
                }
            },
                catchException: function (BOLT) {
                    alert(BOLT.message);
                }
            });
    }

    function createHash() {
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        var transId = $('#txnid').val();
        var hashEncryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var hashEncryptedKey = '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var hashDecryptedSalt = CryptoJS.AES.decrypt(hashEncryptedSalt, "Secret Passphrase");
        var hashDecryptedKey = CryptoJS.AES.decrypt(hashEncryptedKey, "Secret Passphrase");
        var temp = "";
        return $.ajax({
            url: '/Transactions/Hash.aspx',
            type: 'post',
            async: false,
            data: JSON.stringify({
                key: hashDecryptedKey.toString(CryptoJS.enc.Utf8),
                salt: hashDecryptedSalt.toString(CryptoJS.enc.Utf8),
                txnid: transId.toString(),
                amount: amt,
                pinfo: "KSSOCA Payment",
                fname: fname,
                email: "avi.ksrj@gmail.com",
                mobile: mobilenumber,
                udf5: "BOLT_KIT_ASP.NET"
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (json) {

                if (json['error']) {
                    $('#alertinfo').html('<i class="fa fa-info-circle"></i>' + json['error']);
                }
                else if (json['success']) {
                    $('#hash').val(json['success']);
                    temp = json['success'];
                }
            }
        });
        return temp; 
    }


</script>	
</asp:Content>
