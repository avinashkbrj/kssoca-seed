﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneticPurityReportCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.GeneticPurityReportCreate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Genetic Purity Report"> </asp:Label>
                </div>
                <br />
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
              
                <br />
                <div id="divReport" runat="server" visible="true">
                    <table class="table-condensed" width="100%" border="1">
                        <tr class="Note">
                            <td colspan="4">
                                <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>

                            </td>
                        </tr>
                        <tr class="SideHeading">
                            <td colspan="4">Genetic Purity Report                      
                            </td>
                        </tr>
                        <tr>
                            <td> Lab Test No</td>
                            <td>
                                <asp:TextBox ID="txttestno" runat="server" CssClass="form-control" onkeypress="return CheckNumber(event);"></asp:TextBox>
                            </td>
                            <td>Genetic Purity
                            </td>
                            <td>
                                <asp:TextBox ID="txtGenetic" runat="server" CssClass="form-control" onkeypress="return CheckDecimal(event);" Text="0"></asp:TextBox>

                            </td> 
                        </tr>  
                        <tr>
                           
                            <td>Result
                            </td>
                            <td>
                                <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbResult" runat="server">
                                    <asp:ListItem Value="1" class="radio-inline" Text="Pass"></asp:ListItem>
                                    <asp:ListItem Value="0" class="radio-inline" Text="Fail" Selected="true"></asp:ListItem>
                                </asp:RadioButtonList>

                            </td>
                            <td>Remarks</td>
                            <td>
                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control"></asp:TextBox>

                            </td>
                        </tr>
                        <tr> 
                            <td>Prepared By</td>
                            <td>
                                <asp:TextBox ID="txtPreparedBy" runat="server" CssClass="form-control"></asp:TextBox> 
                            </td>
                            <td>Number of labels required<span style="color: red">*</span></td>
                            <td>
                                <asp:TextBox ID="txtNumberOfLabelsRequired" runat="server" CssClass="form-control" onkeypress="return CheckDecimal(event);"></asp:TextBox>
                                   <asp:RequiredFieldValidator runat="server" ID="txtNumberOfLabelsRequiredRequired" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtNumberOfLabelsRequired" ErrorMessage="Number of labels required"></asp:RequiredFieldValidator>

                            </td>
                        </tr>
                       
                        <tr>
                            <td>Result Given By</td>
                            <td>
                                <asp:Label ID="lblCheckedBy" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                            <td>Date

                            </td>
                            <td>
                                <asp:Label ID="lblCheckedDate" runat="server" Font-Bold="true"></asp:Label></td>
                        </tr>
                    </table>
                    <br />
                    <div>
                        <div align="center">
                            <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                                ID="Save" Text="Save" OnClick="Save_Click" />
                        </div>
                    </div>
                </div>
                <br />
                <%--<div id="divGridview" runat="server">
                    <asp:GridView ID="gvReports" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="true" Visible="false">
                        <Columns>
                            <asp:TemplateField HeaderText="Sl.No.">
                                <ItemTemplate>
                                    <%#Container.DataItemIndex+1 %> . 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                    </asp:GridView>
                </div>--%>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
