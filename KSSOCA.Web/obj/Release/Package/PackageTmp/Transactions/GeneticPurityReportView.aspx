﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeneticPurityReportView.aspx.cs" Inherits="KSSOCA.Web.Transactions.GeneticPurityReportView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" style="min-height: 600px;">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading" Text="Genetic Purity Report"> </asp:Label>
                </div>
               
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <div align="left" id="divDecoding" runat="server" style="width: 100%" visible="false">
                    <asp:TextBox runat="server" ID="txtsscNo" placeholder=" Enter Seed Sampling Coupon No." onkeypress="return CheckNumber(event);" />&nbsp;&nbsp;&nbsp;Select GOT 
                     <asp:DropDownList runat="server" ID="ddlGOT" AutoPostBack="true">
                     </asp:DropDownList>&nbsp;&nbsp;&nbsp;
                    <asp:Button runat="server" ID="btngenerateLabNo" class="btn-primary" Text="Generate GOT Test No" OnClick="btngenerateLabNo_Click" />
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td>
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                            <div id="divGridview" runat="server" visible="true">
                                <asp:GridView runat="server" ID="DecodingGridView"
                                    AutoGenerateColumns="false" class="table-condensed" Width="100%"
                                    AllowPaging="true" PageSize="25" OnRowDataBound="DecodingGridView_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl.No.">
                                            <ItemTemplate>
                                                <%#Container.DataItemIndex+1 %> .
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="SSC_ID" HeaderText="Seed Sampling Coupon No." />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="LabTestNo" HeaderText="Test No." />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="CROPNAME" HeaderText="Crop Name" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="VARIETYNAME" HeaderText="Variety Name" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="Class" HeaderText="Seed Class" />
                                    </Columns>
                                    <Columns>
                                        <asp:BoundField DataField="SampledRecievedDate" HeaderText="Sample Recieved Date" />
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Tests Required">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnTestsRequired" runat="server" Value='<%# Eval("TestsRequired") %>' />
                                                <asp:Label runat="server" ID="lblTestsRequired" Font-Bold="true"> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField HeaderText="GOT Status">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("TESTRESULT") %>' />
                                                <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="lnkCreate" PostBackUrl='<%# "~/Transactions/GeneticPurityReportCreate.aspx?Id=" + Eval("LabTestNo") %>'>Enter Test Result </asp:LinkButton>
                                                |
                                                <asp:LinkButton runat="server" ID="lnkView" PostBackUrl='<%# "~/Transactions/GeneticPurityReportApproval.aspx?Id=" + Eval("SSC_ID") %>'>View Result</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
