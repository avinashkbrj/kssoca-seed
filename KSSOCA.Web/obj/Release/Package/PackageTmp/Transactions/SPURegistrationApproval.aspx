﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SPURegistrationApproval.aspx.cs" Inherits="KSSOCA.Web.Transactions.SPURegistrationApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <!-- this meta viewport is required for BOLT //-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    <!-- BOLT Sandbox/test //-->
    <script id="bolt" src="https://sboxcheckout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script>
    <!-- BOLT Production/Live //-->
    <!--// script id="bolt" src="https://checkout-static.citruspay.com/bolt/run/bolt.min.js" bolt-color="e34524" bolt-logo="http://boltiswatching.com/wp-content/uploads/2015/09/Bolt-Logo-e14421724859591.png"></script //-->
       <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js" integrity="sha256-/H4YS+7aYb9kJ5OKhFYPUjSJdrtV6AeyJOtTkw6X72o=" crossorigin="anonymous"></script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" id="DivMain" runat="server" class=" container">
                <br />
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    <input type="hidden" id="surl" name="surl" value="<%= Session["surl"]%>" />
                    <input type="hidden" id="txnid" name="txnid" value="<%= Session["txnid"]%>" />
                    <input type="hidden" id="hash" name="hash" value="<%= Convert.ToString(Session["hash"])%>" />
                    <input type="hidden" id="amount" name="amount" value="<%= Session["amount"]%>" />
                    <input type="hidden" id="fname" name="fname" value="<%= Session["fname"]%>" />
                    <input type="hidden" id="mobilenumber" name="mobilenumber" value="<%= Session["mobilenumber"]%>" />
                    <input type="hidden" id="email" name="emailid" value="<%= Session["emailid"]%>" />
                </div>
                <br />
                <div>
                    <asp:Label Font-Bold="true" runat="server" ID="lblMessage" ForeColor="Red"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                    <asp:LinkButton ID="lnkCertificate" runat="server" Text="View Certificate"  OnClick="lnkCertificate_Click" Visible="false"></asp:LinkButton>
                </div>
                
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">1.Basic Details
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">1.Registration Number </td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="RegistrationNumber"></asp:Label>
                        </td>
                        <td width="25%">2.Name of Firm/Institution </td>
                        <td width="25%">
                            <asp:Label Font-Bold="true" runat="server" ID="ProducerName" />
                        </td>
                    </tr>
                    <tr>
                        <td>3.Name of the SPU  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Name" />
                        </td>

                        <td>4.Mobile Number </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="MobileNumber" /></td>
                    </tr>
                    <tr>
                        <td>5.SPU No  </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="lblSPUNo" /><asp:TextBox runat="server" class="form-control" ID="txtSPUNO" MaxLength="15" Visible="false" placeholder="SPU No" TextMode="Number" onkeypress="return CheckNumber(event);" />
                              
                            <asp:RegularExpressionValidator runat="server" ID="txtSPUNORegEx" ForeColor="Red" Display="Dynamic"
                                ValidationExpression="^[0-9]{1,20}$" ControlToValidate="txtSPUNO"
                                ErrorMessage="Please enter valid  number"></asp:RegularExpressionValidator>
                           
                              <asp:Label Font-Bold="true" runat="server" ID="msgtxtSPUNO" ForeColor="Red"> </asp:Label> 
                        </td>
                        <td>6.District </td>
                        <td>
                            <asp:DropDownList runat="server" ID="District" AutoPostBack="true" Visible="false">
                            </asp:DropDownList>
                            <asp:Label Font-Bold="true" runat="server" ID="lblDistrict" />
                        </td>
                    </tr>
                    <tr>
                        <td>7.Taluk </td>
                        <td>
                            <asp:DropDownList runat="server" ID="Taluk" Visible="false">
                            </asp:DropDownList>
                            <asp:Label Font-Bold="true" runat="server" ID="lblTaluk" /></td>

                        <td>8.Address  
                        </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="Address" /></td>
                    </tr>
                    <tr>
                        <td>9.Aadhaar Number </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="AadharNumber" /></td>


                        <td>10.Last renewal certificate no </td>
                        <td>
                            <asp:Label Font-Bold="true" runat="server" ID="OldRegistrationNumber" /></td>
                    </tr>
                    <tr>
                        <td>11.Registration Date  
                        </td>
                        <td colspan="3">
                            <asp:Label Font-Bold="true" runat="server" ID="RegistrationDate"></asp:Label></td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">2.Particulars of Seed Processing Machineries installed in the Seed Processing Unit.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:GridView ID="gvParticulars" runat="server" class="table-condensed" Width="100%" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .                                         
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                    <asp:BoundField DataField="No" HeaderText="No" />
                                    <asp:BoundField DataField="Type" HeaderText="Type" />
                                    <asp:BoundField DataField="Make" HeaderText="Make" />
                                    <asp:BoundField DataField="Capacity" HeaderText="Capacity" />
                                    <asp:BoundField DataField="Working_Condition" HeaderText="Working_Condition" />
                                </Columns>
                             <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">3.Documents  uploaded
                        </td>
                    </tr>
                    <tr>
                        <td>1.Photo&nbsp;&nbsp;<asp:Literal runat="server" ID="SPUPhoto" EnableViewState="false"></asp:Literal></td>
                        <td>2.Signature&nbsp;&nbsp;<asp:Literal runat="server" ID="SPUSign" EnableViewState="false"></asp:Literal></td>
                        <td>3.Seal&nbsp;&nbsp;<asp:Literal runat="server" ID="SPUSeal" EnableViewState="false"></asp:Literal></td>
                        <td>4.IDCard&nbsp;&nbsp;<asp:Literal runat="server" ID="IDCard" EnableViewState="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>5.Rental Agreement Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="RentLetter" EnableViewState="false"></asp:Literal></td>
                        <td>6.Building Map&nbsp;&nbsp;<asp:Literal runat="server" ID="BuildingMap" EnableViewState="false"></asp:Literal></td>
                        <td>7.Phani Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="PhaniLetter" EnableViewState="false"></asp:Literal></td>
                        <td>8.Current Bill&nbsp;&nbsp;<asp:Literal runat="server" ID="CurrentBill" EnableViewState="false"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>9.District industrial centre Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="CentralRegLetter" EnableViewState="false"></asp:Literal></td>
                        <td>10.GramPanchayat Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="GramPanchayatLetter" EnableViewState="false"></asp:Literal></td>
                        <td>11.Air Pollution Dep Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="AirPollutionDepLetter" EnableViewState="false"></asp:Literal></td>
                        <td>12.Purchase Letter&nbsp;&nbsp;<asp:Literal runat="server" ID="PurchaseLetter" EnableViewState="false"></asp:Literal></td>
                    </tr>



                    <asp:Panel ID="pnlPaymentDetails" runat="server" Visible="false">
                        <tr class="SideHeading">
                            <td colspan="4">4.Service Charge Details                       
                            </td>
                        </tr>
                        <tr>
                            <td>1.Registration Fee paid(in Rs.)</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblRegistrationFee" /></td>
                            <td>2.Payment Mode</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblPaymentMode" /></td>
                        </tr>
                        <tr>
                            <td>3.Cheque No./DD No./BankTransaction ID</td>
                            <td>
                                <asp:Label Font-Bold="true" runat="server" ID="lblBankTransactionID" />
                                <asp:Literal runat="server" ID="LitCheck" EnableViewState="false"></asp:Literal></td>
                            <td>4.Payment Date.</td>
                            <td>
                                <asp:Label ID="lblPaymentDate" Font-Bold="true" runat="server"></asp:Label>
                            </td>
                        </tr>

                    </asp:Panel>
                    <asp:Panel ID="pnlPayment" runat="server" Visible="false">
                        <tr class="SideHeading">
                            <td colspan="4">4.Service Charge Details                     
                            </td>
                        </tr>
                        <tr>
                            <td>1.Payment Mode</td>
                            <td>
                                <asp:RadioButtonList ID="rbPaymentMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbPaymentMode_SelectedIndexChanged">
                                    <asp:ListItem Text="DD/Cheque" Value="C" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="e-Payment" Value="O"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                            <td>2.Amount to be paid</td>
                            <td>Rs.<asp:Label ID="lblamounttobepaid" runat="server"></asp:Label>/-
                                <asp:Label ID="lblpaymenttype" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnPaymentType" runat="server" />
                            </td>
                        </tr>
                        <asp:Panel ID="pnlCheck" runat="server">
                            <tr>
                                <td>3.Cheque/DD Number</td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtCheckNo" placeholder="Cheque Number" TextMode="Number" onkeypress="return CheckNumber(event);" />
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredCheckNo" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="txtCheckNo" ErrorMessage="Cheque Number required"></asp:RequiredFieldValidator>
                                </td>
                                <td>4.Cheque/DD<span style="color: red">*</span></td>
                                <td>
                                    <asp:FileUpload runat="server" CssClass="form-control" ID="fu_Check" accept="image/*" />
                                    <asp:RequiredFieldValidator runat="server" ID="Requiredfu_Check" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="fu_Check" ErrorMessage="Cheque/DD Scanned Photo required"></asp:RequiredFieldValidator>
                                    <cst:FileTypeValidator runat="server" ID="FileTypeCheck" ForeColor="Red" Display="Dynamic"
                                        ControlToValidate="fu_Check" ErrorMessage="Allowed file types are {0}.">
                                    </cst:FileTypeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnCheck" Text="Save Payment Details" OnClick="btnCheck_Click" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel ID="pnlOnline" runat="server">
                            <tr>
                                <td colspan="4" align="center">
                                 <input type="submit" CssClass="btn-primary"   value="Pay Online" onclick="launchBOLT(); return false;" /> 
                                  <%--  <asp:Button runat="server" CssClass="btn-primary"
                                        ID="btnPayOnline" Text="Pay Online" OnClick="btnPayOnline_Click" /></td>--%>
                            </tr>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlRemarks" runat="server" Visible="false">
                        <tr>
                            <td>Remarks</td>
                            <td colspan="3">
                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="80%" Height="100px"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Display="Dynamic"
                                    ControlToValidate="txtRemarks" ErrorMessage="Give Remarks" Enabled="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td>Reject Type</td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rbRejectionType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Application Reject" Value="A" Selected="True">
                                    </asp:ListItem>
                                    <asp:ListItem Text="Payment Reject" Value="P" >
                                    </asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button runat="server" CssClass="btn-danger" CausesValidation="false"
                                    ID="btnReject" Text="Request to Resubmit" OnClick="btnReject_Click" />
                                <asp:Button runat="server" CssClass="btn-primary" CausesValidation="false"
                                    ID="Save" Text="Approve" OnClick="Save_Click" />
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr class="SideHeading">
                        <td colspan="4">5.Application Status                      
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btnEditApplication" runat="server" CssClass="btn-primary" Text="Edit Application" Visible="false" PostBackUrl="~/Transactions/SPURegistrationCreate.aspx" />
                            <br />
                            <asp:GridView ID="gvstatus" runat="server" class=" table-condensed" AutoGenerateColumns="false">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="FromID" HeaderText="From" />
                                    <asp:BoundField DataField="ToID" HeaderText="To" />
                                    <asp:BoundField DataField="TransactionStatus" HeaderText="Transaction Status" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                                    <asp:BoundField DataField="TransactionDate" HeaderText="Transaction Date" />
                                </Columns>
                             <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnCheck" />
            <asp:PostBackTrigger ControlID="btnReject" />
            <asp:PostBackTrigger ControlID="Save" />
        </Triggers>
    </asp:UpdatePanel>
<script type="text/javascript">
$(document).ready(function () {
    createHash();
 
});
    
</script>	
<script type="text/javascript"> 
    function launchBOLT() {
        //var encryptedk = CryptoJS.AES.encrypt("juvhC4rb", "Secret Passphrase");


        var encryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var encryptedKey =  '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var decryptedSalt = CryptoJS.AES.decrypt(encryptedSalt, "Secret Passphrase");
        var decryptedKey = CryptoJS.AES.decrypt(encryptedKey, "Secret Passphrase");

        var transId = $('#txnid').val();
        var hashid1 = $('#hash').val();

        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
         
        bolt.launch({
            key: decryptedKey.toString(CryptoJS.enc.Utf8),
            txnid: transId,
            hash: hashid1.toString(),
            amount: amt,
            firstname: fname,
            email: $('#emailid').val(),
            phone: mobilenumber,
            productinfo: "KSSOCA Payment",
            udf5: "BOLT_KIT_ASP.NET",
            surl: $('#surl').val(),
            furl: $('#surl').val()
        }, {
            responseHandler: function (BOLT) {
                console.log(BOLT.response.txnStatus);
                if (BOLT.response.txnStatus != 'CANCEL') {
                    //Salt is passd here for demo purpose only. For practical use keep salt at server side only.
                    var fr = '<form action=\"' + $('#surl').val() + '\" method=\"post\">' +
                        '<input type=\"hidden\" name=\"key\" value=\"' + BOLT.response.key + '\" />' +
                        '<input type=\"hidden\" name=\"salt\" value=\"' + decryptedSalt.toString(CryptoJS.enc.Utf8) + '\" />' +
                        '<input type=\"hidden\" name=\"txnid\" value=\"' + BOLT.response.txnid + '\" />' +
                        '<input type=\"hidden\" name=\"amount\" value=\"' + BOLT.response.amount + '\" />' +
                        '<input type=\"hidden\" name=\"productinfo\" value=\"' + BOLT.response.productinfo + '\" />' +
                        '<input type=\"hidden\" name=\"firstname\" value=\"' + BOLT.response.firstname + '\" />' +
                        '<input type=\"hidden\" name=\"email\" value=\"' + BOLT.response.email + '\" />' +
                        '<input type=\"hidden\" name=\"udf5\" value=\"' + BOLT.response.udf5 + '\" />' +
                        '<input type=\"hidden\" name=\"mihpayid\" value=\"' + BOLT.response.mihpayid + '\" />' +
                        '<input type=\"hidden\" name=\"status\" value=\"' + BOLT.response.status + '\" />' +
                        '<input type=\"hidden\" name=\"hash\" value=\"' + BOLT.response.hash + '\" />' +
                        '</form>';
                    var form = jQuery(fr);
                    jQuery('body').append(form);
                    form.submit();
                }
            },
                catchException: function (BOLT) {
                    alert(BOLT.message);
                }
            });
    }

    function createHash() {
        var amt = $('#amount').val();
        var fname = $('#fname').val();
        var mobilenumber = $('#mobilenumber').val();
        var transId = $('#txnid').val();
        var emailid = $('#emailid').val();
        var hashEncryptedSalt =  '<%=System.Configuration.ConfigurationManager.AppSettings["SaltKey"]%>';
        var hashEncryptedKey = '<%=System.Configuration.ConfigurationManager.AppSettings["Key"]%>';

        var hashDecryptedSalt = CryptoJS.AES.decrypt(hashEncryptedSalt, "Secret Passphrase");
        var hashDecryptedKey = CryptoJS.AES.decrypt(hashEncryptedKey, "Secret Passphrase");
       
        $.ajax({
            url: '/Transactions/Hash.aspx',
            type: 'post',

            data: JSON.stringify({
                key: hashDecryptedKey.toString(CryptoJS.enc.Utf8),
                salt: hashDecryptedSalt.toString(CryptoJS.enc.Utf8),
                txnid: transId,
                amount: amt,
                pinfo: "KSSOCA Payment",
                fname: fname,
                email: emailid,
                mobile: mobilenumber,
                udf5: "BOLT_KIT_ASP.NET"
            }),
            contentType: "application/json",
            dataType: 'json',
            success: function (json) {

                if (json['error']) {
                    $('#alertinfo').html('<i class="fa fa-info-circle"></i>' + json['error']);
                }
                else if (json['success']) {
                    $('#hash').val(json['success']);

                }
            }
        });

    }


</script>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
