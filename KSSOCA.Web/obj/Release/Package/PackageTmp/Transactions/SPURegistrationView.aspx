﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SPURegistrationView.aspx.cs" Inherits="KSSOCA.Web.Transactions.SPURegistrationView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" style="min-height: 600px;">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td>
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:ObjectDataSource runat="server" ID="SPURegistrationDataSource"
                                TypeName="KSSOCA.Web.Transactions.SPURegistrationView" SelectMethod="SelectMethod"></asp:ObjectDataSource>
                            <asp:GridView runat="server" ID="FormRegistrationGridView" DataSourceID="SPURegistrationDataSource"
                                AutoGenerateColumns="false" CssClass="table-condensed" Width="100%"
                                AllowPaging="true" PageSize="25" OnRowDataBound="FormRegistrationGridView_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="Sl.No.">
                                        <ItemTemplate>
                                            <%#Container.DataItemIndex+1 %> .
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="SPUNO" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="OfficialName" HeaderText="Firm Name" />
                                </Columns>

                                <Columns>
                                    <asp:BoundField DataField="districtName" HeaderText="District" />
                                </Columns>
                                <Columns>
                                    <asp:BoundField DataField="talukName" HeaderText="talukName" />
                                </Columns>

                                <Columns>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                            <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <Columns>
                                    <asp:HyperLinkField Text="View" DataNavigateUrlFields="Id"
                                        DataNavigateUrlFormatString="~/Transactions/SPURegistrationApproval.aspx?Id={0}"></asp:HyperLinkField>
                                </Columns>
                                <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
