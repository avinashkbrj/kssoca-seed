﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SeedSampleCouponCreate.aspx.cs" Inherits="KSSOCA.Web.Transactions.SeedSampleCouponCreate1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnl" runat="server">
        <ContentTemplate>
            <div align="center" class="container">
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>

                </div>

                <br />
                <table class="table-condensed" width="100%" border="1">
                    <tr class="Note">
                        <td colspan="4">
                            <asp:Literal ID="litNote" runat="server" EnableViewState="false"></asp:Literal>
                        </td>
                    </tr>
                    <tr class="SideHeading">
                        <td colspan="4">Seed Sapmling Information                       
                        </td>
                    </tr>
                    <tr>
                        <td width="25%">1.Sampling Center  </td>
                        <td width="25%">
                            <asp:Label ID="lblSamplingCenter" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td width="25%">2.SPU No  </td>
                        <td width="25%">
                            <asp:Label ID="lblSPUNo" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>3.Name of the seed producer
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblproducerName" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>4.Name & Address of seed grower      
                        </td>
                        <td colspan="3">
                            <asp:Label ID="lblGrowerName" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>5.Form-I Registration No.
                        </td>
                        <td>
                            <asp:Label ID="lblForm1NO" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>6.Crop
                        </td>
                        <td>
                            <asp:Label ID="lblCrop" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>7.Variety
                        </td>
                        <td>
                            <asp:Label ID="lblVariety" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>8.Class of seed
                        </td>
                        <td>
                            <asp:Label ID="lblClass" runat="server" Font-Bold="true"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td>9.Final Field Inspection No and Date
                        </td>
                        <td>
                            <asp:Label ID="lblFieldInspection" runat="server" Font-Bold="true"></asp:Label>
                        </td>

                        <td>10.Area Accepted(in acres)
                        </td>
                        <td>
                            <asp:Label ID="lblAcceptedArea" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>11.Lot Size (in Qntls)
                        </td>
                        <td>
                            <asp:Label ID="lblLotSize" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>12.Expected Yield(in Qntls)
                        </td>
                        <td>
                            <asp:Label ID="lblExpectedYield" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>13.Total Quantity of Yield (in Qntls)
                        </td>
                        <td>
                            <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" onkeypress="return CheckDecimal(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredQuantity" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtQuantity" ErrorMessage="Required Quantity."></asp:RequiredFieldValidator>
                        </td>
                        <td>14.Processed/Unprocessed</td>
                        <td>
                            <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbProcessed" runat="server">
                                <asp:ListItem class="radio-inline" Value="1" Text="Processed"></asp:ListItem>
                                <asp:ListItem class="radio-inline" Value="0" Text="Unprocessed" Selected="true"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredrbProcessed" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="rbProcessed" ErrorMessage="Required  Processed or Not."></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>15.Seed Treatment
                        </td>
                        <td>
                            <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbtreatment" runat="server">
                                <asp:ListItem Value="1" class="radio-inline" Text="Untreated"></asp:ListItem>
                                <asp:ListItem Value="0" class="radio-inline" Text="Treated" Selected="true"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>16.Name of the chemical
                        </td>
                        <td>
                            <asp:TextBox ID="txtchemical" runat="server" CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>17.Packing size
                        </td>
                        <td>
                            <asp:TextBox ID="txtpackingsize" runat="server" CssClass="form-control" onkeypress="return CheckNumber(event);"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="Requiredtxtpackingsize" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtpackingsize" ErrorMessage="Required packing size"></asp:RequiredFieldValidator>
                        </td>
                        <td>18.Lot No.
                        </td>
                        <td>
                            <asp:TextBox ID="txtlotno" runat="server" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="Requiredtxtlotno" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="txtlotno" ErrorMessage="Required lot no"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <%-- <tr>
                <td>19.Test Type
                </td>
                <td>
                    <asp:RadioButtonList RepeatLayout="Flow" RepeatDirection="Horizontal" Font-Bold="true" ID="rbTestType" 
                        runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbTestType_SelectedIndexChanged">
                        <asp:ListItem class="radio-inline" Value="STL" Text="STL" Selected="True"></asp:ListItem>
                        <asp:ListItem class="radio-inline" Value="GOT" Text="GOT"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>20.Tests Required
                </td>
                <td>
                    <asp:CheckBoxList ID="chkTests" runat="server" RepeatLayout="Flow" class="radio-inline" RepeatDirection="Vertical" Font-Bold="true" AutoPostBack="true" OnSelectedIndexChanged="chkTests_SelectedIndexChanged"></asp:CheckBoxList>
                   <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredchkTests" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="chkTests" ErrorMessage="Required Tests"></asp:RequiredFieldValidator>

                </td>
            </tr> --%>
                    <tr>
                        <td>21. Sampled By
                        </td>
                        <td>
                            <asp:Label ID="lblSampledBy" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        <td>22.Sampled Date
                        </td>
                        <td>
                            <asp:Label ID="lblSampledDate" runat="server" Font-Bold="true"></asp:Label></td>
                    </tr>
                     
                </table>
                <br />
                <div align="center">
                    <asp:Button runat="server" CssClass="btn-primary" CausesValidation="true"
                        ID="Save" Text="Save" OnClick="Save_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>
