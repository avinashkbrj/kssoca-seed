﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="SeedTransferRequestView.aspx.cs" Inherits="KSSOCA.Web.Transactions.SeedTransferRequestView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div align="center" style="min-height: 600px;">
        <asp:UpdatePanel ID="upnl" runat="server">
            <ContentTemplate>
                <div class="MainHeading">
                    <asp:Label runat="server" ID="lblHeading"> </asp:Label>
                    <asp:HiddenField ID="Id" runat="server" />
                </div>
                <div>
                    <asp:Label runat="server" ID="lblMessage" Font-Bold="true" ForeColor="Red"> </asp:Label>
                </div>
                
                <br />
                  <asp:Label runat="server" ID="checklbl" Font-Bold="true" ForeColor="Red"> </asp:Label>
                  <asp:Label runat="server" ID="checklbl1" Font-Bold="true" ForeColor="Red"> </asp:Label>
                  <asp:Label runat="server" ID="checklbl2" Font-Bold="true" ForeColor="Red"> </asp:Label>
                  <asp:Label runat="server" ID="checklbl3" Font-Bold="true" ForeColor="Red"> </asp:Label>
                                           
                <asp:GridView runat="server" ID="SeedTransferGridView"
                    AutoGenerateColumns="false" class="table-condensed" Width="100%"
                    AllowPaging="true" PageSize="25"  OnRowDataBound="SeedTransferGridView_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Sl.No.">
                            <ItemTemplate>
                                <%#Container.DataItemIndex+1 %> .
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    
                    <Columns>
                        <asp:BoundField DataField="SourceVerificationID" HeaderText="Source Verification No." />
                    
                        <asp:BoundField DataField="MobileNo" HeaderText="Mobile No" />
                   
                        <asp:BoundField DataField="District" HeaderText="District" />
                    
                        <asp:BoundField DataField="Taluk" HeaderText="Taluk" />
                     
                        <asp:BoundField DataField="Quantity" HeaderText="Quanity" />
                        <asp:BoundField DataField="RequestDate" HeaderText="Requested Date" />
                        <asp:BoundField DataField="SourceLocation" HeaderText="Source Location" />
                        <asp:BoundField DataField="Destinationlocation" HeaderText="Destination Location" />
                          <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                             
                                            <asp:HiddenField ID="hdnStatusCode" runat="server" Value='<%# Eval("StatusCode") %>' />
                                            <asp:Label runat="server" ID="lblStatus" Font-Bold="true"> </asp:Label>
                                        </ItemTemplate>
                          </asp:TemplateField>
                    </Columns> 
                      
                    <Columns> 
                        <asp:TemplateField>
                            <ItemTemplate>

                                <asp:LinkButton ID="lnkview" runat="server" Text="view" ForeColor="Navy" Font-Bold="true" PostBackUrl='<%# "~/Transactions/SeedTransferRequestApproval.aspx?Id="+ Eval("SourceVerificationID")+"&lotid="+ Eval("lotid")+"&tid="+ Eval("Id")%>'></asp:LinkButton>

                                </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>----------Records not found-----------</EmptyDataTemplate>
                </asp:GridView>
               </ContentTemplate>
            
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="footer" runat="server">
</asp:Content>


