﻿(function ($) {
    $(document).ready(function () {
        var dynamicTemplate;
        var cropData;
        var dynamicTable = $("#dynamicTable");

        bindCropData();

        $("#add_row").click(function () {
            var row = $("tbody tr", dynamicTable);
            var data = {
                "cropData": cropData, "templateData":
                [{
                    'Area': "",
                    'Location': "",
                    'Crop_Id': 1
                }]
            };

            var _htmlTemplate = prepareTemplate(data);

            if (row.length <= 0) {
                row = $("tbody", dynamicTable);
                $("tbody", dynamicTable).html(_htmlTemplate);
            }
            else {
                $(_htmlTemplate).insertAfter($(row).last());
            }
        });

        $("#delete_row").click(function () {
            $("tbody tr", dynamicTable).last().remove();
        });

        function prepareTemplate(data) {

            var cropData = data.cropData;
            var templateData = data.templateData;
            var tableRowsHtml = "";

            $.each(templateData, function (index, data) {
                var cropId = document.createElement('input');
                var area = document.createElement('input');
                var crops = document.createElement('select');
                var location = document.createElement('input');

                cropId.type = 'hidden';
                area.className = 'form-control';
                crops.className = 'form-control';
                location.className = 'form-control';

                cropId.name = 'CropId[]';
                area.name = 'area[]';
                crops.name = 'crop[]';
                location.name = 'location[]';

                cropId.defaultValue = (data.Area == null) ? "" : data.Id;
                area.defaultValue = (data.Area == null) ? "" : data.Area;
                location.defaultValue = (data.Location == null) ? "" : data.Location;

                for (var i = 0; i < cropData.length; i++) {
                    var options = document.createElement('option');
                    options.value = cropData[i].Id;
                    options.text = cropData[i].Name;

                    if (options.value == data.Crop_Id)
                        options.defaultSelected = true;

                    crops.appendChild(options);
                }

                var tr = document.createElement('tr');
                var td1 = document.createElement('td');
                var td2 = document.createElement('td');
                var td3 = document.createElement('td');

                td1.appendChild(cropId);
                td1.appendChild(crops);
                td2.appendChild(location);
                td3.appendChild(area);

                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);

                tableRowsHtml += tr.outerHTML;
            });

            return tableRowsHtml;
        }

        function bindCropData() {
            var options = {
                type: "POST",
                url: siteRoot + 'WebApi.aspx/GetAllCrops',
                async: true,
                cache: true,
                contentType: "application/json; charset=utf-8",
                failure: function (err) { console.log(err); },
                success: function (response) {
                    if (response != null && response.d != null) {
                        cropData = $.parseJSON(response.d);
                        var templateData = $.parseJSON($('#ContentPlaceHolder1_RegCropOfferred').val() || "{}");
                        var data = { "cropData": cropData, "templateData": templateData };
                        $("tbody", dynamicTable).html(prepareTemplate(data));
                    }
                }
            };
            $.ajax(options);
        };
    });
})(jQuery);