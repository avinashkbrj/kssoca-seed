﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KSSOCA.Core;
using KSSOCA.Data;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using KSSOCA.Core.Helper;
using Newtonsoft.Json;
using WebApi.Models;
using System.Web.Helpers;

namespace WebApi.Controllers
{
    [RoutePrefix("api/FieldInspection")]
    public class FieldInspectionController : ApiController
    {
        FieldInspectionApp fieldInspectionService;
        public FieldInspectionController()
        {
            fieldInspectionService = new  FieldInspectionApp();
        }
        public bool ValidateSecurityCode(string SecurityCode)
        {
            if (SecurityCode == "Kssoca_264@")
                return true;
            else return false;
        }

        [Route("UserInfo")]
        [HttpGet]
        public UserMaster GetLoginDetails(string UserName, string password, string SecurityCode)
        {
            if (ValidateSecurityCode(SecurityCode))
            {
                var usermaster = fieldInspectionService.GetLoginDetails(UserName, password);
                return usermaster;
            }
            else return null;
        }

        [HttpGet]
        [Route("Suggestions")]
        public List<FieldInspectionSuggestionMaster> GetSuggestionMaster(string SecurityCode)
        {
            if (ValidateSecurityCode(SecurityCode))
            {
                return fieldInspectionService.GetSuggestionMaster();
            }
            else
                return null;
        }
        [Route("Form1Details")]
        public Form1Data GetForm1Details(string GrowerRegNo, string SecurityCode)
        {
            try
            {
                if (ValidateSecurityCode(SecurityCode))
                {
                    return fieldInspectionService.GetForm1Details(GrowerRegNo);
                }
                else
                { return null; }
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                return null;
            }
        }
        [Route("SaveFieldInspectionDetails")]
        public int SaveFieldInspection(FieldInspectionReport fi, string SecurityCode)
        {
            return fieldInspectionService.SaveFieldInspection(fi);
        }
    }
}
