﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using KSSOCA.Core;
using KSSOCA.Data;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Extensions;
using KSSOCA.Core.Security;
using System.Data;
using KSSOCA.Core.Helper;
using Newtonsoft.Json;
using WebApi.Models;
using System.Web.Helpers;
using Microsoft.Ajax.Utilities;

namespace WebApi.Controllers
{
    [RoutePrefix("api/FIR")]
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values

        Models.FieldInspectionApp fieldInspectionService;
        public ValuesController()
        {
            fieldInspectionService = new Models.FieldInspectionApp();
        }

        public new class StatusCode
        {
            public string Status { get; set; }
        }
        [Route("Suggestions")]
        [HttpGet]
        public IHttpActionResult GetSuggestionMaster()
        {
            var sugg = fieldInspectionService.GetSuggestionMaster();
            if (sugg != null)
            {
                return Ok(sugg);
            }
            else return Ok("Suggestions not found.");
        }
        [Route("Login")]
        [HttpPost]

        public StatusCode Login([FromBody]  UserMaster um)
        {
            StatusCode stcd = new StatusCode();
            var usermaster = fieldInspectionService.GetLoginDetails(um.UserName, um.Password);
            if (usermaster == null)
            {
                stcd.Status = "Failed";
                return stcd;
            }
            else
            {
                stcd.Status = "Success";
                return stcd;
            }
        }

        [Route("GetUser")]
        [HttpGet]
        public IHttpActionResult GetUser(string UserName)
        {
            var stcd = new StatusCode();
            var um = fieldInspectionService.GetLoginDetails(UserName);
            if (um == null)
            {
                stcd.Status = "Failed";           
                return Ok(stcd);
            }
            else return Ok(um);
        }

        [Route("Form1Details")]
        [HttpGet]
        public IHttpActionResult GetForm1Details(string GrowerRegNo)
        {
            var stcd = new StatusCode();
            var f1data = fieldInspectionService.GetForm1Details(GrowerRegNo);
            if (f1data == null)
            {
                stcd.Status = "Failed";
                return Ok(stcd);
            }
            else return Ok(f1data);
        }
        [Route("SaveFieldInspectionDetails")]
        [HttpPost]
        public IHttpActionResult SaveFieldInspection([FromBody] FieldInspectionReport fi)
        {
            var stcd = new StatusCode();
            if (!ModelState.IsValid)
            {
                stcd.Status = "Failed";
                return Ok(stcd);
            }

            else
            {
                int i = fieldInspectionService.SaveFieldInspection(fi);
                if (i == 1)
                {
                    stcd.Status = "Success";
                    return Ok(stcd);
                }
                else
                {
                    stcd.Status = "Failed";
                    return Ok(stcd);
                }
            }
        }
    }
}
