﻿using System;
using System.Data;
using KSSOCA.Core.Extensions;
using KSSOCA.Data;
using KSSOCA.Model;
using KSSOCA.Core.Data;
using Newtonsoft.Json;
using System.Collections.Generic;
using KSSOCA.Core.Exceptions;
using KSSOCA.Core.Helper;
namespace WebApi.Models
{
    public class FieldInspectionApp
    {
        UserMasterService userMasterService;
        BankMasterService bankMasterService;
        CropMasterService cropMasterService;
        FormOneDetailsService formOneService;
        TalukMasterService talukMasterService;
        SeasonMasterService seasonMasterService;
        DistrictMasterService districtMasterService;
        TalukHobliMasterService HobliMasterService;
        VillageMasterService villageMasterService;
        ClassSeedMasterService classSeedMasterService;
        CropVarietyMasterService cropVarietyMasterService;
        SourceVerificationService sourceverficationservice;
        RegistrationFormService registrationFormService;
        Common common = new Common();
        FieldInspectionService fieldInspectionService;
        FieldInspectionSuggestionMasterService fieldInspectionSuggestionMasterService;
        FarmerDetailsService farmerDetailsService;
        F1LotNumbersService f1LotNumbersService;
        public FieldInspectionApp()
        {
            userMasterService = new UserMasterService();
            cropMasterService = new CropMasterService();
            bankMasterService = new BankMasterService();
            formOneService = new FormOneDetailsService();
            talukMasterService = new TalukMasterService();
            seasonMasterService = new SeasonMasterService();
            districtMasterService = new DistrictMasterService();
            classSeedMasterService = new ClassSeedMasterService();
            cropVarietyMasterService = new CropVarietyMasterService();
            villageMasterService = new VillageMasterService();
            HobliMasterService = new TalukHobliMasterService();
            sourceverficationservice = new SourceVerificationService();
            registrationFormService = new RegistrationFormService();
            fieldInspectionService = new FieldInspectionService();
            fieldInspectionSuggestionMasterService = new FieldInspectionSuggestionMasterService();
            farmerDetailsService = new FarmerDetailsService();
            f1LotNumbersService = new F1LotNumbersService();

        }
        // Field Inspection App Methods

        public UserMaster GetLoginDetails(string UserName, string password)
        {
            if (userMasterService.ValidateLogin(UserName, password, false))
            {
                UserMaster User = userMasterService.GetbyUserName(UserName);
                if (User.Role_Id == 4 || User.Role_Id == 6)
                {
                    var returningUser = new UserMaster()
                    {
                        Name = User.Name,
                        MobileNo = User.MobileNo,
                        EmailId = User.EmailId,
                        Id = User.Id
                    };
                    return returningUser;
                }
                else return null;
            }
            else return null;
        }
        public UserMaster GetLoginDetails(string UserName)
        {
            UserMaster User = userMasterService.GetbyUserName(UserName);
            if (User != null && (User.Role_Id == 4 || User.Role_Id == 6))
            {
                var returningUser = new UserMaster()
                {
                    Name = User.Name,
                    MobileNo = User.MobileNo,
                    EmailId = User.EmailId,
                    Id = User.Id
                };
                return returningUser;
            }
            return null;
        }

        public List<FieldInspectionSuggestionMaster> GetSuggestionMaster()
        {
            return fieldInspectionSuggestionMasterService.GetAll();
        }

        public Form1Data GetForm1Details(string Form1ID)
        {
            try
            {
                Form1Details f1 = formOneService.ReadById(Convert.ToInt16(Form1ID));
                if (f1 != null)
                {
                    FarmerDetails fardet = farmerDetailsService.GetById(f1.FarmerID);
                    SourceVerification sv = sourceverficationservice.ReadById(f1.SourceVarification_Id);
                    RegistrationForm rf = registrationFormService.GetByUserId(f1.Producer_Id);
                    UserMaster um = userMasterService.GetById(f1.Producer_Id);
                    string FarmerAddress = " District :" + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name
                     + ", Taluk:" + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id)).Name
                     + ", Hobli:" + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id)).Name
                     + ", Village:" + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(fardet.Taluk_Id), Convert.ToInt16(fardet.Hobli_Id), Convert.ToInt16(fardet.Village_Id)).Name
                     + ", Mobile Number:" + fardet.MobileNo;
                    string LocationOfSeedPlot = " District :" + districtMasterService.GetById(Convert.ToInt16(fardet.District_Id)).Name
                     + ", Taluk:" + talukMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id)).Name
                     + ", Hobli:" + HobliMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id)).Name
                     + ", Village:" + villageMasterService.GetBy(Convert.ToInt16(fardet.District_Id), Convert.ToInt16(f1.PlotTaluk_Id), Convert.ToInt16(f1.PlotHobli_Id), Convert.ToInt16(f1.PlotVillage_Id)).Name
                     + ", Gram Panchayat:" + f1.PlotGramPanchayath;

                    string InspectionDetail = GetLastInspectionDetails(f1.Id);
                    string[] InspectionDetails = InspectionDetail.Split(',');
                    int InspectionNo = Convert.ToInt16(InspectionDetails[0]);

                    string LastInspectionDetails = InspectionDetails[1];
                    string LotNos = f1LotNumbersService.LotNumbers(f1.Id);
                    if (InspectionNo > 0 && f1.StatusCode == 1)
                    {
                        var Suggestion = fieldInspectionSuggestionMasterService.GetAll();
                        Form1Data fi = new Form1Data();
                        fi.Form1ID = Convert.ToString(f1.Id);
                        fi.InspectionSerialNo = InspectionNo == 1 ? "First" : InspectionNo == 2 ? "Second" : InspectionNo == 3 ? "Third" : "Fourth";
                        fi.FarmerAddress = FarmerAddress;
                        fi.FarmerName = fardet.Name;
                        fi.FatherName = fardet.FatherName;
                        fi.SurveyNo = f1.SurveyNo.ToString();
                        fi.ClassOfSeed = classSeedMasterService.GetById(Convert.ToInt16(f1.ClassSeedtoProduced)).Name;
                        fi.AreaRegistered = f1.Areaoffered.ToString();
                        fi.DateOfSowingAsForm1 = f1.ActualDateofSowing != null ? f1.ActualDateofSowing.Value.ToString("dd/MM/yyyy") : "";
                        fi.Season = seasonMasterService.GetById(Convert.ToInt16(f1.Season_Id)).Name;
                        fi.Crop = cropMasterService.GetById(sv.Crop_Id).Name;
                        fi.Variety = cropVarietyMasterService.GetById(sv.Variety_Id).Name;
                        fi.LotNo = LotNos;
                        fi.ProducerName = um.Name;
                        fi.ProducerRegNo = rf.RegistrationNo;
                        fi.LocationOfSeedPlot = LocationOfSeedPlot;
                        fi.LastInspectionDetails = LastInspectionDetails;
                        return fi;
                    }
                    return null;
                }
                else { return null; }

            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                return null;
            }
        }
        public string GetLastInspectionDetails(int Form1Id)
        {
            try
            {
                int InspectionSerialNo = 0;
                string LastInspectionDetails = "";
                string query1 = "SELECT *  FROM  FieldInspection where Form1Id=" + Form1Id + "";
                DataTable dt = common.GetData(query1);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToInt16(dt.Rows[i]["IsFinal"]) > 0)
                        {
                            return " 0,";
                        }
                        LastInspectionDetails += dt.Rows[i]["InspectionSerialNo"] + ":" + Convert.ToDateTime(dt.Rows[i]["InspectedDate"]).ToString("dd/MM/yyyy") + " ";
                    }
                    string query = "SELECT ISNULL(Max(InspectionSerialNo), 0)   FROM  FieldInspection where Form1Id=" + Form1Id + "";
                    InspectionSerialNo = Convert.ToInt16(common.ExecuteScalar(query));
                }
                return Convert.ToString(InspectionSerialNo + 1) + "," + LastInspectionDetails;
            }
            catch (Exception ex)
            {
                ApplicationExceptionHandler.Handle(ex);
                return " 0,";
            }
        }
        public int SaveFieldInspection(FieldInspectionReport fi)
        {
            try
            {
                string Data = JsonConvert.SerializeObject(fi);
                string Dataquery = "insert into FieldInspectionAppData values('" + Data + "','" + System.DateTime.Now + "'," + fi.Form1Id + ")";
                common.ExecuteNonQuery(Dataquery);

                string InspectionDetail = GetLastInspectionDetails(Convert.ToInt16(fi.Form1Id));
                string[] InspectionDetails = InspectionDetail.Split(',');
                int InspectionSerialNo = Convert.ToInt16(InspectionDetails[0]);
                string LastInspectionDetails = InspectionDetails[1];
                var user = userMasterService.GetById(Convert.ToInt16(fi.InspectedBy));
                if (user.Role_Id == 4 || user.Role_Id == 6)
                {
                    if (InspectionSerialNo > 0 && InspectionSerialNo <= 4)
                    {
                        var FI = new FieldInspection()
                        {
                            Id = common.GenerateID("FieldInspection"),
                            InspectionSerialNo = InspectionSerialNo,
                            IsFinal = fi.IsFinal == true ? 1 : InspectionSerialNo == 4 ? 1 : 0,
                            Form1Id = Convert.ToInt16(fi.Form1Id),
                            ActualDateOfSowing = fi.ActualDateOfSowing,
                            DocumentsInvestigated = fi.DocumentsInvestigated,
                            CropStage = fi.CropStage,
                            LastCrop = fi.PreviousCrop,
                            LastCropVariety = fi.PreviousCropVariety,
                            FemaleMaleLinesRatio = fi.FemaleMaleLinesRatio,
                            MaleLines = fi.MaleLines,
                            DistanceBetweenLines = fi.IsolationDistance,
                            InspectedArea = fi.InspectedArea,
                            RejectedArea = fi.RejectedArea,
                            AcceptedArea = fi.AcceptedArea,
                            PlantCount = fi.PlantCountObserved,
                            StateOfCrop = fi.StateOfCrop,
                            Quality = fi.QualityofSeedProduction,
                            EstimatedYield = fi.EstimatedYield.ToString(),
                            HarvestingMonth = fi.HarvestingMonth,
                            HarvestingFortNight = fi.HarvestingFortNight,
                            Suggestions = fi.Suggestions,
                            Remarks = fi.Remarks,
                            FieldImage = fi.FieldImage,
                            InspectedBy = fi.InspectedBy,
                            InspectedDate = fi.InspectedDate,// System.DateTime.Now,
                            lattitude = fi.lattitude,
                            Longitude = fi.Longitude,
                            AreaRejectedDueTo = fi.AreaRejectedDueTo,
                            IPAddress = common.GetIPAddress(),
                            StatusCode = fi.IsAreaAccepted == true ? 1 : 0,
                        };
                        int resultCount = fieldInspectionService.Create(FI);
                        if (resultCount > 0)
                        {
                            string query = "select Max(Id) from FieldInspection where Form1Id=" + fi.Form1Id + "";
                            int ID = Convert.ToInt16(common.ExecuteScalar(query));
                            var FIRCountDetails = fi.FieldInspectionDetails;
                            InsertCropDetailsData(ID, FIRCountDetails);
                        }
                        else
                        {
                            return 0;
                        }
                        return resultCount;
                    }
                    else return 0;
                }
                else return 0;
            }
            catch (Exception ex)
            {
                string query = "select Max(Id) from FieldInspection where Form1Id=" + fi.Form1Id + "";
                int ID = Convert.ToInt16(common.ExecuteScalar(query));
                fieldInspectionService.Delete(ID);
                ApplicationExceptionHandler.Handle(ex);
                return 0;
            }
        }

        public void InsertCropDetailsData(int FieldInspectionId, List<FieldInspectionCropDetail> FIRCountDetails)
        {

            //foreach (FieldInspectionCropDetail fic in FIRCountDetails)
            //{
            //    var fi=new FieldInspectionCropDetail()
            //    {
            //         offGenderFemale=fic.offGenderFemale,
            //          offGenderMale=fic.offGenderMale,
            //           offPollenFemaleLines=fic.offPollenFemaleLines,
            //            offPollenFemalePlants=fic.offPollenFemalePlants,
            //             offReceptive=fic.offReceptive,
            //              offVariety=fic.offVariety,
            //               seedGenderFemale=fic.seedGenderFemale,
            //                seedGenderMale=fic.seedGenderMale,
            //                 seedOtherCrop=fic.seedOtherCrop,
            //                  seedVariety=fic,  
            //    }
            //}
            //DataTable dt = Serializer.JsonDeSerialize<DataTable>(FIRCountDetails); 
            int i = 1;
            foreach (FieldInspectionCropDetail fic in FIRCountDetails)
            {
                decimal offGenderFemale = fic.offGenderFemale;
                decimal offGenderMale = fic.offGenderMale;// Convert.ToDecimal(dt.Rows[i]["offGenderMale"]);
                decimal offPollenFemaleLines = fic.offPollenFemaleLines;// Convert.ToDecimal(dt.Rows[i]["offPollenFemaleLines"]);
                decimal offPollenFemalePlants = fic.offPollenFemalePlants;// Convert.ToDecimal(dt.Rows[i]["offPollenFemalePlants"]);
                decimal offReceptive = fic.offReceptive;// Convert.ToDecimal(dt.Rows[i]["offReceptive"]);
                decimal offVariety = fic.offVariety;// Convert.ToDecimal(dt.Rows[i]["offVariety"]);
                decimal seedGenderFemale = fic.seedGenderFemale;// Convert.ToDecimal(dt.Rows[i]["seedGenderFemale"]);
                decimal seedGenderMale = fic.seedGenderMale;// Convert.ToDecimal(dt.Rows[i]["seedGenderMale"]);
                decimal seedOtherCrop = fic.seedOtherCrop;//Convert.ToDecimal(dt.Rows[i]["seedOtherCrop"]);
                decimal seedVariety = fic.seedVariety;// Convert.ToDecimal(dt.Rows[i]["seedVariety"]);
                decimal seedWeedPlants = fic.seedWeedPlants;// Convert.ToDecimal(dt.Rows[i]["seedWeedPlants"]);
                string query = @"insert into FieldInspectionCropDetails 
                                 values('" + i + "','"
                                 + FieldInspectionId + "','"
                                 + offGenderFemale + "','"
                                 + offGenderMale + "','"
                                 + offPollenFemaleLines + "','"
                                 + offPollenFemalePlants + "','"
                                 + offReceptive + "','"
                                 + offVariety + "','"
                                 + seedGenderFemale + "','"
                                 + seedGenderMale + "','"
                                 + seedOtherCrop + "','"
                                 + seedVariety + "','"
                                 + seedWeedPlants + "')";
                common.ExecuteNonQuery(query);
                i++;
            }
        }
    }
}