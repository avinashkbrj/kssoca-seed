﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KSSOCA.Model;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class FieldInspectionReport
    {
        [Required]
        public bool IsFinal { get; set; }
        [Required]
        public string Form1Id { get; set; }
        [Required]
        public System.DateTime ActualDateOfSowing { get; set; }
        [Required]
        public string DocumentsInvestigated { get; set; }
        [Required]
        public string CropStage { get; set; }
        public string PreviousCrop { get; set; }
        public string PreviousCropVariety { get; set; }
        public string FemaleMaleLinesRatio { get; set; }
        public Nullable<int> MaleLines { get; set; }
        public string IsolationDistance { get; set; }
        [Required]
        public decimal InspectedArea { get; set; }
        [Required]
        public Nullable<decimal> RejectedArea { get; set; }
        [Required]
        public decimal AcceptedArea { get; set; }
        [Required]
        public int PlantCountObserved { get; set; }
        [Required]
        public string StateOfCrop { get; set; }
        [Required]
        public string QualityofSeedProduction { get; set; }
        public string EstimatedYield { get; set; }
        public string HarvestingMonth { get; set; }
        public string HarvestingFortNight { get; set; }
        public string Suggestions { get; set; }
        public string Remarks { get; set; }
        [Required]
        public byte[] FieldImage { get; set; }  
        public string AreaRejectedDueTo { get; set; }
        public string lattitude { get; set; }
        public string Longitude { get; set; }
        [Required]
        public bool IsAreaAccepted { get; set; }
        [Required]
        public Nullable<int> InspectedBy { get; set; }
        [Required]
        public Nullable<System.DateTime> InspectedDate { get; set; }
        public List<FieldInspectionCropDetail> FieldInspectionDetails { get; set; }
    }
}